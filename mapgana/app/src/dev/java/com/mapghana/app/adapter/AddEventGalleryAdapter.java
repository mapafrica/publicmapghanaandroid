package com.mapghana.app.adapter;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.MediaAddAdpterListener;
import com.mapghana.util.file_path_handler.BitmapHelper;
import com.mapghana.util.file_path_handler.FilePathHelper;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class AddEventGalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private MediaAddAdpterListener adapterClickListener;
    private List<String> list;
    private List<String> imageList;
    private List<String> videoList;
    private Context context;
    private String media_type;


    public AddEventGalleryAdapter(Context context, String media_type, MediaAddAdpterListener adapterClickListener, List<String> list) {
        this.context = context;
        this.media_type = media_type;
        this.adapterClickListener = adapterClickListener;
        this.list = list;
        imageList = new ArrayList<>();
        videoList = new ArrayList<>();
        imageList.addAll(Constants.getImageMimeList());
        videoList.addAll(Constants.getVideoMimeList());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent, false);
            return new MyViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_recycle_view, parent, false);
            return new FooterViewHolder(itemView);
        } else return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            myViewHolder.setData(position);
        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
            footerViewHolder.setFooterData(position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.size() == 0 || position == list.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView footerText;
        private int position;

        public FooterViewHolder(View view) {
            super(view);
            footerText = view.findViewById(R.id.add_icon);
        }

        public void setFooterData(int position) {
            this.position = position;
            footerText.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            adapterClickListener.onAddClick(media_type);
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private int pos;
        private ImageView iv_close;
        final ImageView iv_gallery;
        private TypefaceTextView tv_video_duration;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv_close = itemView.findViewById(R.id.iv_close);
            iv_gallery = itemView.findViewById(R.id.iv_gallery);
            tv_video_duration = itemView.findViewById(R.id.tv_video_duration);
        }

        public void setData(int position) {
            pos = position;
            String path = list.get(position);

            FilePathHelper filePathHelper = new FilePathHelper();
            String mimeType = null;
            try {
                mimeType = filePathHelper.getMimeType(URLEncoder.encode(path, "utf-8"));
                if (imageList.contains(mimeType)) {
                    Glide.with(context).load(path)
                            .placeholder(R.drawable.pleasewait)
                            .dontAnimate()
                            .into(iv_gallery);
                    tv_video_duration.setVisibility(View.GONE);
                } else if (videoList.contains(mimeType)) {
                    tv_video_duration.setVisibility(View.VISIBLE);
                    double duration = getDuration(path) / 1000;
                    tv_video_duration.setText("" + milliSecondsToTimer(getDuration(path)));
                    BitmapHelper bitmapHelper = new BitmapHelper();
                    iv_gallery.setImageBitmap(bitmapHelper.getVideoBitmap(path));
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            iv_close.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_close:
                    adapterClickListener.onDeleteClick(pos, media_type);
                    break;
            }
        }
    }

    private long getDuration(String path) {
        File videoFile = new File(path);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        //use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(context, Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);

        retriever.release();
        return timeInMillisec;
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }
        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

}
