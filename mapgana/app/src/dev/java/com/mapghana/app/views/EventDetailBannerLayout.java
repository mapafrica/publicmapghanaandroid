package com.mapghana.app.views;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.adapter.EventMediaListAdapter;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventGallery;

import java.util.ArrayList;

public class EventDetailBannerLayout extends LinearLayout {

    private SingleEventViewBanner mSingleEventBanner;

    private MultiEventViewBanner mMultiEventBanner;

    private ImageView mJoinMemberOne;

    private ImageView mJoinMemberTwo;

    private ImageView mJoinMemberThree;

    private TextView mNumberOfEventMember;

    private TextView mAboutEvent;

    private TextView mScrollEventPreviousMedia;

    private RecyclerView mRvPreviousMedia;

    private TextView mContactEventInfo;


    public EventDetailBannerLayout(Context context) {
        super(context);
    }

    public EventDetailBannerLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EventDetailBannerLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            mSingleEventBanner = (SingleEventViewBanner) findViewById(R.id.layout_single_event_banner_layout);
            mMultiEventBanner = (MultiEventViewBanner) findViewById(R.id.layout_multi_event_banner_type);
            mJoinMemberOne = findViewById(R.id.join_people_one);
            mJoinMemberTwo =  findViewById(R.id.join_people_two);
            mJoinMemberThree =  findViewById(R.id.join_people_three);
            mNumberOfEventMember =  findViewById(R.id.event_member_join);
            mAboutEvent =  findViewById(R.id.about_event);
            mScrollEventPreviousMedia =  findViewById(R.id.scroll_event_previous_media);
            mRvPreviousMedia = findViewById(R.id.rv_previous_media);
            mContactEventInfo = findViewById(R.id.contact_event_info);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void bind(final EventDetail item, final String eventType, final boolean isDetailPage, final Context context) {

        if(item.getEvent_date_time().size() < 2) {
            mSingleEventBanner.setVisibility(VISIBLE);
            mSingleEventBanner.bind(item,context);
        }else {
            mMultiEventBanner.setVisibility(VISIBLE);
            mMultiEventBanner.bind(item,context);
        }

        initRecycleLayoutManager(context,item);
    }

    private void initRecycleLayoutManager(Context context, EventDetail item) {

        mRvPreviousMedia.setHasFixedSize(true);
        EventMediaListAdapter adapter = new EventMediaListAdapter(getContext(),item.getEvent_gallery(),null);

        mRvPreviousMedia.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mRvPreviousMedia.setAdapter(adapter);

    }
}
