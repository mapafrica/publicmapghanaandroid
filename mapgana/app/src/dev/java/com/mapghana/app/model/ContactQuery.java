package com.mapghana.app.model;

/**
 * Created by ubuntu on 16/1/18.
 */

public class ContactQuery {


    /**
     * code : 200
     * status : 1
     * error :
     * message : successfully created
     * data : {"name":"prashant","email":"chaudhary","phone":"9602635800","message":"hello0 this is testing data","updated_at":"2018-01-24 12:29:28","created_at":"2018-01-24 12:29:28","id":5}
     */

    private int code;
    private int status;
    private String error;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : prashant
         * email : chaudhary
         * phone : 9602635800
         * message : hello0 this is testing data
         * updated_at : 2018-01-24 12:29:28
         * created_at : 2018-01-24 12:29:28
         * id : 5
         */

        private String name;
        private String email;
        private String phone;
        private String message;
        private String updated_at;
        private String created_at;
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
