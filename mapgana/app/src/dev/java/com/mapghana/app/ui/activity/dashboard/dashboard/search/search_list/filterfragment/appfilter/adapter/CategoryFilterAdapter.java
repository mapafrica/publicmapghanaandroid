package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.model.Category;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class CategoryFilterAdapter extends BaseRecycleAdapter {
    private  AdapterClickListener adapterClickListener;
    private List<Category.DataBean> categoryList;
    private static final String TAG = "CategoryFilterAdapter";


    public CategoryFilterAdapter(AdapterClickListener adapterClickListener, List<Category.DataBean> categoryList) {
        this.adapterClickListener = adapterClickListener;
        this.categoryList = categoryList;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_category_filter;
    }

    @Override
    protected int getItemSize() {
        if (categoryList!=null && categoryList.size()>0){
            return categoryList.size();
        }
        return 0;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }
    class MyViewHolder extends ViewHolder{
        private TypefaceTextView tvTitle;
        private LinearLayout ll_header;
        private ImageView imgSel;
        private int pos;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle=(TypefaceTextView)itemView.findViewById(R.id.tvTitle);
            imgSel=(ImageView)itemView.findViewById(R.id.imgSel);
            ll_header=(LinearLayout)itemView.findViewById(R.id.ll_header);

        }

        @Override
        public void setData(int position) {
            pos=position;
            if (categoryList.get(position).isSelected()){
                imgSel.setVisibility(View.VISIBLE);
                displayLog(TAG, "setData: 1" );
                imgSel.setImageResource(R.drawable.green_selected);
                tvTitle.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
            }
            else {
                imgSel.setVisibility(View.INVISIBLE);
                displayLog(TAG, "setData: 2" );
                imgSel.setImageResource(R.drawable.selected);
                tvTitle.setTextColor(getContext().getResources().getColor(R.color.colorDarkGray));

            }
            tvTitle.setText(categoryList.get(position).getName());
            ll_header.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ll_header:
                    updateUI(pos);
                    adapterClickListener.onAdapterClickListener(pos);
                    break;
            }
        }

        private void updateUI(int pos){
            for (int i=0; i<categoryList.size(); i++){
                if (i==pos){
                    categoryList.get(i).setSelected(true);
                }
                else {
                    categoryList.get(i).setSelected(false);
                }
            }
            CategoryFilterAdapter.this.notifyDataSetChanged();
        }
    }
}
