package com.mapghana.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ubuntu on 6/1/18.
 */

public class Login {


    /**
     * status : 1
     * message : successfully login
     * data : {"id":7,"name":"k1","username":"k1","email":"k1@gmail.com","dob":"08-01-2018","gender":"female","phone_number":"2222222222","image":null,"lat":null,"long":null,"device_id":"121212","device_type":"A","location":"jaipur","role":"user","status":"0","created_at":"2018-01-12 06:32:30","updated_at":"2018-01-12 06:32:30"}
     * error :
     */

    private int status;
    private String message;
    private DataBean data;
    private String error;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static class DataBean {
        /**
         * id : 7
         * name : k1
         * username : k1
         * email : k1@gmail.com
         * dob : 08-01-2018
         * gender : female
         * phone_number : 2222222222
         * image : null
         * lat : null
         * long : null
         * device_id : 121212
         * device_type : A
         * location : jaipur
         * role : user
         * status : 0
         * created_at : 2018-01-12 06:32:30
         * updated_at : 2018-01-12 06:32:30
         */

        private int id;
        private String name;
        private String username;
        private String email;
        private String dob;
        private String gender;
        private String phone_number;
        private String image;
        private Object lat;
        @SerializedName("long")
        private Object longX;
        private String device_id;
        private String device_type;
        private String location;
        private String role;
        private String status;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getLat() {
            return lat;
        }

        public void setLat(Object lat) {
            this.lat = lat;
        }

        public Object getLongX() {
            return longX;
        }

        public void setLongX(Object longX) {
            this.longX = longX;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
