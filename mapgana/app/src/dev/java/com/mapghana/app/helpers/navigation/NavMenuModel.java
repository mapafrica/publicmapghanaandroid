package com.mapghana.app.helpers.navigation;

/**
 * Created by ubuntu on 28/12/17.
 */

public class NavMenuModel {

    private int iconPath;
    private String title;
    private boolean textRed=false;
    private  boolean bgGray=false;

    public NavMenuModel() {}

    public NavMenuModel(int iconPath, String title) {
        this.iconPath = iconPath;
        this.title = title;
    }

    public NavMenuModel(int iconPath, String title, boolean textRed, boolean bgGray) {
        this.iconPath = iconPath;
        this.title = title;
        this.textRed = textRed;
        this.bgGray = bgGray;
    }

    public int getIconPath() {
        return iconPath;
    }

    public void setIconPath(int iconPath) {
        this.iconPath = iconPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isTextRed() {
        return textRed;
    }

    public void setTextRed(boolean textRed) {
        this.textRed = textRed;
    }

    public boolean isBgGray() {
        return bgGray;
    }

    public void setBgGray(boolean bgGray) {
        this.bgGray = bgGray;
    }
}
