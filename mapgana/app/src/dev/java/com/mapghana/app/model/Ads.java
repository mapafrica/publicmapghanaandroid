package com.mapghana.app.model;

/**
 * Created by ubuntu on 19/3/18.
 */

public class Ads {


    /**
     * code : 200
     * error :
     * status : 1
     * message : successfully
     * data : {"video":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1519391780297.mp4"}
     */

    private int code;
    private String error;
    private int status;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * video : http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1519391780297.mp4
         */

        private String video;
        private String ads_url;

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getAds_url() {
            return ads_url==null?"":ads_url;
        }

        public void setAds_url(String ads_url) {
            this.ads_url = ads_url;
        }
    }
}
