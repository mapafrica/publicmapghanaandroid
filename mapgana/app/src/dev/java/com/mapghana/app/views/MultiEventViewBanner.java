package com.mapghana.app.views;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.App;
import com.mapghana.app.model.EventDateTime;
import com.mapghana.app.model.EventDetail;

import java.util.ArrayList;

public class MultiEventViewBanner extends LinearLayout {

    ImageView posterImage;

    ImageView gateKeepingIndicator;

    LinearLayout eventInfoSection;

    public MultiEventViewBanner(Context context) {
        super(context);
    }

    public MultiEventViewBanner(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiEventViewBanner(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            posterImage = findViewById(R.id.event_poster);
            gateKeepingIndicator = findViewById(R.id.event_gatekeeping_indicator);
            eventInfoSection =  findViewById(R.id.layout_event_info_section);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void bind(final EventDetail item, final Context context) {

        Glide.clear(posterImage);
        Glide.with(App.sharedInstance())
                .load(item.getImage())
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                            isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                            target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(posterImage);

        if(item.getGatekeeping_services().equalsIgnoreCase("Y")) {
            gateKeepingIndicator.setImageDrawable(context.getDrawable(R.drawable.ic_indicator_green_circle));
        }else {
            gateKeepingIndicator.setImageDrawable(context.getDrawable(R.drawable.ic_indicator_green_circle));

        }
        inflateEventInfoVIew(item.getEvent_date_time(),context);
    }

    private void inflateEventInfoVIew(ArrayList<EventDateTime> list, Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);

        for(EventDateTime section: list) {

            View inflatedLayout= inflater.inflate(R.layout.layout_event_info_section,null, false);
            TextView eventDayDate = inflatedLayout.findViewById(R.id.event_date_day);
            TextView eventVenue = inflatedLayout.findViewById(R.id.event_venue);
            TextView eventStartEndTime = inflatedLayout.findViewById(R.id.event_start_end_time);
                eventDayDate.setText(section.getTime());
            eventVenue.setText(section.getDate());
            eventStartEndTime.setText(section.getDate());
            eventInfoSection.addView(inflatedLayout);
        }

    }

    }

