package com.mapghana.app.ui.sidemenu.Event;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.EventListingAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.dialog.PopUpEventDetail;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventListing;
import com.mapghana.app.model.EventListingContent;
import com.mapghana.app.model.SectionDataModel;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.Constants;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;


public class EventListingFragment extends AppBaseFragment implements EventListCallback {

    private RecyclerView recyclerView;
    private ArrayList<SectionDataModel> allSampleData;
    private Context context;
    private EventListingAdapter adapter;
    private PopUpEventDetail customDialog;

    public static EventListingFragment newInstance() {
        EventListingFragment fragment = new EventListingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_event_listing;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void initializeComponent() {

        init();

        getView().findViewById(R.id.fab).setOnClickListener(view -> {
            navigateToAddEventScreen((AppBaseActivity) getActivity());
        });
        allSampleData = new ArrayList<SectionDataModel>();

        recyclerView = getView().findViewById(R.id.rv_event_listing);
        recyclerView.setHasFixedSize(true);
        adapter = new EventListingAdapter(context, allSampleData, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        recyclerView.setAdapter(adapter);


        recyclerView.setOnScrollChangeListener((view, i, i1, i2, i3) -> {

        });

        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).getEventListing();
        } else {
            displayToast(Constants.No_Internet);
        }

    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_EVENT_LISTING) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    EventListing eventListingData = gson.fromJson(s, EventListing.class);
                    if (eventListingData.getStatus() == 1) {
                        setRecyclerViewContent(eventListingData);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);

    }

    public void navigateToAddEventScreen(AppBaseActivity baseActivity) {
        baseActivity.pushFragment(AddEventFragment.newInstance(), true);
    }

    public void navigateToEventDetailScreen(AppBaseActivity baseActivity, EventDetail item) {
        baseActivity.pushFragment(EventDetailFragment.newInstance(item.getName(), item), true);
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Events));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    private void setRecyclerViewContent(EventListing eventListing) {


        for (EventListingContent eventListingContent : eventListing.getData()) {

            if (eventListingContent.getData().size() != 0) {

                SectionDataModel dm = new SectionDataModel();

                dm.setHeaderTitle(eventListingContent.getEvent_date());
                ArrayList<EventDetail> singleItem = new ArrayList<EventDetail>();
                for (EventDetail detail : eventListingContent.getData()) {
                    detail.setImage(eventListing.getImage_pathe() + "/" + detail.getImage());
                    singleItem.add(detail);
                }

                dm.setAllItemsInSection(singleItem);

                allSampleData.add(dm);
            }
        }
        adapter.setDataList(allSampleData);
    }

    @Override
    public void onClickEvent(EventDetail model) {
        if (model.getEvent_gallery().size() > 0) {
            showPopUp(model, EventListingFragment.this);
        } else {
            Toast.makeText(context, "No media available", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onPopUpClickEvent(EventDetail model) {
        navigateToEventDetailScreen((AppBaseActivity) getActivity(), model);
    }

    @Override
    public void onCloseIcon() {
        customDialog.dismiss();
    }

    private void showPopUp(EventDetail model, EventListCallback listener) {
        customDialog = new PopUpEventDetail(getActivity(), listener, model, true);
        if (customDialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            customDialog.getWindow().setLayout(width, height);
        }
        customDialog.setCancelable(false);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        customDialog.show();
    }

}

