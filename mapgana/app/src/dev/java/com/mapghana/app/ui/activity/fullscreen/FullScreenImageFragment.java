package com.mapghana.app.ui.activity.fullscreen;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.GalleryPojo;
import com.mapghana.util.CustomMediaController;

/**
 * A simple {@link Fragment} subclass.
 */
public class FullScreenImageFragment extends AppBaseFragment {

    private static final String TAG = FullScreenImageFragment.class.getSimpleName();
    private GalleryPojo galleryPojo;
    private CustomMediaController mediaController;

    private int play_position;
    private boolean isFirstTime = false;
    ImageView imageDisplay;

    public void setGalleryPojo(GalleryPojo galleryPojo) {
        this.galleryPojo = galleryPojo;
    }

    public void setPlay_position(int play_position) {
        this.play_position = play_position;
    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_full_screen_image;
    }

    @Override
    public void initializeComponent() {
        View viewLayout = getView();
        imageDisplay = viewLayout.findViewById(R.id.touchImageView);
        final VideoView video_view = (VideoView) viewLayout.findViewById(R.id.video_view);
        final ImageButton ib_start = (ImageButton) viewLayout.findViewById(R.id.ib_start);
        final RelativeLayout rl_main_view = (RelativeLayout) viewLayout.findViewById(R.id.rl_main_view);
        final RelativeLayout rl_view = (RelativeLayout) viewLayout.findViewById(R.id.rl_view);

        if (galleryPojo == null) return;


        if (!galleryPojo.isVideo()) {
            if (mediaController != null) {
                mediaController.setVisibility(View.GONE);
                mediaController.hide();
            }
            displayLog(TAG, galleryPojo.getImage());
            imageDisplay.setVisibility(View.VISIBLE);
            rl_view.setVisibility(View.GONE);
            Glide.with(getActivity()).load(galleryPojo.getImage())
                    .override(500, 500).placeholder(R.drawable.pleasewait).dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imageDisplay);


        } else if (galleryPojo.isVideo()) {
            imageDisplay.setVisibility(View.GONE);
            rl_view.setVisibility(View.VISIBLE);

            if (mediaController == null) {
                mediaController = new CustomMediaController(getContext());
                video_view.setMediaController(mediaController);
                mediaController.setAnchorView(video_view);
            }
            video_view.setVideoPath(galleryPojo.getImage());
            rl_main_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (galleryPojo.isVideo()) {
                        showMediaController(ib_start);
                    }
                }
            });


            ib_start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMediaController(ib_start);
                }
            });

            if (!isFirstTime) {
                if (play_position != 0) {
                    isFirstTime = true;
                    video_view.seekTo(play_position);
                }
            }

        }
    }

    private void showMediaController(ImageButton ib_start) {
        ib_start.setVisibility(View.GONE);
        mediaController.setVisibility(View.VISIBLE);
        mediaController.show();
    }

}
