package com.mapghana.app;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.gu.toolargetool.TooLargeTool;
import com.mapghana.app.service.LocationServiceListner;

import io.fabric.sdk.android.Fabric;


/**
 * Created by ubuntu on 6/12/17.
 */

public class App extends Application {


    private static App mSharedInstance;
    LocationServiceListner locationServiceListner;
    public Location mLocation;
    public static App sharedInstance() {
        return mSharedInstance;
    }

    public LocationServiceListner getLocationServiceListner() {
        return locationServiceListner;
    }

    public void setLocationServiceListner(LocationServiceListner locationServiceListner) {
        this.locationServiceListner = locationServiceListner;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        mSharedInstance = this;
        Fabric.with(this, new Crashlytics());
        TooLargeTool.startLogging(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
