package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.GalleryPojo;
import com.mapghana.app.utils.Constants;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.util.file_path_handler.FilePathHelper;

import java.util.ArrayList;
import java.util.List;

public class SlidingImageAdapter extends PagerAdapter {


    private static final String TAG = "SlidingImageAdapter";
    private List<GalleryPojo> imageList;
    private List<String> image_mime_List;
    private List<String> video_mime_List;

    private VideoClickListener videoClickListener;
    private AdapterClickListener adapterClickListener;
    private LayoutInflater layoutInflater;
    private Context context;

    private int visible_status = 0, play_status = 0;
    private int currentPosition;

    public SlidingImageAdapter(Context context, List<GalleryPojo> imageList, AdapterClickListener adapterClickListener, VideoClickListener videoClickListener) {
        this.imageList = imageList;
        this.context = context;
        this.adapterClickListener = adapterClickListener;
        this.videoClickListener = videoClickListener;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        image_mime_List = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        image_mime_List.addAll(Constants.getImageMimeList());
        video_mime_List.addAll(Constants.getVideoMimeList());
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View img_layout = layoutInflater.inflate(R.layout.sliding_images_layout, container, false);
        assert img_layout != null;
        currentPosition = 0;
        ImageView imageView = (ImageView) img_layout.findViewById(R.id.slide_image);
        final VideoView video_view = (VideoView) img_layout.findViewById(R.id.video_view);
        final ImageButton ib_start = (ImageButton) img_layout.findViewById(R.id.ib_start);
        final ImageButton ib_full_screen = (ImageButton) img_layout.findViewById(R.id.ib_full_screen);
        final RelativeLayout rl_view = (RelativeLayout) img_layout.findViewById(R.id.rl_view);
        final RelativeLayout rl_main_view = (RelativeLayout) img_layout.findViewById(R.id.rl_main_view);

      /* MediaController mediaController= MediaControllerHandler.getInstance(context, video_view);
        video_view.setMediaController(mediaController);*/


        FilePathHelper filePathHelper = new FilePathHelper();
        final String mimeType = filePathHelper.getMimeType(imageList.get(position).getImage());
        String mediaPath = imageList.get(position).getImage();
        if (image_mime_List.contains(mimeType)) {
            imageView.setVisibility(View.VISIBLE);
            rl_view.setVisibility(View.GONE);
            Glide.with(context).load(mediaPath)
                    .override(500, 300).placeholder(R.drawable.pleasewait).dontAnimate().into(imageView);
        } else if (video_mime_List.contains(mimeType)) {
            imageView.setVisibility(View.GONE);
            rl_view.setVisibility(View.VISIBLE);
            video_view.setVideoPath(mediaPath);
            video_view.seekTo(1);
        }
        container.addView(img_layout, 0);
        rl_main_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (video_mime_List.contains(mimeType)) {
                    updateVisibilityOfVideo(ib_start, ib_full_screen);
                } else {
                    adapterClickListener.onAdapterClickListener(position);

                }
                // updateViibleStatusOfImages(ib_full_screen);
            }
        });


        ib_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (play_status == 0) {
                    play_status = 1;
                    ib_start.setVisibility(View.GONE);
                    ib_full_screen.setVisibility(View.GONE);
                    ib_start.setImageResource(R.mipmap.pause);
                    video_view.start();
                } else {
                    play_status = 0;
                    ib_start.setVisibility(View.GONE);
                    ib_full_screen.setVisibility(View.GONE);
                    ib_start.setImageResource(R.mipmap.play_icon);
                    video_view.pause();
                }
                // updateViibleStatusOfImages(ib_full_screen);
            }
        });

        ib_full_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoClickListener.onVideoClickListener(position, video_view.getCurrentPosition());
            }
        });
        return img_layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    private void updateVisibilityOfVideo(ImageButton ib_start, ImageButton ib_full_screen) {
        if (visible_status == 0) {
            visible_status = 1;
            if (ib_start.getVisibility() == View.VISIBLE) {
                ib_start.setVisibility(View.GONE);
                ib_full_screen.setVisibility(View.GONE);
            } else {
                ib_start.setVisibility(View.VISIBLE);
                ib_full_screen.setVisibility(View.VISIBLE);
            }
        } else {
            visible_status = 0;
            if (ib_start.getVisibility() == View.VISIBLE) {
                ib_start.setVisibility(View.GONE);
                ib_full_screen.setVisibility(View.GONE);
            } else {
                ib_start.setVisibility(View.VISIBLE);
                ib_full_screen.setVisibility(View.VISIBLE);
            }
        }
    }

    public interface VideoClickListener {
        void onVideoClickListener(int position, int current_position);
    }
}

