package com.mapghana.app.model;

/**
 * Created by ubuntu on 15/2/18.
 */

public class FilterModel {

    String city_id;
    String categoty_id;
    double lat;
    double log;
    String distance;

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCategoty_id() {
        return categoty_id;
    }

    public void setCategoty_id(String categoty_id) {
        this.categoty_id = categoty_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLog() {
        return log;
    }

    public void setLog(double log) {
        this.log = log;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
