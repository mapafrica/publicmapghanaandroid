package com.mapghana.app.ui.activity.dashboard.dashboard.add_location;


import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.android.ui.geocoder.GeocoderAutoCompleteView;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.services.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.services.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.services.commons.models.Position;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.dialog.MarkerDialog;
import com.mapghana.app.interfaces.EventMapListener;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.sidemenu.Event.AddEventFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingIndividualsFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingOrganizationFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AddLocationListener;
import com.mapghana.util.Valiations;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddLocationFragment extends AppBaseFragment
        implements AppBaseMapBox.OnMapLoaded, MapboxMap.OnCameraIdleListener,
        LocationServiceListner, GeocoderAutoCompleteView.OnFeatureListener {

    private GeocoderAutoCompleteView geocoderAutoCompleteView;
    private static final String TAG = "AddLocationFragment";
    private String type;
    private MapboxMap mapboxMap;
    private ImageView dropPinView;
    private String title;
    private TypefaceTextView tvLoc;
    private int DONT_CALL_TO_CAMERAIDLE = 1;
    private double latitude = 0;
    private double longitude = 0;
    private int zoomLevel;
    private Marker marker;
    private EventMapListener eventMapListener;

    public void setType(String type) {
        this.type = type;
    }

    public static AddLocationFragment newInstance() {
        AddLocationFragment fragment = new AddLocationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_location;
    }

    @Override
    public void initializeComponent() {
        title = "Your Selected Location";
        latitude = 0;
        longitude = 0;
        zoomLevel = 13;
        init();

        if (getArguments() != null) {
            type = getArguments().getString(Constants.type);
        }
        ImageView imgSearch = getView().findViewById(R.id.imgSearch);
        ImageButton imgMyLoc = getView().findViewById(R.id.imgMyLoc);
        dropPinView = getView().findViewById(R.id.dropPinView);
        TypefaceTextView tvDone = getView().findViewById(R.id.tvDone);
        tvLoc = getView().findViewById(R.id.tvLoc);
        geocoderAutoCompleteView = getView().findViewById(R.id.autoCompleteWidget);
        geocoderAutoCompleteView.setAccessToken(getString(R.string.mapbox_api_token));
        geocoderAutoCompleteView.setType(GeocodingCriteria.TYPE_POI);
        geocoderAutoCompleteView.setOnFeatureListener(this);
        imgSearch.setOnClickListener(this);
        dropPinView.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        imgMyLoc.setOnClickListener(this);

        initMap();

    }

    private void initMap() {
        if (getActivity() == null) return;
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnCameraIdleListeners(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());

    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setNavTitle(getString(R.string.Location));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSearch:
                geoCoding();
                break;
            case R.id.tvDone:
                changeFragment();
                break;
            case R.id.dropPinView:
                // openPopupMenu();
                break;
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
        }
    }

    private void setMarkersOnMapAgain() {

        if (getActivity() == null || !isVisible()) return;
        zoomLevel = 17;
        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        LatLng latLng_cur_pos = new LatLng(lat, log);
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(log);
        moveMarker(location);
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(latLng_cur_pos, zoomLevel);

    }
    public void initOnMapIconClickListener(EventMapListener eventMapListener) {
        this.eventMapListener = eventMapListener;

    }
    private void openPopupMenu() {
        MarkerDialog markerDialog = new MarkerDialog(getContext(), title, "");
        markerDialog.show();
    }

    private void changeFragment() {
        reverseGeoCoding(new LatLng(latitude,longitude));
        if (type.equalsIgnoreCase(Constants.ORGANISATION)) {
            EventInfoSection eventInfoSection = new EventInfoSection("","","","",String.valueOf(latitude), String.valueOf(longitude),title);
            ((AppBaseActivity) getActivity()).changeFragment(PostListingOrganizationFragment.newInstance(Constants.ORGANISATION,eventInfoSection), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } else if (type.equalsIgnoreCase(Constants.INDIVIDUALS)) {
            EventInfoSection eventInfoSection = new EventInfoSection("","","","",String.valueOf(latitude), String.valueOf(longitude),title);

            ((AppBaseActivity) getActivity()).changeFragment(PostListingIndividualsFragment.newInstance(Constants.INDIVIDUALS,eventInfoSection), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        }else if (type.equalsIgnoreCase(Constants.ADDEVENTS)) {
            eventMapListener.onMapIconClick(new EventInfoSection("","","","",String.valueOf(latitude), String.valueOf(longitude),title));
            AddEventFragment addEventFragment = new AddEventFragment();
            ((AppBaseActivity) getActivity()).changeFragment(addEventFragment, true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);

        }
    }

    private void geoCoding() {
        if (Valiations.hasText(geocoderAutoCompleteView)) {
            try {
                final String palceName = geocoderAutoCompleteView.getText().toString().trim();
                MapboxGeocoding mapboxGeocoding = new MapboxGeocoding.Builder()
                        .setAccessToken(getString(R.string.mapbox_api_token))
                        .setLocation(palceName)
                        .build();
                displayProgressBar(false);
                mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                    @Override
                    public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                        if (response.isSuccessful()) {
                            List<CarmenFeature> results = response.body().getFeatures();
                            dismissProgressBar();
                            if (results.size() > 0) {
                                // Log the first results position.
                                Position firstResultPos = results.get(0).asPosition();
                                LatLng latLng = new LatLng(firstResultPos.getLatitude(), firstResultPos.getLongitude());
                                title = palceName;
                                zoomMap(latLng);
                            } else {
                                displayToast("No result found...");
                            }
                        } else {
                            dismissProgressBar();
                            displayErrorDialog("Error", response.message());
                        }

                    }

                    @Override
                    public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                        throwable.printStackTrace();
                        dismissProgressBar();
                        displayErrorDialog("Error", throwable.getMessage());
                    }
                });


            } catch (ServicesException e) {
                e.printStackTrace();
                displayToast("Something went wrong...");
            }
        }
    }


    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        addMarker();
    }

    private void addMarker() {
        if (getActivity() == null || !isVisible()) return;

        List<Marker> markers = mapboxMap.getMarkers();
        List<Marker> markers2 = mapboxMap.getMarkers();
        List<LatLng> boundList = new ArrayList<>();
        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        final LatLng target = new LatLng(lat, log);
        boundList.add(target);
        marker = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.my_location, target, "You", "");
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(target, zoomLevel);

        //zoomMap(target);

    }

    private void zoomMap(LatLng target) {
        if (getActivity() == null || !isVisible()) return;
        if (DONT_CALL_TO_CAMERAIDLE != 0) {
            ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(target, 17);
        } else {
            DONT_CALL_TO_CAMERAIDLE = 1;
        }
        setAddress(title);
    }

    private void setAddress(String address) {
        tvLoc.setText(address);
    }

    @Override
    public void onCameraIdle() {
        CameraPosition cameraPosition = mapboxMap.getCameraPosition();
        LatLng latLng = cameraPosition.target;
        if (latLng != null) {
            title = "Your Selected Location";
            geocoderAutoCompleteView.setText("");
            latitude = latLng.getLatitude();
            longitude = latLng.getLongitude();
            setAddress("Location Picked: " + latLng.getLatitude() + ", " + latLng.getLongitude());
            reverseGeoCoding(latLng);
        }
    }

    @Override
    public void userLocationChange(Location location) {
        moveMarker(location);
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    private void moveMarker(Location location) {
        if (location == null || mapboxMap == null) {
            return;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (marker != null) {
            marker.setPosition(latLng);
        }
    }

    private void updateCurrentAddress(String address) {
        geocoderAutoCompleteView.setOnFeatureListener(null);
        geocoderAutoCompleteView.setText(address);
        geocoderAutoCompleteView.setOnFeatureListener(this);

    }

    private void reverseGeoCoding(LatLng latLng) {
        try {
            MapboxGeocoding mapboxGeocoding = new MapboxGeocoding.Builder()
                    .setAccessToken(getString(R.string.mapbox_api_token))
                    .setCoordinates(Position.fromCoordinates(latLng.getLongitude(), latLng.getLatitude()))
                    .setGeocodingType(GeocodingCriteria.TYPE_ADDRESS)
                    .build();

            mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                    dismissProgressBar();
                    List<CarmenFeature> results = response.body().getFeatures();
                    if (results.size() > 0) {
                        Position firstResultPos = results.get(0).asPosition();
                        String title = results.get(0).getPlaceName();
                        if (title != null && title.length() > 0) {
                            String[] array = title.split(",");
                            AddLocationFragment.this.title = title;
                            tvLoc.setText(title);

                            //geocoderAutoCompleteView.setText(title);
                        }
                    }

                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    throwable.printStackTrace();
                }
            });
        } catch (ServicesException e) {
            e.printStackTrace();
            dismissProgressBar();
            changeFragment();
        }

    }

    @Override
    public void onFeatureClick(CarmenFeature feature) {

        Position position = feature.asPosition();
        LatLng latLng = new LatLng(position.getLatitude(), position.getLongitude());
        title = geocoderAutoCompleteView.getText().toString().trim();
        zoomMap(latLng);
    }
}
