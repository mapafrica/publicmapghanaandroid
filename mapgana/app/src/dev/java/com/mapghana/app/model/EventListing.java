package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class EventListing implements Serializable {

 private int status;
 private String image_pathe;
 private String next_page_url;
 private String prev_page_url;
 private String message;
 private ArrayList<EventListingContent> data;
 private String error;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<EventListingContent> getData() {
        return data;
    }

    public String getError() {
        return error;
    }

    public String getImage_pathe() {
        return image_pathe;
    }

    public void setImage_pathe(String image_pathe) {
        this.image_pathe = image_pathe;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }
}

