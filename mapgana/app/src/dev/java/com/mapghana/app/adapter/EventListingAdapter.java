package com.mapghana.app.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mapghana.R;
import com.mapghana.app.model.SectionDataModel;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.app.utils.utils;

import java.text.ParseException;
import java.util.ArrayList;

public class EventListingAdapter extends RecyclerView.Adapter<EventListingAdapter.ItemRowHolder> {

    private ArrayList<SectionDataModel> mList;
    private Context mContext;
    private EventListingFragment mListener;

    public EventListingAdapter(Context context, ArrayList<SectionDataModel> dataList, EventListingFragment listener) {
        this.mList = dataList;
        this.mContext = context;
        this.mListener = listener;
    }

    public void setDataList(ArrayList<SectionDataModel> dataList) {
        this.mList = dataList;
        notifyDataSetChanged();
    }
    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_event_listing_horizontal_view, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String sectionName = mList.get(i).getHeaderTitle();

        ArrayList singleSectionItems = mList.get(i).getAllItemsInSection();

        itemRowHolder.itemTitle.setText(utils.formateddate(sectionName));

        EventSectionListAdapter itemListDataAdapter = new EventSectionListAdapter(mListener, singleSectionItems);
        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);

    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;

        protected RecyclerView recycler_view_list;

        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle =  view.findViewById(R.id.event_header_title);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.rv_item_event_horizontal);

        }

    }

}