package com.mapghana.app.app_base;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.mapghana.BuildConfig;
import com.mapghana.R;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.base.BaseActivity;
import com.mapghana.handler.NavigationViewHandlerInterface;
import com.mapghana.handler.ToolbarHandlerInterface;
import com.mapghana.rest.ApiHitListener;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ubuntu on 28/12/17.
 */

public abstract class AppBaseActivity extends BaseActivity
        implements NavigationViewHandlerInterface, ToolbarHandlerInterface, View.OnClickListener, ApiHitListener {


    private Dialog alertDialogProgressBar;
    private AlertDialog mErrorDialog;

    private static final String TAG = "AppBaseActivity";

    @Override
    public void setTitleButtonVisibiltyTB(boolean visibility) {

    }

    @Override
    public void setTitleTextTB(String title) {

    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {

    }

    @Override
    public void setbackButtonVisibiltyTB(boolean visibility) {

    }

    @Override
    public void setNavTitle(String title) {

    }

    @Override
    public void setToolbarVisibilityTB(boolean visibility) {
    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {

    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {

    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {

    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {

    }

    @Override
    public void setHeaderProfilePic(String uri, int res) {

    }

    @Override
    public void setUserName(String Name) {

    }

    @Override
    public void setNavLocationText(String Name) {

    }

    @Override
    public void lockDrawer(boolean visibilty) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {

    }

    @Override
    public void setNavLocationTextVisibilty(boolean visibilty) {

    }

    @Override
    public void setNavAddressText(String title) {

    }


    @Override
    public String getPostType() {
        return null;
    }


    public void displayProgressBar(boolean isCancellable) {

        alertDialogProgressBar = new Dialog(this,
                R.style.YourCustomStyle);
        alertDialogProgressBar.setCancelable(false);
        alertDialogProgressBar
                .requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.progress_dialog);

        alertDialogProgressBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        if (!isFinishing())
            alertDialogProgressBar.show();

    }

    public void dismissProgressBar() {
        if (!isFinishing() && alertDialogProgressBar != null) {
            alertDialogProgressBar.dismiss();
        }
    }

  /* private Handler myHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (!isFinishing()) {
                alertDialogProgressBar.show();;
            }
        }
    };*/

    public void displayErrorDialog(String title, String content) {
         if (!isFinishing()){
           mErrorDialog = new AlertDialog.Builder(this)
                //  .setTitle(title)
                .setMessage(content)
                // .setIcon(ContextCompat.getDrawable(this, R.mipmap.ic_launcher_round))
                .setCancelable(false)
                .setNegativeButton(R.string.dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();

            mErrorDialog.show();

        }

    }

    public void displayToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    public void displayLog(String TAG, String method, String msg) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, method + ": " + msg);

    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        int code = response.code();
        if (code == 404) {
            dismissProgressBar();
            SessionManager.logout(this);
            finish();
        }

    }

    public void goToLoginScreen(int code) {
        if (code == 404) {
            dismissProgressBar();
            SessionManager.logout(this);
            finish();
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {

    }


}
