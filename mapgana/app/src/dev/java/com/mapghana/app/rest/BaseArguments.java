package com.mapghana.app.rest;


import com.mapghana.BuildConfig;

public class BaseArguments {
    private static  final String user_domain= BuildConfig.User_URL;
    public static  final String domain= BuildConfig.DOMAIN_URL;
    private static  final String baseUrl= BuildConfig.BASE_URL;
    public static final String email = "email";
    public static final String id = "id";
    public static final String type = "type";

    //Registration screen params
    public static final String name = "name";
    public static final String address = "address";
    public static final String post_id = "post_id";
    public static final String username = "username";
    public static final String password = "password";
    public static final String gender = "gender";
    public static final String opening_hours = "opening_hours";
    public static final String phone_number = "phone_number";
    public static final String location = "location";
    public static final String device_type = "device_type";
    public static final String device_id = "device_id";
    public static final String user_id = "user_id";
    public static final String title = "title";
    public static final String post_for = "post_for";
    public static final String phone = "phone";
    public static final String website = "website";
    public static final String twitter = "twitter";
    public static final String dob = "dob";
    public static final String category_id = "category_id";
    public static final String city_id = "city_id";
    public static final String sub_category_id = "sub_category_id";
    public static final String features = "features";
    public static final String notes = "notes";
    public static final String tags = "tags";
    public static final String lat = "lat";
    public static final String log = "log";
    public static final String distance = "distance";
    public static final String image = "image";
    public static final String gallery_images = "gallery_images";
    public static final String message = "message";
    public static final String monday_friday = "monday-friday";
    public static final String saturday = "saturday";
    public static final String sunday = "sunday";
    public static final String start = "start";
    public static final String end = "end";
    public static final String rating = "rating";
    public static final String keyword = "keyword";
    public static final String page = "page";
    public static final String status = "status";


    public static final String details = "details";
    public static final String event_coverage_on = "event_coverage_on";
    public static final String event_coverage = "event_coverage";
    public static final String host_name = "host_name";
    public static final String paid_event = "paid_event";
    public static final String ticket_price = "ticket_price";
    public static final String mapgh_ticket_hosting= "mapgh_ticket_hosting";
    public static final String post_payment_channel = "post_payment_channel";
    public static final String branch_name = "branch_name";
    public static final String bank_name = "bank_name";
    public static final String account_number = "account_number";
    public static final String gatekeeping_services = "gatekeeping_services";
    public static final String previous_media_gallery = "event_previous_image";
    public static final String current_media_gallery = "event_image";
    public static final String event_section = "event_section";

    //add object
    public static final String scheduling = "scheduling";
    public static final String reference = "reference";
    public static final String about = "about";
    public static final String entry_period = "entry_period";
    public static final String exit_period = "exit_period";
    public static final String post_visibility = "post_visibility";
    public static final String one_time_use = "one_time_use";

    public static final String location_profile = "location_profile";
    public static final String phone_visibility = "phone_visibility";
    public static final String availability = "availability";
    public static final String optimization = "optimization";
    public static final String mapghanaid = "mapghanaid";
    public static final String instagram = "instagram";
    public static final String facebook = "facebook";
    public static final String youtube = "youtube";
    public static final String soundclud = "soundclud";
    public static final String blog = "blog";
    public static final String gaming = "gaming";
    public static final String profile_visibility = "profile_visibility";
    public static final String password_protected = "password_protected";
//.........................API NAMES.............................//

    public static final String signup = user_domain+"signup";
    public static final String login = user_domain+"login";
    public static final String forgot = user_domain+"forgot";
    public static final String profile = user_domain+"profile";
    public static final String logout = user_domain+"logout";


    public static final String post = baseUrl+"post";
    public static final String post_send_review = baseUrl+"post/send-review";
    public static final String post_listing_organizations= baseUrl+"post-listing/organizations";
    public static final String post_listing_Individuals= baseUrl+"post-listing/individuals";
    public static final String post_listing_type= baseUrl+"post-listing";



    public static final String category = "category";
    public static final String contact = "contact";
    public static final String city = "city";
    public static final String search = baseUrl+"search";
    public static final String ads = baseUrl+"ads";
    public static final String popular_category = baseUrl+"popular-category";


    public static final String event_listing = "http://67.205.155.52/mapghana/public/api/v1/five_date_events";
    public static final String add_event = "http://67.205.155.52/mapghana/public/api/v1/events";
    public static final String add_object = "http://67.205.155.52/mapghana/public/api/v1/objects";

}
