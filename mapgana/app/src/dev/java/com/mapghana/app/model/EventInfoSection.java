package com.mapghana.app.model;

import java.io.Serializable;

public class EventInfoSection implements Serializable {

    private String date;
    private String end_date;
    private String time;
    private String end_time;
    private String lat;
    private String log;
    private String Address;


    public EventInfoSection(String date, String end_date, String time, String end_time, String lat, String log, String address) {
        this.date = date;
        this.end_date = end_date;
        this.time = time;
        this.end_time = end_time;
        this.lat = lat;
        this.log = log;
        Address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
