package com.mapghana.app.app_base;

import android.content.Context;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.Cluster;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.ClusterItem;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.ClusterManagerPlugin;

import java.util.LinkedList;
import java.util.List;

public class CustomClusterManager<T extends ClusterItem> extends ClusterManagerPlugin<T> {

    public CustomClusterManager(Context context, MapboxMap map) {
        super(context, map);
    }


    public boolean itemsInSameLocation(Cluster<T> cluster) {
        LinkedList<T> items = new LinkedList<>(cluster.getItems());
        T item = items.remove(0);

        LatLng latLng = item.getPosition();
        if (latLng == null) return false;
        double longitude = latLng.getLongitude();
        double latitude = latLng.getLatitude();

        for (T t : items) {
            LatLng latLng1 = t.getPosition();
            if (latLng1 == null) return false;

            if (Double.compare(longitude, latLng1.getLongitude()) != 0 &&
                    Double.compare(latitude, latLng1.getLatitude()) != 0) {
                return false;
            }
        }

        return true;
    }

    public void removeItems(List<T> items) {

        for (T item : items) {
            removeItem(item);
        }
    }

}
