package com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map;


import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.model.GetDetails;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SingleItemOnMapFragment extends AppBaseFragment
        implements AppBaseMapBox.OnMapLoaded, MapboxMap.OnMarkerClickListener,
        MapboxMap.OnInfoWindowClickListener, LocationServiceListner {

    private int zoomLevel;
    private LatLng latLng;
    private String name;
    private String address;
    private double latitude, longitude;
    private int TYPE;
    private int resource;

    //   private DirectionsRoute currentRoute = null;
    private MapboxMap mapboxMap;
    private static final String TAG = "SingleItemOnMapFragment";
    private String detailResponse;
    private GetDetails data;
    private boolean hasResponse = false;
    private Marker marker;

    private int post_id;

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_single_item_on_map;
    }

    @Override
    public void initializeComponent() {
        init();
        zoomLevel = 13;

        TypefaceTextView tvViewDetails = (TypefaceTextView) getView().findViewById(R.id.tvViewDetails);
        ImageButton imgMyLoc = (ImageButton) getView().findViewById(R.id.imgMyLoc);

        tvViewDetails.setOnClickListener(this);
        imgMyLoc.setOnClickListener(this);

        initMap();
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        //getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    private void initMap() {
        if (getActivity() == null) return;
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnMarkerClickListener(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnCameraIdleListeners(null);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnInfoWindowClickListener(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
            case R.id.tvViewDetails:
                changeFragment();
                break;
        }
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        if (!hasResponse) {
            hasResponse = true;
            getDetails();
        } else {
            if (detailResponse != null) {
                updateUI(detailResponse);
            }
        }
    }

    private void getDetails() {
        //  if (getArguments() != null) {
        //  post_id = getArguments().getInt(Constants.post_id);
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).getDetailsOfOnePost(String.valueOf(post_id));
            //  restClient.callback(this).getDetailsOfOnePost("133");
        } else {
            displayToast(Constants.No_Internet);
        }
        // }
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        return false;
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        if (this.marker == marker) {
            return false;
        }
        changeFragment();
        return false;
    }

    private void changeFragment() {
        if (detailResponse == null) {
            return;
        }
        PostDetailFragment detailsFragment = new PostDetailFragment();
        detailsFragment.setPost_id(post_id);
       /* Bundle bundle = new Bundle();
        bundle.putInt(Constants.post_id, post_id);
        //  bundle.putString(Constants.details, detailResponse);
       *//* if (currentRoute!=null){
            detailsFragment.setDirectionsRoute(currentRoute);
        }*//*
        detailsFragment.setArguments(bundle);*/
        ((AppBaseActivity) getActivity()).changeFragment(detailsFragment, true, false, 0, R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_Details_1) {
                try {
                    detailResponse = response.body().string();
                    updateUI(detailResponse);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void updateUI(String s) {
        Gson gson = new Gson();
        data = gson.fromJson(detailResponse, GetDetails.class);
        if (data.getStatus() != 0) {
            latitude = data.getData().getLat();
            longitude = data.getData().getLog();
            try {
                latLng = new LatLng(latitude, longitude);
            } catch (IllegalArgumentException e) {
            }

            name = data.getData().getName();
            address = data.getData().getAddress();
            getNavHandler().setNavTitle(name);
            String category = "";
            if (data.getData().getPost_for() != null &&
                    data.getData().getPost_for().equalsIgnoreCase(Constants.individuals)) {
                String status = "";
                String gender = "";
                category = "Occupation: " + data.getData().getCategory().getName();
                status = data.getData().getStatus().trim();
                gender = data.getData().getGender().trim();
                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_gril;
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resource = R.mipmap.individual_red_girl;
                        TYPE = 2;
                    } else {
                        resource = R.mipmap.indi_girl_gray;
                        TYPE = 3;
                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_man;
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        TYPE = 2;
                        resource = R.mipmap.individual_red_man;
                    } else {
                        resource = R.mipmap.individual_gray_man;
                        TYPE = 3;
                    }
                }
            } else if (data.getData().getPost_for() != null &&
                    data.getData().getPost_for().equalsIgnoreCase(Constants.organizations)) {
                if (data.getData().getStatus() != null &&
                        data.getData().getStatus().equalsIgnoreCase(Constants.verified)) {
                    resource = R.mipmap.location_gree_org;
                    TYPE = 1;
                } else if (data.getData().getStatus() != null &&
                        data.getData().getStatus().equalsIgnoreCase(Constants.unverified)) {
                    resource = R.mipmap.location_red_org;
                    TYPE = 2;
                } else {
                    //(type==3)
                    resource = R.mipmap.location_gry_org;
                    TYPE = 3;
                }

                category = "Category: ";
                GetDetails.DataBean.CategoryBean categoryObj = data.getData().getCategory();
                if (categoryObj != null) {
                    category = category + categoryObj.getName();
                }
                GetDetails.DataBean.SubcategoryBean subcategoryObj = data.getData().getSubcategory();
                if (subcategoryObj != null) {
                    category = category + " (" + subcategoryObj.getName() + ")";
                }
            }
            if (latLng != null) {

                setMarkerFirstTime(latLng, name, address + "\n" + category);
            } else {
                displayToast("Invalid latitude and longitude");
            }
        } else {
            displayErrorDialog("Error", data.getError());
        }
    }

    private void setMarkerFirstTime(LatLng latLng, String title, String snippet) {
        if (getActivity() == null || !isVisible()) return;
        String status = "";
        if (TYPE == 1) {
            status = Constants.verified;
        } else if (TYPE == 2) {
            status = Constants.unverified;
        } else {
            //(type==3)
            status = Constants.anonymous;
        }
        ((DashboardActivity) getActivity()).appBaseMapBox.clearMarker();

        final List<LatLng> boundList = new ArrayList<>();
        ((DashboardActivity) getActivity()).appBaseMapBox.
                addMarker(getContext(), resource, latLng, title, "Address: " + snippet + "\nStatus: " + status);
        boundList.add(latLng);

        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        LatLng currentPos = new LatLng(lat, log);
        marker = ((DashboardActivity) getActivity()).appBaseMapBox.
                addMarker(getContext(), R.mipmap.my_location, currentPos, "You", "");
        boundList.add(currentPos);
        ((DashboardActivity) getActivity()).appBaseMapBox.boundMap(boundList);
    }

    private void setMarkersOnMapAgain() {
        if (getActivity() == null || !isVisible()) return;
        zoomLevel = 17;
        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        LatLng latLng_cur_pos = new LatLng(lat, log);
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(log);
        moveMarker(location);
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(latLng_cur_pos, zoomLevel);

    }

    @Override
    public void userLocationChange(Location location) {
        moveMarker(location);
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    private void moveMarker(Location location) {
        if (location == null || mapboxMap == null) {
            return;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (marker != null) {
            marker.setPosition(latLng);
        }
    }
}
