package com.mapghana.app.app_base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.MapboxConstants;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;
import com.mapghana.R;

import java.util.List;

/**
 * Created by ubuntu on 5/2/18.
 */

public class AppBaseMapBox implements OnMapReadyCallback, MapboxMap.OnInfoWindowClickListener,
        MapboxMap.OnMarkerClickListener, MapboxMap.OnCameraIdleListener {

    private static final String TAG = "AppBaseMapBox";
    private MapboxMap mapboxMap;
    private OnMapLoaded onMapLoaded;
    private MapboxMap.OnInfoWindowClickListener onInfoWindowClickListener;
    private MapboxMap.OnMarkerClickListener onMarkerClickListener;
    private MapboxMap.OnCameraIdleListener onCameraIdleListener;

    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public SupportMapFragment loadMap(FragmentManager fragmentManager){
         SupportMapFragment mapFragment ;

        Fragment fragment = fragmentManager.findFragmentById(R.id.map_contanier);
            MapboxMapOptions options = new MapboxMapOptions();
            options.styleUrl(Style.MAPBOX_STREETS);
            options.attributionEnabled(false);
            options.logoEnabled(false);
            options.compassEnabled(true);
            options.scrollGesturesEnabled(true);
            options.tiltGesturesEnabled(false);
            //options.rotateGesturesEnabled(false);
            options.camera(new CameraPosition.Builder()
                    // .target(patagonia)
                    .zoom(12)
                    .build());
            mapFragment = CustomSupportMapFragment.newInstance(options);

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.map_contanier, mapFragment, "com.mapbox.map");
            transaction.commit();
        mapFragment.getMapAsync(this);
        return mapFragment;
    }

    public void removeMap(FragmentManager fragmentManager){
      Fragment fragment= fragmentManager.findFragmentById(R.id.map_contanier);
      if (fragment!=null){
          FragmentTransaction transaction=fragmentManager.beginTransaction();
          transaction.remove(fragment);
          transaction.commit();
      }
    }

    public void zoomMap(LatLng target, float zoomLevel){
        CameraPosition cameraPosition=new CameraPosition.Builder().target(target).zoom(zoomLevel).build();
        mapboxMap.setCameraPosition(cameraPosition);
    }


    public void zoomMap(LatLng target){
        CameraPosition cameraPosition=new CameraPosition.Builder().target(target).zoom(13).build();
        mapboxMap.setCameraPosition(cameraPosition);
    }

    // TODO: 7/2/18   in onMapReady set all listeners like setOnMarkerClickListener on marker / setOnInfoWindowClickListener etc...
    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapboxMap=mapboxMap;

        onMapLoaded.onMapLoadedd(mapboxMap);
        if (onMarkerClickListener!=null){
            mapboxMap.setOnMarkerClickListener(this);
        }
        if (onInfoWindowClickListener!=null){
            mapboxMap.setOnInfoWindowClickListener(this);
        }
        if (onCameraIdleListener!=null){
            mapboxMap.setOnCameraIdleListener(this);
        }
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        if (onInfoWindowClickListener !=null) {
            onInfoWindowClickListener.onInfoWindowClick(marker);
        }
        return false;
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        if (onMarkerClickListener !=null) {
            onMarkerClickListener.onMarkerClick(marker);
        }
        return false;
    }

    @Override
    public void onCameraIdle() {
        if (onCameraIdleListener !=null){
            onCameraIdleListener.onCameraIdle();
        }
    }

    public Marker addMarker( LatLng latLng, String title, String snipped){

        Marker marker= mapboxMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(title)
                    .snippet(snipped));

       return marker;
    }

    public Marker addMarker(Context context, int resource, LatLng latLng, String title, String snipped){
        // Create Icon once so memory isn't waste it
        IconFactory iconFactory = IconFactory.getInstance(context);
        Drawable drawable;
        drawable = ContextCompat.getDrawable(context, resource);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        Icon icon = iconFactory.fromBitmap(bitmap);


        Marker marker= mapboxMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(title)
                .icon(icon)
                .snippet(snipped));
        return marker;
    }

    public void boundMap(final List<LatLng> boundList){
        if (boundList.size() > 1) {
            LatLngBounds latLngBounds = new LatLngBounds.Builder()
                    .includes(boundList)
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100));
        } else if (boundList.size()==1){
            mapboxMap.animateCamera(mapboxMap -> new CameraPosition.Builder()
                    .target(boundList.get(0))
                    .zoom(7)
                    .build());
        }
    }

    public void clearMarker() {
        if (mapboxMap != null) {
            mapboxMap.clear();
        }
    }

    public OnMapLoaded getOnMapLoaded() {
        return onMapLoaded;
    }

    public void setOnMapLoaded(OnMapLoaded onMapLoaded) {
        this.onMapLoaded = onMapLoaded;
    }

    public interface OnMapLoaded{
        void onMapLoadedd(MapboxMap mapboxMap);
    }

    public MapboxMap.OnInfoWindowClickListener getOnInfoWindowClickListener() {
        return onInfoWindowClickListener;
    }

    public void setOnInfoWindowClickListener(MapboxMap.OnInfoWindowClickListener onInfoWindowClickListener) {
        this.onInfoWindowClickListener = onInfoWindowClickListener;
    }

    public MapboxMap.OnMarkerClickListener getOnMarkerClickListener() {
        return onMarkerClickListener;
    }

    public void setOnMarkerClickListener(MapboxMap.OnMarkerClickListener onMarkerClickListener) {
        this.onMarkerClickListener = onMarkerClickListener;
    }

    public MapboxMap.OnCameraIdleListener getOnCameraIdleListener() {
        return onCameraIdleListener;
    }

    public void setOnCameraIdleListeners(MapboxMap.OnCameraIdleListener onCameraIdleListener) {
        this.onCameraIdleListener = onCameraIdleListener;
    }



   /* public LatLng getPointF(PointF point){
     return   mapboxMap.getProjection().fromScreenLocation(point);
   }*/

  public static class CustomSupportMapFragment extends SupportMapFragment{

      private OnTouchListener mListener;

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
          View layout= super.onCreateView(inflater, container, savedInstanceState);

          TouchableWrapper frameLayout = new TouchableWrapper (getActivity() );

          frameLayout.setBackgroundColor(getResources().getColor(
                  android.R.color.transparent));

          ((ViewGroup) layout).addView(frameLayout, new ViewGroup.LayoutParams(
                  ViewGroup.LayoutParams.MATCH_PARENT,
                  ViewGroup.LayoutParams.MATCH_PARENT));

          return layout;
      }

      @Override
       public void onViewCreated(View view, Bundle savedInstanceState) {
           super.onViewCreated(view, savedInstanceState);
       }

       public static CustomSupportMapFragment newInstance(@Nullable MapboxMapOptions mapboxMapOptions) {
           CustomSupportMapFragment mapFragment = new CustomSupportMapFragment();
           Bundle bundle = new Bundle();
           bundle.putParcelable(MapboxConstants.FRAG_ARG_MAPBOXMAPOPTIONS, mapboxMapOptions);
           mapFragment.setArguments(bundle);
           return mapFragment;
       }

      public class TouchableWrapper extends FrameLayout {

          public TouchableWrapper(Context context) {
              super(context);
          }

          @Override
          public boolean dispatchTouchEvent(MotionEvent event) {
              if (mListener!=null) {
                  switch (event.getAction()) {
                      case MotionEvent.ACTION_DOWN:
                          mListener.onTouch();
                          break;
                      case MotionEvent.ACTION_UP:
                          mListener.onTouch();
                          break;
                  }
              }
              return super.dispatchTouchEvent(event);
          }
      }

      public interface OnTouchListener {
          public abstract void onTouch () ;
      }

      public OnTouchListener getmListener() {
          return mListener;
      }

      public void setmListener(OnTouchListener mListener) {
          this.mListener = mListener;
      }
  }





}
/*
 // Create an Icon object for the marker to use
                 // Icon icon = IconFactory.getInstance(context).fromResource(R.mipmap.location_red);

 */