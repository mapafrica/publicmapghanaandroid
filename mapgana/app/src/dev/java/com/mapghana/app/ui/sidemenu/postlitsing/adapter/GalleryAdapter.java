package com.mapghana.app.ui.sidemenu.postlitsing.adapter;

import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.utils.Constants;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.util.file_path_handler.BitmapHelper;
import com.mapghana.util.file_path_handler.FilePathHelper;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by ubuntu on 23/2/18.
 */

public class GalleryAdapter extends BaseRecycleAdapter {

    private static final String TAG = "GalleryAdapter";


    private AdapterClickListener adapterClickListener;
    private List<String> list;
    private List<String> imageList;
    private List<String> videoList;

    public GalleryAdapter(AdapterClickListener adapterClickListener, List<String> list) {
        this.adapterClickListener = adapterClickListener;
        this.list = list;
        displayLog(TAG, "GalleryAdapter: ");
        imageList = new ArrayList<>();
        videoList = new ArrayList<>();
        imageList.addAll(Constants.getImageMimeList());
        videoList.addAll(Constants.getVideoMimeList());
    }


    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_gallery;
    }

    @Override
    protected int getItemSize() {
        if (list != null && list.size() > 0) {
            return list.size();
        }
        return 0;
    }

    @Override
    protected BaseRecycleAdapter.ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }


    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private int pos;
        private ImageView iv_close;
        final ImageView iv_gallery;
        private TypefaceTextView tv_video_duration;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv_close = (ImageView) itemView.findViewById(R.id.iv_close);
            iv_gallery = (ImageView) itemView.findViewById(R.id.iv_gallery);
            tv_video_duration = (TypefaceTextView) itemView.findViewById(R.id.tv_video_duration);
        }

        @Override
        public void setData(int position) {
            pos = position;
            String path = list.get(position);

            displayLog(TAG, "setData: " + path);
            FilePathHelper filePathHelper = new FilePathHelper();
            String mimeType = null;
            try {
                mimeType = filePathHelper.getMimeType(URLEncoder.encode(path,"utf-8"));
                if (imageList.contains(mimeType)) {
                    displayLog(TAG, "image path: " + path);
                    Glide.with(getContext()).load(path)
                            .placeholder(R.drawable.pleasewait)
                            .dontAnimate()
                            .into(iv_gallery);
                    tv_video_duration.setVisibility(View.GONE);
                } else if (videoList.contains(mimeType)) {
                    tv_video_duration.setVisibility(View.VISIBLE);
                    displayLog(TAG, "setData: " + TimeUnit.MILLISECONDS.toSeconds(getDuration(path)));
                    double duration = getDuration(path) / 1000;
                    tv_video_duration.setText("" + milliSecondsToTimer(getDuration(path)));

                    BitmapHelper bitmapHelper = new BitmapHelper();
                    displayLog(TAG, "video path: " + path);
                    iv_gallery.setImageBitmap(bitmapHelper.getVideoBitmap(path));
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }



            iv_close.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_close:

                    adapterClickListener.onAdapterClickListener(pos);
                    break;
            }
        }
    }

    private long getDuration(String path) {
        File videoFile = new File(path);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        //use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(getContext(), Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);

        retriever.release();
        displayLog(TAG, "time: " + time);
        displayLog(TAG, "timeInMillisec: " + timeInMillisec);
        return timeInMillisec;
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }
        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

}


