package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.IndividualPosts;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

import java.util.List;

/**
 * Created by ubuntu on 29/1/18.
 */

public class ItemListIndiAdapter extends BaseRecycleAdapter{

    AdapterClickListener adapterClickListener;

    private List<IndividualPosts.DataBean> list;


    public ItemListIndiAdapter(AdapterClickListener adapterClickListener, List<IndividualPosts.DataBean> list) {
        this.adapterClickListener = adapterClickListener;
        this.list = list;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_list_item;
    }

    @Override
    protected int getItemSize() {
        if (list!=null && list.size()>0){
            return list.size();
        }
        return 0;
    }

    @Override
    protected BaseRecycleAdapter.ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }
    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private TypefaceTextView tvName, tvRating, tvAddress;
        private LinearLayout llView;
        private ImageView imgSCat;
        private RatingBar rating_bar;
        private int pos;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvName=(TypefaceTextView)itemView.findViewById(R.id.tvName);
            tvRating=(TypefaceTextView)itemView.findViewById(R.id.tvRating);
            tvAddress=(TypefaceTextView)itemView.findViewById(R.id.tvAddress);
            llView=(LinearLayout)itemView.findViewById(R.id.llView);
            imgSCat=(ImageView)itemView.findViewById(R.id.imgSCat);
        }

        @Override
        public void setData(int position) {
            pos=position;
            llView.setOnClickListener(this);
            tvName.setText(list.get(position).getName());
            if (list.get(position).getImage()!=null && !list.get(position).getImage().equals("")) {
                Glide.with(getContext()).load(list.get(position).getImage())                                .override(250, 250)

                        .placeholder(R.drawable.pleasewait)
                        .dontAnimate()
                        .into(imgSCat);
            }
            else {
                imgSCat.setImageResource(R.mipmap.dinner_dummy);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.llView:
                    adapterClickListener.onAdapterClickListener(pos);
                    break;
            }
        }
    }
}
