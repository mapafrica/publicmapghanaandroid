package com.mapghana.app.rest;


/**
 * Created by ubuntu on 1/8/16.
 */
public class ApiIds {

    public static final int ID_SIGNUP = 1;
    public static final int ID_LOGIN = 2;
    public static final int ID_FORGOT = 3;
    public static final int ID_GETPROFILE = 4;
    public static final int ID_UPDATEPROFILE = 5;
    public static final int ID_CATEGORY = 6;
    public static final int ID_PostINDIVIDUAL = 7;
    public static final int ID_contact = 8;
    public static final int ID_contact_query = 9;
    public static final int ID_city = 10;
    public static final int ID_PostOrganization = 11;
    public static final int ID_logout = 12;
    public static final int ID_post_send_review = 13;
    public static final int ID_POSTS = 14;
    public static final int ID_POSTS_Filter_organization = 15;
    public static final int ID_POSTS_Filter_individual = 16;
    public static final int ID_POSTS_Details_1 = 17;
    public static final int ID_POSTS_BaseOnType = 18;
    public static final int ID_POSTS_SEarch = 19;
    public static final int ID_POSTS_SEarch_Filter = 20;
    public static final int ID_ADS= 21;
    public static final int ID_EVENT_LISTING= 22;
    public static final int ID_EVENT_ADD_EVENT= 23;
    public static final int ID_OBJECT = 24;


}
