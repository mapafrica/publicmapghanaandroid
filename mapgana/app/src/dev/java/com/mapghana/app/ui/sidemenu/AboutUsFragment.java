package com.mapghana.app.ui.sidemenu;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends AppBaseFragment {

    private static final String TAG = "AboutUsFragment";

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_about_us;
    }

    @Override
    public void initializeComponent() {
        init();
        TypefaceTextView tv_browse = (TypefaceTextView) getView().findViewById(R.id.tv_browse);
        tv_browse.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_browse:
                openWebsite();
                break;
        }
    }

    private void openWebsite() {
        try {
            String url = getString(R.string.browse);
            String domain = "http://";
            if (!url.startsWith(domain) && !url.startsWith("https://")) {
                url = domain + url;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(Intent.createChooser(intent, "Choose MapGhanaApplication"));
        } catch (ActivityNotFoundException e) {
            displayLog(TAG, "openWebsite excep: " + e.getMessage());
            displayErrorDialog("", "No apps found to open this link in your phone");
        }
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.About));
    }
}
