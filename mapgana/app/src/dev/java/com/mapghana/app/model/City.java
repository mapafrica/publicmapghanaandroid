package com.mapghana.app.model;

import java.util.List;

/**
 * Created by ubuntu on 23/1/18.
 */

public class City {


    /**
     * code : 200
     * error :
     * status : 1
     * message : successfully
     * data : [{"id":1,"name":"accra"},{"id":2,"name":"kumasi"},{"id":3,"name":"sunyani"},{"id":4,"name":"cape coast"},{"id":5,"name":"jaipur"}]
     */

    private int code;
    private String error;
    private int status;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : accra
         */

        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
