package com.mapghana.app.paymentSection;

import android.app.Activity;
import com.flutterwave.raveandroid.RavePayManager;
import com.mapghana.R;
import java.util.UUID;

public class MapghanaPayment {
    private static final String country = "NG";
    private static final String currency = "NGN";
    private static final String publicKey = "FLWPUBK_TEST-29ca751a33a62d117e04ba7a08b86278-X"; //Get your public key from your account
    private static final String encryptionKey = "FLWSECK_TEST0b9abf9cadc8"; //Get your encryption key from your account

    public static void makePayment(String email, String fName, String lName, String narration, int amount, Activity activity) {
        String txRef = email + " " + UUID.randomUUID().toString();

        /*
        Create instance of RavePayManager
         */
        new RavePayManager(activity).setAmount(amount)
                .setCountry(country)
                .setCurrency(currency)
                .setEmail(email)
                .setfName(fName)
                .setlName(lName)
                .setNarration(narration)
                .setPublicKey(publicKey)
                .setEncryptionKey(encryptionKey)
                .setTxRef(txRef)
                .acceptAccountPayments(true)
                .acceptCardPayments(
                        true)
                .acceptMpesaPayments(false)
                .acceptGHMobileMoneyPayments(false)
                .onStagingEnv(false).
                allowSaveCardFeature(true)
                .withTheme(R.style.DefaultTheme)
                .initialize();
    }

}


