package com.mapghana.app.utils;

import android.content.Context;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by ubuntu on 8/3/18.
 */

public class MediaControllerHandler {

    private static MediaController mediaController;

    private MediaControllerHandler(){
    }


    public static MediaController getInstance(Context context, VideoView videoView){
        if (mediaController==null){
            mediaController =new MediaController(context);
            mediaController.setAnchorView(videoView);
        }
        return mediaController;
    }

}
