package com.mapghana.app.ui.sidemenu.contactus;


import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.ContactQuery;
import com.mapghana.app.model.ContactUs;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Valiations;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends AppBaseFragment {


    private TypefaceTextView tvEmail, tvPhone, tvMobile, tvAddress, tvSubmit;
    private TypefaceEditText etName, etEmail, etPhone, etMessage;
    private LinearLayout ll_mail, ll_mobile, ll_phone;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_contact_us;
    }

    @Override
    public void initializeComponent() {


        tvEmail = (TypefaceTextView) getView().findViewById(R.id.tvEmail);
        tvPhone = (TypefaceTextView) getView().findViewById(R.id.tvPhone);
        tvMobile = (TypefaceTextView) getView().findViewById(R.id.tvMobile);
        tvSubmit = (TypefaceTextView) getView().findViewById(R.id.tvSubmit);
        tvAddress = (TypefaceTextView) getView().findViewById(R.id.tvAddress);
        etName = (TypefaceEditText) getView().findViewById(R.id.etName);
        etEmail = (TypefaceEditText) getView().findViewById(R.id.etEmail);
        etPhone = (TypefaceEditText) getView().findViewById(R.id.etPhone);
        etMessage = (TypefaceEditText) getView().findViewById(R.id.etMessage);
        ll_mail = getView().findViewById(R.id.ll_mail);
        ll_mobile = getView().findViewById(R.id.ll_mobile);
        ll_phone = getView().findViewById(R.id.ll_phone);


        init();

        tvSubmit.setOnClickListener(this);
        ll_mail.setOnClickListener(this);
        ll_mobile.setOnClickListener(this);
        ll_phone.setOnClickListener(this);
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).contact();
        } else {
            displayToast(Constants.No_Internet);
        }


    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Contact_Us));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
                hideKeyboard();
                onSubmit();
                break;
            case R.id.ll_mail:
                hideKeyboard();
                onSendMail();
                break;
            case R.id.ll_mobile:
                hideKeyboard();
                onCall(ll_mobile);
                break;
            case R.id.ll_phone:
                hideKeyboard();
                onCall(ll_phone);
                break;

        }
    }

    private void onSendMail() {
        if (tvEmail.getText().toString().trim().length() != 0) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{tvEmail.getText().toString().trim()});
            emailIntent.setType("message/rfc822");
            startActivity(Intent.createChooser(emailIntent, "Choose an email client:"));
        }

    }

    private void onCall(LinearLayout linearLayout) {
        String number = "";
        if (linearLayout == ll_phone) {
            number = tvPhone.getText().toString().trim();
            if (number.length() == 0) return;
        }
        if (linearLayout == ll_mobile) {
            number = tvMobile.getText().toString().trim();
            if (number.length() == 0) return;
        }

        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + number));
        startActivity(callIntent);
    }

    private void onSubmit() {

        if (Valiations.hasText(etName) && Valiations.isEmailAddress(etEmail, true)
                && Valiations.hasText(etPhone) && Valiations.hasText(etMessage)) {
            String name = etName.getText().toString().trim();
            if (name.length() < 3 || name.length() > 30) {
                displayToast("Name should contain 3 to 30 digits.");
                return;
            }
            String phone = etPhone.getText().toString().trim();
            if (phone.length() < 7 || phone.length() > 15) {
                displayToast("Mobile number should contain 7 to 15 digits");
                return;
            }
            if (ConnectionDetector.isNetAvail(getContext())) {
                displayProgressBar(false);
                RestClient restClient = new RestClient(getContext());

                restClient.callback(this).contactQuery(name, etEmail.getText().toString().trim(),
                        phone, etMessage.getText().toString().trim());
            } else {
                displayToast(Constants.No_Internet);
            }
        } else {
            displayToast("Fill All Fields");
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_contact) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    ContactUs contactUs = gson.fromJson(s, ContactUs.class);
                    if (contactUs.getStatus() != 0) {
                        tvEmail.setText(contactUs.getData().getEmail());
                        tvPhone.setText(contactUs.getData().getPhone());
                        tvMobile.setText(contactUs.getData().getMobile());
                        tvAddress.setText(Html.fromHtml(contactUs.getData().getAddress()));
                    } else {
                        displayErrorDialog("Error", contactUs.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_contact_query) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    ContactQuery contactUs = gson.fromJson(s, ContactQuery.class);
                    if (contactUs.getStatus() != 0) {
                        etName.setText("");
                        etEmail.setText("");
                        etPhone.setText("");
                        etMessage.setText("");
                        displayToast(contactUs.getMessage());
                        getActivity().onBackPressed();
                    } else {
                        displayErrorDialog("Error", contactUs.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }
}
