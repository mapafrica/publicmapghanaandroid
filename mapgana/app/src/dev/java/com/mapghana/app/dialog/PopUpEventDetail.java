package com.mapghana.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.App;
import com.mapghana.app.adapter.EventMediaListAdapter;
import com.mapghana.app.interfaces.MediaClickHandler;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventGallery;
import com.mapghana.app.ui.sidemenu.Event.EventListCallback;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.customviews.TypefaceTextView;

public class PopUpEventDetail extends Dialog implements
        android.view.View.OnClickListener, MediaClickHandler {
    private EventListCallback mListener;
    private EventDetail detail;
    private RecyclerView rvMedia;
    private Context context;
    private ImageView imageView;
    private VideoView videoView;
    private RelativeLayout rlImageContainer, rlVideoContainer;
    private ProgressBar progressBar;
    private ImageView playIcon;
    private boolean isButtonAvailable;
    private ImageView imageClose;

    public PopUpEventDetail(Context context, EventListCallback activity, EventDetail item, boolean isButtonAvailable) {
        super(context);
        this.mListener = activity;
        this.detail = item;
        this.context = context;
        this.isButtonAvailable = isButtonAvailable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_pop_up_event_detail);

        rvMedia = findViewById(R.id.rv_media_list);
        TypefaceTextView navigateToDetail = findViewById(R.id.go_detail);
        imageClose = findViewById(R.id.image_close);
        progressBar = findViewById(R.id.progress_bar);
        imageView = findViewById(R.id.image_view);
        videoView = findViewById(R.id.video_view);
        rlImageContainer = findViewById(R.id.rl_image);
        rlVideoContainer = findViewById(R.id.rl_video);
        playIcon = findViewById(R.id.video_icon);
        navigateToDetail.setOnClickListener(this);
        rlVideoContainer.setOnClickListener(this);
        imageClose.setOnClickListener(this);
        initRecycleLayoutManager();

        if(detail.getEvent_gallery().get(0).getType().equalsIgnoreCase("PHOTOS")) {
            initDialogBannerImage("PHOTOS", detail.getEvent_gallery().get(0).getFile_pathname());
        }else {
            initDialogBannerImage("VIDEOS", detail.getEvent_gallery().get(0).getFile_pathname());
        }

        if(isButtonAvailable) {
            navigateToDetail.setVisibility(View.VISIBLE);
        }else {
            navigateToDetail.setVisibility(View.GONE);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.go_detail:
                mListener.onPopUpClickEvent(detail);
                break;
            case R.id.image_close:
                mListener.onCloseIcon();
                break;
            default:
                break;
        }
        dismiss();
    }

    private void initRecycleLayoutManager() {

        rvMedia.setHasFixedSize(true);
        EventMediaListAdapter adapter = new EventMediaListAdapter(context, detail.getEvent_gallery(),this  );

        rvMedia.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvMedia.setAdapter(adapter);
    }


    private void initDialogBannerImage(String type, String url) {
        playIcon.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        if (type.equalsIgnoreCase("PHOTOS")) {
            rlVideoContainer.setVisibility(View.GONE);
            rlImageContainer.setVisibility(View.VISIBLE);
            Glide.clear(imageView);
            Glide.with(App.sharedInstance())
                    .load(url)
                    .dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                                isFirstResource) {
                            progressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                                target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }
                    })
                    .into(imageView);

        } else {
            rlVideoContainer.setVisibility(View.VISIBLE);
            rlImageContainer.setVisibility(View.GONE);
            Uri video = Uri.parse(url);
            videoView.setVideoURI(video);
            videoView.setOnPreparedListener(mp -> {
                mp.setLooping(true);
                playIcon.setVisibility(View.GONE);
                videoView.start();

            });
        }

    }

    @Override
    public void onClick(EventGallery gallery) {
        initDialogBannerImage(gallery.getType(),gallery.getFile_pathname());
    }

}
