package com.mapghana.app.ui.activity.dashboard.dashboard.search.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.utils.Constants;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.util.file_path_handler.BitmapHelper;
import com.mapghana.util.file_path_handler.FilePathHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class SearchAdapter extends BaseRecycleAdapter {

    private AdapterClickListener adapterClickListener;

    private List<GlobeSearch.DataBean> list;
    private   List<String> image_mime_List;
    private   List<String> video_mime_List;

    public SearchAdapter(AdapterClickListener adapterClickListener, List<GlobeSearch.DataBean> list) {
        this.adapterClickListener = adapterClickListener;
        this.list = list;
        image_mime_List=new ArrayList<>();
        video_mime_List=new ArrayList<>();
        image_mime_List.addAll(Constants.getImageMimeList());
        video_mime_List.addAll(Constants.getVideoMimeList());

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_text_view;
    }

    @Override
    protected int getItemSize() {
        if (list!=null && list.size()>0) {
            return list.size();
        }
        else {
            return 0;
        }
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder{
        private TypefaceTextView  tvTitle;
        private RelativeLayout rl_view;
        private ImageView iv_category_pic;
        private int pos;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle=(TypefaceTextView)itemView.findViewById(R.id.tvTitle);
            rl_view=(RelativeLayout)itemView.findViewById(R.id.rl_view);
            iv_category_pic=(ImageView)itemView.findViewById(R.id.iv_category_pic);
        }

        @Override
        public void setData(int position) {
            pos=position;
            tvTitle.setText(list.get(position).getName());
            rl_view.setTag(position);
            String imagePath=list.get(position).getImage();
            List<GlobeSearch.DataBean.PostGalleryBean> postGallery=list.get(position).getPostGallery();
            if (imagePath!=null && imagePath.length()!=0){
                Glide.with(getContext()).load(imagePath).override(250, 250)
                        .placeholder(R.drawable.pleasewait)
                        .dontAnimate()
                        .into(iv_category_pic);
            }
            else if (postGallery!=null && postGallery.size()>0){
                String mediaPath=postGallery.get(0).getImage();
                if (mediaPath!=null && mediaPath.length() >0) {
                    FilePathHelper filePathHelper = new FilePathHelper();
                    String mimeType = filePathHelper.getMimeType(imagePath);
                    if (image_mime_List.contains(mimeType)) {
                        Glide.with(getContext()).load(mediaPath)                                .override(250, 250)

                                .placeholder(R.drawable.pleasewait)
                                .dontAnimate()
                                .into(iv_category_pic);
                    } else if (video_mime_List.contains(mimeType)) {
                        BitmapHelper bitmapHelper = new BitmapHelper();
                        Bitmap bitmap=bitmapHelper.getVideoBitmap(mediaPath);
                        iv_category_pic.setImageBitmap(bitmap);
                    }
                }
            }
            rl_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.rl_view:
                    adapterClickListener.onAdapterClickListener((Integer)v.getTag());
                    break;
            }
        }
    }
}
