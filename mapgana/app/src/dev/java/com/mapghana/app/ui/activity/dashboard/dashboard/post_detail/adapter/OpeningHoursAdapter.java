package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.app.model.OpeningHour;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

import java.util.Map;

/**
 * Created by ubuntu on 30/12/17.
 */

public class OpeningHoursAdapter extends BaseRecycleAdapter {

    private Map<String, OpeningHour> map;

    public OpeningHoursAdapter(Map<String, OpeningHour> map) {
        this.map = map;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_openhours;
    }

    @Override
    protected int getItemSize() {
        return map.size();
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new OpeningHoursAdapter.MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder{
        private TypefaceTextView tvNameLeft, tvNameRight;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvNameLeft=(TypefaceTextView)itemView.findViewById(R.id.tvNameLeft);
            tvNameRight=(TypefaceTextView)itemView.findViewById(R.id.tvNameRight);
        }

        @Override
        public void setData(int position) {
          /*  tvNameLeft.setText();
            tvNameRight.setText(time[position]);*/
        }

    }
}
