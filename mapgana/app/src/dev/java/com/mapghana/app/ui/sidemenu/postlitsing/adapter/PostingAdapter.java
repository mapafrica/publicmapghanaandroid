package com.mapghana.app.ui.sidemenu.postlitsing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mapghana.R;
import com.mapghana.app.model.Category;
import com.mapghana.customviews.TypefaceTextView;

import java.util.List;

/**
 * Created by ubuntu on 12/1/18.
 */

public class PostingAdapter extends BaseAdapter {

    private Context context;
    private List<Category.DataBean> list;

    public PostingAdapter(Context context, List<Category.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        if (list!=null && list.size()>0){
            return list.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.item_simple_spinner, parent, false);
        }
        TypefaceTextView tvTitle=(TypefaceTextView)convertView.findViewById(R.id.tvTitle);
        tvTitle.setText(list.get(position).getName());
        return convertView;
    }
}
