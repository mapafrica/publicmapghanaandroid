package com.mapghana.app.ui.sidemenu.postlitsing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.Category;
import com.mapghana.customviews.TypefaceTextView;

import java.util.ArrayList;

/**
 * Created by ubuntu on 24/1/18.
 */

public class SubCategoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Category.DataBean.SubCategoryBean> subCategoryList;
    private Category.DataBean.SubCategoryBean selectedSubCategory;

    public Category.DataBean.SubCategoryBean getSubCategoryBean() {
        return selectedSubCategory;
    }

    public void setSubCategoryBean(int position) {
        this.selectedSubCategory = subCategoryList.get(position);
        notifyDataSetChanged();
    }


    public SubCategoryAdapter(Context context, ArrayList<Category.DataBean.SubCategoryBean> categoryList) {
        this.context = context;
        this.subCategoryList = categoryList;
    }

    @Override
    public int getCount() {
        if (subCategoryList != null && subCategoryList.size() > 0) {
            return subCategoryList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_spinner_with_image, parent, false);
        }
        TypefaceTextView tvTitle = (TypefaceTextView) convertView.findViewById(R.id.tvTitle);
        tvTitle.setText(subCategoryList.get(position).getName());
        ImageView imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
        ImageView imgDown = (ImageView) convertView.findViewById(R.id.imgDown);

        if (selectedSubCategory != null) {
            if (selectedSubCategory.equals(subCategoryList.get(position)))
                imgDown.setVisibility(View.VISIBLE);
            else
                imgDown.setVisibility(View.GONE);

        } else {
            imgDown.setVisibility(View.GONE);
        }

        if (position == 0) {
            imgCategory.setVisibility(View.GONE);
        } else {
            imgCategory.setVisibility(View.VISIBLE);
            if ((subCategoryList.get(position).getImage() != null) && !(subCategoryList.get(position).getImage().equals(""))) {
                Glide.with(context).load(subCategoryList.get(position).getImage()).override(150, 150)

                        .placeholder(R.drawable.pleasewait)
                        .dontAnimate()
                        .into(imgCategory);
            } else {
                imgCategory.setImageResource(R.mipmap.no_category);
            }
        }

        return convertView;
    }

}