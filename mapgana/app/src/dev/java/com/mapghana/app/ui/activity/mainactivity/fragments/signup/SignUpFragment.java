package com.mapghana.app.ui.activity.mainactivity.fragments.signup;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Signup;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends AppBaseFragment implements CustomDatePickerDialog.OnDateListener {

    private static final String TAG = "SignUpFragment";
    private LinearLayout llExistingUser;
    private Spinner spGender;
    private TypefaceTextView tvSignUp, tvDob;
    private EditText etUname, etPassword, etEmail, etPhone, etLoc, etFullName, etConPassword;
    private String DOB;
    SimpleSpinnerAdapter gender_adapter;
    private Date date;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {

        return R.layout.fragment_sign_up;
    }

    @Override
    public void initializeComponent() {
        getToolBar().setToolbarVisibilityTB(false);
        llExistingUser = (LinearLayout) getView().findViewById(R.id.llExistingUser);
//        spGender = (Spinner) getView().findViewById(R.id.spGender);
        tvSignUp = (TypefaceTextView) getView().findViewById(R.id.tvSignUp);
//        tvDob = (TypefaceTextView) getView().findViewById(R.id.tvDob);
        etUname = getView().findViewById(R.id.etUname);
//        etPassword = getView().findViewById(R.id.etPassword);
        etFullName = getView().findViewById(R.id.etFullName);
        etEmail = getView().findViewById(R.id.etEmail);
        etPhone = getView().findViewById(R.id.etPhone);
//        etLoc = getView().findViewById(R.id.etLoc);
//        etConPassword = getView().findViewById(R.id.etConPassword);
//        date = new Date();

     /*   etFullName.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub


                for( int i = 0;i<s.toString().length(); i++ ) {

                    if( !Character.isLetter(s.charAt( i ) ) ) {
                        s.replace(i, i+1,"");
                    }
                }
            }
        });*/

//        prepareData();
        tvSignUp.setOnClickListener(this);
//        tvDob.setOnClickListener(this);
        llExistingUser.setOnClickListener(this);

//        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                gender_adapter.setSelGender(position);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
    }

    private void prepareData() {
        gender_adapter = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.gender_array));
        gender_adapter.setSelGender(0);
        spGender.setAdapter(gender_adapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llExistingUser:
                hideKeyboard();
                getActivity().onBackPressed();
                break;
            case R.id.tvSignUp:
                hideKeyboard();
                onSignup();
                break;
//            case R.id.tvDob:
//                hideKeyboard();
//                onDobDialog();
//                break;
        }
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        if (dialog != null) {
            dialog.create(getContext(), true).callback(this).show();
        }
    }

    private void onSignup() {

        String name, password, conPassword, mail, phone, loc, full_name;
        name = etUname.getText().toString().trim();
        full_name = etFullName.getText().toString().trim();
//        password = etPassword.getText().toString().trim();
//        conPassword = etConPassword.getText().toString().trim();
        mail = etEmail.getText().toString().trim();
        phone = etPhone.getText().toString().trim();
//        loc = etLoc.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            displayToast("Please enter User Name.");
            return;
        }
        if (name.length() < 3 || name.length() > 30) {
            displayToast("User Name should contain 3 to 30 digits.");
            return;
        }
//        if (password.isEmpty()) {
//            displayToast("Please enter Password");
//            return;
//        }
//        if (password.length() < 6 || password.length() > 14) {
//            displayToast("Password should contain 6 to 14 digits.");
//            return;
//        }
//        if (conPassword.isEmpty()) {
//            displayToast("Please enter Confirm Password");
//            return;
//        }
//        if (conPassword.length() < 6 || conPassword.length() > 14) {
//            displayToast("Confirm Password should contain 6 to 14 digits.");
//            return;
//        }
//        if (!password.equals(conPassword)) {
//            displayToast("Password and Confirm Password does not match.");
//            return;
//        }
        if (full_name.isEmpty()) {
            displayToast("Please enter Full Name.");
            return;
        }
        if (full_name.length() < 3 || full_name.length() > 30) {
            displayToast("Full Name should contain 3 to 30 digits.");
            return;
        }
        if (TextUtils.isEmpty(mail)) {
            displayToast("Please enter Email address.");
            return;
        }

        if (!Pattern.matches(EMAIL_REGEX, mail)) {
            displayToast("Please enter valid Email address.");
            return;
        }
        if (TextUtils.isEmpty(phone)) {
            displayToast("Please enter Phone Number.");
            return;
        }
        if (phone.length() < 7 || phone.length() > 15) {
            displayToast("Phone number should contain 7 to 15 digits.");
            return;
        }
//        if (tvDob.getText().toString().trim().equals(getResources().getString(R.string.Date_of_birth))) {
//            displayToast("Select Date Of Birth");
//            return;
//        }
//        if (date != null
//                && !dobdateValidate(date)) {
//            displayToast("Minimum 12 year age is allowed");
//            return;
//        }
//        if (getGenderItem() == null) {
//            displayToast("Select Gender");
//            return;
//        }

        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
/*
            restClient.callback(this).signup(name, full_name, password, mail, DOB, getGenderItem(), phone, loc, getContext());
*/

            restClient.callback(this).newSignup(name, full_name, mail, phone, getContext());
        } else {
            displayToast(Constants.No_Internet);
        }

    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            this.date = date;

            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            DOB = format.format(date);
            displayLog(TAG, "DOB", "" + DOB);
            tvDob.setText(DOB);
        }
    }

    private String getGenderItem() {
        int position = spGender.getSelectedItemPosition();
        if (position == 0) return null;

        String[] array = getContext().getResources().getStringArray(R.array.gender_array);
        for (int i = 0; i < array.length; i++) {
            if (position == i) {
                return array[i];
            }
        }
        return null;
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_SIGNUP) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Signup signup = gson.fromJson(s, Signup.class);
                    if (signup.getStatus() != 0) {
                        displayToast(signup.getMessage());
                        getActivity().onBackPressed();
                    } else {
                        displayErrorDialog("Error", signup.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    public static boolean dobdateValidate(Date date) {


        try {
            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.YEAR, -12);
            if (date.before(c2.getTime())) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            //  displayLog(TAG, "dobdateValidate: " );
        }
        return false;
    }
}
