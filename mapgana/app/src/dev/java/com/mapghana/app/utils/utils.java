package com.mapghana.app.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

public class utils {

    public static String formateddate(String date) {

        String convertedDate = convertedDate(date);
        DateTime dateTime = DateTimeFormat.forPattern("dd/MM/yyyy").parseDateTime(convertedDate);
        DateTime today = new DateTime();
        DateTime yesterday = today.minusDays(1);
        DateTime twodaysago = today.minusDays(2);
        DateTime tomorrow= today.minusDays(-1);

        if (dateTime.toLocalDate().equals(today.toLocalDate())) {
            return "Today ";
        } else if (dateTime.toLocalDate().equals(yesterday.toLocalDate())) {
            return "Yesterday ";
        } else if (dateTime.toLocalDate().equals(twodaysago.toLocalDate())) {
            return convertedDate;
        } else if (dateTime.toLocalDate().equals(tomorrow.toLocalDate())) {
            return "Tomorrow ";
        } else {
            return convertedDate;
        }
    }

    public static String convertedDate(String date) {

        String[] str = date.split("-");
        return str[2]+"/"+str[1]+"/"+str[0];
    }

    public static String convertedDateMMDDYYYY(String date) {

        String[] str = date.split("-");
        return str[1]+"/"+str[2]+"/"+str[0];
    }

}
