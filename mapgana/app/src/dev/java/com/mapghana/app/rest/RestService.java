package com.mapghana.app.rest;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mapghana.BuildConfig;
import com.mapghana.app.retrofit.Rest;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import com.facebook.stetho.okhttp3.StethoInterceptor;

/**
 * Created by Vishvendra.Singh@Brsoftech on 20/6/16.
 */
public class RestService {
    private static final String TAG = "RestService";

    private static long CONNECTION_TIMEOUT = 1200;
    //   private static String API_BASE_URL = "http://192.168.1.253:82/liquor_finder1/api/users/";
    private static String API_BASE_URL = BuildConfig.BASE_URL;
    public static String BASEURL_GEOCODER="https://maps.googleapis.com/maps/api/";


    public static Rest getService() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient client = getOkHttpClient();

        Rest rest = new Retrofit.Builder().baseUrl(API_BASE_URL).client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()
                .create(Rest.class);
        return rest;
    }

    public static Rest getServiceGeoCoder() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient client = getOkHttpClient();

        Rest rest = new Retrofit.Builder().baseUrl(BASEURL_GEOCODER).client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()
                .create(Rest.class);
        return rest;
    }

    private static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
        //okClientBuilder.addInterceptor(headerAuthorizationInterceptor);
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.e(TAG, message);
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okClientBuilder.addInterceptor(httpLoggingInterceptor);
      //  okClientBuilder.addNetworkInterceptor(new StethoInterceptor());
//        final @Nullable File baseDir = context.getCacheDir();
//        if (baseDir != null) {
//            final File cacheDir = new File(baseDir, “HttpResponseCache”);
//            okClientBuilder.cache(new Cache(cacheDir, HTTP_RESPONSE_DISK_CACHE_MAX_SIZE));
//        }
        okClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okClientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okClientBuilder.writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        return okClientBuilder.build();
    }
}
