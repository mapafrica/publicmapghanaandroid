package com.mapghana.app.ui.activity.ads;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.spf.TimeManager;

public class AdsActivity extends AppBaseActivity {

    private VideoView videoView;
    private ImageView iv_close;
    private int currentPosition;
    private MediaController mediaController;
    private final long SIX_MINUTE = 1000 * 60 * 6;
    private final long SIX_SECOND = 1000 * 6;
    private static final String TAG = "AdsActivity";
    private String ads_path;
    private String video_path;
    private RelativeLayout rl_view;
    private ProgressBar progressBar;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            handler.removeCallbacks(runnable);
            iv_close.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_ads;
    }

    @Override
    public void initializeComponent() {
        videoView = findViewById(R.id.video_view);
        iv_close = findViewById(R.id.iv_close);
        rl_view = findViewById(R.id.rl_view);
        progressBar = findViewById(R.id.progressBar);
        iv_close.setVisibility(View.GONE);

        iv_close.setOnClickListener(this);
        rl_view.setOnClickListener(this);
        if (mediaController == null) {
            mediaController = new MediaController(this);
            videoView.setMediaController(mediaController);
            mediaController.setAnchorView(videoView);
        }
        mediaController.setVisibility(View.GONE);
        getAds();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                finish();
                break;
            case R.id.rl_view:
                if (!ads_path.isEmpty()) {
                    openWebsite();
                }
                break;
        }

    }

    private void openWebsite() {
        try {
            String url = ads_path;
            if (url.length() != 0) {
                String domain = "http://";
                if (!url.startsWith(domain) || !url.startsWith("https://")) {
                    url = domain + url;
                }
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                startActivity(Intent.createChooser(webIntent, "Choose Browser:"));
            }
        } catch (ActivityNotFoundException e) {
            displayErrorDialog("", "No apps found to open this link in your phone");
        }
    }

    private void getAds() {
         video_path = getIntent().getStringExtra("video_path");
        ads_path = getIntent().getStringExtra("ads_path");
        startVideo(video_path);
    }

    private void startVideo(String path) {
        Uri uri = Uri.parse(path);
        videoView.setVideoURI(uri);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                handler.postDelayed(runnable, SIX_SECOND);
                videoView.start();
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                finish();
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                displayToast("Something");
                finish();
                return true;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimeManager.saveLastTime(AdsActivity.this, System.currentTimeMillis() + SIX_MINUTE);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView != null) {
            videoView.pause();
            currentPosition = videoView.getCurrentPosition();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (videoView != null) {
            videoView.seekTo(currentPosition);
            videoView.start();
        }
    }

    @Override
    public void setExploreVisibility(boolean visibility) {

    }
}
