package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.dialog.PhoneListDialog;
import com.mapghana.app.dialog.ShowStatusDialog;
import com.mapghana.app.model.GalleryPojo;
import com.mapghana.app.model.GetDetails;
import com.mapghana.app.model.OpeningHour;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.DialPhoneAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.FeaturesAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.ReviewsAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.SlidingImageAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.navigation.NavigationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.writereview.WriteReviewFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.SearchFragment;
import com.mapghana.app.ui.activity.fullscreen.FullScreenActivity;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;
import com.rd.PageIndicatorView;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostDetailFragment extends AppBaseFragment
        implements AppBaseMapBox.OnMapLoaded, LocationServiceListner,
        SlidingImageAdapter.VideoClickListener {
    private RecyclerView rvFeatures, rvReviews;
    private TypefaceTextView tvName, tvEmail, tvReviews,
            tvWebsite, tvRating, tv_note, tv_type, tvTotalRating, tvCategoryName, tvSubCategoryName, tvPhone, tv_tag, tvTwitter;
    private TypefaceTextView tvMF, tvSat, tvSun, tv_twitter;
    private ImageView imgProfile, imgStatus, iv_profile_pic;
    private RatingBar rating_bar;
    private LinearLayout llOpeningHour, ll_website, ll_feature, ll_twitter, ll_type;
    LinearLayout ll_email;
    private RelativeLayout rlMF, rlSat, rlSunday;
    private ViewPager viewPager;
    private PageIndicatorView pageIndicatorView;
    private NestedScrollView scroll_view;

    private static final String TAG = "PostDetailFragment";
    private String category = "";
    private String name;
    private String address;
    private int TYPE;
    private int resource;
    private int zoomLevel;
    private double latitude, longitude;
    private LatLng latLng;

    private List<GalleryPojo> postGalleryList;
    private List<GetDetails.DataBean.PostReviewBean> userImageList = new ArrayList<>();
    private AppBaseMapBox.CustomSupportMapFragment mapFragment;
    private MapboxMap mapboxMap;
    private Marker marker;
    private String profileImagePath;
    private ReviewsAdapter reviewsAdapter;
    private NavigationFragment navigationFragment;


    private int post_id;


    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_category_details;
    }

    @Override
    public void initializeComponent() {
        init();
        zoomLevel = 13;
        rvFeatures = (RecyclerView) getView().findViewById(R.id.rvFeatures);
        rvReviews = (RecyclerView) getView().findViewById(R.id.rvReviews);
        tvName = (TypefaceTextView) getView().findViewById(R.id.tvName);
        tvEmail = (TypefaceTextView) getView().findViewById(R.id.tvEmail);
        tvWebsite = (TypefaceTextView) getView().findViewById(R.id.tvWebsite);
        tvRating = (TypefaceTextView) getView().findViewById(R.id.tvRating);
        tvTwitter = getView().findViewById(R.id.tvTwitter);
        tvReviews = (TypefaceTextView) getView().findViewById(R.id.tvReviews);
        tv_note = (TypefaceTextView) getView().findViewById(R.id.tv_note);
        tv_type = (TypefaceTextView) getView().findViewById(R.id.tv_type);
        tvTotalRating = (TypefaceTextView) getView().findViewById(R.id.tvTotalRating);
        tvCategoryName = (TypefaceTextView) getView().findViewById(R.id.tvCategoryName);
        tvSubCategoryName = (TypefaceTextView) getView().findViewById(R.id.tvSubCategoryName);
        tvPhone = (TypefaceTextView) getView().findViewById(R.id.tvPhone);
        tv_tag = (TypefaceTextView) getView().findViewById(R.id.tv_tag);
        tvMF = (TypefaceTextView) getView().findViewById(R.id.tvMF);
        tvSat = (TypefaceTextView) getView().findViewById(R.id.tvSat);
        tvSun = (TypefaceTextView) getView().findViewById(R.id.tvSun);
        imgProfile = (ImageView) getView().findViewById(R.id.imgProfile);
        imgStatus = (ImageView) getView().findViewById(R.id.imgStatus);
        iv_profile_pic = (ImageView) getView().findViewById(R.id.iv_profile_pic);
        ll_feature = (LinearLayout) getView().findViewById(R.id.ll_feature);
        rating_bar = (RatingBar) getView().findViewById(R.id.rating_bar);
        llOpeningHour = (LinearLayout) getView().findViewById(R.id.llOpeningHour);
        ll_type = (LinearLayout) getView().findViewById(R.id.ll_type);
        rlMF = (RelativeLayout) getView().findViewById(R.id.rlMF);
        rlSat = (RelativeLayout) getView().findViewById(R.id.rlSat);
        rlSunday = (RelativeLayout) getView().findViewById(R.id.rlSunday);
        viewPager = (ViewPager) getView().findViewById(R.id.view_pager);
        pageIndicatorView = (PageIndicatorView) getView().findViewById(R.id.pageIndicatorView);
        scroll_view = (NestedScrollView) getView().findViewById(R.id.scroll_view);
        ImageButton imgMyLoc = (ImageButton) getView().findViewById(R.id.imgMyLoc);
        TypefaceTextView tv_share_loc = (TypefaceTextView) getView().findViewById(R.id.tv_share_loc);
        TypefaceTextView tv_navigation = (TypefaceTextView) getView().findViewById(R.id.tv_navigation);
        TypefaceTextView tvWriteReview = (TypefaceTextView) getView().findViewById(R.id.tvWriteReview);
        LinearLayout ll_phone = (LinearLayout) getView().findViewById(R.id.ll_phone);

        postGalleryList = new ArrayList<>();
        navigationFragment = new NavigationFragment();

        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        rvFeatures.setLayoutManager(manager);
        rvReviews.setLayoutManager(new LinearLayoutManager(getContext()));

        imgMyLoc.setOnClickListener(this);
        tvWriteReview.setOnClickListener(this);
        tvPhone.setOnClickListener(this);
        tvEmail.setOnClickListener(this);
        tv_share_loc.setOnClickListener(this);
        tv_navigation.setOnClickListener(this);
        imgStatus.setOnClickListener(this);
        tvTwitter.setOnClickListener(this);
        tvWebsite.setOnClickListener(this);
        iv_profile_pic.setOnClickListener(this);
        ll_type.setOnClickListener(this);

        iv_profile_pic.setVisibility(View.GONE);

        initMap();
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    private void initMap() {
        if (getActivity() == null) return;
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.clearMarker();
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnMarkerClickListener(null);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnInfoWindowClickListener(null);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnCameraIdleListeners(null);
        mapFragment = (AppBaseMapBox.CustomSupportMapFragment) ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvWriteReview:
                WriteReviewFragment fragment = new WriteReviewFragment();
                fragment.setPost_id(post_id);
                ((AppBaseActivity) getActivity()).changeFragment(fragment, true, false, 0,
                        R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                        true);
                break;
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
            case R.id.ll_phone:
                onCall();
                break;
            case R.id.tvEmail:
                onSendMail();
                break;
            case R.id.tvWebsite:
                openWebsite();
                break;
            case R.id.tv_share_loc:
                shareLocation();
                break;
//            case R.id.tvTw:
//                openTwitter();
//                break;
            case R.id.imgStatus:
                showStatusSheet();
                break;
            case R.id.iv_profile_pic:
                showFullProfilePic();
                break;
            case R.id.tv_navigation:
                if (navigationFragment != null)
                    changeFragment(navigationFragment);
                break;
            case R.id.ll_type:
                goToSearch();
                break;
        }
    }

    private void goToSearch() {
        String s = tv_type.getText().toString().trim();
        if (s != null && s.length() > 0) {
            SearchFragment fragment = new SearchFragment();
            fragment.setType(s);
            /*Bundle bundle = new Bundle();
            bundle.putString("type", s);
            fragment.setArguments(bundle);*/
            ((DashboardActivity) getActivity()).changeFragment(fragment, false, true,
                    1, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                    true);
        }
    }

    private void showFullProfilePic() {


        List<GalleryPojo> list = new ArrayList<>();
        GalleryPojo galleryPojo = new GalleryPojo();
        galleryPojo.setId(1);
        galleryPojo.setImage(profileImagePath);
        list.add(galleryPojo);
        Bundle bundle = new Bundle();
        bundle.putInt("position", 0);
        bundle.putString("gallery", new Gson().toJson(list));
        //bundle.putParcelableArrayList("gallery", (ArrayList<? extends Parcelable>) list);
        changeActivity(bundle);
    }

    private void changeFragment(Fragment fragment) {
        ((DashboardActivity) getActivity()).changeFragment(fragment, true, false,
                0, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                true);
    }

    private void shareLocation() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "MapGhana");
            String sAux = "\nLet me recommend you this application\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.mapghana \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            displayErrorDialog("", e.getMessage());
        }
    }

    private void openWebsite() {
        try {
            String url = tvWebsite.getText().toString().trim();
            if (url.length() != 0) {
                String domain = "http://";
                if (!url.startsWith(domain) && !url.startsWith("https://")) {
                    url = domain + url;
                }
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                startActivity(Intent.createChooser(webIntent, "Choose Browser:"));
            }
        } catch (ActivityNotFoundException e) {
            displayErrorDialog("", "No apps found to open this link in your phone");
        }
    }

    private void openTwitter() {
        String twitter_name = tv_twitter.getText().toString().trim();

        //  String twitter_name="SrBachchan";

        if (twitter_name.startsWith("@")) {
            twitter_name = twitter_name.substring(1);
        }
        try {
            // checking for twitter app-> if yes than ok else throw exception
            getContext().getPackageManager().getPermissionInfo("com.twitter.android", 0);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setClassName("com.twitter.android", "com.twitter.android.ProfileActivity");
            intent.putExtra("screen_name", twitter_name);
            this.startActivity(intent);
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + twitter_name)));
        }
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf(TAG, "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }


    private void onSendMail() {
        if (tvEmail.getText().toString().trim().length() != 0) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{tvEmail.getText().toString().trim()});
            emailIntent.setType("message/rfc822");
            startActivity(Intent.createChooser(emailIntent, "Choose an email client:"));
        }

    }

    private void onCall() {
        String phone = tvPhone.getText().toString().trim();
        if (phone.length() != 0) {
            final String[] phone_array = phone.split(",");

            if (phone_array.length == 1) {
                startDialing(phone_array[0]);
            } else {
                final PhoneListDialog dialog = new PhoneListDialog(getContext(), phone_array);
                dialog.setDialPhoneListener(new DialPhoneAdapter.DialPhoneListener() {
                    @Override
                    public void dialPhoneListener(int position) {
                        dialog.dismiss();
                        startDialing(phone_array[position]);

                    }
                });
                dialog.show();
            }

        }
    }

    private void startDialing(String phone) {
        if (phone == null || phone.length() == 0) return;

        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phone));
        startActivity(callIntent);
    }

    private void showStatusSheet() {
        ShowStatusDialog dialog = new ShowStatusDialog(getContext());
        dialog.setCancelable(true);
        dialog.show();
    }

    private void setMarkersOnMapAgain() {
        if (getActivity() == null || !isVisible()) return;
        zoomLevel = 17;
        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        LatLng latLng_currentPos = new LatLng(lat, log);
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(log);
        moveMarker(location);
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(latLng_currentPos, zoomLevel);
    }

    private void setMarkerFirstTime(LatLng latLng, String title, String snippet) {
        if (getActivity() == null || !isVisible()) return;
        String status = "";
        if (TYPE == 1) {
            status = Constants.verified;
        } else if (TYPE == 2) {
            status = Constants.unverified;
        } else {
            //(type==3)
            status = Constants.anonymous;
        }

        List<Marker> markers = mapboxMap.getMarkers();
        mapboxMap.clear();
        final List<LatLng> boundList = new ArrayList<>();
        ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), resource, latLng, title, "Address: " + snippet + "\nStatus: " + status);
        boundList.add(latLng);

        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        LatLng currentPos = new LatLng(lat, log);
        marker = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.my_location, currentPos, "You", "");
        boundList.add(currentPos);
        ((DashboardActivity) getActivity()).appBaseMapBox.boundMap(boundList);

        List<Marker> markers2 = mapboxMap.getMarkers();


    }

    private void getDetails() {
        /*if (getArguments() == null) {
            return;
        }

        int id = getArguments().getInt(Constants.post_id);*/
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).getDetailsOfOnePost(String.valueOf(post_id));
            //restClient.callback(this).getDetailsOfOnePost("133");
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_Details_1) {
                try {
                    String s = response.body().string();
                    updateUI(s);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void initializePostGalley(List<GetDetails.DataBean.PostGalleryBean> postGallery) {
        GalleryPojo galleryPojo;
        for (int i = 0; i < postGallery.size(); i++) {
            galleryPojo = new GalleryPojo();
            galleryPojo.setId(postGallery.get(i).getId());
            galleryPojo.setImage(postGallery.get(i).getImage());
            postGalleryList.add(galleryPojo);
        }
        viewPager.setAdapter(new SlidingImageAdapter(getContext(), postGalleryList, this, this));
        pageIndicatorView.setViewPager(viewPager);
        pageIndicatorView.setCount(postGallery.size());
        pageIndicatorView.setSelection(0);
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        this.mapboxMap.clear();
        List<Marker> markers = mapboxMap.getMarkers();


        mapFragment.setmListener(new AppBaseMapBox.CustomSupportMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                scroll_view.requestDisallowInterceptTouchEvent(true);
            }
        });
        getDetails();
    }

    @Override
    public void onAdapterClickListener(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        // bundle.putParcelableArrayList("gallery", (ArrayList<? extends Parcelable>) postGalleryList);

        bundle.putString("gallery", new Gson().toJson(postGalleryList));
        changeActivity(bundle);
    }

    @Override
    public void onVideoClickListener(int position, int current_position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putInt("current_position", current_position);
        bundle.putString("gallery", new Gson().toJson(postGalleryList));

        //bundle.putParcelableArrayList("gallery", (ArrayList<? extends Parcelable>) postGalleryList);
        changeActivity(bundle);
    }

    @Override
    public void onAdapterClickListener(int position, String action) {
        if (action.equalsIgnoreCase("fromSingleImage")) {

            String image = userImageList.get(position).getUser().getImage();
            if (image != null && !image.equals("")) {
                List<GalleryPojo> list = new ArrayList<>();
                GalleryPojo galleryPojo = new GalleryPojo();
                galleryPojo.setId(userImageList.get(position).getId());
                galleryPojo.setImage(image);
                list.add(galleryPojo);

                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                // bundle.putParcelableArrayList("gallery", (ArrayList<? extends Parcelable>) list);
                bundle.putString("gallery", new Gson().toJson(list));

                changeActivity(bundle);
            } else {
                displayToast("There is no profile image");
            }
        }
    }

    private void changeActivity(Bundle bundle) {
        Intent intent = new Intent(getContext(), FullScreenActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    @Override
    public void userLocationChange(Location location) {
        //moveMarker(location);
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    private void moveMarker(Location location) {
        if (location == null || mapboxMap == null) {
            return;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (marker != null) {
            marker.setPosition(latLng);
        }
    }


    private void updateUI(String s) {

        Gson gson = new Gson();
        GetDetails data = gson.fromJson(s, GetDetails.class);
        if (data.getStatus() != 0) {
            navigationFragment.setGetDetails(data);

            if (data.getData().getPost_for().equalsIgnoreCase("individuals")) {
                profileImagePath = data.getData().getImage();
                if (profileImagePath != null && profileImagePath.trim().length() > 0) {
                    iv_profile_pic.setVisibility(View.VISIBLE);

                    try {

                        Glide.with(getContext()).load(profileImagePath).override(250, 250)
                                .placeholder(R.drawable.profile).dontAnimate().into(iv_profile_pic);
                    } catch (IllegalArgumentException e) {

                    }
                } else {
                    iv_profile_pic.setVisibility(View.GONE);
                }
            } else {
                iv_profile_pic.setVisibility(View.GONE);
            }
            name = data.getData().getName();
            address = data.getData().getAddress();
            getNavHandler().setNavTitle(name);
            tvName.setText(name);
            String mail = data.getData().getEmail();
            if (mail != null && !mail.trim().isEmpty()) {
                tvEmail.setVisibility(View.VISIBLE);
                tvEmail.setText(mail);
            } else {
                tvEmail.setVisibility(View.GONE);
            }

            String web = data.getData().getWebsite();
            if (web != null && web.trim().length() != 0) {
                tvWebsite.setText(web);
                ll_website.setVisibility(View.VISIBLE);
            }

            String twitter = data.getData().getTwitter();
            if (twitter != null && twitter.trim().length() > 0) {
                tv_twitter.setText(twitter);
                ll_twitter.setVisibility(View.VISIBLE);
            }

            double rate = data.getData().getAvg_rating();

            if (TextUtils.isEmpty(String.valueOf(rate)) || rate == 0)
                tvRating.setVisibility(View.GONE);
            else
                tvRating.setVisibility(View.VISIBLE);
            tvRating.setText(new DecimalFormat("##.#").format(rate));
            rating_bar.setRating((float) rate);
            tv_note.setText(data.getData().getNotes());
            String type = data.getData().getType();
            if (type != null && type.length() > 0) {
                ll_type.setVisibility(View.VISIBLE);
                tv_type.setText(type);
            }


            tv_tag.setText(data.getData().getTags());
            if (data.getData() != null && data.getData().getPhone() != null) {
                List<String> phoneList = data.getData().getPhone();
                String phone = "";
                for (int i = 0; i < phoneList.size(); i++) {
                    phone = phone + phoneList.get(i);
                    if (i != (phoneList.size() - 1)) {
                        phone = phone + ", ";
                    }
                }
                tvPhone.setText(phone);
            }
            String available_category = data.getData().getCategory().getName();
            tvCategoryName.setText(available_category);
            GetDetails.DataBean.SubcategoryBean subCategoryBean = data.getData().getSubcategory();
            if (subCategoryBean != null) {
                if (subCategoryBean.getName().trim().length() > 0) {
                    tvSubCategoryName.setVisibility(View.VISIBLE);
                    tvSubCategoryName.setText("(" + subCategoryBean.getName() + ")");
                    available_category = subCategoryBean.getName();
                }
            }
            tvTotalRating.setText("(" + data.getData().getTotal_review() + ")");
            List<GetDetails.DataBean.PostGalleryBean> postGallery = data.getData().getPostGallery();
            if (postGallery != null && postGallery.size() > 0 && postGallery.get(0).getImage() != null &&
                    !postGallery.get(0).getImage().equals("")) {
                initializePostGalley(postGallery);
            } else {
                if (profileImagePath != null && !profileImagePath.equals("")) {
                    GetDetails.DataBean.PostGalleryBean bean = new GetDetails.DataBean.PostGalleryBean();
                    bean.setImage(profileImagePath);
                    List<GetDetails.DataBean.PostGalleryBean> list = new ArrayList<>();
                    list.add(bean);
                    initializePostGalley(list);
                } else {
                    imgProfile.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.GONE);
                    //imgProfile.setImageResource(R.mipmap.d);
                }
            }
            // set opening hour
            String openHour = data.getData().getOpening_hours();
            if (openHour != null) {
                llOpeningHour.setVisibility(View.VISIBLE);
                OpeningHour openingHour = gson.fromJson(openHour, OpeningHour.class);
                if (openingHour.getMondayfriday() != null) {
                    rlMF.setVisibility(View.VISIBLE);
                    tvMF.setText(openingHour.getMondayfriday().getStart() + " - " +
                            openingHour.getMondayfriday().getEnd());
                }
                if (openingHour.getSaturday() != null) {
                    rlSat.setVisibility(View.VISIBLE);
                    tvSat.setText(openingHour.getSaturday().getStart() + " - " +
                            openingHour.getSaturday().getEnd());
                }
                if (openingHour.getSunday() != null) {
                    rlSunday.setVisibility(View.VISIBLE);
                    tvSun.setText(openingHour.getSunday().getStart() + " - " +
                            openingHour.getSunday().getEnd());
                }
            } else {
                llOpeningHour.setVisibility(View.GONE);
            }

            List<String> features = data.getData().getFeatures();
            if (features != null && features.size() > 0) {
                ll_feature.setVisibility(View.VISIBLE);
                FeaturesAdapter adapter = new FeaturesAdapter(features);
                rvFeatures.setAdapter(adapter);
            } else {
                ll_feature.setVisibility(View.GONE);
            }

            List<GetDetails.DataBean.PostReviewBean> reviewsList = new ArrayList<>();
            reviewsList.addAll(data.getData().getPostReview());
            if (reviewsList != null) {
                tvReviews.setVisibility(View.VISIBLE);
                userImageList.addAll(reviewsList);
                reviewsAdapter = new ReviewsAdapter(this, reviewsList);
                rvReviews.setAdapter(reviewsAdapter);
            } else {
                tvReviews.setVisibility(View.GONE);
            }

            latitude = data.getData().getLat();
            longitude = data.getData().getLog();

            try {
                latLng = new LatLng(latitude, longitude);
            } catch (IllegalArgumentException e) {

            }


            String status = data.getData().getStatus().trim();
            String postFor = data.getData().getPost_for();
            if (postFor != null && postFor.equalsIgnoreCase(Constants.individuals)) {
                String gender = "";
                gender = data.getData().getGender().trim();
                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_gril;
                        // imgStatus.setImageResource(R.mipmap.right_green);
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resource = R.mipmap.individual_red_girl;
                        // imgStatus.setImageResource(R.mipmap.right_red);
                        TYPE = 2;
                    } else {
                        resource = R.mipmap.indi_girl_gray;
                        TYPE = 3;
                        // imgStatus.setImageResource(R.mipmap.right_gray);

                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_man;
                        //imgStatus.setImageResource(R.mipmap.right_green);
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        //  imgStatus.setImageResource(R.mipmap.right_red);
                        TYPE = 2;
                        resource = R.mipmap.individual_red_man;
                    } else {
                        resource = R.mipmap.individual_gray_man;
                        TYPE = 3;
                        // imgStatus.setImageResource(R.mipmap.right_gray);
                    }
                }

                category = "Occupation: " + available_category;
            } else if (postFor != null && postFor.equalsIgnoreCase(Constants.organizations)) {
                if (status != null && status.equalsIgnoreCase(Constants.verified)) {
                    resource = R.mipmap.location_gree_org;
                    //imgStatus.setImageResource(R.mipmap.right_green);
                    TYPE = 1;

                } else if (status != null && status.equalsIgnoreCase(Constants.unverified)) {
                    resource = R.mipmap.location_red_org;
                    //  imgStatus.setImageResource(R.mipmap.right_red);
                    TYPE = 2;

                } else {
                    //(type==3)
                    resource = R.mipmap.location_gry_org;
                    //  imgStatus.setImageResource(R.mipmap.right_gray);
                    TYPE = 3;

                }
                //  category = "Category: " + available_category;

                category = "Category: ";
                GetDetails.DataBean.CategoryBean categoryObj = data.getData().getCategory();
                if (categoryObj != null) {
                    category = category + categoryObj.getName();
                }
                GetDetails.DataBean.SubcategoryBean subcategoryObj = data.getData().getSubcategory();
                if (subcategoryObj != null) {
                    category = category + " (" + subcategoryObj.getName() + ")";
                }
            }
            if (latLng != null) {
                setMarkerFirstTime(latLng, name, category + "\n" + address);
            } else {
                // displayToast("Invalid latitude and longitude");
            }
        } else {
            displayErrorDialog("Error", data.getError());
        }
    }
}
