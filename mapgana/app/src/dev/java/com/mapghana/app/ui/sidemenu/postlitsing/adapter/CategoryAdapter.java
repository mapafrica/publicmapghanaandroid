package com.mapghana.app.ui.sidemenu.postlitsing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.Category;
import com.mapghana.customviews.TypefaceTextView;

import java.util.ArrayList;

/**
 * Created by ubuntu on 23/1/18.
 */

public class CategoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Category.DataBean> categoryList;

    private Category.DataBean selectedCategory;

    public Category.DataBean getDataBean() {
        return selectedCategory;
    }

    public void setDataBean(int position) {
        this.selectedCategory = categoryList.get(position);
        notifyDataSetChanged();
    }

    public CategoryAdapter(Context context, ArrayList<Category.DataBean> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        if (categoryList != null && categoryList.size() > 0) {
            return categoryList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_spinner_with_image, parent, false);
        }

        TypefaceTextView tvTitle = (TypefaceTextView) convertView.findViewById(R.id.tvTitle);
        tvTitle.setText(categoryList.get(position).getName());
        ImageView imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
        ImageView imgDown = (ImageView) convertView.findViewById(R.id.imgDown);

        if (selectedCategory != null) {
            if (categoryList.get(position).equals(selectedCategory)) {
                imgDown.setVisibility(View.VISIBLE);
            } else {
                imgDown.setVisibility(View.GONE);
            }
        } else {
            imgDown.setVisibility(View.GONE);
        }

        if (position == 0) {
            imgCategory.setVisibility(View.GONE);
        } else {
            imgCategory.setVisibility(View.VISIBLE);
            if ((categoryList.get(position).getImage() != null) && !(categoryList.get(position).getImage().equals(""))) {
                Glide.with(context).load(categoryList.get(position).getImage()).override(150, 150)

                        .placeholder(R.drawable.pleasewait)
                        .dontAnimate()
                        .into(imgCategory);
            } else {
                imgCategory.setImageResource(R.mipmap.no_category);
            }
        }

        return convertView;
    }


}