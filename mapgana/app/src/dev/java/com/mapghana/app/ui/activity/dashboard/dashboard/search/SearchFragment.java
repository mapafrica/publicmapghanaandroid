package com.mapghana.app.ui.activity.dashboard.dashboard.search;


import android.animation.Animator;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map.SingleItemOnMapFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.SearchListFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.adapter.ItemListOrgAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.app.utils.Constants;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends AppBaseFragment implements AdapterClickListener {

    private Animator animator;
    private static final String TAG = "SearchFragment";
    private LinearLayout myView;
    private RecyclerView recycler_view;
    private AutoCompleteTextView autoTv;
    private Handler handler;
    private Runnable runnable;
    private List<GlobeSearch.DataBean> list = new ArrayList();
    private String results = "";
    private int TYPE;
    private final int TIME_INTERVAL = 500;
    private ItemListOrgAdapter itemListOrgAdapter;
    private ProgressBar progressBar;
    private Call<ResponseBody> previousCall;

    private String type;

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (previousCall != null) {
                previousCall.cancel();
            }
            TYPE = 0;
            if (autoTv.getText().toString().trim().length() != 0 && autoTv.getText().toString().trim().length() >= 1) {
                waitForSometime();
            } else {
                clearList();
                notifyAdapter();
            }
        }
    };

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_search;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myView = getView().findViewById(R.id.myView);
        init();
        autoTv.postDelayed(new Runnable() {
            @Override
            public void run() {
                autoTv.addTextChangedListener(textWatcher);
            }
        }, 200);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        childFm = getChildFragmentManager();
        if (getView() == null) {
            setupFragmentViewByResource(inflater, container);
            initializeComponent();

        }
        return getView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        autoTv.removeTextChangedListener(textWatcher);
    }

    @Override
    public void initializeComponent() {

        getNavHandler().setNavigationToolbarVisibilty(false);
        ImageView imgSearch;
        ImageButton imgBack;
        imgSearch = (ImageView) getView().findViewById(R.id.imgSearch);
        imgBack = (ImageButton) getView().findViewById(R.id.imgBack);
        recycler_view = (RecyclerView) getView().findViewById(R.id.recycler_view);
        autoTv = (AutoCompleteTextView) getView().findViewById(R.id.autoTv);
        progressBar = getView().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));


        itemListOrgAdapter = new ItemListOrgAdapter(this, list, list);
        recycler_view.setAdapter(itemListOrgAdapter);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                doRequest();
            }
        };
        autoTv.setThreshold(1);
        if (type != null) {
            if (type != null && type.length() > 0) {
                autoTv.setText(type);
            }
        }


        imgSearch.setOnClickListener(this);
        imgBack.setOnClickListener(this);

    }

    private void doRequest() {
        if (getContext() == null) {
            return;
        }
        if (autoTv.getText() != null && autoTv.getText().toString().trim().length() != 0) {
            if (ConnectionDetector.isNetAvail(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RestClient restClient = new RestClient(getContext());
                previousCall = restClient.callback(this).search(autoTv.getText().toString().trim());
            } else {
                displayToast(Constants.No_Internet);
            }
        }
    }

    private void waitForSometime() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, TIME_INTERVAL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                hideKeyboard();
                getActivity().onBackPressed();
                break;
            case R.id.imgSearch:
                hideKeyboard();
                if (autoTv.getText().toString().trim().length() > 0) {
                    onSearch();
                }
                break;
        }
    }

    private void onSearch() {
        TYPE = 1;
        if (results.trim().length() > 0) {
            // clear old default filters
            DefaulFilterSpf.clearFilter(getContext());
            SearchListFragment itemFragment = new SearchListFragment();
            itemFragment.setSearch_keyword(autoTv.getText().toString().trim());
            itemFragment.setSearch_response(results);
            progressBar.setVisibility(View.INVISIBLE);

            ((AppBaseActivity) getActivity()).changeFragment(itemFragment, true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
            return;
        } else {
            handler.removeCallbacks(runnable);
            handler.post(runnable);
        }
    }

    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            myView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    myView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    // get the final radius for the clipping circle
                    float finalRadius = (float) Math.hypot(0, myView.getHeight());// hypot will calculat how much do you want to run animation
                    playReveal(myView.getWidth(), 0, 0, finalRadius);
                }
            });
        }
    }

    private void playReveal(int cx, int cy, float startRadius, float finalRadius) {
        // Android native animator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (myView == null || myView.getContext() == null) return;
            try {
                animator =
                        ViewAnimationUtils.createCircularReveal(myView, cx, cy, startRadius, finalRadius);
                animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animator.setDuration(500);
                animator.start();
            } catch (IllegalStateException e) {

            }
        }
    }

    @Override
    public void onAdapterClickListener(int position) {
        if (list != null && list.size() > 0) {
            SingleItemOnMapFragment detailsFragment = new SingleItemOnMapFragment();
            detailsFragment.setPost_id(position);
            ((AppBaseActivity) getActivity()).changeFragment(detailsFragment, true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        progressBar.setVisibility(View.INVISIBLE);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_SEarch) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GlobeSearch data = gson.fromJson(s, GlobeSearch.class);
                    if (data.getStatus() != 0) {
                        clearList();
                        list.addAll(data.getData());
                        if (list.size() > 0) {
                            results = s;
                            notifyAdapter();
                            if (TYPE == 1) {
                                onSearch();
                            }
                        } else {
                            displayToast("No result found...");
                        }

                    } else {
                        displayToast(data.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayToast(e.getMessage());
                }
            }
        } else {
            displayToast(response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        progressBar.setVisibility(View.INVISIBLE);
        // displayToast( error);
    }

    private void clearList() {

        if (list != null && list.size() > 0) {
            list.clear();
            results = "";
        }
    }

    private void notifyAdapter() {
        if (list != null && itemListOrgAdapter != null) {
            itemListOrgAdapter.notifyDataSetChanged();
        }
    }
}
