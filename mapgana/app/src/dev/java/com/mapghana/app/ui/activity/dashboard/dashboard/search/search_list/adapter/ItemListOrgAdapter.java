package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.utils.Constants;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.util.file_path_handler.BitmapHelper;
import com.mapghana.util.file_path_handler.FilePathHelper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class ItemListOrgAdapter extends BaseRecycleAdapter implements Filterable {

    AdapterClickListener adapterClickListener;

    private List<GlobeSearch.DataBean> list;
    private List<GlobeSearch.DataBean> filterList;
    private CustomFilter customFilter;
    private List<String> image_mime_List;
    private List<String> video_mime_List;

    private static final String TAG = "ItemListOrgAdapter";

    public ItemListOrgAdapter(AdapterClickListener adapterClickListener,
                              List<GlobeSearch.DataBean> list,
                              List<GlobeSearch.DataBean> filterList) {
        this.adapterClickListener = adapterClickListener;
        this.list = list;
        this.filterList = filterList;
        image_mime_List = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        image_mime_List.addAll(Constants.getImageMimeList());
        video_mime_List.addAll(Constants.getVideoMimeList());
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_list_item;
    }

    @Override
    protected int getItemSize() {
        if (list != null && list.size() > 0) {
            return list.size();
        }
        return 0;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    @Override
    public Filter getFilter() {
        if (customFilter == null) {
            customFilter = new CustomFilter();
        }
        return customFilter;
    }

    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private TypefaceTextView tvName, tvRating, tvAddress, tvTotalReview, tvCategoryName;
        private LinearLayout llView;
        private ImageView imgSCat;
        private RatingBar rating_bar;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = (TypefaceTextView) itemView.findViewById(R.id.tvName);
            tvRating = (TypefaceTextView) itemView.findViewById(R.id.tvRating);
            tvAddress = (TypefaceTextView) itemView.findViewById(R.id.tvAddress);
            tvTotalReview = (TypefaceTextView) itemView.findViewById(R.id.tvTotalReview);
            tvCategoryName = (TypefaceTextView) itemView.findViewById(R.id.tvCategoryName);
            llView = (LinearLayout) itemView.findViewById(R.id.llView);
            imgSCat = (ImageView) itemView.findViewById(R.id.imgSCat);
            rating_bar = (RatingBar) itemView.findViewById(R.id.rating_bar);
        }

        @Override
        public void setData(int position) {
            try {
                llView.setOnClickListener(this);
                llView.setTag(position);
                tvName.setText(list.get(position).getName());

                tvRating.setText(new DecimalFormat("##.#").format(list.get(position).getAvg_rating()));

                tvTotalReview.setText("(" + String.valueOf(list.get(position).getTotal_review()) + ") - ");
                rating_bar.setRating((float) list.get(position).getAvg_rating());
                tvCategoryName.setText(list.get(position).getCategory().getName());
                tvAddress.setText(list.get(position).getAddress());

                String imagePath = list.get(position).getImage();
                GlobeSearch.DataBean dataBean = list.get(position);
                if (dataBean != null) {
                    List<GlobeSearch.DataBean.PostGalleryBean> postGallery = list.get(position).getPostGallery();

                    if (dataBean.getPost_for().equalsIgnoreCase(Constants.individuals)) {
                        if (imagePath != null && imagePath.length() != 0) {
                            Glide.with(getContext()).load(imagePath)
                                    .override(250, 250)
                                    .placeholder(R.drawable.pleasewait)
                                    .dontAnimate()
                                    .into(imgSCat);
                        } else if (postGallery != null) {
                            if (postGallery.size() > 0) {
                                String mediaPath = postGallery.get(0).getImage();
                                if (mediaPath != null && mediaPath.length() > 0) {
                                    FilePathHelper filePathHelper = new FilePathHelper();
                                    String mimeType = filePathHelper.getMimeType(imagePath);
                                    if (image_mime_List.contains(mimeType)) {
                                        Glide.with(getContext()).load(mediaPath).override(250, 250)
                                                .placeholder(R.drawable.pleasewait)
                                                .dontAnimate()
                                                .into(imgSCat);
                                    } else if (video_mime_List.contains(mimeType)) {
                                        BitmapHelper bitmapHelper = new BitmapHelper();
                                        Bitmap bitmap = bitmapHelper.getVideoBitmap(mediaPath);
                                        imgSCat.setImageBitmap(bitmap);
                                    }
                                }
                            }
                        }
                    }

                    if (dataBean.getPost_for().equalsIgnoreCase(Constants.organizations)) {

                        if (postGallery != null && postGallery.size() > 0) {
                            String mediaPath = postGallery.get(0).getImage();
                            if (mediaPath != null && mediaPath.length() > 0) {
                                FilePathHelper filePathHelper = new FilePathHelper();
                                String mimeType = filePathHelper.getMimeType(imagePath);
                                if (image_mime_List.contains(mimeType)) {
                                    Glide.with(getContext()).load(mediaPath).override(250, 250)

                                            .placeholder(R.drawable.pleasewait)
                                            .dontAnimate()
                                            .into(imgSCat);
                                } else if (video_mime_List.contains(mimeType)) {
                                    BitmapHelper bitmapHelper = new BitmapHelper();
                                    Bitmap bitmap = bitmapHelper.getVideoBitmap(mediaPath);
                                    imgSCat.setImageBitmap(bitmap);
                                }
                            }
                        }
                    }
                }

            }catch (IndexOutOfBoundsException e){

            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.llView:
                    displayLog(TAG, "onClick: " + (Integer) v.getTag());
                    displayLog(TAG, "onClick22: " + list.get((Integer) v.getTag()).getId());

                    adapterClickListener.onAdapterClickListener(list.get((Integer) v.getTag()).getId());
                    break;
            }
        }
    }

    class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<GlobeSearch.DataBean> filters = new ArrayList<GlobeSearch.DataBean>();
                constraint = constraint.toString().toUpperCase();
                for (GlobeSearch.DataBean dataBean : filterList) {
                    if (dataBean.getName().toUpperCase().contains(constraint)) {
                        filters.add(dataBean);
                    }
                }
                results.count = filters.size();
                results.values = filters;
                displayLog(TAG, "performFiltering: " + filters.size());

            } else {
                displayLog(TAG, "performFiltering2: " + filterList.size());
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // TODO Auto-generated method stub

            list = (ArrayList<GlobeSearch.DataBean>) results.values;
            displayLog(TAG, "publishResults: " + list.size());
            notifyDataSetChanged();
        }

    }
}
