package com.mapghana.app.ui.sidemenu.Event;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brsoftech.customtimepicker.TimePickerDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.android.ui.geocoder.GeocoderAutoCompleteView;
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.services.commons.models.Position;
import com.mapghana.R;
import com.mapghana.app.App;
import com.mapghana.app.adapter.AddEventGalleryAdapter;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.interfaces.EventMapListener;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.add_location.AddLocationFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.utils;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceCheckBox;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.MediaAddAdpterListener;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class AddEventFragment extends AppBaseFragment implements View.OnClickListener, MediaAddAdpterListener, CustomDatePickerDialog.OnDateListener, TimePickerDialog.TimePickerListner, GeocoderAutoCompleteView.OnFeatureListener, MapboxMap.OnInfoWindowClickListener,
        AppBaseMapBox.OnMapLoaded,
        LocationServiceListner, MapboxMap.OnCameraIdleListener {
    private static final String TAG = "AddEvent";

    private EditText etEventName, etEventLocation, etEventAbout, etEventHostName, etEventPhone, etEventEmail, etNoOfSeats, etTicketPrice, etBankName, etBranchName;
    private SwitchCompat eventRecurringSwitch, eventCoverageSwitch, eventPaidEventSwitch, eventMAPGHTicketHosting;
    private TypefaceTextView etEventStartTime, etEventEndTime, addEventButton, etEventDate, addRecurring,txt_post_channel;
    private RecyclerView rvPreviousMedia, rvCurrentMedia;
    private RelativeLayout flEventPoster;
    private LinearLayout llRecursiveView, llPaidEventSection, llPaymentChannelMode;
    private Spinner spinnerPaymentChannelMode;
    private SimpleSpinnerAdapter paymentChannelModeAdapter;
    private ImageView posterImage, posterIcon, dropPinView;
    private RadioGroup radioGroupEventCoverage, radioGroupEventSeatsRange;
    private TypefaceCheckBox checkBoxGateKeepingServices;
    private TextInputLayout textInputLayoutNoOfSeats;
    private List<String> previousMediaGallery, currentMediaGallery;
    private String imagePoster, title;
    private Marker marker;
    private AddEventGalleryAdapter previousGalleryAdapter, currentGalleryAdapter;
    private static final int REQUEST_CODE_CHOOSE_PREVIOUS_MULTIPLE = 887;
    private static final int REQUEST_CODE_CHOOSE_CURRENT_MULTIPLE = 888;
    private static final int REQUEST_CODE_CHOOSE_SINGLE = 889;
    private Date date;
    private int time_type;
    private TimePickerDialog timePickerDialog;
    private List<String> video_mime_List;
    private RestClient restClient;
    private CardView mapContainer;
    private List<EventInfoSection> eventSectionList;
    private String eventCoverage, address, eventCoverageOn, seats, paidEvent = "N", ticketHosting = "N", postChannelMethod, gateKeepingServices = "N", type = "";
    private GeocoderAutoCompleteView geocoderAutoCompleteView;
    private double latitude = 0;
    private double longitude = 0;
    private LatLng latLng;
    private Uri uriOneImg;

    public static AddEventFragment newInstance() {
        AddEventFragment fragment = new AddEventFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_event;
    }

    @Override
    public void initializeComponent() {
        initToolBar();
        initViewId(getView());
        initRecycleLayoutManager();
        initListeners();
        initSpinnerPaymentModeChannelMode();
        initAutoCompeteBox();
        initMap();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Add_events));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    private void initViewId(View view) {
        restClient = new RestClient(getContext());
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
        eventSectionList = new ArrayList<>();
        etEventName = view.findViewById(R.id.event_name);
        etEventDate = view.findViewById(R.id.event_date);
        etEventLocation = view.findViewById(R.id.event_location);
        etEventStartTime = view.findViewById(R.id.event_start_time);
        etEventEndTime = view.findViewById(R.id.event_end_time);
        etEventAbout = view.findViewById(R.id.event_about);
        etEventHostName = view.findViewById(R.id.event_host_name);
        etEventPhone = view.findViewById(R.id.event_host_phonenumber);
        etEventEmail = view.findViewById(R.id.event_host_email);
        etNoOfSeats = view.findViewById(R.id.event_no_of_seats);
        etTicketPrice = view.findViewById(R.id.event_ticket_price);
        etBankName = view.findViewById(R.id.event_payment_channel_mode_bank_name);
        etBranchName = view.findViewById(R.id.event_payment_channel_mode_bank_branch);
        posterImage = view.findViewById(R.id.event_poster);
        textInputLayoutNoOfSeats = view.findViewById(R.id.text_input_layout_event_no_of_seats);
        radioGroupEventCoverage = view.findViewById(R.id.radio_group_event_coverage);
        radioGroupEventSeatsRange = view.findViewById(R.id.radio_group_event_seats);
        posterIcon = view.findViewById(R.id.image_icon);
        eventRecurringSwitch = view.findViewById(R.id.event_recurring_switch);
        eventCoverageSwitch = view.findViewById(R.id.event_coverage_switch);
        eventPaidEventSwitch = view.findViewById(R.id.paid_event_switch);
        eventMAPGHTicketHosting = view.findViewById(R.id.mapgh_ticket_hosting);
        addRecurring = view.findViewById(R.id.add_multiple_event_recurring);
        mapContainer = view.findViewById(R.id.rl_map_container);
        checkBoxGateKeepingServices = view.findViewById(R.id.checkbox_gatekeeping_services);
        txt_post_channel = view.findViewById(R.id.txt_post_channel);
        spinnerPaymentChannelMode = view.findViewById(R.id.spinner_channel_mode);

        flEventPoster = view.findViewById(R.id.frame_poster);

        llRecursiveView = view.findViewById(R.id.ll_recursive_event_view);
        llPaidEventSection = view.findViewById(R.id.ll_paid_event_section);
        llPaymentChannelMode = view.findViewById(R.id.ll_event_payment_channel_mode);

        rvPreviousMedia = view.findViewById(R.id.rv_previous_media);
        rvCurrentMedia = view.findViewById(R.id.rv_current_media);

        addEventButton = view.findViewById(R.id.tv_add_event);
    }

    private void initListeners() {

        eventRecurringSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                type = "recurring";
                addRecurring.setVisibility(View.VISIBLE);
                addEventSectionView();
            } else {
                llRecursiveView.removeAllViews();
                addRecurring.setVisibility(View.GONE);
                type = "";
            }
        });
        eventCoverageSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                radioGroupEventCoverage.setVisibility(View.VISIBLE);
                eventCoverageOn = "Y";
            } else {
                radioGroupEventCoverage.setVisibility(View.GONE);
                eventCoverageOn = "N";
            }
        });

        radioGroupEventCoverage.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_photo) {
                eventCoverage = getIfEventCoverage(R.id.radio_photo);
            } else if (checkedId == R.id.radio_video) {
                eventCoverage = getIfEventCoverage(R.id.radio_video);
            } else if (checkedId == R.id.radio_photo_video) {
                eventCoverage = getIfEventCoverage(R.id.radio_photo_video);
            }
        });
        eventPaidEventSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                paidEvent = "Y";
                llPaidEventSection.setVisibility(View.VISIBLE);
            } else {
                paidEvent = "N";
                llPaidEventSection.setVisibility(View.GONE);
            }
        });

        radioGroupEventSeatsRange.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_event_limited_seats) {
                seats = "LIMITED";
                textInputLayoutNoOfSeats.setVisibility(View.VISIBLE);
            } else if (checkedId == R.id.radio_event_unlimited_seats) {
                seats = "UNLIMITED";
                textInputLayoutNoOfSeats.setVisibility(View.GONE);
            } else {
                textInputLayoutNoOfSeats.setVisibility(View.GONE);
            }
        });

        spinnerPaymentChannelMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (paymentChannelModeAdapter != null) {
                    paymentChannelModeAdapter.setSelGender(position);

                    if (paymentChannelModeAdapter.getSelGender().equalsIgnoreCase("Select payment channel mode")) {
                        postChannelMethod = "";
                    } else {
                        postChannelMethod = paymentChannelModeAdapter.getSelGender();
                    }
                    if (paymentChannelModeAdapter.getSelGender().equalsIgnoreCase("Any Bank")) {
                        llPaymentChannelMode.setVisibility(View.VISIBLE);
                    } else {
                        llPaymentChannelMode.setVisibility(View.GONE);
                    }


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        eventMAPGHTicketHosting.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b) {
                ticketHosting = "Y";
                txt_post_channel.setVisibility(View.VISIBLE);
                spinnerPaymentChannelMode.setVisibility(View.VISIBLE);
            } else {
                ticketHosting = "N";
                txt_post_channel.setVisibility(View.GONE);
                spinnerPaymentChannelMode.setVisibility(View.GONE);
            }
        });

        checkBoxGateKeepingServices.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b) {
                gateKeepingServices = "Y";
            } else {
                gateKeepingServices = "N";
            }
        });

        etEventDate.setOnClickListener(view -> {

        });
        etEventDate.setOnClickListener(this);
        addEventButton.setOnClickListener(this);
        etEventStartTime.setOnClickListener(this);
        etEventEndTime.setOnClickListener(this);
        flEventPoster.setOnClickListener(this);
        addRecurring.setOnClickListener(this);
    }


    private void initMap() {
        if (getActivity() != null) {
            if (getActivity() != null) {
                ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
                ((DashboardActivity) getActivity()).appBaseMapBox.setOnInfoWindowClickListener(this);
                ((DashboardActivity) getActivity()).appBaseMapBox.setOnCameraIdleListeners(this);
                ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());
            }
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_EVENT_ADD_EVENT) {
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                dismissProgressBar();
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        super.onFailResponse(apiId, error);
        dismissProgressBar();
        Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();
    }

    private void checkEmptyValidation() {

        if (TextUtils.isEmpty(etEventName.getText().toString())) {
            etEventName.setError("Please add event name");
            etEventName.setFocusable(true);
            return;
        }
        if (TextUtils.isEmpty(etEventLocation.getText().toString())) {
            etEventLocation.setError("Please add location ");
            etEventLocation.setFocusable(true);
            return;
        }
        if (TextUtils.isEmpty(etEventAbout.getText().toString())) {
            etEventAbout.setError("Please add something about event");
            etEventAbout.setFocusable(true);
            return;
        }

        if (!TextUtils.isEmpty(eventCoverageOn) && eventCoverageOn.equalsIgnoreCase("Y")) {
            if (TextUtils.isEmpty(eventCoverageOn)) {
                Toast.makeText(getActivity(), "Please select event coverage", Toast.LENGTH_SHORT).show();
                return;

            }
        }
        if (TextUtils.isEmpty(imagePoster)) {
            Toast.makeText(getActivity(), "Please add poster media", Toast.LENGTH_SHORT).show();
            return;

        }

        if (previousMediaGallery.size() == 0) {
            Toast.makeText(getActivity(), "Please add previous media gallery", Toast.LENGTH_SHORT).show();
            return;

        }


        if (currentMediaGallery.size() == 0) {
            Toast.makeText(getActivity(), "Please add current media gallery", Toast.LENGTH_SHORT).show();
            return;

        }
        if (TextUtils.isEmpty(etEventHostName.getText().toString())) {
            etEventHostName.setError("Please add host name");
            etEventHostName.setFocusable(true);

            return;
        }
        if (TextUtils.isEmpty(etEventPhone.getText().toString())) {
            etEventPhone.setError("Please add phone number");
            etEventPhone.setFocusable(true);

            return;
        }
        if (TextUtils.isEmpty(etEventEmail.getText().toString())) {
            etEventEmail.setError("Please add email address");
            etEventEmail.setFocusable(true);

            return;
        }

        if (!TextUtils.isEmpty(seats) && seats.equalsIgnoreCase("LIMITED"))
            if (TextUtils.isEmpty(etNoOfSeats.getText().toString())) {
                etNoOfSeats.setError("Please add number of seats");
                return;
            }
        if (TextUtils.isEmpty(etTicketPrice.getText().toString())) {
            etTicketPrice.setError("Please add ticket price");
            return;
        }
    }


    private void initSpinnerPaymentModeChannelMode() {
        String[] strings = getResources().getStringArray(R.array.payment_channel_mode);
        paymentChannelModeAdapter = new SimpleSpinnerAdapter(getContext(), strings);
        paymentChannelModeAdapter.setSelGender(0);
        spinnerPaymentChannelMode.setAdapter(paymentChannelModeAdapter);
    }

    private String getIfEventCoverage(int radioId) {
        switch (radioId) {
            case R.id.radio_photo:
                return "Photo";
            case R.id.radio_video:
                return "Video";
            case R.id.radio_photo_video:
                return "both";
        }
        return "";
    }

    private void initRecycleLayoutManager() {
        previousMediaGallery = new ArrayList<>();
        currentMediaGallery = new ArrayList<>();
        rvCurrentMedia.setHasFixedSize(true);
        rvPreviousMedia.setHasFixedSize(true);
        previousGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "previous_media", this, previousMediaGallery);

        rvPreviousMedia.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvPreviousMedia.setAdapter(previousGalleryAdapter);

        currentGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "current_media", this, currentMediaGallery);
        rvCurrentMedia.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvCurrentMedia.setAdapter(currentGalleryAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_event:
                displayProgressBar(false);
                try {
                    onPostSubmitAddEvent();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.add_event_previous_media:
                hideKeyboard();

                break;
            case R.id.add_event_current_media:
                hideKeyboard();
                multipleCurrentMediaSelector();
                break;
            case R.id.frame_poster:
                hideKeyboard();
                SingleMediaSelector();
                break;
            case R.id.event_date:
                type = "event_date";
                onDobDialog();
                break;
            case R.id.event_start_time:
                hideKeyboard();
                displayTimePickerDialog(1);
                break;
            case R.id.event_end_time:
                hideKeyboard();
                displayTimePickerDialog(2);
                break;
            case R.id.add_multiple_event_recurring:
                addEventSectionView();
                break;
        }
    }


    private void initAutoCompeteBox() {

        geocoderAutoCompleteView = getView().findViewById(R.id.event_location);
        geocoderAutoCompleteView.setAccessToken(getString(R.string.mapbox_api_token));
        geocoderAutoCompleteView.setType(GeocodingCriteria.TYPE_POI);
        geocoderAutoCompleteView.setOnFeatureListener(this);
    }

    private void addEventSectionView() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_recurring_event, null);
        TextView startEventDate = addView.findViewById(R.id.event_start_date);
        TextView startEventTime = addView.findViewById(R.id.event_start_time);
        TextView endEventDate = addView.findViewById(R.id.event_end_date);
        TextView endEventTime = addView.findViewById(R.id.event_end_time);
        TextView eventRecurringAddress = addView.findViewById(R.id.event_recurring_address);
        ImageView mapIcon = addView.findViewById(R.id.map_icon);
        ImageView deleteIcon = addView.findViewById(R.id.delete_icon);

        CustomDatePickerDialog.OnDateListener onDateListener = date -> {
            if (date != null) {
                this.date = date;
                SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
                String mDob = format.format(date);
                if (type.equalsIgnoreCase("event_start_date")) {
                    startEventDate.setText(mDob);
                } else if (type.equalsIgnoreCase("event_end_date")) {
                    endEventDate.setText(mDob);
                }

            }
        };

        TimePickerDialog.TimePickerListner timePickerListner = new TimePickerDialog.TimePickerListner() {

            @Override
            public void OnDoneButton(Dialog dialog, Calendar c) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                String time = simpleDateFormat.format(c.getTime());
                if (time_type == 3) {
                    startEventTime.setText(time);
                }
                if (time_type == 4) {
                    endEventTime.setText(time);
                }
                timePickerDialog.dismiss();
            }

            @Override
            public void OnCancelButton(Dialog dialog) {

            }
        };

        EventMapListener eventMapListener = item -> {
            String lat = item.getLat();
            String log = item.getLat();
            String address = item.getAddress();

            eventRecurringAddress.setText(address);
        };
        mapIcon.setOnClickListener(view -> {
            navigateToLocationScreen((AppBaseActivity) getActivity(), eventMapListener, Constants.ADDEVENTS);

        });
        startEventTime.setOnClickListener(view -> {
            hideKeyboard();
            displayTimePickerDialogEventSection(3, timePickerListner);

        });

        endEventTime.setOnClickListener(view -> {
            hideKeyboard();
            displayTimePickerDialogEventSection(4, timePickerListner);
        });
        startEventDate.setOnClickListener(view -> {
            type = "event_start_date";
            onEventSectionDobDialog(onDateListener);

        });
        endEventDate.setOnClickListener(view -> {
            type = "event_end_date";
            onEventSectionDobDialog(onDateListener);

        });
        deleteIcon.setOnClickListener(v ->
        {
            if (llRecursiveView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(llRecursiveView.getChildCount());
        llRecursiveView.addView(addView);

    }


    private void multiplePreviousMediaSelector() {
        displayLog(TAG, "multipleImageVideoSelector: ");
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_PREVIOUS_MULTIPLE);

    }

    private void multipleCurrentMediaSelector() {
        displayLog(TAG, "multipleImageVideoSelector: ");
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_CURRENT_MULTIPLE);

    }

    private void SingleMediaSelector() {
        displayLog(TAG, "singleImageVideoSelector: ");
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(1)
                .maxSelectablePerMediaType(1, 1)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_SINGLE);

    }

    private void displayTimePickerDialogEventSection(int type, TimePickerDialog.TimePickerListner timePickerListner) {
        time_type = type;
        timePickerDialog = new TimePickerDialog(getActivity(), Calendar.getInstance(), timePickerListner);
        timePickerDialog.show();

    }

    private void displayTimePickerDialog(int type) {
        time_type = type;
        timePickerDialog = new TimePickerDialog(getActivity(), Calendar.getInstance(), this);
        timePickerDialog.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CODE_CHOOSE_PREVIOUS_MULTIPLE) {
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                previousMediaGallery.addAll(paths);
                displayLog(TAG, "onActivityResult: " + previousMediaGallery.get(0));
                refreshPreviousGalleryAdapter();
            }
        } else if (requestCode == REQUEST_CODE_CHOOSE_CURRENT_MULTIPLE) {
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                currentMediaGallery.addAll(paths);
                displayLog(TAG, "onActivityResult: " + currentMediaGallery.get(0));
                refreshCurrentGalleryAdapter();
            }
        } else if (requestCode == REQUEST_CODE_CHOOSE_SINGLE) {

            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                posterIcon.setVisibility(View.GONE);
                imagePoster = paths.get(0);
                Glide.clear(posterImage);
                Glide.with(App.sharedInstance())
                        .load(paths.get(0))
                        .dontAnimate()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                                    isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                                    target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .into(posterImage);
            } else {
                posterIcon.setVisibility(View.VISIBLE);
            }
        }
    }

    private void refreshPreviousGalleryAdapter() {
        if (previousMediaGallery != null) {
            previousGalleryAdapter.notifyDataSetChanged();
        }
    }

    private void refreshCurrentGalleryAdapter() {
        if (currentMediaGallery != null) {
            currentGalleryAdapter.notifyDataSetChanged();
        }
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        dialog.create(getContext(), false).callback(this).show();
    }

    private void onEventSectionDobDialog(CustomDatePickerDialog.OnDateListener listener) {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        dialog.create(getContext(), false).callback(listener).show();
    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            this.date = date;
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            String mDob = format.format(date);
            etEventDate.setText(mDob);
        }
    }

    public void navigateToLocationScreen(AppBaseActivity baseActivity, EventMapListener listener, String screenName) {

        AddLocationFragment addLocationFragment = new AddLocationFragment();
        addLocationFragment.initOnMapIconClickListener(listener);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.type, screenName);
        addLocationFragment.setArguments(bundle);
        baseActivity.pushFragment(addLocationFragment, true);
    }

    @Override
    public void OnDoneButton(Dialog dialog, Calendar c) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(c.getTime());

        if (time_type == 1) {
            etEventStartTime.setText(time);
        }
        if (time_type == 2) {
            etEventEndTime.setText(time);
        }
        timePickerDialog.dismiss();
    }

    @Override
    public void OnCancelButton(Dialog dialog) {
        timePickerDialog.dismiss();
    }

    private void addEventInfoSection() {
        eventSectionList.add(new EventInfoSection(utils.convertedDateMMDDYYYY(etEventDate.getText().toString()), "", etEventStartTime.getText().toString(), etEventEndTime.getText().toString(), "10.0", "10.0", "address"));
        int count = llRecursiveView.getChildCount();
        if (llRecursiveView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = llRecursiveView.getChildAt(i - 1);
                TextView startEventDate = addView.findViewById(R.id.event_start_date);
                TextView startEventTime = addView.findViewById(R.id.event_start_time);
                TextView endEventDate = addView.findViewById(R.id.event_end_date);
                TextView endEventTime = addView.findViewById(R.id.event_end_time);

                String startDate = startEventDate.getText().toString();
                String startTime = startEventTime.getText().toString();
                String endDate = endEventDate.getText().toString();
                String endTime = endEventTime.getText().toString();
                eventSectionList.add(new EventInfoSection(utils.convertedDateMMDDYYYY(startDate), utils.convertedDateMMDDYYYY(endDate), startTime, endTime, "10.0", "10.0", "address"));
            }
        }
    }

    private void onPostSubmitAddEvent() throws Exception {

        checkEmptyValidation();

        addEventInfoSection();
        HashMap<String, String> map = new HashMap<>();

        map.put(BaseArguments.name, etEventName.getText().toString());
        map.put(BaseArguments.type, type);
        map.put(BaseArguments.details, etEventAbout.getText().toString());
        map.put(BaseArguments.phone, etEventPhone.getText().toString());
        map.put(BaseArguments.email, etEventEmail.getText().toString());
        map.put(BaseArguments.user_id, etEventName.getText().toString());
        map.put(BaseArguments.event_coverage_on, eventCoverageOn);
        map.put(BaseArguments.event_coverage, eventCoverage);
        map.put(BaseArguments.host_name, etEventHostName.getText().toString());
        map.put(BaseArguments.paid_event, paidEvent);
        map.put(BaseArguments.ticket_price, etTicketPrice.getText().toString());
        map.put(BaseArguments.mapgh_ticket_hosting, ticketHosting);
        map.put(BaseArguments.post_payment_channel, postChannelMethod);
        map.put(BaseArguments.branch_name, etBranchName.getText().toString());
        map.put(BaseArguments.bank_name, etBankName.getText().toString());
        map.put(BaseArguments.account_number, etEventName.getText().toString());
        map.put(BaseArguments.gatekeeping_services, gateKeepingServices);
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(eventSectionList, new TypeToken<List<EventInfoSection>>() {
        }.getType());

        if (!element.isJsonArray()) {
            throw new Exception();
        }

        JsonArray jsonArray = element.getAsJsonArray();
        map.put(BaseArguments.event_section, jsonArray.toString());
        final MultipartBody.Part[] multiSinglePosterGalleryPart = new MultipartBody.Part[1];
        MultipartBody.Part[] multiPreviousGalleryPart = new MultipartBody.Part[previousMediaGallery.size()];
        MultipartBody.Part[] multiCurrentGalleryPart = new MultipartBody.Part[currentMediaGallery.size()];
        if (!TextUtils.isEmpty(imagePoster)) {
            int i = 0;
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(imagePoster));
            String mimeType = fileInformation.getMimeType();
            if (video_mime_List.contains(mimeType)) {
                multiSinglePosterGalleryPart[i] = RetrofitUtils.createFilePart(BaseArguments.image,
                        imagePoster, RetrofitUtils.MEDIA_TYPE_VIDEO);
            } else {
                multiSinglePosterGalleryPart[i] = RetrofitUtils.createFilePart(BaseArguments.image,
                        imagePoster, RetrofitUtils.MEDIA_TYPE_IMAGE_PNG);
            }
        }

        for (int i = 0; i < previousMediaGallery.size(); i++) {
            String url = previousMediaGallery.get(0);
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
            String mimeType = fileInformation.getMimeType();

            if (video_mime_List.contains(mimeType)) {
                File file = new File(previousMediaGallery.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                multiPreviousGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.previous_media_gallery + "[" + i + "]", file.getName(), media);

            } else {
                File file = new File(previousMediaGallery.get(i));
                RequestBody media = RequestBody.create(MediaType.parse("image/*"), file);
                multiPreviousGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.previous_media_gallery + "[" + i + "]", file.getName(), media);

            }
        }
        for (int i = 0; i < currentMediaGallery.size(); i++) {
            String url = currentMediaGallery.get(0);
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
            String mimeType = fileInformation.getMimeType();
            if (video_mime_List.contains(mimeType)) {
                File file = new File(currentMediaGallery.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.current_media_gallery + "[" + i + "]", file.getName(), media);

            } else {
                File file = new File(currentMediaGallery.get(i));
                RequestBody media = RequestBody.create(MediaType.parse("image/*"), file);
                multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.current_media_gallery + "[" + i + "]", file.getName(), media);
            }
        }

        restClient.callback(this).postAddEvent(getContext(), RetrofitUtils.createMultipartRequest(map), multiPreviousGalleryPart, multiCurrentGalleryPart, multiSinglePosterGalleryPart);
    }

    @Override
    public void onFeatureClick(CarmenFeature feature) {
        mapContainer.setVisibility(View.VISIBLE);

        Position position = feature.asPosition();
        latLng = new LatLng(position.getLatitude(), position.getLongitude());
        address = geocoderAutoCompleteView.getText().toString().trim();
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(latLng);
        ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.ic_launcher, latLng,
                Constants.You, address);
        geocoderAutoCompleteView.setText(address);

    }

    @Override
    public void onCameraIdle() {
        if (latLng != null) {
            title = "Your Selected Location";
            latitude = latLng.getLatitude();
            longitude = latLng.getLongitude();
        }
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        return false;
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(latLng);
        ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.ic_launcher, latLng,
                Constants.You, address);
    }

    @Override
    public void userLocationChange(Location location) {
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    @Override
    public void onAddClick(String type) {
        if (type.equalsIgnoreCase("previous_media")) {
            multiplePreviousMediaSelector();

        } else if (type.equalsIgnoreCase("current_media")) {
            multipleCurrentMediaSelector();
        }
    }

    @Override
    public void onDeleteClick(int position, String type) {

        if (type.equalsIgnoreCase("previous_media")) {
            for (int i = 0; i < previousMediaGallery.size(); i++) {
                if (previousMediaGallery.get(position).equalsIgnoreCase(previousMediaGallery.get(i))) {
                    previousMediaGallery.remove(i);
                    refreshGalleryAdapter(type);
                }
            }
        } else {
            for (int i = 0; i < currentMediaGallery.size(); i++) {
                if (currentMediaGallery.get(position).equalsIgnoreCase(currentMediaGallery.get(i))) {
                    currentMediaGallery.remove(i);
                    refreshGalleryAdapter(type);
                }
            }
        }
    }

    private void refreshGalleryAdapter(String type) {
        if (type.equalsIgnoreCase("previous_media")) {
            if (previousMediaGallery != null) {
                previousGalleryAdapter.notifyDataSetChanged();
            }
        } else {
            if (currentMediaGallery != null) {
                currentGalleryAdapter.notifyDataSetChanged();
            }
        }
    }
}
