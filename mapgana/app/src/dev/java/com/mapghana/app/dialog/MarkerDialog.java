package com.mapghana.app.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseDialog;
import com.mapghana.customviews.TypefaceTextView;

/**
 * Created by ubuntu on 9/2/18.
 */

public class MarkerDialog extends AppBaseDialog {

    private String name, address;

    public MarkerDialog(@NonNull Context context, String name, String address) {
        super(context);
        this.name = name;
        this.address = address;
    }

    @Override
    protected int getLayoutResourceView() {
        return R.layout.dialog_marker;
    }

    @Override
    protected void initializeComponent() {
        TypefaceTextView tvName = (TypefaceTextView) this.findViewById(R.id.tvName);
        TypefaceTextView tvAddress = (TypefaceTextView) this.findViewById(R.id.tvAddress);
        LinearLayout ll_view = (LinearLayout) this.findViewById(R.id.ll_view);
        tvName.setText("Name: " + name);
        if (address.length() != 0) {
            tvAddress.setVisibility(View.VISIBLE);
            tvAddress.setText("Address: " + address);
        } else {
            tvAddress.setVisibility(View.INVISIBLE);
        }
        ll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }
}
