package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ubuntu on 25/1/18.
 */

public class DefaulFilterSpf {
    private static SharedPreferences preferences;
    private static String prefName="map_ghana_def_filter";
    private static SharedPreferences.Editor editor;
    private static String SUB_CATEGORY_ID="sub_category_id";
    private static String CITY_ID="city_id";
    private static String LAT="lat";
    private static String LOG="log";
    private static String DISTANCE="distance";
    private static String FILTERS="filters";

    private DefaulFilterSpf() {}

    private static void init(Context context){
        preferences=context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor=preferences.edit();
    }


    public static void clearFilter(Context context){
        init(context);
        editor.clear();
        editor.commit();
    }

    public static void saveFilter(Context context, String filters){
        init(context);
        editor.putString(FILTERS, filters);
        editor.commit();
    }

    public static String getFilter(Context context){
        init(context);
        return preferences.getString(FILTERS, null);
    }



    /*public static void setDeafultFitler(Context context, String sub_category_id,String city_id, String lat, String log,String distance  ){
        init(context);
        editor.putString(SUB_CATEGORY_ID, sub_category_id);
        editor.putString(CITY_ID, city_id);
        editor.putString(DISTANCE, distance);
        editor.commit();
    }


    public static HashMap<String, String> getDefaultFilter(Context context){
        init(context);
        String sub_category_id=preferences.getString(SUB_CATEGORY_ID, "");
        String city_id=preferences.getString(CITY_ID, "");
        String lat=preferences.getString(LAT, "");
        String log=preferences.getString(LOG, "");
        String distance=preferences.getString(DISTANCE, "");

        HashMap<String, String> map=new HashMap<>();
        map.put(SUB_CATEGORY_ID ,sub_category_id);
        map.put(CITY_ID ,city_id);
        map.put(LAT ,lat);
        map.put(LOG ,log);
        map.put(DISTANCE ,distance);
        return map;
    }

    public static void sub_category_id(Context context, String sub_category_id){
        init(context);
        editor.putString(SUB_CATEGORY_ID, sub_category_id);
        editor.commit();
    }

    public static void setCityId(Context context, String city_id){
        init(context);
        editor.putString(CITY_ID, city_id);
        editor.commit();
    }

    public static String getCityId(Context context){
        init(context);
        return preferences.getString(CITY_ID, "");

    }

    public static void setLocation(Context context,String distance){
        init(context);
        editor.putString(DISTANCE, distance);
        editor.commit();
    }
    public static String getLocation(Context context){
        init(context);
        return preferences.getString(DISTANCE, "");

    }*/
}
