package com.mapghana.app.ui.sidemenu.individual.viewdetails;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Filter;
import android.widget.ImageButton;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.model.IndividualPosts;
import com.mapghana.app.model.OrganizationPosts;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewDetailsFragment extends AppBaseFragment implements
        MapboxMap.OnInfoWindowClickListener, MapboxMap.OnMarkerClickListener,
        AppBaseMapBox.OnMapLoaded, LocationServiceListner {


    private TypefaceTextView tvViewDetails, tv_no_data_found;
    private ImageButton imgBack;
    private static final String TAG = "ViewDetailsFragment";
    private HashMap<Marker, OrganizationPosts.DataBean> markerHashMap_Org;
    private HashMap<Marker, IndividualPosts.DataBean> markerHashMap_Indi;
    private ArrayList<OrganizationPosts.DataBean> orgList;
    private ArrayList<OrganizationPosts.DataBean> orgFilterList;
    private ArrayList<IndividualPosts.DataBean> indiList;
    private ArrayList<IndividualPosts.DataBean> indiFilterList;
    private int TYPE;
    private int POST_ID;
    private CustomFilter customFilter;
    private int zoomLevel;
    private boolean hasResponse = false;
    private MapboxMap mapboxMap;
    private String responseData_org, responseData_indi;
    Marker currentMarker;

    private String sub_category_id;
    private String category_id;

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_view_details;
    }

    @Override
    public void initializeComponent() {
        init();
        tvViewDetails = (TypefaceTextView) getView().findViewById(R.id.tvViewDetails);
        imgBack = (ImageButton) getView().findViewById(R.id.imgBack);
        tv_no_data_found = getView().findViewById(R.id.tv_no_data_found);
        ImageButton imgMyLoc = (ImageButton) getView().findViewById(R.id.imgMyLoc);
        TypefaceEditText etSearch = (TypefaceEditText) getView().findViewById(R.id.etSearch);

        POST_ID = 0;
        customFilter = new CustomFilter();
        markerHashMap_Org = new HashMap<>();
        markerHashMap_Indi = new HashMap<>();
        orgList = new ArrayList<>();
        orgFilterList = new ArrayList<>();
        indiList = new ArrayList<>();
        indiFilterList = new ArrayList<>();

        tvViewDetails.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgMyLoc.setOnClickListener(this);
        zoomLevel = 13;

        initMap();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (orgFilterList.size() > 0 || indiFilterList.size() > 0) {
                    customFilter.filter(s.toString());
                }
            }
        });
    }

    private void initMap() {
        if (getActivity() == null) return;
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnCameraIdleListeners(null);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnMarkerClickListener(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.setOnInfoWindowClickListener(this);
        ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvViewDetails:
                hideKeyboard();
                if (POST_ID != 0) {
                    changeFragment(POST_ID);
                } else {
                    displayToast("Plese select one marker");
                }
                break;
            case R.id.imgBack:
                hideKeyboard();
                getActivity().onBackPressed();
                break;
            case R.id.imgMyLoc:
                hideKeyboard();
                setMarkersOnMapAgain();
                break;
        }
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_Filter_organization) {
                try {

                    responseData_org = response.body().string();
                    updateUiForOrganization(responseData_org);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_POSTS_Filter_individual) {
                try {
                    responseData_indi = response.body().string();
                    updateUiForIndividual(responseData_indi);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    private void updateUiForIndividual(String responseData_org) {
        Gson gson = new Gson();
        IndividualPosts items = gson.fromJson(responseData_org, IndividualPosts.class);
        if (items.getStatus() != 0) {
            clearIndiHashMap();
            clearOrgHashMap();
            clearIndiList();
            clearIndi_FilterList();
            clearOrgList();
            clearOrg_FilterList();
            TYPE = 1;
            if (items.getData() != null) {
                if (items.getData().size() > 0) {
                    indiList.addAll(items.getData());
                    indiFilterList.addAll(items.getData());
                    setMarkerOfIndividual(items.getData());
                } else {
                    displayToast("No posts found in this category");
                    getActivity().onBackPressed();
                }
            } else {
                displayToast("No posts found in this category");
                getActivity().onBackPressed();
            }
        } else {
            displayErrorDialog("Error", items.getError());
        }

    }


    private void updateUiForOrganization(String response) {
        Gson gson = new Gson();
        OrganizationPosts items = gson.fromJson(response, OrganizationPosts.class);
        if (items.getStatus() != 0) {
            clearIndiHashMap();
            clearOrgHashMap();
            clearIndiList();
            clearIndi_FilterList();
            clearOrgList();
            clearOrg_FilterList();

            TYPE = 0;
            orgList.addAll(items.getData());
            orgFilterList.addAll(items.getData());
            if (items.getData() != null) {
                if (items.getData().size() > 0) {

                    setMarkerOfOrganization(items.getData());
                } else {
                    displayToast("No posts found in this category");
                    getActivity().onBackPressed();
                }
            } else {
                displayToast("No posts found in this category");
                getActivity().onBackPressed();
            }
        } else {
            displayErrorDialog("Error", items.getError());
        }

    }


    private void clearOrgHashMap() {
        if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
            markerHashMap_Org.clear();
        }
    }

    private void clearIndiHashMap() {
        if (markerHashMap_Indi != null && markerHashMap_Indi.size() > 0) {
            markerHashMap_Indi.clear();
        }


    }

    private void clearIndiList() {
        if (indiList != null && indiList.size() > 0) {
            indiList.clear();
        }
    }

    private void clearIndi_FilterList() {
        if (indiFilterList != null && indiFilterList.size() > 0) {
            indiFilterList.clear();
        }
    }

    private void clearOrgList() {
        if (orgList != null && orgList.size() > 0) {
            orgList.clear();
        }
    }

    private void clearOrg_FilterList() {
        if (orgFilterList != null && orgFilterList.size() > 0) {
            orgFilterList.clear();
        }
    }


    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        displayLog(TAG, "onMarkerClick: " + marker);
        displayLog(TAG, "onMarkerClick:size " + markerHashMap_Org.size());
        if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
            POST_ID = getPostIdFromOrganizationMap(marker);
        }
        if (markerHashMap_Indi != null && markerHashMap_Indi.size() > 0) {
            POST_ID = getPostIdFronIndividualMap(marker);
        }
        return false;
    }

    private int getPostIdFromOrganizationMap(Marker marker) {
        if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
            displayLog(TAG, "organization: ");
            for (Map.Entry<Marker, OrganizationPosts.DataBean> m : markerHashMap_Org.entrySet()) {
                if (!m.getValue().getName().equalsIgnoreCase(Constants.You) && m.getKey() == marker) {
                    OrganizationPosts.DataBean dataBean = m.getValue();
                    return dataBean.getId();
                }
            }
        }
        return 0;
    }

    private int getPostIdFronIndividualMap(Marker marker) {
        ////
        if (markerHashMap_Indi != null && markerHashMap_Indi.size() > 0) {
            displayLog(TAG, "Individual: ");
            for (Map.Entry<Marker, IndividualPosts.DataBean> m : markerHashMap_Indi.entrySet()) {
                if (!m.getValue().getName().equalsIgnoreCase(Constants.You) && m.getKey() == marker) {
                    IndividualPosts.DataBean dataBean = m.getValue();
                    displayLog(TAG, "naam: " + dataBean.getId());
                    return dataBean.getId();
                }
            }
        }
        return 0;
    }

    private void changeFragment(int post_id) {
        if (post_id == 0) {
            return;
        }
        PostDetailFragment detailsFragment = new PostDetailFragment();
        detailsFragment.setPost_id(post_id);
      /*  Bundle bundle = new Bundle();
        bundle.putInt(Constants.post_id, post_id);
        detailsFragment.setArguments(bundle);*/
        ((AppBaseActivity) getActivity()).changeFragment(detailsFragment, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        hideKeyboard();


        if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
            POST_ID = getPostIdFromOrganizationMap(marker);
        }
        if (markerHashMap_Indi != null && markerHashMap_Indi.size() > 0) {
            POST_ID = getPostIdFronIndividualMap(marker);
        }
        changeFragment(POST_ID);
        return false;
    }


    private void setMarkerOfOrganization(List<OrganizationPosts.DataBean> data) {
        clearOrgHashMap();

        if (getActivity() == null || !isVisible()) return;
        displayLog(TAG, "setMarkersOnMapAgain: " + TYPE);
        ((DashboardActivity) getActivity()).appBaseMapBox.clearMarker();

        setCurrentLocation();
        try {
            if (data != null && data.size() > 0) {


                displayLog(TAG, "setMarkerOfOrganization: " + data.size());
                final List<LatLng> boundList = new ArrayList<>();
                int resource = 0;
                Marker marker;

                for (int i = 0; i < data.size(); i++) {

                    LatLng latLng = null;
                    try {
                        latLng = new LatLng(data.get(i).getLat(),
                                data.get(i).getLog());
                    } catch (IllegalArgumentException e) {
                        continue;
                    }

                    displayLog(TAG, "lat: " + data.get(i).getLat());
                    displayLog(TAG, "log: " + data.get(i).getLog());
                    if (data.get(i).getStatus().trim().equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.location_gree_org;

                    } else if (data.get(i).getStatus().trim().equalsIgnoreCase(Constants.unverified)) {
                        resource = R.mipmap.location_red_org;

                    } else {
                        resource = R.mipmap.location_gry_org;

                    }

                    String categoryName = "Category: ";
                    OrganizationPosts.DataBean.CategoryBean category = data.get(i).getCategory();
                    if (category != null) {
                        categoryName = categoryName + category.getName();
                    }
                    OrganizationPosts.DataBean.SubcategoryBean subcategory = data.get(i).getSubcategory();
                    if (subcategory != null) {
                        categoryName = categoryName + " (" + subcategory.getName() + ")";
                    }
                    marker = ((DashboardActivity) getActivity()).
                            appBaseMapBox.addMarker(getContext(), resource, latLng, data.get(i).getName(),
                            categoryName + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus());
                    markerHashMap_Org.put(marker, data.get(i));
                    boundList.add(latLng);
                }
                ((DashboardActivity) getActivity()).appBaseMapBox.boundMap(boundList);
            } else {
                displayLog(TAG, "setMarkerOfOrganization: mapview is null");
            }
        } catch (Exception e) {
            displayLog(TAG, "setMarkerOfOrganization: " + e.getMessage());
        }
    }

    private void setMarkerOfIndividual(List<IndividualPosts.DataBean> data) {
        //  Icon icon = Icon.createWithResource();
        clearIndiHashMap();

        if (getActivity() == null || !isVisible()) return;
        ((DashboardActivity) getActivity()).appBaseMapBox.clearMarker();

        setCurrentLocation();

        displayLog(TAG, "setMarkersOnMapAgain: " + TYPE);
        if (data != null && data.size() > 0) {
            displayLog(TAG, "setMarkerOfIndividual: " + data.size());
            int resourse = 0;
            String status = "";
            String gender = "";
            Marker marker;
            List<LatLng> boundList = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                LatLng latLng = null;
                try {
                    latLng = new LatLng(data.get(i).getLat(),
                            data.get(i).getLog());
                } catch (IllegalArgumentException e) {
                    continue;
                }

                status = data.get(i).getStatus().trim();
                gender = data.get(i).getGender().trim();
                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.individual_green_gril;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.individual_red_girl;
                    } else {
                        resourse = R.mipmap.indi_girl_gray;
                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.individual_green_man;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.individual_red_man;
                    } else {
                        resourse = R.mipmap.individual_gray_man;
                    }
                }
                marker = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(),
                        resourse, latLng, data.get(i).getName(),
                        "Occupation: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus());

                markerHashMap_Indi.put(marker, data.get(i));
                boundList.add(latLng);
            }

            ((DashboardActivity) getActivity()).appBaseMapBox.boundMap(boundList);
        } else {
            displayLog(TAG, "setMarkerOfOrganization: mapview is null");
        }
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
       /* displayLog(TAG, "onMapLoadedd: " + getArguments().getString(Constants.sub_category_id));
        displayLog(TAG, "onMapLoadedd: " + getArguments().getString(Constants.category_id));
*/
        if (!hasResponse) {
            hasResponse = true;
            //  if (getArguments() != null) {
            if (sub_category_id != null && sub_category_id.length() > 0) {
                //from organizaion
                displayLog(TAG, "sub_cat_id: " + sub_category_id);

                if (ConnectionDetector.isNetAvail(getContext())) {
                    displayProgressBar(false);
                    RestClient restClient = new RestClient(getContext());
                    restClient.callback(this).post_listing_organizations(sub_category_id);
                } else {
                    displayToast(Constants.No_Internet);
                }
            } else {
                //from individual
                displayLog(TAG, "category_id: " + category_id);
                /*****************/
                if (ConnectionDetector.isNetAvail(getContext())) {
                    displayProgressBar(false);
                    RestClient restClient = new RestClient(getContext());

                    restClient.callback(this).post_listing_Individuals(category_id);
                } else {
                    displayToast(Constants.No_Internet);
                }
            }
            //  }
        } else {
            if (hasResponse && responseData_org != null) {
                displayLog(TAG, "not running retrodfit: ");
                updateUiForOrganization(responseData_org);
            } else if (hasResponse && responseData_indi != null) {
                updateUiForIndividual(responseData_indi);
            }
        }
    }

    private void setMarkersOnMapAgain() {
        if (getActivity() == null || !isVisible()) return;
        zoomLevel = 17;
        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        LatLng latLng = new LatLng(lat, log);
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(log);
        moveMarker(location);
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(latLng, zoomLevel);
    }

    private void setCurrentLocation() {
        if (getActivity() == null || !isVisible()) return;
        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;

        if (lat != 0 && log != 0) {
            Marker marker = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.my_location, new LatLng(lat, log),
                    Constants.You, "");
            ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(new LatLng(lat, log), zoomLevel);
            if (TYPE == 0) {
                OrganizationPosts.DataBean bean = new OrganizationPosts.DataBean();
                bean.setName(Constants.You);
                markerHashMap_Org.put(marker, bean);
            } else if (TYPE == 1) {
                IndividualPosts.DataBean bean = new IndividualPosts.DataBean();
                bean.setName(Constants.You);
                markerHashMap_Indi.put(marker, bean);
            }
        }
    }

    class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.toString().length() != 0) {

                constraint = constraint.toString().toUpperCase();

                if (TYPE == 0) {
                    ArrayList<OrganizationPosts.DataBean> filters = new ArrayList<>();
                    for (OrganizationPosts.DataBean dataBean : orgList) {
                        if (dataBean.getName().toUpperCase().contains(constraint)) {
                            filters.add(dataBean);
                        }
                    }

                    filterResults.count = filters.size();
                    filterResults.values = filters;
                    displayLog(TAG, "performFiltering1 org: " + filters.size());
                } else if (TYPE == 1) {
                    ArrayList<IndividualPosts.DataBean> filters = new ArrayList<>();
                    for (IndividualPosts.DataBean dataBean : indiList) {
                        if (dataBean.getName().toUpperCase().contains(constraint)) {
                            filters.add(dataBean);
                        }
                    }
                  /*  if (filters.size() == 0) {
                        tv_no_data_found.setVisibility(View.VISIBLE);
                    } else {
                        tv_no_data_found.setVisibility(View.GONE);
                    }*/
                    filterResults.count = filters.size();
                    filterResults.values = filters;
                }
            } else {
                if (TYPE == 0) {
                    filterResults.count = orgFilterList.size();
                    filterResults.values = orgFilterList;
                } else if (TYPE == 1) {
                    filterResults.count = indiFilterList.size();
                    filterResults.values = indiFilterList;
                }
            }
            displayLog(TAG, "filterResults.count: " + filterResults.count);
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (TYPE == 0) {
                orgList = (ArrayList<OrganizationPosts.DataBean>) results.values;
                List<OrganizationPosts.DataBean> orgList = (ArrayList<OrganizationPosts.DataBean>) results.values;
                if (orgList == null || orgList.size() == 0) {
                    tv_no_data_found.setVisibility(View.VISIBLE);
                } else {
                    tv_no_data_found.setVisibility(View.GONE);
                }

                setMarkerOfOrganization(orgList);

            } else if (TYPE == 1) {
                indiList = (ArrayList<IndividualPosts.DataBean>) results.values;
                List<IndividualPosts.DataBean> indiList = (ArrayList<IndividualPosts.DataBean>) results.values;
                if (indiList == null || indiList.size() == 0) {
                    tv_no_data_found.setVisibility(View.VISIBLE);
                } else {
                    tv_no_data_found.setVisibility(View.GONE);
                }
                setMarkerOfIndividual(indiList);
            }

            setCurrentLocation();

        }

    }

    @Override
    public void userLocationChange(Location location) {
        if (mapboxMap != null) {
            moveMarker(location);
        }
    }


    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    private void moveMarker(Location location) {
        if (location == null) {
            return;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        Marker marker = null;

        if (TYPE == 0) {
            if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
                for (Map.Entry<Marker, OrganizationPosts.DataBean> m : markerHashMap_Org.entrySet()) {
                    if (m.getValue().getName().equalsIgnoreCase(Constants.You)) {
                        marker = m.getKey();
                        break;
                    }
                }
            }
        } else if (TYPE == 1) {
            for (Map.Entry<Marker, IndividualPosts.DataBean> m : markerHashMap_Indi.entrySet()) {
                if (m.getValue().getName().equalsIgnoreCase(Constants.You)) {
                    marker = m.getKey();
                    break;
                }
            }
        }

        if (marker != null) {
            marker.setPosition(latLng);
        }
    }
}
