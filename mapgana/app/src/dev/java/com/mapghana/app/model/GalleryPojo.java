package com.mapghana.app.model;

/**
 * Created by ubuntu on 26/2/18.
 */

public class GalleryPojo {

    private int id;
    private String image;
    private boolean isVideo;

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
