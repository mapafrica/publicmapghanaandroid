package com.mapghana.app.ui.sidemenu.Event;

import com.mapghana.app.model.EventDetail;

public interface EventListCallback {
    void onClickEvent(EventDetail model);
    void onPopUpClickEvent(EventDetail model);
    void onCloseIcon();
}
