package com.mapghana.app.ui.sidemenu.postlitsing;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flutterwave.raveandroid.RaveConstants;
import com.flutterwave.raveandroid.RavePayActivity;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.adapter.AddEventGalleryAdapter;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.model.Category;
import com.mapghana.app.model.City;
import com.mapghana.app.model.CreateIndividualPost;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.model.Login;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CategoryAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CityAdapter;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.utils;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.handler.MediaAddAdpterListener;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Valiations;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.onegravity.contactpicker.contact.Contact;
import com.onegravity.contactpicker.contact.ContactDescription;
import com.onegravity.contactpicker.contact.ContactSortOrder;
import com.onegravity.contactpicker.core.ContactPickerActivity;
import com.onegravity.contactpicker.picture.ContactPictureType;
import com.theartofdev.edmodo.cropper.CropImage;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostListingIndividualsFragment extends AppBaseFragment implements MediaAddAdpterListener, CustomDatePickerDialog.OnDateListener, MapboxMap.OnInfoWindowClickListener,
        AppBaseMapBox.OnMapLoaded, AdapterClickListener {

    private EditText etName, etEmail, etAddress, etPhone, etWhatsApp, etWebsite, etDob, etBioAboutMe, etNicknameKeywordDescription, etMapghanaId, etTwitter, etInstagram, etFacebook, etYoutubeVimeo, etSoundCloud, etBlog, etGaming, etPasswordProtected;
    private Spinner spinnerRegion, spinnerGender, spinnerStatus, spinnerOccupation, spinnerFeature;
    private RadioGroup radioGroupLocationCategory;
    private RestClient restClient;
    private ImageView imageViewAddPhone, imageViewAddWebsite, imageViewSingle, imageViewSingleBrowse, imageViewAddGallery, imageViewAddFeature, imageViewAddGaming, imageViewAddBlog;
    private TypefaceTextView tv_unverify_status, tv_mapghana_id, tvSubmit;
    private FrameLayout frameLayout;
    private LinearLayout linearLayoutEmailView, linearLayoutFeatureView, linearLayoutMapghanaIdContainer, linearLayoutAddPhoneView, linearLayoutOptimisationView, linearLayoutAdvancedOptimisation, linearLayoutAddWebsiteView, linearLayoutAddBlogView, linearLayoutAddGamingView;
    private SwitchCompat switchCompatPhoneNumberVisibility, switchCompatAvailability, switchCompatOptimisation, switchCompatProfileVisibility, switchCompatPasswordProtection;
    private Uri uriOneImg;
    private String DOB;
    private static final String TAG = "PostListingIndividualsF";
    private ArrayList<City.DataBean> cityList;
    private ArrayList<Category.DataBean> categoryList;
    private List<String> listWebsiteItem, listPhoneItem;
    private final int REQUEST_CONTACT = 111;
    private Date date;
    private RecyclerView rv_gallery;
    private static final int REQUEST_CODE_CHOOSE = 888;
    private List<String> mediaList;
    private List<String> mEmailList;
    private List<String> mFeatureList;
    private List<String> mGamingList;
    private List<String> mBlogList;
    private List<String> video_mime_List;
    private final long VIDEO_MAXDURATION = 30000;
    private CategoryAdapter categoryAdapter;
    private CityAdapter cityAdapter;
    private SimpleSpinnerAdapter status_adapter, RegionAdapter, FeatureAdapter, StatusAdapter;
    private double latitude;
    private double longitude;
    private TextInputLayout passWordProtect;
    private double log = 78.008, lat = 27.176;
    private PopupMenu popupMenu;
    private SimpleSpinnerAdapter genderAdpater;
    private ImageView imageAddEmailView;
    private AddEventGalleryAdapter currentGalleryAdapter;
    private EventInfoSection eventInfoSection;
    private String phoneNumberVisibility, availablity, locationProfile, profileVisibility, passwordProtection, optimisation;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_post_listing_individual;
    }

    public static PostListingIndividualsFragment newInstance(String title, EventInfoSection eventInfoSection) {
        PostListingIndividualsFragment fragment = new PostListingIndividualsFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putSerializable("item", eventInfoSection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void initializeComponent() {
        init();
        initMap();
        restClient = new RestClient(getContext());
        initViewById(getView());
        initListener();
        spinnerClickListener();
        initRecycleLayoutManager();
        initAdapters();
        initList();
        getCity();
        initRecycleLayoutManager();
        if (getArguments() != null) {
            eventInfoSection = (EventInfoSection) getArguments().getSerializable("item");
            etAddress.setText(eventInfoSection.getAddress());
        }
    }

    private void initAdapters() {

        genderAdpater = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.gender_array));
        genderAdpater.setSelGender(0);
        spinnerGender.setAdapter(genderAdpater);

        String[] feature = getResources().getStringArray(R.array.feature);
        FeatureAdapter = new SimpleSpinnerAdapter(getContext(), feature);
        FeatureAdapter.setSelGender(0);
        spinnerFeature.setAdapter(FeatureAdapter);

        String[] status = getResources().getStringArray(R.array.status_array2);
        StatusAdapter = new SimpleSpinnerAdapter(getContext(), status);
        StatusAdapter.setSelGender(0);
        spinnerStatus.setAdapter(StatusAdapter);

    }

    private void initList() {
        mEmailList = new ArrayList<>();
        mFeatureList = new ArrayList<>();
        listWebsiteItem = new ArrayList<>();
        listPhoneItem = new ArrayList<>();
        mGamingList = new ArrayList<>();
        mBlogList = new ArrayList<>();
        cityList = new ArrayList<>();
        categoryList = new ArrayList<>();
        mediaList = new ArrayList<>();
        video_mime_List = new ArrayList<>();

        video_mime_List.addAll(Constants.getVideoMimeList());

        date = new Date();
    }

    private void initRecycleLayoutManager() {
        rv_gallery.setHasFixedSize(true);
        currentGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "current_media", this, mediaList);
        rv_gallery.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rv_gallery.setAdapter(currentGalleryAdapter);

    }

    private void initViewById(View view) {
        etName = view.findViewById(R.id.etName);
        etEmail = view.findViewById(R.id.etEmail);
        etAddress = view.findViewById(R.id.etAddress);
        etPhone = view.findViewById(R.id.et_phone);
        etWhatsApp = view.findViewById(R.id.etWhatsApp);
        etWebsite = view.findViewById(R.id.et_website);
        etDob = view.findViewById(R.id.etDob);
        spinnerGender = view.findViewById(R.id.spinner_gender);
        imageViewSingle = view.findViewById(R.id.img_single);
        imageViewSingleBrowse = view.findViewById(R.id.img_single_browse);
        spinnerRegion = view.findViewById(R.id.spinner_region);
        spinnerOccupation = view.findViewById(R.id.spinner_occupation);
        spinnerStatus = view.findViewById(R.id.sp_status);
        etBioAboutMe = view.findViewById(R.id.et_bio_about_me);
        imageViewAddGallery = view.findViewById(R.id.img_add_gallery);
        imageViewAddFeature = view.findViewById(R.id.img_add_feature);
        etNicknameKeywordDescription = view.findViewById(R.id.et_nickname_keyword_description);
        radioGroupLocationCategory = view.findViewById(R.id.radio_group_location_category);
        switchCompatPhoneNumberVisibility = view.findViewById(R.id.phone_number_visibilty);
        switchCompatAvailability = view.findViewById(R.id.switch_availability);
        switchCompatOptimisation = view.findViewById(R.id.switch_optimization);
        etMapghanaId = view.findViewById(R.id.etmapghana_id);
        etTwitter = view.findViewById(R.id.et_twitter);
        etInstagram = view.findViewById(R.id.et_instagram);
        etFacebook = view.findViewById(R.id.et_facebook);
        etYoutubeVimeo = view.findViewById(R.id.et_youtube_vimeo);
        etSoundCloud = view.findViewById(R.id.et_sound_cloud);
        etBlog = view.findViewById(R.id.et_blog);
        etGaming = view.findViewById(R.id.et_gaming);
        switchCompatProfileVisibility = view.findViewById(R.id.profile_visibility);
        switchCompatPasswordProtection = view.findViewById(R.id.password_protect);
        rv_gallery = view.findViewById(R.id.rv_gallery);
        frameLayout = view.findViewById(R.id.map_contanier);
        imageAddEmailView = view.findViewById(R.id.img_add_email);
        imageViewAddPhone = view.findViewById(R.id.img_add_phone);
        imageViewAddWebsite = view.findViewById(R.id.img_add_website);
        etPasswordProtected = view.findViewById(R.id.et_password_protect);
        imageViewAddGaming = view.findViewById(R.id.img_add_gaming);
        imageViewAddBlog = view.findViewById(R.id.img_add_blog);

        passWordProtect = view.findViewById(R.id.text_input_layout_password);
        tv_unverify_status = getView().findViewById(R.id.tv_unverify_status);
        spinnerFeature = view.findViewById(R.id.spinner_feature);

        linearLayoutEmailView = view.findViewById(R.id.ll_email_view);
        linearLayoutAddPhoneView = view.findViewById(R.id.ll_addPhoneView);
        linearLayoutAddWebsiteView = view.findViewById(R.id.ll_add_website);
        linearLayoutAdvancedOptimisation = view.findViewById(R.id.ll_advanced_optimisation);
        linearLayoutOptimisationView = view.findViewById(R.id.ll_optimisation);
        linearLayoutAddBlogView = view.findViewById(R.id.ll_blog);
        linearLayoutAddGamingView = view.findViewById(R.id.ll_gaming);
        linearLayoutFeatureView = view.findViewById(R.id.ll_feature_view);


        tvSubmit = view.findViewById(R.id.tvSubmit);

    }

    private void initListener() {

        imageViewAddWebsite.setOnClickListener(this);
        imageAddEmailView.setOnClickListener(this);
        imageViewSingle.setOnClickListener(this);
        imageViewSingleBrowse.setOnClickListener(this);
        imageViewAddPhone.setOnClickListener(this);
        imageViewSingleBrowse.setOnClickListener(this);
        imageViewAddFeature.setOnClickListener(this);
        imageViewAddGallery.setOnClickListener(this);
        imageViewAddGaming.setOnClickListener(this);
        imageViewAddBlog.setOnClickListener(this);

        setStatusSpinner();
        etDob.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);


        switchCompatPhoneNumberVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                phoneNumberVisibility = "Y";
            } else {
                phoneNumberVisibility = "N";
            }
        });

        switchCompatAvailability.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                availablity = "Y";
            } else {
                availablity = "N";
            }
        });

        switchCompatOptimisation.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                optimisation = "Y";
                String userInfo = SessionManager.getUserInfoResponse(getContext());
                if (!userInfo.equals("")) {
                    Gson gson = new Gson();
                    Login login = gson.fromJson(userInfo, Login.class);
                    MapghanaPayment.makePayment(login.getData().getEmail(), login.getData().getUsername(), login.getData().getUsername(), login.getData().getUsername(), 100, getActivity());
                }

            } else {
                optimisation = "N";
            }
        });

        switchCompatProfileVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                profileVisibility = "Y";
            } else {
                profileVisibility = "N";
            }

        });

        switchCompatPasswordProtection.setOnCheckedChangeListener((compoundButton, b) -> {

            if (compoundButton.isChecked()) {
                passWordProtect.setVisibility(View.VISIBLE);
                passwordProtection = "Y";
            } else {
                passwordProtection = "N";
                passWordProtect.setVisibility(View.GONE);
            }
        });

    }

    private void spinnerClickListener() {

        spinnerOccupation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (categoryAdapter != null && categoryList != null && categoryList.size() > 0)
                    categoryAdapter.setDataBean(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (cityAdapter != null && cityList != null && cityList.size() > 0)
                    cityAdapter.setDataBean(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genderAdpater.setSelGender(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (status_adapter != null) {
                    status_adapter.setSelGender(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void addPhoneView() {

        final View phoneView = LayoutInflater.from(getContext()).inflate(R.layout.item_phone_view, null, false);

        final TypefaceEditText editText = phoneView.findViewById(R.id.et_phone);
        ImageView deleteIcon = phoneView.findViewById(R.id.imgRemove);
        //  editText.setText("done");
        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddPhoneView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);
            }
            ((LinearLayout) linearLayoutAddPhoneView.getParent()).removeView(phoneView);
        });
        phoneView.setTag(linearLayoutAddPhoneView.getChildCount());
        linearLayoutAddPhoneView.addView(phoneView);
    }

    private void setPhoneList() {

        listPhoneItem.clear();
        listPhoneItem.add(etPhone.getText().toString());
        int count = linearLayoutAddPhoneView.getChildCount();
        if (linearLayoutAddPhoneView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddPhoneView.getChildAt(i - 1);
                EditText phoneEditText = addView.findViewById(R.id.et_phone);
                String phone = phoneEditText.getText().toString();
                if (!TextUtils.isEmpty(phone)) {
                    listPhoneItem.add(phone);
                }

            }
        }
    }

    private void addEmailSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView etEmail = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutEmailView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutEmailView.getChildCount());
        linearLayoutEmailView.addView(addView);
    }

    private void setEmailList() {

        mEmailList.clear();
        mEmailList.add(etEmail.getText().toString());
        int count = linearLayoutEmailView.getChildCount();
        if (linearLayoutEmailView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutEmailView.getChildAt(i - 1);
                EditText email = addView.findViewById(R.id.etEmail);
                String emailText = email.getText().toString();
                if (!TextUtils.isEmpty(emailText)) {
                    mEmailList.add(emailText);
                }

            }
        }
    }


    private void addWebsiteSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView etwebsite = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddWebsiteView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutAddWebsiteView.getChildCount());
        linearLayoutAddWebsiteView.addView(addView);
    }


    private void setWebsiteList() {

        listWebsiteItem.clear();
        if (!TextUtils.isEmpty(etWebsite.getText().toString())) {
            listWebsiteItem.add(etWebsite.getText().toString());
        } else {
            etWebsite.setError("Please add website");
            etWebsite.setFocusable(true);
            return;
        }

        int count = linearLayoutAddWebsiteView.getChildCount();
        if (linearLayoutAddWebsiteView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddWebsiteView.getChildAt(i - 1);
                EditText websiteEditText = addView.findViewById(R.id.etEmail);
                String websiteText = websiteEditText.getText().toString();
                if (!TextUtils.isEmpty(websiteText)) {
                    listWebsiteItem.add(websiteText);
                } else {
                    websiteEditText.setError("Please add website");
                    websiteEditText.setFocusable(true);
                    return;
                }

            }
        }
    }

    private void addBlogSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView etwebsite = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddBlogView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutAddBlogView.getChildCount());
        linearLayoutAddBlogView.addView(addView);
    }


    private void setBlogSection() {

        mBlogList.clear();
        if (!TextUtils.isEmpty(etBlog.getText().toString())) {
            mBlogList.add(etBlog.getText().toString());
        } else {
            etBlog.setError("Please enter blog");
            etBlog.setFocusable(true);
            return;
        }

        int count = linearLayoutAddBlogView.getChildCount();
        if (linearLayoutAddBlogView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddBlogView.getChildAt(i - 1);
                EditText blogEditText = addView.findViewById(R.id.etEmail);
                String blog = blogEditText.getText().toString();
                if (!TextUtils.isEmpty(blog)) {
                    mBlogList.add(blog);
                } else {
                    blogEditText.setError("Please enter blog");
                    blogEditText.setFocusable(true);
                    return;
                }

            }
        }
    }

    private void addGamingSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView gaming = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddGamingView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutAddGamingView.getChildCount());
        linearLayoutAddGamingView.addView(addView);
    }


    private void setGamingSection() {

        mGamingList.clear();
        if (!TextUtils.isEmpty(etGaming.getText().toString())) {
            mGamingList.add(etGaming.getText().toString());
        } else {
            etGaming.setError("Please enter gaming");
            etGaming.setFocusable(true);
            return;
        }

        int count = linearLayoutAddGamingView.getChildCount();
        if (linearLayoutAddGamingView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddGamingView.getChildAt(i - 1);
                EditText gamingEditText = addView.findViewById(R.id.etEmail);
                String gaming = gamingEditText.getText().toString();
                if (!TextUtils.isEmpty(gaming)) {
                    mGamingList.add(gaming);
                } else {
                    gamingEditText.setError("Please enter gaming");
                    gamingEditText.setFocusable(true);
                    return;
                }

            }
        }
    }

    private void addFeatureSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_feature_view, null);
        Spinner featureSpinner = addView.findViewById(R.id.spinner_feature);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);
        String[] feature = getResources().getStringArray(R.array.feature);
        FeatureAdapter = new SimpleSpinnerAdapter(getContext(), feature);
        FeatureAdapter.setSelGender(0);
        featureSpinner.setAdapter(FeatureAdapter);
        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutFeatureView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutFeatureView.getChildCount());
        linearLayoutFeatureView.addView(addView);
    }


    private void setFeatureSection() {

        mFeatureList.clear();
        if (!TextUtils.isEmpty(getFeatureItem())) {
            mFeatureList.add(getFeatureItem());
        } else {
            displayToast("please select feature");
            return;
        }

        int count = linearLayoutFeatureView.getChildCount();
        if (linearLayoutFeatureView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutFeatureView.getChildAt(i - 1);
                Spinner spinnerFeature = addView.findViewById(R.id.spinner_feature);
                String feature = spinnerFeature.getSelectedItem().toString();
                if (!TextUtils.isEmpty(feature)) {
                    mFeatureList.add(feature);
                } else {
                    displayToast("Please select feature");
                    return;
                }

            }
        }
    }


    private void setStatusSpinner() {

        String role = SessionManager.getRole(getContext());
        if (role != null && role.length() > 0 && role.equalsIgnoreCase("user")) {
            spinnerStatus.setVisibility(View.GONE);

        } else {

            spinnerStatus.setTag("admin");
            spinnerStatus.setVisibility(View.VISIBLE);
            String[] strings = getResources().getStringArray(R.array.status_array2);
            status_adapter = new SimpleSpinnerAdapter(getContext(), strings);
            status_adapter.setSelGender(0);
            spinnerStatus.setAdapter(status_adapter);
        }

    }

    private String getStatus() {
        String status;
        int position = spinnerStatus.getSelectedItemPosition();
        if (position == 0) return null;

        if (position == 1) {

            status = "unverified";
            return status;
        } else if (position == 2) {
            status = "verified";
            return status;

        }
        return null;
    }

    private void getCity() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).city();
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private void getCategory() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).category(Constants.individuals);
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private String getGenderItem() {
        int position = spinnerGender.getSelectedItemPosition();
        if (position == 0) return null;
        String[] array = getContext().getResources().getStringArray(R.array.gender_array);
        for (int i = 0; i < array.length; i++) {
            if (position == i) {
                return array[i];
            }
        }
        return null;
    }

    private String getFeatureItem() {
        int position = spinnerFeature.getSelectedItemPosition();
        if (position == 0) return null;
        String[] array = getContext().getResources().getStringArray(R.array.features_array);
        for (int i = 0; i < array.length; i++) {
            if (position == i) {
                return array[i];
            }
        }
        return null;
    }

    private String getItemCategory() {
        int position = spinnerOccupation.getSelectedItemPosition();
        if (position == 0) return null;
        for (int i = 0; i < categoryList.size(); i++) {
            if (position == i) {
                return String.valueOf(categoryList.get(position).getId());
            }
        }
        return null;
    }

    private String getItemCity() {
        int position = spinnerRegion.getSelectedItemPosition();
        if (position == 0) return null;
        for (int i = 0; i < cityList.size(); i++) {
            if (position == i) {
                return String.valueOf(cityList.get(position).getId());
            }
        }
        return null;
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_add_email:
                hideKeyboard();
                addEmailSection();
                break;
            case R.id.img_add_website:
                hideKeyboard();
                addWebsiteSection();
                break;
            case R.id.img_add_feature:
                hideKeyboard();
                addFeatureSection();
                //pickContacts();
                break;
            case R.id.img_add_phone:
                addPhoneView();
                break;
            case R.id.img_single_browse:
                hideKeyboard();
                oneImageSelector();
                break;
            case R.id.img_add_gallery:
                hideKeyboard();
                multipleMediaSelector();
                break;
            case R.id.tvSubmit:
                hideKeyboard();
                try {
                    onSubmit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.img_add_blog:
                addBlogSection();
                break;
            case R.id.img_add_gaming:
                addGamingSection();
                break;
            case R.id.etDob:
                hideKeyboard();
                onDobDialog();
                break;


        }
    }

    private void showHintPopmenu() {

        /*popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });*/
        popupMenu.show();
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        if (dialog != null) {
            dialog.create(getContext(), true).callback(this).show();
        }
    }

    private void pickContacts() {
        Intent intent = new Intent(getContext(), ContactPickerActivity.class)
                // .putExtra(ContactPickerActivity.EXTRA_THEME, mDarkTheme ? R.style.Theme_Dark : R.style.Theme_Light)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_BADGE_TYPE, ContactPictureType.ROUND.name())
                .putExtra(ContactPickerActivity.EXTRA_SHOW_CHECK_ALL, true)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION, ContactDescription.ADDRESS.name())
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_SORT_ORDER, ContactSortOrder.AUTOMATIC.name());
        startActivityForResult(intent, REQUEST_CONTACT);
    }

    private void oneImageSelector() {
        CropImage.activity()
                .start(getContext(), this);
    }


    private void multipleMediaSelector() {
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CONTACT && resultCode == Activity.RESULT_OK &&
                data != null && data.hasExtra(ContactPickerActivity.RESULT_CONTACT_DATA)) {
            List<String> selPhoneList = new ArrayList<>();
            List<Contact> contacts = (List<Contact>) data.getSerializableExtra(ContactPickerActivity.RESULT_CONTACT_DATA);
            for (Contact contact : contacts) {
                contact.getPhone(0);
                if (contact.getPhone(0) != null && contact.getPhone(0).trim().length() != 0) {
                    displayLog(TAG, "onActivityResult: 1");
                    selPhoneList.add(contact.getPhone(0));
                } else if (contact.getPhone(1) != null && contact.getPhone(1).trim().length() != 0) {
                    displayLog(TAG, "onActivityResult: 2");
                    selPhoneList.add(contact.getPhone(1));
                } else if (contact.getPhone(2) != null && contact.getPhone(2).trim().length() != 0) {
                    displayLog(TAG, "onActivityResult: 3");
                    selPhoneList.add(contact.getPhone(2));
                } else {
                    displayLog(TAG, "onActivityResult: no contact found");
                    displayToast("no contact found");
                }

                displayLog(TAG, "phone: " + contact.getPhone(0));
                displayLog(TAG, "email: " + contact.getPhone(1));
                displayLog(TAG, "address: " + contact.getPhone(2));
            }
        }


        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            List<Uri> uris = Matisse.obtainResult(data);
            List<String> paths = Matisse.obtainPathResult(data);
            FilePathHelper filePathHelper = new FilePathHelper();
            String mimeType = filePathHelper.getMimeType(paths.get(0));
            if (paths != null && paths.size() > 0) {
                mediaList.addAll(paths);
                refreshCurrentGalleryAdapter();
            }
        }

        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        uriOneImg = result.getUri();
                        imageViewSingle.setImageURI(uriOneImg);
                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                        displayErrorDialog("Error", error.getMessage());
                        displayLog(TAG, "onActivityResult EXCEp: " + error.getMessage());
                    }
                }
                break;
        }

        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                linearLayoutOptimisationView.setVisibility(View.VISIBLE);
                switchCompatOptimisation.setChecked(true);
                Toast.makeText(getActivity(), "SUCCESS " + message, Toast.LENGTH_SHORT).show();
            } else if (resultCode == RavePayActivity.RESULT_ERROR) {
                switchCompatOptimisation.setChecked(false);
                linearLayoutOptimisationView.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "ERROR " + message, Toast.LENGTH_SHORT).show();
            } else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                switchCompatOptimisation.setChecked(false);
                linearLayoutOptimisationView.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "CANCELLED " + message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void refreshCurrentGalleryAdapter() {
        if (mediaList != null) {
            currentGalleryAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAdapterClickListener(int position) {
        for (int i = 0; i < mediaList.size(); i++) {
            if (mediaList.get(position).equalsIgnoreCase(mediaList.get(i))) {
                mediaList.remove(i);
            }
        }
    }

    private long getDuration(String path) {
        File videoFile = new File(path);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        //use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(getContext(), Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);

        retriever.release();
        return timeInMillisec;

    }


    private void onSubmit() throws Exception {
        if (ConnectionDetector.isNetAvail(getContext())) {
            setEmailList();
            setPhoneList();
            setWebsiteList();
            setBlogSection();
            setGamingSection();
            setFeatureSection();

            HashMap<String, String> map = new HashMap<>();


            String name = etName.getText().toString();
            if (TextUtils.isEmpty(name)) {
                etName.setError("Please enter title");
                etName.setFocusable(true);
                return;
            }

            String address = etAddress.getText().toString();
            if (TextUtils.isEmpty(address)) {
                etAddress.setError("Please enter address");
                etAddress.setFocusable(true);
                return;
            }

            String whatsApp = etWhatsApp.getText().toString();
            if (TextUtils.isEmpty(whatsApp)) {
                etWhatsApp.setError("Please enter whatsApp account detail");
                etWhatsApp.setFocusable(true);
                return;
            }

            String mDob = etDob.getText().toString();
            if (TextUtils.isEmpty(mDob)) {
                displayToast("Please enter date of birth");
                return;
            }

            String gender = getGenderItem().toLowerCase();
            if (TextUtils.isEmpty(gender)) {
                displayToast("Please select gender");
                return;
            }

            if (uriOneImg == null) {
                displayToast("Select Profile Pic");
                return;
            }

            String city = getItemCity().toLowerCase();
            if (TextUtils.isEmpty(city)) {
                displayToast("Please select city");
                return;
            }

            String occupation = getItemCategory().toLowerCase();
            if (TextUtils.isEmpty(occupation)) {
                displayToast("Please select occupation");
                return;
            }

            String bio = etBioAboutMe.getText().toString();
            if (TextUtils.isEmpty(bio)) {
                etBioAboutMe.setError("Please enter bio");
                etBioAboutMe.setFocusable(true);
                return;
            }

            if (mediaList != null && mediaList.size() > 0) {
                String path = "";
                for (int i = 0; i < mediaList.size(); i++) {
                    path = mediaList.get(i);
                    FilePathHelper filePathHelper = new FilePathHelper();
                    final String mimeType = filePathHelper.getMimeType(path);
                    displayLog(TAG, "mimeType: " + mimeType);
                    if (video_mime_List.contains(mimeType)) {
                        if (getDuration(path) > VIDEO_MAXDURATION) {
                            displayToast("Video duration should be of 30 second.");
                            return;
                        }
                    }
                }
            } else {
                displayToast("Please select gallery");
                return;
            }

            int ID = 0;
            Gson gson = new Gson();
            JsonArray phoneArray = null;
            JsonArray emailArray = null;
            JsonArray websiteArray = null;
            JsonArray featureArray = null;
            JsonArray blogArray = null;
            JsonArray gamingArray = null;

            String userInfo = SessionManager.getUserInfoResponse(getContext());
            if (!userInfo.equals("")) {
                Login login = gson.fromJson(userInfo, Login.class);
                ID = login.getData().getId();
            }


            double latitude = 0;
            double longitude = 0;
            if (getArguments() != null) {
                latitude = getArguments().getDouble("latitude");
                longitude = getArguments().getDouble("longitude");
            }
            map.put(BaseArguments.user_id, String.valueOf(ID));
            map.put(BaseArguments.post_for, Constants.individuals);
            map.put(BaseArguments.name, name);

            JsonElement elementEmail = gson.toJsonTree(mEmailList, new TypeToken<List<String>>() {
            }.getType());

            if (!elementEmail.isJsonArray()) {
                throw new Exception();
            }

            emailArray = elementEmail.getAsJsonArray();
            map.put(BaseArguments.email, emailArray.toString());

            map.put(BaseArguments.address, address);

            JsonElement elementPhone = gson.toJsonTree(listPhoneItem, new TypeToken<List<String>>() {
            }.getType());

            if (!elementPhone.isJsonArray()) {
                throw new Exception();
            }

            phoneArray = elementPhone.getAsJsonArray();
            map.put(BaseArguments.phone, phoneArray.toString());
            map.put(BaseArguments.whatsApp, whatsApp);

            JsonElement elementWebsite = gson.toJsonTree(listWebsiteItem, new TypeToken<List<String>>() {
            }.getType());

            if (!elementWebsite.isJsonArray()) {
                throw new Exception();
            }

            websiteArray = elementWebsite.getAsJsonArray();
            map.put(BaseArguments.website, websiteArray.toString());
            map.put(BaseArguments.dob, DOB);

            MultipartBody.Part signleImgPart = RetrofitUtils.
                    createFilePart(BaseArguments.image, uriOneImg.getPath(),
                            RetrofitUtils.MEDIA_TYPE_IMAGE_PNG);

            map.put(BaseArguments.city_id, getItemCity().toLowerCase());
            map.put(BaseArguments.category_id, getItemCategory());
            map.put(BaseArguments.gender, getGenderItem().toLowerCase());
            map.put(BaseArguments.type, "");
            if (tv_unverify_status.getVisibility() == View.VISIBLE) {
                String status = tv_unverify_status.getText().toString().trim();
                map.put(BaseArguments.status, status);
            }
            if (tv_unverify_status.getVisibility() != View.VISIBLE &&
                    getStatus() == null) {
                displayToast("Select Status");
                return;
            } else {
                map.put(BaseArguments.status, getStatus());
            }

            map.put(BaseArguments.notes, bio);
            MultipartBody.Part[] multiImgPart = new MultipartBody.Part[mediaList.size()];
            if (mediaList != null && mediaList.size() > 0) {
                for (int i = 0; i < mediaList.size(); i++) {
                    String url = mediaList.get(0);
                    FilePathHelper filePathHelper = new FilePathHelper();
                    FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
                    String mimeType = fileInformation.getMimeType();
                    if (video_mime_List.contains(mimeType)) {
                        multiImgPart[i] = RetrofitUtils.createFilePart(BaseArguments.gallery_images + "[" + i + "]",
                                mediaList.get(i), RetrofitUtils.MEDIA_TYPE_VIDEO);
                    } else {
                        multiImgPart[i] = RetrofitUtils.createFilePart(BaseArguments.gallery_images + "[" + i + "]",
                                mediaList.get(i), RetrofitUtils.MEDIA_TYPE_IMAGE_PNG);
                    }
                }

            }

            JsonElement elementFeature = gson.toJsonTree(mFeatureList, new TypeToken<List<String>>() {
            }.getType());

            if (!elementFeature.isJsonArray()) {
                throw new Exception();
            }
            featureArray = elementFeature.getAsJsonArray();
            map.put(BaseArguments.features, featureArray.toString());
            map.put(BaseArguments.tags, etNicknameKeywordDescription.getText().toString().trim());

            map.put(BaseArguments.lat, String.valueOf(latitude));
            map.put(BaseArguments.log, String.valueOf(longitude));
            map.put(BaseArguments.location_profile, locationProfile);
            map.put(BaseArguments.phone_visibility, phoneNumberVisibility);
            map.put(BaseArguments.availability, availablity);
            map.put(BaseArguments.optimization, optimisation);
            map.put(BaseArguments.mapghanaid, etMapghanaId.getText().toString());
            map.put(BaseArguments.twitter, etTwitter.getText().toString());
            map.put(BaseArguments.instagram, etInstagram.getText().toString());
            map.put(BaseArguments.facebook, etFacebook.getText().toString());
            map.put(BaseArguments.youtube, etYoutubeVimeo.getText().toString());
            map.put(BaseArguments.soundclud, etSoundCloud.getText().toString());
            JsonElement elementblog = gson.toJsonTree(mBlogList, new TypeToken<List<String>>() {
            }.getType());

            if (!elementblog.isJsonArray()) {
                throw new Exception();
            }

            blogArray = elementblog.getAsJsonArray();
            map.put(BaseArguments.blog, blogArray.toString());

            JsonElement elementGaming = gson.toJsonTree(mGamingList, new TypeToken<List<String>>() {
            }.getType());

            if (!elementGaming.isJsonArray()) {
                throw new Exception();
            }

            gamingArray = elementGaming.getAsJsonArray();
            map.put(BaseArguments.gaming, gamingArray.toString());

            map.put(BaseArguments.profile_visibility, profileVisibility);
            map.put(BaseArguments.password_protected, passwordProtection);
            map.put(BaseArguments.password, etPasswordProtected.getText().toString());
            displayProgressBar(false);
            restClient.post_individual(getContext(), RetrofitUtils.createMultipartRequest(map), signleImgPart, multiImgPart);
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            this.date = date;
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            DOB = format.format(date);
            etDob.setText(DOB);
        }
    }

    public static boolean dobdateValidate(Date date) {
        try {
            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.YEAR, -12);
            if (date.before(c2.getTime())) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            //  displayLog(TAG, "dobdateValidate: " );
        }
        return false;
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_city) {
                dismissProgressBar();
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    City city = gson.fromJson(s, City.class);
                    if (city.getStatus() != 0) {
                        City.DataBean city1 = new City.DataBean();
                        city1.setName("Select Regions");
                        cityList.add(0, city1);
                        cityList.addAll(city.getData());
                        cityAdapter = new CityAdapter(getContext(), cityList);
                        cityAdapter.setDataBean(0);
                        spinnerRegion.setAdapter(cityAdapter);
                        getCategory();
                    } else {
                        displayErrorDialog("Error", city.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_CATEGORY) {
                dismissProgressBar();
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Category category = gson.fromJson(s, Category.class);
                    if (category.getStatus() != 0) {
                        Category.DataBean bean = new Category.DataBean();
                        bean.setName("Select Occupation");
                        categoryList.add(bean);
                        categoryList.addAll(category.getData());
                        categoryAdapter = new CategoryAdapter(getContext(), categoryList);
                        categoryAdapter.setDataBean(0);
                        spinnerOccupation.setAdapter(categoryAdapter);
                    } else {
                        displayErrorDialog("Error", category.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_PostINDIVIDUAL) {
                dismissProgressBar();
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    CreateIndividualPost posting = gson.fromJson(s, CreateIndividualPost.class);
                    if (posting.getStatus() != 0) {
                        displayToast(posting.getMessage());
                        linearLayoutAdvancedOptimisation.setVisibility(View.VISIBLE);
                        ((DashboardActivity) getActivity()).clearFragmentBackStack();
                    } else {
                        displayErrorDialog("Error", posting.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            dismissProgressBar();
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }


    private void initMap() {
        if (getActivity() != null) {
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnInfoWindowClickListener(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());
        }
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        return false;
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(new LatLng(lat, log));
        ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.ic_launcher, new LatLng(lat, log),
                Constants.You, "");
    }

    @Override
    public void onAddClick(String type) {
        multipleMediaSelector();
    }

    @Override
    public void onDeleteClick(int position, String type) {

    }
}
