package com.mapghana.app.model;

import java.util.ArrayList;

public class SectionDataModel {

    private String headerTitle;
    private ArrayList<EventDetail> allItemsInSection;


    public SectionDataModel() {

    }
    public SectionDataModel(String headerTitle, ArrayList<EventDetail> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }



    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<EventDetail> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<EventDetail> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }


}