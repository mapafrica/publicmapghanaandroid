package com.mapghana.app.ui.activity.mainactivity.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Login;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.mainactivity.fragments.signup.SignUpFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceCheckBox;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Valiations;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends AppBaseFragment {

    private static final String TAG = "LoginFragment";
    private EditText etEmail, etPassword;
    private TypefaceCheckBox chkRemember;
    private static final int GOO_SIGN_IN = 9001;
    private static final int FB_SIGN_IN = 9001;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private SignInButton signInButton;
    private LoginButton loginButton;
    private CallbackManager callbackManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_login;
    }

    @Override
    public void initializeComponent() {
        getToolBar().setToolbarVisibilityTB(false);
        LinearLayout llCreateAcc;
        TypefaceTextView tvForgotPass, tvLogin;
        displayLog(TAG, "initializeComponent: ");
        llCreateAcc = getView().findViewById(R.id.llCreateAcc);
        tvForgotPass = getView().findViewById(R.id.tvForgotPass);
        tvLogin = getView().findViewById(R.id.tvLogin);
        signInButton = getView().findViewById(R.id.sign_in_button);
        loginButton = getView().findViewById(R.id.login_button);
        etEmail = getView().findViewById(R.id.etEmail);
        etPassword = getView().findViewById(R.id.etPassword);
        chkRemember = getView().findViewById(R.id.chkRemember);
        llCreateAcc.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
        tvLogin.setOnClickListener(this);

        /*google signIn initialize*/
        initGoogleSignIn();
        initFacebookLogin();
    }


    private void initGoogleSignIn() {

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

        mAuth = FirebaseAuth.getInstance();

        signInButton.setOnClickListener(v -> signIn());
    }

    private void initFacebookLogin() {

        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;

        if (!loggedOut) {
            // Picasso.with(this).load(Profile.getCurrentProfile().getProfilePictureUri(200, 200)).into(imageView);
            Log.d("TAG", "Username is: " + Profile.getCurrentProfile().getName());

            //Using Graph API
            getUserProfile(AccessToken.getCurrentAccessToken());
        }

        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // MapGhanaApplication code
                //loginResult.getAccessToken();
                //loginResult.getRecentlyDeniedPermissions()
                //loginResult.getRecentlyGrantedPermissions()
                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                Log.d("API123", loggedIn + " ??");

            }

            @Override
            public void onCancel() {
                // MapGhanaApplication code
            }

            @Override
            public void onError(FacebookException exception) {
                // MapGhanaApplication code
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOO_SIGN_IN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llCreateAcc:
                ((AppBaseActivity) getActivity()).changeFragment(new SignUpFragment(), true,
                        false, 0, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                        true);

                break;
            case R.id.tvForgotPass:
                ((AppBaseActivity) getActivity()).changeFragment(new ForgotPasswordFragment(), true,
                        false, 0, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.tvLogin:
                hideKeyboard();
                onLogin();
                break;
        }
    }

    private void onLogin() {
        if (Valiations.isEmailAddress(etEmail, true) && Valiations.hasText(etPassword)) {


            if (ConnectionDetector.isNetAvail(getContext())) {
                displayProgressBar(false);
                RestClient restClient = new RestClient(getContext());
                restClient.callback(this).login(etEmail.getText().toString().trim(),
                        etPassword.getText().toString().trim(), getContext());
            } else {
                displayToast(Constants.No_Internet);
            }
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_LOGIN) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Login login = gson.fromJson(s, Login.class);
                    if (login != null) {
                        if (login.getStatus() != 0) {
                            displayToast(login.getMessage());
                            if (chkRemember.isChecked()) {
                                SessionManager.setUserInfoResponse(getContext(), s, true);
                            } else {
                                SessionManager.setUserInfoResponse(getContext(), s, false);
                            }
                            SessionManager.saveUname(getContext(), login.getData().getName());
                            SessionManager.saveRole(getContext(), login.getData().getRole());
                            SessionManager.saveAddress(getContext(), login.getData().getLocation());
                            SessionManager.saveProfileImage(getContext(), login.getData().getImage());
                            ((AppBaseActivity) getActivity()).StartActivityWithFinish(DashboardActivity.class, new Bundle());
                            clearBackStack(0);
                        } else {
                            displayErrorDialog("Error", login.getError());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }else if (apiId == ApiIds.ID_SIGNUP) {
                ((AppBaseActivity) getActivity()).StartActivityWithFinish(DashboardActivity.class, new Bundle());
                clearBackStack(0);
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOO_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        } else if (requestCode == FB_SIGN_IN) {

            callbackManager.onActivityResult(requestCode, resultCode, data);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        displayProgressBar(true);
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        Toast.makeText(getActivity(), "Login Failed: ", Toast.LENGTH_SHORT).show();
                    }

                    dismissProgressBar();
                });
    }

    private void updateUI(FirebaseUser user) {
        dismissProgressBar();
        if (user != null) {

            if (ConnectionDetector.isNetAvail(getContext())) {
                displayProgressBar(false);
                RestClient restClient = new RestClient(getContext());
                restClient.callback(this).newSignup(user.getDisplayName(), user.getDisplayName(), user.getEmail(), user.getPhoneNumber(), getContext());
                SessionManager.saveUname(getContext(), user.getDisplayName());
                SessionManager.saveProfileImage(getContext(), user.getPhotoUrl().toString());


            } else {
                displayToast(Constants.No_Internet);
            }
        } else {
            displayToast(Constants.SOMETHING_WENT_WRONG);
        }
    }

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(getActivity(),
                task -> updateUI(null));
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, (object, response) -> {
                    Log.d("TAG", object.toString());
                    try {
                        String first_name = object.getString("first_name");
                        String last_name = object.getString("last_name");
                        String email = object.getString("email");
                        String id = object.getString("id");
                        String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

}
