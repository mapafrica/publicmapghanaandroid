package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.City;
import com.mapghana.app.model.FilterModel;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CityAdapter;
import com.mapghana.app.utils.Constants;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationFilterFragment extends AppBaseFragment implements AdapterView.OnItemSelectedListener {

    private Spinner spLoc;
    private RestClient restClient;
    private ArrayList<City.DataBean> cityList;
    private static OnCitySelectedListener onCitySelectedListeners;
    private static final String TAG = "LocationFilterFragment";
    CityAdapter cityAdapter;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_location_filter;
    }

    @Override
    public void initializeComponent() {
        restClient = new RestClient(getContext());
        spLoc = (Spinner) getView().findViewById(R.id.spLoc);
        cityList = new ArrayList<>();

        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).city();
        } else {
            displayToast(Constants.No_Internet);
        }
        spLoc.setOnItemSelectedListener(this);
    }

    private String getItemCity() {
        int position = spLoc.getSelectedItemPosition();
        for (int i = 0; i < cityList.size(); i++) {
            if (position == i) {
                //  displayLog(TAG, "getItemCategory: "+ cityList.get(position).getId());
                return String.valueOf(cityList.get(position).getId());
            }
        }
        return null;
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_city) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    City city = gson.fromJson(s, City.class);
                    if (city.getStatus() != 0) {
                        City.DataBean city1 = new City.DataBean();
                        city1.setName("Select Regions");
                        cityList.add(0, city1);
                        cityList.addAll(city.getData());

                        cityAdapter = new CityAdapter(getContext(), cityList);
                        cityAdapter.setDataBean(0);
                        spLoc.setAdapter(cityAdapter);
                        setSelectedCity();
                    } else {
                        displayErrorDialog("Error", city.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void setSelectedCity() {
        if (cityList != null && cityList.size() > 0) {
            String s = DefaulFilterSpf.getFilter(getContext());
            Gson gson = new Gson();
            FilterModel filterModel = gson.fromJson(s, FilterModel.class);


            if (filterModel != null && filterModel.getCity_id() != null && !filterModel.getCity_id().equals("")) {
                int city_id = Integer.parseInt(filterModel.getCity_id());
                for (int i = 0; i < cityList.size(); i++) {
                    if (cityList.get(i).getId() == city_id) {
                        spLoc.setSelection(i);
                        cityAdapter.setDataBean(i);
                    }
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (cityAdapter == null || cityList == null || cityList.size() == 0) return;

        cityAdapter.setDataBean(position);

        if (!cityList.get(position).getName().equals(Constants.Select_City)) {
            onCitySelectedListeners.onCitySelectedListener(String.valueOf(cityList.get(position).getId()));

        } else {
            displayLog(TAG, "onItemSelected: no Regions selected ");
            onCitySelectedListeners.onCitySelectedListener(null);

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface OnCitySelectedListener {
        void onCitySelectedListener(String city_id);
    }

   /* public void setOnCitySelectedListener(OnCitySelectedListener onCitySelectedListener) {
        this.onCitySelectedListener = onCitySelectedListener;
    }*/

    public static LocationFilterFragment newInstance(OnCitySelectedListener onCitySelectedListener) {
        onCitySelectedListeners = onCitySelectedListener;
        Bundle args = new Bundle();

        LocationFilterFragment fragment = new LocationFilterFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
