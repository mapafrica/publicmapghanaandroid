package com.mapghana.app.retrofit;

import android.content.Context;
import android.provider.Settings;

import com.google.gson.Gson;
import com.mapghana.app.model.Login;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.RestService;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.utils.Constants;
import com.mapghana.rest.ApiHitListener;
import com.mapghana.rest.BaseRestClient;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ububtu on 13/7/16.
 */
public class RestClient extends BaseRestClient {
    private static final String TAG = "RestClient";
    ApiHitListener apiHitListener;
    public static int API_COUNT = 0;
    private Rest api;

    public RestClient(Context _context) {
        super(_context);
    }

    public RestClient callback(ApiHitListener apiHitListener) {
        this.apiHitListener = apiHitListener;
        return this;
    }

    private Rest getApi() {
        if (api == null) {
            api = RestService.getService();
        }

        return api;
    }

    private Rest getApiGeoCoding() {
        if (api == null) {
            api = RestService.getServiceGeoCoder();
        }


        return api;
    }

    private String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public void signup(String username, String full_name, String password, String email,
                       String dob, String gender, String phone_number,
                       String location, Context context) {
        Call<ResponseBody> call = getApi().signup(username, full_name, password, email,
                dob, gender, phone_number, location, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_SIGNUP, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_SIGNUP, t.getMessage());
            }
        });
    }

    public void newSignup(String username, String full_name, String email,
                        String phone_number,
                       Context context) {
        Call<ResponseBody> call = getApi().newSignUp(username, full_name, email, phone_number, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_SIGNUP, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_SIGNUP, t.getMessage());
            }
        });
    }

    public void login(String email, String password, Context context) {

        Call<ResponseBody> call = getApi().login(email, password, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_LOGIN, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_LOGIN, t.getMessage());
            }
        });
    }

    public void forgot(String email) {
        Call<ResponseBody> call = getApi().forgot(email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_FORGOT, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_FORGOT, t.getMessage());
            }
        });
    }

    public void getProfile(int id, Context context) {
        API_COUNT++;
        Call<ResponseBody> call = getApi().getProfile(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_GETPROFILE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;
                apiHitListener.onFailResponse(ApiIds.ID_GETPROFILE, t.getMessage());
            }
        });
    }


    public void updateProfile(int id, String username,
                              String email, String dob, String gender,
                              String phone_number, String location,
                              Context context) {
        API_COUNT++;
        Call<ResponseBody> call = getApi().updateProfile(id, username, username, email, dob, gender, phone_number, location, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_UPDATEPROFILE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_UPDATEPROFILE, t.getMessage());
            }
        });
    }

    public void updateProfileWithImage(HashMap<String, RequestBody> map, MultipartBody.Part image) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().updateProfileWithImage(map, image);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_UPDATEPROFILE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_UPDATEPROFILE, t.getMessage());
            }
        });
    }

    public void category(String type) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().category(type);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_CATEGORY, t.getMessage());
            }
        });
    }

    public void category() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().category();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;
                apiHitListener.onFailResponse(ApiIds.ID_CATEGORY, t.getMessage());
            }
        });
    }

    public void city() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().city();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_city, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_city, t.getMessage());
            }
        });
    }


    public void post_individual(Context context, HashMap<String, RequestBody> map, MultipartBody.Part image, MultipartBody.Part[] gallery_images) {
        Call<ResponseBody> call = getApi().post_individual(map, image, gallery_images);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_PostINDIVIDUAL, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PostINDIVIDUAL, t.getMessage());
            }
        });
    }

    public void postAddObject(Context context, HashMap<String, RequestBody> map, MultipartBody.Part[] gallery_images) {
        Call<ResponseBody> call = getApi().postAddObject(map, gallery_images);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_PostINDIVIDUAL, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PostINDIVIDUAL, t.getMessage());
            }
        });
    }

    public void post_individual(Context context, HashMap<String, RequestBody> map, MultipartBody.Part image) {
        Call<ResponseBody> call = getApi().post_individual(map, image);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_PostINDIVIDUAL, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PostINDIVIDUAL, t.getMessage());
            }
        });
    }

    public void post_organization(Context context, HashMap<String, RequestBody> map, MultipartBody.Part[] gallery_images) {
        Call<ResponseBody> call = getApi().post_organization(map, gallery_images);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_PostOrganization, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PostOrganization, t.getMessage());
            }
        });
    }

    public void postAddEvent(Context context, HashMap<String, RequestBody> map, MultipartBody.Part[] previousgallery,MultipartBody.Part[] currentGallery,MultipartBody.Part[] posterMedia) {
        Call<ResponseBody> call = getApi().postAddEventApi(map, previousgallery,currentGallery,posterMedia);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_ADD_EVENT, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_ADD_EVENT, t.getMessage());
            }
        });
    }

    public void contact() {
        Call<ResponseBody> call = getApi().contact();
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_contact, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_contact, t.getMessage());
            }
        });
    }

    public void contactQuery(String name, String email, String phone, String message) {
        Call<ResponseBody> call = getApi().contact(name, email, phone, message);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_contact_query, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_contact_query, t.getMessage());
            }
        });
    }

    public void logout(Context context) {
        Gson gson = new Gson();
        API_COUNT++;

        String userInfo = SessionManager.getUserInfoResponse(context);
        Login login = gson.fromJson(userInfo, Login.class);
        Call<ResponseBody> call = getApi().logout(login.getData().getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_logout, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_logout, t.getMessage());
            }
        });
    }

    public void post_listing_organizationsss(String lat, String log) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_organizations(lat, log, "6");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_organization, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_organization, t.getMessage());
            }
        });
    }

    public void post_listing_organizations(String sub_category_id) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_organizations(sub_category_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_organization, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_organization, t.getMessage());
            }
        });
    }

    public void post_send_review(HashMap<String, RequestBody> map) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_send_review(map);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;


                apiHitListener.onSuccessResponse(ApiIds.ID_post_send_review, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_post_send_review, t.getMessage());
            }
        });
    }

    public void post_listing_Individualsss(String lat, String log) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_Individuals(lat, log, "6");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_individual, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_individual, t.getMessage());
            }
        });
    }

    public void post_listing_Individuals(String category_id) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_Individuals(category_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_individual, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_individual, t.getMessage());
            }
        });
    }

    public void getDetailsOfOnePost(String post_id) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().getDetailsOfOnePost(post_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Details_1, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Details_1, t.getMessage());
            }
        });
    }


    public Call<ResponseBody> search(String keyword) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().search(keyword);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_SEarch, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_SEarch, t.getMessage());
            }
        });
        return call;
    }

    public void searchFilter(String keyword, String category_id, String city_id,
                             String lat, String log, String distance) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().searchFilter(keyword, category_id, city_id, lat, log, distance);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_SEarch_Filter, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_SEarch_Filter, t.getMessage());
            }
        });
    }

    public void ads() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().ads();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_ADS, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_ADS, t.getMessage());
            }
        });
    }

    public void getEventListing() {
        Call<ResponseBody> call = getApi().getEventListing();
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_LISTING, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_LISTING, t.getMessage());
            }
        });
    }
}