package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;

/**
 * Created by ubuntu on 19/4/18.
 */

public class DialPhoneAdapter extends BaseRecycleAdapter {

    private String[] phone_array;
    private DialPhoneListener dialPhoneListener;

    public DialPhoneAdapter(String[] phone_array, DialPhoneListener dialPhoneListener) {
        this.phone_array = phone_array;
        this.dialPhoneListener = dialPhoneListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_dial_phone;
    }

    @Override
    protected int getItemSize() {
        if (phone_array != null && phone_array.length > 0) {
            return phone_array.length;
        }
        return 0;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder {
        private TextView tv_phone, tv_dial;
        private LinearLayout ll_dial;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_phone =  itemView.findViewById(R.id.tv_phone);
            tv_dial =  itemView.findViewById(R.id.tv_dial);
            ll_dial = itemView.findViewById(R.id.ll_dial);
        }

        @Override
        public void setData(int position) {
            String phone = phone_array[position];
            if (phone != null && phone.length() > 0)
                tv_phone.setText(phone);
            ll_dial.setTag(position);
            ll_dial.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            switch (v.getId()){
                case R.id.ll_dial:
                    if (dialPhoneListener!=null)
                    dialPhoneListener.dialPhoneListener((Integer)ll_dial.getTag());
                    break;

            }
        }
    }

    public interface DialPhoneListener{
        void dialPhoneListener(int position);
    }
}
