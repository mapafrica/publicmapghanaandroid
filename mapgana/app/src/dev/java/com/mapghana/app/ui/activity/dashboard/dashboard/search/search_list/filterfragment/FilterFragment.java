package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment;


import android.support.v4.app.Fragment;
import android.view.View;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.FilterModel;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter.AreaRangeFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter.CategoryFilterFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter.LocationFilterFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends AppBaseFragment
        implements LocationFilterFragment.OnCitySelectedListener,
        AreaRangeFragment.OnRangeChangeListener, CategoryFilterFragment.OnCategorySelectedListener {

    private static final String TAG = "FilterFragment";
    private TypefaceTextView tvCat, tvLoc, tvAreaRange;
    private String CITY_ID;
    private String DISTANCE;
    private String CATEGORY_ID;
    private int selectedFilters;
    private FilterModel filterModel;

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_filter;
    }

    @Override
    public void initializeComponent() {
        displayLog(TAG, "initializeComponent: ");
        init();
        TypefaceTextView tvApplyFilter, tvClearAll;
        tvLoc = (TypefaceTextView) getView().findViewById(R.id.tvLoc);
        tvCat = (TypefaceTextView) getView().findViewById(R.id.tvCat);
        tvAreaRange = (TypefaceTextView) getView().findViewById(R.id.tvAreaRange);
        tvApplyFilter = (TypefaceTextView) getView().findViewById(R.id.tvApplyFilter);
        tvClearAll = (TypefaceTextView) getView().findViewById(R.id.tvClearAll);
        updateUI(tvCat);
        changeChildFragment(new CategoryFilterFragment().newInstance(this), false, false,
                0, R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim);

        tvApplyFilter.setOnClickListener(this);
        tvClearAll.setOnClickListener(this);
        tvLoc.setOnClickListener(this);
        tvAreaRange.setOnClickListener(this);
        tvCat.setOnClickListener(this);

    }

    private void init() {
        displayLog(TAG, "init: ");
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Filter));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCat:
                updateUI(tvCat);
                changeChildFragment(new CategoryFilterFragment().newInstance(this), false, false,
                        0, R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim);
                break;
            case R.id.tvLoc:
                updateUI(tvLoc);
                changeChildFragment(new LocationFilterFragment().newInstance(this), false, false,
                        0, R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim);
                break;
            case R.id.tvAreaRange:
                updateUI(tvAreaRange);
                changeChildFragment(new AreaRangeFragment().newInstance(this), false, false,
                        0, R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim);
                break;
            case R.id.tvApplyFilter:
                applyFilter();
                break;
            case R.id.tvClearAll:
                clearFilters();
                break;
        }
    }

    private void clearFilters() {
        DefaulFilterSpf.clearFilter(getContext());
        getActivity().onBackPressed();

    }

    private void applyFilter() {
        if (getActivity() == null) {
            return;
        }
        filterModel = new FilterModel();
        if (CITY_ID != null && !CITY_ID.equals("")) {
            filterModel.setCity_id(CITY_ID);
        }
        if (DISTANCE != null && !DISTANCE.equals("")) {
            filterModel.setDistance(DISTANCE);
            filterModel.setLat(((DashboardActivity) getActivity()).mCurrentLatitude);
            filterModel.setLog(((DashboardActivity) getActivity()).mCurrentLongitude);
        }
        if (CATEGORY_ID != null && !CATEGORY_ID.equals("")) {
            filterModel.setCategoty_id(CATEGORY_ID);
        }

        Gson gson = new Gson();
        String s = gson.toJson(filterModel);
        DefaulFilterSpf.saveFilter(getContext(), s);

        getActivity().onBackPressed();
    }

    @Override
    public int getFragmentContainerResourceId(Fragment fragment) {
        return R.id.child_container;
    }

    private void updateUI(TypefaceTextView view) {
        TypefaceTextView[] tv_array = {tvCat, tvLoc, tvAreaRange};
        for (int i = 0; i < tv_array.length; i++) {
            if (view == tv_array[i]) {
                tv_array[i].setTextColor(getResources().getColor(R.color.colorRed));
                tv_array[i].setBackgroundResource(R.drawable.rectangle_pressed_state_gray);
            } else {
                tv_array[i].setTextColor(getResources().getColor(R.color.colorDarkGray));
                tv_array[i].setBackgroundResource(R.drawable.rectangle_solid_light_gray_with_press_state);

            }
        }
    }

    @Override
    public void onCitySelectedListener(String city_id) {
        displayLog(TAG, "onCitySelectedListener: " + city_id);
        CITY_ID = city_id;
    }

    @Override
    public void onRangeChangeListener(String rightPinValue) {
        DISTANCE = rightPinValue;
        displayLog(TAG, "onRangeChangeListener: " + DISTANCE);
    }

    @Override
    public void onCategorySelectedListener(String categoty_id) {
        displayLog(TAG, "onCategorySelectedListener: " + categoty_id);
        CATEGORY_ID = categoty_id;
    }

}
