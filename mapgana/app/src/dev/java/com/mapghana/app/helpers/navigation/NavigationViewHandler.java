package com.mapghana.app.helpers.navigation;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.SpinnerAdapter;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.model.IndividualPosts;
import com.mapghana.app.model.Logout;
import com.mapghana.app.model.OrganizationPosts;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.activity.dashboard.dashboard.DashboardFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.add_location.AddLocationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.SearchFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map.SingleItemOnMapFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.SearchListFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.FilterFragment;
import com.mapghana.app.ui.sidemenu.Event.AddEventFragment;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.app.ui.sidemenu.AboutUsFragment;
import com.mapghana.app.ui.sidemenu.Object.AddObjectFragment;
import com.mapghana.app.ui.sidemenu.contactus.ContactUsFragment;
import com.mapghana.app.ui.sidemenu.individual.IndividualFragment;
import com.mapghana.app.ui.sidemenu.organization.OrganizationFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog.PostListingDialog;
import com.mapghana.app.ui.sidemenu.profile.ProfileFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.base.BaseFragment;
import com.mapghana.customviews.CircleImageView;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.handler.NavigationViewHandlerInterface;
import com.mapghana.rest.ApiHitListener;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ubuntu on 6/12/17.
 */

public class NavigationViewHandler
        implements NavigationViewHandlerInterface, View.OnClickListener,
        AdapterClickListener, AdapterView.OnItemSelectedListener, ApiHitListener {

    private static AppBaseActivity baseActivity;
    private DrawerLayout drawerLayout;
    private LinearLayout llLocation;
    private RelativeLayout llNavToolBar;
    private ImageButton ivToggleNavButton, ivBack, imgNavSearch;
    private TypefaceTextView tvTitle;
    private NavigationView navigationView;

    private CircleImageView ivUserPic;
    private TypefaceTextView tvUserName, tvNavLocation, tv_addres;
    private Spinner spType;
    private RelativeLayout rl_profile;
    private static final String TAG = "NavigationViewHandler";
    public double mCurrentLongitude = 0.0;
    public double mCurrentLatitude = 0.0;

    private static OnSpinnerItemChangeListeners onSpinnerItemChangeListenersss;

    public NavigationViewHandler(AppBaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public void findViewIds() {
        RecyclerView recyclerView;


        drawerLayout = (DrawerLayout) baseActivity.findViewById(R.id.drawerLayout);
        llNavToolBar = (RelativeLayout) baseActivity.findViewById(R.id.llNavToolBar);
        rl_profile = (RelativeLayout) baseActivity.findViewById(R.id.rl_profile);
        llLocation = (LinearLayout) baseActivity.findViewById(R.id.llLocation);
        ivToggleNavButton = (ImageButton) baseActivity.findViewById(R.id.ivToggleNavButton);
        ivBack = (ImageButton) baseActivity.findViewById(R.id.ivBack);
        imgNavSearch = (ImageButton) baseActivity.findViewById(R.id.imgNavSearch);
        tvTitle = (TypefaceTextView) baseActivity.findViewById(R.id.tvTitle);
        tvUserName = (TypefaceTextView) baseActivity.findViewById(R.id.tvUserName);
        tvNavLocation = (TypefaceTextView) baseActivity.findViewById(R.id.tvNavLocation);
        tv_addres = (TypefaceTextView) baseActivity.findViewById(R.id.tv_addres);
        spType = (Spinner) baseActivity.findViewById(R.id.spType);
        recyclerView = (RecyclerView) baseActivity.findViewById(R.id.recyclerView);
        ivUserPic = (CircleImageView) baseActivity.findViewById(R.id.ivUserPic);
        navigationView = (NavigationView) baseActivity.findViewById(R.id.navigation_view);
        LinearLayoutManager manager = new LinearLayoutManager(baseActivity.getBaseContext());
        recyclerView.setLayoutManager(manager);

        ivBack.setOnClickListener(this);
        imgNavSearch.setOnClickListener(this);
        ivToggleNavButton.setOnClickListener(this);
        rl_profile.setOnClickListener(this);
        spType.setAdapter(new SpinnerAdapter(baseActivity.getBaseContext(), getTypes(), R.layout.item_post_type));

        recyclerView.setAdapter(new NavigationMenuAdapter(this, getMenuList()));
    }

    private String[] getTypes() {
        return baseActivity.getResources().getStringArray(R.array.type_array);
    }

    private List<NavMenuModel> getMenuList() {
        List<NavMenuModel> menuList = new ArrayList<>();

        menuList.add(new NavMenuModel(R.mipmap.organizations, baseActivity.getResources().getString(R.string.Organizations)));
        menuList.add(new NavMenuModel(R.drawable.ic_event_black_24dp, baseActivity.getResources().getString(R.string.Events)));
        menuList.add(new NavMenuModel(R.mipmap.individuals, baseActivity.getResources().getString(R.string.Individuals)));
        menuList.add(new NavMenuModel(R.mipmap.post_listing, baseActivity.getResources().getString(R.string.Post_Listing)));
        menuList.add(new NavMenuModel(R.mipmap.call, baseActivity.getResources().getString(R.string.Contact_Us)));
        menuList.add(new NavMenuModel(R.drawable.about_us, baseActivity.getResources().getString(R.string.About)));
        menuList.add(new NavMenuModel(R.mipmap.log, baseActivity.getResources().getString(R.string.Logout)));
        menuList.add(new NavMenuModel(R.drawable.ic_event_black_24dp,"Object"));


        return menuList;
    }


    @Override
    public void setNavTitle(String title) {
        if (tvTitle != null && !title.equals("")) {
            tvTitle.setText(title);
        }
    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {
        if (visibilty) {
            llNavToolBar.setVisibility(View.VISIBLE);
        } else {
            llNavToolBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {

        if (visibilty) {
            ivToggleNavButton.setVisibility(View.VISIBLE);
        } else {
            ivToggleNavButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {

        if (visibilty) {
            ivBack.setVisibility(View.VISIBLE);
        } else {
            ivBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void setHeaderProfilePic(String uri, int res) {

        if (uri != null && !uri.equals("")) {
            Glide.with(baseActivity).load(uri).override(250, 250)
                    .placeholder(R.drawable.profile).dontAnimate()
                    .into(ivUserPic);
        } else {
            ivUserPic.setImageResource(res);
        }
    }

    @Override
    public void setUserName(String name) {
        if (name != null && !name.equals("")) {
            tvUserName.setText(name);
        }
    }

    @Override
    public void onClick(View view) {

        if (ivToggleNavButton == view) {
            setDrawableStatus();
        }
        switch (view.getId()) {
            case R.id.rl_profile:
                setDrawableStatus();
                baseActivity.changeFragment(new ProfileFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, false);
                break;
            case R.id.imgNavSearch:
                baseActivity.changeFragment(new SearchFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.ivBack:
                baseActivity.onBackPressed();
                break;
        }
    }

    public void setDrawableStatus() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawers();
        } else {
            drawerLayout.openDrawer(navigationView);
        }
    }


    @Override
    public void onAdapterClickListener(int position) {
        Bundle bundle = new Bundle();

        switch (position) {
            case 0:
                setDrawableStatus();

                baseActivity.changeFragment(new OrganizationFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, true);
                break;

            case 1:
                setDrawableStatus();
                baseActivity.changeFragment(new EventListingFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, true);
                break;


            case 2:
                setDrawableStatus();
                baseActivity.changeFragment(new IndividualFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, true);
                break;

            case 3:
                setDrawableStatus();
                goToPostListing();
                break;
            case 4:
                setDrawableStatus();
                baseActivity.changeFragment(new ContactUsFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, false);
                break;

            case 5:
                setDrawableStatus();
                baseActivity.changeFragment(new AboutUsFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, false);
                break;
            case 6:
                setDrawableStatus();
                doLogout();
                break;
            case 7:
                setDrawableStatus();
                baseActivity.changeFragment(new AddObjectFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, true);
                break;
        }
    }

    private void doLogout() {
        if (ConnectionDetector.isNetAvail(baseActivity)) {
            baseActivity.displayProgressBar(false);
            RestClient restClient = new RestClient(baseActivity);
            restClient.callback(this).logout(baseActivity);
        } else {
            baseActivity.displayToast(Constants.No_Internet);
        }
    }

    private void goToPostListing() {
        PostListingDialog dialog = new PostListingDialog(baseActivity,
                this);
        dialog.show();
    }

    @Override
    public void onAdapterClickListener(String action) {

        Bundle bundle = new Bundle();
        AddLocationFragment fragment = new AddLocationFragment();
        if (action.equals(baseActivity.getString(R.string.Individuals))) {
            bundle.putString(Constants.type, Constants.INDIVIDUALS);
        } else if (action.equals(baseActivity.getString(R.string.Organizations))) {
            bundle.putString(Constants.type, Constants.ORGANISATION);
        }
        fragment.setArguments(bundle);
        baseActivity.changeFragment(fragment, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }

    @Override
    public void lockDrawer(boolean visibilty) {
        if (visibilty) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }


    private void showToast(String msg) {
        Toast.makeText(baseActivity, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {

        if (visibilty) {
            llLocation.setVisibility(View.VISIBLE);
        } else {
            llLocation.setVisibility(View.GONE);

        }
    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {
        if (visibilty) {
            imgNavSearch.setVisibility(View.VISIBLE);
        } else {
            imgNavSearch.setVisibility(View.GONE);

        }
    }

    @Override
    public void setNavLocationText(String Name) {
        tvNavLocation.setText(Name);
    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {
        if (visibilty) {
            tvTitle.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);

        }
    }

    @Override
    public void setNavLocationTextVisibilty(boolean visibilty) {
        if (visibilty) {
            tv_addres.setVisibility(View.VISIBLE);
        } else {
            tv_addres.setVisibility(View.GONE);

        }
    }

    @Override
    public void setNavAddressText(String title) {
        tv_addres.setText(title);
    }

    @Override
    public void setExploreVisibility(boolean visibility) {

    }

    @Override
    public void onAdapterClickListener(int position, String action) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position == 0) {
            spType.setSelection(0);
        } else if (position == 1) {
            spType.setSelection(1);
        } else if (position == 2) {
            spType.setSelection(2);
        }

        getPostLists();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void getPostLists() {
        if (ConnectionDetector.isNetAvail(baseActivity)) {
            // baseActivity.displayProgressBar(false);
            RestClient restClient = new RestClient(baseActivity);
            if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.organizations)) {
                restClient.callback(this).post_listing_organizationsss(String.valueOf(getmCurrentLatitude()), String.valueOf(getmCurrentLongitude()));
            } else if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.individuals)) {
                restClient.callback(this).post_listing_Individualsss(String.valueOf(getmCurrentLatitude()), String.valueOf(getmCurrentLongitude()));
            } else if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.All)) {
                restClient.callback(this).searchFilter("", "", "", String.valueOf(getmCurrentLatitude()), String.valueOf(getmCurrentLongitude()), "6");
            } else {
            }
        } else {
            baseActivity.displayToast(Constants.No_Internet);
        }
    }

    @Override
    public String getPostType() {
        if (spType.getSelectedItemPosition() == 1) {
            return baseActivity.getResources().getString(R.string.Individuals);
        } else if (spType.getSelectedItemPosition() == 0) {
            return baseActivity.getResources().getString(R.string.Organizations);
        } else if (spType.getSelectedItemPosition() == 2) {
            return baseActivity.getResources().getString(R.string.All);
        } else {
            // -1 is selected
            return "";
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        baseActivity.dismissProgressBar();
        int code = response.code();
        if (code == 404) {
            SessionManager.logout(baseActivity);
            baseActivity.finish();
        }
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_logout) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Logout logout = gson.fromJson(s, Logout.class);
                    if (logout.getStatus() != 0) {
                        baseActivity.displayToast(logout.getMessage());
                        SessionManager.logout(baseActivity.getBaseContext());
                        baseActivity.finish();
                    } else {
                        baseActivity.displayErrorDialog("Error", logout.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POSTS_Filter_organization) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    OrganizationPosts items = gson.fromJson(s, OrganizationPosts.class);
                    if (items.getStatus() != 0) {
                        onSpinnerItemChangeListenersss.OnSpinnerItemOrganizationListeners(items.getData());
                    } else {
                        baseActivity.displayErrorDialog("Error", items.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POSTS_Filter_individual) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    IndividualPosts items = gson.fromJson(s, IndividualPosts.class);
                    if (items.getStatus() != 0) {
                        onSpinnerItemChangeListenersss.OnSpinnerItemIndividualListeners(items.getData());
                    } else {
                        baseActivity.displayErrorDialog("Error", items.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POSTS_SEarch_Filter) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GlobeSearch data = gson.fromJson(s, GlobeSearch.class);
                    if (data.getStatus() != 0) {
                        onSpinnerItemChangeListenersss.OnSpinnerItemAllListeners(data.getData());

                    } else {
                        baseActivity.displayErrorDialog("Error", data.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            baseActivity.displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        //  baseActivity.dismissProgressBar();
        baseActivity.displayErrorDialog("Error1212", error);
    }


    public static NavigationViewHandler newInstance(OnSpinnerItemChangeListeners onSpinnerItemChangeListeners) {
        onSpinnerItemChangeListenersss = onSpinnerItemChangeListeners;
        Bundle args = new Bundle();

        NavigationViewHandler fragment = new NavigationViewHandler(NavigationViewHandler.baseActivity);
        //   fragment.setArguments(args);
        return fragment;
    }


    public interface OnSpinnerItemChangeListeners {
        void OnSpinnerItemOrganizationListeners(List<OrganizationPosts.DataBean> orList);

        void OnSpinnerItemIndividualListeners(List<IndividualPosts.DataBean> inList);

        void OnSpinnerItemAllListeners(List<GlobeSearch.DataBean> allList);
    }

    public void setOnSpinnerItemChangeListenersss() {
        spType.setOnItemSelectedListener(this);
    }

    public double getmCurrentLongitude() {
        return mCurrentLongitude;
    }

    public void setmCurrentLongitude(double mCurrentLongitude) {
        this.mCurrentLongitude = mCurrentLongitude;
    }

    public double getmCurrentLatitude() {
        return mCurrentLatitude;
    }

    public void setmCurrentLatitude(double mCurrentLatitude) {
        this.mCurrentLatitude = mCurrentLatitude;
    }

    private boolean isViewVisible(View view) {
        return view.getVisibility() == View.VISIBLE;
    }

    private boolean isViewHide(View view) {
        return view.getVisibility() == View.GONE || view.getVisibility() == View.INVISIBLE;
    }

    public void onCreateViewFragment(BaseFragment baseFragment) {
        if (baseFragment instanceof DashboardFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(true);
            setSearchButtonVisibuility(true);
            setBackButtonVisibilty(false);
            setNavTitleTextVisibilty(false);
            lockDrawer(false);
            setNavToggleButtonVisibilty(true);
        } else if (baseFragment instanceof SearchFragment) {
            //   setNavigationToolbarVisibilty(false);
        } else if (baseFragment instanceof SearchListFragment) {
          /*  setNavigationToolbarVisibilty(false);
            lockDrawer(true);*/
        } else if (baseFragment instanceof FilterFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Filter));
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
        } else if (baseFragment instanceof SingleItemOnMapFragment) {
         /*   setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            //getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);*/
        } else if (baseFragment instanceof PostDetailFragment) {
      /*      setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);*/
        } else if (baseFragment instanceof ProfileFragment) {
            setNavigationToolbarVisibilty(false);
        } else if (baseFragment instanceof OrganizationFragment) {
           /* setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Organizations));*/
        } else if (baseFragment instanceof IndividualFragment) {
         /*   setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Individuals));*/
        } else if (baseFragment instanceof ContactUsFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Contact_Us));
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
        } else if (baseFragment instanceof AboutUsFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.About));
        }
    }


}
