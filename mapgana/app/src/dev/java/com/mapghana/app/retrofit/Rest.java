package com.mapghana.app.retrofit;

import com.mapghana.app.rest.BaseArguments;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Rest {


    @FormUrlEncoded
    @POST(BaseArguments.signup)
    Call<ResponseBody> signup(@Field(BaseArguments.name) String name,
            @Field(BaseArguments.username) String full_name,
            @Field(BaseArguments.password) String password,
            @Field(BaseArguments.email) String email,
            @Field(BaseArguments.dob) String dob,
            @Field(BaseArguments.gender) String gender,
            @Field(BaseArguments.phone_number) String phone_number,
            @Field(BaseArguments.location) String location,
            @Field(BaseArguments.device_type) String device_type,
            @Field(BaseArguments.device_id) String device_id);

    @FormUrlEncoded
    @POST(BaseArguments.signup)
    Call<ResponseBody> newSignUp(@Field(BaseArguments.name) String name,
                              @Field(BaseArguments.username) String full_name,
                              @Field(BaseArguments.email) String email,
                              @Field(BaseArguments.phone_number) String phone_number,
                              @Field(BaseArguments.device_type) String device_type,
                              @Field(BaseArguments.device_id) String device_id);


    @FormUrlEncoded
    @POST(BaseArguments.login)
    Call<ResponseBody> login(
            @Field(BaseArguments.email) String email,
            @Field(BaseArguments.password) String password,
            @Field(BaseArguments.device_type) String device_type,
            @Field(BaseArguments.device_id) String device_id);

    @FormUrlEncoded
    @POST(BaseArguments.forgot)
    Call<ResponseBody> forgot(
            @Field(BaseArguments.email) String email);


    @GET(BaseArguments.profile)
    Call<ResponseBody> getProfile(
            @Query(BaseArguments.id) int id);

    @FormUrlEncoded
    @POST(BaseArguments.profile)
    Call<ResponseBody> updateProfile(@Field(BaseArguments.id) int id,
                                     @Field(BaseArguments.username) String username,
                                     @Field(BaseArguments.name) String name,
                                     @Field(BaseArguments.email) String email,
                                     @Field(BaseArguments.dob) String dob,
                                     @Field(BaseArguments.gender) String gender,
                                     @Field(BaseArguments.phone_number) String phone_number,
                                     @Field(BaseArguments.location) String location,
                                     @Field(BaseArguments.device_type) String device_type,
                                     @Field(BaseArguments.device_id) String device_id);
    @Multipart
    @POST(BaseArguments.profile)
    Call<ResponseBody> updateProfileWithImage(@PartMap HashMap<String, RequestBody> map,
                                              @Part MultipartBody.Part image);

    @GET(BaseArguments.category)
    Call<ResponseBody> category(@Query(BaseArguments.type) String type);

    @GET(BaseArguments.category)
    Call<ResponseBody> category();


    @GET(BaseArguments.city)
    Call<ResponseBody> city();


    @Multipart
    @POST(BaseArguments.post)
    Call<ResponseBody> post_individual(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part image,
                                       @Part MultipartBody.Part[] gallery_images);
    @Multipart
    @POST(BaseArguments.add_object)
    Call<ResponseBody> postAddObject(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part[] gallery_images);

    @Multipart
    @POST(BaseArguments.post)
    Call<ResponseBody> post_individual(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part image);

    @Multipart
    @POST(BaseArguments.post)
    Call<ResponseBody> post_organization(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part[] gallery_images);

    @Multipart
    @POST(BaseArguments.add_event)
    Call<ResponseBody> postAddEventApi(@PartMap HashMap<String, RequestBody> map,
                                         @Part MultipartBody.Part[] peviousGallery,@Part MultipartBody.Part[] currentGallery,@Part MultipartBody.Part[] posterMedia);


    @GET(BaseArguments.contact)
    Call<ResponseBody> contact();

    @FormUrlEncoded
    @POST(BaseArguments.contact)
    Call<ResponseBody> contact(  @Field(BaseArguments.name) String name,
                                 @Field(BaseArguments.email) String email,
                                 @Field(BaseArguments.phone) String phone,
                                 @Field(BaseArguments.message) String message);

    @GET(BaseArguments.logout+"/{user_id}")
    Call<ResponseBody> logout(@Path(BaseArguments.user_id) int user_id);

    @Multipart
    @POST(BaseArguments.post_send_review)
    Call<ResponseBody> post_send_review(@PartMap HashMap<String, RequestBody> map);

    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(  @Query(BaseArguments.lat) String lat,
                                                    @Query(BaseArguments.log) String log,
                                                    @Query(BaseArguments.distance) String distance);
    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id);


    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id,
                                                  @Query(BaseArguments.city_id) String city_id);

    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id,
                                                  @Query(BaseArguments.lat) String lat,
                                                  @Query(BaseArguments.log) String log,
                                                  @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id,
                                                  @Query(BaseArguments.city_id) String city_id,
                                                  @Query(BaseArguments.lat) String lat,
                                                  @Query(BaseArguments.log) String log,
                                                  @Query(BaseArguments.distance) String distance);


    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id,
                                                  @Query(BaseArguments.city_id) String city_id,
                                                  @Query(BaseArguments.lat) String lat,
                                                  @Query(BaseArguments.log) String log,
                                                  @Query(BaseArguments.distance) String distance);
    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id,
                                                  @Query(BaseArguments.city_id) String city_id);

    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id,
                                                  @Query(BaseArguments.lat) String lat,
                                                  @Query(BaseArguments.log) String log,
                                                  @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id);

    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.lat) String lat,
                                                @Query(BaseArguments.log) String log,
                                                @Query(BaseArguments.distance) String distance);


    @GET(BaseArguments.post_listing_type+"/{type}")
    Call<ResponseBody> post_listing_type(@Path("type") String type);

    @GET(BaseArguments.post+"/{post_id}")
    Call<ResponseBody> getDetailsOfOnePost(@Path(BaseArguments.post_id) String post_id);

    @GET(BaseArguments.search)
    Call<ResponseBody> search(@Query(BaseArguments.keyword) String keyword);

    @GET(BaseArguments.search)
    Call<ResponseBody> searchFilter(@Query(BaseArguments.keyword) String keyword,
                                    @Query(BaseArguments.category_id) String category_id,
                                    @Query(BaseArguments.city_id) String city_id,
                                    @Query(BaseArguments.lat) String lat,
                                    @Query(BaseArguments.log) String log,
                                    @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.ads)
    Call<ResponseBody> ads();

    @GET(BaseArguments.event_listing)
    Call<ResponseBody> getEventListing();

}
