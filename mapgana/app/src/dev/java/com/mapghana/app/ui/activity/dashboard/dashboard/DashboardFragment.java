package com.mapghana.app.ui.activity.dashboard.dashboard;


import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.helpers.navigation.NavigationViewHandler;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.model.IndividualPosts;
import com.mapghana.app.model.OrganizationPosts;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.add_location.AddLocationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog.PostListingDialog;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends AppBaseFragment implements
        NavigationViewHandler.OnSpinnerItemChangeListeners,

        MapboxMap.OnInfoWindowClickListener,
        AppBaseMapBox.OnMapLoaded,
        LocationServiceListner, MapboxMap.OnCameraIdleListener {

    private static final String TAG = "DashboardFragment";
    private HashMap<Marker, OrganizationPosts.DataBean> markerHashMap_Org;
    private HashMap<Marker, IndividualPosts.DataBean> markerHashMap_Indi;
    private HashMap<Marker, GlobeSearch.DataBean> markerHashMap_All;
    private ArrayList<OrganizationPosts.DataBean> orgList;
    private ArrayList<IndividualPosts.DataBean> indiList;
    private ArrayList<GlobeSearch.DataBean> allList;
    private int TYPE;
    private FrameLayout frameLayout;
    private float zoomLevel;
    private MapboxMap mapboxMap;
    private Marker marker_current_loc;
    private double log, lat;
    private boolean isAlreadyRequesting;
    PostListingDialog dialog;
    private Handler reHitHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            reHitHandler.removeCallbacks(runnable);
            reHitHandler.postDelayed(runnable, 1000 * 10);
            ((DashboardActivity) getActivity()).navigationViewHandler.getPostLists();
        }
    };


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public void initializeComponent() {
        init();
        if (getActivity() == null) return;
        isAlreadyRequesting = false;
        TypefaceTextView tvPostListing;
        ImageButton imgMyLoc;
        tvPostListing = (TypefaceTextView) getView().findViewById(R.id.tvPostListing);
        imgMyLoc = (ImageButton) getView().findViewById(R.id.imgMyLoc);
        frameLayout = (FrameLayout) getView().findViewById(R.id.map_contanier);
        dialog = new PostListingDialog(getActivity(), this);
        orgList = new ArrayList<>();
        indiList = new ArrayList<>();
        allList = new ArrayList<>();
        tvPostListing.setOnClickListener(this);
        imgMyLoc.setOnClickListener(this);
        markerHashMap_Org = new HashMap<>();
        markerHashMap_Indi = new HashMap<>();
        markerHashMap_All = new HashMap<>();
        zoomLevel = 13;
        ((DashboardActivity) getActivity()).navigationViewHandler.newInstance(this);

        initMap();
    }

    private void initMap() {
        if (getActivity() != null) {
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnInfoWindowClickListener(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnCameraIdleListeners(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());
        }
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(true);
        getNavHandler().setSearchButtonVisibuility(true);
        getNavHandler().setBackButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(false);
        getNavHandler().lockDrawer(false);
        getNavHandler().setNavToggleButtonVisibilty(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvPostListing:
                if (dialog != null) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    dialog.show();
                }
                break;
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
        }
    }

    @Override
    public void onAdapterClickListener(String action) {
        // Bundle bundle = new Bundle();
        AddLocationFragment fragment = new AddLocationFragment();
        if (action.equals(getResources().getString(R.string.Individuals))) {
            fragment.setType(Constants.INDIVIDUALS);
            //  bundle.putInt(Constants.type, 1);
        } else if (action.equals(getResources().getString(R.string.Organizations))) {
            //bundle.putInt(Constants.type, 0);
            fragment.setType(Constants.ORGANISATION);
        }
        //fragment.setArguments(bundle);
        ((AppBaseActivity) getActivity()).changeFragment(fragment, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
    }

    @Override
    public void onFailResponse(int apiId, String error) {
    }


    @Override
    public void OnSpinnerItemOrganizationListeners(List<OrganizationPosts.DataBean> orList) {
        // addMarkersAgain(orList);
        clearIndiHashMap();
        clearOrgHashMap();
        clearAllHashMap();
        clearAllList();
        clearIndiList();
        clearOrgList();
        TYPE = 0;
        if (getActivity() == null || !isVisible()) return;

        ((DashboardActivity) getActivity()).appBaseMapBox.clearMarker();
        orgList.addAll(orList);
        setMarkerOfOrganization(orList);

    }

    private void addMarkersAgain(List<OrganizationPosts.DataBean> orList) {
        TYPE = 0;
        if (getActivity() == null || !isVisible()) return;
        orgList.addAll(orList);
        setMarkerOfOrganization(this.orgList);
    }

    @Override
    public void OnSpinnerItemIndividualListeners(List<IndividualPosts.DataBean> inList) {
        clearIndiHashMap();
        clearOrgHashMap();
        clearAllHashMap();
        clearAllList();
        clearIndiList();
        clearOrgList();
        TYPE = 1;
        if (getActivity() == null || !isVisible()) return;
        ((DashboardActivity) getActivity()).appBaseMapBox.clearMarker();
        indiList.addAll(inList);
        setMarkerOfIndividual(inList);

    }

    @Override
    public void OnSpinnerItemAllListeners(List<GlobeSearch.DataBean> allList) {
        clearIndiHashMap();
        clearOrgHashMap();
        clearAllHashMap();
        clearAllList();
        clearIndiList();
        clearOrgList();
        TYPE = 2;
        if (getActivity() == null || !isVisible()) return;
        ((DashboardActivity) getActivity()).appBaseMapBox.clearMarker();
        this.allList.addAll(allList);
        setMarkerOfAll(this.allList);


    }


    private void changeFragment(int post_id) {
        PostDetailFragment detailsFragment = new PostDetailFragment();
        detailsFragment.setPost_id(post_id);
        ((AppBaseActivity) getActivity()).changeFragment(detailsFragment, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        int post_id = 0;

        if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {

            for (Map.Entry<Marker, OrganizationPosts.DataBean> m : markerHashMap_Org.entrySet()) {
                if (!m.getValue().getName().equalsIgnoreCase(Constants.You) && m.getKey() == marker) {
                    OrganizationPosts.DataBean dataBean = m.getValue();
                    post_id = dataBean.getId();
                    changeFragment(post_id);
                }
            }
        }
        if (markerHashMap_Indi != null && markerHashMap_Indi.size() > 0) {
            for (Map.Entry<Marker, IndividualPosts.DataBean> m : markerHashMap_Indi.entrySet()) {
                if (!m.getValue().getName().equalsIgnoreCase(Constants.You) && m.getKey() == marker) {
                    IndividualPosts.DataBean dataBean = m.getValue();
                    post_id = dataBean.getId();
                    changeFragment(post_id);
                }
            }
        }
        if (markerHashMap_All != null && markerHashMap_All.size() > 0) {
            for (Map.Entry<Marker, GlobeSearch.DataBean> m : markerHashMap_All.entrySet()) {
                if (!m.getValue().getName().equalsIgnoreCase(Constants.You) && m.getKey() == marker) {
                    GlobeSearch.DataBean dataBean = m.getValue();
                    post_id = dataBean.getId();
                    changeFragment(post_id);
                }
            }
        }
        return false;
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        if (getActivity() == null || !isVisible()) return;

        lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(new LatLng(lat, log), zoomLevel);
        ((DashboardActivity) getActivity()).navigationViewHandler.setOnSpinnerItemChangeListenersss();

       /* getActivity().runOnUiThread(runnable);
        reHitHandler.postDelayed(runnable, 10 * 1000);*/

    }

    private void setMarkersOnMapAgain() {
        if (getActivity() == null || !isVisible()) return;
        zoomLevel = 17;
        double lat = ((DashboardActivity) getActivity()).mCurrentLatitude;
        double log = ((DashboardActivity) getActivity()).mCurrentLongitude;
        LatLng latLng = new LatLng(lat, log);
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(log);
        moveMarker(location);
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(latLng, zoomLevel);
    }

    private void setCurrentLocation() {
        if (getActivity() == null || !isVisible()) return;

        if (lat != 0 && log != 0) {
            marker_current_loc = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.my_location, new LatLng(lat, log),
                    Constants.You, "");
            //  ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(new LatLng(lat, log), zoomLevel);
         /* List<LatLng> boundList=new ArrayList<>();
            LatLng latLng=new LatLng(lat, log);
            boundList.add(latLng);
            ((DashboardActivity) getActivity()).appBaseMapBox.boundMap(boundList);*/
            if (TYPE == 0) {
                OrganizationPosts.DataBean bean = new OrganizationPosts.DataBean();
                bean.setName(Constants.You);
                markerHashMap_Org.put(marker_current_loc, bean);
            } else if (TYPE == 1) {
                IndividualPosts.DataBean bean = new IndividualPosts.DataBean();
                bean.setName(Constants.You);
                markerHashMap_Indi.put(marker_current_loc, bean);
            } else if (TYPE == 2) {
                GlobeSearch.DataBean bean = new GlobeSearch.DataBean();
                bean.setName(Constants.You);
                markerHashMap_All.put(marker_current_loc, bean);
            }
        }
    }

    @Override
    public void onCameraIdle() {
        if (getActivity() == null || !isVisible()) return;
        CameraPosition cameraPosition = mapboxMap.getCameraPosition();
        LatLng latLng = cameraPosition.target;
        if (!isAlreadyRequesting && latLng != null) {
            isAlreadyRequesting = true;
            ((DashboardActivity) getActivity()).navigationViewHandler.setmCurrentLatitude(latLng.getLatitude());
            ((DashboardActivity) getActivity()).navigationViewHandler.setmCurrentLongitude(latLng.getLongitude());
            ((DashboardActivity) getActivity()).navigationViewHandler.getPostLists();
        }
    }

    private void moveMarker(Location location) {
        if (location == null && mapboxMap != null) {
            return;
        }
        if (getActivity() == null || !isVisible()) return;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        Marker marker = null;
        if (TYPE == 0) {
            if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
                for (Map.Entry<Marker, OrganizationPosts.DataBean> m : markerHashMap_Org.entrySet()) {
                    if (m.getValue().getName().equalsIgnoreCase(Constants.You)) {
                        marker = m.getKey();
                        break;
                    }
                }
            }
        } else if (TYPE == 1) {
            for (Map.Entry<Marker, IndividualPosts.DataBean> m : markerHashMap_Indi.entrySet()) {
                if (m.getValue().getName().equalsIgnoreCase(Constants.You)) {
                    marker = m.getKey();
                    break;
                }
            }
        } else if (TYPE == 2) {
            for (Map.Entry<Marker, GlobeSearch.DataBean> m : markerHashMap_All.entrySet()) {
                if (m.getValue().getName().equalsIgnoreCase(Constants.You)) {
                    marker = m.getKey();
                    break;
                }
            }
        }
        if (marker != null) {
            marker.setPosition(latLng);
        }
    }

    @Override
    public void userLocationChange(Location location) {
        if (mapboxMap != null)
            moveMarker(location);
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {
    }

    private void setMarkerOfOrganization(List<OrganizationPosts.DataBean> data) {
        if (getActivity() == null || !isVisible()) return;

        clearOrgHashMap();
        try {
            if (data != null && data.size() > 0) {

                int resource = 0;
                for (int i = 0; i < data.size(); i++) {
                    OrganizationPosts.DataBean dataBean = data.get(i);

                    LatLng latLng = new LatLng(dataBean.getLat(),
                            dataBean.getLog());
                    if (dataBean.getStatus().trim().equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.location_gree_org;
                    } else if (dataBean.getStatus().trim().equalsIgnoreCase(Constants.unverified)) {
                        resource = R.mipmap.location_red_org;
                    } else {
                        resource = R.mipmap.location_gry_org;
                    }

                    String categoryName = "Category: ";
                    OrganizationPosts.DataBean.CategoryBean category = dataBean.getCategory();
                    if (category != null) {
                        categoryName = categoryName + category.getName();
                    }
                    OrganizationPosts.DataBean.SubcategoryBean subcategory = dataBean.getSubcategory();
                    if (subcategory != null) {
                        categoryName = categoryName + " (" + subcategory.getName() + ")";
                    }
                    Marker marker = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(),
                            resource, latLng, dataBean.getName(),
                            categoryName + "\nAddress: " + dataBean.getAddress() + "\nStatus: " + dataBean.getStatus());
                    markerHashMap_Org.put(marker, dataBean);
                }
            } else {
                displayLog(TAG, "setMarkerOfOrganization: mapview is null");
            }
        } catch (Exception e) {
            displayLog(TAG, "setMarkerOfOrganization:Excep: " + e.getMessage());
        }
        isAlreadyRequesting = false;

    }

    private void setMarkerOfIndividual(List<IndividualPosts.DataBean> data) {
        //  Icon icon = Icon.createWithResource();
        if (getActivity() == null || !isVisible()) return;
        clearIndiHashMap();

        Marker marker;
        setCurrentLocation();

        if (data != null && data.size() > 0) {
            int resourse = 0;
            String status = "";
            String gender = "";
            String category = "";
            for (int i = 0; i < data.size(); i++) {
                LatLng latLng = new LatLng(data.get(i).getLat(),
                        data.get(i).getLog());
                status = data.get(i).getStatus().trim();
                gender = data.get(i).getGender().trim();

                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.individual_green_gril;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.individual_red_girl;
                    } else {
                        resourse = R.mipmap.indi_girl_gray;
                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.individual_green_man;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.individual_red_man;
                    } else {
                        resourse = R.mipmap.individual_gray_man;
                    }
                }
                marker = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), resourse, latLng, data.get(i).getName(),
                        "Occupation: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus());
                markerHashMap_Indi.put(marker, data.get(i));
            }
        } else {
            displayLog(TAG, "setMarkerOfOrganization: mapview is null");
        }
        isAlreadyRequesting = false;

    }

    private void setMarkerOfAll(ArrayList<GlobeSearch.DataBean> data) {
        if (getActivity() == null || !isVisible()) return;
        clearAllHashMap();

        Marker marker;
        String category = "";

        setCurrentLocation();
        if (data != null && data.size() > 0) {
            int resourse = 0;
            String status = "";
            String gender = "";
            String postFor = "";
            for (int i = 0; i < data.size(); i++) {
                GlobeSearch.DataBean dataBean = data.get(i);
                LatLng latLng = new LatLng(dataBean.getLat(),
                        dataBean.getLog());

                status = dataBean.getStatus().trim();
                gender = dataBean.getGender().trim();
                postFor = dataBean.getPost_for();
                if (postFor != null && postFor.equalsIgnoreCase(Constants.individuals)) {
                    if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                        if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.verified)) {
                            resourse = R.mipmap.individual_green_gril;
                        } else if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.unverified)) {
                            resourse = R.mipmap.individual_red_girl;
                        } else {
                            resourse = R.mipmap.indi_girl_gray;
                        }
                    } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                        if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.verified)) {
                            resourse = R.mipmap.individual_green_man;
                        } else if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.unverified)) {
                            resourse = R.mipmap.individual_red_man;
                        } else {
                            resourse = R.mipmap.individual_gray_man;
                        }
                    }
                    category = "Occupation: " + dataBean.getCategory().getName();

                } else if (postFor != null &&
                        postFor.equalsIgnoreCase(Constants.organizations)) {
                    if (status != null &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.location_gree_org;
                    } else if (status != null && status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.location_red_org;
                    } else {
                        resourse = R.mipmap.location_gry_org;
                    }
                    category = "Category: " + dataBean.getCategory().getName();
                    GlobeSearch.DataBean.SubcategoryBean subcategory = dataBean.getSubcategory();
                    if (subcategory != null) {
                        category = category + " (" + subcategory.getName() + ")";
                    }

                }
                /*************************/
                marker = ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), resourse, latLng, dataBean.getName(),
                        category + "\nAddress: " + dataBean.getAddress() + "\nStatus: " + dataBean.getStatus());
                markerHashMap_All.put(marker, dataBean);
            }
        } else {
            displayLog(TAG, "setMarkerOfOrganization: mapview is null");
        }
        isAlreadyRequesting = false;

    }

    private void clearOrgHashMap() {
        if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
            markerHashMap_Org.clear();
        }

    }

    private void clearOrgList() {
        if (orgList != null && orgList.size() > 0) {
            orgList.clear();
        }
    }

    private void clearIndiHashMap() {
        if (markerHashMap_Indi != null && markerHashMap_Indi.size() > 0) {
            markerHashMap_Indi.clear();
        }
    }

    private void clearIndiList() {
        if (indiList != null && indiList.size() > 0) {
            indiList.clear();
        }
    }

    private void clearAllHashMap() {
        if (markerHashMap_All != null && markerHashMap_All.size() > 0) {
            markerHashMap_All.clear();
        }
    }

    private void clearAllList() {
        if (allList != null && allList.size() > 0) {
            allList.clear();
        }
    }

}