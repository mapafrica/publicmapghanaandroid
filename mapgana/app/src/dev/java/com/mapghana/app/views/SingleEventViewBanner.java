package com.mapghana.app.views;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.model.EventDetail;

public class SingleEventViewBanner  extends LinearLayout {

    FrameLayout mapContainer;

    ImageView gateKeepingIndication;

    TextView mapNavigate;

    TextView ticket_price;

    TextView bookTicket;

    TextView rsvp;

    View eventDetailSection;



    public SingleEventViewBanner(Context context) {
        super(context);
    }

    public SingleEventViewBanner(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SingleEventViewBanner(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            mapContainer = findViewById(R.id.map_contanier);
            gateKeepingIndication =  findViewById(R.id.event_gatekeeping_indicator);
            mapNavigate = findViewById(R.id.map_navigate);
            ticket_price =  findViewById(R.id.ticket_price);
            bookTicket =  findViewById(R.id.book_ticket);
            rsvp =  findViewById(R.id.rsvp);
            eventDetailSection =  findViewById(R.id.layout_event_info_section);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void bind(final EventDetail item, final Context context) {
        if(item.getGatekeeping_services().equalsIgnoreCase("Y")) {
            gateKeepingIndication.setImageDrawable(context.getDrawable(R.drawable.ic_indicator_green_circle));
        }else {
            gateKeepingIndication.setImageDrawable(context.getDrawable(R.drawable.ic_indicator_green_circle));

        }
        ticket_price.setText("GHC "+item.getTicket_price());


    }
}
