package com.mapghana.app.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.App;
import com.mapghana.app.interfaces.MediaClickHandler;
import com.mapghana.app.model.EventGallery;
import com.mapghana.app.model.SingleItemModel;

import java.util.ArrayList;
import java.util.List;

public class EventMediaListAdapter extends RecyclerView.Adapter<EventMediaListAdapter.SingleItemRowHolder>  {

    private List<EventGallery> itemsList;
    private Context mContext;
    private MediaClickHandler listener;

    public EventMediaListAdapter(Context context, List<EventGallery> itemsList, MediaClickHandler listener) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.listener = listener;
    }
    @NonNull
    @Override
    public SingleItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event_media_list, null);
        return new EventMediaListAdapter.SingleItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EventMediaListAdapter.SingleItemRowHolder holder, int position) {
        EventGallery eventGallery = itemsList.get(position);
        holder.setPosition(position);
        if(eventGallery.getType().equalsIgnoreCase("PHOTOS")) {
            holder.rlVideo.setVisibility(View.GONE);
            holder.rl_image.setVisibility(View.VISIBLE);
            Glide.clear(holder.itemImage);
            Glide.with(App.sharedInstance())
                    .load(eventGallery.getFile_pathname())
                    .dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                                isFirstResource) {
                            holder.bar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                                target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.bar.setVisibility(View.INVISIBLE);
                            return false;
                        }
                    })
                    .into(holder.itemImage);
        }else {
            holder.rlVideo.setVisibility(View.VISIBLE);
            holder.rl_image.setVisibility(View.GONE);
            Uri video = Uri.parse(eventGallery.getFile_pathname());
            holder.itemVideo.setVideoURI(video);
            holder.itemVideo.setOnPreparedListener(mp -> {
                mp.setLooping(true);
                holder.itemVideo.start();

            });

        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size()>0 ? itemsList.size() : 0;
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder{

        private ImageView itemImage;
        private VideoView itemVideo;
        private ImageView playIcon;
        private ProgressBar bar;
        private RelativeLayout rlVideo;
        private RelativeLayout rl_image;
        private int position;


        public SingleItemRowHolder(View view) {
            super(view);
            playIcon = view.findViewById(R.id.play_icon);
            this.bar = view.findViewById(R.id.progress_bar);
            this.itemImage = view.findViewById(R.id.image);
            this.itemVideo = view.findViewById(R.id.video);
            this.rlVideo = view.findViewById(R.id.rl_video);
            this.rl_image = view.findViewById(R.id.rl_image);

            view.setOnClickListener(v -> {
                listener.onClick(itemsList.get(position));
            });

        }
        public void setPosition(int position) {
            this.position = position;
        }
    }

}
