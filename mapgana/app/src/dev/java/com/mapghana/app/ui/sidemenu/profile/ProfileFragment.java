package com.mapghana.app.ui.sidemenu.profile;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.GetProfile;
import com.mapghana.app.model.Login;
import com.mapghana.app.model.UpdateProfile;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.CircleImageView;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Valiations;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends AppBaseFragment implements CustomDatePickerDialog.OnDateListener {

    private static final String TAG = "SignUpFragment";
    private Spinner spGender;
    private ImageButton ivBackButton;
    private TypefaceTextView tvUpdate, tvDob, tvTitle;
    private TypefaceEditText etUname, etEmail, etPhone, etLoc;
    private RestClient restClient;
    private CircleImageView ivProfilePic;
    private ImageButton ivUploadProfilePic;
    private Uri uri;
    SimpleSpinnerAdapter gender_adapter;
    private Date date;

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void initializeComponent() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        spGender = (Spinner) getView().findViewById(R.id.spGender);
        tvUpdate = (TypefaceTextView) getView().findViewById(R.id.tvUpdate);
        tvDob = (TypefaceTextView) getView().findViewById(R.id.tvDob);
        tvTitle = (TypefaceTextView) getView().findViewById(R.id.tvTitle);
        etUname = (TypefaceEditText) getView().findViewById(R.id.etUname);
        etEmail = (TypefaceEditText) getView().findViewById(R.id.etEmail);
        etPhone = (TypefaceEditText) getView().findViewById(R.id.etPhone);
        etLoc = (TypefaceEditText) getView().findViewById(R.id.etLoc);
        ivBackButton = (ImageButton) getView().findViewById(R.id.ivBackButton);
        ivProfilePic = (CircleImageView) getView().findViewById(R.id.ivProfilePic);
        ivUploadProfilePic = (ImageButton) getView().findViewById(R.id.ivUploadProfilePic);
        restClient = new RestClient(getContext());
        date = new Date();

        tvUpdate.setOnClickListener(this);
        tvDob.setOnClickListener(this);
        ivBackButton.setOnClickListener(this);
        ivUploadProfilePic.setOnClickListener(this);


        prepareData();

        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                gender_adapter.setSelGender(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void prepareData() {
        tvTitle.setText(getContext().getResources().getString(R.string.Profile));
        gender_adapter = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.gender_array));
        gender_adapter.setSelGender(0);
        spGender.setAdapter(gender_adapter);

        getProfile();
    }

    private int getUserId() {
        String userInfo = SessionManager.getUserInfoResponse(getContext());
        if (!userInfo.equals("")) {
            Gson gson = new Gson();
            Login login = gson.fromJson(userInfo, Login.class);
            return login.getData().getId();
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvUpdate:
                onUpdate();
                break;
            case R.id.tvDob:
                onDobDialog();
                break;
            case R.id.ivBackButton:
                getActivity().onBackPressed();
                break;
            case R.id.ivUploadProfilePic:
                onUploadImage();
                break;
        }
    }

    private void onUploadImage() {
        CropImage.activity()
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        uri = result.getUri();
                        ivProfilePic.setImageURI(uri);
                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                        displayErrorDialog("Error", error.getMessage());
                    }
                }
                break;
        }
    }

    private void onUpdate() {
        String name, mail, phone, loc;
        name = etUname.getText().toString().trim();
        mail = etEmail.getText().toString().trim();
        phone = etPhone.getText().toString().trim();
        loc = etLoc.getText().toString().trim();

        if (name.length() < 3 || name.length() > 30) {
            displayToast("User Name should contain 3 to 30 digits.");
            return;
        }


        if (phone.length() < 7 || phone.length() > 15) {
            displayToast("Phone should contain 7 to 15 digits.");
            return;
        }


        if (Valiations.hasText(etUname) && Valiations.isEmailAddress(etEmail, true)
                && Valiations.hasText(etLoc)) {
            if (tvDob.getText().toString().trim().equals(getResources().getString(R.string.Date_of_birth))) {
                displayToast("Select Date Of Birth");
                return;
            }
            if (date != null
                    && !dobdateValidate(date)) {
                displayToast("Minimum 12 year age is allowed");
                return;
            } else if (getItem() == null) {
                displayToast("Select Gender");
                return;
            }
            if (ConnectionDetector.isNetAvail(getContext())) {
                displayProgressBar(false);
                if (uri == null) {
                    restClient.callback(this).updateProfile(getUserId(), name, mail, tvDob.getText().toString().trim(), getItem(), phone, loc, getContext());
                } else {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(BaseArguments.id, String.valueOf(getUserId()));
                    map.put(BaseArguments.username, name);
                    map.put(BaseArguments.name, name);
                    map.put(BaseArguments.email, mail);
                    map.put(BaseArguments.dob, tvDob.getText().toString().trim());
                    map.put(BaseArguments.gender, getItem());
                    map.put(BaseArguments.phone_number, phone);
                    map.put(BaseArguments.location, loc);
                    map.put(BaseArguments.device_type, Constants.device_type);
                    map.put(BaseArguments.device_id, getDeviceID());

                    MultipartBody.Part part;
                    part = RetrofitUtils.createFilePart("image", uri.getPath(), RetrofitUtils.MEDIA_TYPE_IMAGE_PNG);

                    restClient.callback(this).updateProfileWithImage(RetrofitUtils.createMultipartRequest(map), part);
                }
            } else {
                displayToast(Constants.No_Internet);
            }
        }
    }

    public static boolean dobdateValidate(Date date) {


        try {
            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.YEAR, -12);
            if (date.before(c2.getTime())) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String getDeviceID() {
        return Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        if (dialog != null) {
            dialog.create(getContext(), true).callback(this).show();
        }
    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            this.date = date;
            String DOB;
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            DOB = format.format(date);
            tvDob.setText(DOB);
        }
    }

    private String getItem() {
        int position = spGender.getSelectedItemPosition();
        if (position == 0) return null;
        String[] array = getContext().getResources().getStringArray(R.array.gender_array);
        for (int i = 0; i < array.length; i++) {
            if (position == i) {
                return array[i];
            }
        }
        return null;
    }

    private void setItem(String gender) {
        String[] array = getContext().getResources().getStringArray(R.array.gender_array);
        for (int i = 0; i < array.length; i++) {
            if (gender.equalsIgnoreCase(array[i])) {
                spGender.setSelection(i);
            }
        }
    }

    private void getProfile() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).getProfile(getUserId(), getContext());
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_GETPROFILE) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GetProfile getProfile = gson.fromJson(s, GetProfile.class);
                    if (getProfile.getStatus() != 0) {
                        etEmail.setText(getProfile.getData().getEmail());
                        etPhone.setText(getProfile.getData().getPhone_number());
                        etLoc.setText(getProfile.getData().getLocation());
                        date = new Date();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.date_format);
                        try {
                            date = simpleDateFormat.parse(getProfile.getData().getDob());

                        } catch (Exception e) {
                        }
                        tvDob.setText(getProfile.getData().getDob());

                        etUname.setText(getProfile.getData().getUsername());
                        setItem(getProfile.getData().getGender());
                        if (getProfile.getData().getImage() != null && !getProfile.getData().getImage().equals("")) {
                            Glide.with(getContext()).load(getProfile.getData().getImage()).override(250, 250).placeholder(R.drawable.profile)
                                    .dontAnimate()
                                    .thumbnail(0.5f).into(ivProfilePic);
                        }
                    } else {
                        displayErrorDialog("Error", getProfile.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_UPDATEPROFILE) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    UpdateProfile updatePRofile = gson.fromJson(s, UpdateProfile.class);
                    if (updatePRofile.getStatus() != 0) {
                        etEmail.setText(updatePRofile.getData().getEmail());
                        etPhone.setText(updatePRofile.getData().getPhone_number());
                        etLoc.setText(updatePRofile.getData().getLocation());
                        tvDob.setText(updatePRofile.getData().getDob());
                        etUname.setText(updatePRofile.getData().getUsername());
                        setItem(updatePRofile.getData().getGender());

                        SessionManager.saveUname(getContext(), updatePRofile.getData().getName());
                        SessionManager.saveAddress(getContext(), updatePRofile.getData().getLocation());

                        if (updatePRofile.getData().getImage() != null && !updatePRofile.getData().getImage().equals("")) {

                            SessionManager.saveProfileImage(getContext(), updatePRofile.getData().getImage());

                            Glide.with(getContext()).load(updatePRofile.getData().getImage()).override(250, 250).placeholder(R.drawable.profile)
                                    .dontAnimate()
                                    .into(ivProfilePic);
                            uri = null;

                        }

                        if (updatePRofile.getData().getImage() != null && !updatePRofile.getData().getImage().equals("")) {
                            getNavHandler().setHeaderProfilePic(updatePRofile.getData().getImage(), R.mipmap.user_profile_round_dummy);
                        }

                        if (updatePRofile.getData().getLocation() != null && !updatePRofile.getData().getLocation().equals("")) {
                            getNavHandler().setNavLocationText(updatePRofile.getData().getLocation());
                        }

                        if (updatePRofile.getData().getName() != null && !updatePRofile.getData().getName().equals("")) {
                            getNavHandler().setUserName(updatePRofile.getData().getName());
                        }

                        displayToast(updatePRofile.getMessage());
                        getActivity().onBackPressed();
                    } else {
                        displayErrorDialog("Error", updatePRofile.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    public String getImagePath(Uri uri) {
        Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContext().getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }
}
