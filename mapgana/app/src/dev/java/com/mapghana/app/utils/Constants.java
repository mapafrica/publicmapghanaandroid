package com.mapghana.app.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 6/1/18.
 */

public class Constants {
    public static String date_format = "dd-MM-yyyy";
    public static String date_format2 = "dd MMM yyyy @ hh:mm a";

    public static String device_type = "A";
    public static String No_Internet = "No Internet Connection";
    public static String SOMETHING_WENT_WRONG = "Something went wrong";
    public static String individuals = "individuals";
    public static String All = "All";
    public static String organizations = "organizations";
    public static String sub_category_id = "sub_category_id";
    public static String Select_City = "Select Regions";
    public static String type = "type";
   // public static String category_id = "category_id";
    // public static String post_id = "post_id";
    public static String You = "You";
    public static String verified = "verified";
    public static String unverified = "unverified";
    public static String anonymous = "anonymous";
    public static String male = "male";
    public static String female = "female";
    public static String ITEMS = "items";
    public static String TITLE = "title";
    public static String ORGANISATION = "organisation";
    public static String INDIVIDUALS = "individuals";
    public static String ADDEVENTS = "addEvents";


    // public static String video = "/video/";
  //  public static String images = "/images/";

    private static List<String> imageList;
    private static List<String> videoList;

    private static void initilaizeImageList() {
        imageList = new ArrayList<>();
        imageList.add("image/jpeg");
        imageList.add("image/png");
        imageList.add("image/gif");
        imageList.add("image/x-ms-bmp");
        imageList.add("image/webp");
    }

    public static List<String> getImageMimeList() {
        initilaizeImageList();
        return imageList;
    }

    private static void initilaizeVideoList() {
        videoList = new ArrayList<>();

        videoList.add("video/mpeg");
        videoList.add("video/mp4");
        videoList.add("video/quicktime");
        videoList.add("video/3gpp");
        videoList.add("video/3gpp2");
        videoList.add("video/x-matroska");
        videoList.add("video/webm");
        videoList.add("video/mp2ts");
        videoList.add("video/avi");
    }

    public static List<String> getVideoMimeList() {
        initilaizeVideoList();
        return videoList;
    }


}
