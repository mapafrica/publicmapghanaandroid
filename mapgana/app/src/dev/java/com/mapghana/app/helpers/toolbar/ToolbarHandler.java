package com.mapghana.app.helpers.toolbar;

import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.customviews.TypefaceTextView;

/**
 * Created by ubuntu on 7/12/17.
 */

public class ToolbarHandler implements View.OnClickListener {

    private AppBaseActivity baseActivity;
    private LinearLayout llToolbar1;
    private ImageButton ivBackButton;
    private TypefaceTextView tvTitle;
    private static final String TAG = "ToolbarHandler";

    public ToolbarHandler(AppBaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public void findViews(){
        llToolbar1 = (LinearLayout) baseActivity.findViewById(R.id.llTollbar1);
        ivBackButton = (ImageButton) baseActivity.findViewById(R.id.ivBackButton);
        tvTitle = (TypefaceTextView) baseActivity.findViewById(R.id.tvTitle);

        ivBackButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackButton:
                baseActivity.onBackPressed();
                break;
            case R.id.tvTitle:

                titleButtonClick();
                break;

        }
    }

    public void titleButtonClick() {
    }


    public void setTitleButtonVisibilty(boolean visibility){
        if (visibility){
            tvTitle.setVisibility(View.VISIBLE);
        }
        else {
            tvTitle.setVisibility(View.GONE);
        }

    }

    public void setTitleText(String title){

        if (title!=null && !title.isEmpty()){
            tvTitle.setText(title);
        }
    }

    public  void setbackButtonVisibilty(boolean visibility){
        if (visibility){
            ivBackButton.setVisibility(View.VISIBLE);
        }
        else {
            ivBackButton.setVisibility(View.GONE);
        }
    }
    public void setToolbarVisibility(boolean visibility){
        if (visibility){
            llToolbar1.setVisibility(View.VISIBLE);
        }
        else {
            llToolbar1.setVisibility(View.GONE);
        }
    }
}
