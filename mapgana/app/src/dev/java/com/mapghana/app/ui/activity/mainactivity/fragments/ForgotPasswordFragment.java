package com.mapghana.app.ui.activity.mainactivity.fragments;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Forgot;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Valiations;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends AppBaseFragment {

    private TypefaceTextView tvSubmit;
    private EditText etEmail;

    public ForgotPasswordFragment() {
    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void initializeComponent() {
        getToolBar().setToolbarVisibilityTB(true);
        getToolBar().setTitleTextTB(getResources().getString(R.string.Forgot_password));

        tvSubmit = (TypefaceTextView) getView().findViewById(R.id.tvSubmit);
        etEmail = getView().findViewById(R.id.etEmail);
        tvSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
              /*  ((AppBaseActivity)getActivity()).changeFragment(new NewPasswordFragment(), true,
                        false, 0, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                        true);*/
                hideKeyboard();

                onSubmit();
                break;
        }
    }

    private void onSubmit() {
        if (Valiations.isEmailAddress(etEmail, true)) {
            if (ConnectionDetector.isNetAvail(getContext())) {
                displayProgressBar(false);
                RestClient restClient = new RestClient(getContext());
                restClient.callback(this).forgot(etEmail.getText().toString().trim());
            } else {
                displayToast(Constants.No_Internet);
            }
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_FORGOT) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Forgot forgot = gson.fromJson(s, Forgot.class);
                    if (forgot.getStatus() != 0) {
                        displayToast(forgot.getMessage());
                        getActivity().onBackPressed();
                    } else {
                        displayErrorDialog("Error", forgot.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

}
