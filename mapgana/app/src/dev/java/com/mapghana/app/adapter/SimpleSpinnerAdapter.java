package com.mapghana.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mapghana.R;
import com.mapghana.customviews.TypefaceTextView;

/**
 * Created by ubuntu on 5/1/18.
 */

public class SimpleSpinnerAdapter extends BaseAdapter {

    private Context context;
    private String string[];
    private String selGender;

    public String getSelGender() {
        return selGender;
    }

    public void setSelGender(int selGender) {
        this.selGender = string[selGender];
        notifyDataSetChanged();
    }

    public SimpleSpinnerAdapter(Context context, String[] string) {
        this.context = context;
        this.string = string;
    }

    @Override
    public int getCount() {
        if (string!=null && string.length>0){
            return string.length;
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.item_simple_spinner, parent, false);
        }
        TypefaceTextView  tvTitle=(TypefaceTextView)convertView.findViewById(R.id.tvTitle);
        ImageView imgDown=(ImageView)convertView.findViewById(R.id.imgDown);

        if (selGender!=null){
            if (selGender.equals(string[position])){
                imgDown.setVisibility(View.VISIBLE);
            }else {
                imgDown.setVisibility(View.GONE);
            }
        }
        else {
            imgDown.setVisibility(View.GONE);
        }
        tvTitle.setText(string[position]);
        return convertView;
    }

}