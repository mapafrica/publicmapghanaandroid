package com.mapghana.app.ui.activity.splash;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.util.PermissionManager;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppBaseActivity
{
    private static final String TAG = "SplashActivity";

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_splash;
    }

    @Override
    public void initializeComponent() {
    }

    private void checkPermission(){
        if (PermissionManager.areExplicitPermissionsRequired()) {
            List<String> required = PermissionManager.isAllPremissiongranted(SplashActivity.this);
            if (required != null && required.size() > 0) {
                PermissionManager.show(SplashActivity.this,
                        getResources().getString(R.string.app_name), required);
            } else {
//if (checkOverlayOn()) {
                goToNextActivity();
// }
            }
        } else {
            goToNextActivity();
        }
    }

    private void goToNextActivity(){
        displayLog(TAG, "goToNextActivity main ");
        if (checkGpsStatus()) {
            displayLog(TAG, "gps on: ");
            final Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                        // StartActivityWithFinish(MainActivity.class);
                        SessionManager.checkLogin(SplashActivity.this);
                        finish();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        PermissionManager.requestRunning=false;
        List<String> requiredPermission=new ArrayList<>();
        for (int i=0; i<grantResults.length; i++){
            if (grantResults[i]==-1){
                requiredPermission.add(permissions[i]);
            }
        }
        if (requiredPermission !=null && requiredPermission.size()>0){
            displayLog(TAG, "onRequestPermissionsResult: " );
            PermissionManager.show(SplashActivity.this, "Permission Required", requiredPermission);
        }
        /*else {
            displayLog(TAG, "goToNextActivity 333 " );
            goToNextActivity();
        }*/
    }


    private boolean checkGpsStatus(){
        LocationManager manager=(LocationManager)getSystemService(LOCATION_SERVICE);
        boolean gpsStatus=manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return gpsStatus;
    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog;
        alertDialog=new AlertDialog.Builder(SplashActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
                dialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayLog(TAG, "onResume: " );
        if (!checkGpsStatus()){
            showSettingsAlert();
        }else {
            checkPermission();
        }
        // setUplocationcheck();
    }

    @Override
    public void setExploreVisibility(boolean visibility) {

    }


 /*   public void setUplocationcheck() {
        if (mGoogleApiClient != null && locationRequest != null && mGoogleApiClient.isConnected()) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true); // this is the key ingredient
            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(SplashActivity.this, LOCATION_CHECK_RESOLUTION);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==LOCATION_CHECK_RESOLUTION){
            if (resultCode==RESULT_OK){
                displayToast("done");
            }
            else {
                displayErrorDialog("Gps", "Do Location in High accuracy");
            }
        }

        displayLog(TAG, "onActivityResult: nothing" );

    }*/
}