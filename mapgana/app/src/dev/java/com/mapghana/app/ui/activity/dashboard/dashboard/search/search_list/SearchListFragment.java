package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.FilterModel;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map.SingleItemOnMapFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.adapter.ItemListOrgAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.FilterFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchListFragment extends AppBaseFragment {

    private static final String TAG = "SearchListFragment";
    private TypefaceTextView tvTitle;
    private RecyclerView rvItems;
    private ImageButton ivFilter, ivBackButton;
    private List<GlobeSearch.DataBean> listData;
    private TypefaceEditText etSearch;
    private ItemListOrgAdapter itemListOrgAdapter;
    private String search_keyword, search_response;

    public void setSearch_keyword(String search_keyword) {
        this.search_keyword = search_keyword;
    }

    public void setSearch_response(String search_response) {
        this.search_response = search_response;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_category_item;
    }

    @Override
    public void initializeComponent() {
        init();
        tvTitle = (TypefaceTextView) getView().findViewById(R.id.tvTitle);
        rvItems = (RecyclerView) getView().findViewById(R.id.rvItems);
        ivFilter = (ImageButton) getView().findViewById(R.id.ivFilter);
        ivBackButton = (ImageButton) getView().findViewById(R.id.ivBackButton);
        etSearch = (TypefaceEditText) getView().findViewById(R.id.etSearch);
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        ivBackButton.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        tvTitle.setText(getString(R.string.Search_Results));
        listData = new ArrayList<>();
        itemListOrgAdapter = new ItemListOrgAdapter(this, listData, listData);
        rvItems.setAdapter(itemListOrgAdapter);
        if (search_response != null && search_response.length() > 0 && DefaulFilterSpf.getFilter(getContext()) == null) {
            Gson gson = new Gson();
            GlobeSearch globeSearch = gson.fromJson(search_response, GlobeSearch.class);
            listData.addAll(globeSearch.getData());
            itemListOrgAdapter.notifyDataSetChanged();
        } else if (search_keyword != null && search_keyword.length() > 0 && DefaulFilterSpf.getFilter(getContext()) != null) {
            // search_keyword=getArguments().getString(Constants.keyword);
            String s = DefaulFilterSpf.getFilter(getContext());
            Gson gson = new Gson();
            FilterModel filterModel = gson.fromJson(s, FilterModel.class);
            String city_id = "";
            String categoty_id = "";
            String lat = "";
            String log = "";
            String distance = "";
            if (filterModel.getCategoty_id() != null && !filterModel.getCategoty_id().equals("")) {
                categoty_id = filterModel.getCategoty_id();
            }
            if (filterModel.getCity_id() != null && !filterModel.getCity_id().equals("")) {
                city_id = filterModel.getCity_id();
            }
            if (filterModel.getLat() != 0) {
                lat = String.valueOf(filterModel.getLat());
            }
            if (filterModel.getLog() != 0) {
                log = String.valueOf(filterModel.getLog());
            }
            if (filterModel.getDistance() != null && !filterModel.getDistance().equals("")) {
                distance = filterModel.getDistance();
            }

            doRequest(search_keyword, categoty_id, city_id, lat, log, distance);
        }
        displayLog(TAG, "initializeComponent: ");
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (listData != null && listData.size() > 0) {
                    displayLog(TAG, "afterTextChanged:");
                    itemListOrgAdapter.getFilter().filter(s.toString());
                }
            }
        });

    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().lockDrawer(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackButton:
                getActivity().onBackPressed();
                DefaulFilterSpf.clearFilter(getContext());
                break;
            case R.id.ivFilter:
                ((AppBaseActivity) getActivity()).changeFragment(new FilterFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;
        }
    }

    @Override
    public void onAdapterClickListener(int id) {
        if (listData != null && listData.size() > 0) {
            displayLog(TAG, "onAdapterClickListener: " + id);
           /* Bundle bundle = new Bundle();
            bundle.putInt(Constants.post_id, id);*/
            SingleItemOnMapFragment detailsFragment = new SingleItemOnMapFragment();
            detailsFragment.setPost_id(id);
           // detailsFragment.setArguments(bundle);
            ((AppBaseActivity) getActivity()).changeFragment(detailsFragment, true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        }
    }

    private void doRequest(String keyword, String category_id, String city_id,
                           String lat, String log, String distance) {

        displayLog(TAG, "keyword: " + keyword);
        displayLog(TAG, "category_id: " + category_id);
        displayLog(TAG, "city_id: " + city_id);
        displayLog(TAG, "lat: " + lat);
        displayLog(TAG, "log: " + log);
        displayLog(TAG, "distance: " + distance);
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).searchFilter(keyword, category_id, city_id, lat, log, distance);
        } else {
            displayToast(Constants.No_Internet);
        }
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_SEarch_Filter) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GlobeSearch data = gson.fromJson(s, GlobeSearch.class);
                    if (data.getStatus() != 0) {
                        clearList();
                        displayLog(TAG, "onSuccessResponse before: " + listData.size());
                        listData.addAll(data.getData());
                        if (listData.size() > 0) {
                            displayLog(TAG, "onSuccessResponse after: " + listData.size());
                            itemListOrgAdapter.notifyDataSetChanged();
                        } else {
                            displayToast("No result found...");
                        }
                    } else {
                        displayErrorDialog("Error", data.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void clearList() {
        if (listData != null && listData.size() > 0) {
            listData.clear();
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }
}
