package com.mapghana.app.ui.sidemenu.Event;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.adapter.EventMediaListAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.dialog.PopUpEventDetail;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.utils.Constants;

public class EventDetailFragment extends AppBaseFragment implements MapboxMap.OnInfoWindowClickListener,
        AppBaseMapBox.OnMapLoaded,
        LocationServiceListner, MapboxMap.OnCameraIdleListener, EventListCallback {

    private RecyclerView rvPreviousMedia;
    private FrameLayout frameLayout;
    private float zoomLevel;
    private MapboxMap mapboxMap;
    private Marker marker_current_loc;
    private double log = 78.008, lat = 27.176;
    private String mEventType = "multiEvent";
    private String mTitle;
    private EventDetail eventDetail;

    public static EventDetailFragment newInstance(String title, EventDetail item) {
        EventDetailFragment fragment = new EventDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constants.TITLE, title);
        args.putSerializable(Constants.ITEMS, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mTitle = getArguments().getString(Constants.TITLE);
            eventDetail = (EventDetail) getArguments().getSerializable(Constants.ITEMS);
        }

    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_event_detail_page;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initializeComponent() {
        initToolBar();
        initMap();
        bindArticleBanner(eventDetail);
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(mTitle);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void bindArticleBanner(EventDetail eventDetail) {
        View view = getView();
        if (view != null) {
            EventDetailBannerLayout articleBannerLayout = (EventDetailBannerLayout) view.findViewById(R.id
                    .event_detail_banner_layout);
            articleBannerLayout.bind(eventDetail, mEventType, true, getActivity());

        }
    }

    private void initMap() {
        if (getActivity() != null) {
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnInfoWindowClickListener(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnCameraIdleListeners(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());
        }
    }


    @Override
    public void onCameraIdle() {

    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        return false;
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(new LatLng(lat, log));
        ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.ic_launcher, new LatLng(lat, log),
                Constants.You, "");

    }

    @Override
    public void userLocationChange(Location location) {

    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    @Override
    public void onClickEvent(EventDetail model) {

    }

    @Override
    public void onPopUpClickEvent(EventDetail model) {

    }

    @Override
    public void onCloseIcon() {

    }

}
