package com.mapghana.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ubuntu on 30/1/18.
 */

public class OpeningHour {

    @SerializedName("monday-friday")
    private MondayfridayBean mondayfriday;
    private SaturdayBean saturday;
    private SundayBean sunday;

    public MondayfridayBean getMondayfriday() {
        return mondayfriday;
    }

    public void setMondayfriday(MondayfridayBean mondayfriday) {
        this.mondayfriday = mondayfriday;
    }

    public SaturdayBean getSaturday() {
        return saturday;
    }

    public void setSaturday(SaturdayBean saturday) {
        this.saturday = saturday;
    }

    public SundayBean getSunday() {
        return sunday;
    }

    public void setSunday(SundayBean sunday) {
        this.sunday = sunday;
    }

    public static class MondayfridayBean {

        private String start;
        private String end;

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }
    }

    public static class SaturdayBean {
        private String start;
        private String end;

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }
    }

    public static class SundayBean {

        private String start;
        private String end;

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }
    }
}
