package com.mapghana.app.ui.sidemenu.Object;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.brsoftech.customtimepicker.TimePickerDialog;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.adapter.AddEventGalleryAdapter;
import com.mapghana.app.adapter.EventMediaListAdapter;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.model.EventGallery;
import com.mapghana.app.model.Login;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.MediaAddAdpterListener;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.mapghana.app.rest.BaseArguments.login;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddObjectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddObjectFragment extends AppBaseFragment implements View.OnClickListener, AppBaseMapBox.OnMapLoaded,  TimePickerDialog.TimePickerListner, MediaAddAdpterListener{

    private EditText etDeadDrop, etName, etReference, etAbout, etPassword;
    private Spinner spinnerObjectType;
    private SwitchCompat switchCompatPostVisibility, switchCompatSchedule, switchCompatPasswordProtect, switchCompatOneTimeAccess;
    private RecyclerView recyclerViewGallery;
    private RadioGroup radioGroupLocationCategory;
    private RadioButton radioButtonCategory;
    private FrameLayout frameLayoutLocationMap;
    private TypefaceTextView submit, txtHint, etStartDate, etStartTime, etEndDate, etEndTime;
    private SimpleSpinnerAdapter objectTypeAdapter;
    private TextInputLayout textInputLayoutSizeDeadDrop, textInputLayoutPasswordProtect;
    private double log = 78.008, lat = 27.176;
    private LinearLayout llSchedule;
    private List<String> currentMediaGallery;
    private AddEventGalleryAdapter currentGalleryAdapter;
    private String locationReference;
    private RestClient restClient;
    private static final int REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE = 887;
    private List<String> video_mime_List;
    private TimePickerDialog timePickerDialog;
    private int time_type;
    private CustomDatePickerDialog.OnDateListener onDateStartListener, onDateEndListener;
    private String scheduling = "0", postVisibilty = "0", passwordProtect = "0", onTimePassword = "0";

    public AddObjectFragment() {
        // Required empty public constructor
    }

    public static AddObjectFragment newInstance() {
        AddObjectFragment fragment = new AddObjectFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_object;
    }

    @Override
    public void initializeComponent() {
        initToolBar();
        initViewId(getView());
        initSpinnerObjectType();
        initRecycleLayoutManager();
        initMap();
        initListeners();
        restClient = new RestClient(getContext());
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle("Object");
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    private void initViewId(View view) {

        etDeadDrop = view.findViewById(R.id.object_dead_drop);
        etName = view.findViewById(R.id.object_name);
        etReference = view.findViewById(R.id.object_reference);
        etAbout = view.findViewById(R.id.object_about);
        etPassword = view.findViewById(R.id.object_password_protect);
        etStartDate = view.findViewById(R.id.start_date);
        etStartTime = view.findViewById(R.id.start_time);
        etEndDate = view.findViewById(R.id.end_date);
        etEndTime = view.findViewById(R.id.end_time);
        txtHint = view.findViewById(R.id.txt_hint);
        spinnerObjectType = view.findViewById(R.id.spinner_type);
        switchCompatPostVisibility = view.findViewById(R.id.post_visibility);
        switchCompatPasswordProtect = view.findViewById(R.id.password_protect);
        switchCompatOneTimeAccess = view.findViewById(R.id.switch_one_time_access);
        switchCompatSchedule = view.findViewById(R.id.post_scheduling);
        recyclerViewGallery = view.findViewById(R.id.rv_media);
        radioGroupLocationCategory = view.findViewById(R.id.radio_group_location_category);
        frameLayoutLocationMap = view.findViewById(R.id.map_contanier);

        textInputLayoutSizeDeadDrop = view.findViewById(R.id.text_input_layout_size_deaddrop);
        textInputLayoutPasswordProtect = view.findViewById(R.id.text_input_layout_password_protect);

        llSchedule = view.findViewById(R.id.ll_schedule);

        submit = view.findViewById(R.id.tv_add_object);
    }

    private void initMap() {
        if (getActivity() != null) {
            ((DashboardActivity) getActivity()).appBaseMapBox.setOnMapLoaded(this);
            ((DashboardActivity) getActivity()).appBaseMapBox.loadMap(getChildFm());
        }
    }

    private void initListeners() {
        timePickerDialog = new TimePickerDialog(getContext(), Calendar.getInstance(), this);
        etStartDate.setOnClickListener(this);
        etEndDate.setOnClickListener(this);
        etStartTime.setOnClickListener(this);
        etEndTime.setOnClickListener(this);
        submit.setOnClickListener(this);
        radioGroupLocationCategory.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_general) {
                locationReference = getIfLocationReference(R.id.radio_general);
            } else if (checkedId == R.id.radio_specific) {
                locationReference = getIfLocationReference(R.id.radio_specific);
            }
        });

        spinnerObjectType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (objectTypeAdapter != null) {
                    objectTypeAdapter.setSelGender(position);

                    if (objectTypeAdapter.getSelGender().equalsIgnoreCase("DeadDrop")) {
                        textInputLayoutSizeDeadDrop.setVisibility(View.VISIBLE);
                        txtHint.setText(getString(R.string.hint_format, getHintInfo(objectTypeAdapter.getSelGender())));
                    } else {
                        textInputLayoutSizeDeadDrop.setVisibility(View.GONE);
                    }


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        switchCompatPasswordProtect.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                textInputLayoutPasswordProtect.setVisibility(View.VISIBLE);
                passwordProtect = "1";
            }
            else {
                passwordProtect = "0";
                textInputLayoutPasswordProtect.setVisibility(View.GONE);
            }
        });

        switchCompatPostVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                postVisibilty = "1";
            }
            else {
                postVisibilty = "0";
            }
        });
        switchCompatSchedule.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                llSchedule.setVisibility(View.VISIBLE);
                scheduling = "1";
            } else {
                scheduling = "0";
                llSchedule.setVisibility(View.GONE);
            }
        });

        switchCompatOneTimeAccess.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                onTimePassword = "1";
            } else {
                onTimePassword = "0";
            }
        });

         onDateStartListener  = date -> {
            if (date != null) {
                SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
               etStartDate.setText(format.format(date));
            }
        };
        onDateEndListener  = date -> {
            if (date != null) {
                SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
                etStartDate.setText(format.format(date));
            }
        };

    }

    private String getIfLocationReference(int radioId) {
        switch (radioId) {
            case R.id.radio_general:
                return "general";
            case R.id.radio_specific:
                return "specific";
        }
        return "";
    }

    @Override
    public void onMapLoadedd(MapboxMap mapboxMap) {
        ((DashboardActivity) getActivity()).appBaseMapBox.zoomMap(new LatLng(lat, log));
        ((DashboardActivity) getActivity()).appBaseMapBox.addMarker(getContext(), R.mipmap.ic_launcher, new LatLng(lat, log),
                Constants.You, "");
    }

    private void initSpinnerObjectType() {
        String[] strings = getResources().getStringArray(R.array.object_type);
        objectTypeAdapter = new SimpleSpinnerAdapter(getContext(), strings);
        objectTypeAdapter.setSelGender(0);
        spinnerObjectType.setAdapter(objectTypeAdapter);
    }

    private void initRecycleLayoutManager() {
        currentMediaGallery = new ArrayList<>();
        recyclerViewGallery.setHasFixedSize(true);
        currentGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "previous_media", this, currentMediaGallery);
        recyclerViewGallery.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewGallery.setAdapter(currentGalleryAdapter);

    }

    @Override
    public void onAddClick(String type) {
        multipleGalleryMedia();
    }

    @Override
    public void onDeleteClick(int position, String type) {

    }

    private String getHintInfo(String key) {
        if (key.equalsIgnoreCase("deaddrop")) {
            return "deaddrop";
        } else if (key.equalsIgnoreCase("zi network")) {
            return "zi network";
        } else if (key.equalsIgnoreCase("relic")) {
            return "relic";
        } else if (key.equalsIgnoreCase("node")) {
            return "node";
        } else if (key.equalsIgnoreCase("powerlync")) {
            return "powerlync";
        }
        return "";

    }

    private void multipleGalleryMedia() {
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE) {
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                currentMediaGallery.addAll(paths);
                refreshMediaGalleryAdapter();
            }
        }
    }

    private void refreshMediaGalleryAdapter() {
        if (currentMediaGallery != null) {
            currentGalleryAdapter.notifyDataSetChanged();
        }
    }

    private void onDobDialog(CustomDatePickerDialog.OnDateListener onDateListener) {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        if (dialog != null) {
            dialog.create(getContext(), true).callback(onDateListener).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_date:
                onDobDialog(onDateStartListener);
                break;
            case R.id.end_date:
                onDobDialog(onDateEndListener);
                break;
            case R.id.start_time:
                displayTimePickerDialog(1);
                break;
            case R.id.end_time:
                displayTimePickerDialog(2);
                break;
            case R.id.tv_add_object:
                onApiCall();
                break;
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_OBJECT) {
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                dismissProgressBar();
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        super.onFailResponse(apiId, error);
        dismissProgressBar();
        Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();
    }
    private void displayTimePickerDialog(int type) {
        time_type = type;
        timePickerDialog.show();

    }

    private void onApiCall() {

        displayProgressBar(false);
        if (ConnectionDetector.isNetAvail(getContext())) {


            HashMap<String, String> map = new HashMap<>();
            int ID = 0;
            Gson gson = new Gson();
            String userInfo = SessionManager.getUserInfoResponse(getContext());
            if (!userInfo.equals("")) {
                Login login = gson.fromJson(userInfo, Login.class);
                ID = login.getData().getId();
            }

            map.put(BaseArguments.id, String.valueOf(ID));
            map.put(BaseArguments.user_id, String.valueOf(ID));
            map.put(BaseArguments.name, etName.getText().toString());
            map.put(BaseArguments.type, objectTypeAdapter.getSelGender());
            map.put(BaseArguments.scheduling, scheduling);
            map.put(BaseArguments.reference, etReference.getText().toString());
            map.put(BaseArguments.about, etAbout.getText().toString());
            map.put(BaseArguments.entry_period, etStartDate.getText().toString() +" "+etStartTime.getText().toString());
            map.put(BaseArguments.exit_period, etEndDate.getText().toString() +" "+etEndTime.getText().toString());
            map.put(BaseArguments.location_profile, locationReference);
            map.put(BaseArguments.post_visibility, postVisibilty);
            map.put(BaseArguments.password_protected, passwordProtect);
            map.put(BaseArguments.password, etPassword.getText().toString());
            map.put(BaseArguments.one_time_use, etPassword.getText().toString());

            MultipartBody.Part[] multiCurrentGalleryPart = new MultipartBody.Part[currentMediaGallery.size()];
            for (int i = 0; i < currentMediaGallery.size(); i++) {
                String url = currentMediaGallery.get(0);
                FilePathHelper filePathHelper = new FilePathHelper();
                FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
                String mimeType = fileInformation.getMimeType();
                if (video_mime_List.contains(mimeType)) {
                    File file = new File(currentMediaGallery.get(i));
                    RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                    multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.current_media_gallery + "[" + i + "]", file.getName(), media);

                } else {
                    File file = new File(currentMediaGallery.get(i));
                    RequestBody media = RequestBody.create(MediaType.parse("image/*"), file);
                    multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.current_media_gallery + "[" + i + "]", file.getName(), media);
                }
            }
            restClient.callback(this).postAddObject(getContext(), RetrofitUtils.createMultipartRequest(map), multiCurrentGalleryPart);
        } else {
            displayToast("Please check internet connection");
        }
    }


    @Override
    public void OnDoneButton(Dialog dialog, Calendar c) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(c.getTime());

        if (time_type == 1) {
            etStartTime.setText(time);
        }
        if (time_type == 2) {
            etEndTime.setText(time);
        }
    }

    @Override
    public void OnCancelButton(Dialog dialog) {
        timePickerDialog.dismiss();
    }
}
