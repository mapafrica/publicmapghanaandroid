package com.mapghana.app.model;

/**
 * Created by ubuntu on 16/1/18.
 */

public class ContactUs
{

    /**
     * code : 200
     * error :
     * status : 1
     * message : successfully
     * data : {"email":"info@mapghana.com","phone":"0244658978","mobile":"0302542364","address":"<p>First Floor<\/p>\r\n\r\n<p>Queensland Building<\/p>\r\n\r\n<p>Haatso, Accra<\/p>"}
     */

    private int code;
    private String error;
    private int status;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * email : info@mapghana.com
         * phone : 0244658978
         * mobile : 0302542364
         * address : <p>First Floor</p>

         <p>Queensland Building</p>

         <p>Haatso, Accra</p>
         */

        private String email;
        private String phone;
        private String mobile;
        private String address;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
