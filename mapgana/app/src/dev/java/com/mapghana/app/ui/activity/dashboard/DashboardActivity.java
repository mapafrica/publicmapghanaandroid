package com.mapghana.app.ui.activity.dashboard;

import android.app.ActivityManager;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.services.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.services.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.services.commons.models.Position;
import com.mapghana.R;
import com.mapghana.app.App;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.helpers.navigation.NavigationViewHandler;
import com.mapghana.app.model.Ads;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.spf.TimeManager;
import com.mapghana.app.ui.activity.ads.AdsActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.DashboardFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.navigation.NavigationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map.SingleItemOnMapFragment;
import com.mapghana.app.ui.sidemenu.individual.viewdetails.ViewDetailsFragment;
import com.mapghana.base.BaseFragment;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppBaseActivity implements LocationServiceListner {

    private static final String TAG = "DashboardActivity";
    public NavigationViewHandler navigationViewHandler;
    public AppBaseMapBox appBaseMapBox;

    private Location mLocation;
    public double mCurrentLongitude = 0;
    public double mCurrentLatitude = 0;
    private LocationRequest locationRequest;
    private GoogleApiClient mGoogleApiClient;
    private int flag = 0;
    private Handler handler;
    private Runnable runnable;
    private final long TWO_MINUTE = 1000 * 60 * 2;
    private final long FIVE_MINUTE = 1000 * 60 * 5;

    private Handler backStackHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                BaseFragment baseFragment = getLatestFragment();
                if (baseFragment == null) {
                    return;
                }
                navigationViewHandler.onCreateViewFragment(baseFragment);
                baseFragment.viewCreateFromBackStack();
            }
        }
    };

    FragmentManager.OnBackStackChangedListener onBackStackChangedListener = () -> {
        backStackHandler.removeMessages(1);
        backStackHandler.sendEmptyMessageDelayed(1, 100);
    };

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void beforeSetContentView() {
        Mapbox.getInstance(this, getString(R.string.mapbox_api_token));
    }

    @Override
    public void initializeComponent() {
        navigationViewHandler = new NavigationViewHandler(this);
        navigationViewHandler.findViewIds();
        getFm().addOnBackStackChangedListener(onBackStackChangedListener);
        appBaseMapBox = new AppBaseMapBox();
        if (SessionManager.getProfileImage(DashboardActivity.this) != null
                && !SessionManager.getProfileImage(DashboardActivity.this).equals("")) {
            setHeaderProfilePic(SessionManager.getProfileImage(DashboardActivity.this)
                    , R.mipmap.user_profile_round_dummy);
        }
        if (SessionManager.getAddress(DashboardActivity.this) != null
                && !SessionManager.getAddress(DashboardActivity.this).equals("")) {
            setNavLocationText(SessionManager.getAddress(DashboardActivity.this));
        }
        if (SessionManager.getUname(DashboardActivity.this) != null && !SessionManager.getUname(DashboardActivity.this).equals("")) {
            setUserName(SessionManager.getUname(DashboardActivity.this));
        }

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                showAds();
                TimeManager.saveLastTime(DashboardActivity.this, System.currentTimeMillis() + FIVE_MINUTE);
                waitForSomeTime(this, FIVE_MINUTE);
            }
        };
        TimeManager.saveLastTime(DashboardActivity.this, System.currentTimeMillis() + TWO_MINUTE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (handler != null) {
            calculateTime();
        }
        getLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(runnable);

        }
    }

    private void calculateTime() {
        long time;
        time = TimeManager.getLastTime(this);
        long diffTime;

        diffTime = time - System.currentTimeMillis();
        if (diffTime < 0) {
            diffTime = 0;
        }
        waitForSomeTime(runnable, diffTime);
    }

    private void waitForSomeTime(Runnable runnable, long minuts) {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, minuts);
    }

    private void showAds() {
        getAds();
    }


    private void getAds() {
        RestClient restClient = new RestClient(this);
        restClient.callback(this).ads();
    }

    private void getLocation() {
        ((App) getApplication()).setLocationServiceListner(this);
        mLocation = ((App) getApplication()).mLocation;
        if (ConnectionDetector.isNetAvail(this)) {

            if (!isMyServiceRunning()) {

                startService(new Intent(this, Locationservice.class));
            }
            if (mLocation != null) {
                userLocationChange(mLocation);
            }
        }
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName().equals(service1.service.getPackageName())
                    && Locationservice.class.getName().equals(service1.service.getClassName()))) {

                return true;
            }
        }
        return false;
    }

    private void reverseGeoCoding(LatLng latLng) {
        try {
            MapboxGeocoding mapboxGeocoding = new MapboxGeocoding.Builder()
                    .setAccessToken(getString(R.string.mapbox_api_token))
                    .setCoordinates(Position.fromCoordinates(latLng.getLongitude(), latLng.getLatitude()))
                    .setGeocodingType(GeocodingCriteria.TYPE_ADDRESS)
                    .build();;
            displayProgressBar(false);
            mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                    dismissProgressBar();
                    List<CarmenFeature> results = response.body().getFeatures();
                    if (results.size() > 0) {
                        Position firstResultPos = results.get(0).asPosition();
                        String title = results.get(0).getPlaceName();
                        if (title != null && title.length() > 0) {
                            String[] array = title.split(",");
                            setNavLocationTextVisibilty(true);
                            setNavAddressText(array[array.length - 1]);
                        }
                    } else {
                        setNavLocationTextVisibilty(false);
                    }
                    changeFragment();
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    throwable.printStackTrace();
                    dismissProgressBar();
                    setNavLocationTextVisibilty(false);
                    changeFragment();
                }
            });
        } catch (ServicesException e) {
            e.printStackTrace();
            dismissProgressBar();
            //  displayToast("Something went wrong...");
            changeFragment();
        }

    }

    private void changeFragment() {
        changeFragment(new DashboardFragment(), false, true, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, false);

        onBackStackChangedListener.onBackStackChanged();

    }

    @Override
    public int getFragmentContainerResourceId() {
        return R.id.frame_container;
    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {
        navigationViewHandler.setNavToggleButtonVisibilty(visibilty);
    }

    @Override
    public void setNavTitle(String title) {
        navigationViewHandler.setNavTitle(title);
    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {
        navigationViewHandler.setBussinessTypeLayoutVisibuility(visibilty);
    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {
        navigationViewHandler.setSearchButtonVisibuility(visibilty);
    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {
        navigationViewHandler.setNavigationToolbarVisibilty(visibilty);
    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {
        navigationViewHandler.setBackButtonVisibilty(visibilty);
    }

    @Override
    public void setHeaderProfilePic(String uri, int res) {
        navigationViewHandler.setHeaderProfilePic(uri, res);
    }

    @Override
    public void setUserName(String Name) {
        navigationViewHandler.setUserName(Name);
    }

    @Override
    public void setNavLocationText(String Name) {
        navigationViewHandler.setNavLocationText(Name);
    }

    @Override
    public void lockDrawer(boolean visibilty) {
        navigationViewHandler.lockDrawer(visibilty);
    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {
        navigationViewHandler.setNavTitleTextVisibilty(visibilty);
    }

    @Override
    public void setNavLocationTextVisibilty(boolean visibilty) {
        navigationViewHandler.setNavLocationTextVisibilty(visibilty);
    }

    @Override
    public void setNavAddressText(String title) {
        navigationViewHandler.setNavAddressText(title);
    }

    @Override
    public void setExploreVisibility(boolean visibility) {

    }


    @Override
    public String getPostType() {
        return navigationViewHandler.getPostType();
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        if (apiId == ApiIds.ID_ADS) {
            TimeManager.saveFirst(this);
            if (response.isSuccessful()) {
                Gson gson = new Gson();
                try {
                    Ads ads = gson.fromJson(response.body().string(), Ads.class);
                    if (ads.getCode() != 0) {
                        String path = ads.getData().getVideo();
                        if (path != null && path.length() > 0) {
                            if (RestClient.API_COUNT == 0) {
                                Intent intent = new Intent(this, AdsActivity.class);
                                intent.putExtra("video_path", path);
                                intent.putExtra("ads_path", ads.getData().getAds_url());
                                startActivity(intent);
                            }

                        } else {
                            displayLog(TAG, ads.getMessage());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayLog(TAG, e.getMessage());

                }
            } else {
                displayLog(TAG, response.message());
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        TimeManager.saveFirst(this);

    }

    @Override
    public void userLocationChange(Location location) {
        mLocation = location;
        if (mLocation != null) {
            mCurrentLatitude = location.getLatitude();
            mCurrentLongitude = location.getLongitude();
            if (mCurrentLatitude != 0 && mCurrentLongitude != 0 && flag == 0) {
                flag = 1;
                reverseGeoCoding(new LatLng(mCurrentLatitude, mCurrentLongitude));
            } else {
            }

            BaseFragment baseFragment = getLatestFragment();
            if (baseFragment != null && baseFragment instanceof DashboardFragment) {
                ((DashboardFragment) baseFragment).userLocationChange(location);
            } else if (baseFragment instanceof PostDetailFragment) {
                ((PostDetailFragment) baseFragment).userLocationChange(location);
            } else if (baseFragment instanceof ViewDetailsFragment) {
                ((ViewDetailsFragment) baseFragment).userLocationChange(location);
            } else if (baseFragment instanceof SingleItemOnMapFragment) {
                ((SingleItemOnMapFragment) baseFragment).userLocationChange(location);
            } else if (baseFragment instanceof NavigationFragment) {
                ((NavigationFragment) baseFragment).userLocationChange(location);
            }
           /* else if (baseFragment instanceof AddLocationFragment){
                ((AddLocationFragment)baseFragment).userLocationChange(location);
            }*/
        }
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {
        this.locationRequest = locationRequest;
        mGoogleApiClient = googleApiClient;
        setGoogleApiClient(googleApiClient);
        setLocationRequest(locationRequest);
    }

    public LocationRequest getLocationRequest() {
        return locationRequest;
    }

    public void setLocationRequest(LocationRequest locationRequest) {
        this.locationRequest = locationRequest;
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.mGoogleApiClient = googleApiClient;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onDestroy() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        stopService(new Intent(this, Locationservice.class));
        super.onDestroy();
    }
}
