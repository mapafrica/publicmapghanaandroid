package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.writereview;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.RatingBar;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Login;
import com.mapghana.app.model.SendReview;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Valiations;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WriteReviewFragment extends AppBaseFragment implements RatingBar.OnRatingBarChangeListener {

    private RatingBar ratingBar;
    private TypefaceEditText etTitle, etMessage;
    private TypefaceTextView tvWriteReview, tvRating;

    private RestClient restClient;

    private static final String TAG = "WriteReviewFragment";
    private int post_id;

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_write_review;
    }

    @Override
    public void initializeComponent() {
        init();
        restClient = new RestClient(getContext());
        ratingBar = (RatingBar) getView().findViewById(R.id.ratingBar);
        etTitle = (TypefaceEditText) getView().findViewById(R.id.etTitle);
        etMessage = (TypefaceEditText) getView().findViewById(R.id.etMessage);
        tvRating = (TypefaceTextView) getView().findViewById(R.id.tvRating);
        tvWriteReview = (TypefaceTextView) getView().findViewById(R.id.tvWriteReview);

        tvWriteReview.setOnClickListener(this);
        ratingBar.setOnRatingBarChangeListener(this);
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Write_a_Review));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvWriteReview:
                hideKeyboard();
                onWriteReview();
                break;
        }
    }

    private void onWriteReview() {

        if (Valiations.hasText(etTitle) && Valiations.hasText(etMessage)) {
            if (tvRating.getText().equals("")) {
                displayToast("Select stars to rate");
                return;
            }
            //   if (getArguments()!=null) {
            //  int post_id= getArguments().getInt(Constants.post_id);
            if (ConnectionDetector.isNetAvail(getContext())) {
                displayProgressBar(false);

                int ID = 0;
                String userInfo = SessionManager.getUserInfoResponse(getContext());
                if (!userInfo.equals("")) {
                    Gson gson = new Gson();
                    Login login = gson.fromJson(userInfo, Login.class);
                    ID = login.getData().getId();
                }

                String title = etTitle.getText().toString().trim();
                String message = etMessage.getText().toString().trim();
                String rating = tvRating.getText().toString().trim();

                HashMap<String, String> map = new HashMap<>();
                map.put(BaseArguments.user_id, String.valueOf(ID));
                map.put(BaseArguments.post_id, String.valueOf(post_id));
                map.put(BaseArguments.title, title);
                map.put(BaseArguments.message, message);
                map.put(BaseArguments.rating, rating);

                restClient.callback(this).post_send_review(RetrofitUtils.createMultipartRequest(map));
            } else {
                displayToast(Constants.No_Internet);
            }
           /* }
            else {
                displayToast("Something went wrong");
            }*/
        }
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        tvRating.setText("" + rating);
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_post_send_review) {
                try {

                    String s = response.body().string();
                    Gson gson = new Gson();
                    SendReview review = gson.fromJson(s, SendReview.class);
                    if (review.getStatus() != 0) {
                        clearAllFields();
                        displayToast(review.getMessage());
                        getActivity().onBackPressed();
                    } else {
                        displayErrorDialog("Error", review.getMessage());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    private void clearAllFields() {
        etTitle.setText("");
        etMessage.setText("");
        tvRating.setText("");
        ratingBar.setRating(0);
    }
}
