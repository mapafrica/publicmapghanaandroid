package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EventIdListingForGroup implements Serializable {
    private int status;
    private String message;
    private List<EventIdListingForGroupData> data;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<EventIdListingForGroupData> getData() {
        return data;
    }
}
