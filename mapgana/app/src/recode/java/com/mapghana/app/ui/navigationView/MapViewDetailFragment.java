package com.mapghana.app.ui.navigationView;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.utils.ColorUtils;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.GetDetails;
import com.mapghana.app.model.IndividualPosts;
import com.mapghana.app.model.ObjectDetail;
import com.mapghana.app.model.ObjectListingModel;
import com.mapghana.app.model.ObjectMapModel;
import com.mapghana.app.model.OrganizationPosts;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.ui.sidemenu.Object.ObjectListing;
import com.mapghana.app.utils.Constants;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;


public class MapViewDetailFragment extends AppBaseFragment implements OnMapReadyCallback, PermissionsListener, LocationServiceListner {

    private static final String ARG_TITLE = "title";
    private static final String ARG_ID = "id";
    private static final String ARG_TYPE = "type";
    private String mId;
    private String mType;

    private boolean isMapReady;
    private MapView mapView;
    private MapboxMap map;
    private GetDetails data;
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;

    private ArrayList<IndividualPosts.DataBean> individualDataList;
    private ArrayList<OrganizationPosts.DataBean> organizationDataList;
    private ArrayList<ObjectDetail> objectDataList;
    private SymbolManager symbolManager;
    private CircleManager circleManager;
    private List<Symbol> symbolLayerIconFeatureList;
    private List<SymbolOptions> options;
    private Style style;
    double coef;

    public static MapViewDetailFragment newInstance(String title, String id, String type) {
        MapViewDetailFragment fragment = new MapViewDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_ID, id);
        args.putString(ARG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
        coef = 100 * 0.0000089;

    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_map_view_detail;
    }

    @Override
    public void initializeComponent() {

        individualDataList = new ArrayList<>();
        organizationDataList = new ArrayList<>();
        objectDataList = new ArrayList<>();
        if (getArguments() != null) {
            String mTitle = getArguments().getString(ARG_TITLE);
            mId = getArguments().getString(ARG_ID);
            mType = getArguments().getString(ARG_TYPE);
            init(mTitle);
        }


        if (Constants.INDIVIDUALS.equalsIgnoreCase(mType)) {
            if (!TextUtils.isEmpty(mId)) {
                if (ConnectionDetector.isNetAvail(getContext())) {
                    displayProgressBar(false);
                    RestClient restClient = new RestClient(getContext());

                    restClient.callback(this).post_listing_Individuals(mId);
                } else {
                    displayToast(Constants.No_Internet);
                }
            } else {
                getActivity().onBackPressed();
            }

        } else if (Constants.ORGANISATION.equalsIgnoreCase(mType)) {
            if (!TextUtils.isEmpty(mId)) {
                if (ConnectionDetector.isNetAvail(getContext())) {
                    displayProgressBar(false);
                    RestClient restClient = new RestClient(getContext());
                    restClient.callback(this).post_listing_organizations(mId);
                } else {
                    displayToast(Constants.No_Internet);
                }
            } else {
                getActivity().onBackPressed();
            }

        } else if (Constants.SEARCH.equalsIgnoreCase(mType)) {
            if (!TextUtils.isEmpty(mId)) {
                if (ConnectionDetector.isNetAvail(getContext())) {
                    displayProgressBar(false);
                    RestClient restClient = new RestClient(getContext());
                    restClient.callback(this).getDetailsOfOnePost(mId);
                } else {
                    displayToast(Constants.No_Internet);
                }
            } else {
                getActivity().onBackPressed();
            }

        }else if (Constants.OBJECT.equalsIgnoreCase(mType)) {
            if (!TextUtils.isEmpty(mId)) {
                if (ConnectionDetector.isNetAvail(getContext())) {
                    displayProgressBar(false);
                    String url = BaseArguments.OBJECT_DATA_CATEGORY_WISE+""+mId;
                    RestClient restClient = new RestClient(getContext());
                    restClient.callback(this).getDetailsOfOneCatgoryObject(url);
                } else {
                    displayToast(Constants.No_Internet);
                }
            } else {
                getActivity().onBackPressed();
            }

        }

        mapView = getView().findViewById(R.id.mapView);
        mapView.getMapAsync(this);

    }

    private void init(String title) {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(title);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);

    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.map = mapboxMap;
        isMapReady = true;

        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            this.style = style;

            symbolManager = new SymbolManager(mapView, map, style);
            circleManager = new CircleManager(mapView, map, style);
            symbolManager.setIconAllowOverlap(false);  //your choice t/f
            symbolManager.setTextAllowOverlap(false);  //your choice t/f
            enableLocationComponent(style);
            if (mType.equalsIgnoreCase(Constants.INDIVIDUALS)) {
                setMarkerIndividual(individualDataList);
            } else if (mType.equalsIgnoreCase(Constants.ORGANISATION)) {
                setMarkerOrganization(organizationDataList);
            }

            Location location = Locationservice.sharedInstance().getLastLocation();
            if (location != null) {
                Point originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
                map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originPoint.latitude(), originPoint.longitude()), 8));
            }

            symbolManager.addClickListener(symbol -> {
                String[] str = symbol.getIconImage().split("-");
                changeFragment(Integer.valueOf(str[1]));
            });

        });
    }

    private void changeFragment(int post_id) {
        if (post_id == 0) {
            return;
        }
        PostDetailFragment detailsFragment = new PostDetailFragment();
        detailsFragment.setPost_id(post_id);
        try {
            getDashboardActivity().changeFragment(detailsFragment, true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = map.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(map.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();

        }
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            Gson gson = new Gson();
            if (apiId == ApiIds.ID_POSTS_Filter_organization) {
                try {

                    String responseOrganization = response.body().string();
                    OrganizationPosts items = gson.fromJson(responseOrganization, OrganizationPosts.class);
                    if (items.getStatus() != 0 && items.getData().size() > 0) {
                        showOrganizationDetail(items.getData());
                    } else {
                        displayToast("No post found");
                        getActivity().onBackPressed();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_POSTS_Filter_individual) {
                try {
                    String responseIndividual = response.body().string();
                    IndividualPosts items = gson.fromJson(responseIndividual, IndividualPosts.class);
                    if (items.getStatus() != 0 && items.getData().size() > 0) {
                        showIndividualDetail(items.getData());
                    } else {
                        displayToast("No post found");
                        getActivity().onBackPressed();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_POSTS_Details_1) {
                try {
                    String detailResponse = response.body().string();
                    updateUI(detailResponse);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }

            if (apiId == ApiIds.ID_OBJECT_CATEGORY_WISE_DATA) {
                try {

                    String responseObject = response.body().string();
                    ObjectMapModel items = gson.fromJson(responseObject, ObjectMapModel.class);
                    if (items.getStatus() != 0 && items.getData().size() > 0) {
                        showObjectDetail(items.getData());
                    } else {
                        displayToast("No post found");
                        getActivity().onBackPressed();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void updateUI(String s) {
        if (!isVisible()) return;
        Gson gson = new Gson();
        data = gson.fromJson(s, GetDetails.class);
        if (data.getStatus() != 0) {
            symbolLayerIconFeatureList = new ArrayList<>();
            int drawableResourceId = 0;
            double latitude = data.getData().getLat();
            double longitude = data.getData().getLog();
            LatLng latLng = null;
            try {
                latLng = new LatLng(latitude, longitude);
            } catch (IllegalArgumentException e) {
            }

            String name = data.getData().getName();
            String address = data.getData().getAddress();
            getNavHandler().setNavTitle(name);
            String category = "";
            if (data.getData().getPost_for() != null &&
                    data.getData().getPost_for().equalsIgnoreCase(Constants.individuals)) {
                String status = "";
                String gender = "";
                category = "Occupation: " + data.getData().getCategory().getName();
                status = data.getData().getStatus().trim();
                gender = data.getData().getGender().trim();
                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        drawableResourceId = R.mipmap.individual_green_gril;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        drawableResourceId = R.mipmap.individual_red_girl;
                    } else {
                        drawableResourceId = R.mipmap.indi_girl_gray;
                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        drawableResourceId = R.mipmap.individual_green_man;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        drawableResourceId = R.mipmap.individual_red_man;
                    } else {
                        drawableResourceId = R.mipmap.individual_gray_man;
                    }
                }
            } else if (data.getData().getPost_for() != null &&
                    data.getData().getPost_for().equalsIgnoreCase(Constants.organizations)) {
                if (data.getData().getStatus() != null &&
                        data.getData().getStatus().equalsIgnoreCase(Constants.verified)) {
                    drawableResourceId = R.mipmap.location_gree_org;
                } else if (data.getData().getStatus() != null &&
                        data.getData().getStatus().equalsIgnoreCase(Constants.unverified)) {
                    drawableResourceId = R.mipmap.location_red_org;
                } else {
                    drawableResourceId = R.mipmap.location_gry_org;
                }

                category = "Category: ";
                GetDetails.DataBean.CategoryBean categoryObj = data.getData().getCategory();
                if (categoryObj != null) {
                    category = category + categoryObj.getName();
                }
                GetDetails.DataBean.SubcategoryBean subcategoryObj = data.getData().getSubcategory();
                if (subcategoryObj != null) {
                    category = category + " (" + subcategoryObj.getName() + ")";
                }
            }
            if (latLng != null) {

                if ("general".equalsIgnoreCase(data.getData().getLocation_profile())) {
                    createPolygon(latLng);
                    String markerOption = data.getData().getName() + "\nCategory: " + data.getData().getCategory().getName() + "\nAddress: " + data.getData().getAddress() + "\nStatus: " + data.getData().getStatus();
                    Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_icon_transparent);
                    map.getStyle().addImage("search_marker-" + data.getData().getId(), bm);
                    symbolManager.create(new SymbolOptions()
                            .withLatLng(latLng)
                            .withIconImage("search_marker-" + data.getData().getId())
                            //set the below attributes according to your requirements
                            .withIconSize(0.5f)
                            .withIconOffset(new Float[]{0f, -1.5f})
                            .withZIndex(10)
                            .withTextField(markerOption)
                            .withTextHaloColor("rgba(255, 255, 255, 100)")
                            .withTextHaloWidth(5.0f)
                            .withTextAnchor("top")
                            .withTextOffset(new Float[]{0f, 1.5f})
                    );
                } else {
                    String markerOption = data.getData().getName() + "\nCategory: " + data.getData().getCategory().getName() + "\nAddress: " + data.getData().getAddress() + "\nStatus: " + data.getData().getStatus();
                    Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableResourceId);
                    map.getStyle().addImage("search_marker-" + data.getData().getId(), bm);

//                    double n = latLng.getLatitude() + coef;
//                    double so = latLng.getLatitude() - coef;
//                    double w = latLng.getLongitude() + coef / Math.cos(n * 0.018);
//                    double e = latLng.getLongitude() - coef / Math.cos(so * 0.018);;
//
//
//                    LatLng BOUND_CORNER_NW = new LatLng(n, w);
//                    LatLng BOUND_CORNER_SE = new LatLng(so, e);
//
//                    LatLngBounds RESTRICTED_BOUNDS_AREA = new LatLngBounds.Builder()
//                            .include(BOUND_CORNER_NW)
//                            .include(BOUND_CORNER_SE)
//                            .build();
//
//                    map.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);



                    symbolManager.create(new SymbolOptions()
                            .withLatLng(latLng)
                            .withIconImage("search_marker-" + data.getData().getId())
                            //set the below attributes according to your requirements
                            .withIconSize(0.5f)
                            .withIconOffset(new Float[]{0f, -1.5f})
                            .withZIndex(10)
                            .withTextField(markerOption)
                            .withTextHaloColor("rgba(255, 255, 255, 100)")
                            .withTextHaloWidth(5.0f)
                            .withTextAnchor("top")
                            .withTextOffset(new Float[]{0f, 1.5f})
                    );
                }
            } else {
                displayToast("Invalid latitude and longitude");
            }
        } else {
            displayErrorDialog("Error", data.getError());
        }
    }



    private void showOrganizationDetail(List<OrganizationPosts.DataBean> dataList) {
        organizationDataList.clear();
        organizationDataList.addAll(dataList);
        setMarkerOrganization(dataList);
    }

    private void showObjectDetail(List<ObjectDetail> dataList) {
        objectDataList.clear();
        objectDataList.addAll(dataList);
        setMarkerObject(dataList);
    }
    private void showIndividualDetail(List<IndividualPosts.DataBean> dataList) {
        individualDataList.clear();
        individualDataList.addAll(dataList);
        setMarkerIndividual(dataList);
    }

    private void setMarkerOrganization(List<OrganizationPosts.DataBean> data) {
        if (isMapReady) {
            symbolLayerIconFeatureList = new ArrayList<>();
            options = new ArrayList<>();
            symbolLayerIconFeatureList.clear();
            options.clear();
            try {

                if (data != null && data.size() > 0) {
                    int drawableResourceId = 0;
                    for (int i = 0; i < data.size(); i++) {
                        LatLng latLng = null;
                        try {
                            latLng = new LatLng(data.get(i).getLat(),
                                    data.get(i).getLog());
                        } catch (IllegalArgumentException e) {
                            continue;
                        }

                        if (data.get(i).getStatus().trim().equalsIgnoreCase(Constants.verified)) {
                            drawableResourceId = R.mipmap.location_gree_org;

                        } else if (data.get(i).getStatus().trim().equalsIgnoreCase(Constants.unverified)) {
                            drawableResourceId = R.mipmap.location_red_org;

                        } else {
                            drawableResourceId = R.mipmap.location_gry_org;

                        }

                        if ("general".equalsIgnoreCase(data.get(i).getLocation_profile())) {
                            createPolygon(latLng);
                            String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_icon_transparent);
                            map.getStyle().addImage("org_marker-" + data.get(i).getId(), bm);
                            symbolManager.create(new SymbolOptions()
                                    .withLatLng(latLng)
                                    .withIconImage("org_marker-" + data.get(i).getId())
                                    //set the below attributes according to your requirements
                                    .withIconSize(0.5f)
                                    .withIconOffset(new Float[]{0f, -1.5f})
                                    .withZIndex(10)
                                    .withTextField(markerOption)
                                    .withTextHaloColor("rgba(255, 255, 255, 100)")
                                    .withTextHaloWidth(5.0f)
                                    .withTextAnchor("top")
                                    .withTextOffset(new Float[]{0f, 1.5f})
                            );
                        } else {
                            String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                            Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableResourceId);
                            map.getStyle().addImage("org_marker-" + data.get(i).getId(), bm);
//                            double n = latLng.getLatitude() + coef;
//                            double s = latLng.getLatitude() - coef;
//                            double w = latLng.getLongitude() + coef / Math.cos(n * 0.018);
//                            double e = latLng.getLongitude() - coef / Math.cos(s * 0.018);;
//
//
//                            LatLng BOUND_CORNER_NW = new LatLng(n, w);
//                            LatLng BOUND_CORNER_SE = new LatLng(s, e);
//
//                            LatLngBounds RESTRICTED_BOUNDS_AREA = new LatLngBounds.Builder()
//                                    .include(BOUND_CORNER_NW)
//                                    .include(BOUND_CORNER_SE)
//                                    .build();
//
//                            map.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);


                            symbolManager.create(new SymbolOptions()
                                    .withLatLng(latLng)
                                    .withIconImage("org_marker-" + data.get(i).getId())
                                    //set the below attributes according to your requirements
                                    .withIconSize(1.5f)
                                    .withIconOffset(new Float[]{0f, -1.5f})
                                    .withZIndex(10)
                                    .withTextField(markerOption)
                                    .withTextHaloColor("rgba(255, 255, 255, 100)")
                                    .withTextHaloWidth(5.0f)
                                    .withTextAnchor("top")
                                    .withTextOffset(new Float[]{0f, 1.5f})
                            );
                        }
                    }

                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private void setMarkerObject(List<ObjectDetail> data) {
        if(isMapReady) {
            symbolLayerIconFeatureList = new ArrayList<>();
            options = new ArrayList<>();
            symbolLayerIconFeatureList.clear();
            options.clear();
            try{

                if(data != null && data.size() > 0) {
                    int drawableResourceId = 0;
                    for(int i = 0; i < data.size(); i++) {
                        LatLng latLng = null;
                        if(!TextUtils.isEmpty(data.get(i).getLat()) && !TextUtils.isEmpty(data.get(i).getLog())) {
                            try {
                                latLng = new LatLng(Double.valueOf(data.get(i).getLat()),
                                        Double.valueOf(data.get(i).getLog()));
                            } catch (IllegalArgumentException e) {
                                continue;
                            }

                            if (mId.equalsIgnoreCase("deaddrop")) {
                                drawableResourceId = R.drawable.deaddrop;

                            } else if (mId.equalsIgnoreCase("zi network")) {
                                drawableResourceId = R.drawable.zi_network;

                            } else if (mId.equalsIgnoreCase("relic")) {
                                drawableResourceId = R.drawable.relic;

                            } else {
                                drawableResourceId = R.mipmap.location_gry_org;

                            }

                            if ("general".equalsIgnoreCase(data.get(i).getLocation_profile())) {
                                createPolygon(latLng);
                                String markerOption = data.get(i).getName();
                                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_icon_transparent);
                                map.getStyle().addImage("object-" + data.get(i).getId(), bm);
                                symbolManager.create(new SymbolOptions()
                                        .withLatLng(latLng)
                                        .withIconImage("object-" + data.get(i).getId())
                                        //set the below attributes according to your requirements
                                        .withIconSize(0.5f)
                                        .withIconOffset(new Float[]{0f, -1.5f})
                                        .withZIndex(10)
                                        .withTextField(markerOption)
                                        .withTextHaloColor("rgba(255, 255, 255, 100)")
                                        .withTextHaloWidth(5.0f)
                                        .withTextAnchor("top")
                                        .withTextOffset(new Float[]{0f, 1.5f})
                                );
                            } else {
                                String markerOption = data.get(i).getName();
                                Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableResourceId);
                                map.getStyle().addImage("object-" + data.get(i).getId(), bm);
//                                double n = latLng.getLatitude() + coef;
//                                double s = latLng.getLatitude() - coef;
//                                double w = latLng.getLongitude() + coef / Math.cos(n * 0.018);
//                                double e = latLng.getLongitude() - coef / Math.cos(s * 0.018);;
//
//
//                                LatLng BOUND_CORNER_NW = new LatLng(n, w);
//                                LatLng BOUND_CORNER_SE = new LatLng(s, e);
//
//
//                                LatLngBounds RESTRICTED_BOUNDS_AREA = new LatLngBounds.Builder()
//                                        .include(BOUND_CORNER_NW)
//                                        .include(BOUND_CORNER_SE)
//                                        .build();
//
//                                map.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);

                                symbolManager.create(new SymbolOptions()
                                        .withLatLng(latLng)
                                        .withIconImage("object-" + data.get(i).getId())
                                        //set the below attributes according to your requirements
                                        .withIconSize(0.5f)
                                        .withIconOffset(new Float[]{0f, -1.5f})
                                        .withZIndex(10)
                                        .withTextField(markerOption)
                                        .withTextHaloColor("rgba(255, 255, 255, 100)")
                                        .withTextHaloWidth(5.0f)
                                        .withTextAnchor("top")
                                        .withTextOffset(new Float[]{0f, 1.5f})
                                );
                            }
                        }

                    }

                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void setMarkerIndividual(List<IndividualPosts.DataBean> data) {
        if (isMapReady) {
            symbolLayerIconFeatureList = new ArrayList<>();
            options = new ArrayList<>();
            symbolLayerIconFeatureList.clear();
            options.clear();
            try {

                if (data != null && data.size() > 0) {
                    int drawableResourceId = 0;
                    for (int i = 0; i < data.size(); i++) {
                        LatLng latLng = null;
                        try {
                            latLng = new LatLng(data.get(i).getLat(),
                                    data.get(i).getLog());
                        } catch (IllegalArgumentException e) {
                            continue;
                        }

                        String status = data.get(i).getStatus().trim();
                        String gender = data.get(i).getGender().trim();

                        if (TextUtils.isEmpty(gender)) {
                            return;
                        }

                        if (TextUtils.isEmpty(status)) {
                            return;
                        }

                        if (gender.equalsIgnoreCase(Constants.female)) {

                            if (!TextUtils.isEmpty(status)) {

                                if (status.equalsIgnoreCase(Constants.verified)) {
                                    drawableResourceId = R.mipmap.individual_green_gril;
                                } else if (status.equalsIgnoreCase(Constants.unverified)) {
                                    drawableResourceId = R.mipmap.individual_red_girl;
                                } else {
                                    drawableResourceId = R.mipmap.indi_girl_gray;
                                }
                            }
                        }

                        if (gender.equalsIgnoreCase(Constants.male)) {
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase(Constants.verified)) {
                                    drawableResourceId = R.mipmap.individual_green_man;
                                } else if (status.equalsIgnoreCase(Constants.unverified)) {
                                    drawableResourceId = R.mipmap.individual_red_man;
                                } else {
                                    drawableResourceId = R.mipmap.individual_gray_man;
                                }
                            }
                        }

                        if ("general".equalsIgnoreCase(data.get(i).getLocation_profile())) {
                            createPolygon(latLng);
                        } else {
                            String markerOption = data.get(i).getName() + "\nOccupation: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                            Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableResourceId);
                            map.getStyle().addImage("ind_marker-" + data.get(i).getId(), bm);

//                            double n = latLng.getLatitude() + coef;
//                            double s = latLng.getLatitude() - coef;
//                            double w = latLng.getLongitude() + coef / Math.cos(n * 0.018);
//                            double e = latLng.getLongitude() - coef / Math.cos(s * 0.018);
//
//
//                            LatLng BOUND_CORNER_NW = new LatLng(n, w);
//                            LatLng BOUND_CORNER_SE = new LatLng(s, e);
//
//                            LatLngBounds RESTRICTED_BOUNDS_AREA = new LatLngBounds.Builder()
//                                    .include(BOUND_CORNER_NW)
//                                    .include(BOUND_CORNER_SE)
//                                    .build();
//
//                            map.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);

                            symbolManager.create(new SymbolOptions()
                                    .withLatLng(latLng)
                                    .withIconImage("ind_marker-" + data.get(i).getId())
                                    //set the below attributes according to your requirements
                                    .withIconSize(0.5f)
                                    .withIconOffset(new Float[]{0f, -1.5f})
                                    .withZIndex(10)
                                    .withTextField(markerOption)
                                    .withTextHaloColor("rgba(255, 255, 255, 100)")
                                    .withTextHaloWidth(5.0f)
                                    .withTextAnchor("top")
                                    .withTextOffset(new Float[]{0f, 1.5f})
                            );
                        }

                    }

                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    private void createPolygon(LatLng latLng) {
        circleManager.create(new CircleOptions()
                .withLatLng(latLng)
                .withCircleColor(ColorUtils.colorToRgbaString(getActivity().getResources().getColor(R.color.colorYellowtransparent)))
                .withCircleRadius(100f)
                .withDraggable(false)

        );
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if(symbolLayerIconFeatureList.size()>0) {
            symbolManager.delete(symbolLayerIconFeatureList);
        }

        symbolManager.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void userLocationChange(Location location) {
        Toast.makeText(getActivity(), "" + location.getLatitude(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }
}