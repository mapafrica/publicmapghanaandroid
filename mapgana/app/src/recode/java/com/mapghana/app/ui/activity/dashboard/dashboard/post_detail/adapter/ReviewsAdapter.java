package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.GetDetails;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.CircleImageView;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class ReviewsAdapter extends BaseRecycleAdapter {
    private AdapterClickListener adapterClickListener;
    private List<GetDetails.DataBean.PostReviewBean> list;

    String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public ReviewsAdapter(AdapterClickListener adapterClickListener,
                          List<GetDetails.DataBean.PostReviewBean> list) {
        this.adapterClickListener = adapterClickListener;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_reviews;
    }

    @Override
    protected int getItemSize() {
        if (list != null && list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder {
        private AppCompatTextView tvName, tvTitle, tvTime, tvMsg;
        private RatingBar rating_bar;
        private CircleImageView imgProfile;
        private int pos;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvMsg = itemView.findViewById(R.id.tvMsg);
            rating_bar = itemView.findViewById(R.id.rating_bar);
            imgProfile = itemView.findViewById(R.id.imgProfile);

        }

        @Override
        public void setData(int position) {
            pos = position;
            tvTitle.setText(list.get(position).getTitle());
            tvMsg.setText(list.get(position).getMessage());
            GetDetails.DataBean.PostReviewBean.UserBeanX userBeanX = list.get(position).getUser();
            if (userBeanX != null) {
                tvName.setText("By: " + userBeanX.getName());
                String image = userBeanX.getImage();
                if (image != null && !image.equals("")) {
//                    ((AppBaseActivity)getContext()).loadImage(getContext(), imgProfile, null, image,
//                            R.drawable.pleasewait, R.drawable.pleasewait, R.drawable.pleasewait);
                    Glide.with(getContext())
                            .load(image).override(250, 250)
                            .placeholder(R.drawable.profile).dontAnimate()
                            .into(imgProfile);
                } else {
                    imgProfile.setImageResource(R.drawable.profile);
                }
            }
            rating_bar.setRating(list.get(position).getRating());

            LayerDrawable stars = (LayerDrawable) rating_bar.getProgressDrawable();
            stars.getDrawable(1).setColorFilter(list.get(position).getRatingColor(getContext(), status),
                    PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(2).setColorFilter(list.get(position).getRatingColor(getContext(), status),
                    PorterDuff.Mode.SRC_ATOP);
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy @ hh:mm a");

            try {
                Date date = inputFormat.parse(list.get(position).getCreated_at());
                String output = outputFormat.format(date);
                tvTime.setText(output);
            } catch (ParseException e) {
            }


            imgProfile.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imgProfile:
                    adapterClickListener.onAdapterClickListener(pos, "fromSingleImage");
                    break;
            }
        }
    }

}
