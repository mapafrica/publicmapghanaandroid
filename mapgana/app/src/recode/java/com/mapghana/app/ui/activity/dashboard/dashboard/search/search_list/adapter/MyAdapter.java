package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.interfaces.SearchSingleItemClick;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.utils.Constants;
import com.mapghana.base.BaseLatestRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.util.file_path_handler.BitmapHelper;
import com.mapghana.util.file_path_handler.FilePathHelper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends BaseLatestRecycleAdapter {

    private static final String TAG = MyAdapter.class.getSimpleName();
    private SearchSingleItemClick adapterClickListener;

    private List<GlobeSearch.DataBean> list;
    private List<String> image_mime_List;
    private List<String> video_mime_List;

    private boolean loadMore = false;
    private Context context;

    public MyAdapter(Context context, SearchSingleItemClick adapterClickListener,
                     List<GlobeSearch.DataBean> list) {
        this.context = context;
        this.adapterClickListener = adapterClickListener;
        this.list = list;
        image_mime_List = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        image_mime_List.addAll(Constants.getImageMimeList());
        video_mime_List.addAll(Constants.getVideoMimeList());
    }

    @Override
    public BaseViewHolder getViewHolder() {
        return null;
    }

    @Override
    public BaseViewHolder getViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_DATA)
            return new MyViewHolder(inflateLayout(R.layout.item_list_item));

        return new LoadMoreViewHolder(inflateLayout(R.layout.item_load_more));
    }

    @Override
    public int getDataCount() {
        return list == null ? 0 : (loadMore ? list.size() + 1 : list.size());
    }

    @Override
    public int getViewType(int position) {
        if (list == null) return VIEW_TYPE_DATA;
        if (loadMore && position == list.size())
            return VIEW_TYPE_LOAD_MORE;

        return VIEW_TYPE_DATA;
    }

    class MyViewHolder extends BaseViewHolder {
        private AppCompatTextView tvName, tvRating, tvAddress, tvTotalReview, tvCategoryName;
        private LinearLayout llView;
        private ImageView imgSCat;
        private ProgressBar pb_image;
        private RatingBar rating_bar;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvRating = itemView.findViewById(R.id.tvRating);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvTotalReview = itemView.findViewById(R.id.tvTotalReview);
            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
            llView = itemView.findViewById(R.id.llView);
            imgSCat = itemView.findViewById(R.id.imgSCat);
            pb_image = itemView.findViewById(R.id.pb_image);
            rating_bar = itemView.findViewById(R.id.rating_bar);

        }

        @Override
        public void setData(int position) {
            if (list == null) return;
            GlobeSearch.DataBean dataBean = list.get(position);
            try {
                llView.setOnClickListener(this);
                llView.setTag(position);
                tvName.setText(dataBean.getName());
                LayerDrawable stars = (LayerDrawable) rating_bar.getProgressDrawable();
                String status = dataBean.getStatus();
                int color = context.getResources().getColor(R.color.colorMidDarkGray);
                if (status != null && !status.equals("") &&
                        status.equalsIgnoreCase(Constants.unverified)) {
                    color = context.getResources().getColor(R.color.colorRed);
                } else if (status != null && !status.equals("") &&
                        status.equalsIgnoreCase(Constants.verified)) {
                    color = context.getResources().getColor(R.color.colorGreen);
                } else if (status != null && !status.equals("") &&
                        status.equalsIgnoreCase(Constants.anonymous)) {
                    color = context.getResources().getColor(R.color.colorMidDarkGray);
                }


                LayerDrawable layerDrawable = (LayerDrawable) rating_bar.getProgressDrawable();
                DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), color); // Partial star
                DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)), color);  // Full star

                if (dataBean.hasRating()) {
                    tvRating.setTextColor(color);
                    tvRating.setVisibility(View.VISIBLE);
                    tvRating.setText(new DecimalFormat("##.#").format(dataBean.getAvg_rating()));

                } else {
                    tvRating.setVisibility(View.GONE);
                }
                int total_review = dataBean.getTotal_review();
                if (total_review > 0) {
                    updateViewVisibility(tvTotalReview, View.VISIBLE);
                    tvTotalReview.setText("(" + String.valueOf(dataBean.getTotal_review()) + ") - ");
                } else {
                    updateViewVisibility(tvTotalReview, View.GONE);
                }


                rating_bar.setRating((float) dataBean.getAvg_rating());
                tvCategoryName.setText(dataBean.getCategory().getName());
                tvAddress.setText(dataBean.getAddress());

                String imagePath = dataBean.getImage();

                if (dataBean != null) {
                    List<GlobeSearch.DataBean.PostGalleryBean> postGallery = dataBean.getPostGallery();

                    if (dataBean.getPost_for().equalsIgnoreCase(Constants.individuals)) {
                        if (imagePath != null && imagePath.length() != 0) {
                            /*if (isValidString(imagePath)) {
                                ((AppBaseActivity) context).loadImage(context, imgSCat, pb_image, imagePath,
                                        R.drawable.pleasewait, R.drawable.pleasewait, R.drawable.pleasewait);
                            } else {
                                imgSCat.setImageResource(R.drawable.pleasewait);
                                updateViewVisibitity(pb_image, View.INVISIBLE);
                            }*/
                            Glide.with(getContext()).load(imagePath)
                                    .override(250, 250)
                                    .placeholder(R.drawable.ic_avatar)
                                    .dontAnimate()
                                    .into(imgSCat);
                        } else if (postGallery != null) {
                            if (postGallery.size() > 0) {
                                String mediaPath = postGallery.get(0).getImage();
                                if (mediaPath != null && mediaPath.length() > 0) {
                                    FilePathHelper filePathHelper = new FilePathHelper();
                                    String mimeType = filePathHelper.getMimeType(imagePath);
                                    if (image_mime_List.contains(mimeType)) {
                                        /*if (isValidString(mediaPath)) {
                                            ((AppBaseActivity) context).loadImage(context, imgSCat, pb_image, mediaPath,
                                                    R.drawable.pleasewait, R.drawable.pleasewait, R.drawable.pleasewait);
                                        } else {
                                            imgSCat.setImageResource(R.drawable.pleasewait);
                                            updateViewVisibitity(pb_image, View.INVISIBLE);
                                        }*/
                                        Glide.with(getContext()).load(mediaPath).override(250, 250)
                                                .placeholder(R.drawable.pleasewait)
                                                .dontAnimate()
                                                .into(imgSCat);
                                    } else if (video_mime_List.contains(mimeType)) {
                                        BitmapHelper bitmapHelper = new BitmapHelper();
                                        Bitmap bitmap = bitmapHelper.getVideoBitmap(mediaPath);
                                        imgSCat.setImageBitmap(bitmap);
                                    }
                                }
                            }
                        }
                    }

                    if (dataBean.getPost_for().equalsIgnoreCase(Constants.organizations)) {

                        if (postGallery != null && postGallery.size() > 0) {
                            String mediaPath = postGallery.get(0).getImage();
                            if (mediaPath != null && mediaPath.length() > 0) {
                                FilePathHelper filePathHelper = new FilePathHelper();
                                String mimeType = filePathHelper.getMimeType(imagePath);
                                if (image_mime_List.contains(mimeType)) {
                                    /*if (isValidString(mediaPath)) {
                                        ((AppBaseActivity) context).loadImage(context, imgSCat, pb_image, mediaPath,
                                                R.drawable.pleasewait, R.drawable.pleasewait, R.drawable.pleasewait);
                                    } else {
                                        imgSCat.setImageResource(R.drawable.pleasewait);
                                        updateViewVisibitity(pb_image, View.INVISIBLE);
                                    }*/

                                    Glide.with(getContext()).load(mediaPath).override(250, 250)
                                            .placeholder(R.drawable.pleasewait)
                                            .dontAnimate()
                                            .into(imgSCat);
                                } else if (video_mime_List.contains(mimeType)) {
                                    BitmapHelper bitmapHelper = new BitmapHelper();
                                    Bitmap bitmap = bitmapHelper.getVideoBitmap(mediaPath);
                                    imgSCat.setImageBitmap(bitmap);
                                }
                            }
                        }
                    }
                }

            } catch (IndexOutOfBoundsException e) {

            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.llView:

                    adapterClickListener.onClick(list.get((Integer) v.getTag()));
                    break;
            }
        }
    }

}
