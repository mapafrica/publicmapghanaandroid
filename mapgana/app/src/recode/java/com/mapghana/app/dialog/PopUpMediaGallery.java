package com.mapghana.app.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.mapghana.R;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.adapter.EventMediaListAdapter;
import com.mapghana.app.interfaces.MediaClickHandler;
import com.mapghana.app.model.EventGallery;
import com.mapghana.app.ui.sidemenu.Event.EventListCallback;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceTextView;

import java.util.List;

public class PopUpMediaGallery extends AlertDialog implements
        View.OnClickListener, MediaClickHandler , Player.EventListener{
    private EventListCallback mListener;
    private List<EventGallery> galleryList;
    private RecyclerView rvMedia;
    private Context context;
    private ImageView imageView;
    private RelativeLayout rlImageContainer, rlVideoContainer;
    private LottieAnimationView progressBar;
    private ImageView playIcon;
    private boolean isButtonAvailable;
    private ImageView imageClose;
    private AppCompatTextView noMedia;
    private String typeScreen;
    private static final String TAG = "ExoPlayerActivity";

    private static final String KEY_VIDEO_URI = "video_uri";
    private PlayerView videoFullScreenPlayer;
    private SimpleExoPlayer player;
    private Handler mHandler;
    private Runnable mRunnable;

    public PopUpMediaGallery(Context context, EventListCallback activity, List<EventGallery> galleryList, boolean isButtonAvailable, String screenType) {
        super(context);
        this.mListener = activity;
        this.typeScreen = screenType;
        this.galleryList = galleryList;
        this.context = context;
        this.isButtonAvailable = isButtonAvailable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pop_up_media_gallery);

        rvMedia = findViewById(R.id.rv_media_list);
        imageClose = findViewById(R.id.image_close);
        progressBar = findViewById(R.id.mk_loader);
        imageView = findViewById(R.id.image_view);
        videoFullScreenPlayer = findViewById(R.id.videoFullScreenPlayer);
        rlImageContainer = findViewById(R.id.rl_image);
        rlVideoContainer = findViewById(R.id.rl_video);
        noMedia = findViewById(R.id.no_media);
        rlVideoContainer.setOnClickListener(this);
        imageClose.setOnClickListener(this);
        initRecycleLayoutManager();
        setUp();
        if(galleryList.size() == 0) {
            progressBar.setVisibility(View.GONE);
            noMedia.setVisibility(View.VISIBLE);
        }else {
            progressBar.setVisibility(View.VISIBLE);
            noMedia.setVisibility(View.GONE);

            if(galleryList.get(0).getType().equalsIgnoreCase("PHOTOS")) {
                initDialogBannerImage("PHOTOS", galleryList.get(0).getFile_pathname());
            }else {
                initDialogBannerImage("VIDEOS", galleryList.get(0).getFile_pathname());
            }
        }


    }
    private void setUp() {
        initializePlayer();

    }


    private void initializePlayer() {
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 16),
                    Constants.MIN_BUFFER_DURATION,
                    Constants.MAX_BUFFER_DURATION,
                    Constants.MIN_PLAYBACK_START_BUFFER,
                    Constants.MIN_PLAYBACK_RESUME_BUFFER, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), trackSelector, loadControl);

            videoFullScreenPlayer.setPlayer(player);

        }


    }

    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
                Util.getUserAgent(context, context.getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_close:
                dismiss();
                player.release();
                mListener.onCloseIcon();
                break;
            default:
                break;
        }
    }

    private void initRecycleLayoutManager() {

        rvMedia.setHasFixedSize(true);
        EventMediaListAdapter adapter = new EventMediaListAdapter(context, galleryList,this  );

        rvMedia.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvMedia.setAdapter(adapter);
    }


    private void initDialogBannerImage(String type, String url) {
        progressBar.setVisibility(View.VISIBLE);
        if (type.equalsIgnoreCase("PHOTOS")) {
            rlVideoContainer.setVisibility(View.GONE);
            rlImageContainer.setVisibility(View.VISIBLE);
            Glide.clear(imageView);
            Glide.with(MapGhanaApplication.sharedInstance())
                    .load(url)
                    .dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                                isFirstResource) {
                            progressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                                target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }
                    })
                    .into(imageView);

        } else {
            rlVideoContainer.setVisibility(View.VISIBLE);
            rlImageContainer.setVisibility(View.GONE);
            Uri video = Uri.parse(url);
            buildMediaSource(video);
        }

    }

    @Override
    public void onClick(EventGallery gallery) {
        initDialogBannerImage(gallery.getType(),gallery.getFile_pathname());
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                progressBar.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                // Activate the force enable
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
                progressBar.setVisibility(View.GONE);

                break;
            default:
                // status = PlaybackStatus.IDLE;
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
}
