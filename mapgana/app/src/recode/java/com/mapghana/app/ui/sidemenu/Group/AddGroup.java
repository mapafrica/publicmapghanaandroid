package com.mapghana.app.ui.sidemenu.Group;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.AddEventGalleryAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.CompressionModel;
import com.mapghana.app.model.EventIdListingForGroup;
import com.mapghana.app.model.EventIdListingForGroupData;
import com.mapghana.app.model.PaymentResponse;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.Success;
import com.mapghana.app.model.TransactionPrice;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;
import com.mapghana.handler.MediaAddAdpterListener;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.mapghana.videocompression.VideoCompress;
import com.theartofdev.edmodo.cropper.CropImage;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class AddGroup extends AppBaseFragment implements MediaAddAdpterListener {
    private static final String TAG = "AddGroup";
    private EditText groupName, aboutUs, linkChat, mapghanaAdminContact, etPasswordProtect;
    private ImageView groupPosterIcon;
    private AutoCompleteTextView eventMapghanaLink;
    private RelativeLayout addGroupIcon;
    private ImageView posterImage;
    private String accesType;
    private RecyclerView rvGalleryMedia;
    private List<String> galleryList;
    private List<String> video_mime_List;
    private Uri uriOneImg;
    private TextWatcher linkChattextWatcher, eventMapghanaLinkTextWatcher;
    private RestClient restClient;
    private AddEventGalleryAdapter groupGalleryAdapter;
    private AppCompatTextView submitGroup;
    private SwitchCompat passwordProtect;
    private TextInputLayout textInputLayoutPasswordProtect;
    private static final int REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE = 887;
    private List<String> eventIdLinkData;
    private List<EventIdListingForGroupData> eventIdLinkDataModel;
    private String eventLink;
    private List<CompressionModel> videoLists = new ArrayList<>();
    private String screenType, paymentType;
    private AlertDialog.Builder builder;
    private TransactionPrice transactionPrice;

    public static AddGroup newInstance() {
        AddGroup addGroup = new AddGroup();
        Bundle args = new Bundle();
        addGroup.setArguments(args);
        return addGroup;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_add_group;
    }

    @Override
    public void initializeComponent() {
        restClient = new RestClient(getContext());
        video_mime_List = new ArrayList<>();
        initToolBar();
        initViewById(getView());
        initRecycleLayoutManager();
        initListener();
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
        builder = new AlertDialog.Builder(getActivity());
        transactionPrice = AppPreferences.getTransactionPrice();
        paymentConfirmationDialog();

    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle("Add Groups");
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }

    private void initViewById(View view) {
        galleryList = new ArrayList<>();
        eventIdLinkData = new ArrayList<>();
        eventIdLinkDataModel = new ArrayList<>();
        groupName = view.findViewById(R.id.group_name);
        addGroupIcon = view.findViewById(R.id.group_frame);
        posterImage = view.findViewById(R.id.image_icon_group);
        groupPosterIcon = view.findViewById(R.id.group_poster);
        aboutUs = view.findViewById(R.id.group_about);
        linkChat = view.findViewById(R.id.et_link);
        eventMapghanaLink = view.findViewById(R.id.et_mapghana_group_link);
        mapghanaAdminContact = view.findViewById(R.id.et_mapghana_admin);
        submitGroup = view.findViewById(R.id.tv_add_group);
        rvGalleryMedia = view.findViewById(R.id.media);
        passwordProtect = view.findViewById(R.id.password_protect);
        etPasswordProtect = view.findViewById(R.id.et_password);
        textInputLayoutPasswordProtect = view.findViewById(R.id.text_input_layout_password);

        restClient.callback(this).getEventIdList();

        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                paymentReceiver,
                new IntentFilter(Constants.LOCAL_BROADCAST_PAYMENT)
        );
    }

    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver paymentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_SUCCESS)) {

                screenType = intent.getStringExtra("screen_type");
                paymentType = intent.getStringExtra("payment_type");

                if(paymentType.equalsIgnoreCase("password_protected")) {
                    passwordProtect.setChecked(true);
                    passwordProtect.setEnabled(false);
                    textInputLayoutPasswordProtect.setVisibility(View.VISIBLE);
                    Gson gson = new Gson();
                    PaymentResponse paymentResponse = gson.fromJson(intent.getStringExtra(Constants.data), PaymentResponse.class);
                    callPaymentToServer(paymentResponse);
                }else if(paymentType.equalsIgnoreCase("add_group")){
                    Gson gson = new Gson();
                    PaymentResponse paymentResponse = gson.fromJson(intent.getStringExtra(Constants.data), PaymentResponse.class);
                    callPaymentToServer(paymentResponse);
                }


            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_ERROR)) {

                if(paymentType.equalsIgnoreCase("password_protected")){
                    passwordProtect.setChecked(false);
                    passwordProtect.setEnabled(true);
                    textInputLayoutPasswordProtect.setVisibility(View.GONE);
                }else if(paymentType.equalsIgnoreCase("add_group")) {
                    Utils.showToast(getActivity(), getView(), "Payment error");
                }


            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_CANCELLED)) {
                if(paymentType.equalsIgnoreCase("password_protected")){
                    passwordProtect.setChecked(false);
                    passwordProtect.setEnabled(true);
                    textInputLayoutPasswordProtect.setVisibility(View.GONE);
                }else if(paymentType.equalsIgnoreCase("add_group")) {
                    Utils.showToast(getActivity(), getView(), "Payment cancelled");
                }


            }
        }
    };

    private void callPaymentToServer(PaymentResponse paymentResponse ) {
        Session session = AppPreferences.getSession();
        restClient.callback(this).transaction("0",String.valueOf(paymentResponse.getData().getAmount()),paymentResponse.getData().getPaymentId(),session.getEmail(),session.getName(),session.getName(),screenType,paymentType);
    }
    private void validationCheck() {
        if (TextUtils.isEmpty(groupName.getText().toString())) {
            groupName.setError("Please add group name");
            Utils.showToast(getActivity(), getView(), "Please add group name");
            return;
        }

        if (uriOneImg != null) {
            if (TextUtils.isEmpty(uriOneImg.getPath())) {
                Utils.showToast(getActivity(), getView(), "Please add group poster image");
                return;
            }
        } else {
            Utils.showToast(getActivity(), null, "Please add group poster image");
            Utils.showToast(getActivity(), getView(), "Please add group poster image");
            return;
        }


        if (TextUtils.isEmpty(aboutUs.getText().toString())) {
            aboutUs.setError("Please add something about group");
            Utils.showToast(getActivity(), getView(), "Please add something about group");
            return;
        }

        if (TextUtils.isEmpty(linkChat.getText().toString())) {
            linkChat.setError("Please add chat link");
            Utils.showToast(getActivity(), getView(), "Please add chat link");
            return;
        }

        if (TextUtils.isEmpty(mapghanaAdminContact.getText().toString())) {
            mapghanaAdminContact.setError("Please add contact detail");
            Utils.showToast(getActivity(), getView(), "Please add contact detail");
            return;
        }

        if (mapghanaAdminContact.getText().toString().length() < 7 || mapghanaAdminContact.getText().toString().length() > 12) {
            mapghanaAdminContact.setError("Please add correct contact detail");
            Utils.showToast(getActivity(), getView(), "Please add correct contact detail");
            return;
        }

        if (ConnectionDetector.isNetAvail(getContext())) {
            showDialog();


        } else {
            displayToast(Constants.No_Internet);
        }


    }

    private void showDialog() {
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Mapghana Payment");
        alert.show();
    }

    private void paymentConfirmationDialog() {


        builder.setMessage("Mapghana will charge  "+transactionPrice.getGroup().getAdd_group_charges()+" GHS . Do you really want to continue?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> openMakePayment("add_group", "add_group", transactionPrice.getGroup().getAdd_group_charges()));
    }


    private void initRecycleLayoutManager() {
        galleryList = new ArrayList<>();
        rvGalleryMedia.setHasFixedSize(true);
        groupGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "group_media", this, galleryList);

        rvGalleryMedia.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvGalleryMedia.setAdapter(groupGalleryAdapter);

    }

    private void openMakePayment(String screennType, String paymentType, int amount) {
        Intent intent = new Intent(getActivity(), MapghanaPayment.class);
        intent.putExtra("id", 0);
        intent.putExtra("screen_type", screennType);
        intent.putExtra("payment_type", paymentType);
        intent.putExtra("amount", amount);
        startActivity(intent);
    }

    private void initListener() {

        passwordProtect.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                openMakePayment("add_group","password_protected", 100);

            } else {
                textInputLayoutPasswordProtect.setVisibility(View.GONE);
            }
        });

        submitGroup.setOnClickListener(v -> {
            validationCheck();
        });

        addGroupIcon.setOnClickListener(v -> {
            oneImageSelector();
        });

        linkChattextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setLinkUrlIcon(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        linkChat.addTextChangedListener(linkChattextWatcher);
    }


    private void multiplePreviousMediaSelector() {
        displayLog(TAG, "multipleImageVideoSelector: ");
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }


        if (requestCode == REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE) {
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                galleryList.addAll(paths);
                displayLog(TAG, "onActivityResult: " + galleryList.get(0));
                refreshGroupGalleryAdapter();
                if (isVideoCompressingNeeded()) {
                    compressVideo(videoLists);
                }
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)

            if (data != null) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    uriOneImg = result.getUri();
                    posterImage.setVisibility(View.GONE);
                    groupPosterIcon.setImageURI(uriOneImg);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    displayErrorDialog("Error", error.getMessage());
                }
            }

    }

    private void oneImageSelector() {
        CropImage.activity()
                .start(getContext(), this);
    }

    private void refreshGroupGalleryAdapter() {
        if (galleryList != null) {
            groupGalleryAdapter.notifyDataSetChanged();
        }
    }

    private void refreshGalleryAdapter(String type) {
        if (type.equalsIgnoreCase("group_media")) {
            if (galleryList != null) {
                groupGalleryAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onAddClick(String type) {
        multiplePreviousMediaSelector();
    }

    @Override
    public void onDeleteClick(int position, String type) {
        if (type.equalsIgnoreCase("group_media")) {
            for (int i = 0; i < galleryList.size(); i++) {
                if (galleryList.get(position).equalsIgnoreCase(galleryList.get(i))) {
                    galleryList.remove(i);
                    refreshGalleryAdapter(type);
                }
            }
        }
    }

    private void setLinkUrlIcon(String key) {
        if (key.contains("meetup")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.meetup, 0);
        } else if (key.contains("t.me")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.telegram, 0);
        } else if (key.contains("whatsapp")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.whatsapp, 0);
        } else if (key.contains("skype")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.skype, 0);
        } else if (key.contains("irc")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ircchat, 0);
        } else if (key.contains("viber")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.viber, 0);
        } else if (key.contains("line.me")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.line, 0);
        } else if (key.contains("weixin")) {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.wechat, 0);
        } else {
            linkChat.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            Utils.showToast(getActivity(), getView(), "Please add valid chat link");
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        linkChat.removeTextChangedListener(linkChattextWatcher);
        eventMapghanaLink.removeTextChangedListener(eventMapghanaLinkTextWatcher);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(paymentReceiver);
    }

    private void postGroupData() {

        displayProgressBar(false);
        HashMap<String, String> map = new HashMap<>();
        map.put(BaseArguments.group_name, groupName.getText().toString());
        map.put(BaseArguments.about, aboutUs.getText().toString());
        map.put(BaseArguments.chat_link, linkChat.getText().toString());
        map.put(BaseArguments.mapghana_event_link, eventLink);
        map.put(BaseArguments.group_admin_contact, mapghanaAdminContact.getText().toString());

        Session session = AppPreferences.getSession();
        map.put(BaseArguments.user_id, String.valueOf(session.getId()));
        map.put(BaseArguments.admin_password, etPasswordProtect.getText().toString());


        final MultipartBody.Part[] multiSinglePosterGalleryPart = new MultipartBody.Part[1];
        MultipartBody.Part[] multiPreviousGalleryPart = new MultipartBody.Part[galleryList.size()];
        if (!TextUtils.isEmpty(uriOneImg.getPath())) {
            int i = 0;
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), uriOneImg);
            File file = new File(uriOneImg.getPath());
            RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_IMAGE_PNG, file);
            multiSinglePosterGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.image,
                    uriOneImg.getPath(), media);
        }

        for (int i = 0; i < galleryList.size(); i++) {
            String url = galleryList.get(0);
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
            String mimeType = fileInformation.getMimeType();

            if (video_mime_List.contains(mimeType)) {
                File file = new File(galleryList.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                multiPreviousGalleryPart[i] = MultipartBody.Part.createFormData("gallary_image" + "[" + i + "]", file.getName(), media);

            } else {
                File file = new File(galleryList.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_IMAGE_PNG, file);
                multiPreviousGalleryPart[i] = MultipartBody.Part.createFormData("gallary_image" + "[" + i + "]", file.getName(), media);

            }
        }

        restClient.callback(this).postAddGroup(getContext(), RetrofitUtils.createMultipartRequest(map), multiPreviousGalleryPart, multiSinglePosterGalleryPart);
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        super.onSuccessResponse(apiId, response);
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_ADD_GROUP) {
                String s = null;
                try {
                    s = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                Success success = gson.fromJson(s, Success.class);
                if (success.getStatus() == 1) {
                    getActivity().onBackPressed();

                }else{
                    displayErrorDialog("Error", success.getMessage());
                }

            } else if (apiId == ApiIds.ID_EVENT_ID_LISTING_FOR_GROUP) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    EventIdListingForGroup eventIdListingForGroup = gson.fromJson(s, EventIdListingForGroup.class);
                    if (eventIdListingForGroup.getStatus() == 1) {
                        setAutocompleteData(eventIdListingForGroup);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }else if(apiId == ApiIds.ID_TRANSACTION) {

                if(paymentType.equalsIgnoreCase("password_protected")) {
                    Utils.showToast(getActivity(),getView(),paymentType+" transaction successfully");
                }else if(paymentType.equalsIgnoreCase("add_group")) {
                    postGroupData();
                }

            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        super.onFailResponse(apiId, error);
        if (apiId == ApiIds.ID_ADD_GROUP) {
            dismissProgressBar();
            Utils.showToast(getActivity(), getView(), "failure");
        }
    }


    private void setAutocompleteData(EventIdListingForGroup eventIdListingForGroup) {
        eventIdLinkData.clear();
        eventIdLinkDataModel.clear();
        for (EventIdListingForGroupData data : eventIdListingForGroup.getData()) {
            eventIdLinkData.add(data.getName());
            eventIdLinkDataModel.add(data);
        }
        //Creating the instance of ArrayAdapter containing list of fruit names
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), R.layout.item_autocomplete_box, eventIdLinkData);

        eventMapghanaLink.setThreshold(1);//will start working from first character
        eventMapghanaLink.setAdapter(adapter);
        eventMapghanaLink.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                eventLink = eventIdLinkDataModel.get(position).getName() + " - " + eventIdLinkDataModel.get(position).getId();
                eventLink = eventIdLinkDataModel.get(position).getName() + " - " + eventIdLinkDataModel.get(position).getId();
            }
        });
    }

    private boolean isVideoCompressingNeeded() {

        videoLists.clear();
        String path = "";
        CompressionModel compressionModel;

        for (int i = 0; i < galleryList.size(); i++) {
            path = galleryList.get(i);
            FilePathHelper filePathHelper = new FilePathHelper();
            final String mimeType = filePathHelper.getMimeType(path);
            if (video_mime_List.contains(mimeType)) {
                compressionModel = new CompressionModel();
                compressionModel.setSourcePath(path);
                compressionModel.setId(i);

                videoLists.add(compressionModel);
            }
        }

        return videoLists.size() > 0;
    }

    private void compressVideo(List<CompressionModel> filePath) {

        String outputDir = Environment.getExternalStorageDirectory() + "/MapGhanaVideo";
        File file = new File(outputDir);

        if (!file.exists()) {
            file.mkdirs();
        }


        if (filePath != null && filePath.size() > 0) {
            displayProgressBar(false, "Video is compressing...");
            for (int i = 0; i < filePath.size(); i++) {
                File file1 = new File(filePath.get(i).getSourcePath());
                String destPath = outputDir + File.separator + "MapGhana_VID_IND_" + i +
                        System.currentTimeMillis() + ".mp4";

                final CompressionModel compressionModel = filePath.get(i);
                compressionModel.setDestinationPath(destPath);
                compressionModel.setCompressionStatus(0);
                VideoCompress.compressVideoHigh(compressionModel.getSourcePath(),
                        compressionModel.getDestinationPath(), compressionModel.getId(),
                        new VideoCompress.CompressListener() {
                            @Override
                            public void onStart(long mTask_id) {
                            }

                            @Override
                            public void onSuccess(long mTask_id, String source, String destination) {
                                compressionModel.setCompressionStatus(1);
                                checkVideoCompressingComplete();

                            }

                            @Override
                            public void onFail(long mTask_id, String source, String destination) {
                                compressionModel.setCompressionStatus(-1);
                                checkVideoCompressingComplete();
                            }

                            @Override
                            public void onProgress(long mTask_id, float percent) {
                            }
                        });
            }
        }

    }

    private void checkVideoCompressingComplete() {
        boolean isCompressingComplete = true;
        for (CompressionModel model :
                videoLists) {
            if (model.getCompressionStatus() == 0) {
                isCompressingComplete = false;
                break;
            }

        }
        if (isCompressingComplete) {
            dismissProgressBar();
        }
        if (isCompressingComplete && checkAllVideoCompressingSuccess()) {
            for (CompressionModel model : videoLists) {
                galleryList.set((int) model.getId(), model.getDestinationPath());
            }
        }
    }

    private boolean checkAllVideoCompressingSuccess() {

        boolean allCompressingSucces = true;
        for (CompressionModel model : videoLists) {
            if (model.getCompressionStatus() == -1) {
                allCompressingSucces = false;
                break;

            }
        }
        return allCompressingSucces;
    }



}
