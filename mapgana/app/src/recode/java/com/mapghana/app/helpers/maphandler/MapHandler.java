package com.mapghana.app.helpers.maphandler;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.base.BaseFragment;


/**
 * Created by Sunil kumar yadav on 15/3/18.
 */

public class MapHandler {

    private DashboardActivity _activity;
    private FrameLayout bottom_container, main_container;
    private RelativeLayout rl_map_group;
    private AppBaseMapBox mapBoxFragment;
    private SupportMapFragment supportMapBoxMapFragment;

    private FrameLayout frag_map;

    private int enterfromtop;
    private int exittotop;
    private int enterVisibleAnim;
    private int exitInvisibleAnim;

    public SupportMapFragment getSupportMapBoxMapFragment() {
        return supportMapBoxMapFragment;
    }

    public AppBaseMapBox getMapBoxFragment() {
        return mapBoxFragment;
    }

    public MapHandler(DashboardActivity _activity, String trackingdata) {
        this._activity = _activity;
        enterfromtop = R.anim.enterfromtop;
        exittotop = R.anim.exittotop;
        enterVisibleAnim = R.anim.alpha_visible_anim;
        exitInvisibleAnim = R.anim.alpha_gone_anim;
        setupMap();
    }

    private void setupMap() {
        rl_map_group = _activity.findViewById(R.id.rl_map_group);
        bottom_container = _activity.findViewById(R.id.bottom_container);
        frag_map = _activity.findViewById(R.id.map_contanier);
        main_container = _activity.findViewById(R.id.container);
        rl_map_group.setVisibility(View.VISIBLE);
        // main_container.setVisibility(View.GONE);
        addMapFragment();
    }

    public void addMapFragment() {
        mapBoxFragment = new AppBaseMapBox();
        supportMapBoxMapFragment = mapBoxFragment.loadMap(_activity.getFm());

    }

    public boolean isMainContainerVisible() {
        return main_container.getVisibility() == View.VISIBLE;
    }

    public void updateMainContainerVisivility(int visibility) {
        if (main_container.getVisibility() != visibility) {
            main_container.setVisibility(visibility);
        }
    }

    public void clearMapFragment() {
        mapBoxFragment.clearMarker();
    }

    public Marker addMarker(Context context, int resource, com.mapbox.mapboxsdk.geometry.LatLng latLng,
                            String title, String snipped) {
        return mapBoxFragment.addMarker(context, resource, latLng, title, snipped);
    }

    public void showLayout() {
        _activity.clearFragmentBackStack();
        rl_map_group.setVisibility(View.VISIBLE);
        // main_container.setVisibility(View.GONE);
        // main_container.removeAllViews();
        Fragment fragment = getBottomFragment();
    }

    public void hideLayout() {
        rl_map_group.setVisibility(View.GONE);
        main_container.setVisibility(View.VISIBLE);
    }

    public void addBottomFragment(BaseFragment f) {
        try {
            FragmentTransaction ft = _activity.getNewFragmentTransaction();
            ft.setCustomAnimations(enterVisibleAnim, exitInvisibleAnim, enterVisibleAnim, exitInvisibleAnim);
            ft.replace(R.id.bottom_container, f, f.getClass().getSimpleName()).addToBackStack(f.getClass().getSimpleName());
            ft.commit();
            _activity.getNavigationViewHandler().onCreateViewFragment(f);
        } catch (IllegalStateException e) {

        }
    }

    public void removeBottonFragment() {
        Fragment fragment = _activity.getFm().findFragmentById(R.id.bottom_container);
        if (fragment == null) return;
        try {
            FragmentTransaction ft = _activity.getNewFragmentTransaction();
            ft.remove(fragment);
            ft.commit();
        } catch (IllegalStateException e) {
        }

    }

    public void removeFragment(Fragment f) {
        if (f != null) {
            try {
                FragmentTransaction ft = _activity.getFm().beginTransaction();
                ft.remove(f);
                ft.commit();
            } catch (IllegalStateException e) {

            }
        }
    }

    public Fragment getBottomFragment() {
        Fragment fragment = _activity.getFm().findFragmentById(R.id.bottom_container);
        return fragment;
    }


    public int getBottomFragmentCount() {
        Fragment fragment = _activity.getFm().findFragmentById(R.id.bottom_container);
        return fragment.getFragmentManager().getBackStackEntryCount();
    }

    public void setMapPadding(int left, int top, int right, int bottom) {
        mapBoxFragment.setMapPadding(left, top, right, bottom);
        mapBoxFragment.updateMapPaddingChange();
    }
}
