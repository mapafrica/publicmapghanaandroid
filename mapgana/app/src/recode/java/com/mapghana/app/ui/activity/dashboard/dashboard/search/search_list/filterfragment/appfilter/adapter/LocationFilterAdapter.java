package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mapghana.R;
import com.mapghana.customviews.TypefaceTextView;

/**
 * Created by ubuntu on 3/1/18.
 */

public class LocationFilterAdapter extends BaseAdapter {

    private Context context;
    private String[] arrays;

    public LocationFilterAdapter(Context context, String[] arrays) {
        this.context = context;
        this.arrays = arrays;
    }

    @Override
    public int getCount() {
        return R.layout.item_location_filter;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TypefaceTextView tvTitle;
        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.item_location_filter, parent, false);
        }
        tvTitle=(TypefaceTextView)convertView.findViewById(R.id.tvTitle);
        tvTitle.setText(arrays[position]);
        return convertView;
    }
}
