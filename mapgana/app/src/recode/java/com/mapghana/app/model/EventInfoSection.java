package com.mapghana.app.model;

import java.io.Serializable;

public class EventInfoSection implements Serializable {

    private String start_date_time;
    private String end_date_time;
    private String lat;
    private String log;
    private String address;

    public EventInfoSection(String start_date_time, String end_date_time, String lat, String log, String address) {
        this.start_date_time = start_date_time;
        this.end_date_time = end_date_time;
        this.lat = lat;
        this.log = log;
        this.address = address;
    }

    public String getStart_date_time() {
        return start_date_time;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
