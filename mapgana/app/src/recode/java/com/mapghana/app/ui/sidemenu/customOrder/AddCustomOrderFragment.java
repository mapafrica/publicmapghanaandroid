package com.mapghana.app.ui.sidemenu.customOrder;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Session;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Utils;
import com.mapghana.retrofit.RetrofitUtils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class AddCustomOrderFragment extends AppBaseFragment {

    private TextView mTitle, mDescription, tvMin, tvMax, mSubmitOrder;
    private EditText seekBar;
    private ImageView orderImage;
    private String mBudgetPrice;
    private String selectedImageUri;
    private Uri uriOneImg;
    private RestClient restClient;
    private int post_id;
    private Spinner orderType;
    private String txtOrderType;
    private List<String> list;

    public static AddCustomOrderFragment newInstance() {
        AddCustomOrderFragment fragment = new AddCustomOrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle("Add Order");
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_custom_order;
    }

    @Override
    public void initializeComponent() {
        initToolBar();
        restClient = new RestClient(getContext());

        findViewById(getView());

    }

    private void findViewById(View view) {
        mTitle = view.findViewById(R.id.et_title);
        mDescription = view.findViewById(R.id.et_description);
        orderImage = view.findViewById(R.id.select_image_order);
        seekBar = view.findViewById(R.id.rangeSeekbar);
        mSubmitOrder = view.findViewById(R.id.submit_order);
        orderType = view.findViewById(R.id.spinner_order_type);

        orderImage.setOnClickListener(view12 -> {
            oneImageSelector();
        });
        mSubmitOrder.setOnClickListener(view1 -> {
            checkValiation();
        });

        if (post_id != 0) {
            mTitle.setVisibility(View.GONE);
            orderType.setVisibility(View.VISIBLE);
        } else {
            mTitle.setVisibility(View.VISIBLE);
            orderType.setVisibility(View.GONE);
        }
        setListinerSpinner();

    }

    private void setListinerSpinner() {
        list = new ArrayList<>();
        list.add("Select order type");
        list.add("Service");
        list.add("Purchase");
        list.add("Pick Up");
        list.add("Drop Up");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderType.setAdapter(dataAdapter);

        orderType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtOrderType = list.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void oneImageSelector() {
        CropImage.activity()
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        uriOneImg = result.getUri();
                        orderImage.setImageURI(uriOneImg);
                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                        displayErrorDialog("Error", error.getMessage());
                    }
                }
                break;
        }
    }

    private void checkValiation() {
        if (TextUtils.isEmpty(mTitle.getText().toString())) {
            Utils.showToast(getActivity(), getView(), "Field is manadtory");
            return;
        }

        if (TextUtils.isEmpty(mDescription.getText().toString())) {
            Utils.showToast(getActivity(), getView(), "Field is manadtory");
            return;
        }

        mBudgetPrice = seekBar.getText().toString();
        if (TextUtils.isEmpty(mBudgetPrice)) {
            Utils.showToast(getActivity(), getView(), "Please select price range.");
            return;
        }
        Session session = AppPreferences.getSession();
        displayProgressBar(false);
        HashMap<String, String> map = new HashMap<>();
        if (post_id != 0) {
            map.put("title", txtOrderType);
        } else {
            map.put("title", mTitle.getText().toString());
        }
        map.put("user_id", String.valueOf(session.getId()));
        map.put("description", mDescription.getText().toString());
        map.put("budget", mBudgetPrice+"ghs");
        MultipartBody.Part signleImgPart = null;
        if (uriOneImg != null) {
            signleImgPart = RetrofitUtils.
                    createFilePart(BaseArguments.image, uriOneImg.getPath(),
                            RetrofitUtils.MEDIA_TYPE_IMAGE_PNG);
        }

        restClient.callback(this).postAddCustomOrder(getContext(), RetrofitUtils.createMultipartRequest(map), signleImgPart);

    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_ADD_CUSTOM_ORDER) {
                dismissProgressBar();

                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    getActivity().onBackPressed();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            dismissProgressBar();
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }
}
