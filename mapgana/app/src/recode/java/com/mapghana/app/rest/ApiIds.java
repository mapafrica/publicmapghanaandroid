package com.mapghana.app.rest;


/**
 * Created by ubuntu on 1/8/16.
 */
public class ApiIds {

    public static final int ID_SIGNUP = 1;
    public static final int ID_LOGIN = 2;
    public static final int ID_FORGOT = 3;
    public static final int ID_GETPROFILE = 4;
    public static final int ID_UPDATEPROFILE = 5;
    public static final int ID_CATEGORY = 6;
    public static final int ID_PostINDIVIDUAL = 7;
    public static final int ID_contact = 8;
    public static final int ID_contact_query = 9;
    public static final int ID_city = 10;
    public static final int ID_PostOrganization = 11;
    public static final int ID_logout = 12;
    public static final int ID_post_send_review = 13;
    public static final int ID_POSTS_Filter_organization = 15;
    public static final int ID_POSTS_Filter_individual = 16;
    public static final int ID_POSTS_Details_1 = 17;
    public static final int ID_POSTS_SEarch_Filter = 20;
    public static final int ID_ADS= 21;
    public static final int ID_POPULAR_CATEGORY = 22;
    public static final int ID_EVENT_LISTING= 22;
    public static final int ID_EVENT_ADD_EVENT= 23;
    public static final int ID_OBJECT = 24;
    public static final int ID_DEEPLINKING = 25;
    public static final int ID_EVENT_DETAIL_BY_ID = 26;
    public static final int ID_OBJECT_LISTING_CATEGORY = 27;
    public static final int ID_EVENT_LISTING_BY_DATE= 28;
    public static final int ID_EVENT_CALENDAR= 29;
    public static final int ID_ADD_GROUP= 30;
    public static final int ID_GROUP_LISTING= 31;
    public static final int ID_EVENT_SEARCH_LIST= 32;
    public static final int ID_GROUP_GRID_LISTING= 33;
    public static final int ID_EVENT_ID_LISTING_FOR_GROUP= 34;
    public static final int ID_MAPGHANA_ID= 35;
    public static final int ID_OBJECT_CATEGORY_WISE_DATA= 36;
    public static final int ID_RSVP_EVENT= 37;
    public static final int ID_EVENT_EXPLORE= 38;
    public static final int ID_OBJECT_EXPLORE= 39;
    public static final int ID_GET_RSVP_EVENT= 40;
    public static final int ID_TRANSACTION = 41;
    public static final int ID_PRICE_TRANSACTION= 42;
    public static final int ID_ADD_CUSTOM_ORDER= 43;
    public static final int ID_USER_CUSTOM_ORDER_LIST= 44;
    public static final int ID_USER_STATUS= 45;

}
