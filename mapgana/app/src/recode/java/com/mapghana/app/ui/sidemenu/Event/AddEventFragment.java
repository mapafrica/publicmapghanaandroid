package com.mapghana.app.ui.sidemenu.Event;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brsoftech.customtimepicker.TimePickerDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.ui.geocoder.GeocoderAutoCompleteView;
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.services.commons.models.Position;
import com.mapghana.R;
import com.mapghana.app.adapter.AddEventGalleryAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.interfaces.EventMapListener;
import com.mapghana.app.model.CompressionModel;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.model.MapLocationMapModel;
import com.mapghana.app.model.PaymentResponse;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.TransactionPrice;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.ui.activity.dashboard.dashboard.add_location.AddLocationFragment;
import com.mapghana.app.ui.navigationView.MapviewNavigation;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.handler.MediaAddAdpterListener;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.mapghana.videocompression.VideoCompress;
import com.theartofdev.edmodo.cropper.CropImage;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;

public class AddEventFragment extends AppBaseFragment implements View.OnClickListener, MediaAddAdpterListener, CustomDatePickerDialog.OnDateListener, TimePickerDialog.TimePickerListner, GeocoderAutoCompleteView.OnFeatureListener, MapboxMap.OnInfoWindowClickListener, OnMapReadyCallback
        , LocationServiceListner, MapboxMap.OnMapClickListener, DialogCallback {
    private static final String TAG = "AddEvent";
    private static final int REQUEST_CODE_CHOOSE_PREVIOUS_MULTIPLE = 887;
    private static final int REQUEST_CODE_CHOOSE_CURRENT_MULTIPLE = 888;
    private static final int REQUEST_CODE_CHOOSE_SINGLE = 889;
    private EditText etEventName, etEventAddress, etEventAbout, etEventHostName, etEventPhone, etEventEmail, etNoOfSeats, etTicketPrice;
    private SwitchCompat eventRecurringSwitch, eventCoverageSwitch, eventPaidEventSwitch, eventMAPGHTicketHosting, eventMtnMobileSwitch, eventAirtelMobileSwitch, eventVodafoneSwitch;
    private AppCompatTextView etEventStartTime, etEventEndTime, addEventButton, etEventDate, addRecurring, txt_post_channel;
    private RecyclerView rvPreviousMedia, rvCurrentMedia;
    private RelativeLayout flEventPoster;
    private LinearLayout llRecursiveView, llPaidEventSection, llPaymentChannelMode, llPaymentChannelModeVodafone, llPaymentChannelModeAirtel, llPaymentChannelModeMtnMoney, llCoverageView;
    private ImageView posterImage, posterIcon;
    private RadioGroup radioGroupEventCoverage, radioGroupEventSeatsRange;
    private AppCompatCheckBox checkBoxGateKeepingServices;
    private TextInputLayout textInputLayoutNoOfSeats;
    private List<String> previousMediaGallery, currentMediaGallery;
    private String imagePoster, title;
    private Marker marker;
    private AddEventGalleryAdapter previousGalleryAdapter, currentGalleryAdapter;
    private Date date;
    private List<CompressionModel> currentVideoLists = new ArrayList<>();
    private List<CompressionModel> previouseVideoLists = new ArrayList<>();

    private int time_type;
    private TimePickerDialog timePickerDialog;
    private List<String> video_mime_List;
    private RestClient restClient;
    private List<EventInfoSection> eventSectionList;
    private String eventCoverage, address, eventCoverageOn, seats, paidEvent = "N", ticketHosting = "N", postChannelMethod, gateKeepingServices = "N", type = "";
    private GeocoderAutoCompleteView geocoderAutoCompleteView;
    private String latitude = "0.0";
    private String longitude = "0.0";
    private LatLng latLng;
    private Point originPoint;
    private ImageView addEventLocation;
    private boolean isEditLocation = true;
    private MapboxMap mapboxMap;
    private MapView mapView;
    private Marker pointMarker;
    private String recurrsiveChildPositionCount;
    private String paymentMode, mtnMobile, aitelTigo, vodafone;
    private CardView cardBankAccount, cardMtnMobileMoney, cardAirtelTigoMobileMoney, cardVodafoneCash;
    private EventMapListener eventMapListener, eventMapRecurrsiveListener;
    private LinearLayout llTicketHosting;
    private Uri uriOneImg;
    private String screenType, paymentType;
    private ImageView mShowCoverageView, mShowPaidEventView;
    private boolean isEventCoverage, isPaidEvent;
    private EditText mtnHolderName, mtnHolderPhoneNumber, airtelTigoHolderName, airtelTigoHolderPhoneNumber, vodafoneHolderName, vodafoneHolderPhoneNumber, vodafoneVoucher;
    private boolean isCoverageShow, isPaidEventShow;
    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver paymentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            screenType = intent.getStringExtra("screen_type");
            paymentType = intent.getStringExtra("payment_type");
            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_SUCCESS)) {
                if ("event_coverage".equalsIgnoreCase(paymentType) && "add_event".equalsIgnoreCase(screenType)) {

                    isEventCoverage = true;
//                    eventCoverageSwitch.setChecked(true);
//                    eventCoverageSwitch.setEnabled(false);
                    radioGroupEventCoverage.setVisibility(View.VISIBLE);
                } else if ("paid_event".equalsIgnoreCase(paymentType) && "add_event".equalsIgnoreCase(screenType)) {
                    isPaidEvent = true;
                    llPaidEventSection.setVisibility(View.VISIBLE);
                    eventPaidEventSwitch.setChecked(true);
                    eventPaidEventSwitch.setEnabled(false);
                }

                Gson gson = new Gson();
                PaymentResponse paymentResponse = gson.fromJson(intent.getStringExtra(Constants.data), PaymentResponse.class);
                callPaymentToServer(paymentResponse);

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_ERROR)) {
                if ("event_coverage".equalsIgnoreCase(paymentType) && "add_event".equalsIgnoreCase(screenType)) {
                    radioGroupEventCoverage.setVisibility(GONE);
                    isEventCoverage = false;
//                    eventCoverageSwitch.setChecked(false);
//                    eventCoverageSwitch.setEnabled(true);
                } else if ("paid_event".equalsIgnoreCase(paymentType) && "add_event".equalsIgnoreCase(screenType)) {
                    isPaidEvent = false;
                    llPaidEventSection.setVisibility(GONE);
                    eventPaidEventSwitch.setChecked(false);
                    eventPaidEventSwitch.setEnabled(true);
                }

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_CANCELLED)) {
                if ("event_coverage".equalsIgnoreCase(paymentType) && "add_event".equalsIgnoreCase(screenType)) {
                    radioGroupEventCoverage.setVisibility(View.GONE);
                    isEventCoverage = false;
//                    eventCoverageSwitch.setChecked(false);
//                    eventCoverageSwitch.setEnabled(true);
                } else if ("paid_event".equalsIgnoreCase(paymentType) && "add_event".equalsIgnoreCase(screenType)) {
                    isPaidEvent = false;
                    llPaidEventSection.setVisibility(GONE);
                    eventPaidEventSwitch.setChecked(false);
                    eventPaidEventSwitch.setEnabled(true);
                }
            }

        }
    };

    public static AddEventFragment newInstance() {
        AddEventFragment fragment = new AddEventFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_event;
    }

    @Override
    public void initializeComponent() {
        initToolBar();
        initViewId(getView());
        initRecycleLayoutManager();
        initListeners();
        initAutoCompeteBox();
        initMap();

        Location location = Locationservice.sharedInstance().getLastLocation();
        if (location != null) {
            originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
        super.onCreate(savedInstanceState);

    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Add_events));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }

    private void initMap() {
        if (getActivity() != null) {
            mapView = getView().findViewById(R.id.mapView);

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.addOnMapClickListener(this);
        setCameraPosition(latLng);

        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {

            GeoJsonSource source = mapboxMap.getStyle().getSourceAs("destination-source-id");
            if (source != null) {
                Point desitnationPosition = Point.fromLngLat(latLng.getLatitude(), latLng.getLongitude());
                source.setGeoJson(Feature.fromGeometry(desitnationPosition));
            }
        });

    }

    private void initViewId(View view) {
        restClient = new RestClient(getContext());
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
        eventSectionList = new ArrayList<>();
        etEventName = view.findViewById(R.id.event_name);
        etEventDate = view.findViewById(R.id.event_date);
        etEventStartTime = view.findViewById(R.id.event_start_time);
        etEventEndTime = view.findViewById(R.id.event_end_time);
        etEventAbout = view.findViewById(R.id.event_about);
        etEventHostName = view.findViewById(R.id.event_host_name);
        etEventPhone = view.findViewById(R.id.event_host_phonenumber);
        etEventEmail = view.findViewById(R.id.event_host_email);
        etNoOfSeats = view.findViewById(R.id.event_no_of_seats);
        etTicketPrice = view.findViewById(R.id.event_ticket_price);
        posterImage = view.findViewById(R.id.event_poster);
        textInputLayoutNoOfSeats = view.findViewById(R.id.text_input_layout_event_no_of_seats);
        radioGroupEventCoverage = view.findViewById(R.id.radio_group_event_coverage);
        radioGroupEventSeatsRange = view.findViewById(R.id.radio_group_event_seats);
        posterIcon = view.findViewById(R.id.image_icon);
        eventRecurringSwitch = view.findViewById(R.id.event_recurring_switch);
//        eventCoverageSwitch = view.findViewById(R.id.event_coverage_switch);
        eventPaidEventSwitch = view.findViewById(R.id.paid_event_switch);
        eventMAPGHTicketHosting = view.findViewById(R.id.mapgh_ticket_hosting);
        addRecurring = view.findViewById(R.id.add_multiple_event_recurring);
        checkBoxGateKeepingServices = view.findViewById(R.id.checkbox_gatekeeping_services);
        txt_post_channel = view.findViewById(R.id.txt_post_channel);
        llTicketHosting = view.findViewById(R.id.ll_ticket_hosting);
        addEventLocation = view.findViewById(R.id.add_location);
        flEventPoster = view.findViewById(R.id.frame_poster);
        etEventAddress = view.findViewById(R.id.et_location);
        llRecursiveView = view.findViewById(R.id.ll_recursive_event_view);
        llPaidEventSection = view.findViewById(R.id.ll_paid_event_section);
        llPaymentChannelModeVodafone = view.findViewById(R.id.ll_event_payment_channel_mode_vodafone);
        llPaymentChannelModeAirtel = view.findViewById(R.id.ll_event_payment_channel_mode_airtel);
        llPaymentChannelModeMtnMoney = view.findViewById(R.id.ll_event_payment_channel_mode_mtn);
        rvPreviousMedia = view.findViewById(R.id.rv_previous_media);
        rvCurrentMedia = view.findViewById(R.id.rv_current_media);
        addEventButton = view.findViewById(R.id.tv_add_event);

        llCoverageView = view.findViewById(R.id.ll_coverage_view);
//        mShowCoverageView = view.findViewById(R.id.show_coverage_view);
        mShowPaidEventView = view.findViewById(R.id.show_paid_event_view);
        eventAirtelMobileSwitch = view.findViewById(R.id.paid_airtel_tigo_mobile);

        eventMtnMobileSwitch = view.findViewById(R.id.paid_mtn_mobile_money);
        eventVodafoneSwitch = view.findViewById(R.id.paid_vodafone_cash);

        cardMtnMobileMoney = view.findViewById(R.id.card_MtnMobileMoney);
        cardAirtelTigoMobileMoney = view.findViewById(R.id.card_airtel_money);
        cardVodafoneCash = view.findViewById(R.id.card_vodafone_cash);

        mtnHolderName = view.findViewById(R.id.mtn_holder_name);
        mtnHolderPhoneNumber = view.findViewById(R.id.mtn_holder_number);

        airtelTigoHolderName = view.findViewById(R.id.airtel_tigo_holder_name);
        airtelTigoHolderPhoneNumber = view.findViewById(R.id.airtel_tigo_holder_phonenumber);

        vodafoneHolderName = view.findViewById(R.id.vodafone_holder_name);
        vodafoneHolderPhoneNumber = view.findViewById(R.id.vodafone_holder_phonenumber);
        vodafoneVoucher = view.findViewById(R.id.vodafone_voucher);


// Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                paymentReceiver,
                new IntentFilter(Constants.LOCAL_BROADCAST_PAYMENT)
        );

//        mShowCoverageView.setOnClickListener(v -> {
//            toggleCoverageView(isCoverageShow);
//        });

        mShowPaidEventView.setOnClickListener(v -> {
            togglePaidEventView(isPaidEventShow);
        });

    }

    private void toggleCoverageView(boolean value) {

        if (!value) {
            isCoverageShow = true;
            llCoverageView.setVisibility(View.VISIBLE);
            mShowCoverageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_up));

        } else {
            llCoverageView.setVisibility(View.GONE);
            isCoverageShow = false;
            mShowCoverageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down_black));

        }
    }

    private void togglePaidEventView(boolean value) {

        if (!value) {
            isPaidEventShow = true;
            llPaidEventSection.setVisibility(View.VISIBLE);
            mShowPaidEventView.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_up));
        } else {
            llPaidEventSection.setVisibility(View.GONE);
            isPaidEventShow = false;
            mShowPaidEventView.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down_black));

        }
    }

    private void initListeners() {

        eventMapListener = item -> {
            if (item != null) {
                latitude = item.getLat();
                longitude = item.getLog();
                address = item.getAddress();
                latLng = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
                etEventAddress.setEnabled(true);
                etEventAddress.setText(item.getAddress());
                setCameraPosition(latLng);
                mapView.setVisibility(View.VISIBLE);
            } else {
                Utils.showToast(getActivity(), getView(), "No location found");
            }

        };
        addEventLocation.setOnClickListener(view1 -> {
            AddLocationFragment mapViewFragment = new AddLocationFragment();
            mapViewFragment.setAddLocationListenerType(eventMapListener, Constants.ADDEVENTS, new LatLng(Double.valueOf(latitude), Double.valueOf(longitude)));
            ((AppBaseActivity) getActivity()).addFragment(mapViewFragment, true);
        });


        eventMtnMobileSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                mtnMobile = "mtnMobile";
                cardMtnMobileMoney.setVisibility(View.VISIBLE);
            } else {
                cardMtnMobileMoney.setVisibility(View.GONE);
                mtnMobile = "";
                mtnHolderName.setText("");
                mtnHolderPhoneNumber.setText("");
            }
        });

        eventAirtelMobileSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                aitelTigo = "airtelMobile";
                cardAirtelTigoMobileMoney.setVisibility(View.VISIBLE);
            } else {
                cardAirtelTigoMobileMoney.setVisibility(View.GONE);
                aitelTigo = "";
                airtelTigoHolderName.setText("");
                airtelTigoHolderPhoneNumber.setText("");
            }
        });

        eventVodafoneSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                vodafone = "vodafone";
                cardVodafoneCash.setVisibility(View.VISIBLE);
            } else {
                cardVodafoneCash.setVisibility(View.GONE);
                vodafone = "";
                vodafoneHolderName.setText("");
                vodafoneHolderPhoneNumber.setText("");
                vodafoneVoucher.setText("");
            }
        });


        eventRecurringSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                type = "recurring";
                addRecurring.setVisibility(View.VISIBLE);
                addEventSectionView();
            } else {
                llRecursiveView.removeAllViews();
                addRecurring.setVisibility(View.GONE);
                type = "";
            }
        });
//        eventCoverageSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
//            if (compoundButton.isChecked()) {
//                Utils.openDialog((AppBaseActivity) getActivity(), "HIGHLIGHTS / COVERAGE", getString(R.string.high_light_coverage), "event_coverage", this);
//                eventCoverageOn = "Y";
//            } else {
//                radioGroupEventCoverage.setVisibility(View.GONE);
//                eventCoverageOn = "N";
//            }
//        });

        radioGroupEventCoverage.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_photo) {
                eventCoverage = getIfEventCoverage(R.id.radio_photo);
                Utils.openDialog((AppBaseActivity) getActivity(), "HIGHLIGHTS / COVERAGE", getString(R.string.high_light_photos_coverage), "event_coverage", this);

            } else if (checkedId == R.id.radio_video) {
                eventCoverage = getIfEventCoverage(R.id.radio_video);
                Utils.openDialog((AppBaseActivity) getActivity(), "HIGHLIGHTS / COVERAGE", getString(R.string.high_light_videos_coverage), "event_coverage", this);

            } else if (checkedId == R.id.radio_photo_video) {
                eventCoverage = getIfEventCoverage(R.id.radio_photo_video);
                Utils.openDialog((AppBaseActivity) getActivity(), "HIGHLIGHTS / COVERAGE", getString(R.string.high_light_photos_videos_coverage), "event_coverage", this);

            }
        });
        eventPaidEventSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                paidEvent = "Y";
                TransactionPrice price = AppPreferences.getTransactionPrice();
                Utils.openDialog((AppBaseActivity) getActivity(), "Optimization", String.format(Locale.ENGLISH, getString(R.string.optimization_dialog_message), String.valueOf(price.getEvent().getPaid_event())), "paid_event", this);
            } else {
                paidEvent = "N";
                eventMAPGHTicketHosting.setChecked(false);
                llPaidEventSection.setVisibility(View.GONE);

            }
        });

        radioGroupEventSeatsRange.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_event_limited_seats) {
                seats = "LIMITED";
                textInputLayoutNoOfSeats.setVisibility(View.VISIBLE);
            } else if (checkedId == R.id.radio_event_unlimited_seats) {
                seats = "UNLIMITED";
                etNoOfSeats.setText("");
                textInputLayoutNoOfSeats.setVisibility(View.GONE);
            } else {
                textInputLayoutNoOfSeats.setVisibility(View.GONE);
            }
        });

        eventMAPGHTicketHosting.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b) {
                ticketHosting = "Y";
                llTicketHosting.setVisibility(View.VISIBLE);
            } else {
                ticketHosting = "N";
                llTicketHosting.setVisibility(View.GONE);
            }
        });

        checkBoxGateKeepingServices.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b) {
                gateKeepingServices = "Y";
            } else {
                gateKeepingServices = "N";
            }
        });

        etEventDate.setOnClickListener(this);
        addEventButton.setOnClickListener(this);
        etEventStartTime.setOnClickListener(this);
        etEventEndTime.setOnClickListener(this);
        flEventPoster.setOnClickListener(this);
        addRecurring.setOnClickListener(this);

    }

    private void openMakePayemntScreen(String screenType, String paymentType, int amount) {
        Intent intent = new Intent(getActivity(), MapghanaPayment.class);
        intent.putExtra("id", "0");
        intent.putExtra("screen_type", screenType);
        intent.putExtra("payment_type", paymentType);
        intent.putExtra("amount", amount);
        startActivity(intent);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(paymentReceiver);
    }

    private void callPaymentToServer(PaymentResponse paymentResponse) {
        Session session = AppPreferences.getSession();
        restClient.callback(this).transaction(String.valueOf(0), String.valueOf(paymentResponse.getData().getAmount()), paymentResponse.getData().getPaymentId(), session.getEmail(), session.getName(), session.getName(), screenType, paymentType);
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_EVENT_ADD_EVENT) {
                dismissProgressBar();
                getActivity().onBackPressed();
            } else if (apiId == ApiIds.ID_TRANSACTION) {
                Utils.showToast(getActivity(), getView(), "transaction successfully");
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        super.onFailResponse(apiId, error);
        dismissProgressBar();
        Utils.showToast(getActivity(), getView(), "failure");
    }

    private void checkEmptyValidation() {

        if (uriOneImg != null) {
            if (TextUtils.isEmpty(uriOneImg.getPath())) {
                Utils.showToast(getActivity(), null, "Please add event poster image");
                Utils.showToast(getActivity(), getView(), "Please add event poster image");
                return;
            }
        } else {
            Utils.showToast(getActivity(), null, "Please add event poster image");
            Utils.showToast(getActivity(), getView(), "Please add event poster image");
            return;
        }

        if (TextUtils.isEmpty(etEventName.getText().toString())) {
            etEventName.setError("Please add event name");
            Utils.showToast(getActivity(), getView(), "Please add event name ");
            etEventName.setFocusable(true);
            return;
        } else {
            etEventName.setError(null);
        }

        if (TextUtils.isEmpty(etEventDate.getText().toString())) {
            etEventDate.setError("Please select event date");
            Utils.showToast(getActivity(), getView(), "Please select event date");
            return;
        } else {
            etEventDate.setError(null);
        }

        if (TextUtils.isEmpty(etEventAddress.getText().toString())) {
            etEventAddress.setError("Please add location ");
            Utils.showToast(getActivity(), getView(), "Please add location ");
            return;
        } else {
            etEventAddress.setError(null);
        }


        if (TextUtils.isEmpty(etEventStartTime.getText().toString())) {
            etEventStartTime.setError("Please select event start time");
            Utils.showToast(getActivity(), getView(), "Please select event start time");
            return;
        } else {
            etEventStartTime.setError(null);
        }

        if (TextUtils.isEmpty(etEventEndTime.getText().toString())) {
            etEventEndTime.setError("Please select event end time");
            Utils.showToast(getActivity(), getView(), "Please select event end time");
            return;
        } else {
            etEventEndTime.setError(null);
        }

        if (TextUtils.isEmpty(etEventAbout.getText().toString())) {
            etEventAbout.setError("Please add something about event");
            Utils.showToast(getActivity(), getView(), "Please add something about event ");
            return;
        } else {
            etEventAbout.setError(null);
        }

        if (!TextUtils.isEmpty(etEventPhone.getText().toString()) && (etEventPhone.getText().toString().length() < 7 || etEventPhone.getText().toString().length() > 12)) {
            etEventPhone.setError("Please enter valid phone number");
            Utils.showToast(getActivity(), getView(), "Please enter valid phone number ");
            return;
        } else {
            etEventPhone.setError(null);
        }

        try {
            onPostSubmitAddEvent();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean isVideoCompressingforPreviousNeeded() {

        previouseVideoLists.clear();
        String path = "";
        CompressionModel compressionModel;

        for (int i = 0; i < previousMediaGallery.size(); i++) {
            path = previousMediaGallery.get(i);
            FilePathHelper filePathHelper = new FilePathHelper();
            final String mimeType = filePathHelper.getMimeType(path);
            if (video_mime_List.contains(mimeType)) {
                compressionModel = new CompressionModel();
                compressionModel.setSourcePath(path);
                compressionModel.setId(i);

                previouseVideoLists.add(compressionModel);
            }
        }

        return previouseVideoLists.size() > 0;
    }

    private boolean isVideoCompressingforCurrentNeeded() {

        currentVideoLists.clear();
        String path = "";
        CompressionModel compressionModel;

        for (int i = 0; i < currentMediaGallery.size(); i++) {
            path = currentMediaGallery.get(i);
            FilePathHelper filePathHelper = new FilePathHelper();
            final String mimeType = filePathHelper.getMimeType(path);
            if (video_mime_List.contains(mimeType)) {
                compressionModel = new CompressionModel();
                compressionModel.setSourcePath(path);
                compressionModel.setId(i);

                currentVideoLists.add(compressionModel);
            }
        }

        return currentVideoLists.size() > 0;
    }

    private void compressVideo(List<CompressionModel> filePath) {

        String outputDir = Environment.getExternalStorageDirectory() + "/MapGhanaVideo";
        File file = new File(outputDir);

        if (!file.exists()) {
            file.mkdirs();
        }


        if (filePath != null && filePath.size() > 0) {
            displayProgressBar(false, "Video is compressing...");
            for (int i = 0; i < filePath.size(); i++) {
                File file1 = new File(filePath.get(i).getSourcePath());
                String destPath = outputDir + File.separator + "MapGhana_VID_IND_" + i +
                        System.currentTimeMillis() + ".mp4";

                final CompressionModel compressionModel = filePath.get(i);
                compressionModel.setDestinationPath(destPath);
                compressionModel.setCompressionStatus(0);
                VideoCompress.compressVideoHigh(compressionModel.getSourcePath(),
                        compressionModel.getDestinationPath(), compressionModel.getId(),
                        new VideoCompress.CompressListener() {
                            @Override
                            public void onStart(long mTask_id) {
                            }

                            @Override
                            public void onSuccess(long mTask_id, String source, String destination) {
                                compressionModel.setCompressionStatus(1);
                                checkPreviousVideoCompressingComplete();
                                try {
                                    checkCurrentVideoCompressingComplete();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFail(long mTask_id, String source, String destination) {
                                compressionModel.setCompressionStatus(-1);
                                checkPreviousVideoCompressingComplete();
                                try {
                                    checkCurrentVideoCompressingComplete();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onProgress(long mTask_id, float percent) {
                            }
                        });
            }
        }

    }

    private void checkPreviousVideoCompressingComplete() {
        boolean isCompressingComplete = true;
        for (CompressionModel model :
                previouseVideoLists) {
            if (model.getCompressionStatus() == 0) {
                isCompressingComplete = false;
                break;
            }

        }
        if (isCompressingComplete) {
            dismissProgressBar();
        }
        if (isCompressingComplete && checkPreviousVideoCompressingSuccess()) {
            for (CompressionModel model : previouseVideoLists) {
                previousMediaGallery.set((int) model.getId(), model.getDestinationPath());
            }

        }
    }

    private void checkCurrentVideoCompressingComplete() throws Exception {
        boolean isCompressingComplete = true;
        for (CompressionModel model :
                currentVideoLists) {
            if (model.getCompressionStatus() == 0) {
                isCompressingComplete = false;
                break;
            }

        }
        if (isCompressingComplete) {
            dismissProgressBar();
        }
        if (isCompressingComplete && checkCurrentVideoCompressingSuccess()) {
            for (CompressionModel model : currentVideoLists) {
                currentMediaGallery.set((int) model.getId(), model.getDestinationPath());
            }
        }
    }

    private boolean checkPreviousVideoCompressingSuccess() {

        boolean allCompressingSucces = true;
        for (CompressionModel model : previouseVideoLists) {
            if (model.getCompressionStatus() == -1) {
                allCompressingSucces = false;
                break;

            }
        }
        return allCompressingSucces;
    }

    private boolean checkCurrentVideoCompressingSuccess() {

        boolean allCompressingSucces = true;
        for (CompressionModel model : previouseVideoLists) {
            if (model.getCompressionStatus() == -1) {
                allCompressingSucces = false;
                break;

            }
        }
        for (CompressionModel model : currentVideoLists) {
            if (model.getCompressionStatus() == -1) {
                allCompressingSucces = false;
                break;

            }
        }
        return allCompressingSucces;
    }

    private String getIfEventCoverage(int radioId) {
        switch (radioId) {
            case R.id.radio_photo:
                return "Photo";
            case R.id.radio_video:
                return "Video";
            case R.id.radio_photo_video:
                return "both";
        }
        return "";
    }

    private void initRecycleLayoutManager() {
        previousMediaGallery = new ArrayList<>();
        currentMediaGallery = new ArrayList<>();
        rvCurrentMedia.setHasFixedSize(true);
        rvPreviousMedia.setHasFixedSize(true);
        previousGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "previous_media", this, previousMediaGallery);

        rvPreviousMedia.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvPreviousMedia.setAdapter(previousGalleryAdapter);

        currentGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "current_media", this, currentMediaGallery);
        rvCurrentMedia.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvCurrentMedia.setAdapter(currentGalleryAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_event:
                checkEmptyValidation();
                break;
            case R.id.add_event_previous_media:
                hideKeyboard();

                break;
            case R.id.add_event_current_media:
                hideKeyboard();
                multipleCurrentMediaSelector();
                break;
            case R.id.frame_poster:
                hideKeyboard();
                SingleMediaSelector();
                break;
            case R.id.event_date:
                type = "event_date";
                onDobDialog();
                break;
            case R.id.event_start_time:
                hideKeyboard();
                displayTimePickerDialog(1);
                break;
            case R.id.event_end_time:
                hideKeyboard();
                displayTimePickerDialog(2);
                break;
            case R.id.add_multiple_event_recurring:
                addEventSectionView();
                break;
        }
    }


    private void initAutoCompeteBox() {

        geocoderAutoCompleteView = getView().findViewById(R.id.event_location);
        geocoderAutoCompleteView.setAccessToken(getString(R.string.mapbox_api_token));
        geocoderAutoCompleteView.setType(GeocodingCriteria.TYPE_POI);
        geocoderAutoCompleteView.setOnFeatureListener(this);
    }

    private void addEventSectionView() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_recurring_event, null);
        TextView startEventDate = addView.findViewById(R.id.event_start_date);
        TextView startEventTime = addView.findViewById(R.id.event_start_time);
        TextView endEventDate = addView.findViewById(R.id.event_end_date);
        TextView endEventTime = addView.findViewById(R.id.event_end_time);
        TextView eventRecurringAddress = addView.findViewById(R.id.event_recurring_address);
        TextView latText = addView.findViewById(R.id.lat);
        TextView longText = addView.findViewById(R.id.log);
        ImageView mapIcon = addView.findViewById(R.id.map_icon);
        ImageView deleteIcon = addView.findViewById(R.id.delete_icon);

        CustomDatePickerDialog.OnDateListener onDateListener = date -> {
            if (date != null) {
                this.date = date;
                SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
                String mDob = format.format(date);
                if (type.equalsIgnoreCase("event_start_date")) {
                    startEventDate.setText(mDob);
                } else if (type.equalsIgnoreCase("event_end_date")) {
                    endEventDate.setText(mDob);
                }

            }
        };

        TimePickerDialog.TimePickerListner timePickerListner = new TimePickerDialog.TimePickerListner() {

            @Override
            public void OnDoneButton(Dialog dialog, Calendar c) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                String time = simpleDateFormat.format(c.getTime());
                if (time_type == 3) {
                    startEventTime.setText(time);
                }
                if (time_type == 4) {
                    endEventTime.setText(time);
                }
                timePickerDialog.dismiss();
            }

            @Override
            public void OnCancelButton(Dialog dialog) {
                dialog.dismiss();
            }
        };

        eventMapRecurrsiveListener = item -> {
            if (item != null) {
                eventRecurringAddress.setText(item.getAddress());
                latText.setText(item.getLat());
                longText.setText(item.getLog());
            } else {
                Utils.showToast(getActivity(), getView(), "No location found");
            }

        };

        mapIcon.setOnClickListener(view -> {
            AddLocationFragment mapViewFragment = new AddLocationFragment();
            mapViewFragment.setClickOnMapOff(false);
            mapViewFragment.setAddLocationListenerType(eventMapRecurrsiveListener, Constants.ADDEVENTS, new LatLng(Double.valueOf(latitude), Double.valueOf(longitude)));
            ((AppBaseActivity) getActivity()).addFragment(mapViewFragment, true);

        });
        startEventTime.setOnClickListener(view -> {
            hideKeyboard();
            displayTimePickerDialogEventSection(3, timePickerListner);

        });

        endEventTime.setOnClickListener(view -> {
            hideKeyboard();
            displayTimePickerDialogEventSection(4, timePickerListner);
        });
        startEventDate.setOnClickListener(view -> {
            type = "event_start_date";
            onEventSectionDobDialog(onDateListener);

        });
        endEventDate.setOnClickListener(view -> {
            type = "event_end_date";
            onEventSectionDobDialog(onDateListener);

        });

        if (llRecursiveView.getChildCount() == 0) {
            deleteIcon.setVisibility(View.GONE);

        }

        deleteIcon.setOnClickListener(v ->
        {
            if (llRecursiveView.getChildCount() == 0) {
                deleteIcon.setVisibility(View.GONE);
                eventRecurringSwitch.setChecked(false);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(llRecursiveView.getChildCount());
        llRecursiveView.addView(addView);

    }


    private void multiplePreviousMediaSelector() {
        displayLog(TAG, "multipleImageVideoSelector: ");
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_PREVIOUS_MULTIPLE);

    }

    private void multipleCurrentMediaSelector() {
        displayLog(TAG, "multipleImageVideoSelector: ");
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_CURRENT_MULTIPLE);

    }

    private void SingleMediaSelector() {
        CropImage.activity()
                .start(getContext(), this);

    }

    private void displayTimePickerDialogEventSection(int type, TimePickerDialog.TimePickerListner timePickerListner) {
        time_type = type;
        timePickerDialog = new TimePickerDialog(getActivity(), Calendar.getInstance(), timePickerListner);
        timePickerDialog.show();

    }

    private void displayTimePickerDialog(int type) {
        time_type = type;
        timePickerDialog = new TimePickerDialog(getActivity(), Calendar.getInstance(), this);
        timePickerDialog.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CODE_CHOOSE_PREVIOUS_MULTIPLE) {
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                previousMediaGallery.addAll(paths);
                displayLog(TAG, "onActivityResult: " + previousMediaGallery.get(0));
                refreshPreviousGalleryAdapter();
                if (isVideoCompressingforPreviousNeeded()) {
                    compressVideo(previouseVideoLists);

                }
            }
        } else if (requestCode == REQUEST_CODE_CHOOSE_CURRENT_MULTIPLE) {
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                currentMediaGallery.addAll(paths);
                displayLog(TAG, "onActivityResult: " + currentMediaGallery.get(0));
                refreshCurrentGalleryAdapter();
                if (isVideoCompressingforCurrentNeeded()) {
                    compressVideo(currentVideoLists);
                }
            }
        }

        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        uriOneImg = result.getUri();
                        posterIcon.setVisibility(View.GONE);
                        posterImage.setImageURI(uriOneImg);
                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                        displayErrorDialog("Error", error.getMessage());
                    }
                }
                break;
        }
    }

    private void refreshPreviousGalleryAdapter() {
        if (previousMediaGallery != null) {
            previousGalleryAdapter.notifyDataSetChanged();
        }
    }

    private void refreshCurrentGalleryAdapter() {
        if (currentMediaGallery != null) {
            currentGalleryAdapter.notifyDataSetChanged();
        }
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        dialog.create(getContext(), false).callback(this).show();
    }

    private void onEventSectionDobDialog(CustomDatePickerDialog.OnDateListener listener) {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        dialog.create(getContext(), false).callback(listener).show();
    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            this.date = date;
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            String mDob = format.format(date);
            etEventDate.setText(mDob);
        }
    }

    public void navigateToLocationScreen(String type) {
        Intent intent = new Intent(getActivity(), MapviewNavigation.class);
        MapLocationMapModel model = new MapLocationMapModel(latitude, longitude, type, address, true);
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", model);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void OnDoneButton(Dialog dialog, Calendar c) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(c.getTime());

        if (time_type == 1) {
            etEventStartTime.setText(time);
        }
        if (time_type == 2) {
            etEventEndTime.setText(time);
        }
        timePickerDialog.dismiss();
    }

    @Override
    public void OnCancelButton(Dialog dialog) {
        timePickerDialog.dismiss();
    }

    private void addEventInfoSection() {
        eventSectionList.add(new EventInfoSection(Utils.getYYYYMMDDHHSS(etEventDate.getText().toString() + " " + etEventStartTime.getText().toString()), Utils.getYYYYMMDDHHSS(etEventDate.getText().toString() + " " + etEventEndTime.getText().toString()), String.valueOf(latLng.getLatitude()), String.valueOf(latLng.getLongitude()), address));
        int count = llRecursiveView.getChildCount();
        if (llRecursiveView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = llRecursiveView.getChildAt(i - 1);
                TextView startEventDate = addView.findViewById(R.id.event_start_date);
                TextView startEventTime = addView.findViewById(R.id.event_start_time);
                TextView endEventDate = addView.findViewById(R.id.event_end_date);
                TextView endEventTime = addView.findViewById(R.id.event_end_time);
                TextView eventRecurringAddress = addView.findViewById(R.id.event_recurring_address);
                TextView latText = addView.findViewById(R.id.lat);
                TextView logText = addView.findViewById(R.id.log);

                String startDate = startEventDate.getText().toString();
                String startTime = startEventTime.getText().toString();
                String endDate = endEventDate.getText().toString();
                String endTime = endEventTime.getText().toString();
                eventSectionList.add(new EventInfoSection(Utils.getYYYYMMDDHHSS(startDate + " " + startTime), Utils.getYYYYMMDDHHSS(endDate + " " + endTime), latText.getText().toString(), logText.getText().toString(), eventRecurringAddress.getText().toString()));
            }
        }
    }

    private void onPostSubmitAddEvent() throws Exception {
        displayProgressBar(false);
        addEventInfoSection();
        Session session = AppPreferences.getSession();
        HashMap<String, String> map = new HashMap<>();
        map.put(BaseArguments.name, etEventName.getText().toString());
        map.put(BaseArguments.type, type);
        map.put(BaseArguments.details, etEventAbout.getText().toString());
        map.put(BaseArguments.phone, etEventPhone.getText().toString());
        map.put(BaseArguments.email, etEventEmail.getText().toString());
        map.put(BaseArguments.user_id, String.valueOf(session.getId()));
        map.put(BaseArguments.event_coverage_on, eventCoverageOn);
        map.put(BaseArguments.event_coverage, eventCoverage);
        map.put(BaseArguments.host_name, etEventHostName.getText().toString());
        map.put(BaseArguments.paid_event, paidEvent);
        map.put(BaseArguments.ticket_price, etTicketPrice.getText().toString());
        map.put(BaseArguments.mapgh_ticket_hosting, ticketHosting);
        map.put(BaseArguments.post_payment_channel, postChannelMethod);
        map.put(BaseArguments.mtnmobile, mtnMobile);
        map.put(BaseArguments.mtnmobile_holder_name, mtnHolderName.getText().toString().trim());
        map.put(BaseArguments.mtnmobile_holder_phone_num, mtnHolderPhoneNumber.getText().toString().trim());
        map.put(BaseArguments.airteltigo, aitelTigo);
        map.put(BaseArguments.airteltigo_holder_name, airtelTigoHolderName.getText().toString().trim());
        map.put(BaseArguments.airteltigo_holder_phone_num, airtelTigoHolderPhoneNumber.getText().toString().trim());
        map.put(BaseArguments.vodafone, vodafone);
        map.put(BaseArguments.vodafone_holder_name, vodafoneHolderName.getText().toString().trim());
        map.put(BaseArguments.vodafone_holder_phone_num, vodafoneHolderPhoneNumber.getText().toString().trim());
        map.put(BaseArguments.vodafone_voucher, vodafoneVoucher.getText().toString());

        map.put(BaseArguments.gatekeeping_services, gateKeepingServices);
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(eventSectionList, new TypeToken<List<EventInfoSection>>() {
        }.getType());

        if (!element.isJsonArray()) {
            throw new Exception();
        }

        JsonArray jsonArray = element.getAsJsonArray();
        map.put(BaseArguments.event_section, jsonArray.toString());
        final MultipartBody.Part[] multiSinglePosterGalleryPart = new MultipartBody.Part[1];
        MultipartBody.Part[] multiPreviousGalleryPart = new MultipartBody.Part[previousMediaGallery.size()];
        MultipartBody.Part[] multiCurrentGalleryPart = new MultipartBody.Part[currentMediaGallery.size()];
        if (!TextUtils.isEmpty(uriOneImg.getPath())) {
            int i = 0;
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), uriOneImg);
            String mimeType = fileInformation.getMimeType();
            File file = new File(uriOneImg.getPath());
            RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_IMAGE_PNG, file);
            multiSinglePosterGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.image,
                    uriOneImg.getPath(), media);
        }

        for (int i = 0; i < previousMediaGallery.size(); i++) {
            String url = previousMediaGallery.get(0);
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
            String mimeType = fileInformation.getMimeType();

            if (video_mime_List.contains(mimeType)) {
                File file = new File(previousMediaGallery.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                multiPreviousGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.previous_media_gallery + "[" + i + "]", file.getName(), media);

            } else {
                File file = new File(previousMediaGallery.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_IMAGE_PNG, file);
                multiPreviousGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.previous_media_gallery + "[" + i + "]", file.getName(), media);

            }
        }
        for (int i = 0; i < currentMediaGallery.size(); i++) {
            String url = currentMediaGallery.get(0);
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
            String mimeType = fileInformation.getMimeType();
            if (video_mime_List.contains(mimeType)) {
                File file = new File(currentMediaGallery.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.current_media_gallery + "[" + i + "]", file.getName(), media);

            } else {
                File file = new File(currentMediaGallery.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_IMAGE_PNG, file);
                multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.current_media_gallery + "[" + i + "]", file.getName(), media);
            }
        }

        restClient.callback(this).postAddEvent(getContext(), RetrofitUtils.createMultipartRequest(map), multiPreviousGalleryPart, multiCurrentGalleryPart, multiSinglePosterGalleryPart);
    }

    @Override
    public void onFeatureClick(CarmenFeature feature) {
        Position position = feature.asPosition();
        latLng = new LatLng(position.getLatitude(), position.getLongitude());
        address = geocoderAutoCompleteView.getText().toString().trim();
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().zoomMap(latLng);
            getDashboardActivity().getMapHandler().getMapBoxFragment().addMarker(getContext(), R.mipmap.location_dark_gray, latLng,
                    Constants.You, address);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        geocoderAutoCompleteView.setText(address);

    }


    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {

        return false;
    }

    @Override
    public void onAddClick(String type) {
        if (type.equalsIgnoreCase("previous_media")) {
            multiplePreviousMediaSelector();

        } else if (type.equalsIgnoreCase("current_media")) {
            multipleCurrentMediaSelector();
        }
    }

    @Override
    public void onDeleteClick(int position, String type) {

        if (type.equalsIgnoreCase("previous_media")) {
            for (int i = 0; i < previousMediaGallery.size(); i++) {
                if (previousMediaGallery.get(position).equalsIgnoreCase(previousMediaGallery.get(i))) {
                    previousMediaGallery.remove(i);
                    refreshGalleryAdapter(type);
                }
            }
        } else {
            for (int i = 0; i < currentMediaGallery.size(); i++) {
                if (currentMediaGallery.get(position).equalsIgnoreCase(currentMediaGallery.get(i))) {
                    currentMediaGallery.remove(i);
                    refreshGalleryAdapter(type);
                }
            }
        }
    }

    private void refreshGalleryAdapter(String type) {
        if (type.equalsIgnoreCase("previous_media")) {
            if (previousMediaGallery != null) {
                previousGalleryAdapter.notifyDataSetChanged();
            }
        } else {
            if (currentMediaGallery != null) {
                currentGalleryAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void userLocationChange(Location location) {
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    private void setCameraPosition(LatLng location) {
        if (mapboxMap != null) {
            if (pointMarker != null) {
                mapboxMap.removeMarker(pointMarker);
            }
            pointMarker = mapboxMap.addMarker(new MarkerOptions().setPosition(location));
            mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 10));
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        AddLocationFragment mapViewFragment = new AddLocationFragment();
        mapViewFragment.setAddLocationListenerType(eventMapListener, Constants.ADDEVENTS, new LatLng(Double.valueOf(latitude), Double.valueOf(longitude)));
        ((AppBaseActivity) getActivity()).addFragment(mapViewFragment, true);
        return true;
    }

    @Override
    public void onSave(String type) {
        TransactionPrice price = AppPreferences.getTransactionPrice();
        if (type.equalsIgnoreCase("event_coverage")) {

            switch (eventCoverage){
                case "Photo":

                    openMakePayemntScreen("add_event", "event_coverage", 1000);

                    break;
                case "Video":
                    openMakePayemntScreen("add_event", "event_coverage", 1000);

                    break;
                default:
                    openMakePayemntScreen("add_event", "event_coverage", 1900);

                    break;
            }

        } else if (type.equalsIgnoreCase("paid_event")) {
            openMakePayemntScreen("add_event", "paid_event", price.getEvent().getPaid_event());
        }

    }

    @Override
    public void onCancel(String type) {
        if (type.equalsIgnoreCase("event_coverage")) {

            eventCoverage =null;

            radioGroupEventCoverage.clearCheck();



//            eventCoverageSwitch.setChecked(false);
        } else if (type.equalsIgnoreCase("paid_event")) {
            eventPaidEventSwitch.setChecked(false);
        }
    }
}
