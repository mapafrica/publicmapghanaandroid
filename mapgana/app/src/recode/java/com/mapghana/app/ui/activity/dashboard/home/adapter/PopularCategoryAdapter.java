package com.mapghana.app.ui.activity.dashboard.home.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.PopularCategory;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

import java.util.List;

public class PopularCategoryAdapter extends BaseRecycleAdapter {

    List<PopularCategory.PopulatCategoryModel> list;
    AdapterClickListener adapterClickListener;

    public PopularCategoryAdapter(AdapterClickListener adapterClickListener, List<PopularCategory.PopulatCategoryModel> list) {
        this.adapterClickListener = adapterClickListener;
        this.list = list;
    }


    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_popular_category;
    }

    @Override
    protected int getItemSize() {
        if (list != null && list.size() > 0) {
            return list.size();
        }
        return 0;
    }

    @Override
    protected BaseRecycleAdapter.ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private AppCompatTextView tv_popular;
        private LinearLayout ll_popular;
        private ImageView iv_category;
        private int pos;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_popular = itemView.findViewById(R.id.tv_popular);
            ll_popular = itemView.findViewById(R.id.ll_popular);
            iv_category = itemView.findViewById(R.id.iv_category);
        }

        @Override
        public void setData(int position) {
            pos = position;
            ll_popular.setOnClickListener(this);
            tv_popular.setText(list.get(position).getName());
            String image = list.get(position).getImage();
            if (image != null && !image.equals("")) {

//
                Glide.with(getContext()).load(list.get(position).getImage()).override(250, 250)
                        .placeholder(R.drawable.pleasewait)
                        .dontAnimate()
                        .into(iv_category);


            } else {
                iv_category.setImageResource(R.mipmap.no_category);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ll_popular:
                    adapterClickListener.onAdapterClickListener(pos);
                    break;
            }
        }
    }
}
