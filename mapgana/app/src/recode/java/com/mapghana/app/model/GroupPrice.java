package com.mapghana.app.model;

public class GroupPrice {

    private int add_group_charges;
    private int password_protect;

    public int getAdd_group_charges() {
        return add_group_charges;
    }

    public void setAdd_group_charges(int add_group_charges) {
        this.add_group_charges = add_group_charges;
    }

    public int getPassword_protect() {
        return password_protect;
    }

    public void setPassword_protect(int password_protect) {
        this.password_protect = password_protect;
    }
}
