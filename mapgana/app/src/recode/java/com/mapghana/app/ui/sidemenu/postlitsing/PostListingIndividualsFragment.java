package com.mapghana.app.ui.sidemenu.postlitsing;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Circle;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.plugins.annotation.OnCircleDragListener;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.utils.ColorUtils;
import com.mapghana.R;
import com.mapghana.app.adapter.AddEventGalleryAdapter;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Category;
import com.mapghana.app.model.City;
import com.mapghana.app.model.CompressionModel;
import com.mapghana.app.model.CreateIndividualPost;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.model.Login;
import com.mapghana.app.model.MapGhanaId;
import com.mapghana.app.model.PaymentResponse;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.TransactionPrice;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.sidemenu.Event.DialogCallback;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CategoryAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CityAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.GalleryAdapter;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Utility;
import com.mapghana.util.Valiations;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.mapghana.videocompression.VideoCompress;
import com.onegravity.contactpicker.contact.Contact;
import com.onegravity.contactpicker.contact.ContactDescription;
import com.onegravity.contactpicker.contact.ContactSortOrder;
import com.onegravity.contactpicker.core.ContactPickerActivity;
import com.onegravity.contactpicker.picture.ContactPictureType;
import com.theartofdev.edmodo.cropper.CropImage;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostListingIndividualsFragment extends AppBaseFragment implements CustomDatePickerDialog.OnDateListener, OnMapReadyCallback, PermissionsListener, LocationServiceListner, DialogCallback {

    private Spinner spCategory, spGender, spCity;
    private RestClient restClient;
    private EditText etName, etEmail, etTwitter, etWebsite, etWhatsApp,
            etNote, etTag, etMobile, etAddress, etMapghanaId, etInstagram, etFacebook, etYoutubeVimeo, etSoundCloud, etBlog, etGaming, etPasswordProtected;
    private ImageButton imgPickContact, imgSingleBrowse;
    private ImageView imageViewAddWebsite, imageViewSingle, imageViewSingleBrowse, imageViewAddGallery, imageViewAddFeature, imageViewAddGaming, imageViewAddBlog, imgAddPhone;
    private AppCompatTextView tvDob, tvSubmit, tvGallery, tv_unverify_status, tvHints;
    private RadioGroup radioGroupLocationCategory;
    private Location destinationLatLng;
    private Point desitnationPosition;
    private Spinner etServices;
    private ImageView imgSingle, imgAddServices;
    private Uri uriOneImg;
    private String DOB;
    private ArrayList<City.DataBean> cityList;
    private ArrayList<Category.DataBean> categoryList;
    private LinearLayout ll_addServiceView, ll_addPhoneView;
    private List<EditText> phoneTextList;
    private List<String> serviceTextList;
    private final int REQUEST_CONTACT = 111;
    private Date date;
    private RecyclerView rv_gallery;
    private ImageView imageAddEmailView;
    private GalleryAdapter galleryAdapter;
    private static final int REQUEST_CODE_CHOOSE = 888;
    private List<String> mediaList;
    private List<String> video_mime_List;
    private final long VIDEO_MAXDURATION = 30000;
    private Spinner sp_status;
    private EventInfoSection eventInfoSection;
    private CategoryAdapter categoryAdapter;
    private CityAdapter cityAdapter;
    private SimpleSpinnerAdapter status_adapter;
    double latitude;
    private MapView mapView;
    private SimpleSpinnerAdapter spinnerFeatureAdapter;
    private MapboxMap map;
    private PermissionsManager permissionsManager;
    public LocationComponent locationComponent;
    double longitude;
    private DirectionsRoute currentRoute;
    private LatLng latLng;
    private static final String TAG = "postIndividual";
    private PopupMenu popupMenu;
    private List<String> mEmailList;
    private List<String> mFeatureList;
    private List<String> mGamingList;
    private List<String> mBlogList;
    private SimpleSpinnerAdapter adapter, featureAdapter;
    private List<CompressionModel> videoLists = new ArrayList<>();
    private String phoneNumberVisibility, availablity, locationProfile, profileVisibility, passwordProtection, optimisation;
    private SwitchCompat switchCompatPhoneNumberVisibility, switchCompatAvailability, switchCompatProfileVisibility, switchCompatPasswordProtection;
    private LinearLayout linearLayoutEmailView, linearLayoutFeatureView, linearLayoutMapghanaIdContainer, linearLayoutOptimisationView, linearLayoutAdvancedOptimisation, linearLayoutAddWebsiteView, linearLayoutAddBlogView, linearLayoutAddGamingView;
    private AddEventGalleryAdapter currentGalleryAdapter;
    private List<String> listWebsiteItem;
    private TextInputLayout passwordProtect;
    private Point originPoint;
    private Double generalLatitude, generalLongitude;
    private AppCompatRadioButton radioButtonGeneral, radioButtonSpecific;
    private SymbolManager symbolManager;
    private CircleManager circleManager;
    private TextWatcher watcher;
    private String email, website;
    private ImageView checkMapghanaId;
    private LinearLayout llMapghanaIDAvailable;
    private AppCompatTextView txtMapGhanaId;
    private RelativeLayout rlAdvanced, rlOptimization;
    private LinearLayout llAdvancedContent;
    private ImageView icAdvanced, icOptimization;
    private boolean isAdvancedToggle, isOptimizationToggle;
    private ImageView QrImage;
    private String screenType, paymentType;
    private boolean isOptimization, isAdvanced;
    private SwitchCompat switchCompatAdvance, switchCompatOptimization;

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    private String address;

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static PostListingIndividualsFragment newInstance(String title, EventInfoSection eventInfoSection) {
        PostListingIndividualsFragment fragment = new PostListingIndividualsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.TITLE, title);
        args.putSerializable(Constants.ITEMS, eventInfoSection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_post_listing_individuals;
    }

    @Override
    public void initializeComponent() {
        restClient = new RestClient(getContext());
        init();
        initViewById(getView());
        initListener();
        initList();

        if (getArguments() != null) {
            eventInfoSection = (EventInfoSection) getArguments().getSerializable(Constants.ITEMS);
        }
        mapView = getView().findViewById(R.id.map_view);

        mapView.getMapAsync(this);
        int mNoOfColumns = Utility.calculateNoOfColumns(getContext());
        rv_gallery.setLayoutManager(new GridLayoutManager(getContext(), mNoOfColumns));

        date = new Date();
        adapter = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.gender_array));
        adapter.setSelGender(0);
        spGender.setAdapter(adapter);


        galleryAdapter = new GalleryAdapter(this, mediaList);
        rv_gallery.setAdapter(galleryAdapter);

//        setStatusSpinner();
        setupPopmenu();
        getCity();

        if (eventInfoSection != null && !TextUtils.isEmpty(eventInfoSection.getAddress())) {
            latitude = Double.valueOf(eventInfoSection.getLat());
            longitude = Double.valueOf(eventInfoSection.getLog());

        }
    }

    private void initViewById(View view) {

        spCategory = view.findViewById(R.id.spCategory);
        spGender = view.findViewById(R.id.spGender);
        spCity = view.findViewById(R.id.spCity);
        etName = view.findViewById(R.id.etName);
        etWhatsApp = view.findViewById(R.id.etWhatsApp);
        etEmail = view.findViewById(R.id.etEmail);
        etTwitter = view.findViewById(R.id.et_twitter);
        etWebsite = view.findViewById(R.id.etWebsite);
        etMobile = view.findViewById(R.id.etMobile);
        tvGallery = view.findViewById(R.id.tvGallery);
        etServices = view.findViewById(R.id.etServices);
        etNote = view.findViewById(R.id.etNote);
        etTag = view.findViewById(R.id.etTag);
        etAddress = view.findViewById(R.id.etAddress);
        imgPickContact = view.findViewById(R.id.imgPickContact);
        imgAddPhone = view.findViewById(R.id.imgAddPhone);
        imgSingleBrowse = view.findViewById(R.id.imgSingleBrowse);
//        tvDob = view.findViewById(R.id.tvDob);
        tvSubmit = view.findViewById(R.id.tvSubmit);
        imgSingle = view.findViewById(R.id.imgSingle);
        imgAddServices = view.findViewById(R.id.imgAddServices);
        ll_addServiceView = view.findViewById(R.id.ll_addServiceView);
        ll_addPhoneView = view.findViewById(R.id.ll_addPhoneView);
        rv_gallery = view.findViewById(R.id.rv_gallery);
//        sp_status = view.findViewById(R.id.sp_status);
//        tv_unverify_status = view.findViewById(R.id.tv_unverify_status);
        tvHints = view.findViewById(R.id.tvHints);
        passwordProtect = view.findViewById(R.id.text_input_layout_password);
        switchCompatAdvance = view.findViewById(R.id.switch_advanced);
        switchCompatOptimization = view.findViewById(R.id.switch_optimization);
        radioButtonGeneral = view.findViewById(R.id.radio_general);
        radioButtonSpecific = view.findViewById(R.id.radio_specific);

        radioGroupLocationCategory = view.findViewById(R.id.radio_group_location_category);
        switchCompatPhoneNumberVisibility = view.findViewById(R.id.phone_number_visibilty);
        switchCompatAvailability = view.findViewById(R.id.switch_availability);
        etMapghanaId = view.findViewById(R.id.etmapghana_id);
        etTwitter = view.findViewById(R.id.et_twitter);
        etInstagram = view.findViewById(R.id.et_instagram);
        etFacebook = view.findViewById(R.id.et_facebook);
        etYoutubeVimeo = view.findViewById(R.id.et_youtube_vimeo);
        etSoundCloud = view.findViewById(R.id.et_sound_cloud);
        etBlog = view.findViewById(R.id.et_blog);
        etGaming = view.findViewById(R.id.et_gaming);
        switchCompatProfileVisibility = view.findViewById(R.id.profile_visibility);
        switchCompatPasswordProtection = view.findViewById(R.id.password_protect);
        imageAddEmailView = view.findViewById(R.id.img_add_email);
        imageViewAddWebsite = view.findViewById(R.id.img_add_website);
        imageViewAddGallery = view.findViewById(R.id.imgGallery);
        etPasswordProtected = view.findViewById(R.id.et_password_protect);
        imageViewAddGaming = view.findViewById(R.id.img_add_gaming);
        imageViewAddBlog = view.findViewById(R.id.img_add_blog);

        linearLayoutEmailView = view.findViewById(R.id.ll_email_view);
        linearLayoutAddWebsiteView = view.findViewById(R.id.ll_add_website);
        linearLayoutAdvancedOptimisation = view.findViewById(R.id.ll_advanced_optimisation);
        linearLayoutAddBlogView = view.findViewById(R.id.ll_blog);
        linearLayoutAddGamingView = view.findViewById(R.id.ll_gaming);
        linearLayoutMapghanaIdContainer = view.findViewById(R.id.optimisation_mapghana);
        llMapghanaIDAvailable = view.findViewById(R.id.ll_id_availble_container);
        checkMapghanaId = view.findViewById(R.id.check_map_id);
        txtMapGhanaId = view.findViewById(R.id.tv_mapghana_id);

        rlAdvanced = view.findViewById(R.id.ll_advanced);
        llAdvancedContent = view.findViewById(R.id.ll_advanced_content);
        icAdvanced = view.findViewById(R.id.ic_advanced);

        icOptimization = view.findViewById(R.id.ic_optimization);
        rlOptimization = view.findViewById(R.id.rl_optimisation);

        QrImage = view.findViewById(R.id.image_qr);


        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(QrImage);
        Glide.with(this).load(R.drawable.image_qr).into(imageViewTarget);

        checkMapghanaId.setOnClickListener(v -> {
            String url = BaseArguments.MAPGHANA_ID_CHECK + "" + etMapghanaId.getText().toString().trim();
            restClient.callback(this).postCheckMapghanaId(url);
            displayProgressBar(false);
        });

        spinnerFeatureAdapter = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.feature));
        etServices.setAdapter(spinnerFeatureAdapter);

        etServices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerFeatureAdapter.setSelGender(position);
                serviceTextList.add(spinnerFeatureAdapter.getSelGender());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                paymentReceiver,
                new IntentFilter(Constants.LOCAL_BROADCAST_PAYMENT)
        );
    }

    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver paymentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            screenType = intent.getStringExtra("screen_type");
            paymentType = intent.getStringExtra("payment_type");
            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_SUCCESS)) {
                if("is_advanced".equalsIgnoreCase(paymentType) && "add_individual".equalsIgnoreCase(screenType)) {
                    isAdvanced = true;
                    llAdvancedContent.setVisibility(View.VISIBLE);
                    rlAdvanced.setEnabled(false);
                    rlAdvanced.setClickable(false);
                    switchCompatAdvance.setEnabled(false);




                }else if("is_optimization".equalsIgnoreCase(paymentType) && "add_individual".equalsIgnoreCase(screenType)) {
                    isOptimization = true;
                    linearLayoutMapghanaIdContainer.setVisibility(View.VISIBLE);
                    rlOptimization.setEnabled(false);
                    rlOptimization.setClickable(false);
                    switchCompatOptimization.setEnabled(false);

                }

                Gson gson = new Gson();
                PaymentResponse paymentResponse = gson.fromJson(intent.getStringExtra(Constants.data), PaymentResponse.class);
                callPaymentToServer(paymentResponse);

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_ERROR)) {
                if("is_advanced".equalsIgnoreCase(paymentType) && "add_individual".equalsIgnoreCase(screenType)) {
                    isAdvanced = false;
                    rlAdvanced.setEnabled(true);
                    rlAdvanced.setClickable(true);
                    switchCompatAdvance.setChecked(false);

                }else if("is_optimization".equalsIgnoreCase(paymentType) && "add_individual".equalsIgnoreCase(screenType)) {
                    isOptimization = false;
                    rlOptimization.setEnabled(true);
                    rlOptimization.setClickable(true);
                    switchCompatOptimization.setChecked(false);
                }

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_CANCELLED)) {
                if("is_advanced".equalsIgnoreCase(paymentType) && "add_individual".equalsIgnoreCase(screenType)) {
                    isAdvanced = false;
                    rlAdvanced.setEnabled(true);
                    rlAdvanced.setClickable(true);
                    switchCompatAdvance.setChecked(false);

                }else if("is_optimization".equalsIgnoreCase(paymentType) && "add_individual".equalsIgnoreCase(screenType)) {
                    isOptimization = false;
                    rlOptimization.setEnabled(true);
                    rlOptimization.setClickable(true);
                    switchCompatOptimization.setChecked(false);
                }
            }

        }
    };

    private void callPaymentToServer(PaymentResponse paymentResponse ) {
        Session session = AppPreferences.getSession();
        restClient.callback(this).transaction(String.valueOf(0),String.valueOf(paymentResponse.getData().getAmount()),paymentResponse.getData().getPaymentId(),session.getEmail(),session.getName(),session.getName(),screenType, paymentType);
    }

    private void openMakePayemntScreen(String screenType, String paymentType, int amount) {
        Intent intent = new Intent(getActivity(), MapghanaPayment.class);
        intent.putExtra("id", "0");
        intent.putExtra("screen_type", screenType);
        intent.putExtra("payment_type", paymentType);
        intent.putExtra("amount", amount);
        startActivity(intent);
    }
    private void initList() {
        cityList = new ArrayList<>();
        categoryList = new ArrayList<>();
        mediaList = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
        serviceTextList = new ArrayList<>();
        phoneTextList = new ArrayList<>();
        mEmailList = new ArrayList<>();
        mFeatureList = new ArrayList<>();
        listWebsiteItem = new ArrayList<>();
        mGamingList = new ArrayList<>();
        mBlogList = new ArrayList<>();

        Location location = Locationservice.sharedInstance().getLastLocation();
        if (location != null) {
            originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
            generalLatitude = location.getLatitude();
            generalLongitude = location.getLongitude();

        }
    }

    private String getLocationProfile(int radioId) {
        switch (radioId) {
            case R.id.radio_general:
                return "general";
            case R.id.radio_specific:
                return "specific";
        }
        return "";
    }

    private void createPolygon() {
        circleManager.create(new CircleOptions()
                .withLatLng(new LatLng(originPoint.latitude(), originPoint.longitude()))
                .withCircleColor(ColorUtils.colorToRgbaString(getActivity().getResources().getColor(R.color.colorYellowtransparent)))
                .withCircleRadius(50f)
                .withDraggable(true)

        );
    }

    private void toggleAdvanced() {
        if (isAdvancedToggle) {
            isAdvancedToggle = false;
            llAdvancedContent.setVisibility(View.GONE);
            icAdvanced.setImageResource(R.drawable.ic_arrow_down_black);
        } else {
            isAdvancedToggle = true;
            llAdvancedContent.setVisibility(View.VISIBLE);
            icAdvanced.setImageResource(R.drawable.ic_arrow_up);
        }
    }

    private void toggleOptimised() {
        if (isOptimizationToggle) {
            isOptimizationToggle = false;
            linearLayoutMapghanaIdContainer.setVisibility(View.GONE);
            icOptimization.setImageResource(R.drawable.ic_arrow_down_black);
        } else {
            isOptimizationToggle = true;
            linearLayoutMapghanaIdContainer.setVisibility(View.VISIBLE);
            icOptimization.setImageResource(R.drawable.ic_arrow_up);
        }
    }

    private void initListener() {


        radioGroupLocationCategory.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_general) {
                symbolManager.deleteAll();
                locationProfile = getLocationProfile(R.id.radio_general);
                createPolygon();
                map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originPoint.latitude(), originPoint.longitude()), 6));
            } else if (checkedId == R.id.radio_specific) {
                circleManager.deleteAll();
                locationProfile = getLocationProfile(R.id.radio_specific);
                createMarkerSymbol();
            } else {
                locationProfile = "";
            }
        });

        rlAdvanced.setOnClickListener(v -> {
            toggleAdvanced();
        });

        rlOptimization.setOnClickListener(v -> {
            toggleOptimised();
        });

        switchCompatOptimization.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                TransactionPrice price = AppPreferences.getTransactionPrice();
                Utils.openDialog((AppBaseActivity) getActivity(), "OPTIMIZATION", String.format(Locale.ENGLISH, getString(R.string.optimization_dialog_message), String.valueOf(price.getIndividual().getOptimization())), "is_optimization", this);
            }
        });

        switchCompatAdvance.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                TransactionPrice price = AppPreferences.getTransactionPrice();
                Utils.openDialog((AppBaseActivity) getActivity(), "ADVANCED", String.format(Locale.ENGLISH, getString(R.string.advanced_dialog_message), String.valueOf(price.getIndividual().getAdvance_feature())), "is_advanced", this);
            }
        });
        imageViewAddWebsite.setOnClickListener(this);
        imgPickContact.setOnClickListener(this);
        tvHints.setOnClickListener(this);
        imgSingleBrowse.setOnClickListener(this);
        imgAddServices.setOnClickListener(this);
//        tvDob.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        imgAddPhone.setOnClickListener(this);
        imageAddEmailView.setOnClickListener(this);
        imageViewAddGallery.setOnClickListener(this);
        imageViewAddGaming.setOnClickListener(this);
        imageViewAddBlog.setOnClickListener(this);


        switchCompatPhoneNumberVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                phoneNumberVisibility = "Y";
            } else {
                phoneNumberVisibility = "N";
            }
        });

        switchCompatAvailability.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                availablity = "Y";
            } else {
                availablity = "N";
            }
        });


        switchCompatProfileVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                profileVisibility = "Y";
            } else {
                profileVisibility = "N";
            }

        });

        switchCompatPasswordProtection.setOnCheckedChangeListener((compoundButton, b) -> {

            if (compoundButton.isChecked()) {
                passwordProtect.setVisibility(View.VISIBLE);
                passwordProtection = "Y";
            } else {
                passwordProtection = "N";
                passwordProtect.setVisibility(View.GONE);
            }
        });


        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (categoryAdapter != null && categoryList != null && categoryList.size() > 0)
                    categoryAdapter.setDataBean(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (cityAdapter != null && cityList != null && cityList.size() > 0)
                    cityAdapter.setDataBean(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelGender(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        sp_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (status_adapter != null) {
//                    status_adapter.setSelGender(position);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etSoundCloud.getText().hashCode() == s.hashCode() && etSoundCloud.getText().toString().contains("soundcloud")) {
                    etSoundCloud.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_soundcloud, 0);
                } else if (etYoutubeVimeo.getText().hashCode() == s.hashCode()) {
                    if (etYoutubeVimeo.getText().toString().contains("youtube")) {
                        etYoutubeVimeo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_youtube, 0);
                    } else if (etYoutubeVimeo.getText().toString().contains("vimeo")) {
                        etYoutubeVimeo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.vimeo_icon, 0);
                    }

                } else if (etFacebook.getText().hashCode() == s.hashCode() && etFacebook.getText().toString().contains("facebook")) {
                    etFacebook.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.facebook, 0);
                } else if (etInstagram.getText().hashCode() == s.hashCode() && etInstagram.getText().toString().contains("instagram")) {
                    etInstagram.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_insta, 0);
                } else if (etTwitter.getText().hashCode() == s.hashCode() && etTwitter.getText().toString().contains("@")) {
                    etTwitter.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_twitter, 0);
                } else if (etBlog.getText().hashCode() == s.hashCode() && etBlog.getText().toString().contains("blogspot")) {
                    etBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_blogger, 0);
                } else if (etBlog.getText().hashCode() == s.hashCode() && etBlog.getText().toString().contains("tumblr")) {
                    etBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tumblr, 0);
                } else if (etBlog.getText().hashCode() == s.hashCode() && etBlog.getText().toString().contains("wordpress")) {
                    etBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_wordpress, 0);
                } else if (etBlog.getText().hashCode() == s.hashCode() && etBlog.getText().toString().contains("medium")) {
                    etBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_medium, 0);
                }else if (etGaming.getText().hashCode() == s.hashCode() && etGaming.getText().toString().contains("steam")) {
                    etGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_steam, 0);
                } else if (etGaming.getText().hashCode() == s.hashCode() && etGaming.getText().toString().contains("psn")) {
                    etGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_playstation, 0);
                } else if (etGaming.getText().hashCode() == s.hashCode() && etGaming.getText().toString().contains("nin")) {
                    etGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_nintendo, 0);
                } else if (etGaming.getText().hashCode() == s.hashCode() && etGaming.getText().toString().contains("xbl")) {
                    etGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_xbox, 0);
                } else {
                    if (TextUtils.isEmpty(etSoundCloud.getText().toString())) {
                        etSoundCloud.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etYoutubeVimeo.getText().toString())) {
                        etYoutubeVimeo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etFacebook.getText().toString())) {
                        etFacebook.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etInstagram.getText().toString())) {
                        etInstagram.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etInstagram.getText().toString())) {
                        etTwitter.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };


        etSoundCloud.addTextChangedListener(watcher);
        etYoutubeVimeo.addTextChangedListener(watcher);
        etFacebook.addTextChangedListener(watcher);
        etInstagram.addTextChangedListener(watcher);
        etTwitter.addTextChangedListener(watcher);
        etBlog.addTextChangedListener(watcher);
        etGaming.addTextChangedListener(watcher);
    }

    private void setupPopmenu() {
        Context context = new ContextThemeWrapper(getContext(), R.style.CustomPopMenuTheme);
        popupMenu = new PopupMenu(context, imgAddServices);
        popupMenu.getMenuInflater().inflate(R.menu.menu_hints, popupMenu.getMenu());
    }

    private void createMarkerSymbol() {
        String markerOption = "Address: " + eventInfoSection.getAddress();
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.mapbox_marker_icon_default);
        map.getStyle().addImage("individual_marker", bm);
        symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(latitude, longitude))
                .withIconImage("individual_marker")
                //set the below attributes according to your requirements
                .withIconSize(0.5f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withZIndex(10)
                .withTextField(markerOption)
                .withTextHaloColor("rgba(255, 255, 255, 100)")
                .withTextHaloWidth(5.0f)
                .withTextAnchor("top")
                .withTextOffset(new Float[]{0f, 1.5f})
        );

    }

    private void setStatusSpinner() {

        String role = SessionManager.getRole(getContext());
        if (role != null && role.length() > 0 && role.equalsIgnoreCase("user")) {
            tv_unverify_status.setVisibility(View.VISIBLE);
            sp_status.setVisibility(View.GONE);
        } else {
            sp_status.setTag("admin");
            tv_unverify_status.setVisibility(View.GONE);
            sp_status.setVisibility(View.VISIBLE);
            String[] strings = getResources().getStringArray(R.array.status_array2);
            status_adapter = new SimpleSpinnerAdapter(getContext(), strings);
            status_adapter.setSelGender(0);
            sp_status.setAdapter(status_adapter);
        }

    }

    private String getStatus() {
//        String status;
//        int position = sp_status.getSelectedItemPosition();
//        if (position == 0) return null;
//        if (position == 1) {
//
//            status = "unverified";
//            return status;
//        } else if (position == 2) {
//            status = "verified";
//            return status;
//
//        }
        return "unverified";
    }

    private void addEmailSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView etEmail = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutEmailView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutEmailView.getChildCount());
        linearLayoutEmailView.addView(addView);
    }

    private void setEmailList() {

        mEmailList.clear();
        mEmailList.add(etEmail.getText().toString());
        int count = linearLayoutEmailView.getChildCount();
        if (linearLayoutEmailView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutEmailView.getChildAt(i - 1);
                EditText email = addView.findViewById(R.id.etEmail);
                String emailText = email.getText().toString();
                if (!TextUtils.isEmpty(emailText)) {
                    mEmailList.add(emailText);

                }

            }
        }
    }


    private void addWebsiteSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView etwebsite = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddWebsiteView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutAddWebsiteView.getChildCount());
        linearLayoutAddWebsiteView.addView(addView);
    }


    private void setWebsiteList() {

        listWebsiteItem.clear();
        if (!TextUtils.isEmpty(etWebsite.getText().toString())) {
            listWebsiteItem.add(etWebsite.getText().toString());
        }

        int count = linearLayoutAddWebsiteView.getChildCount();
        if (linearLayoutAddWebsiteView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddWebsiteView.getChildAt(i - 1);
                EditText websiteEditText = addView.findViewById(R.id.etEmail);
                String websiteText = websiteEditText.getText().toString();
                if (!TextUtils.isEmpty(websiteText)) {
                    listWebsiteItem.add(websiteText);
                }

            }
        }
    }

    private void addBlogSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        EditText etwebsite = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);



        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddBlogView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutAddBlogView.getChildCount());
        etwebsite.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ( etwebsite.getText().toString().contains("blogspot")) {
                    etwebsite.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_blogger, 0);
                } else if ( etwebsite.getText().toString().contains("tumblr")) {
                    etwebsite.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tumblr, 0);
                } else if ( etwebsite.getText().toString().contains("wordpress")) {
                    etwebsite.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_wordpress, 0);
                } else if ( etwebsite.getText().toString().contains("medium")) {
                    etwebsite.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_medium, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        linearLayoutAddBlogView.addView(addView);
    }


    private void setBlogSection() {

        mBlogList.clear();
        if (!TextUtils.isEmpty(etBlog.getText().toString())) {
            mBlogList.add(etBlog.getText().toString());
        }

        int count = linearLayoutAddBlogView.getChildCount();
        if (linearLayoutAddBlogView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddBlogView.getChildAt(i - 1);
                EditText blogEditText = addView.findViewById(R.id.etEmail);
                String blog = blogEditText.getText().toString();
                if (!TextUtils.isEmpty(blog)) {
                    mBlogList.add(blog);
                }

            }
        }
    }


    private void addGamingSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        EditText gaming = addView.findViewById(R.id.etEmail);
        gaming.setHint("gamenetwork = profileid eg psn = kinkelson");
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);
        gaming.addTextChangedListener(watcher);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddGamingView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutAddGamingView.getChildCount());


        gaming.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ( gaming.getText().toString().contains("steam")) {
                    gaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_steam, 0);
                } else if ( gaming.getText().toString().contains("psn")) {
                    gaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_playstation, 0);
                } else if ( gaming.getText().toString().contains("nin")) {
                    gaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_nintendo, 0);
                } else if ( gaming.getText().toString().contains("xbl")) {
                    gaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_xbox, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        linearLayoutAddGamingView.addView(addView);
    }


    private void setGamingSection() {

        mGamingList.clear();
        if (!TextUtils.isEmpty(etGaming.getText().toString())) {
            mGamingList.add(etGaming.getText().toString());
        }

        int count = linearLayoutAddGamingView.getChildCount();
        if (linearLayoutAddGamingView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddGamingView.getChildAt(i - 1);
                EditText gamingEditText = addView.findViewById(R.id.etEmail);
                String gaming = gamingEditText.getText().toString();
                if (!TextUtils.isEmpty(gaming)) {
                    mGamingList.add(gaming);
                }

            }
        }
    }

    private void getCity() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).city();
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private void getCategory() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).category(Constants.individuals);
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private String getGenderItem() {
        int position = spGender.getSelectedItemPosition();
        if (position == 0) return null;
        String[] array = getContext().getResources().getStringArray(R.array.gender_array);
        for (int i = 0; i < array.length; i++) {
            if (position == i) {
                return array[i];
            }
        }
        return null;
    }

    private String getItemCategory() {
        int position = spCategory.getSelectedItemPosition();
        if (position == 0) return null;
        for (int i = 0; i < categoryList.size(); i++) {
            if (position == i) {
                return String.valueOf(categoryList.get(position).getId());
            }
        }
        return null;
    }

    private String getItemCity() {
        int position = spCity.getSelectedItemPosition();
        //  if (position == 0) return null;
        for (int i = 0; i < cityList.size(); i++) {
            if (position == i) {
                return String.valueOf(cityList.get(position).getId());
            }
        }
        return null;
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgPickContact:
                hideKeyboard();
                pickContacts();
                break;
            case R.id.imgAddPhone:
                addPhoneView();
                break;
            case R.id.imgSingleBrowse:
                hideKeyboard();
                oneImageSelector();
                break;
            case R.id.imgGallery:
                hideKeyboard();
                multipleMediaSelector();
                break;
            case R.id.tvSubmit:
                hideKeyboard();
                onSubmit();
                break;
//            case R.id.tvDob:
//                hideKeyboard();
//                onDobDialog();
//                break;
            case R.id.imgAddServices:
                addServicesView();
                break;
            case R.id.tvHints:
                showHintPopmenu();
                break;
            case R.id.img_add_website:
                addWebsiteSection();
                break;
            case R.id.img_add_email:
                addEmailSection();
                break;
            case R.id.img_add_blog:
                addBlogSection();
                break;
            case R.id.img_add_gaming:
                addGamingSection();
                break;

        }
    }

    private void showHintPopmenu() {

        /*popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });*/
        popupMenu.show();
    }

    private String getServices() {
        String services = "";
        if (serviceTextList != null && serviceTextList.size() > 0) {
            for (int i = 0; i < serviceTextList.size(); i++) {
                if (serviceTextList.get(i).trim().length() != 0) {
                    services = "\"" + serviceTextList.get(i).trim() + "\", " + services;
                }
            }
        }
        return services;

    }


    private String getContacts() {
        String services = "";
        if (phoneTextList != null && phoneTextList.size() > 0) {
            for (int i = 0; i < phoneTextList.size(); i++) {
                String phoneNumber = phoneTextList.get(i).getText().toString().trim();
                if (phoneNumber.length() < 7 || phoneNumber.length() > 15) {
                    return null;
                }
                services = "\"" + phoneNumber + "\", " + services;
            }
        }
        return services;
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        if (dialog != null) {
            dialog.create(getContext(), true).callback(this).show();
        }
    }

    private void pickContacts() {
        Intent intent = new Intent(getContext(), ContactPickerActivity.class)
                // .putExtra(ContactPickerActivity.EXTRA_THEME, mDarkTheme ? R.style.Theme_Dark : R.style.Theme_Light)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_BADGE_TYPE, ContactPictureType.ROUND.name())
                .putExtra(ContactPickerActivity.EXTRA_SHOW_CHECK_ALL, true)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION, ContactDescription.ADDRESS.name())
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_SORT_ORDER, ContactSortOrder.AUTOMATIC.name());
        startActivityForResult(intent, REQUEST_CONTACT);
    }

    private void oneImageSelector() {
        CropImage.activity()
                .start(getContext(), this);
    }


    private void multipleMediaSelector() {
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CONTACT && resultCode == Activity.RESULT_OK &&
                data != null && data.hasExtra(ContactPickerActivity.RESULT_CONTACT_DATA)) {
            List<String> selPhoneList = new ArrayList<>();
            List<Contact> contacts = (List<Contact>) data.getSerializableExtra(ContactPickerActivity.RESULT_CONTACT_DATA);
            for (Contact contact : contacts) {
                contact.getPhone(0);
                if (contact.getPhone(0) != null && contact.getPhone(0).trim().length() != 0) {

                    selPhoneList.add(contact.getPhone(0));
                } else if (contact.getPhone(1) != null && contact.getPhone(1).trim().length() != 0) {

                    selPhoneList.add(contact.getPhone(1));
                } else if (contact.getPhone(2) != null && contact.getPhone(2).trim().length() != 0) {
                    selPhoneList.add(contact.getPhone(2));
                } else {
                    displayToast("no contact found");
                }
            }

            if (contacts != null && contacts.size() > 0) {
                etMobile.setText(selPhoneList.get(0));
                addPhoneView(selPhoneList);
            }
        }


        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            List<Uri> uris = Matisse.obtainResult(data);
            List<String> paths = Matisse.obtainPathResult(data);
            FilePathHelper filePathHelper = new FilePathHelper();
            if (paths != null && paths.size() > 0) {
                mediaList.addAll(paths);
                refreshGalleryAdapter();
                if (isVideoCompressingNeeded()) {
                    compressVideo(videoLists);
                }
            }
        }

        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        uriOneImg = result.getUri();
                        imgSingle.setImageURI(uriOneImg);
                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                        displayErrorDialog("Error", error.getMessage());
                    }
                }
                break;
        }
    }

    private void refreshGalleryAdapter() {
        if (mediaList != null) {
            galleryAdapter.notifyDataSetChanged();
            tvGallery.setText("" + mediaList.size() + " items are selected.. ");
        }
    }

    @Override
    public void onAdapterClickListener(int position) {
        for (int i = 0; i < mediaList.size(); i++) {
            if (mediaList.get(position).equalsIgnoreCase(mediaList.get(i))) {
                mediaList.remove(i);
                refreshGalleryAdapter();
            }
        }
    }

    private long getDuration(String path) {
        File videoFile = new File(path);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        //use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(getContext(), Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);

        retriever.release();
        return timeInMillisec;

    }

    private void sub() {
        if (mediaList != null && mediaList.size() > 0) {

            videoLists.clear();
            String path = "";
            CompressionModel compressionModel;

            for (int i = 0; i < mediaList.size(); i++) {
                path = mediaList.get(i);
                FilePathHelper filePathHelper = new FilePathHelper();
                final String mimeType = filePathHelper.getMimeType(path);
                if (video_mime_List.contains(mimeType)) {
                    compressionModel = new CompressionModel();
                    compressionModel.setSourcePath(path);
                    compressionModel.setId(i);
                    videoLists.add(compressionModel);
                }
            }
            if (videoLists.size() == 0) {
                //api hit
            } else {
                // compressVideo(videoLists);
            }


        }
    }

    private boolean isValidate() {


        if (Valiations.hasText(etName)
                && Valiations.hasText(etAddress)
                && Valiations.hasText(etNote) && Valiations.hasText(etTag)) {


            String name = etName.getText().toString().trim();
            if (name.length() <= 3 || name.length() >= 30) {
                displayToast("Name should contain 3 to 30 letters");
                return false;
            }

            String email = etEmail.getText().toString().trim();
            if (!email.isEmpty()) {
                if (isEmailValid(email)) {
                    email = etEmail.getText().toString().trim();
                } else {
                    displayToast("Invalid email address");
                    return false;
                }
            }
            if (!etMobile.getText().toString().trim().matches("") || phoneTextList.size() > 0) {
                String mobile = etMobile.getText().toString().trim();
                if (mobile.length() == 0) {
                    displayToast("Add mobile number");
                    return false;
                }

                if (mobile.length() < 7 || mobile.length() > 15) {
                    displayToast("Mobile number should contain 7 to 15 digits");
                    return false;
                }
                if (getContacts() == null) {
                    displayToast("Mobile number should contain 7 to 15 digits");
                    return false;
                }

            }

            if (etMobile.getText().toString().trim().length() == 0) {
                displayToast("Add mobile number");
                return false;
            }


//            if (tvDob.getText().toString().trim().equals(getResources().getString(R.string.Date_of_birth))) {
//                displayToast("Select Date Of Birth");
//                return false;
//            }
//            if (date != null
//                    && !dobdateValidate(date)) {
//                displayToast("Minimum 12 year age is allowed");
//                return false;
//            }
            if (getGenderItem() == null) {
                displayToast("Select Gender");
                return false;
            }

            if (getItemCategory() == null) {
                displayToast("Select Occupation");
                return false;
            }

            String city = getItemCity();
            if (city == null) {
                displayToast("Select Region");
                return false;
            }


            if (uriOneImg == null) {
                displayToast("Select Profile Pic");
                return false;
            }


            if (etTwitter.getText().toString().trim().length() > 0) {
                if (!etTwitter.getText().toString().trim().startsWith("@")) {
                    displayToast("Please enter valid twitter name");
                    return false;
                }

            }
        } else {
            displayToast("Please fill all fields.");

            return false;
        }

        return true;
    }


    private void onSubmit() {
        if (!isValidate()) return;
        if (ConnectionDetector.isNetAvail(getContext())) {
            submitToServer();

        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private void submitToServer() {

        setEmailList();
        setWebsiteList();
        setBlogSection();
        setGamingSection();

        JsonArray emailArray = null;
        JsonArray websiteArray = null;
        JsonArray blogArray = null;
        JsonArray gamingArray = null;
        Gson gson = new Gson();
        int ID = 0;
        String userInfo = SessionManager.getUserInfoResponse(getContext());
        if (!userInfo.equals("")) {
            Login login = gson.fromJson(userInfo, Login.class);
            ID = login.getData().getId();
        }
        String website = "", twitter = "", services = "";


        JsonElement elementEmail = gson.toJsonTree(mEmailList, new TypeToken<List<String>>() {
        }.getType());

        if (!elementEmail.isJsonArray()) {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        emailArray = elementEmail.getAsJsonArray();

        JsonElement elementWebsite = gson.toJsonTree(listWebsiteItem, new TypeToken<List<String>>() {
        }.getType());

        if (!elementWebsite.isJsonArray()) {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        websiteArray = elementWebsite.getAsJsonArray();

//        JsonArray array = null;
//        if (!etServices.getText().toString().trim().matches("") || serviceTextList.size() > 0) {
//            services = getServices() + "\"" + etServices.getText().toString().trim();
//            services = "[" + services + "\"]";
//            JsonParser jsonParser = new JsonParser();
//            JsonElement jsonElement = jsonParser.parse(services);
//            array = jsonElement.getAsJsonArray();
//            gson.toJson(jsonElement);
//        }
        HashMap<String, String> map = new HashMap<>();
//        if (tv_unverify_status.getVisibility() == View.VISIBLE) {
//            String status = tv_unverify_status.getText().toString().trim();
//            map.put(BaseArguments.status, status);
//        }
//        if (tv_unverify_status.getVisibility() != View.VISIBLE &&
//                getStatus() == null) {
//            displayToast("Select Status");
//            return;
//        } else {
//            if (tv_unverify_status.getVisibility() != View.VISIBLE)
//                map.put(BaseArguments.status, getStatus());
//        }


        map.put(BaseArguments.status, getStatus());
        if (mediaList.size() <= 0) {
            displayToast("Select Gallery Images");
            return;
        }
        String city = getItemCity();
        String name = etName.getText().toString().trim();
        String phone = "";
        phone = getContacts() + "\"" + etMobile.getText().toString().trim();
        phone = "[" + phone + "\"]";
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(phone);
        JsonArray phoneArray = null;
        phoneArray = jsonElement.getAsJsonArray();
        map.put(BaseArguments.user_id, String.valueOf(ID));
        map.put(BaseArguments.post_for, Constants.individuals);
        map.put(BaseArguments.email, TextUtils.join(", ", mEmailList));
        map.put(BaseArguments.website, TextUtils.join(", ", listWebsiteItem));
//        map.put(BaseArguments.dob, DOB);
        map.put(BaseArguments.gender, getGenderItem().toLowerCase());
        map.put(BaseArguments.category_id, getItemCategory());
        map.put(BaseArguments.city_id, city);
        map.put(BaseArguments.notes, etNote.getText().toString().trim());
        map.put(BaseArguments.tags, etTag.getText().toString().trim());

        if (serviceTextList.size() > 0) {
            map.put(BaseArguments.features, TextUtils.join(",", serviceTextList));
        } else {
            map.put(BaseArguments.features, "");
        }
        if ("general".equalsIgnoreCase(locationProfile)) {
            map.put(BaseArguments.lat, String.valueOf(generalLatitude));
            map.put(BaseArguments.log, String.valueOf(generalLongitude));
        } else {
            map.put(BaseArguments.lat, String.valueOf(latitude));
            map.put(BaseArguments.log, String.valueOf(longitude));
        }

        map.put(BaseArguments.name, name);
        map.put(BaseArguments.whatsApp, etWhatsApp.getText().toString().trim());
        map.put(BaseArguments.address, etAddress.getText().toString().trim());
        map.put(BaseArguments.location_profile, locationProfile);
        map.put(BaseArguments.phone_visibility, phoneNumberVisibility);
        map.put(BaseArguments.availability, availablity);
        map.put(BaseArguments.optimization, optimisation);
        map.put(BaseArguments.mapghanaid, txtMapGhanaId.getText().toString());
        map.put(BaseArguments.twitter, etTwitter.getText().toString());
        map.put(BaseArguments.instagram, etInstagram.getText().toString());
        map.put(BaseArguments.facebook, etFacebook.getText().toString());
        map.put(BaseArguments.youtube, etYoutubeVimeo.getText().toString());
        map.put(BaseArguments.soundclud, etSoundCloud.getText().toString());
        JsonElement elementblog = gson.toJsonTree(mBlogList, new TypeToken<List<String>>() {
        }.getType());

        if (!elementblog.isJsonArray()) {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        blogArray = elementblog.getAsJsonArray();
        map.put(BaseArguments.blog, blogArray.toString());

        JsonElement elementGaming = gson.toJsonTree(mGamingList, new TypeToken<List<String>>() {
        }.getType());

        if (!elementGaming.isJsonArray()) {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        gamingArray = elementGaming.getAsJsonArray();
        map.put(BaseArguments.gaming, gamingArray.toString());

        map.put(BaseArguments.profile_visibility, profileVisibility);
        map.put(BaseArguments.password_protected, passwordProtection);
        map.put(BaseArguments.password, etPasswordProtected.getText().toString());

        if (phoneArray != null) {
            map.put(BaseArguments.phone, phoneArray.toString());
        } else {
            map.put(BaseArguments.phone, "");
        }
        MultipartBody.Part signleImgPart = RetrofitUtils.
                createFilePart(BaseArguments.image, uriOneImg.getPath(),
                        RetrofitUtils.MEDIA_TYPE_IMAGE_PNG);

        displayProgressBar(false);
        MultipartBody.Part[] multiImgPart = new MultipartBody.Part[mediaList.size()];
        for (int i = 0; i < mediaList.size(); i++) {
            String url = mediaList.get(i);
            FilePathHelper filePathHelper = new FilePathHelper();
            FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
            String mimeType = fileInformation.getMimeType();
            if (video_mime_List.contains(mimeType)) {
                File file = new File(mediaList.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                multiImgPart[i] = MultipartBody.Part.createFormData(BaseArguments.gallery_images + "[" + i + "]",
                        file.getName(), media);
            } else {
                File file = new File(mediaList.get(i));
                RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_IMAGE_PNG, file);
                multiImgPart[i] = MultipartBody.Part.createFormData(BaseArguments.gallery_images + "[" + i + "]", file.getName(), media);
            }
        }
        if (multiImgPart.length == 0) {
            restClient.post_individual(getContext(), RetrofitUtils.createMultipartRequest(map), signleImgPart);
        } else {
            restClient.post_individual(getContext(), RetrofitUtils.createMultipartRequest(map), signleImgPart, multiImgPart);
        }

    }

    private boolean isVideoCompressingNeeded() {

        videoLists.clear();
        String path = "";
        CompressionModel compressionModel;

        for (int i = 0; i < mediaList.size(); i++) {
            path = mediaList.get(i);
            FilePathHelper filePathHelper = new FilePathHelper();
            final String mimeType = filePathHelper.getMimeType(path);
            if (video_mime_List.contains(mimeType)) {
                compressionModel = new CompressionModel();
                compressionModel.setSourcePath(path);
                compressionModel.setId(i);

                videoLists.add(compressionModel);
            }
        }

        return videoLists.size() > 0;
    }

    private void compressVideo(List<CompressionModel> filePath) {

        String outputDir = Environment.getExternalStorageDirectory() + "/MapGhanaVideo";
        File file = new File(outputDir);

        if (!file.exists()) {
            file.mkdirs();
        }


        if (filePath != null && filePath.size() > 0) {
            displayProgressBar(false, "Video is compressing...");
            for (int i = 0; i < filePath.size(); i++) {
                File file1 = new File(filePath.get(i).getSourcePath());
                String destPath = outputDir + File.separator + "MapGhana_VID_IND_" + i +
                        System.currentTimeMillis() + ".mp4";

                final CompressionModel compressionModel = filePath.get(i);
                compressionModel.setDestinationPath(destPath);
                compressionModel.setCompressionStatus(0);
                VideoCompress.compressVideoHigh(compressionModel.getSourcePath(),
                        compressionModel.getDestinationPath(), compressionModel.getId(),
                        new VideoCompress.CompressListener() {
                            @Override
                            public void onStart(long mTask_id) {
                            }

                            @Override
                            public void onSuccess(long mTask_id, String source, String destination) {
                                compressionModel.setCompressionStatus(1);
                                checkVideoCompressingComplete();

                            }

                            @Override
                            public void onFail(long mTask_id, String source, String destination) {
                                compressionModel.setCompressionStatus(-1);
                                checkVideoCompressingComplete();
                            }

                            @Override
                            public void onProgress(long mTask_id, float percent) {
                            }
                        });
            }
        }

    }

    private void checkVideoCompressingComplete() {
        boolean isCompressingComplete = true;
        for (CompressionModel model :
                videoLists) {
            if (model.getCompressionStatus() == 0) {
                isCompressingComplete = false;
                break;
            }

        }
        if (isCompressingComplete) {
            dismissProgressBar();
        }
        if (isCompressingComplete && checkAllVideoCompressingSuccess()) {
            for (CompressionModel model : videoLists) {
                mediaList.set((int) model.getId(), model.getDestinationPath());
            }

        }
    }

    private boolean checkAllVideoCompressingSuccess() {

        boolean allCompressingSucces = true;
        for (CompressionModel model : videoLists) {
            if (model.getCompressionStatus() == -1) {
                allCompressingSucces = false;
                break;

            }
        }
        return allCompressingSucces;
    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            this.date = date;
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            DOB = format.format(date);
            tvDob.setText(DOB);
        }
    }

    public static boolean dobdateValidate(Date date) {


        try {
            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.YEAR, -12);
            if (date.before(c2.getTime())) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_city) {
                dismissProgressBar();
                try {

                    String s = response.body().string();
                    Gson gson = new Gson();
                    City city = gson.fromJson(s, City.class);
                    if (city.getStatus() != 0) {
                        City.DataBean city1 = new City.DataBean();
                       /* city1.setName("Select City");
                        cityList.add(0, city1);
       */
                        cityList.addAll(city.getData());
                        cityAdapter = new CityAdapter(getContext(), cityList);
                        cityAdapter.setDataBean(0);
                        spCity.setSelection(0);
                        spCity.setAdapter(cityAdapter);
                        getCategory();

                    } else {
                        displayErrorDialog("Error", city.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_CATEGORY) {
                dismissProgressBar();
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Category category = gson.fromJson(s, Category.class);
                    if (category.getStatus() != 0) {
                        Category.DataBean bean = new Category.DataBean();
                        bean.setName("Select Occupation");
                        categoryList.add(bean);
                        categoryList.addAll(category.getData());
                        categoryAdapter = new CategoryAdapter(getContext(), categoryList);
                        categoryAdapter.setDataBean(0);
                        spCategory.setAdapter(categoryAdapter);
                    } else {
                        displayErrorDialog("Error", category.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_PostINDIVIDUAL) {
                dismissProgressBar();
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    CreateIndividualPost posting = gson.fromJson(s, CreateIndividualPost.class);
                    if (posting.getStatus() != 0) {
                        displayToast(posting.getMessage());
                        try {
                            getDashboardActivity().clearFragmentBackStack();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else {
                        displayErrorDialog("Error", posting.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_MAPGHANA_ID) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    MapGhanaId posting = gson.fromJson(s, MapGhanaId.class);
                    if (posting.getStatus() != 0) {
                        dismissProgressBar();
                        llMapghanaIDAvailable.setVisibility(View.VISIBLE);
                        txtMapGhanaId.setVisibility(View.VISIBLE);
                        txtMapGhanaId.setText(posting.getData());
                    } else {
                        llMapghanaIDAvailable.setVisibility(View.GONE);
                        displayErrorDialog("Error", posting.getMessage());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }

            }
        } else {
            dismissProgressBar();
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    private void addServicesView() {
        final View serviceView = LayoutInflater.from(getContext()).inflate(R.layout.item_add_services, null, false);

        final Spinner spinnerText = serviceView.findViewById(R.id.etServices2);
        ImageView imgServices = serviceView.findViewById(R.id.imgRemove);


        SimpleSpinnerAdapter adapter = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.feature));
        spinnerText.setAdapter(adapter);

        spinnerText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelGender(position);
                serviceTextList.add(adapter.getSelGender());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imgServices.setOnClickListener(v -> {
            ((LinearLayout) serviceView.getParent()).removeView(serviceView);
            serviceTextList.remove(serviceView.getParent());
        });


        serviceView.setTag(ll_addServiceView.getChildCount() - 1);
        ll_addServiceView.addView(serviceView, ll_addServiceView.getChildCount());
    }


//    private void removeServiceView(View serviceView) {
//        ll_addServiceView.removeView(serviceView);
//        onrefresh();
//    }
//
//    private void refreshEditTextList(TypefaceEditText editText) {
//        for (int i = 0; i < serviceTextList.size(); i++) {
//            if (editText == serviceTextList.get(i)) {
//                serviceTextList.remove(i);
//            }
//        }
//    }

    private void onrefresh() {
        for (int i = 0; i < ll_addServiceView.getChildCount(); i++) {
            View view = ll_addServiceView.getChildAt(i);
            TypefaceEditText editText = view.findViewById(R.id.etServices2);
            ImageView imgServices = view.findViewById(R.id.imgRemove);
            imgServices.setTag(i);
        }
    }

    private void addPhoneView() {
        final View phoneView = LayoutInflater.from(getContext()).inflate(R.layout.item_phone_view, null, false);

        final AppCompatEditText editText = phoneView.findViewById(R.id.etServices2);
        ImageView img = phoneView.findViewById(R.id.imgRemove);
        //  editText.setText("done");
        img.setOnClickListener(v -> {
            removePhoneView(phoneView, editText);
            refreshPhoneTextList(editText);

        });
        phoneTextList.add(editText);
        phoneView.setTag(ll_addPhoneView.getChildCount() - 1);
        ll_addPhoneView.addView(phoneView, ll_addPhoneView.getChildCount());
    }

    private void addPhoneView(List<String> contactList) {
        for (int i = contactList.size() - 1; i > 0; i--) {
            final View phoneView = LayoutInflater.from(getContext()).inflate(R.layout.item_phone_view, null, false);
            final AppCompatEditText editText = phoneView.findViewById(R.id.etServices2);
            ImageView img = (ImageView) phoneView.findViewById(R.id.imgRemove);
            editText.setText(contactList.get(i));
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removePhoneView(phoneView, editText);
                    refreshPhoneTextList(editText);

                }
            });
            phoneTextList.add(editText);
            phoneView.setTag(ll_addPhoneView.getChildCount() - 1);
            ll_addPhoneView.addView(phoneView, ll_addPhoneView.getChildCount());
        }
    }


    private void removePhoneView(View serviceView, EditText editText) {
        ll_addPhoneView.removeView(serviceView);
        phoneTextList.remove(editText);
        onrefreshPhoneList();
    }

    private void refreshPhoneTextList(AppCompatEditText editText) {
        for (int i = 0; i < phoneTextList.size(); i++) {
            if (editText == phoneTextList.get(i)) {
                phoneTextList.remove(i);
            }
        }
    }

    private void onrefreshPhoneList() {
        for (int i = 0; i < ll_addPhoneView.getChildCount(); i++) {
            View view = ll_addPhoneView.getChildAt(i);
            AppCompatEditText editText = view.findViewById(R.id.etServices2);
            ImageView imgServices = (ImageView) view.findViewById(R.id.imgRemove);
            imgServices.setTag(i);
        }
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.map = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            enableLocationComponent(style);
            symbolManager = new SymbolManager(mapView, map, style);
            circleManager = new CircleManager(mapView, map, style);
            symbolManager.setIconAllowOverlap(false);  //your choice t/f
            symbolManager.setTextAllowOverlap(false);  //your choice t/f

            Location location = Locationservice.sharedInstance().getLastLocation();
            if (location != null) {
                Point originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());

            }

            circleManager.addDragListener(new OnCircleDragListener() {
                @Override
                public void onAnnotationDragStarted(Circle annotation) {

                }

                @Override
                public void onAnnotationDrag(Circle annotation) {
                    generalLatitude = annotation.getLatLng().getLatitude();
                    generalLongitude = annotation.getLatLng().getLongitude();
                }

                @Override
                public void onAnnotationDragFinished(Circle annotation) {

                }
            });

            radioButtonSpecific.setChecked(true);
            getLocationProfile(R.id.radio_specific);
            createMarkerSymbol();
            map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 6));
        });

    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = map.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(map.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        etSoundCloud.removeTextChangedListener(watcher);
        etYoutubeVimeo.removeTextChangedListener(watcher);
        etFacebook.removeTextChangedListener(watcher);
        etInstagram.removeTextChangedListener(watcher);
        etTwitter.removeTextChangedListener(watcher);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(paymentReceiver);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void userLocationChange(Location location) {
        Toast.makeText(getActivity(), "" + location.getLatitude(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    @Override
    public void onSave(String type) {
        TransactionPrice price = AppPreferences.getTransactionPrice();
        if (type.equalsIgnoreCase("is_advanced")) {
            openMakePayemntScreen("add_individual", "is_advanced",price.getIndividual().getAdvance_feature());
        } else if (type.equalsIgnoreCase("is_optimization")) {
            openMakePayemntScreen("add_individual", "is_optimization",price.getIndividual().getOptimization());
        }
    }

    @Override
    public void onCancel(String type) {
        if (type.equalsIgnoreCase("is_advanced")) {
            switchCompatAdvance.setChecked(false);
        } else if (type.equalsIgnoreCase("is_optimization")) {
            switchCompatOptimization.setChecked(false);
        }
    }
}
