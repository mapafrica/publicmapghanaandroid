package com.mapghana.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;

import java.util.ArrayList;

public class EventSectionListAdapter extends RecyclerView.Adapter<EventSectionListAdapter.SingleItemRowHolder> {

    private ArrayList<EventDetail> itemsList;
    private EventListingFragment mListener;

    public EventSectionListAdapter(EventListingFragment listener, ArrayList<EventDetail> itemsList) {
        this.itemsList = itemsList;
        this.mListener = listener;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event_list_view, null);
        return new SingleItemRowHolder(v);

    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {
        holder.setPosition(i);
        EventDetail singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getName());
        if("Y".equalsIgnoreCase(singleItem.getMapgh_ticket_hosting())) {
            holder.hostingIcon.setVisibility(View.VISIBLE);
        }else{
            holder.hostingIcon.setVisibility(View.INVISIBLE);
        }

        if("Y".equalsIgnoreCase(singleItem.getPaid_event())) {
            holder.ticketIcon.setVisibility(View.VISIBLE);
        }else{
            holder.ticketIcon.setVisibility(View.INVISIBLE);
        }
        Glide.clear(holder.itemImage);
        Glide.with(MapGhanaApplication.sharedInstance())
                .load(singleItem.getImage())
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                            isFirstResource) {
                        holder.progress_bar.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                            target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progress_bar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.itemImage);
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected ImageView itemImage;
        protected ImageView ticketIcon;
        protected ImageView hostingIcon;
        private LottieAnimationView progress_bar;
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = view.findViewById(R.id.event_title);
            this.itemImage = view.findViewById(R.id.event_image);
            this.progress_bar = view.findViewById(R.id.progress_bar);
            this.ticketIcon = view.findViewById(R.id.event_price_icon);
            this.hostingIcon = view.findViewById(R.id.event_hosting_icon);

            view.setOnClickListener(v -> {
               mListener.onClickEvent(itemsList.get(position));
            });

        }
    }

}