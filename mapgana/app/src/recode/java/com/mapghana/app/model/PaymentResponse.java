package com.mapghana.app.model;

import java.io.Serializable;

public class PaymentResponse implements Serializable {

    private String status;
    private String message;
    private PaymentData data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public PaymentData getData() {
        return data;
    }
}
