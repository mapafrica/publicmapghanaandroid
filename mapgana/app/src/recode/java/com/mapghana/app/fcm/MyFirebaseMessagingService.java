package com.mapghana.app.fcm;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mapghana.R;
import com.mapghana.app.utils.Constants;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
 private static final String TAG = "FCM Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

     Map<String, String> messageType = remoteMessage.getData();

     if (!remoteMessage.getData().isEmpty()) {

         sendNotification(remoteMessage);

//         if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//             // TODO: Show proper message
//         } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
//             // TODO: Show proper message
//         } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//             try {
//                 sendNotification(remoteMessage);
//             } catch (Exception ex) {
//                 Log.d("Exception", "Exception in Notification");
//             }
//         }
     }

    }

    private void sendNotification(RemoteMessage remoteMessage) {

        System.out.println("Extras::: " + remoteMessage.getData().toString());

        String key = remoteMessage.getData().get(Constants.NOTIFICATION_KEY);
        if (key != null) {

            int notificationKey = Integer.parseInt(key);
            String notificationContent =remoteMessage.getData().get(Constants.NOTIFICATION_TITLE);

            Intent redirectIntent = new Intent();
            redirectIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            switch (notificationKey) {


            }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, notificationKey, redirectIntent, PendingIntent
                    .FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

            // Initialize Notification Builder and set pending intent to it.
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(getResources().getString(R.string.app_name))
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationContent))
                            .setAutoCancel(true)
                            .setContentText(notificationContent)
                            .setDefaults(Notification.DEFAULT_ALL); // For notification sound, vibration and light

            notificationBuilder.setContentIntent(pendingIntent);
            // Initialize Notification Manager and notify user
            NotificationManager notificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = getString(R.string.default_notification_channel_id);
                NotificationChannel channel = new NotificationChannel(channelId, "global" , NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder.setChannelId(channelId);
            }
            notificationManager.notify(notificationKey, notificationBuilder.build());

        }
    }
}



