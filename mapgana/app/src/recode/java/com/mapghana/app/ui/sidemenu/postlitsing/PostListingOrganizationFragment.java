package com.mapghana.app.ui.sidemenu.postlitsing;


import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brsoftech.customtimepicker.TimePickerDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.flutterwave.raveandroid.RaveConstants;
import com.flutterwave.raveandroid.RavePayActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Circle;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.plugins.annotation.OnCircleDragListener;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.utils.ColorUtils;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapghana.R;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Category;
import com.mapghana.app.model.City;
import com.mapghana.app.model.CompressionModel;
import com.mapghana.app.model.CreateOrganizationPost;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.model.Login;
import com.mapghana.app.model.MapGhanaId;
import com.mapghana.app.model.PaymentResponse;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.TransactionPrice;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.sidemenu.Event.DialogCallback;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CategoryAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CityAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.GalleryAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.PostingAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.SubCategoryAdapter;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Utility;
import com.mapghana.util.Valiations;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.mapghana.videocompression.VideoCompress;
import com.onegravity.contactpicker.contact.Contact;
import com.onegravity.contactpicker.contact.ContactDescription;
import com.onegravity.contactpicker.contact.ContactSortOrder;
import com.onegravity.contactpicker.core.ContactPickerActivity;
import com.onegravity.contactpicker.picture.ContactPictureType;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostListingOrganizationFragment extends AppBaseFragment
        implements TimePickerDialog.TimePickerListner, OnMapReadyCallback, PermissionsListener, LocationServiceListner , DialogCallback {

    private MapView mapView;
    private MapboxMap map;
    private Point originPoint;
    private Location destinationLatLng;
    private Point desitnationPosition;
    private DirectionsRoute currentRoute;
    private static final String TAG = "postOrganisation";
    private PermissionsManager permissionsManager;
    public LocationComponent locationComponent;
    private PostingAdapter adapter;
    private Spinner spCategory, spCity, spSubCategory;
    private List<Category.DataBean> list;
    private RestClient restClient;
    private EditText etName, etEmail, etTwitter, etWebsite,
            etNote, etTag, etMobile, etAddress, etType, etMapghanaId, etInstagram, etFacebook, etYoutubeVimeo, etSoundCloud, etBlog, etGaming, etPasswordProtected;
    private ImageView imageViewAddWebsite, imageViewSingle, imageViewSingleBrowse, imageViewAddGallery, imageViewAddFeature, imageAddEmailView, imgAddPhone, imgAddServices;
    private AppCompatTextView tvSubmit, tvGallery;
    private Spinner etServices;
    private EventInfoSection eventInfoSection;
    private ArrayList<City.DataBean> cityList;
    private ArrayList<Category.DataBean> categoryList;
    private ArrayList<Category.DataBean.SubCategoryBean> subCategoryList;
    private ImageButton imgPickContact, imgBrowse;
    private AppCompatCheckBox chkSun, chkSat, chkMF;
    private AppCompatTextView tvStartMF, tvEndMF, tvStartSat, tvHints, tvEndSat, tvStartSun, tvEndSun, tv_unverify_status;
    private int time_type;//tvStartMF=1, tvEndMF=2, tvStartSat=3, tvEndSat=4, tvStartSun=5, tvEndSun=6
    private TimePickerDialog timePickerDialog;
    private TextWatcher watcher;
    private LinearLayout ll_addServiceView, ll_addPhoneView;
    private List<EditText> phoneTextList;
    private List<String> serviceTextList;
    private final int REQUEST_CONTACT = 111;
    private RecyclerView rv_gallery;
    private GalleryAdapter galleryAdapter;
    private static final int REQUEST_CODE_CHOOSE = 888;
    private List<String> mediaList;
    private List<String> video_mime_List;
    private final long VIDEO_MAXDURATION = 30000;
    private Spinner sp_status;
    private LinearLayout ll_sub_category;
    private CategoryAdapter categoryAdapter;
    private CityAdapter cityAdapter;
    private SimpleSpinnerAdapter status_adapter;
    private SubCategoryAdapter subCategoryAdapter;
    private List<CompressionModel> videoLists = new ArrayList<>();
    private double latitude;
    private double longitude;
    private PopupMenu popupMenu;
    private List<String> mEmailList;
    private List<String> mFeatureList;
    private List<String> mGamingList;
    private SimpleSpinnerAdapter spinnerFeatureAdapter;
    private List<String> mBlogList;
    private NavigationMapRoute navigationMapRoute;
    private List<String> listWebsiteItem;
    private String phoneNumberVisibility, availablity, locationProfile, profileVisibility, passwordProtection, optimisation;
    private SwitchCompat switchCompatPhoneNumberVisibility, switchCompatAvailability, switchCompatOptimisation, switchCompatProfileVisibility, switchCompatPasswordProtection;
    private LinearLayout linearLayoutEmailView, linearLayoutFeatureView, linearLayoutMapghanaIdContainer, linearLayoutAddPhoneView, linearLayoutOptimisationView, linearLayoutAdvancedOptimisation, linearLayoutAddWebsiteView, linearLayoutAddBlogView, linearLayoutAddGamingView;
    private TextInputLayout passwordProtect;
    private RadioGroup radioGroupLocationCategory;
    private String address;
    private AppCompatRadioButton radioButtonGeneral, radioButtonSpecific;
    private SymbolManager symbolManager;
    private CircleManager circleManager;
    private Double generalLatitude, generalLongitude;
    private ImageView checkMapghanaId;
    private LinearLayout llMapghanaIDAvailable;
    private AppCompatTextView txtMapGhanaId;
    private RelativeLayout rlAdvanced, rlOptimization;
    private LinearLayout llAdvancedContent;
    private ImageView icAdvanced, icOptimization;
    private boolean isAdvancedToggle, isOptimizationToggle;
    private String screenType, paymentType;
    private boolean isOptimization, isAdvanced;
    private ImageView QrImage;
    private SwitchCompat switchCompatAdvance, switchCompatOptimization;

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static PostListingOrganizationFragment newInstance(String title, EventInfoSection eventInfoSection) {
        PostListingOrganizationFragment fragment = new PostListingOrganizationFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putSerializable("item", eventInfoSection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
    }




    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_post_listing_organization;
    }

    @Override
    public void initializeComponent() {
        restClient = new RestClient(getContext());
        init();
        initViewById(getView());
        initListener();
        initList();

        int mNoOfColumns = Utility.calculateNoOfColumns(getContext());
        rv_gallery.setLayoutManager(new GridLayoutManager(getContext(), mNoOfColumns));

//        setStatusSpinner();
        timePickerDialog = new TimePickerDialog(getContext(), Calendar.getInstance(), this);

        galleryAdapter = new GalleryAdapter(this, mediaList);
        rv_gallery.setAdapter(galleryAdapter);

        ll_sub_category.setVisibility(View.GONE);
        setupPopmenu();
        getCity();

        if (getArguments() != null) {
            eventInfoSection = (EventInfoSection) getArguments().getSerializable("item");
            latitude = Double.valueOf(eventInfoSection.getLat());
            longitude = Double.valueOf(eventInfoSection.getLog());
        }
    }


    private void initViewById(View view) {
        mapView = getView().findViewById(R.id.map_view);
        mapView.getMapAsync(this);
        spCategory = view.findViewById(R.id.spCategory);
        spSubCategory = view.findViewById(R.id.spSubCategory);
        spCity = view.findViewById(R.id.spCity);
        etName = view.findViewById(R.id.etName);
        etEmail = view.findViewById(R.id.etEmail);
        etWebsite = view.findViewById(R.id.etWebsite);
        etMobile = view.findViewById(R.id.etMobile);
        tvGallery = view.findViewById(R.id.tvGallery);
        etServices = view.findViewById(R.id.etServices);
        etType = view.findViewById(R.id.etType);
        etNote = view.findViewById(R.id.etNote);
        etTag = view.findViewById(R.id.etTag);
        etTwitter = view.findViewById(R.id.et_twitter);
        etAddress = view.findViewById(R.id.etAddress);
        imgAddPhone = view.findViewById(R.id.imgAddPhone);
        tvSubmit = view.findViewById(R.id.tvSubmit);
        tvStartMF = view.findViewById(R.id.tvStartMF);
        tvEndMF = view.findViewById(R.id.tvEndMF);
        tvStartSat = view.findViewById(R.id.tvStartSat);
        tvEndSat = view.findViewById(R.id.tvEndSat);
        tvStartSun = view.findViewById(R.id.tvStartSun);
//        tv_unverify_status = view.findViewById(R.id.tv_unverify_status);
        tvEndSun = view.findViewById(R.id.tvEndSun);
        chkSun = view.findViewById(R.id.chkSun);
        chkSat = view.findViewById(R.id.chkSat);
        chkMF = view.findViewById(R.id.chkMF);
        ll_addServiceView = view.findViewById(R.id.ll_addServiceView);
        imgAddServices = view.findViewById(R.id.imgAddServices);
        imgPickContact = view.findViewById(R.id.imgPickContact);
//        sp_status = view.findViewById(R.id.sp_status);
        tvHints = view.findViewById(R.id.tvHints);
        ll_addPhoneView = view.findViewById(R.id.ll_addPhoneView);
        ll_sub_category = view.findViewById(R.id.ll_sub_category);
        rv_gallery = view.findViewById(R.id.rv_gallery);
        passwordProtect = view.findViewById(R.id.text_input_layout_password);
        radioGroupLocationCategory = view.findViewById(R.id.radio_group_location_category);
        switchCompatPhoneNumberVisibility = view.findViewById(R.id.phone_number_visibilty);
        switchCompatAvailability = view.findViewById(R.id.switch_availability);
        etMapghanaId = view.findViewById(R.id.etmapghana_id);
        etTwitter = view.findViewById(R.id.et_twitter);
        etInstagram = view.findViewById(R.id.et_instagram);
        etFacebook = view.findViewById(R.id.et_facebook);
        etYoutubeVimeo = view.findViewById(R.id.et_youtube_vimeo);
        etSoundCloud = view.findViewById(R.id.et_sound_cloud);
        etBlog = view.findViewById(R.id.et_blog);
        etGaming = view.findViewById(R.id.et_gaming);
        switchCompatProfileVisibility = view.findViewById(R.id.profile_visibility);
        switchCompatPasswordProtection = view.findViewById(R.id.password_protect);
        imageAddEmailView = view.findViewById(R.id.img_add_email);
        imageViewAddWebsite = view.findViewById(R.id.img_add_website);
        imageViewAddGallery = view.findViewById(R.id.imgGallery);
        etPasswordProtected = view.findViewById(R.id.et_password_protect);
        switchCompatAdvance = view.findViewById(R.id.switch_advanced);
        switchCompatOptimization = view.findViewById(R.id.switch_optimization);
        radioButtonGeneral = view.findViewById(R.id.radio_general);
        radioButtonSpecific = view.findViewById(R.id.radio_specific);
        linearLayoutEmailView = view.findViewById(R.id.ll_email_view);
        linearLayoutAddWebsiteView = view.findViewById(R.id.ll_add_website);
        linearLayoutAdvancedOptimisation = view.findViewById(R.id.ll_advanced_optimisation);
        linearLayoutAddBlogView = view.findViewById(R.id.ll_blog);
        linearLayoutAddGamingView = view.findViewById(R.id.ll_gaming);
        llMapghanaIDAvailable = view.findViewById(R.id.ll_id_availble_container);
        linearLayoutMapghanaIdContainer = view.findViewById(R.id.optimisation_mapghana);

        checkMapghanaId = view.findViewById(R.id.check_map_id);
        txtMapGhanaId = view.findViewById(R.id.tv_mapghana_id);

        rlAdvanced = view.findViewById(R.id.ll_advanced);
        llAdvancedContent = view.findViewById(R.id.ll_advanced_content);
        icAdvanced = view.findViewById(R.id.ic_advanced);
        QrImage = view.findViewById(R.id.image_qr);
        icOptimization = view.findViewById(R.id.ic_optimization);
        rlOptimization = view.findViewById(R.id.rl_optimisation);

        checkMapghanaId.setOnClickListener(v -> {
            String url = BaseArguments.MAPGHANA_ID_CHECK + "" + etMapghanaId.getText().toString().trim();
            restClient.callback(this).postCheckMapghanaId(url);
            displayProgressBar(false);
        });
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(QrImage);
        Glide.with(this).load(R.drawable.image_qr).into(imageViewTarget);

// Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                paymentReceiver,
                new IntentFilter(Constants.LOCAL_BROADCAST_PAYMENT)
        );
    }

    private void initList() {
        list = new ArrayList<>();
        cityList = new ArrayList<>();
        categoryList = new ArrayList<>();
        subCategoryList = new ArrayList<>();
        mediaList = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
        serviceTextList = new ArrayList<>();
        phoneTextList = new ArrayList<>();
        mEmailList = new ArrayList<>();
        mFeatureList = new ArrayList<>();
        listWebsiteItem = new ArrayList<>();
        mGamingList = new ArrayList<>();
        mBlogList = new ArrayList<>();

        Location location = Locationservice.sharedInstance().getLastLocation();
        if (location != null) {
            originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
            generalLatitude = location.getLatitude();
            generalLongitude = location.getLongitude();

        }
    }

    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver paymentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            screenType = intent.getStringExtra("screen_type");
            paymentType = intent.getStringExtra("payment_type");
            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_SUCCESS)) {
                if("is_advanced".equalsIgnoreCase(paymentType) && "add_organisation".equalsIgnoreCase(screenType)) {
                    isAdvanced = true;
                    rlAdvanced.setEnabled(false);
                    rlAdvanced.setClickable(false);
                    isAdvancedToggle = true;
                    llAdvancedContent.setVisibility(View.VISIBLE);
                    icAdvanced.setImageResource(R.drawable.ic_arrow_up);
                    switchCompatAdvance.setEnabled(false);


                }else if("is_optimization".equalsIgnoreCase(paymentType) && "add_organisation".equalsIgnoreCase(screenType)) {
                    isOptimization = true;
                    rlOptimization.setEnabled(false);
                    rlOptimization.setClickable(false);
                    isOptimizationToggle = true;
                    linearLayoutMapghanaIdContainer.setVisibility(View.VISIBLE);
                    icOptimization.setImageResource(R.drawable.ic_arrow_up);
                    switchCompatOptimization.setEnabled(false);
                }

                Gson gson = new Gson();
                PaymentResponse paymentResponse = gson.fromJson(intent.getStringExtra(Constants.data), PaymentResponse.class);
                callPaymentToServer(paymentResponse);

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_ERROR)) {
                if("is_advanced".equalsIgnoreCase(paymentType) && "add_organisation".equalsIgnoreCase(screenType)) {
                    isAdvanced = false;
                    rlAdvanced.setEnabled(true);
                    rlAdvanced.setClickable(true);
                    switchCompatAdvance.setChecked(false);
                }else if("is_optimization".equalsIgnoreCase(paymentType) && "add_organisation".equalsIgnoreCase(screenType)) {
                    isOptimization = false;
                    rlOptimization.setEnabled(true);
                    rlOptimization.setClickable(true);
                    switchCompatOptimization.setChecked(false);
                }

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_CANCELLED)) {
                if("is_advanced".equalsIgnoreCase(paymentType) && "add_organisation".equalsIgnoreCase(screenType)) {
                    isAdvanced = false;
                    rlAdvanced.setEnabled(true);
                    rlAdvanced.setClickable(true);
                    switchCompatAdvance.setChecked(false);
                }else if("is_optimization".equalsIgnoreCase(paymentType) && "add_organisation".equalsIgnoreCase(screenType)) {
                    isOptimization = false;
                    rlOptimization.setEnabled(true);
                    rlOptimization.setClickable(true);
                    switchCompatOptimization.setChecked(false);

                }
            }

        }
    };

    private void toggleAdvanced() {
        if (isAdvancedToggle) {
            isAdvancedToggle = false;
            llAdvancedContent.setVisibility(View.GONE);
            icAdvanced.setImageResource(R.drawable.ic_arrow_down_black);
        } else {
            isAdvancedToggle = true;
            llAdvancedContent.setVisibility(View.VISIBLE);
            icAdvanced.setImageResource(R.drawable.ic_arrow_up);
        }
    }

    private void toggleOptimised() {
        if (isOptimizationToggle) {
            isOptimizationToggle = false;
            linearLayoutMapghanaIdContainer.setVisibility(View.GONE);
            icOptimization.setImageResource(R.drawable.ic_arrow_down_black);
        } else {
            isOptimizationToggle = true;
            linearLayoutMapghanaIdContainer.setVisibility(View.VISIBLE);
            icOptimization.setImageResource(R.drawable.ic_arrow_up);
        }
    }


    private void callPaymentToServer(PaymentResponse paymentResponse ) {
        Session session = AppPreferences.getSession();
        restClient.callback(this).transaction(String.valueOf(0),String.valueOf(paymentResponse.getData().getAmount()),paymentResponse.getData().getPaymentId(),session.getEmail(),session.getName(),session.getName(),screenType, paymentType);
    }

    private void openMakePayemntScreen(String screenType, String paymentType, int amount) {
        Intent intent = new Intent(getActivity(), MapghanaPayment.class);
        intent.putExtra("id", "0");
        intent.putExtra("screen_type", screenType);
        intent.putExtra("payment_type", paymentType);
        intent.putExtra("amount", amount);
        startActivity(intent);
    }

    private void initListener() {


        imageViewAddWebsite.setOnClickListener(this);
        imgAddServices.setOnClickListener(this);
        imgAddPhone.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        tvStartMF.setOnClickListener(this);
        tvStartSat.setOnClickListener(this);
        tvEndSat.setOnClickListener(this);
        tvStartSun.setOnClickListener(this);
        tvEndSun.setOnClickListener(this);
        tvEndMF.setOnClickListener(this);
        imgPickContact.setOnClickListener(this);
        tvHints.setOnClickListener(this);
        imageAddEmailView.setOnClickListener(this);
        imageViewAddGallery.setOnClickListener(this);

        rlAdvanced.setOnClickListener(v -> {
            toggleAdvanced();
        });

        rlOptimization.setOnClickListener(v -> {
            toggleOptimised();
        });

        radioGroupLocationCategory.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_general) {
                symbolManager.deleteAll();
                locationProfile = getLocationProfile(R.id.radio_general);
                createPolygon();
                map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originPoint.latitude(), originPoint.longitude()), 6));
            } else if (checkedId == R.id.radio_specific) {
                circleManager.deleteAll();
                locationProfile = getLocationProfile(R.id.radio_specific);
                createMarkerSymbol();
            } else {
                locationProfile = "";
            }
        });

        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (categoryAdapter != null && categoryList != null && categoryList.size() > 0)
                    categoryAdapter.setDataBean(position);
                if (position == 0) {
                    ll_sub_category.setVisibility(View.GONE);
                    return;
                }
                if (categoryList != null && categoryList.size() > 0) {
                    if (categoryList.get(position).getSub_category() != null &&
                            categoryList.get(position).getSub_category().size() > 0) {
                        setSubCategory();
                    } else {
                        clearSubCategory();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (cityAdapter != null && cityList != null && cityList.size() > 0)
                    cityAdapter.setDataBean(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        sp_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (status_adapter != null) {
//                    status_adapter.setSelGender(position);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        spSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (subCategoryAdapter != null && subCategoryList != null && subCategoryList.size() > 0)
                    subCategoryAdapter.setSubCategoryBean(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        switchCompatPhoneNumberVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                phoneNumberVisibility = "Y";
            } else {
                phoneNumberVisibility = "N";
            }
        });

        switchCompatAvailability.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                availablity = "Y";
            } else {
                availablity = "N";
            }
        });


        switchCompatProfileVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                profileVisibility = "Y";
            } else {
                profileVisibility = "N";
            }

        });

        switchCompatPasswordProtection.setOnCheckedChangeListener((compoundButton, b) -> {

            if (compoundButton.isChecked()) {
                passwordProtect.setVisibility(View.VISIBLE);
                passwordProtection = "Y";
            } else {
                passwordProtection = "N";
                passwordProtect.setVisibility(View.GONE);
            }
        });

        switchCompatOptimization.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                TransactionPrice price = AppPreferences.getTransactionPrice();
                Utils.openDialog((AppBaseActivity) getActivity(), "OPTIMIZATION", String.format(Locale.ENGLISH, getString(R.string.optimization_dialog_message), String.valueOf(price.getIndividual().getOptimization())), "is_optimization", this);
            }
        });

        switchCompatAdvance.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                TransactionPrice price = AppPreferences.getTransactionPrice();
                Utils.openDialog((AppBaseActivity) getActivity(), "ADVANCED", String.format(Locale.ENGLISH, getString(R.string.advanced_dialog_message), String.valueOf(price.getIndividual().getAdvance_feature())), "is_advanced", this);
            }
        });
        watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etSoundCloud.getText().hashCode() == s.hashCode() && etSoundCloud.getText().toString().contains("soundcloud")) {
                    etSoundCloud.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_soundcloud, 0);
                } else if (etYoutubeVimeo.getText().hashCode() == s.hashCode()) {
                    if (etYoutubeVimeo.getText().toString().contains("youtube")) {
                        etYoutubeVimeo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_youtube, 0);
                    } else if (etYoutubeVimeo.getText().toString().contains("vimeo")) {
                        etYoutubeVimeo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.vimeo_icon, 0);
                    }

                } else if (etFacebook.getText().hashCode() == s.hashCode() && etFacebook.getText().toString().contains("facebook")) {
                    etFacebook.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.facebook, 0);
                } else if (etInstagram.getText().hashCode() == s.hashCode() && etInstagram.getText().toString().contains("instagram")) {
                    etInstagram.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_insta, 0);
                } else if (etTwitter.getText().hashCode() == s.hashCode() && etTwitter.getText().toString().contains("@")) {
                    etTwitter.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_twitter, 0);
                } else {
                    if (TextUtils.isEmpty(etSoundCloud.getText().toString())) {
                        etSoundCloud.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etYoutubeVimeo.getText().toString())) {
                        etYoutubeVimeo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etFacebook.getText().toString())) {
                        etFacebook.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etInstagram.getText().toString())) {
                        etInstagram.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                    if (TextUtils.isEmpty(etInstagram.getText().toString())) {
                        etTwitter.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        etSoundCloud.addTextChangedListener(watcher);
        etYoutubeVimeo.addTextChangedListener(watcher);
        etFacebook.addTextChangedListener(watcher);
        etInstagram.addTextChangedListener(watcher);
        etTwitter.addTextChangedListener(watcher);


        spinnerFeatureAdapter = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.feature));
        etServices.setAdapter(spinnerFeatureAdapter);

        etServices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerFeatureAdapter.setSelGender(position);
                serviceTextList.add(spinnerFeatureAdapter.getSelGender());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void createMarkerSymbol() {

        String markerOption = "Address: " + eventInfoSection.getAddress();
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.mapbox_marker_icon_default);
        map.getStyle().addImage("individual_marker", bm);
        symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(latitude, longitude))
                .withIconImage("individual_marker")
                //set the below attributes according to your requirements
                .withIconSize(0.5f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withZIndex(10)
                .withTextField(markerOption)
                .withTextHaloColor("rgba(255, 255, 255, 100)")
                .withTextHaloWidth(5.0f)
                .withTextAnchor("top")
                .withTextOffset(new Float[]{0f, 1.5f})
        );

    }

    private String getLocationProfile(int radioId) {
        switch (radioId) {
            case R.id.radio_general:
                return "general";
            case R.id.radio_specific:
                return "specific";
        }
        return "";
    }

    private void createPolygon() {
        circleManager.create(new CircleOptions()
                .withLatLng(new LatLng(originPoint.latitude(), originPoint.longitude()))
                .withCircleColor(ColorUtils.colorToRgbaString(getActivity().getResources().getColor(R.color.colorYellowtransparent)))
                .withCircleRadius(50f)
                .withDraggable(true)

        );
    }

    private void setupPopmenu() {
        Context context = new ContextThemeWrapper(getContext(), R.style.CustomPopMenuTheme);
        popupMenu = new PopupMenu(context, imgAddServices);
        popupMenu.getMenuInflater().inflate(R.menu.menu_hints, popupMenu.getMenu());
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }


    private void setStatusSpinner() {

        String role = SessionManager.getRole(getContext());
        if (role != null && role.length() > 0 && role.equalsIgnoreCase("user")) {
            tv_unverify_status.setVisibility(View.VISIBLE);
            sp_status.setVisibility(View.GONE);
        } else {
            tv_unverify_status.setVisibility(View.GONE);
            sp_status.setVisibility(View.VISIBLE);

            String[] strings = getResources().getStringArray(R.array.status_array2);
            status_adapter = new SimpleSpinnerAdapter(getContext(), strings);
            status_adapter.setSelGender(0);
            sp_status.setAdapter(status_adapter);
        }

    }

    private String getStatus() {
//        String status;
//        int position = sp_status.getSelectedItemPosition();
//        if (position == 0) return null;
//        if (position == 1) {
//            status = "unverified";
//            return status;
//        } else if (position == 2) {
//            status = "verified";
//            return status;
//
//        }
        return "unverified";
    }

    private void addEmailSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView etEmail = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutEmailView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutEmailView.getChildCount());
        linearLayoutEmailView.addView(addView);
    }

    private void setEmailList() {

        mEmailList.clear();
        mEmailList.add(etEmail.getText().toString());
        int count = linearLayoutEmailView.getChildCount();
        if (linearLayoutEmailView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutEmailView.getChildAt(i - 1);
                EditText email = addView.findViewById(R.id.etEmail);
                String emailText = email.getText().toString();
                if (!TextUtils.isEmpty(emailText)) {
                    mEmailList.add(emailText);
                }

            }
        }
    }

    private void addWebsiteSection() {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        final View addView = layoutInflater.inflate(R.layout.layout_email_view, null);
        TextView etwebsite = addView.findViewById(R.id.etEmail);
        ImageView deleteIcon = addView.findViewById(R.id.img_delete);

        deleteIcon.setOnClickListener(v ->
        {
            if (linearLayoutAddWebsiteView.getChildCount() == 1) {
                deleteIcon.setVisibility(View.GONE);

            }
            ((LinearLayout) addView.getParent()).removeView(addView);
        });
        addView.setTag(linearLayoutAddWebsiteView.getChildCount());
        linearLayoutAddWebsiteView.addView(addView);
    }


    private void setWebsiteList() {

        listWebsiteItem.clear();
        if (!TextUtils.isEmpty(etWebsite.getText().toString())) {
            listWebsiteItem.add(etWebsite.getText().toString());
        } else {
            etWebsite.setError("Please add website");
            etWebsite.setFocusable(true);
            return;
        }

        int count = linearLayoutAddWebsiteView.getChildCount();
        if (linearLayoutAddWebsiteView.getChildCount() != 0) {

            for (int i = 1; i <= count; i++) {

                final View addView = linearLayoutAddWebsiteView.getChildAt(i - 1);
                EditText websiteEditText = addView.findViewById(R.id.etEmail);
                String websiteText = websiteEditText.getText().toString();
                if (!TextUtils.isEmpty(websiteText)) {
                    listWebsiteItem.add(websiteText);
                } else {
                    websiteEditText.setError("Please add website");
                    websiteEditText.setFocusable(true);
                    return;
                }

            }
        }
    }


    private void getCategory() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).category(Constants.organizations);
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private void getCity() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).city();
        } else {
            displayToast(Constants.No_Internet);
        }
    }


    private String getItemCategory() {
        int position = spCategory.getSelectedItemPosition();
        if (position == 0) return null;

        for (int i = 0; i < categoryList.size(); i++) {
            if (position == i) {
                // displayLog(TAG, "getItemCategory: "+ categoryList.get(position).getId());
                return String.valueOf(categoryList.get(position).getId());
            }
        }
        return null;
    }

    private String getItemSubCategory() {
        int position = spSubCategory.getSelectedItemPosition();
        if (position == 0) return null;

        for (int i = 0; i < subCategoryList.size(); i++) {
            if (position == i) {
                return String.valueOf(subCategoryList.get(position).getId());
            }
        }
        return null;
    }

    private String getItemCity() {
        int position = spCity.getSelectedItemPosition();
        // if (position == 0) return null;

        for (int i = 0; i < cityList.size(); i++) {
            if (position == i) {
                return String.valueOf(cityList.get(position).getId());
            }
        }
        return null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgPickContact:
                hideKeyboard();
                pickContacts();
                break;
            case R.id.imgAddPhone:
                addPhoneView();
                break;
            case R.id.tvSubmit:
                hideKeyboard();
                onSubmit();
                break;
            case R.id.imgGallery:
                hideKeyboard();
                multipleMediaSelector();
                break;
            case R.id.tvStartMF:
                hideKeyboard();
                displayTimePickerDialog(1);
                break;
            case R.id.tvEndMF:
                hideKeyboard();
                displayTimePickerDialog(2);
                break;
            case R.id.tvStartSat:
                hideKeyboard();
                displayTimePickerDialog(3);
                break;
            case R.id.tvEndSat:
                hideKeyboard();
                displayTimePickerDialog(4);
                break;
            case R.id.tvStartSun:
                hideKeyboard();
                displayTimePickerDialog(5);
                break;
            case R.id.tvEndSun:
                hideKeyboard();
                displayTimePickerDialog(6);
                break;
            case R.id.imgAddServices:
                addServicesView();
                break;
            case R.id.tvHints:
                showHintPopmenu();
                break;
            case R.id.img_add_website:
                addWebsiteSection();
                break;
            case R.id.img_add_email:
                addEmailSection();
                break;
        }

    }

    private void showHintPopmenu() {

        /*popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });*/
        popupMenu.show();
    }

    private void displayTimePickerDialog(int type) {
        time_type = type;
        timePickerDialog.show();

    }

    private void pickContacts() {

        Intent intent = new Intent(getContext(), ContactPickerActivity.class)
                // .putExtra(ContactPickerActivity.EXTRA_THEME, mDarkTheme ? R.style.Theme_Dark : R.style.Theme_Light)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_BADGE_TYPE, ContactPictureType.ROUND.name())
                .putExtra(ContactPickerActivity.EXTRA_SHOW_CHECK_ALL, true)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION, ContactDescription.ADDRESS.name())
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_SORT_ORDER, ContactSortOrder.AUTOMATIC.name());
        startActivityForResult(intent, REQUEST_CONTACT);
    }


    private void multipleMediaSelector() {
        mediaList.clear();
        videoLists.clear();
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CONTACT && resultCode == Activity.RESULT_OK &&
                data != null && data.hasExtra(ContactPickerActivity.RESULT_CONTACT_DATA)) {
            List<String> selPhoneList = new ArrayList<>();

            // we got a result from the contact picker

            // process contacts
            List<Contact> contacts = (List<Contact>) data.getSerializableExtra(ContactPickerActivity.RESULT_CONTACT_DATA);
            for (Contact contact : contacts) {
                // process the contacts...
                contact.getPhone(0);
                //   displayLog(TAG, "onActivityResult:contact: "+contact.getPhotoUri() );
                if (contact.getPhone(0) != null && contact.getPhone(0).trim().length() != 0) {
                    selPhoneList.add(contact.getPhone(0));
                } else if (contact.getPhone(1) != null && contact.getPhone(1).trim().length() != 0) {
                    selPhoneList.add(contact.getPhone(1));
                } else if (contact.getPhone(2) != null && contact.getPhone(2).trim().length() != 0) {
                    selPhoneList.add(contact.getPhone(2));
                } else {
                    displayToast("no contact found");
                }

            }

            if (contacts != null && contacts.size() > 0) {
                etMobile.setText(selPhoneList.get(0));
                addPhoneView(selPhoneList);
            }
        }


        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            //  List<Uri> uris = Matisse.obtainResult(data);
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                mediaList.addAll(paths);
                refreshGalleryAdapter();
                if (isVideoCompressingNeeded()) {
                    compressVideo(videoLists);

                }
            }
        }


        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {

            }
            else if (resultCode == RavePayActivity.RESULT_ERROR) {

            }
            else if (resultCode == RavePayActivity.RESULT_CANCELLED) {

            }
        }


    }

    private void refreshGalleryAdapter() {
        if (mediaList != null) {
            galleryAdapter.notifyDataSetChanged();
            tvGallery.setText("" + mediaList.size() + " items are selected.. ");
        }
    }

    @Override
    public void onAdapterClickListener(int position) {
        for (int i = 0; i < mediaList.size(); i++) {
            if (mediaList.get(position).equalsIgnoreCase(mediaList.get(i))) {
                mediaList.remove(i);
                refreshGalleryAdapter();
                if (videoLists != null && videoLists.size() > 0) {

                }
            }
        }
    }

    private boolean isValidate() {

        if (Valiations.hasText(etName)
                && Valiations.hasText(etType) && Valiations.hasText(etNote) && Valiations.hasText(etTag)) {

            String name = etName.getText().toString().trim();
            if (name.length() <= 3 || name.length() >= 30) {
                displayToast("Title should contain 3 to 30 letters");
                return false;
            }
            String address = etAddress.getText().toString().trim();
            if (address.isEmpty()) {
                displayToast("Enter address");
                return false;
            }


            String email = etEmail.getText().toString().trim();
            if (!email.isEmpty()) {
                if (isEmailValid(email)) {
                    email = etEmail.getText().toString().trim();
                } else {
                    displayToast("Invalid email address");
                    return false;
                }
            }

            if (!etMobile.getText().toString().trim().matches("") || phoneTextList.size() > 0) {
                String mobile = etMobile.getText().toString().trim();
                if (mobile.length() == 0) {
                    displayToast("Add mobile number");
                    return false;
                }

                if (mobile.length() < 7 || mobile.length() > 15) {
                    displayToast("Mobile number should contain 7 to 15 digits");
                    return false;
                }
                if (getContacts() == null) {
                    displayToast("Mobile number should contain 7 to 15 digits");
                    return false;
                }

            } else {
                displayToast("Add mobile number");
                return false;
            }


            if (getItemCategory() == null) {
                displayToast("Select Category");
                return false;
            }
            if (getItemSubCategory() == null) {
                displayToast("Select Subcategory");
                return false;
            }
            if (getItemCity() == null) {
                displayToast("Select Region");
                return false;
            }

            if (etTwitter.getText().toString().trim().length() > 0) {
                if (!etTwitter.getText().toString().trim().startsWith("@")) {
                    displayToast("Please enter valid twitter name");
                    return false;
                }
            }

            HashMap<String, String> map = new HashMap<>();

//            if (tv_unverify_status.getVisibility() == View.VISIBLE) {
//                String status = tv_unverify_status.getText().toString().trim();
//                map.put(BaseArguments.status, status);
//            }
//            if (tv_unverify_status.getVisibility() != View.VISIBLE &&
//                    getStatus() == null) {
//                displayToast("Select Status");
//                return false;
//            } else {
//                if (tv_unverify_status.getVisibility() != View.VISIBLE)
//                    map.put(BaseArguments.status, getStatus());
//            }
            map.put(BaseArguments.status, getStatus());

            if (mediaList.size() <= 0) {
                displayToast("Select Gallery Images");
                return false;
            }
            boolean isOpenHourSelected = false;

            if (chkMF.isChecked()) {
                isOpenHourSelected = true;
                if (tvStartMF.getText().toString().trim().equals(getContext().getResources().getString(R.string.Start_Time))) {
                    displayToast("Select Start Time");
                    return false;
                }
                if (tvEndMF.getText().toString().trim().equals(getContext().getResources().getString(R.string.End_Time))) {
                    displayToast("Select End Time");
                    return false;
                }
            }

            if (chkSat.isChecked()) {
                isOpenHourSelected = true;
                if (tvStartSat.getText().toString().trim().equals(getContext().getResources().getString(R.string.Start_Time))) {
                    displayToast("Select Start Time");
                    return false;
                }
                if (tvEndSat.getText().toString().trim().equals(getContext().getResources().getString(R.string.End_Time))) {
                    displayToast("Select End Time");
                    return false;
                }
            }
            if (chkSun.isChecked()) {
                isOpenHourSelected = true;

                if (tvStartSun.getText().toString().trim().equals(getContext().getResources().getString(R.string.Start_Time))) {
                    displayToast("Select Start Time");
                    return false;
                }
                if (tvEndSun.getText().toString().trim().equals(getContext().getResources().getString(R.string.End_Time))) {
                    displayToast("Select End Time");
                    return false;
                }
            }

            if (!isOpenHourSelected) {
                displayToast("Select at least one Opening Hour");
                return false;
            }
        } else {
            displayToast("Please fill all fields.");

            return false;
        }
        return true;
    }

    private void onSubmit() {
        if (!isValidate()) return;
        if (ConnectionDetector.isNetAvail(getContext())) {
            onSubmitToServer();
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private void compressVideo(List<CompressionModel> videoLists) {
        String outputDir = Environment.getExternalStorageDirectory() + "/MapGhanaVideo";
        File file = new File(outputDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        if (videoLists == null || videoLists.size() == 0) return;
        displayProgressBar(false, "Video is compressing...");
        for (int i = 0; i < videoLists.size(); i++) {
            String destinationPath = outputDir + File.separator + "MapGhana_VID_ORG_" + i + System.currentTimeMillis() + ".mp4";
            final CompressionModel model = videoLists.get(i);
            model.setCompressionStatus(0);
            model.setDestinationPath(destinationPath);
            VideoCompress.compressVideoHigh(model.getSourcePath(), model.getDestinationPath(), model.getId(), new VideoCompress.CompressListener() {
                @Override
                public void onStart(long mTask_id) {
                }

                @Override
                public void onSuccess(long mTask_id, String source, String destination) {
                    model.setCompressionStatus(1);
                    checkVideoCompressingComplete();
                }

                @Override
                public void onFail(long mTask_id, String source, String destination) {
                    model.setCompressionStatus(-1);
                    checkVideoCompressingComplete();
                }

                @Override
                public void onProgress(long mTask_id, float percent) {

                }
            });
        }
    }

    private void checkVideoCompressingComplete() {
        boolean isCompressComplete = true;
        for (CompressionModel model : videoLists) {
            if (model.getCompressionStatus() == 0) {
                isCompressComplete = false;
                break;
            }

        }

        if (isCompressComplete) dismissProgressBar();
        if (isCompressComplete && checkAllVideoCompressingSuccess()) {
            for (CompressionModel model : videoLists) {
                mediaList.set((int) model.getId(), model.getDestinationPath());
            }
        }
    }

    private boolean checkAllVideoCompressingSuccess() {
        boolean allCompressingStatus = true;
        for (CompressionModel model : videoLists) {
            if (model.getCompressionStatus() == -1) {
                allCompressingStatus = false;
                break;
            }

        }
        return allCompressingStatus;
    }

    private boolean isVideoCompressingNeeded() {
        videoLists.clear();
        CompressionModel model;
        for (int i = 0; i < mediaList.size(); i++) {
            FilePathHelper filePathHelper = new FilePathHelper();
            String mimeType = filePathHelper.getMimeType(mediaList.get(i));
            if (video_mime_List.contains(mimeType)) {
                model = new CompressionModel();
                model.setId(i);
                model.setSourcePath(mediaList.get(i));
                videoLists.add(model);
            }
        }

        return videoLists.size() > 0;
    }

    private void onSubmitToServer() {

        setEmailList();
        setWebsiteList();


        JsonArray emailArray = null;
        JsonArray websiteArray = null;

        Gson gson = new Gson();

        Gson open_hour = new Gson();
        JsonObject open_hour_jsonObject = new JsonObject();
        JsonObject mon_friday = new JsonObject();
        JsonObject sat = new JsonObject();
        JsonObject sun = new JsonObject();
        String start_mf = "";
        String end_mf = "";
        String start_Sat = "";
        String end_Sat = "";
        String start_Sun = "";
        String end_Sun = "";
        if (chkMF.isChecked()) {
            start_mf = tvStartMF.getText().toString().trim();
            end_mf = tvEndMF.getText().toString().trim();
            mon_friday.addProperty(BaseArguments.start, start_mf);
            mon_friday.addProperty(BaseArguments.end, end_mf);
        }
        if (chkSat.isChecked()) {
            start_Sat = tvStartSat.getText().toString().trim();
            end_Sat = tvEndSat.getText().toString().trim();
            sat.addProperty(BaseArguments.start, start_Sat);
            sat.addProperty(BaseArguments.end, end_Sat);
        }
        if (chkSun.isChecked()) {
            start_Sun = tvStartSun.getText().toString().trim();
            end_Sun = tvEndSun.getText().toString().trim();
            sun.addProperty(BaseArguments.start, start_Sun);
            sun.addProperty(BaseArguments.end, end_Sun);
        }

        if (mon_friday.size() > 0) {
            open_hour_jsonObject.add(BaseArguments.monday_friday, mon_friday);
        }
        if (sat.size() > 0) {
            open_hour_jsonObject.add(BaseArguments.saturday, sat);
        }
        if (sun.size() > 0) {
            open_hour_jsonObject.add(BaseArguments.sunday, sun);
        }

        String op_hour = open_hour.toJson(open_hour_jsonObject);
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);

            String website = "", twitter = "", services = "";


            if (!etWebsite.getText().toString().trim().matches("")) {
                website = etWebsite.getText().toString().trim();
            }
            if (!etTwitter.getText().toString().trim().matches("")) {
                twitter = etTwitter.getText().toString().trim();
            }

            JsonElement elementEmail = gson.toJsonTree(mEmailList, new TypeToken<List<String>>() {
            }.getType());

            if (!elementEmail.isJsonArray()) {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            emailArray = elementEmail.getAsJsonArray();

            JsonElement elementWebsite = gson.toJsonTree(listWebsiteItem, new TypeToken<List<String>>() {
            }.getType());

            if (!elementWebsite.isJsonArray()) {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            websiteArray = elementWebsite.getAsJsonArray();

            JsonArray array = null;
//            if (!etServices.getText().toString().trim().matches("") || serviceTextList.size() > 0) {
//                services = getServices() + "\"" + etServices.getText().toString().trim();
//
//                services = "[" + services + "\"]";
//
//                JsonParser jsonParser = new JsonParser();
//                JsonElement jsonElement = jsonParser.parse(services);
//                array = jsonElement.getAsJsonArray();
//            }
            String email = etEmail.getText().toString().trim();
            String phone = "";
            phone = getContacts() + "\"" + etMobile.getText().toString().trim();
            phone = "[" + phone + "\"]";
            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(phone);
            JsonArray phoneArray = null;
            phoneArray = jsonElement.getAsJsonArray();
            HashMap<String, String> map = new HashMap<>();

//            if (tv_unverify_status.getVisibility() == View.VISIBLE) {
//                String status = tv_unverify_status.getText().toString().trim();
//                map.put(BaseArguments.status, status);
//            }
//            if (tv_unverify_status.getVisibility() != View.VISIBLE &&
//                    getStatus() == null) {
//                displayToast("Select Status");
//                return;
//            } else {
//                if (tv_unverify_status.getVisibility() != View.VISIBLE)
//                    map.put(BaseArguments.status, getStatus());
//            }
            map.put(BaseArguments.status, getStatus());
            Session session = AppPreferences.getSession();
            if(session != null && !TextUtils.isEmpty(String.valueOf(session.getId()))) {
                map.put(BaseArguments.user_id, String.valueOf(session.getId()));
            }
            map.put(BaseArguments.post_for, Constants.organizations);
            map.put(BaseArguments.email, TextUtils.join(",", mEmailList));
            map.put(BaseArguments.website, TextUtils.join(",", listWebsiteItem));
            map.put(BaseArguments.twitter, twitter);
            map.put(BaseArguments.category_id, getItemCategory());
            map.put(BaseArguments.sub_category_id, getItemSubCategory());
            map.put(BaseArguments.gender, "female");
            map.put(BaseArguments.opening_hours, op_hour);
            map.put(BaseArguments.mapghanaid, txtMapGhanaId.getText().toString());
            map.put(BaseArguments.city_id, getItemCity());
            map.put(BaseArguments.type, etType.getText().toString().trim());
            map.put(BaseArguments.notes, etNote.getText().toString().trim());
            map.put(BaseArguments.tags, etTag.getText().toString().trim());
            if (serviceTextList.size() > 0) {
                map.put(BaseArguments.features, TextUtils.join(",", serviceTextList));

            } else {
                map.put(BaseArguments.features, "");
            }
            if ("general".equalsIgnoreCase(locationProfile)) {
                map.put(BaseArguments.lat, String.valueOf(generalLatitude));
                map.put(BaseArguments.log, String.valueOf(generalLongitude));
            } else {
                map.put(BaseArguments.lat, String.valueOf(latitude));
                map.put(BaseArguments.log, String.valueOf(longitude));
            }
            map.put(BaseArguments.name, etName.getText().toString().trim());
            String address = etAddress.getText().toString().trim();
            map.put(BaseArguments.address, address);

            if (phoneArray != null) {
                map.put(BaseArguments.phone, phoneArray.toString());
            } else {
                map.put(BaseArguments.phone, "");

            }
            map.put(BaseArguments.location_profile, locationProfile);
            map.put(BaseArguments.phone_visibility, phoneNumberVisibility);
            map.put(BaseArguments.availability, availablity);
            map.put(BaseArguments.optimization, optimisation);
            map.put(BaseArguments.twitter, etTwitter.getText().toString());
            map.put(BaseArguments.instagram, etInstagram.getText().toString());
            map.put(BaseArguments.facebook, etFacebook.getText().toString());
            map.put(BaseArguments.youtube, etYoutubeVimeo.getText().toString());
            map.put(BaseArguments.soundclud, etSoundCloud.getText().toString());
            JsonElement elementblog = gson.toJsonTree(mBlogList, new TypeToken<List<String>>() {
            }.getType());

            if (!elementblog.isJsonArray()) {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            map.put(BaseArguments.profile_visibility, profileVisibility);
            map.put(BaseArguments.password_protected, passwordProtection);
            map.put(BaseArguments.password, etPasswordProtected.getText().toString());

            MultipartBody.Part[] multiImgPart = new MultipartBody.Part[mediaList.size()];
            for (int i = 0; i < mediaList.size(); i++) {
                String url = mediaList.get(i);
                FilePathHelper filePathHelper = new FilePathHelper();
                FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
                String mimeType = fileInformation.getMimeType();
                if (video_mime_List.contains(mimeType)) {
                    File file = new File(mediaList.get(i));
                    RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                    multiImgPart[i] = MultipartBody.Part.createFormData(BaseArguments.gallery_images + "[" + i + "]",
                            file.getName(), media);
                } else {
                    File file = new File(mediaList.get(i));
                    RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_IMAGE_PNG, file);
                    multiImgPart[i] = MultipartBody.Part.createFormData(BaseArguments.gallery_images + "[" + i + "]", file.getName(), media);
                }
            }
            restClient.post_organization(getContext(), RetrofitUtils.createMultipartRequest(map), multiImgPart);
        } else {
            displayToast(Constants.No_Internet);
        }


    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_city) {
                try {

                    String s = response.body().string();
                    Gson gson = new Gson();
                    City city = gson.fromJson(s, City.class);
                    if (city.getStatus() != 0) {
                       /* City.DataBean city1 = new City.DataBean();
                        city1.setName("Select City");
                        cityList.add(0, city1);*/
                        cityList.addAll(city.getData());
                        cityAdapter = new CityAdapter(getContext(), cityList);
                        cityAdapter.setDataBean(0);
                        spCity.setSelection(0);
                        spCity.setAdapter(cityAdapter);
                        getCategory();

                    } else {
                        displayErrorDialog("Error", city.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_CATEGORY) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Category category = gson.fromJson(s, Category.class);
                    if (category.getStatus() != 0) {
                        Category.DataBean bean = new Category.DataBean();
                        bean.setName("Select Category");
                        categoryList.add(bean);
                        categoryList.addAll(category.getData());
                        categoryAdapter = new CategoryAdapter(getContext(), categoryList);
                        categoryAdapter.setDataBean(0);
                        spCategory.setAdapter(categoryAdapter);

                        ///setSubCategory();
                    } else {
                        displayErrorDialog("Error", category.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_PostOrganization) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    CreateOrganizationPost posting = gson.fromJson(s, CreateOrganizationPost.class);
                    if (posting.getStatus() != 0) {
                        displayToast(posting.getMessage());

                        try {
                            getDashboardActivity().clearFragmentBackStack();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else {
                        displayErrorDialog("Error", posting.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }

            } else if (apiId == ApiIds.ID_MAPGHANA_ID) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    MapGhanaId posting = gson.fromJson(s, MapGhanaId.class);
                    if (posting.getStatus() != 0) {
                        dismissProgressBar();
                        llMapghanaIDAvailable.setVisibility(View.VISIBLE);
                        txtMapGhanaId.setVisibility(View.VISIBLE);
                        txtMapGhanaId.setText(BaseArguments.MAPGHANA_ID_URL + "" + etMapghanaId.getText().toString());
                    } else {
                        llMapghanaIDAvailable.setVisibility(View.GONE);
                        displayErrorDialog("Error", posting.getMessage());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }

            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    private void setSubCategory() {
        clearSubCategory();
        String data = getItemCategory();
        if (data == null) return;
        ll_sub_category.setVisibility(View.VISIBLE);

        int cat_id = Integer.parseInt(data);
        for (int i = 0; i < categoryList.size(); i++) {
            List<Category.DataBean.SubCategoryBean> sub_category_list = categoryList.get(i).getSub_category();
            int _id = categoryList.get(i).getId();
            if (sub_category_list != null && sub_category_list.size() > 0 && _id == cat_id) {
                Category.DataBean.SubCategoryBean bean = new Category.DataBean.SubCategoryBean();
                bean.setName("Select sub category");
                subCategoryList.add(bean);
                subCategoryList.addAll(sub_category_list);
                subCategoryAdapter = new SubCategoryAdapter(getContext(), subCategoryList);
                subCategoryAdapter.setSubCategoryBean(0);
                spSubCategory.setAdapter(subCategoryAdapter);
            }
        }
    }

    private void clearSubCategory() {
        if (subCategoryList != null && subCategoryList.size() > 0) {
            subCategoryList.clear();
        }
    }

    @Override
    public void OnDoneButton(Dialog dialog, Calendar c) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(c.getTime());

        if (time_type == 1) {
            tvStartMF.setText(time);
        }
        if (time_type == 2) {
            tvEndMF.setText(time);
        }
        if (time_type == 3) {
            tvStartSat.setText(time);
        }
        if (time_type == 4) {
            tvEndSat.setText(time);
        }
        if (time_type == 5) {
            tvStartSun.setText(time);
        }
        if (time_type == 6) {
            tvEndSun.setText(time);
        }
        timePickerDialog.dismiss();
    }

    @Override
    public void OnCancelButton(Dialog dialog) {
        timePickerDialog.dismiss();

    }

    private void addServicesView() {
        final View serviceView = LayoutInflater.from(getContext()).inflate(R.layout.item_add_services, null, false);

        final Spinner spinnerText = serviceView.findViewById(R.id.etServices2);
        ImageView imgServices = serviceView.findViewById(R.id.imgRemove);


        SimpleSpinnerAdapter adapter = new SimpleSpinnerAdapter(getContext(), getContext().getResources().getStringArray(R.array.feature));
        spinnerText.setAdapter(adapter);

        spinnerText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelGender(position);
                serviceTextList.add(adapter.getSelGender());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imgServices.setOnClickListener(v -> {
            ((LinearLayout) serviceView.getParent()).removeView(serviceView);
            serviceTextList.remove(serviceView.getParent());
        });


        serviceView.setTag(ll_addServiceView.getChildCount() - 1);
        ll_addServiceView.addView(serviceView, ll_addServiceView.getChildCount());
    }

    private void onrefresh() {
        for (int i = 0; i < ll_addServiceView.getChildCount(); i++) {
            View view = ll_addServiceView.getChildAt(i);
            TypefaceEditText editText = view.findViewById(R.id.etServices2);
            ImageView imgServices = view.findViewById(R.id.imgRemove);
            imgServices.setTag(i);
            //remove_icon.setTag(i);

        }
    }

//    private String getServices() {
//        String services = "";
//        if (serviceTextList != null && serviceTextList.size() > 0) {
//            for (int i = 0; i < serviceTextList.size(); i++) {
//                if (serviceTextList.get(i).getText().toString().trim().length() != 0) {
//                    services = "\"" + serviceTextList.get(i).getText().toString().trim() + "\", " + services;
//                }
//            }
//        }
//        return services;
//    }


    private void addPhoneView() {

        final View phoneView = LayoutInflater.from(getContext()).inflate(R.layout.item_phone_view, null, false);

        final AppCompatEditText editText = phoneView.findViewById(R.id.etServices2);
        ImageView img = phoneView.findViewById(R.id.imgRemove);
        //  editText.setText("done");
        img.setOnClickListener(v -> {
            removePhoneView(phoneView, editText);
            refreshPhoneTextList(editText);

        });
        phoneTextList.add(editText);
        phoneView.setTag(ll_addPhoneView.getChildCount() - 1);
        ll_addPhoneView.addView(phoneView, ll_addPhoneView.getChildCount());
    }

    private void addPhoneView(List<String> contactList) {
        for (int i = contactList.size() - 1; i > 0; i--) {
            final View phoneView = LayoutInflater.from(getContext()).inflate(R.layout.item_phone_view, null, false);
            final AppCompatEditText editText = phoneView.findViewById(R.id.etServices2);
            ImageView img = phoneView.findViewById(R.id.imgRemove);
            editText.setText(contactList.get(i));
            img.setOnClickListener(v -> {
                removePhoneView(phoneView, editText);
                refreshPhoneTextList(editText);

            });
            phoneTextList.add(editText);
            phoneView.setTag(ll_addPhoneView.getChildCount() - 1);
            ll_addPhoneView.addView(phoneView, ll_addPhoneView.getChildCount());
        }
    }


    private void removePhoneView(View serviceView, EditText editText) {
        ll_addPhoneView.removeView(serviceView);
        if (phoneTextList != null && phoneTextList.size() > 0) {
            phoneTextList.remove(editText);
        }
        onrefreshPhoneList();
    }

    private void refreshPhoneTextList(AppCompatEditText editText) {
        for (int i = 0; i < phoneTextList.size(); i++) {
            if (editText == phoneTextList.get(i)) {
                phoneTextList.remove(i);
            }
        }
    }

    private void onrefreshPhoneList() {
        for (int i = 0; i < ll_addPhoneView.getChildCount(); i++) {
            View view = ll_addPhoneView.getChildAt(i);
            TypefaceEditText editText = (TypefaceEditText) view.findViewById(R.id.etServices2);
            ImageView imgServices = (ImageView) view.findViewById(R.id.imgRemove);
            imgServices.setTag(i);
            //remove_icon.setTag(i);
        }
    }

    private String getContacts() {
        String services = "";
        if (phoneTextList != null && phoneTextList.size() > 0) {
            for (int i = 0; i < phoneTextList.size(); i++) {
                String phoneNumber = phoneTextList.get(i).getText().toString().trim();
                if (phoneNumber.length() != 0) {
                    if (phoneNumber.length() < 7 || phoneNumber.length() > 15) {
                        return null;
                    }
                    services = "\"" + phoneNumber + "\", " + services;
                }
            }
        }
        return services;

    }

    private long getDuration(String path) {
        File videoFile = new File(path);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        //use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(getContext(), Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        retriever.release();
        return timeInMillisec;

    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.map = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            enableLocationComponent(style);

            symbolManager = new SymbolManager(mapView, map, style);
            circleManager = new CircleManager(mapView, map, style);
            symbolManager.setIconAllowOverlap(false);  //your choice t/f
            symbolManager.setTextAllowOverlap(false);  //your choice t/f

            Location location = Locationservice.sharedInstance().getLastLocation();
            if (location != null) {
                Point originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());

            }
            circleManager.addDragListener(new OnCircleDragListener() {
                @Override
                public void onAnnotationDragStarted(Circle annotation) {

                }

                @Override
                public void onAnnotationDrag(Circle annotation) {
                    generalLatitude = annotation.getLatLng().getLatitude();
                    generalLongitude = annotation.getLatLng().getLongitude();
                }

                @Override
                public void onAnnotationDragFinished(Circle annotation) {

                }
            });

            radioButtonSpecific.setChecked(true);
            getLocationProfile(R.id.radio_specific);
            map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 6));
            createMarkerSymbol();
        });
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = map.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(map.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        etSoundCloud.removeTextChangedListener(watcher);
        etYoutubeVimeo.removeTextChangedListener(watcher);
        etFacebook.removeTextChangedListener(watcher);
        etInstagram.removeTextChangedListener(watcher);
        etTwitter.removeTextChangedListener(watcher);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(paymentReceiver);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void userLocationChange(Location location) {
        Toast.makeText(getActivity(), "" + location.getLatitude(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    @Override
    public void onSave(String type) {
        TransactionPrice price = AppPreferences.getTransactionPrice();
        if (type.equalsIgnoreCase("is_advanced")) {
            openMakePayemntScreen("add_organisation", "is_advanced",price.getOrganisation().getAdvance_feature());
        } else if (type.equalsIgnoreCase("is_optimization")) {
            openMakePayemntScreen("add_organisation", "is_optimization",price.getOrganisation().getOptimization());
        }
    }

    @Override
    public void onCancel(String type) {
        if (type.equalsIgnoreCase("is_advanced")) {
            switchCompatAdvance.setChecked(false);
        } else if (type.equalsIgnoreCase("is_optimization")) {
            switchCompatOptimization.setChecked(false);
        }
    }
}