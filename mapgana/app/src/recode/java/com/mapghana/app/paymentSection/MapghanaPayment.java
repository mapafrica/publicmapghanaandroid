package com.mapghana.app.paymentSection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.flutterwave.raveandroid.RaveConstants;
import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RavePayManager;
import com.mapghana.R;
import com.mapghana.app.model.Session;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;

import java.util.UUID;

public class MapghanaPayment extends AppCompatActivity {
    private static final String country = "GH";
    private static final String currency = "GHS";
    private static final String publicKey = "FLWPUBK_TEST-5fc7c7809f26f4ce66fcfa967c033f80-X"; //Get your public key from your account
    private static final String encryptionKey = "FLWSECK_TEST76a88014027e"; //Get your encryption key from your account
    private Session session;
    private String narration = "pay";
    private double amount;
    private int id;
    private String screen_type, paymentType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = AppPreferences.getSession();

        Intent intent = getIntent();
        if (intent != null) {
            amount = intent.getIntExtra("amount", 0);
            id = intent.getIntExtra("id", 0);
            screen_type = intent.getStringExtra("screen_type");
            paymentType = intent.getStringExtra("payment_type");
        }

        String txRef = session.getEmail() + " " + UUID.randomUUID().toString();
        new RavePayManager(this).setAmount(amount)
                .setCountry(country)
                .setCurrency(currency)
                .setEmail(session.getEmail())
                .setfName(session.getName())
                .setlName(session.getUsername())
                .setNarration(narration)
                .setPublicKey(publicKey)
                .setEncryptionKey(encryptionKey)
                .setTxRef(txRef)
                .acceptAccountPayments(false)
                .acceptCardPayments(
                        true)
                .acceptMpesaPayments(false)
                .acceptGHMobileMoneyPayments(true)
                .onStagingEnv(false).
                allowSaveCardFeature(true)
                .withTheme(R.style.DefaultTheme)
                .initialize();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Intent intent = new Intent(Constants.LOCAL_BROADCAST_PAYMENT);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);

        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_PAYMENT_SUCCESS);
                intent.putExtra("screen_type",screen_type);
                intent.putExtra("payment_type",paymentType);
                intent.putExtra("id",id);
                intent.putExtra("data",message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                finish();
            } else if (resultCode == RavePayActivity.RESULT_ERROR) {
                intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_PAYMENT_ERROR);
                intent.putExtra("data",message);
                intent.putExtra("screen_type",screen_type);
                intent.putExtra("payment_type",paymentType);
                intent.putExtra("id",id);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                finish();
            } else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_PAYMENT_CANCELLED);
                intent.putExtra("data",message);
                intent.putExtra("screen_type",screen_type);
                intent.putExtra("payment_type",paymentType);
                intent.putExtra("id",id);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}


