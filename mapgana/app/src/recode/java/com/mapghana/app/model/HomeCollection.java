package com.mapghana.app.model;

public class HomeCollection {
    public String _id="";
    public String event_name="";
    public String date="";
    public String event_desc="";

    public HomeCollection(String event_name, String date, String event_desc) {
        this.event_name = event_name;
        this.date = date;
        this.event_desc = event_desc;
    }
}
