package com.mapghana.app.utils;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.gu.toolargetool.TooLargeTool;
import com.mapghana.app.model.City;
import com.mapghana.app.model.PopularCategory;
import com.mapghana.app.service.LocationServiceListner;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;


/**
 * Created by ubuntu on 6/12/17.
 */

public class MapGhanaApplication extends Application {

    private static MapGhanaApplication mSharedInstance;
    private String currentLocationAddress="";
    private ArrayList<City.DataBean> cityList;
    private List<PopularCategory.PopulatCategoryModel> populatCategoryList;
    public static MapGhanaApplication sharedInstance() {
        return mSharedInstance;
    }
    LocationServiceListner locationServiceListner;
    public Location mLocation;

    public LocationServiceListner getLocationServiceListner() {
        return locationServiceListner;
    }

    public void setLocationServiceListner(LocationServiceListner locationServiceListner) {
        this.locationServiceListner = locationServiceListner;
    }

    public void setCityList(ArrayList<City.DataBean> cityList) {
        this.cityList = cityList;
    }

    public ArrayList<City.DataBean> getCityList() {
        return cityList;
    }

    public void setCurrentLocationAddress(String currentLocationAddress) {
        this.currentLocationAddress = currentLocationAddress;
    }

    public void setPopulatCategoryList(List<PopularCategory.PopulatCategoryModel> populatCategoryList) {
        this.populatCategoryList = populatCategoryList;
    }

    public List<PopularCategory.PopulatCategoryModel> getPopulatCategoryList() {
        return populatCategoryList;
    }

    public String getCurrentLocationAddress() {
        return currentLocationAddress;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            mSharedInstance = this;
            AppPreferences.init(this);
            Fabric.with(this, new Crashlytics());
            TooLargeTool.startLogging(this);
        }catch (Exception e){}
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
