package com.mapghana.app.interfaces;

import com.mapbox.mapboxsdk.geometry.LatLng;

public interface NavigateListener {
    void onNavigateClick(LatLng position);
}
