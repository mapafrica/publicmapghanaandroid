package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class EventCalendarModel implements Serializable {

    private int status;
    private String message;
    private ArrayList<CalendarContent> data;
    private String error;


    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<CalendarContent> getData() {
        return data;
    }

    public String getError() {
        return error;
    }

}
