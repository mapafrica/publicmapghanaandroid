package com.mapghana.app.interfaces;

import com.mapghana.app.model.EventGallery;

public interface MediaClickHandler {
    void onClick(EventGallery gallery);
}
