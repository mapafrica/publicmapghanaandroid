package com.mapghana.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.TransactionPrice;

public class AppPreferences {

    private static final String PREFS_NAME = "MyPrefsFile";
    private static final String SESSION = "session";
    private static final String TRANSACTION_PRICE = "transaction_price";
    private static final String IS_PREFERENCE_ADDED = "is_preference_added";
    private static final String USER_STATUS = "user_status";
    private static final String IMAGE_PATH = "image_path";
    private static final String BOOKMARK_IS_FIRST_TIME = "bookmark_is_first_time";

    private static final String SELECTED_HOME_SCREEN = "selected_home_screen";
    private static final String SELECTED_TEXT_SIZE = "selected_text_size";
    private static final String HAS_MEMBERSHIP_MONTH_NOTIFICATION_DISPLAYED = "membership_month_notification_displayed";
    private static final String MEMBERSHIP_EXPIRED_NOTIFICATION_LAST_DISPLAYED =
            "membership_expired_notification_last_displayed";


    private static final String IS_FCM_TOKEN_SEND = "IS_FCM_TOKEN_SEND";

    private static SharedPreferences mSharedPreferences;
    //ref to the last updated session
    private static Session mSession;

    private static TransactionPrice mTransactionPrice;

    private AppPreferences() {
        //private constructor to enforce Singleton pattern
    }

    /**
     * initialize the reference to shared preferences, this need to be initialize before any function call to this class
     *
     * @param context application context
     */
    static void init(@NonNull Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
    }

    /**
     * put boolean value with key
     *
     * @param key   key
     * @param value value
     */
    private static void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * get boolean value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value
     */
    @SuppressWarnings("SameParameterValue")
    private static boolean getBoolean(String key, boolean defValue) {
        return mSharedPreferences.getBoolean(key, defValue);
    }

    /**
     * put long value with key
     *
     * @param key   key
     * @param value value
     */
    static void putLong(String key, long value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * get value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value
     */
    @SuppressWarnings("SameParameterValue")
    static long getLong(String key, long defValue) {
        return mSharedPreferences.getLong(key, defValue);
    }

    /**
     * put int value with key
     *
     * @param key   key
     * @param value value
     */
    private static void putInt(String key, int value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * get value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value
     */
    @SuppressWarnings("SameParameterValue")
    private static int getInt(String key, int defValue) {
        return mSharedPreferences.getInt(key, defValue);
    }

    /**
     * get the string value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value for key
     */
    @SuppressWarnings("SameParameterValue")
    public static String getString(String key, @Nullable String defValue) {
        return mSharedPreferences.getString(key, defValue);
    }

    /**
     * put long value with key
     *
     * @param key   key
     * @param value value
     */
    public static void putString(String key, @Nullable String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * remove the keys that are required to be removed on logout
     */
    public static void logout() {

        //reset the session
        mSession = new Session();

        //clear all preferences
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * Get the reference to last updated session
     *
     * @return the session
     */
    public static Session getSession() {

        //create the single instance of session
        if (mSession == null) {
            synchronized (AppPreferences.class) {
                if (mSession == null) {
                    String json = mSharedPreferences.getString(SESSION, null);
                    if (TextUtils.isEmpty(json)) {
                        mSession = new Session();
                    } else {
                        mSession = new Gson().fromJson(json, Session.class);
                    }
                }
            }
        }

        return mSession;
    }

    public static TransactionPrice getTransactionPrice() {

        //create the single instance of session
        if (mTransactionPrice == null) {
            synchronized (AppPreferences.class) {
                if (mTransactionPrice == null) {
                    String json = mSharedPreferences.getString(TRANSACTION_PRICE, null);
                    if (TextUtils.isEmpty(json)) {
                        mTransactionPrice = new TransactionPrice();
                    } else {
                        mTransactionPrice = new Gson().fromJson(json, TransactionPrice.class);
                    }
                }
            }
        }

        return mTransactionPrice;
    }

    /**
     * Save the current session
     *
     * @param session session
     */
    public static void setSession(Session session) {
        mSession = session;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(SESSION, new Gson().toJson(session));
        editor.apply();
    }


    public static void setTransactionPrice(TransactionPrice price) {
        mTransactionPrice = price;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TRANSACTION_PRICE, new Gson().toJson(price));
        editor.apply();
    }
    /**
     * Return true if init preference setup is done
     *
     * @return true if init preference setup is done
     */
    public static boolean isPreferenceAdded() {
        return mSharedPreferences.getBoolean(IS_PREFERENCE_ADDED, false);
    }

    /**
     * mark the preference setup is done
     */
    public static void markPreferenceAdded() {
        putBoolean(IS_PREFERENCE_ADDED, true);
    }

    /**
     * Clear bookmark after mentioned days
     *
     * @return days
     */
    public static int getUserStatus() {
        return getInt(USER_STATUS, 0);
    }

    /**
     * Set bookmark expiry days
     *
     * @param status status
     */
    public static void setUserStatus(int status) {
        putInt(USER_STATUS, status);
    }

    /**
     * get sectorID for spinner
     */
    public static String getImagePath() {
        return getString(IMAGE_PATH, null);
    }

    /**
     * Set sectorID for spinner
     *
     * @param imagePath url
     */
    public static void setImagePath(String imagePath) {
        putString(IMAGE_PATH, imagePath);
    }

    /**
     * Is first time bookmarked
     *
     * @return is first time bookmarked
     */
    public static boolean isFirstTimeBookmarked() {
        return getBoolean(BOOKMARK_IS_FIRST_TIME, true);
    }

    /**
     * mark first time bookmarked
     */
    public static void markFirstTimeBookmarked() {
        putBoolean(BOOKMARK_IS_FIRST_TIME, false);
    }




    /**
     * Get the selected home screen in the setting
     *
     * @return int value
     */
    public static int getSelectedHomeScreen() {
        return mSharedPreferences.getInt(SELECTED_HOME_SCREEN, 0);
    }

    /**
     * Set selected home screen
     *
     * @param selectedHomeScreen selected home screen value
     */
    public static void setSelectedHomeScreen(int selectedHomeScreen) {
        putInt(SELECTED_HOME_SCREEN, selectedHomeScreen);
    }

    /**
     * get selected text size
     *
     * @return selected text size
     */
    public static int getSelectedTextSize() {
        return mSharedPreferences.getInt(SELECTED_TEXT_SIZE, 0);
    }

    /**
     * set selected text size
     *
     * @param selectedTextSize selected text size
     */
    public static void setSelectedTextSize(int selectedTextSize) {
        putInt(SELECTED_TEXT_SIZE, selectedTextSize);
    }


    /**
     * set incremented count
     *
     * @param key Key
     */
    public static void incrementViewCount(String key, boolean isShown) {
        // isShown will be true when pop up has been shown for session otherwise false
        if (isShown) {
            putInt("notification_" + key, -1);
        } else {
            // if already shown then don't increase the count
            if (getViewCount(key) >= 0) {
                putInt("notification_" + key, getViewCount(key) + 1);
            }
        }
    }

    /**
     * get incremented count
     *
     * @param key Key
     * @return Value of count
     */
    public static int getViewCount(String key) {
        return mSharedPreferences.getInt("notification_" + key, 0);
    }

    /**
     * is membership notification for month has been shown
     *
     * @return has been shown
     */
    static boolean hasMonthNotificationDisplayed() {
        return mSharedPreferences.getBoolean(HAS_MEMBERSHIP_MONTH_NOTIFICATION_DISPLAYED, false);
    }

    /**
     * set membership notification for month
     */
    static void setMonthNotificationDisplayed() {
        putBoolean(HAS_MEMBERSHIP_MONTH_NOTIFICATION_DISPLAYED, true);
    }

    /**
     * is expired membership notification for month has been shown
     *
     * @return has been shown
     */
    static String getExpiredNotificationLastDisplayed() {
        return mSharedPreferences.getString(MEMBERSHIP_EXPIRED_NOTIFICATION_LAST_DISPLAYED, null);
    }

    /**
     * set expired membership notification for month
     */
    public static void setExpiredNotificationLastDisplayed(String date) {
        putString(MEMBERSHIP_EXPIRED_NOTIFICATION_LAST_DISPLAYED, date);
    }

    /**
     * is fcm token send on server
     *
     * @return true/false
     */
    public static boolean isFCMTokenSendOnServer() {
        return mSharedPreferences.getBoolean(IS_FCM_TOKEN_SEND, false);
    }

    /**
     * mark fcm token send on server
     */
    public static void markFCMTokenSendOnServer() {
        putBoolean(IS_FCM_TOKEN_SEND, true);
    }
}
