package com.mapghana.app.model;

import java.util.ArrayList;

public class GroupGridSectionDataModel {

    private String headerTitle;
    private ArrayList<GroupData> allItemsInSection;


    public GroupGridSectionDataModel() {

    }
    public GroupGridSectionDataModel(String headerTitle, ArrayList<GroupData> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }



    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<GroupData> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<GroupData> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }


}