package com.mapghana.app.ui.sidemenu.organization.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.model.Category;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.iterface.SetOnCategoryClickListener;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

import java.util.ArrayList;

/**
 * Created by ubuntu on 3/1/18.
 */

public class TypeAdapter extends BaseRecycleAdapter implements Filterable{

    private ArrayList<Category.DataBean> categoryList;
    private ArrayList<Category.DataBean> filterList;
    private SetOnCategoryClickListener setOnCategoryClickListener;

    private CustomFilter filter;

    public TypeAdapter(SetOnCategoryClickListener setOnCategoryClickListener,
                       ArrayList<Category.DataBean> categoryList,
                       ArrayList<Category.DataBean> filterList
                       ) {
        this.categoryList = categoryList;
        this.filterList = filterList;
        this.setOnCategoryClickListener = setOnCategoryClickListener;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_types;
    }

    @Override
    protected int getItemSize() {
        if (categoryList!=null && categoryList.size()>0){
            return categoryList.size();
        }
        return 0;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    @Override
    public Filter getFilter() {
        if (filter==null){
            filter=new CustomFilter();
        }
        return filter;
    }

    class MyViewHolder extends ViewHolder{
        private AppCompatTextView tvTitle;

        private RecyclerView rvSubType;
        private int pos;
        private int visibility;
        private LinearLayout llButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle= itemView.findViewById(R.id.tvTitle);
            llButton = itemView.findViewById(R.id.llButton);
                rvSubType = itemView.findViewById(R.id.rvSubType);
                rvSubType.setLayoutManager(new LinearLayoutManager(getContext()));
        }

        @Override
        public void setData(int position) {
            pos=position;
            tvTitle.setText(categoryList.get(position).getName());
            llButton.setOnClickListener(this);
            llButton.setTag(position);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case  R.id.llButton:
                        updateRv((Integer) v.getTag());
                    break;
            }
        }

        private void updateRv(int position){
            if (visibility==0) {
                visibility = 1;
                rvSubType.setVisibility(View.VISIBLE);
                rvSubType.setAdapter(new SubTypeAdapter(setOnCategoryClickListener,
                        categoryList.get(position).getSub_category()));
            }
            else {
                visibility=0;
                rvSubType.setVisibility(View.GONE);
            }
        }
    }

    class CustomFilter extends Filter{
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults=new FilterResults();
            if (constraint!=null && constraint.toString().length()!=0){
                constraint=constraint.toString().toUpperCase();
                 ArrayList<Category.DataBean> filters=new ArrayList<>();
                 for (Category.DataBean dataBean:categoryList){
                     if (dataBean.getName().toUpperCase().contains(constraint)) {
                         filters.add(dataBean);
                     }
                 }

                 filterResults.count=filters.size();
                filterResults.values=filters;
            }
            else {
                filterResults.count=filterList.size();
                filterResults.values=filterList;
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            categoryList=(ArrayList<Category.DataBean>)results.values;
            notifyDataSetChanged();
        }
    }

}
