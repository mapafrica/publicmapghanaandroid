package com.mapghana.app.spf;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.mainactivity.MainActivity;

/**
 * Created by ubuntu on 5/1/18.
 */

public class SessionManager {

    private static final String Prefsname = "mapghana_user";
    private static final String USER_INFO_RESPONSE="user_info";
    private static final String KEY_REMEMBER_ME="remember_me";
    private static final String isLogin="isLogin";
    private static final String UNAME="uname";
    private static final String ROLE="role";
    private static final String ADDRESS="address";
    private static final String IMAGE="image";
    private static final String SEL_CITY="sel_city_id";
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    private SessionManager(){}

    private static void init(Context context){
        if (context==null)return;
        preferences=context.getSharedPreferences(Prefsname, Context.MODE_PRIVATE);
        editor=preferences.edit();
    }
    public static void setUserInfoResponse(Context context, String response,boolean rememberMe ){
       init(context);
       editor.putString(USER_INFO_RESPONSE, response);
       /*if (rememberMe) {
           editor.putBoolean(isLogin, true);
       }*/
       editor.putBoolean(isLogin, true);
       editor.putBoolean(KEY_REMEMBER_ME, rememberMe);
       editor.commit();
    }

    public static boolean isRememberUser(Context context){
        init(context);
       return preferences.getBoolean(KEY_REMEMBER_ME, false);
    }
    public static String getUserInfoResponse(Context context){
        init(context);
       return preferences.getString(USER_INFO_RESPONSE, "");
    }

    public static void checkLogin(Context context){
        init(context);
        if (isLoggedIn(context)){
            // user already is login
            Intent intent=new Intent(context, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
        else {
            // user is not login
            Intent intent=new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public static boolean isLoggedIn(Context context){
        init(context);
        return preferences.getBoolean(isLogin, false);
    }


    public static void clearUserData(Context context){
        init(context);
        editor.clear();
        editor.commit();
    }

    public static void logout(Context context){
        init(context);
        editor.clear();
        editor.commit();
        Intent intent=new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void saveUname(Context context,String uname){
        init(context);
        editor.putString(UNAME, uname);
        editor.commit();
    }

    public static String getUname(Context context){
        init(context);
        return preferences.getString(UNAME, "");
    }



    public static void saveRole(Context context,String role){
        init(context);
        editor.putString(ROLE, role);
        editor.commit();
    }

    public static String getRole(Context context){
        init(context);
        return preferences.getString(ROLE, "");
    }
    public static void saveAddress(Context context,String address){
        init(context);
        editor.putString(ADDRESS, address);
        editor.commit();
    }

    public static String getAddress(Context context){
        init(context);
        return preferences.getString(ADDRESS, "");
    }

    public static void saveProfileImage(Context context,String image){
        init(context);
        editor.putString(IMAGE, image);
        editor.commit();
    }

    public static String getProfileImage(Context context){
        init(context);
        return preferences.getString(IMAGE, "");
    }

    public static void saveSelectedCityId(Context context,String city_id){
        init(context);
        editor.putString(SEL_CITY, city_id);
        editor.commit();
    }

    public static String getSelectedCityId(Context context){
        init(context);
        return preferences.getString(SEL_CITY, "");
    }


}
