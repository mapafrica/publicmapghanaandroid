package com.mapghana.app.ui.activity.dashboard.dashboard.add_location;


import android.content.Context;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.mapbox.services.android.ui.geocoder.GeocoderAutoCompleteView;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.services.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.services.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.services.commons.models.Position;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.dialog.MarkerDialog;
import com.mapghana.app.interfaces.EventMapListener;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.model.Session;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingIndividualsFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingOrganizationFragment;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.Valiations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddLocationFragment extends AppBaseFragment
        implements OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener, GeocoderAutoCompleteView.OnFeatureListener, LocationServiceListner, MapboxMap.OnCameraIdleListener {

    private GeocoderAutoCompleteView geocoderAutoCompleteView;
    private String type;
    private String title = "Your Selected Location";
    private String locationAddress;
    private AppCompatTextView tvLoc;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private int zoomLevel = 13;
    private ImageView imgBack;
    private ImageView iv_close;
    private MapView mapView;
    private MapboxMap map;
    private PermissionsManager permissionsManager;
    public LocationComponent locationComponent;
    private DirectionsRoute currentRoute;
    private static final String TAG = "DirectionsActivity";
    private NavigationMapRoute navigationMapRoute;
    private LinearLayout ll_instruction;
    private EventMapListener eventMapListener;
    private ImageView submitButton;
    private Point originPoint;
    private ImageView dropPinView;
    private Session session;
    private ImageButton mImgBtnCurrentLocation;
    private boolean mOnMapClickOff = true;


    public void setType(String type) {
        this.type = type;
    }


    public void setClickOnMapOff(boolean value) {
    this.mOnMapClickOff = value;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_location;
    }


    public void setAddLocationListenerType(EventMapListener eventMapListener, String type, LatLng latLng) {
    this.type = type;
    this.eventMapListener = eventMapListener;
    this.latitude = latLng.getLatitude();
    this.longitude = latLng.getLongitude();
    }

    @Override
    public void initializeComponent() {
        init();
        initViewById(getView());
        session = AppPreferences.getSession();
    }

    private void initViewById(View rootView) {

        mapView = rootView.findViewById(R.id.mapView);
        mapView.getMapAsync(this);

        ImageView imgSearch = rootView.findViewById(R.id.imgSearch);
        submitButton = rootView.findViewById(R.id.tvDone);
        tvLoc = rootView.findViewById(R.id.tvLoc);
        mImgBtnCurrentLocation  = rootView.findViewById(R.id.imgMyLoc);
        imgBack = rootView.findViewById(R.id.imgBack);
        iv_close = rootView.findViewById(R.id.iv_close);
        ll_instruction = rootView.findViewById(R.id.ll_instruction);
        geocoderAutoCompleteView = rootView.findViewById(R.id.autoCompleteWidget);
        dropPinView = getView().findViewById(R.id.dropPinView);
        geocoderAutoCompleteView.setAccessToken(getString(R.string.mapbox_api_token));
        geocoderAutoCompleteView.setType(GeocodingCriteria.TYPE_POI);
        geocoderAutoCompleteView.setOnFeatureListener(this);
        imgSearch.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        iv_close.setOnClickListener(this);
        submitButton.setEnabled(false);
        dropPinView.setOnClickListener(this);

        mImgBtnCurrentLocation.setOnClickListener(this);
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setNavTitle(getString(R.string.Location));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setExploreVisibility(false);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSearch:
                geoCoding();
                break;
            case R.id.tvDone:
                try {
                    if (!TextUtils.isEmpty(session.getUsername())) {
                        changeFragment();
                    } else {
                        getDashboardActivity().showLoginMessageDialog();
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
            case R.id.iv_close:
                ll_instruction.setVisibility(View.GONE);
                break;
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
        }
    }

    private void setMarkersOnMapAgain() {
        map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originPoint.latitude(), originPoint.longitude()), 16));
    }


    private void changeFragment() {
        if(longitude == 0.0 && latitude == 0.0) {
            Toast.makeText(getActivity(),"Something went wrong", Toast.LENGTH_SHORT).show();
            Utils.showToast(getActivity(), getView(), "Something went wrong");
            return;
        }
        if (type.equalsIgnoreCase(Constants.ORGANISATION)) {
            EventInfoSection eventInfoSection = new EventInfoSection("", "", String.valueOf(latitude), String.valueOf(longitude), locationAddress);
            ((AppBaseActivity) getActivity()).changeFragment(PostListingOrganizationFragment.newInstance(Constants.ORGANISATION, eventInfoSection), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } else if (type.equalsIgnoreCase(Constants.INDIVIDUALS)) {
            EventInfoSection eventInfoSection = new EventInfoSection("", "", String.valueOf(latitude), String.valueOf(longitude), locationAddress);

            ((AppBaseActivity) getActivity()).changeFragment(PostListingIndividualsFragment.newInstance(Constants.INDIVIDUALS, eventInfoSection), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } else if (type.equalsIgnoreCase(Constants.ADDEVENTS)) {
            eventMapListener.onMapIconClick(new EventInfoSection("", "", String.valueOf(latitude), String.valueOf(longitude), locationAddress));
                getActivity().onBackPressed();
        }
    }

    @Override
    public void viewCreateFromBackStack() {
        super.viewCreateFromBackStack();
        init();
    }


    private void geoCoding() {
        if (Valiations.hasText(geocoderAutoCompleteView)) {
            try {
                final String palceName = geocoderAutoCompleteView.getText().toString().trim();
                MapboxGeocoding mapboxGeocoding = new MapboxGeocoding.Builder()
                        .setAccessToken(getString(R.string.mapbox_api_token))
                        .setLocation(palceName)
                        .build();
                displayProgressBar(false);
                mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                    @Override
                    public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                        dismissProgressBar();
                        GeocodingResponse body = response.body();
                        if (body != null) {
                            if (response.isSuccessful()) {
                                List<CarmenFeature> results = body.getFeatures();
                                if (results.size() > 0) {
                                    Position firstResultPos = results.get(0).asPosition();
                                    LatLng latLng = new LatLng(firstResultPos.getLatitude(), firstResultPos.getLongitude());
                                    title = palceName;
                                } else {
                                    Utils.showToast(getActivity(), getView(), "No result found...");
                                }
                            } else {

                                displayErrorDialog("Error", response.message());
                            }
                        } else {
                            Utils.showToast(getActivity(), getView(), "No result found...");
                        }


                    }

                    @Override
                    public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                        throwable.printStackTrace();
                        dismissProgressBar();
                        displayErrorDialog("Error", throwable.getMessage());
                    }
                });


            } catch (ServicesException e) {
                e.printStackTrace();
                Utils.showToast(getActivity(), getView(), "Something went wrong...");
            }
        }
    }

    private void setAddress(String address) {
        tvLoc.setText(address);
    }

    private void updateCurrentAddress(String address) {
        geocoderAutoCompleteView.setOnFeatureListener(null);
        geocoderAutoCompleteView.setText(address);
        geocoderAutoCompleteView.setOnFeatureListener(this);

    }

    public  void getAddressFromLocation(final double latitude, final double longitude, Context context) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String result = null;

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if ((addresses != null) && (addresses.size() == 1)) {
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            locationAddress = address;
            setAddress(address);
        }
    }

    @Override
    public void onFeatureClick(CarmenFeature feature) {

        Position position = feature.asPosition();
        LatLng latLng = new LatLng(position.getLatitude(), position.getLongitude());
        latitude = latLng.getLatitude();
        longitude = latLng.getLongitude();
        submitButton.setEnabled(true);
        submitButton.setBackgroundResource(R.color.colorGreen);
        locationAddress = geocoderAutoCompleteView.getText().toString().trim();
        Location location = Locationservice.sharedInstance().getLastLocation();
        tvLoc.setText(locationAddress);
//        if (latLng != null) {
//            Point currentPoint = Point.fromLngLat(location.getLongitude(),location.getLatitude());
//            Point destinationPoint = Point.fromLngLat(latLng.getLatitude(), latLng.getLatitude());
//            GeoJsonSource source = map.getStyle().getSourceAs("destination-source-id");
//            if (source != null) {
//                source.setGeoJson(Feature.fromGeometry(destinationPoint));
//            }
//
//            if(location != null) {
//                getRoute(currentPoint, destinationPoint);
//            }
//
//        }

        map.easeCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));

    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.map = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            enableLocationComponent(style);

            addDestinationIconSymbolLayer(style);

            mapboxMap.addOnMapClickListener(AddLocationFragment.this);

            mapboxMap.addOnCameraIdleListener(this);
//            if(longitude != 0.0 && longitude != 0.0) {
//                Point destinationPoint = Point.fromLngLat(longitude, latitude);
//                GeoJsonSource source = map.getStyle().getSourceAs("destination-source-id");
//                if (source != null) {
//                    source.setGeoJson(Feature.fromGeometry(destinationPoint));
//                }
//            }

            submitButton.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(session.getUsername())) {
                    changeFragment();
                } else {
                    try {
                        getDashboardActivity().showLoginMessageDialog();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            });

            Location location = Locationservice.sharedInstance().getLastLocation();
            if (location != null) {
                originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
                map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originPoint.latitude(),originPoint.longitude()),12));

            }
        });


    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public boolean onMapClick(@NonNull LatLng point) {

        if(mOnMapClickOff) {
            Point destinationPoint = Point.fromLngLat(point.getLongitude(), point.getLatitude());
            originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                    locationComponent.getLastKnownLocation().getLatitude());

            latitude = point.getLatitude();
            longitude = point.getLongitude();
            GeoJsonSource source = map.getStyle().getSourceAs("destination-source-id");
            if (source != null) {
                source.setGeoJson(Feature.fromGeometry(destinationPoint));
            }

            displayProgressBar(false);
            getRoute(originPoint, destinationPoint);
            submitButton.setEnabled(true);
            submitButton.setBackgroundResource(R.color.colorPrimary);
            getAddressFromLocation(point.getLatitude(),point.getLongitude(),getActivity());
        }

        return true;
    }

    @Override
    public void onCameraIdle() {
        CameraPosition cameraPosition = map.getCameraPosition();
        LatLng latLng = cameraPosition.target;
        if (latLng != null) {
            title = "Your Selected Location";
            geocoderAutoCompleteView.setText("");
            latitude = latLng.getLatitude();
            longitude = latLng.getLongitude();
            setAddress("Location Picked: " + latLng.getLatitude() + ", " + latLng.getLongitude());
            getAddressFromLocation(latLng.getLatitude(), latLng.getLongitude(), getActivity());
            submitButton.setEnabled(true);
            submitButton.setBackgroundResource(R.color.colorPrimary);
        }


    }

    private void getRoute(Point origin, Point destination) {
        NavigationRoute.builder(getActivity())
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        dismissProgressBar();
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }

                        currentRoute = response.body().routes().get(0);

                        if (navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, map, R.style.NavigationMapRoute);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        displayProgressBar(false);
                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = map.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), "User location permission explanation", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(map.getStyle());
        } else {
            Toast.makeText(getActivity(), "User location permission not granted", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        session = AppPreferences.getSession();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void userLocationChange(Location location) {

    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }


}
