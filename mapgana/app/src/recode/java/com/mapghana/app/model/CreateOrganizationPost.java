package com.mapghana.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ubuntu on 24/1/18.
 */

public class CreateOrganizationPost {


    /**
     * status : 1
     * code : 200
     * data : {"id":50,"name":"dsd","user_id":11,"city_id":1,"post_for":"organizations","category_id":1,"sub_category_id":1,"email":"abc@gail.com","phone":["7894561235","1236598426"],"website":"http://localhost/mapgh/gii/controller","twitter":"@dfdsfads","dob":null,"gender":"male","image":null,"features":["dfgdfsg","123"],"type":"type","status":"unverified","opening_hours":"{\"monday-friday\":{\"start\":\"6 am\",\"end\":\"7 pm\"},\"saturday\":{\"start\":\"6 am\",\"end\":\"7 pm\"},\"sunday\":{\"start\":\"6 am\",\"end\":\"7 pm\"}}","notes":"notes","tags":"tags","lat":"2343564","log":"4365465446","deleted_at":null,"created_at":"2018-01-24 11:08:49","updated_at":"2018-01-24 11:08:49","user":{"id":11,"name":"kk1","username":"kk1","email":"kk@gmail.com","dob":"23-01-2018","gender":"male","phone_number":"9632580741","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516684434.jpg","lat":null,"long":null,"device_id":"4b5d39a9c20132f7","device_type":"A","location":"xff","address":null,"about":null,"role":"user","status":"0","deleted_at":null,"created_at":"2018-01-23 05:03:30","updated_at":"2018-01-23 05:13:54"},"postGallery":[{"id":102,"post_id":50,"image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516792129343.jpg","deleted_at":null,"created_at":"2018-01-24 11:08:49","updated_at":"2018-01-24 11:08:49"},{"id":103,"post_id":50,"image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516792129849.jpg","deleted_at":null,"created_at":"2018-01-24 11:08:49","updated_at":"2018-01-24 11:08:49"},{"id":104,"post_id":50,"image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/151679212946.jpg","deleted_at":null,"created_at":"2018-01-24 11:08:49","updated_at":"2018-01-24 11:08:49"}],"category":null,"postReview":[]}
     * message : successfully
     * error :
     */

    private int status;
    private int code;
    private DataBean data;
    private String message;
    private String error;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static class DataBean {
        /**
         * id : 50
         * name : dsd
         * user_id : 11
         * city_id : 1
         * post_for : organizations
         * category_id : 1
         * sub_category_id : 1
         * email : abc@gail.com
         * phone : ["7894561235","1236598426"]
         * website : http://localhost/mapgh/gii/controller
         * twitter : @dfdsfads
         * dob : null
         * gender : male
         * image : null
         * features : ["dfgdfsg","123"]
         * type : type
         * status : unverified
         * opening_hours : {"monday-friday":{"start":"6 am","end":"7 pm"},"saturday":{"start":"6 am","end":"7 pm"},"sunday":{"start":"6 am","end":"7 pm"}}
         * notes : notes
         * tags : tags
         * lat : 2343564
         * log : 4365465446
         * deleted_at : null
         * created_at : 2018-01-24 11:08:49
         * updated_at : 2018-01-24 11:08:49
         * user : {"id":11,"name":"kk1","username":"kk1","email":"kk@gmail.com","dob":"23-01-2018","gender":"male","phone_number":"9632580741","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516684434.jpg","lat":null,"long":null,"device_id":"4b5d39a9c20132f7","device_type":"A","location":"xff","address":null,"about":null,"role":"user","status":"0","deleted_at":null,"created_at":"2018-01-23 05:03:30","updated_at":"2018-01-23 05:13:54"}
         * postGallery : [{"id":102,"post_id":50,"image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516792129343.jpg","deleted_at":null,"created_at":"2018-01-24 11:08:49","updated_at":"2018-01-24 11:08:49"},{"id":103,"post_id":50,"image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516792129849.jpg","deleted_at":null,"created_at":"2018-01-24 11:08:49","updated_at":"2018-01-24 11:08:49"},{"id":104,"post_id":50,"image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/151679212946.jpg","deleted_at":null,"created_at":"2018-01-24 11:08:49","updated_at":"2018-01-24 11:08:49"}]
         * category : null
         * postReview : []
         */

        private int id;
        private String name;
        private int user_id;
        private int city_id;
        private String post_for;
        private int category_id;
        private int sub_category_id;
        private String email;
        private String website;
        private String twitter;
        private Object dob;
        private String gender;
        private Object image;
        private String type;
        private String status;
        private String opening_hours;
        private String notes;
        private String tags;
        private String lat;
        private String log;
        private Object deleted_at;
        private String created_at;
        private String updated_at;
        private UserBean user;
        private Object category;
        private List<String> phone;
        private List<String> features;
        private List<PostGalleryBean> postGallery;
        private List<?> postReview;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }

        public String getPost_for() {
            return post_for;
        }

        public void setPost_for(String post_for) {
            this.post_for = post_for;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(int sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public Object getDob() {
            return dob;
        }

        public void setDob(Object dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOpening_hours() {
            return opening_hours;
        }

        public void setOpening_hours(String opening_hours) {
            this.opening_hours = opening_hours;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public Object getCategory() {
            return category;
        }

        public void setCategory(Object category) {
            this.category = category;
        }

        public List<String> getPhone() {
            return phone;
        }

        public void setPhone(List<String> phone) {
            this.phone = phone;
        }

        public List<String> getFeatures() {
            return features;
        }

        public void setFeatures(List<String> features) {
            this.features = features;
        }

        public List<PostGalleryBean> getPostGallery() {
            return postGallery;
        }

        public void setPostGallery(List<PostGalleryBean> postGallery) {
            this.postGallery = postGallery;
        }

        public List<?> getPostReview() {
            return postReview;
        }

        public void setPostReview(List<?> postReview) {
            this.postReview = postReview;
        }

        public static class UserBean {
            /**
             * id : 11
             * name : kk1
             * username : kk1
             * email : kk@gmail.com
             * dob : 23-01-2018
             * gender : male
             * phone_number : 9632580741
             * image : http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516684434.jpg
             * lat : null
             * long : null
             * device_id : 4b5d39a9c20132f7
             * device_type : A
             * location : xff
             * address : null
             * about : null
             * role : user
             * status : 0
             * deleted_at : null
             * created_at : 2018-01-23 05:03:30
             * updated_at : 2018-01-23 05:13:54
             */

            private int id;
            private String name;
            private String username;
            private String email;
            private String dob;
            private String gender;
            private String phone_number;
            private String image;
            private Object lat;
            @SerializedName("long")
            private Object longX;
            private String device_id;
            private String device_type;
            private String location;
            private Object address;
            private Object about;
            private String role;
            private String status;
            private Object deleted_at;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getPhone_number() {
                return phone_number;
            }

            public void setPhone_number(String phone_number) {
                this.phone_number = phone_number;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Object getLat() {
                return lat;
            }

            public void setLat(Object lat) {
                this.lat = lat;
            }

            public Object getLongX() {
                return longX;
            }

            public void setLongX(Object longX) {
                this.longX = longX;
            }

            public String getDevice_id() {
                return device_id;
            }

            public void setDevice_id(String device_id) {
                this.device_id = device_id;
            }

            public String getDevice_type() {
                return device_type;
            }

            public void setDevice_type(String device_type) {
                this.device_type = device_type;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public Object getAddress() {
                return address;
            }

            public void setAddress(Object address) {
                this.address = address;
            }

            public Object getAbout() {
                return about;
            }

            public void setAbout(Object about) {
                this.about = about;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class PostGalleryBean {
            /**
             * id : 102
             * post_id : 50
             * image : http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516792129343.jpg
             * deleted_at : null
             * created_at : 2018-01-24 11:08:49
             * updated_at : 2018-01-24 11:08:49
             */

            private int id;
            private int post_id;
            private String image;
            private Object deleted_at;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getPost_id() {
                return post_id;
            }

            public void setPost_id(int post_id) {
                this.post_id = post_id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
