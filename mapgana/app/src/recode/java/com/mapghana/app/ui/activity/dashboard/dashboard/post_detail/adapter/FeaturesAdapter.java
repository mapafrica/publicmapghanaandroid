package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

import java.util.List;

/**
 * Created by ubuntu on 30/12/17.
 */

public class FeaturesAdapter extends BaseRecycleAdapter {

    private List<String> features;

    public FeaturesAdapter(List<String> features) {
        this.features = features;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_features;
    }

    @Override
    protected int getItemSize() {
        if (features!=null && features.size()>0) {
            return features.size();
        }
        else {
            return 0;
        }
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder{
        private AppCompatTextView tvNameLeft;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvNameLeft= itemView.findViewById(R.id.tvNameLeft);
        }

        @Override
        public void setData(int position) {
            tvNameLeft.setText(features.get(position));
        }

    }
}
