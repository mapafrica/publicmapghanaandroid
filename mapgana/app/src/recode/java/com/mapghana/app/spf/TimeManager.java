package com.mapghana.app.spf;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ubuntu on 20/3/18.
 */

public class TimeManager {

    private static String last_time="last_time";
    private static String first="first";
    private static final String prefName="time_pref_mg";
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    private static void init(Context context){
        preferences=context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor=preferences.edit();
    }

    public static void saveLastTime(Context context, long time){
        init(context);
        editor.putLong(last_time, time);
        editor.commit();
    }

    public static long getLastTime(Context context){
        init(context);
        return preferences.getLong(last_time, 0);
    }

    public static void saveFirst(Context context){
        init(context);
        editor.putBoolean(first, false);
        editor.commit();
    }

    public static boolean isFirst(Context context){
        init(context);
        return preferences.getBoolean(first, true);
    }

    public static void clearTime(Context context){
        init(context);
        editor.clear();
        editor.commit();
    }
}
