package com.mapghana.app.model;

import java.io.Serializable;

public class Success implements Serializable {

    private int status;
    private String message;
    private String error;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }
}
