package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class EventListingByDate implements Serializable {

    private int status;
    private String message;
    private EventListingByDateContent data;
    private String error;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public EventListingByDateContent getData() {
        return data;
    }

    public String getError() {
        return error;
    }

    public class EventListingByDateContent implements Serializable {
        private int current_page;
        private ArrayList<EventDetail> data;
        private String from;
        private int last_page;
        private String next_page_url;
        private String path;
        private int per_page;
        private String prev_page_url;
        private String to;
        private int total;

        public int getCurrent_page() {
            return current_page;
        }

        public ArrayList<EventDetail> getData() {
            return data;
        }

        public String getFrom() {
            return from;
        }

        public int getLast_page() {
            return last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public String getPath() {
            return path;
        }

        public int getPer_page() {
            return per_page;
        }

        public String getPrev_page_url() {
            return prev_page_url;
        }

        public String getTo() {
            return to;
        }

        public int getTotal() {
            return total;
        }
    }
}
