package com.mapghana.app.model;

import java.io.Serializable;

public class DeeplinkingData implements Serializable {

    private String event_id;
    private String type;

    public String getEvent_id() {
        return event_id;
    }

    public String getType() {
        return type;
    }
}
