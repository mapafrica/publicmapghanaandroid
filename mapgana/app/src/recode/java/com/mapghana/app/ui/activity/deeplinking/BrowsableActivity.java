package com.mapghana.app.ui.activity.deeplinking;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.mapghana.app.ui.activity.splash.SplashActivity;
import com.mapghana.app.utils.Constants;

public class BrowsableActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getData() != null) {
            Uri data = getIntent().getData();
            String id = data.getQueryParameter("id");

            if (!TextUtils.isEmpty(id)) {
                Intent intent = new Intent(BrowsableActivity.this, SplashActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("url", data.toString());
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        }
    }


}
