package com.mapghana.app.model;

import java.io.Serializable;

public class ObjectListingContent implements Serializable {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}