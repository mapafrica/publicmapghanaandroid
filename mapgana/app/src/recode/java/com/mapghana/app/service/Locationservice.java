package com.mapghana.app.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.mapghana.app.utils.MapGhanaApplication;


public class Locationservice extends Service implements
        OnConnectionFailedListener, ConnectionCallbacks, LocationListener {
    public static String BROADCAST_REQUEST_LOCATION_PERMISSION = "BROADCAST_REQUEST_LOCATION_PERMISSION";

    public static GoogleApiClient mGoogleApiClient;
    public Location mLastLocation;
    MapGhanaApplication myApplication;
    private LocationRequest mLocationRequest;
    private boolean mRequestingLocationUpdates = false;
    private static Locationservice mSharedInstance;
    public static Locationservice sharedInstance() {

        if (mSharedInstance == null) {
            synchronized (Locationservice.class) {
                if (mSharedInstance == null) {
                    mSharedInstance = new Locationservice();
                }
            }
        }
        return mSharedInstance;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        //  Utils.printLog("my service onStartCommand");
        buildGoogleApiClient();
        mRequestingLocationUpdates = false;
        myApplication = (MapGhanaApplication) this.getApplication();

        return super.onStartCommand(intent, flags, startId);


    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        //  Utils.printLog("my service GoogleApiclient onConnected");
        createLocationRequest();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        startLocationUpdates();
        if (myApplication.getLocationServiceListner() != null) {
            myApplication.getLocationServiceListner().googleApiclientConnecte(mGoogleApiClient, mLocationRequest);
            myApplication.getLocationServiceListner().userLocationChange(mLastLocation);
        }


    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        mGoogleApiClient.connect();
        //   Utils.printLog("my service GoogleApiclient onConnectionSuspended");

    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub
        //  Utils.printLog("my service GoogleApiclient onConnectionFailed");
    }

    @Override
    public void onLocationChanged(Location updatelocation) {
        // TODO Auto-generated method stub
        if (updatelocation != null) {

            mLastLocation = updatelocation;
            if(myApplication!=null){
                myApplication.mLocation=updatelocation;
            }
            if (myApplication.getLocationServiceListner() != null) {
                myApplication.getLocationServiceListner().userLocationChange(updatelocation);
            }
        }

    }

    public Location getLastLocation() {
        if (ActivityCompat.checkSelfPermission(MapGhanaApplication.sharedInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapGhanaApplication.sharedInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // permission has not been granted.
            requestLocationPermission();
        } else if (mGoogleApiClient.isConnected()) {
            return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

        return null;
    }

    private void requestLocationPermission() {
        //send the request location permission broadcast
        Intent intent = new Intent(BROADCAST_REQUEST_LOCATION_PERMISSION);
        LocalBroadcastManager.getInstance(MapGhanaApplication.sharedInstance()).sendBroadcast(intent);
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).addApi(Places.GEO_DATA_API)
                .build();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);

        // mLocationRequest.setSmallestDisplacement(1.0f);// with this setSmallestDisplacement(), it will check that after each 5 seconds that if distance is 5 meter then it will give you updated location otherwise it will not gice
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (!mRequestingLocationUpdates && mGoogleApiClient != null) {
            mRequestingLocationUpdates = true;
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

            /* Process: brsoftech.com.smartwatchpolice, PID: 31903
         java.lang.IllegalStateException: GoogleApiClient is not connected yet.
                    at com.google.android.gms.internal.zzqn.zzd(Unknown Source)
            at com.google.android.gms.internal.zzqr.zzd(Unknown Source)
            at com.google.android.gms.internal.zzqp.zzd(Unknown Source)
            at com.google.android.gms.location.internal.zzd.requestLocationUpdates(Unknown Source)
            at brsoftech.com.smartwatchpolice.user.services.Locationservice.startLocationUpdates(Locationservice.java:134)
            at brsoftech.com.smartwatchpolice.user.services.Locationservice.onConnected(Locationservice.java:67)
            at com.google.android.gms.common.internal.zzm.zzp(Unknown Source)
            at com.google.android.gms.internal.zzqp.zzn(Unknown Source)
            at com.google.android.gms.internal.zzqn.zzarm(Unknown Source)
            at com.google.android.gms.internal.zzqn.onConnected(Unknown Source)
            at com.google.android.gms.internal.zzqr.onConnected(Unknown Source)
            at com.google.android.gms.internal.zzqf.onConnected(Unknown Source)
            at com.google.android.gms.common.internal.zzl$1.onConnected(Unknown Source)
            at com.google.android.gms.common.internal.zze$zzj.zzaua(Unknown Source)
            at com.google.android.gms.common.internal.zze$zza.zzc(Unknown Source)
            at com.google.android.gms.common.internal.zze$zza.zzv(Unknown Source)
            at com.google.android.gms.common.internal.zze$zze.zzauc(Unknown Source)
            at com.google.android.gms.common.internal.zze$zzd.handleMessage(Unknown Source)
            at android.os.Handler.dispatchMessage(Handler.java:111)
            at android.os.Looper.loop(Looper.java:207)
            at android.app.ActivityThread.main(ActivityThread.java:5728)
            at java.lang.reflect.Method.invoke(Native Method)
            at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:789)
            at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:679)*/

try {
    LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient, mLocationRequest, this);
}
catch (Exception e)
{
    e.printStackTrace();
}

        }
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()
                && mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        stopLocationUpdates();
        super.onDestroy();
    }


}
