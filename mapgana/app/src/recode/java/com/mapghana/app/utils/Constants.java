package com.mapghana.app.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 6/1/18.
 */

public class Constants {
    public static String date_format = "dd-MM-yyyy";

    public static String device_type = "A";
    public static String No_Internet = "No Internet Connection";
    public static String individuals = "individuals";
    public static String events = "events";
    public static String objects = "objects";
    public static String All = "All";
    public static String organizations = "organizations";
    public static String sub_category_id = "sub_category_id";
    public static String Select_City = "Select Regions";
    public static String type = "type";
    public static String id = "id";
    public static String data = "data";

    /* Push Notification Keys */
    public static String IS_NOTIFICATION = "is_notification";
    public static String NOTIFICATION_KEY = "key";
    public static String NOTIFICATION_TITLE = "title";
    public static String URL = "url";
    public static String API_TYPE = "api_type";

    public static String Destination = "Destination";
    public static String You = "You";
    public static String verified = "verified";
    public static String unverified = "unverified";
    public static String anonymous = "anonymous";
    public static String male = "male";
    public static String female = "female";

    private static List<String> imageList;
    private static List<String> videoList;

    public static String ADDEVENTS = "addEvents";
    public static String ITEMS = "items";
    public static String TITLE = "title";
    public static String ORGANISATION = "organisation";
    public static String SEARCH = "search";
    public static String OBJECT = "object";
    public static String INDIVIDUALS = "individuals";
    public static String SOMETHING_WENT_WRONG = "Something went wrong";
    public static String LOCAL_BROADCAST_CALENDAR = "LOCAL_BROADCAST_CALENDAR";
    public static String LOCAL_BROADCAST_SIGNIN = "LOCAL_BROADCAST_SIGNIN";
    public static String LOCAL_BROADCAST_SIGNOUT = "LOCAL_BROADCAST_SIGNOUT";
    public static String LOCAL_BROADCAST_EVENT_SEARCH = "LOCAL_BROADCAST_EVENT_SEARCH";
    public static String LOCAL_BROADCAST_AtoZ_GROUP = "LOCAL_BROADCAST_AtoZ_GROUP";
    public static String LOCAL_BROADCAST_MULTIVIEW= "LOCAL_BROADCAST_MULTIVIEW";
    public static String LOCAL_BROADCAST_LIST= "LOCAL_BROADCAST_LIST";
    public static String LOCAL_BROADCAST_GRID= "LOCAL_BROADCAST_GRID";

    public static String LOCAL_BROADCAST_PAYMENT= "LOCAL_BROADCAST_PAYMENT";
    public static String LOCAL_BROADCAST_PAYMENT_SUCCESS= "LOCAL_BROADCAST_PAYMENT_SUCCESS";
    public static String LOCAL_BROADCAST_PAYMENT_ERROR= "LOCAL_BROADCAST_PAYMENT_ERROR";
    public static String LOCAL_BROADCAST_PAYMENT_CANCELLED= "LOCAL_BROADCAST_PAYMENT_CANCELLED";

    public static final int MIN_BUFFER_DURATION = 3000;
    //Max Video you want to buffer during PlayBack
    public static final int MAX_BUFFER_DURATION = 5000;
    //Min Video you want to buffer before start Playing it
    public static final int MIN_PLAYBACK_START_BUFFER = 1500;
    //Min video You want to buffer when user resumes video
    public static final int MIN_PLAYBACK_RESUME_BUFFER = 5000;

    public static String LOCAL_BROADCAST_EVENT_LOCATION = "LOCAL_BROADCAST_EVENT_LOCATION";


    public static final String country = "NG";
    public static final String currency = "NGN";
    public static final String publicKey = "FLWPUBK_TEST-29ca751a33a62d117e04ba7a08b86278-X"; //Get your public key from your account
    public static final String encryptionKey = "FLWSECK_TEST0b9abf9cadc8"; //Get your encryption key from your account

    private static void initilaizeImageList() {
        imageList = new ArrayList<>();
        imageList.add("image/jpeg");
        imageList.add("image/png");
        imageList.add("image/gif");
        imageList.add("image/x-ms-bmp");
        imageList.add("image/webp");
    }

    public static List<String> getImageMimeList() {
        initilaizeImageList();
        return imageList;
    }

    private static void initilaizeVideoList() {
        videoList = new ArrayList<>();

        videoList.add("video/mpeg");
        videoList.add("video/mp4");
        videoList.add("video/quicktime");
        videoList.add("video/3gpp");
        videoList.add("video/3gpp2");
        videoList.add("video/x-matroska");
        videoList.add("video/webm");
        videoList.add("video/mp2ts");
        videoList.add("video/avi");
    }

    public static List<String> getVideoMimeList() {
        initilaizeVideoList();
        return videoList;
    }


}
