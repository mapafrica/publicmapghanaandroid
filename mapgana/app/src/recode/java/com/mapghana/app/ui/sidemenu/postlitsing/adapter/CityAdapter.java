package com.mapghana.app.ui.sidemenu.postlitsing.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.model.City;
import com.mapghana.customviews.TypefaceTextView;

import java.util.ArrayList;

/**
 * Created by ubuntu on 23/1/18.
 */

public class CityAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<City.DataBean> cityList;
    private City.DataBean selectedCity;

    private int textColor=R.color.colorDarkGray;
    private int viewBackgroundColor=R.color.colorWhite;

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public void setViewBackgroundColor(int viewBackgroundColor) {
        this.viewBackgroundColor = viewBackgroundColor;
    }

    public City.DataBean getDataBean() {
        return selectedCity;
    }

    public void setDataBean(int selectedPosition) {
        this.selectedCity =cityList.get(selectedPosition);
        notifyDataSetChanged();
    }

    public CityAdapter(Context context, ArrayList<City.DataBean> cityList) {
        this.context = context;
        this.cityList = cityList;
    }

    @Override
    public int getCount() {
        if (cityList!=null && cityList.size()>0){
            return cityList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.item_simple_spinner, parent, false);
        }
        LinearLayout ll_view=convertView.findViewById(R.id.ll_view);
        ll_view.setBackgroundColor(context.getResources().getColor(viewBackgroundColor));
        AppCompatTextView tvTitle= convertView.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(context.getResources().getColor(textColor));
        tvTitle.setText(cityList.get(position).getName());
        ImageView imgDown=convertView.findViewById(R.id.imgDown);
        imgDown.setColorFilter(context.getResources().getColor(textColor));
        if (selectedCity!=null){
            if (cityList.get(position).equals(selectedCity)){
                imgDown.setVisibility(View.VISIBLE);
            }else {
                imgDown.setVisibility(View.GONE);
            }
        }
        else {
            imgDown.setVisibility(View.GONE);
        }
        return convertView;
    }

}