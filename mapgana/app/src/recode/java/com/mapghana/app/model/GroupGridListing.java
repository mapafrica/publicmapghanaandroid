package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupGridListing implements Serializable {

    private int status;
    private String image_path;
    private String message;
    private ArrayList<GroupListData> data;
    private String error;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<GroupListData> getData() {
        return data;
    }

    public String getError() {
        return error;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_pathe) {
        this.image_path = image_pathe;
    }

}

