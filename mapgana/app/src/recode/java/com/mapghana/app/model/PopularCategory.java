package com.mapghana.app.model;

import java.util.List;

public class PopularCategory  {

    private int code;
    private String error;
    private int status;
    private String message;
    private List<PopulatCategoryModel> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PopulatCategoryModel> getData() {
        return data;
    }

    public void setData(List<PopulatCategoryModel> data) {
        this.data = data;
    }

    public static class PopulatCategoryModel {
        /**
         * id : 33
         * name : MECHANIC
         * image : http://mapghana.com/images/404.jpeg
         * type : individuals
         */

        private int id;
        private String name;
        private String image;
        private String type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
