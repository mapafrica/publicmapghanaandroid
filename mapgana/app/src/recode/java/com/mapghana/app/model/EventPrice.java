package com.mapghana.app.model;

public class EventPrice {

    private int map_navigation;
    private int event_coverae;
    private String mapghana_contribution;
    private int paid_event;

    public int getMap_navigation() {
        return map_navigation;
    }

    public void setMap_navigation(int map_navigation) {
        this.map_navigation = map_navigation;
    }

    public int getEvent_coverae() {
        return event_coverae;
    }

    public void setEvent_coverae(int event_coverae) {
        this.event_coverae = event_coverae;
    }

    public String getMapghana_contribution() {
        return mapghana_contribution;
    }

    public void setMapghana_contribution(String mapghana_contribution) {
        this.mapghana_contribution = mapghana_contribution;
    }

    public int getPaid_event() {
        return paid_event;
    }

    public void setPaid_event(int paid_event) {
        this.paid_event = paid_event;
    }
}
