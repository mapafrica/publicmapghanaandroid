package com.mapghana.app.model;

/**
 * Created by ubuntu on 6/1/18.
 */

public class Signup {


    /**
     * status : 1
     * message : successfully  register
     * data : {"name":"b2","email":"b2@mail.com","username":"b2","phone_number":"7777777777","dob":"08-01-2018","gender":"female","location":"jaipur","device_type":"A","device_id":"121212","updated_at":"2018-01-12 04:47:03","created_at":"2018-01-12 04:47:03","id":5}
     * error :
     */

    private int status;
    private String message;
    private DataBean data;
    private String error;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static class DataBean {
        /**
         * name : b2
         * email : b2@mail.com
         * username : b2
         * phone_number : 7777777777
         * dob : 08-01-2018
         * gender : female
         * location : jaipur
         * device_type : A
         * device_id : 121212
         * updated_at : 2018-01-12 04:47:03
         * created_at : 2018-01-12 04:47:03
         * id : 5
         */

        private String name;
        private String email;
        private String username;
        private String phone_number;
        private String dob;
        private String gender;
        private String location;
        private String device_type;
        private String device_id;
        private String updated_at;
        private String created_at;
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
