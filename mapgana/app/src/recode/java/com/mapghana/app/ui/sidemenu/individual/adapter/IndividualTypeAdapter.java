package com.mapghana.app.ui.sidemenu.individual.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.model.Category;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.iterface.SetOnCategoryClickListener;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 29/1/18.
 */

public class IndividualTypeAdapter extends BaseRecycleAdapter implements Filterable {

    private SetOnCategoryClickListener setOnCategoryClickListener;
    private List<Category.DataBean> categoryList;
    private List<Category.DataBean> filterList;

    private CustomFilter customFilter;

    public IndividualTypeAdapter(SetOnCategoryClickListener setOnCategoryClickListener,
                                 List<Category.DataBean> categoryList, List<Category.DataBean> filterList) {
        this.setOnCategoryClickListener = setOnCategoryClickListener;
        this.categoryList = categoryList;
        this.filterList = filterList;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_types_individual;
    }

    @Override
    protected int getItemSize() {
        if (categoryList!=null && categoryList.size()>0){
            return categoryList.size();
        }
        return 0;
    }

    @Override
    protected BaseRecycleAdapter.ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    @Override
    public Filter getFilter() {
        if (customFilter==null){
            customFilter=new CustomFilter();
        }
        return customFilter;
    }


    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private AppCompatTextView tvTitle;

        private int pos;
        private LinearLayout llButton;
        private ImageView imgDown;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle= itemView.findViewById(R.id.tvTitle);
            llButton = itemView.findViewById(R.id.llButton);
            imgDown =  itemView.findViewById(R.id.imgDown);
            imgDown.setVisibility(View.GONE);
        }

        @Override
        public void setData(int position) {
            pos=position;
            tvTitle.setText(categoryList.get(position).getName());
            llButton.setOnClickListener(this);
            llButton.setTag(position);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case  R.id.llButton:
                    setOnCategoryClickListener.setOnCategoryClickListener((Integer) v.getTag(), String.valueOf(categoryList.get((Integer) v.getTag()).getId()));
                    break;
            }
        }
    }

    class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            FilterResults results=new FilterResults();

            if(constraint != null && constraint.length()>0)
            {
                ArrayList<Category.DataBean> filters=new ArrayList<Category.DataBean>();
                constraint=constraint.toString().toUpperCase();
                for (Category.DataBean dataBean:filterList){
                    if (dataBean.getName().toUpperCase().contains(constraint)){
                        filters.add(dataBean);
                    }
                }
                results.count=filters.size();
                results.values=filters;

            }else
            {
                results.count=filterList.size();
                results.values=filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // TODO Auto-generated method stub

            categoryList=(ArrayList<Category.DataBean>) results.values;
            notifyDataSetChanged();
        }

    }
}
