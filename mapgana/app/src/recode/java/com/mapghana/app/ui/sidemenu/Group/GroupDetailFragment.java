package com.mapghana.app.ui.sidemenu.Group;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.adapter.EventMediaListAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.dialog.PopQRCodeFullScreen;
import com.mapghana.app.dialog.PopUpMediaGallery;
import com.mapghana.app.interfaces.MediaClickHandler;
import com.mapghana.app.interfaces.QRcodeSizeListener;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventGallery;
import com.mapghana.app.model.GroupData;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.ui.sidemenu.Event.EventDetailFragment;
import com.mapghana.app.ui.sidemenu.Event.EventListCallback;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

public class GroupDetailFragment extends AppBaseFragment implements QRcodeSizeListener, EventListCallback {
    private AppCompatTextView txtGroupName, groupName, txtGroupAbout, about, txtChatLink, chatLink, txtMapghanaEventLink, mapghanaEventLink, txtAdminContact, admiContact, txtPassWord, password;
    private ImageView posterImage, qrCode, share_qr, chatLinkAccess;
    private LottieAnimationView progressBar, Qr_scanner;
    private RecyclerView recyclerView;
    private String title;
    private GroupData groupData;
    private PopQRCodeFullScreen dialog;
    private boolean isHidden;
    private LinearLayout llPassword;
    private FrameLayout maskLayout;
    private EditText etPassword;
    private ImageView checkChatLinkPassword;
    private RelativeLayout rlChatLink;
    private PopUpMediaGallery customDialog;
    private AppCompatTextView txtMedia;

    public static GroupDetailFragment newInstance(String title, GroupData item) {
        GroupDetailFragment fragment = new GroupDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constants.TITLE, title);
        args.putSerializable(Constants.ITEMS, item);
        fragment.setArguments(args);
        return fragment;
    }


    private void initToolBar(String title) {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(title);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);

    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_group_detail;
    }

    @Override
    public void initializeComponent() {

        initViewById(getView());

    }


    private void initViewById(View view) {
        txtGroupName = view.findViewById(R.id.txt_group_name);
        groupName = view.findViewById(R.id.group_name);

        txtGroupAbout = view.findViewById(R.id.txt_group_description);
        about = view.findViewById(R.id.group_description);

        txtChatLink = view.findViewById(R.id.txt_group_chat_link);
        chatLink = view.findViewById(R.id.group_chat_link);

        txtMapghanaEventLink = view.findViewById(R.id.txt_group_mapghana_event_link);
        mapghanaEventLink = view.findViewById(R.id.group_mapghana_event_link);

        txtAdminContact = view.findViewById(R.id.txt_group_admin_contact);
        admiContact = view.findViewById(R.id.group_admin_contact);

        chatLinkAccess = view.findViewById(R.id.chat_link_access);
        posterImage = view.findViewById(R.id.image_poster);
        progressBar = view.findViewById(R.id.progress_bar);
        Qr_scanner = view.findViewById(R.id.Qr_scanner);
        recyclerView = view.findViewById(R.id.rv_group_media);
        qrCode = view.findViewById(R.id.qr_code);
        share_qr = view.findViewById(R.id.share_qr);

        maskLayout = view.findViewById(R.id.frame_hidden);
        txtMedia = view.findViewById(R.id.txt_group_media);
        etPassword = view.findViewById(R.id.et_password);
        rlChatLink = view.findViewById(R.id.rl_chat_link);
        llPassword = view.findViewById(R.id.ll_password);
        checkChatLinkPassword = view.findViewById(R.id.ic_check_password);

        if (getArguments() != null) {

            title = getArguments().getString(Constants.TITLE);
            initToolBar(title);
            groupData = (GroupData) getArguments().getSerializable(Constants.ITEMS);
            setGroupData(groupData);
        }

        chatLinkAccess.setOnClickListener(v -> {
            toggleVisibility();
        });

        checkChatLinkPassword.setOnClickListener(v -> {
            if (groupData.getAdmin_password().equalsIgnoreCase(etPassword.getText().toString().trim())) {
                maskLayout.setVisibility(View.GONE);
                chatLink.setVisibility(View.VISIBLE);
                makeSpanableString(groupData.getChat_link(), getActivity(), chatLink);
                llPassword.setVisibility(View.GONE);
                chatLinkAccess.setVisibility(View.GONE);

            } else {
                Utils.showToast(getActivity(), getView(), "Please enter correct password");
            }
        });

        posterImage.setOnClickListener(v -> {
            dialog = new PopQRCodeFullScreen();
            dialog.setQrImage(groupData.getImage(), String.valueOf(groupData.getId()));
            dialog.setListener(this);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            dialog.show(ft, "FullScreenDialog");
        });

    }

    private void openEventDetail(String id, AppBaseActivity baseActivity) {
        EventDetailFragment eventDetail = EventDetailFragment.newInstance("Event", id);

        baseActivity.changeFragment(eventDetail, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);

    }

    private void initRecycleLayoutManager(Context context, List<EventGallery> item, MediaClickHandler listener) {

        recyclerView.setHasFixedSize(true);
        EventMediaListAdapter adapter = new EventMediaListAdapter(getContext(), item, listener);

        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);

    }

    public void makeSpanableString(String link, Context context, TextView view) {
        SpannableString ss = new SpannableString(link);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                switch (textView.getId()) {
                    case R.id.group_chat_link:
                        Utils.openChatLink(link, context);
                        break;
                    case R.id.group_mapghana_event_link:
                        String[] str = mapghanaEventLink.getText().toString().split("-");
                        openEventDetail(str[1], (AppBaseActivity) getActivity());
                        break;
                    case R.id.group_admin_contact:
                        Utils.startDialing(admiContact.getText().toString(), getActivity());
                        break;
                }

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan, 0, link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        view.setText(ss);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setHighlightColor(Color.TRANSPARENT);
    }

    private void setGroupData(GroupData data) {

        if (TextUtils.isEmpty(data.getGroup_name())) {
            setViewVisibility(groupName, View.GONE);
            setViewVisibility(txtGroupName, View.GONE);
        } else {
            setViewVisibility(groupName, View.VISIBLE);
            setViewVisibility(txtGroupName, View.VISIBLE);
            groupName.setText(data.getGroup_name());
        }

        if (TextUtils.isEmpty(data.getAbout())) {
            setViewVisibility(txtGroupAbout, View.GONE);
            setViewVisibility(about, View.GONE);
        } else {
            setViewVisibility(txtGroupAbout, View.VISIBLE);
            setViewVisibility(about, View.VISIBLE);
            about.setText(data.getAbout());
        }

        if (TextUtils.isEmpty(data.getChat_link())) {
            setViewVisibility(txtChatLink, View.GONE);
            setViewVisibility(chatLink, View.GONE);
        } else {
            if (TextUtils.isEmpty(data.getAdmin_password())) {
                chatLink.setVisibility(View.VISIBLE);
            }
            makeSpanableString(data.getChat_link(), getActivity(), chatLink);
            setLinkUrlIcon(data.getChat_link());
        }


        if (TextUtils.isEmpty(data.getMapghana_event_link())) {
            setViewVisibility(txtMapghanaEventLink, View.GONE);
            setViewVisibility(mapghanaEventLink, View.GONE);
        } else {
            makeSpanableString(data.getMapghana_event_link(), getActivity(), mapghanaEventLink);
        }

        if (TextUtils.isEmpty(data.getGroup_admin_contact())) {
            setViewVisibility(txtAdminContact, View.GONE);
            setViewVisibility(admiContact, View.GONE);
        } else {
            makeSpanableString(data.getGroup_admin_contact(), getActivity(), admiContact);

        }

        if (TextUtils.isEmpty(data.getAdmin_password())) {
            setViewVisibility(maskLayout, View.GONE);
            setViewVisibility(chatLinkAccess, View.GONE);

        } else {
            setViewVisibility(maskLayout, View.VISIBLE);
            setViewVisibility(chatLinkAccess, View.VISIBLE);
        }


        if (data.group_gallery.size() == 0) {
            setViewVisibility(txtMedia, View.GONE);
        } else {
            setViewVisibility(txtMedia, View.VISIBLE);
        }
        qrCode.setOnClickListener(view -> {
            dialog = new PopQRCodeFullScreen();
            dialog.setQrImage(BaseArguments.QR_CODE_URL + "" + data.getId(), String.valueOf(data.getId()));
            dialog.setListener(this);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            dialog.show(ft, "FullScreenDialog");
        });


        final Bitmap[] QrCodImage = {null};
        Glide.clear(qrCode);
        Glide.with(MapGhanaApplication.sharedInstance())
                .load(BaseArguments.QR_CODE_URL + "" + data.getId())
                .asBitmap()
                .dontAnimate()
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        Qr_scanner.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        QrCodImage[0] = resource;
                        Qr_scanner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(qrCode);

        if (TextUtils.isEmpty(data.getImage())) {
            setViewVisibility(posterImage, View.GONE);
            setViewVisibility(progressBar, View.GONE);
        } else {
            Glide.clear(posterImage);
            Glide.with(MapGhanaApplication.sharedInstance())
                    .load(data.getImage())
                    .asBitmap()
                    .dontAnimate()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.INVISIBLE);
                            Bitmap image = resource;
                            posterImage.setImageBitmap(image);
                            return false;
                        }
                    })
                    .into(posterImage);
        }
        share_qr.setOnClickListener(view -> {
            saveImage(QrCodImage[0], groupData);
            send(getActivity(), groupData);
        });

        MediaClickHandler mediaClickHandler = gallery -> {
            popUpgallery(getActivity(), groupData);
        };

        initRecycleLayoutManager(getActivity(), groupData.getGroup_gallery(), mediaClickHandler);
    }

    private void setLinkUrlIcon(String key) {
        if (key.contains("meetup")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.meetup, 0);
        } else if (key.contains("t.me")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.telegram, 0);
        } else if (key.contains("whatsapp")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.whatsapp, 0);
        } else if (key.contains("skype")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.skype, 0);
        } else if (key.contains("irc")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ircchat, 0);
        } else if (key.contains("viber")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.viber, 0);
        } else if (key.contains("line.me")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.line, 0);
        } else if (key.contains("weixin")) {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.wechat, 0);
        } else {
            chatLink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            setViewVisibility(txtChatLink,View.GONE);
            setViewVisibility(chatLink,View.GONE);

        }

    }

    private void popUpgallery(Context context, GroupData item) {
        customDialog = new PopUpMediaGallery(context, this, item.getGroup_gallery(), true, "eventDetail");
        customDialog.setCancelable(false);
        customDialog.show();
    }

    private void toggleVisibility() {
        if (isHidden) {
            isHidden = false;
            llPassword.setVisibility(View.GONE);
            chatLinkAccess.setImageResource(R.drawable.ic_visibility_off_maroon);
        } else {
            llPassword.setVisibility(View.VISIBLE);
            isHidden = true;
            chatLinkAccess.setImageResource(R.drawable.ic_visibility_maroon);
        }
    }

    private void setViewVisibility(View view, int value) {
        view.setVisibility(value);
    }

    @Override
    public void onClose() {
        dialog.dismiss();
    }

    public static void saveImage(Bitmap finalBitmap, GroupData item) {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/saved_images");
        Log.i("Directory", "==" + myDir);
        myDir.mkdirs();

        String fname = item.getGroup_name() + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void send(Context context, GroupData item) {
        try {
            File myFile = new File("/storage/emulated/0/saved_images/" + item.getGroup_name() + ".jpg");
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String ext = myFile.getName().substring(myFile.getName().lastIndexOf(".") + 1);
            String type = mime.getMimeTypeFromExtension(ext);
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            sharingIntent.setType(type);
            sharingIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(myFile));
            context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClickEvent(EventDetail model) {

    }

    @Override
    public void onPopUpClickEvent(EventDetail model) {

    }

    @Override
    public void onCloseIcon() {
        customDialog.dismiss();
    }
}
