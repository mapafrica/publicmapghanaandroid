package com.mapghana.app.helpers.navigation;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.SpinnerAdapter;
import com.mapghana.app.dialog.ConfirmationDialog;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventListingContent;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.model.IndividualPosts;
import com.mapghana.app.model.Logout;
import com.mapghana.app.model.ObjectDetail;
import com.mapghana.app.model.ObjectMapModel;
import com.mapghana.app.model.OrganizationPosts;
import com.mapghana.app.model.Session;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.DashboardFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.add_location.AddLocationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.SearchFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map.SingleItemOnMapFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.SearchListFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.FilterFragment;
import com.mapghana.app.ui.activity.dashboard.home.HomeFragment;
import com.mapghana.app.ui.sidemenu.AboutUsFragment;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.app.ui.sidemenu.Group.GroupListing;
import com.mapghana.app.ui.sidemenu.contactus.ContactUsFragment;
import com.mapghana.app.ui.sidemenu.customOrder.UserCustomOrderListingFragment;
import com.mapghana.app.ui.sidemenu.individual.IndividualFragment;
import com.mapghana.app.ui.sidemenu.organization.OrganizationFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog.PostListingDialog;
import com.mapghana.app.ui.sidemenu.profile.ProfileFragment;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.base.BaseFragment;
import com.mapghana.customviews.CircleImageView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.handler.NavigationViewHandlerInterface;
import com.mapghana.rest.ApiHitListener;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ubuntu on 6/12/17.
 */

public class NavigationViewHandler
        implements NavigationViewHandlerInterface, View.OnClickListener,
        AdapterClickListener, AdapterView.OnItemSelectedListener, ApiHitListener {

    private static AppBaseActivity baseActivity;
    private static OnSpinnerItemChangeListeners onSpinnerItemChangeListenersss;
    public double mCurrentLongitude = 0.0;
    public double mCurrentLatitude = 0.0;
    private DrawerLayout drawerLayout;
    private LinearLayout llLocation, ll_explore;
    private ImageView ll_map_explore;
    private RelativeLayout llNavToolBar;
    private ImageButton ivToggleNavButton, ivBack, imgNavSearch, imgNavCalender, imgNavSearchEvent, imgGroupMultiView, imgAtoZ_ZtoA;
    private TextView tvTitle;
    private NavigationView navigationView;
    private RecyclerView recyclerView;
    private boolean isMultiView = false;
    private CircleImageView ivUserPic;
    private TextView tvUserName, tvNavLocation, tv_addres;
    private Spinner spType;
    private RelativeLayout rl_profile;
    private Session session;
    private Switch mOnlineActive;
    private ProgressBar progressBar;
    private Call<ResponseBody> previousCall;
    private LinearLayout ll_profile_details;
    private TextView tvlogin;
    private NavigationMenuAdapter navigationMenuAdapter;
    private LocalBroadcastManager localBroadcastManager = null;
    private String mOnlineStatus;
    

    public NavigationViewHandler(AppBaseActivity baseActivity) {
        NavigationViewHandler.baseActivity = baseActivity;
    }

    public static NavigationViewHandler newInstance(OnSpinnerItemChangeListeners onSpinnerItemChangeListeners) {
        onSpinnerItemChangeListenersss = onSpinnerItemChangeListeners;
        Bundle args = new Bundle();

        NavigationViewHandler fragment = new NavigationViewHandler(NavigationViewHandler.baseActivity);
        //   fragment.setArguments(args);
        return fragment;
    }

    public void findViewIds() {
        drawerLayout = baseActivity.findViewById(R.id.drawerLayout);
        llNavToolBar = baseActivity.findViewById(R.id.llNavToolBar);
        rl_profile = baseActivity.findViewById(R.id.rl_profile);
        llLocation = baseActivity.findViewById(R.id.llLocation);
        ivToggleNavButton = baseActivity.findViewById(R.id.ivToggleNavButton);
        ivBack = baseActivity.findViewById(R.id.ivBack);
        mOnlineActive = baseActivity.findViewById(R.id.switch_active);
        imgNavSearch = baseActivity.findViewById(R.id.imgNavSearch);
        tvTitle = baseActivity.findViewById(R.id.tvTitle);
        tvUserName = baseActivity.findViewById(R.id.tvUserName);
        tvNavLocation = baseActivity.findViewById(R.id.tvNavLocation);
        tv_addres = baseActivity.findViewById(R.id.tv_addres);
        spType = baseActivity.findViewById(R.id.spType);
        recyclerView = baseActivity.findViewById(R.id.recyclerView);
        ivUserPic = baseActivity.findViewById(R.id.ivUserPic);
        navigationView = baseActivity.findViewById(R.id.navigation_view);
        progressBar = baseActivity.findViewById(R.id.progressBar);
        ll_explore = baseActivity.findViewById(R.id.ll_explore);
        ll_map_explore = baseActivity.findViewById(R.id.ll_map_explore);
        imgGroupMultiView = baseActivity.findViewById(R.id.img_multiView);
        imgAtoZ_ZtoA = baseActivity.findViewById(R.id.imgAtoZ_ZtoA);
        imgNavCalender = baseActivity.findViewById(R.id.imgNavCalender);
        imgNavSearchEvent = baseActivity.findViewById(R.id.imgNavSearchEvent);
        ll_profile_details = baseActivity.findViewById(R.id.ll_profile_details);
        tvlogin = baseActivity.findViewById(R.id.tvlogin);
        LinearLayoutManager manager = new LinearLayoutManager(baseActivity.getBaseContext());
        recyclerView.setLayoutManager(manager);
        progressBar.getIndeterminateDrawable().setColorFilter(baseActivity.getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
        imgNavSearch.setOnClickListener(this);
        imgNavSearchEvent.setOnClickListener(this);
        imgNavCalender.setOnClickListener(this);
        ivToggleNavButton.setOnClickListener(this);
        rl_profile.setOnClickListener(this);
        ll_map_explore.setOnClickListener(this);
        ll_explore.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        tvlogin.setOnClickListener(this);
        imgGroupMultiView.setOnClickListener(this);
        imgAtoZ_ZtoA.setOnClickListener(this);
        spType.setAdapter(new SpinnerAdapter(baseActivity.getBaseContext(), getTypes(), R.layout.item_post_type));
        setOnSpinnerItemChangeListenersss();
        localBroadcastManager = LocalBroadcastManager.getInstance(baseActivity);

        mOnlineActive.setOnCheckedChangeListener((compoundButton, b) -> {
            baseActivity.displayProgressBar(false);
            RestClient restClient = new RestClient(baseActivity);
            if (compoundButton.isChecked()) {
                mOnlineStatus = baseActivity.getString(R.string.string_online);
                if (ConnectionDetector.isNetAvail(baseActivity)) {
                    restClient.callback(this).postApiUserStatus(baseActivity, 1, session.getId());
                } else {
                    baseActivity.displayToast(Constants.No_Internet);
                }
            } else {
                mOnlineStatus = baseActivity.getString(R.string.string_offline);
                if (ConnectionDetector.isNetAvail(baseActivity)) {
                    restClient.callback(this).postApiUserStatus(baseActivity, 0, session.getId());
                } else {
                    baseActivity.displayToast(Constants.No_Internet);
                }
            }
        });
        setUpUserData();

    }


    public void setUpUserData() {
        session = AppPreferences.getSession();
        if (session != null) {
            if (!TextUtils.isEmpty(session.getUsername())) {
                ll_profile_details.setVisibility(View.VISIBLE);
                tvlogin.setVisibility(View.GONE);
                mOnlineActive.setVisibility(View.VISIBLE);
                if(AppPreferences.getUserStatus() == 1) {
                    mOnlineActive.setChecked(true);
                    mOnlineActive.setText(baseActivity.getString(R.string.string_online));
                }else{
                    mOnlineActive.setChecked(false);
                    mOnlineActive.setText(baseActivity.getString(R.string.string_offline));
                }
            } else {
                mOnlineActive.setVisibility(View.GONE);
                ll_profile_details.setVisibility(View.GONE);
                tvlogin.setVisibility(View.VISIBLE);
            }
        }
        recyclerView.setAdapter(new NavigationMenuAdapter(this, getMenuList()));

    }

    private String[] getTypes() {
        return baseActivity.getResources().getStringArray(R.array.type_array);
    }

    private List<NavMenuModel> getMenuList() {
        List<NavMenuModel> menuList = new ArrayList<>();

        menuList.add(new NavMenuModel(R.mipmap.organizations, baseActivity.getResources().getString(R.string.Organizations)));
        menuList.add(new NavMenuModel(R.drawable.ic_event_black_24dp, baseActivity.getResources().getString(R.string.Events)));
        menuList.add(new NavMenuModel(R.mipmap.individuals, baseActivity.getResources().getString(R.string.Individuals)));
        menuList.add(new NavMenuModel(R.mipmap.post_listing, baseActivity.getResources().getString(R.string.Post_Listing)));
        menuList.add(new NavMenuModel(R.mipmap.call, baseActivity.getResources().getString(R.string.Contact_Us)));
        menuList.add(new NavMenuModel(R.drawable.about_us, baseActivity.getResources().getString(R.string.About)));
//        menuList.add(new NavMenuModel(R.drawable.ic_event_black_24dp, "Object"));
        menuList.add(new NavMenuModel(R.drawable.ic_event_black_24dp, "Groups"));
        menuList.add(new NavMenuModel(R.drawable.ic_event_black_24dp, "Custom Orders"));

        if (!TextUtils.isEmpty(session.getUsername())) {
            menuList.add(new NavMenuModel(R.mipmap.log, baseActivity.getResources().getString(R.string.Logout)));
        }
        return menuList;
    }

    @Override
    public void setNavTitle(String title) {
        if (tvTitle != null && !title.equals("")) {
            tvTitle.setText(title);
        }
    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {
        if (visibilty) {
            llNavToolBar.setVisibility(View.VISIBLE);
        } else {
            llNavToolBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {

        if (visibilty) {
            ivToggleNavButton.setVisibility(View.VISIBLE);
        } else {
            ivToggleNavButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {

        if (visibilty) {
            ivBack.setVisibility(View.VISIBLE);
        } else {
            ivBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void setHeaderProfilePic(String uri, int res) {

        if (uri != null && !uri.equals("")) {
            Glide.with(baseActivity).load(uri).override(250, 250)
                    .placeholder(R.drawable.profile).dontAnimate()
                    .into(ivUserPic);
        } else {
            ivUserPic.setImageResource(res);
        }
    }

    @Override
    public void setUserName(String name) {
        if (name != null && !name.equals("")) {
            tvUserName.setText(name);
        }
    }

    @Override
    public void onClick(View view) {

        if (ivToggleNavButton == view) {
            setDrawableStatus();
        }
        switch (view.getId()) {
            case R.id.rl_profile:
                setDrawableStatus();
                baseActivity.changeFragment(new ProfileFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.imgNavSearch:

                baseActivity.changeFragment(new SearchFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.imgNavSearchEvent: {
                sendEventSearchBroadCast();
            }
            break;
            case R.id.img_multiView:
                toogleMultView();
                break;
            case R.id.imgAtoZ_ZtoA:
                sendBroadCastAtoZGroup();
                break;
            case R.id.ll_map_explore:
                baseActivity.changeFragment(new DashboardFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;

            case R.id.ll_explore:
                baseActivity.changeFragment(new DashboardFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.imgNavCalender:
                sendCalendarOpenBroadCast();
                break;

            case R.id.ivBack:
                baseActivity.onBackPressed();
                break;

            case R.id.tvlogin:
                setDrawableStatus();
                showLoginMessageDialog();
                break;

        }
    }

    private void sendEventSearchBroadCast() {
        Intent intent = new Intent(Constants.LOCAL_BROADCAST_EVENT_SEARCH);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_EVENT_SEARCH);
        // Send the broadcast
        LocalBroadcastManager.getInstance(baseActivity).sendBroadcast(intent);
    }

    private void sendBroadCastAtoZGroup() {
        Intent intent = new Intent(Constants.LOCAL_BROADCAST_AtoZ_GROUP);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_AtoZ_GROUP);
        // Send the broadcast
        LocalBroadcastManager.getInstance(baseActivity).sendBroadcast(intent);
    }

    private void toogleMultView() {
        Intent intent = new Intent(Constants.LOCAL_BROADCAST_MULTIVIEW);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        if (isMultiView) {
            isMultiView = false;
            intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_GRID);
            imgGroupMultiView.setImageResource(R.drawable.ic_list_white);
        } else {
            isMultiView = true;
            imgGroupMultiView.setImageResource(R.drawable.ic_grid_white);
            intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_LIST);

        }
        // Send the broadcast
        LocalBroadcastManager.getInstance(baseActivity).sendBroadcast(intent);
    }

    private void sendCalendarOpenBroadCast() {
        Intent intent = new Intent(Constants.LOCAL_BROADCAST_CALENDAR);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        intent.putExtra(Constants.type, Constants.LOCAL_BROADCAST_CALENDAR);
        // Send the broadcast
        LocalBroadcastManager.getInstance(baseActivity).sendBroadcast(intent);
    }

    private void showLoginMessageDialog() {
        if (baseActivity != null) {
            if (baseActivity instanceof DashboardActivity) {
                DashboardActivity dashboardActivity = (DashboardActivity) NavigationViewHandler.baseActivity;
                dashboardActivity.showLoginMessageDialog();
            }
        }
    }

    public void setDrawableStatus() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawers();
        } else {
            drawerLayout.openDrawer(navigationView);
        }
    }

    @Override
    public void onAdapterClickListener(int position) {
        Bundle bundle = new Bundle();

        switch (position) {


            case 0:
                setDrawableStatus();
                baseActivity.pushFragment(new OrganizationFragment(), true);
                break;
            case 1:
                setDrawableStatus();
                baseActivity.pushFragment(new EventListingFragment(), true);
                break;
            case 2:
                setDrawableStatus();
                baseActivity.pushFragment(new IndividualFragment(), true);
                break;
            case 3:
                setDrawableStatus();
                goToPostListing();
                break;
            case 4:
                setDrawableStatus();
                baseActivity.pushFragment(new ContactUsFragment(), true);
                break;
            case 5:
                setDrawableStatus();
                baseActivity.pushFragment(new AboutUsFragment(), true);
                break;
//            case 6:
//                setDrawableStatus();
//                baseActivity.pushFragment(new ObjectListing(), true);
//                break;
            case 6:
                setDrawableStatus();
                baseActivity.pushFragment(new GroupListing(), true);

            case 7:
                setDrawableStatus();
                baseActivity.pushFragment(new UserCustomOrderListingFragment(), true);

//                baseActivity.startActivity(new Intent(baseActivity, TestingActivity.class));
                break;
            case 8:
                setDrawableStatus();
                showConfirmationDialog();
                break;
        }
    }

    public void showConfirmationDialog() {
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(baseActivity);
        confirmationDialog.setLoginListener(() -> {
            confirmationDialog.dismiss();
            doLogout();
        });
        confirmationDialog.show();
    }

    private void doLogout() {
        if (ConnectionDetector.isNetAvail(baseActivity)) {
            baseActivity.displayProgressBar(false);
            RestClient restClient = new RestClient(baseActivity);
            restClient.callback(this).logout(baseActivity);
        } else {
            baseActivity.displayToast(Constants.No_Internet);
        }
    }

    private void goToPostListing() {
        PostListingDialog dialog = new PostListingDialog(baseActivity,
                this);
        dialog.show();
    }

    @Override
    public void onAdapterClickListener(String action) {

        Bundle bundle = new Bundle();
        AddLocationFragment fragment = new AddLocationFragment();
        if (action.equals(baseActivity.getString(R.string.Individuals))) {
            fragment.setType(Constants.INDIVIDUALS);
            bundle.putString(Constants.type, Constants.INDIVIDUALS);
        } else if (action.equals(baseActivity.getString(R.string.Organizations))) {
            fragment.setType(Constants.ORGANISATION);
            bundle.putString(Constants.type, Constants.ORGANISATION);
        }
        fragment.setArguments(bundle);
        baseActivity.changeFragment(fragment, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }

    @Override
    public void lockDrawer(boolean visibilty) {
        if (visibilty) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    public void setViewMutliple(boolean value) {
        isMultiView = value;
    }

    private void showToast(String msg) {
        Toast.makeText(baseActivity, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {

        if (visibilty) {
            llLocation.setVisibility(View.VISIBLE);
        } else {
            llLocation.setVisibility(View.GONE);

        }
    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {
        if (visibilty) {
            imgNavSearch.setVisibility(View.VISIBLE);
        } else {
            imgNavSearch.setVisibility(View.GONE);

        }
    }

    @Override
    public void setNavLocationText(String Name) {
        tvNavLocation.setText(Name);
    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {
        if (visibilty) {
            tvTitle.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);

        }
    }

    @Override
    public void setNavLocationTextVisibilty(boolean visibilty) {
        if (visibilty) {
            tv_addres.setVisibility(View.VISIBLE);
        } else {
            tv_addres.setVisibility(View.GONE);

        }
    }

    @Override
    public void setNavAddressText(String title) {
        tv_addres.setText(title);
    }

    @Override
    public void onAdapterClickListener(int position, String action) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        clearData();
        if (position == 0) {
            spType.setSelection(0);
        } else if (position == 1) {
            spType.setSelection(1);
        } else if (position == 2) {
            spType.setSelection(2);
        } else if (position == 3) {
            spType.setSelection(3);
        } else if (position == 4) {
            spType.setSelection(4);
        }


        getPostLists();
    }

    private void clearData() {

        BaseFragment latestFragment = baseActivity.getLatestFragment();
        if (latestFragment != null && latestFragment instanceof DashboardFragment) {
            DashboardFragment dashboardFragment = (DashboardFragment) latestFragment;
            dashboardFragment.clearIndiHashMap();
            dashboardFragment.clearOrgHashMap();
            dashboardFragment.clearAllHashMap();
            dashboardFragment.clearAllList();
            dashboardFragment.clearIndiList();
            dashboardFragment.clearOrgList();
            try {
                if (!dashboardFragment.isVisible()) return;
                dashboardFragment.getDashboardActivity().getMapHandler().getMapBoxFragment().clearMarker();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void getPostLists() {
        if (ConnectionDetector.isNetAvail(baseActivity)) {
            // baseActivity.displayProgressBar(false);
            progressBar.setVisibility(View.VISIBLE);
            // baseActivity.updateViewVisibitity(progressBar, View.VISIBLE);
            if (previousCall != null) {
                previousCall.cancel();
            }
            RestClient restClient = new RestClient(baseActivity);
            if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.organizations)) {

                previousCall = restClient.callback(this).post_listing_organizationsss(String.valueOf(getmCurrentLatitude()), String.valueOf(getmCurrentLongitude()));
            } else if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.individuals)) {
                previousCall = restClient.callback(this).post_listing_Individualsss(String.valueOf(getmCurrentLatitude()), String.valueOf(getmCurrentLongitude()));
            } else if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.events)) {
                previousCall = restClient.callback(this).post_listing_Event_Explore();
            } else if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.objects)) {
                previousCall = restClient.callback(this).post_listing_object_Explore();
            } else if (getPostType() != null &&
                    getPostType().equalsIgnoreCase(Constants.All)) {
                previousCall = restClient.callback(this).searchFilter("", "", "", String.valueOf(getmCurrentLatitude()), String.valueOf(getmCurrentLongitude()), "1000", "");
            } else {
            }
        } else {
            baseActivity.displayToast(Constants.No_Internet);
        }
    }

    @Override
    public String getPostType() {
        if (spType.getSelectedItemPosition() == 1) {
            return baseActivity.getResources().getString(R.string.Individuals);
        } else if (spType.getSelectedItemPosition() == 0) {
            return baseActivity.getResources().getString(R.string.Organizations);
        } else if (spType.getSelectedItemPosition() == 2) {
            return baseActivity.getResources().getString(R.string.Events);
        } else if (spType.getSelectedItemPosition() == 3) {
            return baseActivity.getResources().getString(R.string.All);
        } else {
            // -1 is selected
            return "";
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        baseActivity.dismissProgressBar();
        progressBar.setVisibility(View.INVISIBLE);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_logout) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Logout logout = gson.fromJson(s, Logout.class);
                    if (logout.getStatus() != 0) {
                        baseActivity.displayToast(logout.getMessage());
                        AppPreferences.logout();
                        setUpUserData();
                        if (baseActivity != null && baseActivity instanceof DashboardActivity) {
                            DashboardActivity dashboardActivity = (DashboardActivity) NavigationViewHandler.baseActivity;
                            dashboardActivity.setUpUserData();
                        }
                    } else {
                        baseActivity.displayErrorDialog("Error", logout.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POSTS_Filter_organization) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    OrganizationPosts items = gson.fromJson(s, OrganizationPosts.class);
                    if (items.getStatus() != 0) {
                        onSpinnerItemChangeListenersss.OnSpinnerItemOrganizationListeners(items.getData(), Constants.ORGANISATION);
                    } else {
                        baseActivity.displayErrorDialog("Organization", items.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POSTS_Filter_individual) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    IndividualPosts items = gson.fromJson(s, IndividualPosts.class);
                    if (items.getStatus() != 0) {
                        onSpinnerItemChangeListenersss.OnSpinnerItemIndividualListeners(items.getData(), Constants.INDIVIDUALS);
                    } else {
                        baseActivity.displayErrorDialog("Individual", items.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_EVENT_EXPLORE) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    EventListingContent items = gson.fromJson(s, EventListingContent.class);
                    if (items.getData().size() > 0) {
                        onSpinnerItemChangeListenersss.OnSpinnerItemEventListeners(items.getData(), Constants.events);
                    } else {
                        baseActivity.displayErrorDialog("Event", "No data found");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_OBJECT_EXPLORE) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    ObjectMapModel items = gson.fromJson(s, ObjectMapModel.class);
                    if (items.getData().size() > 0) {
                        onSpinnerItemChangeListenersss.OnSpinnerItemObjectListeners(items.getData(), Constants.objects);
                    } else {
                        baseActivity.displayErrorDialog("Object", "No data found");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_USER_STATUS) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    mOnlineActive.setText(mOnlineStatus);
                    if (mOnlineStatus.equalsIgnoreCase(baseActivity.getString(R.string.string_online)))
                        AppPreferences.setUserStatus(1);
                    else
                        AppPreferences.setUserStatus(0);
                    
                } catch (IOException e) {
                    e.printStackTrace();
                    baseActivity.displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POSTS_SEarch_Filter) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GlobeSearch data = gson.fromJson(s, GlobeSearch.class);
                    if (data.getStatus() != 0) {
                        GlobeSearch.SearchData searchData = data.getSearchData();
                        if (searchData != null) {
                            List<GlobeSearch.DataBean> data1 = searchData.getData();
                            if (data1 != null) {
                                onSpinnerItemChangeListenersss.OnSpinnerItemAllListeners(data1);
                            }
                        }
                    } else {
                    }
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {

        if(apiId!=15) {
            progressBar.setVisibility(View.INVISIBLE);
            baseActivity.displayErrorDialog("Error", error);
        }
    }

    public void setOnSpinnerItemChangeListenersss() {
        spType.setOnItemSelectedListener(this);
    }

    public double getmCurrentLongitude() {
        return mCurrentLongitude;
    }

    public void setmCurrentLongitude(double mCurrentLongitude) {
        this.mCurrentLongitude = mCurrentLongitude;
    }

    public double getmCurrentLatitude() {
        return mCurrentLatitude;
    }

    public void setmCurrentLatitude(double mCurrentLatitude) {
        this.mCurrentLatitude = mCurrentLatitude;
    }

    private boolean isViewVisible(View view) {
        return view.getVisibility() == View.VISIBLE;
    }

    private boolean isViewHide(View view) {
        return view.getVisibility() == View.GONE || view.getVisibility() == View.INVISIBLE;
    }

    public void onCreateViewFragment(BaseFragment baseFragment) {
        if (baseFragment instanceof DashboardFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(true);
            setSearchButtonVisibuility(true);
            setBackButtonVisibilty(false);
            setNavTitleTextVisibilty(false);
            lockDrawer(false);
            setNavToggleButtonVisibilty(true);
            setExploreVisibility(false);
            setCalenderVisibility(false);
            setSearchVisibiltyInternal(false);
            setAtoZZtoAVisibiltyInternal(false);
            setimgMultiViewVisibility(false);

        } else if (baseFragment instanceof SearchFragment) {
            //   setNavigationToolbarVisibilty(false);
        } else if (baseFragment instanceof SearchListFragment) {
          /*  setNavigationToolbarVisibilty(false);
            lockDrawer(true);*/
        } else if (baseFragment instanceof FilterFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Filter));
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setExploreVisibility(false);
            setCalenderVisibility(false);
            setSearchVisibiltyInternal(false);
            setAtoZZtoAVisibiltyInternal(false);
            setimgMultiViewVisibility(false);

        } else if (baseFragment instanceof SingleItemOnMapFragment) {
         /*   setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            //getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);*/
        } else if (baseFragment instanceof PostDetailFragment) {
      /*      setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);*/
        } else if (baseFragment instanceof ProfileFragment) {
            setNavigationToolbarVisibilty(false);
        } else if (baseFragment instanceof OrganizationFragment) {
           /* setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Organizations));*/
        } else if (baseFragment instanceof IndividualFragment) {
         /*   setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Individuals));*/
        } else if (baseFragment instanceof ContactUsFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.Contact_Us));
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setExploreVisibility(false);
            setCalenderVisibility(false);
            setSearchVisibiltyInternal(false);
            setAtoZZtoAVisibiltyInternal(false);
            setimgMultiViewVisibility(false);

        } else if (baseFragment instanceof AboutUsFragment) {
            setNavigationToolbarVisibilty(true);
            setBussinessTypeLayoutVisibuility(false);
            setSearchButtonVisibuility(false);
            setBackButtonVisibilty(true);
            lockDrawer(true);
            setNavToggleButtonVisibilty(false);
            setNavTitleTextVisibilty(true);
            setNavTitle(baseActivity.getResources().getString(R.string.About));
            setExploreVisibility(false);
            setCalenderVisibility(false);
            setSearchVisibiltyInternal(false);
            setAtoZZtoAVisibiltyInternal(false);
            setimgMultiViewVisibility(false);
        } else if (baseFragment instanceof HomeFragment) {

            isMultiView = false;
        } else if (baseFragment instanceof GroupListing) {
            if (isMultiView) {
                imgGroupMultiView.setImageResource(R.drawable.ic_grid_white);
            } else {
                imgGroupMultiView.setImageResource(R.drawable.ic_list_white);
            }
        }
    }

    @Override
    public void setExploreVisibility(boolean visibility) {
        if (visibility) {
            ll_explore.setVisibility(View.VISIBLE);
            ll_map_explore.setVisibility(View.VISIBLE);
        } else {
            ll_explore.setVisibility(View.GONE);
            ll_map_explore.setVisibility(View.GONE);
        }
    }

    public void setIsMultipleView() {
        isMultiView = false;
    }

    @Override
    public void setCalenderVisibility(boolean visibility) {
        if (visibility) {
            imgNavCalender.setVisibility(View.VISIBLE);
        } else {
            imgNavCalender.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSearchVisibiltyInternal(boolean visibility) {
        if (visibility) {
            imgNavSearchEvent.setVisibility(View.VISIBLE);
        } else {
            imgNavSearchEvent.setVisibility(View.GONE);
        }
    }

    @Override
    public void setimgMultiViewVisibility(boolean visibility) {
        if (visibility) {
            imgGroupMultiView.setVisibility(View.VISIBLE);
        } else {
            imgGroupMultiView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setAtoZZtoAVisibiltyInternal(boolean visibility) {
        if (visibility) {
            imgAtoZ_ZtoA.setVisibility(View.VISIBLE);
        } else {
            imgAtoZ_ZtoA.setVisibility(View.GONE);
        }
    }

    public interface OnSpinnerItemChangeListeners {
        void OnSpinnerItemOrganizationListeners(List<OrganizationPosts.DataBean> orList, String type);

        void OnSpinnerItemIndividualListeners(List<IndividualPosts.DataBean> inList, String type);

        void OnSpinnerItemEventListeners(List<EventDetail> inList, String type);

        void OnSpinnerItemObjectListeners(List<ObjectDetail> inList, String type);

        void OnSpinnerItemAllListeners(List<GlobeSearch.DataBean> allList);
    }

}
