package com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map;


import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.GetDetails;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SingleItemOnMapFragment extends AppBaseFragment
        implements MapboxMap.OnMarkerClickListener,
        MapboxMap.OnInfoWindowClickListener, LocationServiceListner {

    private int zoomLevel= 13;
    private LatLng latLng;
    private String name;
    private String address;
    private double latitude, longitude;
    private int TYPE;
    private int resource;
    private String detailResponse;
    private GetDetails data;
    private boolean hasResponse = false;
    private Marker marker;
    private LinearLayout ll_view;
    private boolean mIsSpecific;

    private ImageButton imgMyLoc;
    private int post_id;

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_single_item_on_map;
    }

    @Override
    public void initializeComponent() {
        init();

        ll_view =  getView().findViewById(R.id.ll_view);

        TypefaceTextView tvViewDetails = (TypefaceTextView) getView().findViewById(R.id.tvViewDetails);
         imgMyLoc = (ImageButton) getView().findViewById(R.id.imgMyLoc);

        tvViewDetails.setOnClickListener(this);
        imgMyLoc.setOnClickListener(this);


        getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                initMap();
                getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setMapPadding();
                setData();
            }
        });


    }

    private void setMapPadding() {
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().setMapPadding(0,
                    Math.round(getResources().getDimension(R.dimen.dp50)),
                    0, ll_view.getHeight()-Math.round(imgMyLoc.getHeight()*0.80f));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
    }

    private void initMap() {
        if (!isVisible())return;
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().clearMarker();
            getDashboardActivity().getMapHandler().getMapBoxFragment().setOnMarkerClickListener(this);
            getDashboardActivity().getMapHandler().getMapBoxFragment().setOnInfoWindowClickListener(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void viewCreateFromBackStack() {
        super.viewCreateFromBackStack();
        init();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
            case R.id.tvViewDetails:
                changeFragment();
                break;
        }
    }

    private void setData() {
        if (!hasResponse) {
            hasResponse = true;
            getDetails();
        } else {
            if (detailResponse != null) {
                updateUI(detailResponse);
            }
        }
    }

    private void getDetails() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).getDetailsOfOnePost(String.valueOf(post_id));
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        return false;
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        if (this.marker == marker) {
            return false;
        }
        changeFragment();
        return false;
    }

    private void changeFragment() {
        if (detailResponse == null) {
            return;
        }

        PostDetailFragment detailsFragment = new PostDetailFragment();
        detailsFragment.setPost_id(post_id);
        try {
            getDashboardActivity().changeFragment(detailsFragment, true, false, 0, R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_Details_1) {
                try {
                    detailResponse = response.body().string();
                    updateUI(detailResponse);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void updateUI(String s) {
        if (!isVisible())return;
        Gson gson = new Gson();
        data = gson.fromJson(detailResponse, GetDetails.class);
        if (data.getStatus() != 0) {
            latitude = data.getData().getLat();
            longitude = data.getData().getLog();
            try {
                latLng = new LatLng(latitude, longitude);
            } catch (IllegalArgumentException e) {
            }

            name = data.getData().getName();
            address = data.getData().getAddress();
            getNavHandler().setNavTitle(name);
            String category = "";
            if (data.getData().getPost_for() != null &&
                    data.getData().getPost_for().equalsIgnoreCase(Constants.individuals)) {
                String status = "";
                String gender = "";
                category = "Occupation: " + data.getData().getCategory().getName();
                status = data.getData().getStatus().trim();
                gender = data.getData().getGender().trim();
                String locationProfile = data.getData().getLocation_profile();

                if(!TextUtils.isEmpty(locationProfile)){
                    if(locationProfile.equalsIgnoreCase("general")){
                        mIsSpecific = false;
                    }else{
                        mIsSpecific = true;

                    }
                }

                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_gril;
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resource = R.mipmap.individual_red_girl;
                        TYPE = 2;
                    } else {
                        resource = R.mipmap.indi_girl_gray;
                        TYPE = 3;
                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_man;
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        TYPE = 2;
                        resource = R.mipmap.individual_red_man;
                    } else {
                        resource = R.mipmap.individual_gray_man;
                        TYPE = 3;
                    }
                }
            } else if (data.getData().getPost_for() != null &&
                    data.getData().getPost_for().equalsIgnoreCase(Constants.organizations)) {
                if (data.getData().getStatus() != null &&
                        data.getData().getStatus().equalsIgnoreCase(Constants.verified)) {
                    resource = R.mipmap.location_gree_org;
                    TYPE = 1;
                } else if (data.getData().getStatus() != null &&
                        data.getData().getStatus().equalsIgnoreCase(Constants.unverified)) {
                    resource = R.mipmap.location_red_org;
                    TYPE = 2;
                } else {
                    resource = R.mipmap.location_gry_org;
                    TYPE = 3;
                }

                category = "Category: ";
                GetDetails.DataBean.CategoryBean categoryObj = data.getData().getCategory();
                if (categoryObj != null) {
                    category = category + categoryObj.getName();
                }
                GetDetails.DataBean.SubcategoryBean subcategoryObj = data.getData().getSubcategory();
                if (subcategoryObj != null) {
                    category = category + " (" + subcategoryObj.getName() + ")";
                }
            }
            if (latLng != null) {

                setMarkerFirstTime(latLng, name, address + "\n" + category);
            } else {
                displayToast("Invalid latitude and longitude");
            }
        } else {
            displayErrorDialog("Error", data.getError());
        }
    }

    private void setMarkerFirstTime(LatLng latLng, String title, String snippet) {
        if (!isVisible())return;

        String status = "";
        if (TYPE == 1) {
            status = Constants.verified;
        } else if (TYPE == 2) {
            status = Constants.unverified;
        } else {
            //(type==3)
            status = Constants.anonymous;
        }
        try {
            final List<LatLng> boundList = new ArrayList<>();
            getDashboardActivity().getMapHandler().getMapBoxFragment().
                    addMarker(getContext(), resource, latLng, title, "Address: " + snippet + "\nStatus: " + status);
            boundList.add(latLng);

            double lat = getDashboardActivity().mCurrentLatitude;
            double log = getDashboardActivity().mCurrentLongitude;
            LatLng currentPos = new LatLng(lat, log);


            marker = getDashboardActivity().getMapHandler().getMapBoxFragment().
                    addMarker(getContext(), R.mipmap.my_location, currentPos, "You", "");
            boundList.add(currentPos);
            getDashboardActivity().getMapHandler().getMapBoxFragment().boundMap(boundList);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    private void setMarkersOnMapAgain() {
        try {
            if ( !isVisible()) return;
            zoomLevel = 17;
            double lat = getDashboardActivity().mCurrentLatitude;
            double log = getDashboardActivity().mCurrentLongitude;
            LatLng latLng_cur_pos = new LatLng(lat, log);
            Location location = new Location("");
            location.setLatitude(lat);
            location.setLongitude(log);
            moveMarker(location);

            getDashboardActivity().getMapHandler().getMapBoxFragment().animateMap(latLng_cur_pos, zoomLevel);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void userLocationChange(Location location) {
        moveMarker(location);
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    private void moveMarker(Location location) {
        if (location == null) {
            return;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (marker != null) {
            marker.setPosition(latLng);
        }
    }

}
