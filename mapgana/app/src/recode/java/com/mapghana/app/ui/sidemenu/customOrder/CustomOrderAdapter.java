package com.mapghana.app.ui.sidemenu.customOrder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.model.CustomOrderModel;
import com.mapghana.app.utils.MapGhanaApplication;

import java.util.ArrayList;
import java.util.List;

public class CustomOrderAdapter extends RecyclerView.Adapter<CustomOrderAdapter.MyViewHolder> {
    private List<CustomOrderModel> userCustomOrderModelList;
    private CustomOrderInterface listener;
    private boolean userOrderList;
    private AppBaseActivity activity;

    interface CustomOrderInterface{
        void onPayClick(CustomOrderModel model);
        void ItemClick(CustomOrderModel model);
    }
    public CustomOrderAdapter(CustomOrderInterface listerner, AppBaseActivity activity,ArrayList<CustomOrderModel> list, boolean userOrderList) {
        this.listener  = listerner;
        this.activity = activity;
        this.userCustomOrderModelList = list;
        this.userOrderList = userOrderList;
    }

    public CustomOrderAdapter(ArrayList<CustomOrderModel> list, boolean userOrderList) {
        this.userCustomOrderModelList = list;
        this.userOrderList = userOrderList;
    }

    public void setDataList(ArrayList<CustomOrderModel> list){
        this.userCustomOrderModelList = list;
        notifyDataSetChanged();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_user_custom_order, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CustomOrderModel CustomOrderModel = userCustomOrderModelList.get(position);
        holder.title.setText(CustomOrderModel.getTitle());
        holder.description.setText(CustomOrderModel.getDescription());
        holder.price.setText("Amount : "+CustomOrderModel.getBudget());
        holder.status.setText(CustomOrderModel.getSearch_status());

        Glide.clear(holder.mProductImage);
        Glide.with(MapGhanaApplication.sharedInstance())
                .load(CustomOrderModel.getImageUrl())
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                            isFirstResource) {
                        holder.progressBar.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                            target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.mProductImage);

        if(userOrderList) {
            holder.status.setVisibility(View.VISIBLE);
            holder.mBtnPay.setVisibility(View.GONE);
        }else{
            holder.mBtnPay.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.GONE);
        }

        holder.mBtnPay.setOnClickListener(view -> {
            listener.onPayClick(CustomOrderModel);
        });

        holder.llOrder.setOnClickListener(view -> {
            listener.ItemClick(CustomOrderModel);
        });

        if(CustomOrderModel.getSearch_status().equals("Pending")) {
            holder.status.setTextColor(activity.getResources().getColor(R.color.colorRed));
        }else {
            holder.status.setTextColor(activity.getResources().getColor(R.color.colorDarkGreen));
        }
    }

    @Override
    public int getItemCount() {
        return userCustomOrderModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, price, status;
        public ImageView mProductImage;
        public ProgressBar progressBar;
        public Button mBtnPay;
        public LinearLayout llOrder;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.txt_title);
            description = view.findViewById(R.id.txt_description);
            price = view.findViewById(R.id.txt_price);
            status = view.findViewById(R.id.txt_status);
            mProductImage = view.findViewById(R.id.product_image);
            progressBar = view.findViewById(R.id.progress_bar);
            mBtnPay = view.findViewById(R.id.btn_pay);
            llOrder = view.findViewById(R.id.ll_order);
        }
    }
}
