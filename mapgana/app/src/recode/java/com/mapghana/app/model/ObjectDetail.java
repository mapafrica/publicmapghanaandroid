package com.mapghana.app.model;

import java.io.Serializable;

public class ObjectDetail implements Serializable {

    private int id;
    private int user_id;
    private String name;
    private String image;
    private String type;
    private String scheduling;
    private String reference;
    private String about;
    private String entry_period;
    private String exit_period;
    private String location_profile;
    private String lat;
    private String log;
    private String post_visibility;
    private String one_time_use;
    private String password_protected;
    private String password;
    private String deleted_at;
    private String created_at;
    private String updated_at;


    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getType() {
        return type;
    }

    public String getScheduling() {
        return scheduling;
    }

    public String getReference() {
        return reference;
    }

    public String getAbout() {
        return about;
    }

    public String getEntry_period() {
        return entry_period;
    }

    public String getExit_period() {
        return exit_period;
    }

    public String getLocation_profile() {
        return location_profile;
    }

    public String getLat() {
        return lat;
    }

    public String getLog() {
        return log;
    }

    public String getPost_visibility() {
        return post_visibility;
    }

    public String getOne_time_use() {
        return one_time_use;
    }

    public String getPassword_protected() {
        return password_protected;
    }

    public String getPassword() {
        return password;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
