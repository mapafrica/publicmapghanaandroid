package com.mapghana.app.helpers.navigation;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

import java.util.List;


/**
 * Created by ubuntu on 6/12/17.
 */

public class NavigationMenuAdapter extends BaseRecycleAdapter {

    private AdapterClickListener clickListener;
    private List<NavMenuModel> menuList;
    private RelativeLayout rlNavItem;

    public NavigationMenuAdapter(AdapterClickListener clickListener, List<NavMenuModel> menuList) {
        this.clickListener = clickListener;
        this.menuList = menuList;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_nav_menu;
    }

    @Override
    protected int getItemSize() {
        return menuList.size();
    }

    @Override
    protected BaseRecycleAdapter.ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder {
        private AppCompatTextView tvItemName;
        private int pos;
        private ImageView imgMenu;
        public MyViewHolder(View itemView) {
            super(itemView);
            rlNavItem= itemView.findViewById(R.id.rlNavItem);
            tvItemName= itemView.findViewById(R.id.tvItemName);
            imgMenu= itemView.findViewById(R.id.imgMenu);
        }

        @Override
        public void setData(int position) {
            pos=position;
            tvItemName.setText(menuList.get(position).getTitle());
            imgMenu.setImageResource(menuList.get(position).getIconPath());
            rlNavItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onAdapterClickListener(pos);
        }
    }
}
