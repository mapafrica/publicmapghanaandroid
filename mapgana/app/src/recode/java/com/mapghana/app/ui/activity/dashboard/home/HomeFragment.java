package com.mapghana.app.ui.activity.dashboard.home;


import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.City;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.model.PopularCategory;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.activity.dashboard.dashboard.add_location.AddLocationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.SearchFragment;
import com.mapghana.app.ui.activity.dashboard.home.adapter.PopularCategoryAdapter;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.app.ui.sidemenu.Group.GroupListing;
import com.mapghana.app.ui.sidemenu.customOrder.UserCustomOrderListingFragment;
import com.mapghana.app.ui.sidemenu.individual.IndividualFragment;
import com.mapghana.app.ui.sidemenu.organization.OrganizationFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CityAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog.PostListingDialog;
import com.mapghana.app.utils.Constants;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends AppBaseFragment implements AdapterView.OnItemSelectedListener {

    TextView tvPostListing;
    CityAdapter cityAdapter;
    PostListingDialog dialog;
    PopularCategoryAdapter popularCategoryAdapter;
    private RelativeLayout rl_search;
    private RestClient restClient;
    private LinearLayout ll_all_organization, ll_all_individual;
    private EditText et_search;
    private Spinner spLoc;
    private BottomNavigationView bottomNavigationView;
    private RecyclerView recycler_view;
    private LinearLayout llAllOrganisation, llPopularOrganisation, llPopularIndividual, llAllIndividual;
    private ArrayList<City.DataBean> cityList;
    private String city_id;
    private int clickfrom = 0;
    private String category_id = "";
    private List<PopularCategory.PopulatCategoryModel> data;

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initializeComponent() {
        init();
        getActivity().getWindow()
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        restClient = new RestClient(getContext());
        spLoc = getView().findViewById(R.id.spLoc);
        rl_search = getView().findViewById(R.id.rl_search);
        recycler_view = getView().findViewById(R.id.recycler_view);
        data = new ArrayList<>();

        et_search = getView().findViewById(R.id.et_search);
        ll_all_organization = getView().findViewById(R.id.ll_all_organization);
        ll_all_individual = getView().findViewById(R.id.ll_all_individual);
        tvPostListing = getView().findViewById(R.id.tvPostListing);

        rl_search.setOnClickListener(this);
        ll_all_organization.setOnClickListener(this);
        ll_all_individual.setOnClickListener(this);
        tvPostListing.setOnClickListener(this);
        cityList = new ArrayList<>();
        dialog = new PostListingDialog(getActivity(), this);


        llAllOrganisation = getView().findViewById(R.id.all_organisation);
        llPopularOrganisation = getView().findViewById(R.id.popular_organisation);
        llPopularIndividual = getView().findViewById(R.id.popular_individual);
        llAllIndividual = getView().findViewById(R.id.all_individual);
        getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                getPopularCategory();
            }
        });

        setUpRecyclerView();
        //  changeFragment();

        llAllOrganisation.setOnClickListener(this);
        llPopularOrganisation.setOnClickListener(this);
        llPopularIndividual.setOnClickListener(this);
        llAllIndividual.setOnClickListener(this);
    }

    /*   private void changeFragment() {

           PostDetailFragment detailsFragment = new PostDetailFragment();
           detailsFragment.setPost_id(1224);
           try {
               getDashboardActivity().changeFragment(detailsFragment, true, false, 0, R.anim.alpha_visible_anim, 0,
                       0, R.anim.alpha_gone_anim, true);
           } catch (IllegalAccessException e) {
               e.printStackTrace();
           }
       }
   */
    private void setUpRecyclerView() {
        popularCategoryAdapter = new PopularCategoryAdapter(this, data);
        recycler_view.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        recycler_view.setAdapter(popularCategoryAdapter);
    }


    @Override
    public void onPause() {
        super.onPause();
        spLoc.setOnItemSelectedListener(null);

    }

    @Override
    public void onResume() {
        super.onResume();
        spLoc.setOnItemSelectedListener(this);
        try {
            getDashboardActivity().getNavigationViewHandler().setIsMultipleView();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void getCity() {
        try {
            ArrayList<City.DataBean> myCityList = getApplication().getCityList();
            if (myCityList != null && myCityList.size() > 0) {
                cityList.addAll(myCityList);
                cityAdapter = new CityAdapter(getContext(), cityList);
                cityAdapter.setDataBean(0);
                spLoc.setAdapter(cityAdapter);
                //setSelectedCity();

            } else {
                if (ConnectionDetector.isNetAvail(getActivity())) {
                    displayProgressBar(false);
                    restClient.callback(this).city();
                } else {
                    displayToast(Constants.No_Internet);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void getPopularCategory() {

        try {
            List<PopularCategory.PopulatCategoryModel> myCityList = getApplication().getPopulatCategoryList();
            if (myCityList != null && myCityList.size() > 0) {
                data.clear();
                data.addAll(myCityList);
                popularCategoryAdapter.notifyDataSetChanged();
                getCity();
            } else {
                if (ConnectionDetector.isNetAvail(getActivity())) {
                    displayProgressBar(false);
                    restClient.callback(this).popularCategory();

                } else {
                    displayToast(Constants.No_Internet);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    private void getSelrctrdPopularData(int id, String keyword) {
        if (ConnectionDetector.isNetAvail(getActivity())) {
            category_id = String.valueOf(id);
            displayProgressBar(false);
            restClient.callback(this).searchFilter(keyword, String.valueOf(id), city_id, "", "", "",
                    "1");
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private void getPopularIndividualListingData() {
        if (ConnectionDetector.isNetAvail(getActivity())) {

            displayProgressBar(false);
            restClient.callback(this).popularIndividuals();
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    private void getPopularOrganisationListingData() {
        if (ConnectionDetector.isNetAvail(getActivity())) {

            displayProgressBar(false);
            restClient.callback(this).popularOrganisation();
        } else {
            displayToast(Constants.No_Internet);
        }
    }


    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(false);
        getNavHandler().lockDrawer(false);
        getNavHandler().setNavToggleButtonVisibilty(true);
        getNavHandler().setExploreVisibility(true);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);

    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_city) {
                cityList.clear();
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    City city = gson.fromJson(s, City.class);
                    if (city.getStatus() != 0) {

                        City.DataBean city1 = new City.DataBean();
                        city1.setName("Select Region..");
                        cityList.add(0, city1);
                        cityList.addAll(city.getData());
                        try {
                            getApplication().setCityList(cityList);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }

                        cityAdapter = new CityAdapter(getContext(), cityList);
                        cityAdapter.setDataBean(0);
                        spLoc.setSelection(0);
                        spLoc.setAdapter(cityAdapter);
                        //setSelectedCity();
                    } else {
                        displayErrorDialog("Error", city.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_CATEGORY) {
                try {
                    String s = response.body().string();
                    showSearchFragmentForPopular(s);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POPULAR_CATEGORY) {
                data.clear();
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    PopularCategory popularCategory = gson.fromJson(s, PopularCategory.class);
                    if (popularCategory.getStatus() != 0) {
                        List<PopularCategory.PopulatCategoryModel> list_data = popularCategory.getData();
                        if (list_data != null && list_data.size() > 0) {
                            try {
                                data.addAll(list_data);
                                getApplication().setPopulatCategoryList(data);
                                popularCategoryAdapter.notifyDataSetChanged();
                                getCity();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        } else {
                            displayErrorDialog("Error", "No popular category available, please contact to support.");

                        }
                    } else {
                        displayErrorDialog("Error", popularCategory.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_POSTS_SEarch_Filter) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GlobeSearch data = gson.fromJson(s, GlobeSearch.class);
                    if (data.getStatus() != 0) {
                        List<GlobeSearch.DataBean> data1 = data.getSearchData().getData();
                        if (data1 != null && data1.size() > 0) {
                            //  showSearchFragment(s);

                            showSearchFragment(s);
                        } else {
                            displayToast("No posts available.");
                        }

                    } else {
                        displayToast(data.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayToast(e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void setSelectedCity() {
        if (cityList != null && cityList.size() > 0) {
            int flag = 0;
            String id = SessionManager.getSelectedCityId(getContext());

            if (id != null && !id.isEmpty()) {
                int my_sel_id = Integer.parseInt(id);
                for (int i = 0; i < cityList.size(); i++) {
                    if (cityList.get(i).getId() == 1) {
                        spLoc.setSelection(i);
                        cityAdapter.setDataBean(i);
                    }
                    if (cityList.get(i).getId() == my_sel_id) {
                        spLoc.setSelection(i);
                        cityAdapter.setDataBean(i);
                        flag = 1;
                        break;
                    }
                }
            }
            if (flag == 0) {

            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (cityAdapter == null || cityList == null || cityList.size() == 0) return;

        cityAdapter.setDataBean(position);

        if (!cityList.get(position).getName().equals(Constants.Select_City)) {
            city_id = String.valueOf(cityList.get(position).getId());
            SessionManager.saveSelectedCityId(HomeFragment.this.getActivity(), city_id);
        } else {
            city_id = "";
            SessionManager.saveSelectedCityId(HomeFragment.this.getActivity(), city_id);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onAdapterClickListener(int position) {
        clickfrom = 1;
        getSelrctrdPopularData(data.get(position).getId(), "");

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_search:
                clickfrom = 0;
                if (et_search.getText().toString().trim().length() > 0)
                    getSelrctrdPopularData(0, et_search.getText().toString().trim());
                else
                    displayToast("Please enter keyword to search.");

                // showSearchFragment(et_search.getText().toString().trim());
                break;
            case R.id.ll_all_organization:
                getPopularOrganisationListingData();
                break;
            case R.id.all_organisation:
                showAllOrganizationCategory(false);
                break;
            case R.id.popular_organisation:
                getPopularOrganisationListingData();

                break;
            case R.id.popular_individual:
                getPopularIndividualListingData();

                break;
            case R.id.all_individual:
                showAllIndividualCategory(false);
                break;
            case R.id.ll_all_individual:
                getPopularIndividualListingData();
                break;
            case R.id.tvPostListing:
                if (dialog != null) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    dialog.show();
                }
                break;
        }
    }


    private void showAllOrganizationCategory(boolean isPopular) {
        try {
            getDashboardActivity().changeFragment(OrganizationFragment.newInstance(isPopular), true, false, 0,
                    0, 0,
                    0, 0, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void showAllIndividualCategory(boolean isPopular) {
        try {
            getDashboardActivity().changeFragment(IndividualFragment.newInstance(isPopular), true, false, 0,
                    0, 0,
                    0, 0, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void showSearchFragmentForPopular(String response) {

        try {
            SearchFragment searchFragment = new SearchFragment();
            searchFragment.setResults(response);
            searchFragment.setPopularDataClick(true);

            getDashboardActivity().changeFragment(searchFragment, true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void showSearchFragment(String response) {
        try {
            SearchFragment searchFragment = new SearchFragment();
            searchFragment.setResults(response);
            searchFragment.setFromHome(true);
            searchFragment.setCurrentPage(1);
            searchFragment.setCity_id(city_id);
            if (clickfrom == 0)
                searchFragment.setKeyword(et_search.getText().toString().trim());
            else
                searchFragment.setCategory_id(category_id);
            getDashboardActivity().changeFragment(searchFragment, true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAdapterClickListener(String action) {

        if (action.equals("Individual")) {
            AddLocationFragment fragment = new AddLocationFragment();
            fragment.setType(getResources().getString(R.string.Individuals));
            fragment.setClickOnMapOff(false);
            try {

                getDashboardActivity().changeFragment(fragment, true, false, 0, R.anim.alpha_visible_anim,
                        0, 0, R.anim.alpha_gone_anim, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (action.equals("Organization")) {
            AddLocationFragment fragment = new AddLocationFragment();
            fragment.setType(Constants.ORGANISATION);
            fragment.setClickOnMapOff(false);
            try {

                getDashboardActivity().changeFragment(fragment, true, false, 0, R.anim.alpha_visible_anim,
                        0, 0, R.anim.alpha_gone_anim, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (action.equals("Events")) {

            try {

                getDashboardActivity().changeFragment(EventListingFragment.newInstance(), true, false, 0, R.anim.alpha_visible_anim,
                        0, 0, R.anim.alpha_gone_anim, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (action.equals("Groups")) {
            try {

                getDashboardActivity().changeFragment(GroupListing.newInstance(), true, false, 0, R.anim.alpha_visible_anim,
                        0, 0, R.anim.alpha_gone_anim, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (action.equals("Custom orders")) {
            try {

                getDashboardActivity().changeFragment(UserCustomOrderListingFragment.newInstance(), true, false, 0, R.anim.alpha_visible_anim,
                        0, 0, R.anim.alpha_gone_anim, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }
}
