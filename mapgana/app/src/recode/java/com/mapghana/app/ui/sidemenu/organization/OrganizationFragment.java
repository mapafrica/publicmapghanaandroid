package com.mapghana.app.ui.sidemenu.organization;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Category;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.iterface.SetOnCategoryClickListener;
import com.mapghana.app.ui.navigationView.MapViewDetailFragment;
import com.mapghana.app.ui.sidemenu.Event.AddEventFragment;
import com.mapghana.app.ui.sidemenu.individual.viewdetails.ViewDetailsFragment;
import com.mapghana.app.ui.sidemenu.organization.adapter.TypeAdapter;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrganizationFragment extends AppBaseFragment implements SetOnCategoryClickListener {

    private RecyclerView rvTypes;
    private AppCompatEditText etSearch;

    private RestClient restClient;
    private ArrayList<Category.DataBean.SubCategoryBean> subCategoryList = new ArrayList<>();

    private ArrayList<Category.DataBean> categoryList = new ArrayList<>();
    private TypeAdapter adapter;
    private boolean hasResponse = false, mIsPopular;
    private String responseData;


    public static OrganizationFragment newInstance(boolean isPopular) {
        OrganizationFragment fragment = new OrganizationFragment();
        Bundle args = new Bundle();
        args.putBoolean("popular",isPopular);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            mIsPopular = getArguments().getBoolean("popular");
        }
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_organization;
    }

    @Override
    public void initializeComponent() {
        init();
        restClient = new RestClient(getContext());

        rvTypes = getView().findViewById(R.id.rvTypes);
        etSearch = getView().findViewById(R.id.etSearch);

        rvTypes.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new TypeAdapter(this, categoryList, categoryList);
        rvTypes.setAdapter(adapter);

        if (!hasResponse) {
            hasResponse = true;
            getCategory(Constants.organizations);
        }

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Organizations));
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }

    @Override
    public void viewCreateFromBackStack() {
        if (hasResponse && responseData != null) {
            updateUI(responseData);
        }
    }

    private void getCategory(String type) {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            if(mIsPopular){
                restClient.callback(this).popularOrganisation();

            }else{
                restClient.callback(this).category(type);

            }
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_CATEGORY) {
                try {
                    responseData = response.body().string();
                    if (responseData == null) {
                        return;
                    }
                    updateUI(responseData);


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);

    }

    private void updateUI(String response) {
        Gson gson = new Gson();
        Category category = gson.fromJson(response, Category.class);
        if (category.getStatus() != 0) {
            clearList();
            categoryList.addAll(category.getData());
            notifyAdapter();
        } else {
            displayErrorDialog("Error", category.getError());
        }

    }

    private void notifyAdapter() {
        if (categoryList.size() > 0) {
            adapter.notifyDataSetChanged();
        }
    }

    private void clearList() {
        if (categoryList != null && categoryList.size() > 0) {
            categoryList.clear();
        }
    }

    @Override
    public void setOnCategoryClickListener(int position, String catId) {
        hideKeyboard();
        try {
            getDashboardActivity().changeFragment(MapViewDetailFragment.newInstance(Constants.ORGANISATION,catId,Constants.ORGANISATION), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim,
                    true);
        } catch (IllegalAccessException e) {

        }
    }
}
