package com.mapghana.app.ui.activity.splash;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.util.PermissionManager;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppBaseActivity {

    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_splash;
    }

    @Override
    public void initializeComponent() {

    }

    private void checkPermission() {
        String token = FirebaseInstanceId.getInstance().getToken();
        if (PermissionManager.areExplicitPermissionsRequired()) {
            List<String> required = PermissionManager.isAllPremissiongranted(SplashActivity.this);
            if (required != null && required.size() > 0) {
                PermissionManager.show(SplashActivity.this,
                        getResources().getString(R.string.app_name), required);
            } else {
//if (checkOverlayOn()) {
                goToNextActivity();
// }
            }
        } else {
            goToNextActivity();
        }
    }

    private void goToNextActivity() {
        if (checkGpsStatus()) {
            final Thread thread = new Thread(() -> {
                try {
                    Thread.sleep(2000);
                    Bundle extras = getIntent().getExtras();
                    StartActivityWithFinish(DashboardActivity.class, extras);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        PermissionManager.requestRunning = false;
        List<String> requiredPermission = new ArrayList<>();
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == -1) {
                requiredPermission.add(permissions[i]);
            }
        }
        if (requiredPermission != null && requiredPermission.size() > 0) {
            PermissionManager.show(SplashActivity.this, "Permission Required", requiredPermission);
        }
    }


    private boolean checkGpsStatus() {
        LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean gpsStatus = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return gpsStatus;
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog;
        alertDialog = new AlertDialog.Builder(SplashActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkGpsStatus()) {
            showSettingsAlert();
        } else {
            checkPermission();
        }
        // setUplocationcheck();
    }

    @Override
    public void setCalenderVisibility(boolean visibility) {

    }

    @Override
    public void setSearchVisibiltyInternal(boolean visibility) {

    }

    @Override
    public void setimgMultiViewVisibility(boolean visibility) {

    }

    @Override
    public void setAtoZZtoAVisibiltyInternal(boolean visibility) {

    }
}
