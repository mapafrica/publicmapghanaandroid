package com.mapghana.app.ui.sidemenu.Object;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.brsoftech.customtimepicker.TimePickerDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Circle;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.plugins.annotation.OnCircleDragListener;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.utils.ColorUtils;
import com.mapghana.R;
import com.mapghana.app.adapter.AddEventGalleryAdapter;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Login;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.MediaAddAdpterListener;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.file_path_handler.FileInformation;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddObjectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddObjectFragment extends AppBaseFragment implements View.OnClickListener, OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener, LocationServiceListner, TimePickerDialog.TimePickerListner, MediaAddAdpterListener {

    private EditText etDeadDrop, etName, etAbout, etPassword, objectMode;
    private Spinner spinnerObjectType;
    private SwitchCompat switchCompatPostVisibility, switchCompatSchedule, switchCompatPasswordProtect, switchCompatOneTimeAccess;
    private RecyclerView recyclerViewGallery;
    private RadioGroup radioGroupLocationCategory;
    private RadioButton radioButtonCategory;
    private FrameLayout frameLayoutLocationMap;
    private TypefaceTextView submit, txtHint, etStartDate, etStartTime, etEndDate, etEndTime;
    private SimpleSpinnerAdapter objectTypeAdapter;
    private TextInputLayout textInputLayoutSizeDeadDrop, textInputLayoutPasswordProtect;
    private double latitude, longitude;
    private LinearLayout llSchedule;
    private List<String> currentMediaGallery;
    private AddEventGalleryAdapter currentGalleryAdapter;
    private String locationReference;
    private RestClient restClient;
    private static final int REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE = 887;
    private List<String> video_mime_List;
    private TimePickerDialog timePickerDialog;
    private int time_type;
    private MapView mapView;
    private Double generalLatitude, generalLongitude;
    private MapboxMap map;
    private PermissionsManager permissionsManager;
    public LocationComponent locationComponent;
    private Point originPoint;
    private SymbolManager symbolManager;
    private CircleManager circleManager;
    private String locationAddress;
    private CustomDatePickerDialog.OnDateListener onDateStartListener, onDateEndListener;
    private String scheduling = "0", postVisibilty = "0", passwordProtect = "0", onTimePassword = "0";

    public AddObjectFragment() {
        // Required empty public constructor
    }

    public static AddObjectFragment newInstance() {
        AddObjectFragment fragment = new AddObjectFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_object;
    }

    @Override
    public void initializeComponent() {
        initToolBar();
        mapView = getView().findViewById(R.id.map_view);

        mapView.getMapAsync(this);
        initViewId(getView());
        initSpinnerObjectType();
        initRecycleLayoutManager();
        initListeners();
        restClient = new RestClient(getContext());
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle("Object");
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }


    private void initViewId(View view) {

        etDeadDrop = view.findViewById(R.id.object_dead_drop);
        etName = view.findViewById(R.id.object_name);
        objectMode = view.findViewById(R.id.object_mode);
        etAbout = view.findViewById(R.id.object_about);
        etPassword = view.findViewById(R.id.object_password_protect);
        etStartDate = view.findViewById(R.id.start_date);
        etStartTime = view.findViewById(R.id.start_time);
        etEndDate = view.findViewById(R.id.end_date);
        etEndTime = view.findViewById(R.id.end_time);
        txtHint = view.findViewById(R.id.txt_hint);
        spinnerObjectType = view.findViewById(R.id.spinner_type);
        switchCompatPostVisibility = view.findViewById(R.id.post_visibility);
        switchCompatPasswordProtect = view.findViewById(R.id.password_protect);
        switchCompatOneTimeAccess = view.findViewById(R.id.switch_one_time_access);
        switchCompatSchedule = view.findViewById(R.id.post_scheduling);
        recyclerViewGallery = view.findViewById(R.id.rv_media);
        radioGroupLocationCategory = view.findViewById(R.id.radio_group_location_category);
        frameLayoutLocationMap = view.findViewById(R.id.map_contanier);
        textInputLayoutSizeDeadDrop = view.findViewById(R.id.text_input_layout_size_deaddrop);
        textInputLayoutPasswordProtect = view.findViewById(R.id.text_input_layout_password_protect);
        llSchedule = view.findViewById(R.id.ll_schedule);
        submit = view.findViewById(R.id.tv_add_object);

        Location location = Locationservice.sharedInstance().getLastLocation();
        if (location != null) {
            originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
            generalLatitude = location.getLatitude();
            generalLongitude = location.getLongitude();
            latitude = location.getLatitude();
            longitude = location.getLongitude();

        }
    }


    private void initListeners() {
        timePickerDialog = new TimePickerDialog(getContext(), Calendar.getInstance(), this);
        etStartDate.setOnClickListener(this);
        etEndDate.setOnClickListener(this);
        etStartTime.setOnClickListener(this);
        etEndTime.setOnClickListener(this);
        submit.setOnClickListener(this);
        radioGroupLocationCategory.setOnCheckedChangeListener((group, checkedId) -> {

            if (checkedId == R.id.radio_general) {
                symbolManager.deleteAll();
                locationReference = getIfLocationReference(R.id.radio_general);
                createPolygon();
                map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originPoint.latitude(), originPoint.longitude()), 6));
            } else if (checkedId == R.id.radio_specific) {
                circleManager.deleteAll();
                locationReference = getIfLocationReference(R.id.radio_specific);
                createMarkerSymbol(originPoint);
            } else {
                locationReference = "";
            }
        });

        spinnerObjectType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (objectTypeAdapter != null) {
                    objectTypeAdapter.setSelGender(position);

                    if (objectTypeAdapter.getSelGender().equalsIgnoreCase("Select type")) {
                        txtHint.setVisibility(View.GONE);
                        return;
                    } else {
                        txtHint.setVisibility(View.VISIBLE);
                    }
                    if (objectTypeAdapter.getSelGender().equalsIgnoreCase("DeadDrop")) {
                        textInputLayoutSizeDeadDrop.setVisibility(View.VISIBLE);

                    } else {
                        textInputLayoutSizeDeadDrop.setVisibility(View.GONE);
                    }
                    txtHint.setText(getString(R.string.hint_format, getHintInfo(objectTypeAdapter.getSelGender())));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        switchCompatPasswordProtect.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                textInputLayoutPasswordProtect.setVisibility(View.VISIBLE);
                passwordProtect = "1";
            } else {
                passwordProtect = "0";
                textInputLayoutPasswordProtect.setVisibility(View.GONE);
            }
        });

        switchCompatPostVisibility.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                postVisibilty = "1";
            } else {
                postVisibilty = "0";
            }
        });
        switchCompatSchedule.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                llSchedule.setVisibility(View.VISIBLE);
                scheduling = "1";
            } else {
                scheduling = "0";
                llSchedule.setVisibility(View.GONE);
            }
        });

        switchCompatOneTimeAccess.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                onTimePassword = "1";
            } else {
                onTimePassword = "0";
            }
        });

        onDateStartListener = date -> {
            if (date != null) {
                SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
                etStartDate.setText(format.format(date));
            }
        };
        onDateEndListener = date -> {
            if (date != null) {
                SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
                etEndDate.setText(format.format(date));
            }
        };

    }

    public String getAddressFromLocation(final double latitude, final double longitude, Context context) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String result = null;

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if ((addresses != null) && (addresses.size() == 1)) {
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            locationAddress = address;
            return address;
        }
        return locationAddress;
    }

    private void createMarkerSymbol(Point point) {
        String markerOption = "Address: " + getAddressFromLocation(point.latitude(), point.longitude(), getActivity());
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.mapbox_marker_icon_default);
        map.getStyle().addImage("individual_marker", bm);
        symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(point.latitude(), point.longitude()))
                .withIconImage("individual_marker")
                //set the below attributes according to your requirements
                .withIconSize(0.5f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withZIndex(10)
                .withTextField(markerOption)
                .withTextHaloColor("rgba(255, 255, 255, 100)")
                .withTextHaloWidth(5.0f)
                .withTextAnchor("top")
                .withTextOffset(new Float[]{0f, 1.5f})
        );

    }

    private String getIfLocationReference(int radioId) {
        switch (radioId) {
            case R.id.radio_general:
                return "general";
            case R.id.radio_specific:
                return "specific";
        }
        return "";
    }

    private void createPolygon() {
        circleManager.create(new CircleOptions()
                .withLatLng(new LatLng(originPoint.latitude(), originPoint.longitude()))
                .withCircleColor(ColorUtils.colorToRgbaString(getActivity().getResources().getColor(R.color.colorYellowtransparent)))
                .withCircleRadius(50f)
                .withDraggable(true)

        );
    }

    private void initSpinnerObjectType() {
        String[] strings = getResources().getStringArray(R.array.object_type);
        objectTypeAdapter = new SimpleSpinnerAdapter(getContext(), strings);
        objectTypeAdapter.setSelGender(0);
        spinnerObjectType.setAdapter(objectTypeAdapter);
    }

    private void initRecycleLayoutManager() {
        currentMediaGallery = new ArrayList<>();
        recyclerViewGallery.setHasFixedSize(true);
        currentGalleryAdapter = new AddEventGalleryAdapter(getActivity(), "previous_media", this, currentMediaGallery);
        recyclerViewGallery.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewGallery.setAdapter(currentGalleryAdapter);

    }

    @Override
    public void onAddClick(String type) {
        multipleGalleryMedia();
    }

    @Override
    public void onDeleteClick(int position, String type) {

    }

    private String getHintInfo(String key) {
        if (key.equalsIgnoreCase("deaddrop")) {
            objectMode.setText("Online");
            return "A deaddrop is a Temporal File Storage area. All users have full access to files in this location.";
        } else if (key.equalsIgnoreCase("zi network")) {
            objectMode.setText("Online");
            return "An independant Wireless Network with a variety of Services. Its like a Wireless HotSpot";
        } else if (key.equalsIgnoreCase("relic")) {
            objectMode.setText("Offline");
            return "An object of importance like a Treasure or Money";
        } else if (key.equalsIgnoreCase("node")) {
            objectMode.setText("Offline");
            return "A node is a publicly accessible computing device capable of basic to advanced operations.\n" +
                    "Its like a computer kiosk in a public place";
        } else if (key.equalsIgnoreCase("powerlync")) {
            objectMode.setText("Offline");
            return "An offgrid power source to charge a variety of Devices";
        }else if (key.equalsIgnoreCase("LOOTBOX")) {
            objectMode.setText("Offline");
            return "A temporal box leased to hold items for a short time span.";
        }
        objectMode.setText("N/A");
        return "These are other undefined objects that can be mapped";

    }

    private void multipleGalleryMedia() {
        Matisse.from(this)
                .choose(MimeType.ofAll(), false)
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(30)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CODE_CHOOSE_GALLERY_MULTIPLE) {
            List<String> paths = Matisse.obtainPathResult(data);
            if (paths != null && paths.size() > 0) {
                currentMediaGallery.addAll(paths);
                refreshMediaGalleryAdapter();
            }
        }
    }

    private void refreshMediaGalleryAdapter() {
        if (currentMediaGallery != null) {
            currentGalleryAdapter.notifyDataSetChanged();
        }
    }

    private void onDobDialog(CustomDatePickerDialog.OnDateListener onDateListener) {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        if (dialog != null) {
            dialog.create(getContext(), true).callback(onDateListener).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_date:
                onDobDialog(onDateStartListener);
                break;
            case R.id.end_date:
                onDobDialog(onDateEndListener);
                break;
            case R.id.start_time:
                displayTimePickerDialog(1);
                break;
            case R.id.end_time:
                displayTimePickerDialog(2);
                break;
            case R.id.tv_add_object:
                onApiCall();
                break;
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_OBJECT) {
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                dismissProgressBar();
                getActivity().onBackPressed();
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        super.onFailResponse(apiId, error);
        dismissProgressBar();
        Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();

    }

    private void displayTimePickerDialog(int type) {
        time_type = type;
        timePickerDialog.show();

    }

    private void onApiCall() {

        displayProgressBar(true);
        if (ConnectionDetector.isNetAvail(getContext())) {


            HashMap<String, String> map = new HashMap<>();
            int ID = 0;
            Gson gson = new Gson();
            String userInfo = SessionManager.getUserInfoResponse(getContext());
            if (!userInfo.equals("")) {
                Login login = gson.fromJson(userInfo, Login.class);
                ID = login.getData().getId();
            }
            map.put(BaseArguments.user_id, String.valueOf(ID));
            map.put(BaseArguments.name, etName.getText().toString());
            map.put(BaseArguments.type, objectTypeAdapter.getSelGender());
            map.put(BaseArguments.scheduling, scheduling);
            map.put(BaseArguments.about, etAbout.getText().toString());
            map.put(BaseArguments.entry_period, etStartDate.getText().toString() + " " + etStartTime.getText().toString());
            map.put(BaseArguments.exit_period, etEndDate.getText().toString() + " " + etEndTime.getText().toString());
            map.put(BaseArguments.location_profile, locationReference);
            map.put(BaseArguments.post_visibility, postVisibilty);
            map.put(BaseArguments.password_protected, passwordProtect);
            map.put(BaseArguments.password, etPassword.getText().toString());
            if ("general".equalsIgnoreCase(locationReference)) {
                map.put(BaseArguments.lat, String.valueOf(generalLatitude));
                map.put(BaseArguments.log, String.valueOf(generalLongitude));
            } else {
                map.put(BaseArguments.lat, String.valueOf(latitude));
                map.put(BaseArguments.log, String.valueOf(longitude));
            }
            map.put(BaseArguments.one_time_use, etPassword.getText().toString());

            MultipartBody.Part[] multiCurrentGalleryPart = new MultipartBody.Part[currentMediaGallery.size()];
            for (int i = 0; i < currentMediaGallery.size(); i++) {
                String url = currentMediaGallery.get(0);
                FilePathHelper filePathHelper = new FilePathHelper();
                FileInformation fileInformation = filePathHelper.getUriInformation(getContext(), Uri.parse(url));
                String mimeType = fileInformation.getMimeType();
                if (video_mime_List.contains(mimeType)) {
                    File file = new File(currentMediaGallery.get(i));
                    RequestBody media = RequestBody.create(RetrofitUtils.MEDIA_TYPE_VIDEO, file);
                    multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.object_image + "[" + i + "]", file.getName(), media);

                } else {
                    File file = new File(currentMediaGallery.get(i));
                    RequestBody media = RequestBody.create(MediaType.parse("image/*"), file);
                    multiCurrentGalleryPart[i] = MultipartBody.Part.createFormData(BaseArguments.object_image + "[" + i + "]", file.getName(), media);
                }
            }
            restClient.callback(this).postAddObject(getContext(), RetrofitUtils.createMultipartRequest(map), multiCurrentGalleryPart);
        } else {
            displayToast("Please check internet connection");
        }
    }


    @Override
    public void OnDoneButton(Dialog dialog, Calendar c) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(c.getTime());

        if (time_type == 1) {
            etStartTime.setText(time);
        }
        if (time_type == 2) {
            etEndTime.setText(time);
        }
        dialog.dismiss();
    }

    @Override
    public void OnCancelButton(Dialog dialog) {
        timePickerDialog.dismiss();
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.map = mapboxMap;
        mapboxMap.addOnMapClickListener(this);
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {

            enableLocationComponent(style);
            symbolManager = new SymbolManager(mapView, map, style);
            symbolManager.setIconAllowOverlap(false);  //your choice t/f
            symbolManager.setTextAllowOverlap(false);  //your choice t/f
            circleManager = new CircleManager(mapView, map, style);
            Location location = Locationservice.sharedInstance().getLastLocation();
            if (location != null) {
                Point originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());

            }

            circleManager.addDragListener(new OnCircleDragListener() {
                @Override
                public void onAnnotationDragStarted(Circle annotation) {

                }

                @Override
                public void onAnnotationDrag(Circle annotation) {
                    generalLatitude = annotation.getLatLng().getLatitude();
                    generalLongitude = annotation.getLatLng().getLongitude();
                }

                @Override
                public void onAnnotationDragFinished(Circle annotation) {

                }
            });


        });
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = map.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(map.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void userLocationChange(Location location) {
        Toast.makeText(getActivity(), "" + location.getLatitude(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        symbolManager.deleteAll();
        latitude = point.getLatitude();
        longitude = point.getLongitude();
        createMarkerSymbol(Point.fromLngLat(point.getLongitude(), point.getLatitude()));
        return true;
    }
}
