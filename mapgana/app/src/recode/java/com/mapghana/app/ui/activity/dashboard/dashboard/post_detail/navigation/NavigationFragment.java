package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.navigation;


import android.location.Location;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.mapbox.directions.DirectionsCriteria;
import com.mapbox.directions.MapboxDirections;
import com.mapbox.directions.service.models.DirectionsResponse;
import com.mapbox.directions.service.models.DirectionsRoute;
import com.mapbox.directions.service.models.Waypoint;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.GetDetails;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationFragment extends AppBaseFragment
        implements LocationServiceListner {

    private int zoomLevel;
    private LatLng latLng;
    double latitude, longitude;
    String name;
    String address;
    ImageButton imgMyLoc;
    private int TYPE;
    private int resource;
    // private MapboxMap mapboxMap;
    private GetDetails getDetails;
    private DirectionsRoute currentRoute = null;
    private Marker marker;

    // private  DirectionsRoute directionsRoute;
    String category = "";


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_navigation;
    }

    @Override
    public void initializeComponent() {
        init();
        zoomLevel = 13;

         imgMyLoc = (ImageButton) getView().findViewById(R.id.imgMyLoc);

        imgMyLoc.setOnClickListener(this);

        getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                initMap();
                setMapPadding();
                setData();
            }
        });
    }
    private void setMapPadding() {
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().setMapPadding(0,
                    Math.round(getResources().getDimension(R.dimen.dp50)),
                    0, Math.round(getResources().getDimension(R.dimen.dp20)));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void viewCreateFromBackStack() {
        super.viewCreateFromBackStack();
        init();
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
    }

    private void initMap() {
        if (!isVisible())return;
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().clearMarker();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    private void setData() {
        if (!isVisible())return;

        if (getDetails != null && getDetails.getStatus() != 0 && getDetails.getData() != null) {
            GetDetails.DataBean dataBean = getDetails.getData();


            latitude = dataBean.getLat();
            longitude = dataBean.getLog();

            try {
                latLng = new LatLng(latitude, longitude);
            } catch (IllegalArgumentException e) {
            }

            name = dataBean.getName();
            address = dataBean.getAddress();
            initializeType(getDetails);
            if (latLng != null) {
                setMarkerFirstTime(latLng, name, category + "\n" + address);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
        }
    }

    private void setMarkersOnMapAgain() {
        zoomLevel = 17;
        double lat = 0;
        try {
            lat = getDashboardActivity().mCurrentLatitude;
            double log = getDashboardActivity().mCurrentLongitude;
            LatLng latLng_currentPos = new LatLng(lat, log);
            Location location = new Location("");
            location.setLatitude(lat);
            location.setLongitude(log);
            moveMarker(location);
            getDashboardActivity().getMapHandler().getMapBoxFragment().animateMap(latLng_currentPos, zoomLevel);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    private void setMarkerFirstTime(LatLng latLng, String title, String snippet) {
      if ( !isVisible()) return;
        String status = "";
        if (TYPE == 1) {
            status = Constants.verified;
        } else if (TYPE == 2) {
            status = Constants.unverified;
        } else {
            //(type==3)
            status = Constants.anonymous;
        }
        final List<LatLng> boundList = new ArrayList<>();
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().
                    addMarker(getContext(), resource, latLng, title, "Address: " + snippet + "\nStatus: " + status);
            boundList.add(latLng);

            double lat = getDashboardActivity().mCurrentLatitude;
            double log = getDashboardActivity().mCurrentLongitude;
            LatLng currentPos = new LatLng(lat, log);
            marker = getDashboardActivity().getMapHandler().getMapBoxFragment().addMarker(getContext(), R.mipmap.my_location, currentPos, "You", "");
            boundList.add(currentPos);
            getDashboardActivity().getMapHandler().getMapBoxFragment().boundMap(boundList);
            Waypoint destination = new Waypoint(longitude, latitude);
            Waypoint origin = new Waypoint(log, lat);
            getRoute(origin, destination);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    public GetDetails getGetDetails() {
        return getDetails;
    }

    public void setGetDetails(GetDetails getDetails) {
        this.getDetails = getDetails;
    }

    /*public DirectionsRoute getDirectionsRoute() {
        return directionsRoute;
    }

    public void setDirectionsRoute(DirectionsRoute directionsRoute) {
        this.directionsRoute = directionsRoute;
    }*/

    private void initializeType(GetDetails data) {
        /****************/
        if (data.getData().getPost_for() != null &&
                data.getData().getPost_for().equalsIgnoreCase(Constants.individuals)) {
            String status = "";
            String gender = "";

            status = data.getData().getStatus().trim();
            gender = data.getData().getGender().trim();
            if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                if (status != null && !status.equals("") &&
                        status.equalsIgnoreCase(Constants.verified)) {
                    resource = R.mipmap.individual_green_gril;
                    TYPE = 1;
                } else if (status != null && !status.equals("") &&
                        status.equalsIgnoreCase(Constants.unverified)) {
                    resource = R.mipmap.individual_red_girl;
                    TYPE = 2;
                } else {
                    resource = R.mipmap.indi_girl_gray;
                    TYPE = 3;

                }
            } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                if (status != null && !status.equals("") &&
                        status.equalsIgnoreCase(Constants.verified)) {
                    resource = R.mipmap.individual_green_man;
                    TYPE = 1;
                } else if (status != null && !status.equals("") &&
                        status.equalsIgnoreCase(Constants.unverified)) {
                    TYPE = 2;
                    resource = R.mipmap.individual_red_man;
                } else {
                    resource = R.mipmap.individual_gray_man;
                    TYPE = 3;

                }
            }
            category = "Occupation: " + data.getData().getCategory().getName();

        } else if (data.getData().getPost_for() != null &&
                data.getData().getPost_for().equalsIgnoreCase(Constants.organizations)) {
            if (data.getData().getStatus() != null &&
                    data.getData().getStatus().equalsIgnoreCase(Constants.verified)) {
                resource = R.mipmap.location_gree_org;
                TYPE = 1;

            } else if (data.getData().getStatus() != null &&
                    data.getData().getStatus().equalsIgnoreCase(Constants.unverified)) {
                resource = R.mipmap.location_red_org;
                TYPE = 2;

            } else {
                //(type==3)
                resource = R.mipmap.location_gry_org;
                TYPE = 3;

            }
            //  category="Category: "+data.getData().getSubcategory().getName();

            category = "Category: ";
            GetDetails.DataBean.CategoryBean categoryObj = data.getData().getCategory();
            if (categoryObj != null) {
                category = category + categoryObj.getName();
            }
            GetDetails.DataBean.SubcategoryBean subcategoryObj = data.getData().getSubcategory();
            if (subcategoryObj != null) {
                category = category + " (" + subcategoryObj.getName() + ")";
            }

        }
    }


    private void getRoute(Waypoint origin, Waypoint destination) {

        MapboxDirections directions = new MapboxDirections.Builder()
                .setAccessToken(getString(R.string.mapbox_api_token))
                .setOrigin(origin)
                .setDestination(destination)
                .setProfile(DirectionsCriteria.PROFILE_DRIVING)
                .build();
        displayProgressBar(false);
        directions.enqueue(new retrofit.Callback<DirectionsResponse>() {
            @Override
            public void onResponse(retrofit.Response<DirectionsResponse> response, Retrofit retrofit) {
                // Display some info about the route
                dismissProgressBar();

                if (response != null && response.isSuccess() && response.body().getRoutes() != null && response.body().getRoutes().size() > 0) {
                    currentRoute = response.body().getRoutes().get(0);
                    displayToast(String.format("You are %d meters \nfrom your destination", currentRoute.getDistance()));

                    // Draw the route on the map
                    drawRoute(currentRoute);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dismissProgressBar();
                displayErrorDialog("Error", t.getMessage());
            }
        });
    }

    private void drawRoute(DirectionsRoute route) {
        // Convert List<Waypoint> into LatLng[]
        List<Waypoint> waypoints = route.getGeometry().getWaypoints();
        LatLng[] point = new LatLng[waypoints.size()];
        for (int i = 0; i < waypoints.size(); i++) {
            point[i] = new LatLng(
                    waypoints.get(i).getLatitude(),
                    waypoints.get(i).getLongitude());
        }

        // Draw Points on MapView
        try {
            getDashboardActivity().getMapboxMap().addPolyline(new PolylineOptions()
                    .add(point)
                    .color(getResources().getColor(R.color.colorBlue))
                    .width(5));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void userLocationChange(Location location) {
        moveMarker(location);
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

    private void moveMarker(Location location) {
        if (location == null) {
            return;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (marker != null) {
            marker.setPosition(latLng);

        }
    }


}
