package com.mapghana.app.ui.sidemenu.customOrder;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.CustomOrderModel;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.UserCustomOrderListModel;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.AppPreferences;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class UserCustomOrderListingFragment extends AppBaseFragment implements CustomOrderAdapter.CustomOrderInterface {

    private RecyclerView mRecyclerView;
    private FloatingActionButton mAddCustomOrder;
    private CustomOrderAdapter customOrderAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView mNoDataFound;

    public static UserCustomOrderListingFragment newInstance() {
        UserCustomOrderListingFragment fragment = new UserCustomOrderListingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle("Custom Order");
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_user_custom_order_listing;
    }

    @Override
    public void initializeComponent() {
        initToolBar();
        findViewById(getView());
    }

    @Override
    public void reInitializeComponent() {
    }

    private void findViewById(View view) {
        mRecyclerView = view.findViewById(R.id.custom_order);
        mAddCustomOrder = view.findViewById(R.id.add_custom_order);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mNoDataFound = view.findViewById(R.id.img_no_data_found);
        mRecyclerView.setHasFixedSize(true);

        Session session = AppPreferences.getSession();
        callGetApi(session);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            callGetApi(session);
        });
        mAddCustomOrder.setOnClickListener(view1 -> {

            if (!TextUtils.isEmpty(session.getUsername())) {
                navigateToAddCustomOrderScreen((AppBaseActivity) getActivity());
            } else {
                try {
                    getDashboardActivity().showLoginMessageDialog();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        initRecyclerView();
    }

    private void callGetApi(Session session) {
        displayProgressBar(false);
        RestClient restClient = new RestClient(getContext());
        restClient.callback(this).getUserCustomOrderList(String.valueOf(session.getId()));
    }

    private void initRecyclerView() {
        customOrderAdapter = new CustomOrderAdapter(this, (AppBaseActivity) getActivity(),new ArrayList<>(), true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(customOrderAdapter);
    }

    public void navigateToAddCustomOrderScreen(AppBaseActivity baseActivity) {
        baseActivity.pushFragment(AddCustomOrderFragment.newInstance(), true);
    }

    public void navigateToAdminCustomOrderScreen(AppBaseActivity baseActivity, String id) {
        baseActivity.pushFragment(AdminCustomListingFragment.newInstance(id), true);
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_USER_CUSTOM_ORDER_LIST) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    UserCustomOrderListModel objectListingModel = gson.fromJson(s, UserCustomOrderListModel.class);
                    swipeRefreshLayout.setRefreshing(false);
                    if (objectListingModel.getStatus() == 1) {
                        if (objectListingModel.getData().size() > 0) {
                            mNoDataFound.setVisibility(View.GONE);
                        } else {
                            mNoDataFound.setVisibility(View.VISIBLE);
                        }
                        customOrderAdapter.setDataList(objectListingModel.getData());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        swipeRefreshLayout.setRefreshing(false);
        displayErrorDialog("Error", error);

    }

    @Override
    public void onPayClick(CustomOrderModel model) {

    }

    @Override
    public void ItemClick(CustomOrderModel model) {
        navigateToAdminCustomOrderScreen((AppBaseActivity) getActivity(), model.getId());
    }
}
