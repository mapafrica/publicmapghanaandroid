package com.mapghana.app.interfaces;

public interface OnBottomReachedListener {
    void onBottomReached(int position);
}
