package com.mapghana.app.retrofit;

import com.mapghana.app.rest.BaseArguments;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Rest {


    @FormUrlEncoded
    @POST(BaseArguments.signup)
    Call<ResponseBody> signup(@Field(BaseArguments.name) String name,
                              @Field(BaseArguments.username) String full_name,
                              @Field(BaseArguments.password) String password,
                              @Field(BaseArguments.email) String email,
                              @Field(BaseArguments.phone_number) String phone_number,
                              @Field(BaseArguments.device_type) String device_type,
                              @Field(BaseArguments.device_id) String device_id);


    @FormUrlEncoded
    @POST(BaseArguments.login)
    Call<ResponseBody> login(
            @Field(BaseArguments.email) String email,
            @Field(BaseArguments.password) String password,
            @Field(BaseArguments.device_type) String device_type,
            @Field(BaseArguments.device_id) String device_id);

    @FormUrlEncoded
    @POST(BaseArguments.forgot)
    Call<ResponseBody> forgot(
            @Field(BaseArguments.email) String email);


    @GET(BaseArguments.profile)
    Call<ResponseBody> getProfile(
            @Query(BaseArguments.id) int id);

    @FormUrlEncoded
    @POST(BaseArguments.TRANSACTION)
    Call<ResponseBody> transaction(@Field(BaseArguments.event_id) String event_id,
                                   @Field(BaseArguments.amount) String amount,
                                   @Field(BaseArguments.transection_id) String transection_id,
                                   @Field(BaseArguments.email) String email,
                                   @Field(BaseArguments.first_name) String first_name,
                                   @Field(BaseArguments.last_name) String last_name,
                                   @Field(BaseArguments.type) String type,
                                   @Field(BaseArguments.paymentType) String paymentType);

    @FormUrlEncoded
    @POST(BaseArguments.profile)
    Call<ResponseBody> updateProfile(@Field(BaseArguments.id) int id,
                                     @Field(BaseArguments.username) String username,
                                     @Field(BaseArguments.name) String name,
                                     @Field(BaseArguments.email) String email,
                                     @Field(BaseArguments.dob) String dob,
                                     @Field(BaseArguments.gender) String gender,
                                     @Field(BaseArguments.phone_number) String phone_number,
                                     @Field(BaseArguments.location) String location,
                                     @Field(BaseArguments.device_type) String device_type,
                                     @Field(BaseArguments.device_id) String device_id);

    @Multipart
    @POST(BaseArguments.profile)
    Call<ResponseBody> updateProfileWithImage(@PartMap HashMap<String, RequestBody> map,
                                              @Part MultipartBody.Part image);

    @GET(BaseArguments.category)
    Call<ResponseBody> category(@Query(BaseArguments.type) String type);

    @GET(BaseArguments.category)
    Call<ResponseBody> category();


    @GET(BaseArguments.city)
    Call<ResponseBody> city();


    @Multipart
    @POST(BaseArguments.post)
    Call<ResponseBody> post_individual(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part image,
                                       @Part MultipartBody.Part[] gallery_images);

    @Multipart
    @POST(BaseArguments.post)
    Call<ResponseBody> post_individual(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part image);

    @Multipart
    @POST(BaseArguments.URL_ADD_CUSTOM_ORDER)
    Call<ResponseBody> postAddCustomOrder(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part image);


    @Multipart
    @POST(BaseArguments.post)
    Call<ResponseBody> post_organization(@PartMap HashMap<String, RequestBody> map,
                                         @Part MultipartBody.Part[] gallery_images);

    @GET(BaseArguments.contact)
    Call<ResponseBody> contact();

    @FormUrlEncoded
    @POST(BaseArguments.contact)
    Call<ResponseBody> contact(@Field(BaseArguments.name) String name,
                               @Field(BaseArguments.email) String email,
                               @Field(BaseArguments.phone) String phone,
                               @Field(BaseArguments.message) String message);

    @GET(BaseArguments.logout + "/{user_id}")
    Call<ResponseBody> logout(@Path(BaseArguments.user_id) int user_id);

    @FormUrlEncoded
    @POST(BaseArguments.URL_USER_CUSTOM_ORDER_LIST)
    Call<ResponseBody> getUserCustomList(@Field(BaseArguments.user_id) String user_id);


    @FormUrlEncoded
    @POST(BaseArguments.URL_USER_STATUS)
    Call<ResponseBody> postUserStatus(@Field(BaseArguments.user_id) String user_id, @Field(BaseArguments.online_status) int online_status);


    @Multipart
    @POST(BaseArguments.post_send_review)
    Call<ResponseBody> post_send_review(@PartMap HashMap<String, RequestBody> map);


    @GET(BaseArguments.post_popular_listing_organizations)
    Call<ResponseBody> post_listing_popular_organizations();

    @GET(BaseArguments.post_popular_listing_Individuals)
    Call<ResponseBody> post_listing_popular_individuals();

    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.lat) String lat,
                                                  @Query(BaseArguments.log) String log,
                                                  @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id);


    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id,
                                                  @Query(BaseArguments.city_id) String city_id);

    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id,
                                                  @Query(BaseArguments.lat) String lat,
                                                  @Query(BaseArguments.log) String log,
                                                  @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.post_listing_organizations)
    Call<ResponseBody> post_listing_organizations(@Query(BaseArguments.sub_category_id) String sub_category_id,
                                                  @Query(BaseArguments.city_id) String city_id,
                                                  @Query(BaseArguments.lat) String lat,
                                                  @Query(BaseArguments.log) String log,
                                                  @Query(BaseArguments.distance) String distance);


    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id,
                                                @Query(BaseArguments.city_id) String city_id,
                                                @Query(BaseArguments.lat) String lat,
                                                @Query(BaseArguments.log) String log,
                                                @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id,
                                                @Query(BaseArguments.city_id) String city_id);

    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id,
                                                @Query(BaseArguments.lat) String lat,
                                                @Query(BaseArguments.log) String log,
                                                @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.category_id) String category_id);

    @GET(BaseArguments.post_listing_Individuals)
    Call<ResponseBody> post_listing_Individuals(@Query(BaseArguments.lat) String lat,
                                                @Query(BaseArguments.log) String log,
                                                @Query(BaseArguments.distance) String distance);

    @GET(BaseArguments.post_listing_event)
    Call<ResponseBody> post_listing_Event();

    @GET(BaseArguments.post_listing_object)
    Call<ResponseBody> post_listing_object();

    @GET(BaseArguments.post_listing_type + "/{type}")
    Call<ResponseBody> post_listing_type(@Path("type") String type);

    @GET(BaseArguments.post + "/{post_id}")
    Call<ResponseBody> getDetailsOfOnePost(@Path(BaseArguments.post_id) String post_id);

    @GET
    Call<ResponseBody> getDetailsOfOneCategoryObject(@Url String url);

    @GET(BaseArguments.search)
    Call<ResponseBody> search(@Query(BaseArguments.keyword) String keyword,
                              @Query(BaseArguments.page) String page);

    @GET(BaseArguments.search)
    Call<ResponseBody> searchFilter(@Query(BaseArguments.keyword) String keyword,
                                    @Query(BaseArguments.category_id) String category_id,
                                    @Query(BaseArguments.city_id) String city_id,
                                    @Query(BaseArguments.lat) String lat,
                                    @Query(BaseArguments.log) String log,
                                    @Query(BaseArguments.distance) String distance,
                                    @Query(BaseArguments.page) String page);

    @GET(BaseArguments.ads)
    Call<ResponseBody> ads();

    @GET(BaseArguments.popular_category)
    Call<ResponseBody> popular_category();

    @FormUrlEncoded
    @POST(BaseArguments.signup)
    Call<ResponseBody> newSignUp(@Field(BaseArguments.name) String name,
                                 @Field(BaseArguments.username) String full_name,
                                 @Field(BaseArguments.email) String email,
                                 @Field(BaseArguments.phone_number) String phone_number,
                                 @Field(BaseArguments.device_type) String device_type,
                                 @Field(BaseArguments.device_id) String device_id);

    @GET(BaseArguments.event_listing)
    Call<ResponseBody> getEventListing();

    @GET(BaseArguments.EVENT_CALENDAR)
    Call<ResponseBody> getEventCalendarDates();

    @GET
    Call<ResponseBody> getDeeplinking(@Url String url);

    @GET
    Call<ResponseBody> getPrice(@Url String url);

    @GET("http://67.205.155.52/mapghana/public/api/v1/events/{id}")
    Call<ResponseBody> getEventDetailById(@Path("id") String id);

    @GET
    Call<ResponseBody> getEventSearchList(@Url String url);

    @GET
    Call<ResponseBody> getCheckMapGhanaId(@Url String url);


    @GET
    Call<ResponseBody> getEventListingByDate(@Url String url);


    @GET(BaseArguments.OBJECT_LISTING_CATEGORY)
    Call<ResponseBody> getObjectListingCategory();


    @GET(BaseArguments.EVENT_ID_LISTING_FOR_GROUP)
    Call<ResponseBody> getEventIdListingForGroup();

    @GET
    Call<ResponseBody> getGroupGridListing(@Url String url);

    @GET
    Call<ResponseBody> getGroupListing(@Url String url);

    @Multipart
    @POST(BaseArguments.add_event)
    Call<ResponseBody> postAddEventApi(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part[] peviousGallery, @Part MultipartBody.Part[] currentGallery, @Part MultipartBody.Part[] posterMedia);

    @FormUrlEncoded
    @POST(BaseArguments.SAVE_RSVP_EVENT)
    Call<ResponseBody> postRsvp(
            @Field(BaseArguments.user_id) int user_id,
            @Field(BaseArguments.event_id) int event_id);

    @GET
    Call<ResponseBody> getRSVPEvent(@Url String url);

    @Multipart
    @POST(BaseArguments.add_group)
    Call<ResponseBody> postAddGroupApi(@PartMap HashMap<String, RequestBody> map,
                                       @Part MultipartBody.Part[] peviousGallery, @Part MultipartBody.Part[] posterMedia);


    @Multipart
    @POST(BaseArguments.add_object)
    Call<ResponseBody> postAddObject(@PartMap HashMap<String, RequestBody> map,
                                     @Part MultipartBody.Part[] gallery_images);
}
