package com.mapghana.app.ui.sidemenu.Group;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.GroupGridListingAdapter;
import com.mapghana.app.adapter.GroupListingAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.dialog.PopUpGroupDetail;
import com.mapghana.app.interfaces.GroupListCallBack;
import com.mapghana.app.model.GroupData;
import com.mapghana.app.model.GroupGridListing;
import com.mapghana.app.model.GroupGridSectionDataModel;
import com.mapghana.app.model.GroupListData;
import com.mapghana.app.model.GroupListListing;
import com.mapghana.app.model.PaymentResponse;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.TransactionPrice;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class GroupListing extends AppBaseFragment implements GroupListCallBack {

    private RecyclerView recyclerView;
    private ArrayList<GroupData> data;
    private Context context;
    private GroupGridListingAdapter gridAdapter;
    private ArrayList<GroupGridSectionDataModel> allSampleData;
    private GroupListingAdapter adapter;
    private RestClient restClient;
    private String AlphabetOrder = "AtoZ";
    private TextView emptyView;
    private String viewType = "Grid";
    private PopUpGroupDetail customDialog;
    private String orderType = "AtoZ";
    private AlertDialog.Builder builder;
    private TransactionPrice transactionPrice;
    private String screenType, paymentType;

    public static GroupListing newInstance() {
        GroupListing fragment = new GroupListing();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_object_listing;
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle("Groups");
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(true);
        getNavHandler().setAtoZZtoAVisibiltyInternal(true);
    }


    @Override
    public void initializeComponent() {

        initToolBar();
        restClient = new RestClient(getContext());
        restClient.callback(this).getGroupListing(orderType);
        recyclerView = getView().findViewById(R.id.rv_group_listing);
        emptyView = getView().findViewById(R.id.empty_view);
        data = new ArrayList<>();
        allSampleData = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        adapter = new GroupListingAdapter(context, data, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        transactionPrice = AppPreferences.getTransactionPrice();
        recyclerView.setAdapter(adapter);
        builder = new AlertDialog.Builder(getActivity());
        paymentConfirmationDialog();
        getView().findViewById(R.id.fab).setOnClickListener(view -> {

            Session session = AppPreferences.getSession();
            if (!TextUtils.isEmpty(session.getUsername())) {

                showDialog();

            } else {
                try {
                    getDashboardActivity().showLoginMessageDialog();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        });

        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMultiViewListener,
                new IntentFilter(Constants.LOCAL_BROADCAST_MULTIVIEW)
        );

        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMultiViewListener,
                new IntentFilter(Constants.LOCAL_BROADCAST_AtoZ_GROUP)


        );
        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                paymentReceiver,
                new IntentFilter(Constants.LOCAL_BROADCAST_PAYMENT)
        );

        displayProgressBar(false);
    }

    private void showDialog() {
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Mapghana Payment");
        alert.show();
    }

    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver mMultiViewListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_LIST)) {
                data.clear();
                viewType = "List";
                displayProgressBar(false);
                setListRecyclerViewAdapter();
//                callListApi(AlphabetOrder);
            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_GRID)) {
                allSampleData.clear();
                viewType = "Grid";
                setGridRecyclerViewAdapter();
                displayProgressBar(false);
//                callGridApi(AlphabetOrder);
            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_AtoZ_GROUP)) {
                if (orderType.equalsIgnoreCase("AtoZ")) {
                    orderType = "ZtoA";
                } else {
                    orderType = "AtoZ";
                }
            }
            setOrderTypeList(orderType);
        }
    };

    private void setOrderTypeList(String orderType) {
        if ("List".equalsIgnoreCase(viewType)) {
            callListApi(orderType);

        } else {
            callGridApi(orderType);
            setGridRecyclerViewAdapter();
        }
    }

    private void setListRecyclerViewAdapter() {
        adapter = new GroupListingAdapter(context, data, this);
        recyclerView.setAdapter(adapter);
    }

    private void setGridRecyclerViewAdapter() {
        gridAdapter = new GroupGridListingAdapter(context, allSampleData, this);
        recyclerView.setAdapter(gridAdapter);
    }

    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver paymentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_SUCCESS)) {

                screenType = intent.getStringExtra("screen_type");
                paymentType = intent.getStringExtra("payment_type");
                Gson gson = new Gson();
                PaymentResponse paymentResponse = gson.fromJson(intent.getStringExtra(Constants.data), PaymentResponse.class);
                callPaymentToServer(paymentResponse);

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_ERROR)) {


            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_CANCELLED)) {

            }
        }
    };


    private void callPaymentToServer(PaymentResponse paymentResponse) {
        Session session = AppPreferences.getSession();
        restClient.callback(this).transaction("0", String.valueOf(paymentResponse.getData().getAmount()), paymentResponse.getData().getPaymentId(), session.getEmail(), session.getName(), session.getName(), screenType, paymentType);
    }

    private void paymentConfirmationDialog() {


        builder.setMessage("Groups Allow you to broadcast Chat Groups so that other people outside your social circle with similar Interests can connect. Cost = "+transactionPrice.getGroup().getAdd_group_charges()+ " GHS")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) ->  navigateToAddGroupScreen((AppBaseActivity) getActivity()));
    }

    private void callGridApi(String order) {
        String url = BaseArguments.GROUP_GRID_LISTING + "" + order;
        restClient.callback(this).getGroupGridListing(url);
    }

    private void callListApi(String order) {
        String url = BaseArguments.GROUP_LISTING + "" + order;
        restClient.callback(this).getGroupListing(url);
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_GROUP_LISTING) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GroupListListing groupListListing = gson.fromJson(s, GroupListListing.class);
                    if (groupListListing.getStatus() == 1) {
                        viewType = "List";
                        setRecyclerViewContent(groupListListing.getData());

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_TRANSACTION) {

                Utils.showToast(getActivity(), getView(), "transaction successfully");
            } else if (apiId == ApiIds.ID_GROUP_GRID_LISTING) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GroupGridListing groupListListing = gson.fromJson(s, GroupGridListing.class);
                    if (groupListListing.getStatus() == 1) {
                        viewType = "Grid";
                        setRecyclerGridViewContent(groupListListing);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);

    }

    private void setRecyclerGridViewContent(GroupGridListing groupGridListing) {
        allSampleData.clear();
        AppPreferences.setImagePath(groupGridListing.getImage_path());
        for (GroupListData groupListData : groupGridListing.getData()) {

            if (groupListData.getData().size() != 0) {

                GroupGridSectionDataModel dm = new GroupGridSectionDataModel();

                dm.setHeaderTitle(groupListData.getLetter());
                ArrayList<GroupData> singleItem = new ArrayList<GroupData>();
                for (GroupData detail : groupListData.getData()) {
                    detail.setImage(detail.getImage());
                    singleItem.add(detail);
                }

                dm.setAllItemsInSection(singleItem);

                allSampleData.add(dm);
            }
        }

        if (allSampleData.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
        gridAdapter.setDataList(allSampleData);
    }

    private void setRecyclerViewContent(GroupListData groupListData) {
        data.clear();

        for (GroupData eventListingContent : groupListData.getData()) {

            data.add(eventListingContent);
        }

        adapter.setDataList(data);
    }

    public void navigateToAddGroupScreen(AppBaseActivity baseActivity) {
        baseActivity.pushFragment(AddGroup.newInstance(), true);
    }

    public void navigateToGroupDetailScreen(AppBaseActivity baseActivity, String title, GroupData data) {
        baseActivity.pushFragment(GroupDetailFragment.newInstance(title, data), true);
    }

    private void showPopUp(GroupData model, String type, GroupListCallBack listener) {
        customDialog = new PopUpGroupDetail(getActivity(), listener, model, true, type);
        customDialog.setCancelable(false);
        customDialog.show();
    }


    @Override
    public void onClickEvent(GroupData model, String type) {
        showPopUp(model, type, GroupListing.this);
    }

    @Override
    public void onPopUpClickEvent(GroupData model) {
        navigateToGroupDetailScreen((AppBaseActivity) getActivity(), model.getGroup_name(), model);
    }

    @Override
    public void onCloseIcon() {
        customDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMultiViewListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        setOrderTypeList(orderType);
    }
}

