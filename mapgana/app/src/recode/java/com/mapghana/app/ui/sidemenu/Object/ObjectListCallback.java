package com.mapghana.app.ui.sidemenu.Object;

import com.mapghana.app.model.ObjectListingContent;

public interface ObjectListCallback {
    void onClickEvent(ObjectListingContent model);
}
