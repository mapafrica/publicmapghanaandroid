package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.model.GalleryPojo;
import com.mapghana.app.utils.Constants;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.util.file_path_handler.FilePathHelper;

import java.util.ArrayList;
import java.util.List;

public class SlidingImageAdapter extends PagerAdapter {


    private List<GalleryPojo> imageList;
    private List<String> image_mime_List;
    private List<String> video_mime_List;

    private VideoClickListener videoClickListener;
    private AdapterClickListener adapterClickListener;
    private LayoutInflater layoutInflater;
    private Context context;
    private static final int INTERVAL = 5 * 1000;

    private int play_status = 0;


    public SlidingImageAdapter(Context context, List<GalleryPojo> imageList, AdapterClickListener adapterClickListener, VideoClickListener videoClickListener) {
        this.imageList = imageList;
        this.context = context;
        this.adapterClickListener = adapterClickListener;
        this.videoClickListener = videoClickListener;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        image_mime_List = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        image_mime_List.addAll(Constants.getImageMimeList());
        video_mime_List.addAll(Constants.getVideoMimeList());
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View img_layout = layoutInflater.inflate(R.layout.sliding_images_layout, container, false);
        assert img_layout != null;
        RelativeLayout rl_main_view = img_layout.findViewById(R.id.rl_main_view);
        ImageView imageView = img_layout.findViewById(R.id.slide_image);
        final VideoView video_view = img_layout.findViewById(R.id.video_view);
        final ImageButton ib_start = img_layout.findViewById(R.id.ib_start);
        final ImageButton ib_full_screen = (ImageButton) img_layout.findViewById(R.id.ib_full_screen);
        LottieAnimationView progressBar = img_layout.findViewById(R.id.progressBar);
        //ib_full_screen.setVisibility(View.GONE);
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ib_full_screen.setVisibility(View.GONE);
                ib_start.setVisibility(View.GONE);
                handler.removeCallbacks(this);
            }
        };

        FilePathHelper filePathHelper = new FilePathHelper();
        final String mimeType = filePathHelper.getMimeType(imageList.get(position).getImage());
        String mediaPath = imageList.get(position).getImage();
        if (image_mime_List.contains(mimeType)) {
            imageView.setVisibility(View.VISIBLE);
            video_view.setVisibility(View.GONE);
            ib_start.setVisibility(View.GONE);

            Glide.with(context).load(mediaPath)
                    .override(500, 300).placeholder(R.drawable.pleasewait).dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(imageView);
        } else if (video_mime_List.contains(mimeType)) {
            imageView.setVisibility(View.GONE);
            video_view.setVisibility(View.VISIBLE);
            ib_start.setVisibility(View.VISIBLE);
            video_view.setVideoURI(Uri.parse(mediaPath));
            video_view.seekTo(1);
            video_view.setOnPreparedListener(mp -> {
                progressBar.setVisibility(View.GONE);
                ib_start.setVisibility(View.VISIBLE);
            });
            video_view.setOnErrorListener((mp, what, extra) -> {
                progressBar.setVisibility(View.GONE);
                return true;
            });
        }
        container.addView(img_layout, 0);

        video_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_status = 0;
                video_view.seekTo(1);
                ib_start.setImageResource(R.mipmap.play_icon);
                ib_start.setVisibility(View.VISIBLE);
                ib_full_screen.setVisibility(View.VISIBLE);
            }
        });

        rl_main_view.setOnClickListener(v -> {
            if (video_mime_List.contains(mimeType)) {
                ib_start.setVisibility(View.VISIBLE);
            }
            ib_full_screen.setVisibility(View.VISIBLE);
            handler.postDelayed(runnable, INTERVAL);
        });

        ib_start.setOnClickListener(v -> {
            if (play_status == 0) {
                play_status = 1;
                ib_start.setImageResource(R.mipmap.pause);
                video_view.start();
            } else {
                play_status = 0;
                ib_start.setImageResource(R.mipmap.play_icon);
                video_view.pause();
            }
            handler.postDelayed(runnable, INTERVAL);
        });

        imageView.setOnClickListener(v -> {
            handler.removeCallbacks(runnable);
            if (image_mime_List.contains(mimeType)) {
                adapterClickListener.onAdapterClickListener(position);
            } else {
                videoClickListener.onVideoClickListener(position, video_view.getCurrentPosition());
            }
        });
        ib_full_screen.setOnClickListener(v -> {
            handler.removeCallbacks(runnable);
            if (image_mime_List.contains(mimeType)) {
                adapterClickListener.onAdapterClickListener(position);
            } else {
                videoClickListener.onVideoClickListener(position, video_view.getCurrentPosition());
            }
        });
        return img_layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public interface VideoClickListener {
        void onVideoClickListener(int position, int current_position);
    }
}

