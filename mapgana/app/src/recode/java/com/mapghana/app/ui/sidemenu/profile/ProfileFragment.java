package com.mapghana.app.ui.sidemenu.profile;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.GetProfile;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.UpdateProfile;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.CircleImageView;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.retrofit.RetrofitUtils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.Valiations;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends AppBaseFragment implements CustomDatePickerDialog.OnDateListener {

    private Spinner spGender;
    private ImageButton ivBackButton;
    private AppCompatTextView tvUpdate, tvDob, tvTitle;
    private AppCompatEditText etUname, etEmail, etPhone, etLoc;
    private RestClient restClient;
    private CircleImageView ivProfilePic;
    private ImageButton ivUploadProfilePic;
    private Uri uri;
    private Date date;

    public static boolean dobdateValidate(Date date) {


        try {
            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.YEAR, -12);
            if (date.before(c2.getTime())) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            //  displayLog(TAG, "dobdateValidate: " );
        }
        return false;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void initializeComponent() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().lockDrawer(true);
        tvUpdate = getView().findViewById(R.id.tvUpdate);
        tvTitle = getView().findViewById(R.id.tvTitle);
        etUname = getView().findViewById(R.id.etUname);
        etEmail = getView().findViewById(R.id.etEmail);
        etPhone = getView().findViewById(R.id.etPhone);
        ivBackButton = getView().findViewById(R.id.ivBackButton);
        ivProfilePic = getView().findViewById(R.id.ivProfilePic);
        ivUploadProfilePic = getView().findViewById(R.id.ivUploadProfilePic);
        restClient = new RestClient(getContext());
        date = new Date();

        tvUpdate.setOnClickListener(this);
        ivBackButton.setOnClickListener(this);
        ivUploadProfilePic.setOnClickListener(this);


        prepareData();


    }

    private void prepareData() {
        tvTitle.setText(getContext().getResources().getString(R.string.Profile));
        getProfile();
    }

    private int getUserId() {
        return AppPreferences.getSession().getId();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvUpdate:
                onUpdate();
                break;
//            case R.id.tvDob:
//                onDobDialog();
//                break;
            case R.id.ivBackButton:
                getActivity().onBackPressed();
                break;
            case R.id.ivUploadProfilePic:
                onUploadImage();
                break;
        }
    }

    private void onUploadImage() {
        CropImage.activity()
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        uri = result.getUri();
                        ivProfilePic.setImageURI(uri);
                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                        displayErrorDialog("Error", error.getMessage());
                    }
                }
                break;
        }
    }

    private void onUpdate() {
        String name, mail, phone, loc;
        name = etUname.getText().toString().trim();
        mail = etEmail.getText().toString().trim();
        phone = etPhone.getText().toString().trim();

        if (name.length() < 3 || name.length() > 30) {
            displayToast("User Name should contain 3 to 30 digits.");
            return;
        }


        if (phone.length() < 7 || phone.length() > 15) {
            displayToast("Phone should contain 7 to 15 digits.");
            return;
        }


        if (Valiations.hasText(etUname) && Valiations.isEmailAddress(etEmail, true)) {

            if (ConnectionDetector.isNetAvail(getContext())) {
                displayProgressBar(false);
                if (uri == null) {
                    restClient.callback(this).updateProfile(getUserId(), name, mail, "", "", phone, "", getContext());
                } else {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(BaseArguments.id, String.valueOf(getUserId()));
                    map.put(BaseArguments.username, name);
                    map.put(BaseArguments.name, name);
                    map.put(BaseArguments.email, mail);
                    map.put(BaseArguments.dob, "");
                    map.put(BaseArguments.gender, "");
                    map.put(BaseArguments.phone_number, phone);
                    map.put(BaseArguments.location, "");
                    map.put(BaseArguments.device_type, Constants.device_type);
                    map.put(BaseArguments.device_id, getDeviceID());

                    MultipartBody.Part part;

                    part = RetrofitUtils.createFilePart("image", uri.getPath(), RetrofitUtils.MEDIA_TYPE_IMAGE_PNG);

                    restClient.callback(this).updateProfileWithImage(RetrofitUtils.createMultipartRequest(map), part);
                }
            } else {
                displayToast(Constants.No_Internet);
            }
        }
    }

    private String getDeviceID() {
        return Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        if (dialog != null) {
            dialog.create(getContext(), true).callback(this).show();
        }
    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            this.date = date;
            String DOB;
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            DOB = format.format(date);
//            tvDob.setText(DOB);
        }
    }


    private void getProfile() {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient.callback(this).getProfile(getUserId(), getContext());
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_GETPROFILE) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GetProfile getProfile = gson.fromJson(s, GetProfile.class);
                    Session session = getProfile.getData();
                    if (session == null) return;
                    if (getProfile.getStatus() != 0) {
                        etEmail.setText(session.getEmail());
                        etPhone.setText(session.getPhone_number());
                        date = new Date();


                        etUname.setText(session.getUsername());
                        if (session.getImage() != null && !session.getImage().equals("")) {

                            Glide.with(getContext()).load(session.getImage()).override(250, 250).placeholder(R.drawable.profile)
                                    .dontAnimate()
                                    .thumbnail(0.5f).into(ivProfilePic);
                        }
                        AppPreferences.setSession(session);
                    } else {
                        displayErrorDialog("Error", getProfile.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
            if (apiId == ApiIds.ID_UPDATEPROFILE) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    UpdateProfile updatePRofile = gson.fromJson(s, UpdateProfile.class);
                    if (updatePRofile.getStatus() != 0) {
                        etEmail.setText(updatePRofile.getData().getEmail());
                        etPhone.setText(updatePRofile.getData().getPhone_number());
                        etUname.setText(updatePRofile.getData().getUsername());
                        AppPreferences.setSession(updatePRofile.getData());

                        if (updatePRofile.getData().getImage() != null && !updatePRofile.getData().getImage().equals("")) {

                            SessionManager.saveProfileImage(getContext(), updatePRofile.getData().getImage());
                            Glide.with(getContext()).load(updatePRofile.getData().getImage()).
                                    override(250, 250).placeholder(R.drawable.profile)
                                    .dontAnimate()
                                    .into(ivProfilePic);
                            uri = null;

                        }

                        if (updatePRofile.getData().getImage() != null && !updatePRofile.getData().getImage().equals("")) {
                            getNavHandler().setHeaderProfilePic(updatePRofile.getData().getImage(), R.mipmap.user_profile_round_dummy);
                        }

                        if (updatePRofile.getData().getName() != null && !updatePRofile.getData().getName().equals("")) {
                            getNavHandler().setUserName(updatePRofile.getData().getName());
                        }

                        displayToast(updatePRofile.getMessage());
                        getActivity().onBackPressed();
                    } else {
                        displayErrorDialog("Error", updatePRofile.getError());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    public String getImagePath(Uri uri) {
        Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContext().getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }
}
