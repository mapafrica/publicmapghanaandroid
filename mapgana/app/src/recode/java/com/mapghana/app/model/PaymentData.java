package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentData implements Serializable {

    private int id;
    private String txRef;
    private String orderRef;
    private String flwRef;
    private String redirectUrl;
    private String device_fingerprint;
    private String settlement_token;
    private String cycle;
    private int amount;
    private int charged_amount;
    private double appfee;
    private int merchantfee;
    private int merchantbearsfee;
    private String chargeResponseCode;
    private String raveRef;
    private String chargeResponseMessage;
    private String authModelUsed;
    private String currency;
    private String IP;
    private String narration;
    private String status;
    private String modalauditid;
    private String vbvrespmessage;
    private String authurl;
    private String vbvrespcode;
    private String acctvalrespmsg;
    private String acctvalrespcode;
    private String paymentType;
    private String paymentPlan;
    private String paymentPage;
    private String paymentId;
    private String fraud_status;
    private String charge_type;
    private int is_live;
    private String retry_attempt;
    private String getpaidBatchId;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;
    private String customerId;
    private int AccountId;
    private ArrayList<String> meta;

    public int getId() {
        return id;
    }

    public String getTxRef() {
        return txRef;
    }

    public String getOrderRef() {
        return orderRef;
    }

    public String getFlwRef() {
        return flwRef;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public String getDevice_fingerprint() {
        return device_fingerprint;
    }

    public String getSettlement_token() {
        return settlement_token;
    }

    public String getCycle() {
        return cycle;
    }

    public int getAmount() {
        return amount;
    }

    public int getCharged_amount() {
        return charged_amount;
    }

    public double getAppfee() {
        return appfee;
    }

    public int getMerchantfee() {
        return merchantfee;
    }

    public int getMerchantbearsfee() {
        return merchantbearsfee;
    }

    public String getChargeResponseCode() {
        return chargeResponseCode;
    }

    public String getRaveRef() {
        return raveRef;
    }

    public String getChargeResponseMessage() {
        return chargeResponseMessage;
    }

    public String getAuthModelUsed() {
        return authModelUsed;
    }

    public String getCurrency() {
        return currency;
    }

    public String getIP() {
        return IP;
    }

    public String getNarration() {
        return narration;
    }

    public String getStatus() {
        return status;
    }

    public String getModalauditid() {
        return modalauditid;
    }

    public String getVbvrespmessage() {
        return vbvrespmessage;
    }

    public String getAuthurl() {
        return authurl;
    }

    public String getVbvrespcode() {
        return vbvrespcode;
    }

    public String getAcctvalrespmsg() {
        return acctvalrespmsg;
    }

    public String getAcctvalrespcode() {
        return acctvalrespcode;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getPaymentPlan() {
        return paymentPlan;
    }

    public String getPaymentPage() {
        return paymentPage;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getFraud_status() {
        return fraud_status;
    }

    public String getCharge_type() {
        return charge_type;
    }

    public int getIs_live() {
        return is_live;
    }

    public String getRetry_attempt() {
        return retry_attempt;
    }

    public String getGetpaidBatchId() {
        return getpaidBatchId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public String getCustomerId() {
        return customerId;
    }

    public int getAccountId() {
        return AccountId;
    }

    public ArrayList<String> getMeta() {
        return meta;
    }
}
