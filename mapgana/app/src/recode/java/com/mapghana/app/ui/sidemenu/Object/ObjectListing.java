package com.mapghana.app.ui.sidemenu.Object;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.ObjectListingAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.ObjectListingContent;
import com.mapghana.app.model.ObjectListingModel;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.navigationView.MapViewDetailFragment;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class ObjectListing extends AppBaseFragment implements ObjectListCallback {

    private RecyclerView recyclerView;
    private ArrayList<ObjectListingContent> data;
    private Context context;
    private ObjectListingAdapter adapter;

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_object_listing;
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Object));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }


    @Override
    public void initializeComponent() {

        init();

        recyclerView = getView().findViewById(R.id.rv_group_listing);
        recyclerView.setHasFixedSize(true);

        RestClient restClient = new RestClient(getContext());
        restClient.callback(this).getObjectListingCategory();
        getView().findViewById(R.id.fab).setOnClickListener(view -> {
            navigateToAddEventScreen((AppBaseActivity) getActivity());
        });
        data = new ArrayList<>();
        adapter = new ObjectListingAdapter(context, data, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_OBJECT_LISTING_CATEGORY) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    ObjectListingModel objectListingModel = gson.fromJson(s, ObjectListingModel.class);
                    if (objectListingModel.getStatus() == 1) {
                        setRecyclerViewContent(objectListingModel);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);

    }

    public void navigateToAddEventScreen(AppBaseActivity baseActivity) {
        baseActivity.pushFragment(AddObjectFragment.newInstance(), true);
    }

    private void setRecyclerViewContent(ObjectListingModel objectListingModel) {


        for (ObjectListingContent eventListingContent : objectListingModel.getData()) {

            data.add(eventListingContent);
        }

        adapter.setDataList(data);
    }

    @Override
    public void onClickEvent(ObjectListingContent model) {
        hideKeyboard();

        try {
            getDashboardActivity().changeFragment(MapViewDetailFragment.newInstance("Object",model.getName(),"Object"), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim,
                    true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}

