package com.mapghana.app.ui.sidemenu.Event;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.EventListingAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.dialog.PopCalendarView;
import com.mapghana.app.dialog.PopUpEventDetail;
import com.mapghana.app.interfaces.CalenderClick;
import com.mapghana.app.model.CalendarContent;
import com.mapghana.app.model.EventCalendarModel;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventListing;
import com.mapghana.app.model.EventListingContent;
import com.mapghana.app.model.SectionDataModel;
import com.mapghana.app.model.Session;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;


public class EventListingFragment extends AppBaseFragment implements EventListCallback {

    private RecyclerView recyclerView;
    private ArrayList<SectionDataModel> allSampleData;
    private Context context;
    private EventListingAdapter adapter;
    private PopUpEventDetail customDialog;
    private PopCalendarView popCalendarView;
    private ArrayList<String> listcalendar;
    private TextView emptyView;
    private RelativeLayout rlSearchView;
    private EditText etSearchView;
    private ImageView imageClear;
    private RestClient restClient;
    private TextWatcher mSearchTextWatcher;

    public static EventListingFragment newInstance() {
        EventListingFragment fragment = new EventListingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_event_listing;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void initializeComponent() {

        init();
        getView().findViewById(R.id.fab).setOnClickListener(view -> {
            Session session = AppPreferences.getSession();
            if (!TextUtils.isEmpty(session.getUsername())) {
                navigateToAddEventScreen((AppBaseActivity) getActivity());
            } else {
                try {
                    getDashboardActivity().showLoginMessageDialog();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        });
        emptyView = getView().findViewById(R.id.empty_view);
        rlSearchView = getView().findViewById(R.id.ll_search_view);
        etSearchView = getView().findViewById(R.id.etSearch);
        imageClear = getView().findViewById(R.id.ic_clear);
        allSampleData = new ArrayList<>();
        listcalendar = new ArrayList<>();

        recyclerView = getView().findViewById(R.id.rv_event_listing);
        recyclerView.setHasFixedSize(true);
        adapter = new EventListingAdapter(context, allSampleData, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        recyclerView.setAdapter(adapter);


        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            restClient = new RestClient(getContext());
            callEventListApi();
            restClient.callback(this).getEventCalendarDates();
        } else {
            Utils.showToast(getActivity(), getView(), Constants.No_Internet);
        }


        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mEventCalendarListener,
                new IntentFilter(Constants.LOCAL_BROADCAST_CALENDAR)
        );

        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mEventCalendarListener,
                new IntentFilter(Constants.LOCAL_BROADCAST_EVENT_SEARCH)
        );

        CalenderClick calenderClick = date -> {
            popCalendarView.dismiss();
            navigateToEventListingByDate((AppBaseActivity) getActivity(), date);
        };

        popCalendarView = new PopCalendarView((AppBaseActivity) getActivity(), listcalendar, calenderClick);
        popCalendarView.setCancelable(true);
        popCalendarView.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        imageClear.setOnClickListener(v -> {
            etSearchView.setText("");
            rlSearchView.setVisibility(View.GONE);
            getNavHandler().setSearchVisibiltyInternal(true);
        });


        mSearchTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                callSearchApi(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(etSearchView.getText().toString())) {
                    callEventListApi();
                }
            }
        };
    }

    private void callSearchApi(String searchText) {
        if(searchText.length() > 3) {
            displayProgressBar(false);
            String url = BaseArguments.EVENT_LISTING_BY_SEARCH + "" + searchText;
            restClient.callback(this).getEventSearchEventList(url);
        }
    }

    private void callEventListApi() {
        restClient.callback(this).getEventListing();
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_EVENT_LISTING) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    EventListing eventListingData = gson.fromJson(s, EventListing.class);
                    if (eventListingData.getStatus() == 1) {
                        setRecyclerViewContent(eventListingData);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_EVENT_CALENDAR) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    EventCalendarModel eventListingData = gson.fromJson(s, EventCalendarModel.class);
                    if (eventListingData.getStatus() == 1) {
                        setCalendarData(eventListingData);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_EVENT_SEARCH_LIST) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    EventListing eventListingData = gson.fromJson(s, EventListing.class);
                    if (eventListingData.getStatus() == 1) {
                        setRecyclerViewContent(eventListingData);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }

        }
    }

    private void setCalendarData(EventCalendarModel model) {
        listcalendar.clear();
        if (model != null) {
            for (CalendarContent content : model.getData()) {
                listcalendar.add(getDate(content.getStart_date_time()));
            }
        }

    }

    private String getDate(String date) {
        String[] str = date.split(" ");

        return str[0];
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);

    }

    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver mEventCalendarListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_CALENDAR)) {
                etSearchView.setText("");
                rlSearchView.setVisibility(View.GONE);
                getNavHandler().setSearchVisibiltyInternal(true);
                popCalendarView.show();
            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_EVENT_SEARCH)) {
                etSearchView.addTextChangedListener(mSearchTextWatcher);
                getNavHandler().setSearchVisibiltyInternal(false);
                rlSearchView.setVisibility(View.VISIBLE);

            } else {
                rlSearchView.setVisibility(View.GONE);
                getNavHandler().setSearchVisibiltyInternal(false);
            }


        }
    };


    public void navigateToAddEventScreen(AppBaseActivity baseActivity) {
        baseActivity.pushFragment(AddEventFragment.newInstance(), true);
    }

    public void navigateToEventDetailScreen(AppBaseActivity baseActivity, EventDetail item) {
        baseActivity.pushFragment(EventDetailFragment.newInstance(item.getName(), Integer.toString(item.getId())), true);
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Events));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(true);
        getNavHandler().setSearchVisibiltyInternal(true);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);

    }

    private void setRecyclerViewContent(EventListing eventListing) {
        allSampleData.clear();
        for (EventListingContent eventListingContent : eventListing.getData()) {

            if (eventListingContent.getData().size() != 0) {

                SectionDataModel dm = new SectionDataModel();

                dm.setHeaderTitle(eventListingContent.getEvent_date());
                ArrayList<EventDetail> singleItem = new ArrayList<EventDetail>();
                for (EventDetail detail : eventListingContent.getData()) {
                    singleItem.add(detail);
                }

                dm.setAllItemsInSection(singleItem);

                allSampleData.add(dm);
            }
        }

        if (allSampleData.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
        adapter.setDataList(allSampleData);
        dismissProgressBar();
    }

    @Override
    public void onClickEvent(EventDetail model) {
        showPopUp(model, EventListingFragment.this);

    }

    @Override
    public void onPopUpClickEvent(EventDetail model) {
        navigateToEventDetailScreen((AppBaseActivity) getActivity(), model);
    }

    @Override
    public void onCloseIcon() {
        customDialog.dismiss();
    }

    private void showPopUp(EventDetail model, EventListCallback listener) {
        customDialog = new PopUpEventDetail(getActivity(), listener, model, true);
        customDialog.setCancelable(false);
        customDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        etSearchView.removeTextChangedListener(mSearchTextWatcher);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mEventCalendarListener);
    }

    private void navigateToEventListingByDate(AppBaseActivity baseActivity, String date) {
        baseActivity.pushFragment(EventListByDate.newInstance(date), true);
    }
}

