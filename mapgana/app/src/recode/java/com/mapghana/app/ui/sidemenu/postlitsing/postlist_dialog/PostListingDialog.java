package com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseDialog;
import com.mapghana.customviews.TypefaceRadioButton;
import com.mapghana.handler.AdapterClickListener;

/**
 * Created by ubuntu on 30/12/17.
 */

public class PostListingDialog extends AppBaseDialog {

    private AdapterClickListener adapterClickListener;
    private AppCompatRadioButton rdOrg, rdIndi;
    private TextView mOrganisation, mIndividual, mEvents, mGroups, mCustomOrder, mMore;
    private LinearLayout mllOtherView;
    public PostListingDialog(@NonNull Context context, AdapterClickListener adapterClickListener) {
        super(context);
        this.adapterClickListener = adapterClickListener;
        setAnimation();
    }

    @Override
    protected int getLayoutResourceView() {
        return R.layout.dialog_bussiness_type;
    }

    @Override
    protected void initializeComponent() {

        mOrganisation = this.findViewById(R.id.select_organisation);
        mIndividual = this.findViewById(R.id.select_individual);
        mEvents = this.findViewById(R.id.select_events);
        mGroups = this.findViewById(R.id.select_groups);
        mCustomOrder = this.findViewById(R.id.select_custom_order);
        mMore = this.findViewById(R.id.select_more);
        mllOtherView = this.findViewById(R.id.ll_more);

        mOrganisation.setOnClickListener(this);
        mIndividual.setOnClickListener(this);
        mEvents.setOnClickListener(this);
        mGroups.setOnClickListener(this);
        mCustomOrder.setOnClickListener(this);
        mMore.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String title = "";
        switch (v.getId()) {
            case R.id.select_organisation:
                title = mOrganisation.getText().toString().trim();
                adapterClickListener.onAdapterClickListener(title);
                dismiss();
                break;
            case R.id.select_individual:
                title = mIndividual.getText().toString().trim();
                adapterClickListener.onAdapterClickListener(title);
                dismiss();
                break;

            case R.id.select_events:
                title = mEvents.getText().toString().trim();
                adapterClickListener.onAdapterClickListener(title);
                dismiss();
                break;

            case R.id.select_groups:
                title = mGroups.getText().toString().trim();
                adapterClickListener.onAdapterClickListener(title);
                dismiss();
                break;

            case R.id.select_custom_order:
                title = mCustomOrder.getText().toString().trim();
                adapterClickListener.onAdapterClickListener(title);
                dismiss();
                break;
            case R.id.select_more:
                mllOtherView.setVisibility(View.VISIBLE);
                mMore.setVisibility(View.GONE);
                break;
        }

    }

}
