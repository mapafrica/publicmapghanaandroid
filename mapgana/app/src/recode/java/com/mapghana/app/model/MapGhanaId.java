package com.mapghana.app.model;

import java.io.Serializable;

public class MapGhanaId implements Serializable {

    private int status;
    private String message;
    private String data;
    private String error;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getData() {
        return data;
    }

    public String getError() {
        return error;
    }
}
