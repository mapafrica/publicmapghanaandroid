package com.mapghana.app.model;

/**
 * Created by ubuntu on 8/1/18.
 */

public class Forgot {


    /**
     * status : 1
     * message : Please check your email for further instruction
     * data : y@mail.com
     * error :
     */

    private int status;
    private String message;
    private String data;
    private String error;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
