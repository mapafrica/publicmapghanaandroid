package com.mapghana.app.model;

import java.io.Serializable;

public class GroupListListing implements Serializable {

    private int status;
    private String message;
    private GroupListData data;
    private String error;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public GroupListData getData() {
        return data;
    }

    public String getError() {
        return error;
    }
}
