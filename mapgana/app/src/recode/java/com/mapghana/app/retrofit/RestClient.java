package com.mapghana.app.retrofit;

import android.content.Context;
import android.provider.Settings;

import com.google.gson.Gson;
import com.mapghana.app.model.Login;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.RestService;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.rest.ApiHitListener;
import com.mapghana.rest.BaseRestClient;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ububtu on 13/7/16.
 */
public class RestClient extends BaseRestClient {
    ApiHitListener apiHitListener;
    public static int API_COUNT = 0;
    private Rest api;

    public RestClient(Context _context) {
        super(_context);
    }

    public RestClient callback(ApiHitListener apiHitListener) {
        this.apiHitListener = apiHitListener;
        return this;
    }

    private Rest getApi() {
        if (api == null) {
            api = RestService.getService();
        }

        return api;
    }

    private Rest getApiGeoCoding() {
        if (api == null) {
            api = RestService.getServiceGeoCoder();
        }


        return api;
    }

    private String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public void signup(String username, String full_name, String password, String email,
                       String phone_number,
                        Context context) {
        Call<ResponseBody> call = getApi().signup(username, full_name, password, email, phone_number, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_SIGNUP, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_SIGNUP, t.getMessage());
            }
        });
    }

    public void transaction(String event_id, String amount, String transection_id, String email,
                       String first_name,
                            String last_name, String type, String paymentType) {
        Call<ResponseBody> call = getApi().transaction(event_id, amount, transection_id, email, first_name, last_name,type,paymentType);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_TRANSACTION, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_TRANSACTION, t.getMessage());
            }
        });
    }

    public void login(String email, String password, Context context) {

        Call<ResponseBody> call = getApi().login(email, password, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_LOGIN, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_LOGIN, t.getMessage());
            }
        });
    }

    public void forgot(String email) {
        Call<ResponseBody> call = getApi().forgot(email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_FORGOT, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_FORGOT, t.getMessage());
            }
        });
    }

    public void getProfile(int id, Context context) {
        API_COUNT++;
        Call<ResponseBody> call = getApi().getProfile(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_GETPROFILE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;
                apiHitListener.onFailResponse(ApiIds.ID_GETPROFILE, t.getMessage());
            }
        });
    }


    public void updateProfile(int id, String username,
                              String email, String dob, String gender,
                              String phone_number, String location,
                              Context context) {
        API_COUNT++;
        Call<ResponseBody> call = getApi().updateProfile(id, username, username, email, dob, gender, phone_number, location, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_UPDATEPROFILE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_UPDATEPROFILE, t.getMessage());
            }
        });
    }

    public void updateProfileWithImage(HashMap<String, RequestBody> map, MultipartBody.Part image) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().updateProfileWithImage(map, image);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_UPDATEPROFILE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_UPDATEPROFILE, t.getMessage());
            }
        });
    }

    public void category(String type) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().category(type);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_CATEGORY, t.getMessage());
            }
        });
    }

    public void popularOrganisation() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_popular_organizations();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_CATEGORY, t.getMessage());
            }
        });
    }


    public void popularIndividuals() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_popular_individuals();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_CATEGORY, t.getMessage());
            }
        });
    }


    public void postCheckMapghanaId(String url) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().getCheckMapGhanaId(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_MAPGHANA_ID, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_MAPGHANA_ID, t.getMessage());
            }
        });
    }

    public void category() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().category();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;
                apiHitListener.onFailResponse(ApiIds.ID_CATEGORY, t.getMessage());
            }
        });
    }

    public void city() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().city();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_city, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_city, t.getMessage());
            }
        });
    }


    public void post_individual(Context context, HashMap<String, RequestBody> map, MultipartBody.Part image, MultipartBody.Part[] gallery_images) {
        Call<ResponseBody> call = getApi().post_individual(map, image, gallery_images);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_PostINDIVIDUAL, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PostINDIVIDUAL, t.getMessage());
            }
        });
    }

    public void post_individual(Context context, HashMap<String, RequestBody> map, MultipartBody.Part image) {
        Call<ResponseBody> call = getApi().post_individual(map, image);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_PostINDIVIDUAL, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PostINDIVIDUAL, t.getMessage());
            }
        });
    }


    public void postAddCustomOrder(Context context, HashMap<String, RequestBody> map, MultipartBody.Part image) {
        Call<ResponseBody> call = getApi().postAddCustomOrder(map, image);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_ADD_CUSTOM_ORDER, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_ADD_CUSTOM_ORDER, t.getMessage());
            }
        });
    }


    public void post_organization(Context context, HashMap<String, RequestBody> map, MultipartBody.Part[] gallery_images) {
        Call<ResponseBody> call = getApi().post_organization(map, gallery_images);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_PostOrganization, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PostOrganization, t.getMessage());
            }
        });
    }

    public void contact() {
        Call<ResponseBody> call = getApi().contact();
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_contact, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_contact, t.getMessage());
            }
        });
    }

    public void contactQuery(String name, String email, String phone, String message) {
        Call<ResponseBody> call = getApi().contact(name, email, phone, message);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_contact_query, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_contact_query, t.getMessage());
            }
        });
    }

    public void logout(Context context) {
        Gson gson = new Gson();
        API_COUNT++;

        Call<ResponseBody> call = getApi().logout(AppPreferences.getSession().getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_logout, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_logout, t.getMessage());
            }
        });
    }

    public void postApiUserStatus(Context context, int status, int user_id) {
        Gson gson = new Gson();
        API_COUNT++;

        Call<ResponseBody> call = getApi().postUserStatus(String.valueOf(user_id),status );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_USER_STATUS, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_USER_STATUS, t.getMessage());
            }
        });
    }

    public Call<ResponseBody> post_listing_organizationsss(String lat, String log) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_organizations(lat, log, "1000");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_organization, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_organization, t.getMessage());
            }
        });
        return call;
    }

    public void post_listing_organizations(String sub_category_id) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_organizations(sub_category_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_organization, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_organization, t.getMessage());
            }
        });
    }

    public void post_send_review(HashMap<String, RequestBody> map) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_send_review(map);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;


                apiHitListener.onSuccessResponse(ApiIds.ID_post_send_review, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_post_send_review, t.getMessage());
            }
        });
    }

    public void postRsvp(int user_id, int event_id) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().postRsvp(user_id,event_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;


                apiHitListener.onSuccessResponse(ApiIds.ID_RSVP_EVENT, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_RSVP_EVENT, t.getMessage());
            }
        });
    }

    public Call<ResponseBody> post_listing_Individualsss(String lat, String log) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_Individuals(lat, log, "1000");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_individual, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_individual, t.getMessage());
            }
        });
        return call;
    }

    public Call<ResponseBody> post_listing_Event_Explore() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_Event();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_EXPLORE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_EXPLORE, t.getMessage());
            }
        });
        return call;
    }

    public Call<ResponseBody> post_listing_object_Explore() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_object();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_OBJECT_EXPLORE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_OBJECT_EXPLORE, t.getMessage());
            }
        });
        return call;
    }

    public void post_listing_Individuals(String category_id) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().post_listing_Individuals(category_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Filter_individual, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Filter_individual, t.getMessage());
            }
        });
    }

    public void getRSVPEvent(String url) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().getRSVPEvent(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_GET_RSVP_EVENT, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_GET_RSVP_EVENT, t.getMessage());
            }
        });
    }

    public void getDetailsOfOnePost(String post_id) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().getDetailsOfOnePost(post_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_Details_1, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_Details_1, t.getMessage());
            }
        });
    }

    public void getDetailsOfOneCatgoryObject(String url) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().getDetailsOfOneCategoryObject(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_OBJECT_CATEGORY_WISE_DATA, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_OBJECT_CATEGORY_WISE_DATA, t.getMessage());
            }
        });
    }

    public Call<ResponseBody> searchFilter(String keyword, String category_id, String city_id,
                             String lat, String log, String distance, String page) {
        API_COUNT++;

        Call<ResponseBody> call = getApi().searchFilter(keyword, category_id, city_id, lat, log, distance, page);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POSTS_SEarch_Filter, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POSTS_SEarch_Filter, t.getMessage());
            }
        });
        return call;
    }

    public void ads() {
        API_COUNT++;

        Call<ResponseBody> call = getApi().ads();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_ADS, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_ADS, t.getMessage());
            }
        });
    }

    public void popularCategory(){
        API_COUNT++;

        Call<ResponseBody> call = getApi().popular_category();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_POPULAR_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_POPULAR_CATEGORY, t.getMessage());
            }
        });
    }

    public void newSignup(String username, String full_name, String email,
                          String phone_number,
                          Context context) {
        Call<ResponseBody> call = getApi().newSignUp(username, full_name, email, phone_number, Constants.device_type, getDeviceID(context));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiHitListener.onSuccessResponse(ApiIds.ID_SIGNUP, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.ID_SIGNUP, t.getMessage());
            }
        });
    }

    public void postAddObject(Context context, HashMap<String, RequestBody> map, MultipartBody.Part[] gallery_images) {
        Call<ResponseBody> call = getApi().postAddObject(map, gallery_images);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;
                apiHitListener.onSuccessResponse(ApiIds.ID_OBJECT, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_OBJECT, t.getMessage());
            }
        });
    }

    public void postAddEvent(Context context, HashMap<String, RequestBody> map, MultipartBody.Part[] previousgallery,MultipartBody.Part[] currentGallery,MultipartBody.Part[] posterMedia) {
        Call<ResponseBody> call = getApi().postAddEventApi(map, previousgallery,currentGallery,posterMedia);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_ADD_EVENT, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_ADD_EVENT, t.getMessage());
            }
        });
    }

    public void postAddGroup(Context context, HashMap<String, RequestBody> map, MultipartBody.Part[] previousgallery, MultipartBody.Part[] posterMedia) {
        Call<ResponseBody> call = getApi().postAddGroupApi(map, previousgallery,posterMedia);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_ADD_GROUP, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_ADD_GROUP, t.getMessage());
            }
        });
    }

    public void getEventListing() {
        Call<ResponseBody> call = getApi().getEventListing();
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_LISTING, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_LISTING, t.getMessage());
            }
        });
    }

    public void getEventCalendarDates() {
        Call<ResponseBody> call = getApi().getEventCalendarDates();
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_CALENDAR, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_CALENDAR, t.getMessage());
            }
        });
    }

    public void getEventSearchEventList(String url) {
        Call<ResponseBody> call = getApi().getEventSearchList(url);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_SEARCH_LIST, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_SEARCH_LIST, t.getMessage());
            }
        });
    }

    public void getGroupGridListing(String url) {
        Call<ResponseBody> call = getApi().getGroupGridListing(url);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_GROUP_GRID_LISTING, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_GROUP_GRID_LISTING, t.getMessage());
            }
        });
    }

    public void getGroupListing(String url) {
        Call<ResponseBody> call = getApi().getGroupListing(url);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_GROUP_LISTING, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_GROUP_LISTING, t.getMessage());
            }
        });
    }
    public void getEventListingByDate(String url) {
        Call<ResponseBody> call = getApi().getEventListingByDate(url);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_LISTING_BY_DATE, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_LISTING_BY_DATE, t.getMessage());
            }
        });
    }

    public void getEventIdList() {
        Call<ResponseBody> call = getApi().getEventIdListingForGroup();
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_ID_LISTING_FOR_GROUP, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_ID_LISTING_FOR_GROUP, t.getMessage());
            }
        });
    }

    public void getObjectListingCategory() {
        Call<ResponseBody> call = getApi().getObjectListingCategory();
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_OBJECT_LISTING_CATEGORY, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_OBJECT_LISTING_CATEGORY, t.getMessage());
            }
        });
    }

    public void getUserCustomOrderList(String userId) {
        Call<ResponseBody> call = getApi().getUserCustomList(userId);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_USER_CUSTOM_ORDER_LIST, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_USER_CUSTOM_ORDER_LIST, t.getMessage());
            }
        });
    }


    public void getDeplinkingData(String url) {
        Call<ResponseBody> call = getApi().getDeeplinking(url);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_DEEPLINKING, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_DEEPLINKING, t.getMessage());
            }
        });
    }

    public void getPriceData(String url) {
        Call<ResponseBody> call = getApi().getPrice(url);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_PRICE_TRANSACTION, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_PRICE_TRANSACTION, t.getMessage());
            }
        });
    }

    public void getEventDetailById(String id) {
        Call<ResponseBody> call = getApi().getEventDetailById(id);
        API_COUNT++;

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                API_COUNT--;

                apiHitListener.onSuccessResponse(ApiIds.ID_EVENT_DETAIL_BY_ID, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                API_COUNT--;

                apiHitListener.onFailResponse(ApiIds.ID_EVENT_DETAIL_BY_ID, t.getMessage());
            }
        });
    }

}