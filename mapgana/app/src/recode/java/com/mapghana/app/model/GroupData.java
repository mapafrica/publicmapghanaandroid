package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupData implements Serializable {

    private int id;
    private String user_id;
    private String group_name;
    private String image;
    private String about;
    private String chat_link;
    private String mapghana_event_link;
    private String group_admin_contact;
    private String admin_password;
    private String elite;
    private String verified;
    public ArrayList<EventGallery> group_gallery;

    public String getElite() {
        return elite;
    }

    public String getVerified() {
        return verified;
    }

    public ArrayList<EventGallery> getGroup_gallery() {
        return group_gallery;
    }

    public String getAdmin_password() {
        return admin_password;
    }

    public int getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public String getImage() {
        return image;
    }

    public String getAbout() {
        return about;
    }

    public String getChat_link() {
        return chat_link;
    }

    public String getMapghana_event_link() {
        return mapghana_event_link;
    }

    public String getGroup_admin_contact() {
        return group_admin_contact;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setChat_link(String chat_link) {
        this.chat_link = chat_link;
    }

    public void setMapghana_event_link(String mapghana_event_link) {
        this.mapghana_event_link = mapghana_event_link;
    }

    public void setGroup_admin_contact(String group_admin_contact) {
        this.group_admin_contact = group_admin_contact;
    }

    public void setAdmin_password(String admin_password) {
        this.admin_password = admin_password;
    }

    public void setGroup_gallery(ArrayList<EventGallery> group_gallery) {
        this.group_gallery = group_gallery;
    }
}
