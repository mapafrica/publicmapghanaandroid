package com.mapghana.app.ui.activity.dashboard;

import android.app.ActivityManager;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.services.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.services.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.services.commons.models.Position;
import com.mapghana.R;
import com.mapghana.app.model.Login;
import com.mapghana.app.model.TransactionPrice;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.ui.activity.dashboard.dashboard.DashboardFragment;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.app.ui.sidemenu.Group.GroupListing;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.dialog.MessageDialog;
import com.mapghana.app.helpers.maphandler.MapHandler;
import com.mapghana.app.helpers.navigation.NavigationViewHandler;
import com.mapghana.app.model.Ads;
import com.mapghana.app.model.DeeplinkingData;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.Session;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.spf.TimeManager;
import com.mapghana.app.ui.activity.ads.AdsActivity;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.ui.activity.dashboard.home.HomeFragment;
import com.mapghana.app.ui.activity.mainactivity.MainActivity;
import com.mapghana.app.ui.sidemenu.Event.EventDetailFragment;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.base.BaseFragment;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppBaseActivity {

    public NavigationViewHandler navigationViewHandler;
    public AppBaseMapBox appBaseMapBox;
    public static final int LOGIN_REQUEST_CODE = 888;
    private Location mLocation;
    public double mCurrentLongitude = 0;
    public double mCurrentLatitude = 0;
    private LocationRequest locationRequest;
    private GoogleApiClient mGoogleApiClient;
    private int flag = 0;
    private Handler handler;
    private Runnable runnable;
    private final long TWO_MINUTE = 1000 * 60 * 2;
    private final long FIVE_MINUTE = 1000 * 60 * 5;

    private MapHandler mapHandler;
    private MapboxMap mapboxMap;
    private FrameLayout map_contanier;

    public MapHandler getMapHandler() {
        return mapHandler;
    }

    public MapboxMap getMapboxMap() {
        return mapboxMap;
    }


    private Handler backStackHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                BaseFragment baseFragment = getLatestFragment();
                if (baseFragment == null) {
                    return;
                }
                if (!(baseFragment instanceof PostDetailFragment))
                    setMapContainerHeight(ViewGroup.LayoutParams.MATCH_PARENT);
                navigationViewHandler.onCreateViewFragment(baseFragment);
                baseFragment.viewCreateFromBackStack();
            }
        }
    };

    FragmentManager.OnBackStackChangedListener onBackStackChangedListener = () -> {
        backStackHandler.removeMessages(1);
        backStackHandler.sendEmptyMessageDelayed(1, 100);
    };

    public FrameLayout getMap_contanier() {
        return map_contanier;
    }

    @Override
    public void onBackPressed() {

        BaseFragment latestFragment = getLatestFragment();
        if (latestFragment != null) {
            if (!(latestFragment instanceof PostDetailFragment)) {
                setMapContainerHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            }
            super.onBackPressed();

        } else {
            finish();
        }
    }

    public void setMapContainerHeight(int height) {
        if (map_contanier != null) {
            ViewGroup.LayoutParams layoutParams = map_contanier.getLayoutParams();
            layoutParams.height = height;
            map_contanier.setLayoutParams(layoutParams);
        }
    }

    public NavigationViewHandler getNavigationViewHandler() {
        return navigationViewHandler;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void beforeSetContentView() {
        Mapbox.getInstance(this, getString(R.string.mapbox_api_token));
    }

    @Override
    public void initializeComponent() {
        navigationViewHandler = new NavigationViewHandler(this);
        navigationViewHandler.findViewIds();
        getFm().addOnBackStackChangedListener(onBackStackChangedListener);

        appBaseMapBox = new AppBaseMapBox();

        map_contanier = findViewById(R.id.container);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                showAds();
                TimeManager.saveLastTime(DashboardActivity.this, System.currentTimeMillis() + FIVE_MINUTE);
                waitForSomeTime(this, FIVE_MINUTE);
            }
        };
        TimeManager.saveLastTime(DashboardActivity.this, System.currentTimeMillis() + TWO_MINUTE);
        mapHandler = new MapHandler(this, "");

        showDashboardFragment();
        getLocation();

        setUpUserData();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if ("event_value".equalsIgnoreCase(bundle.getString("event_key"))) {
                openEventListing();
            } else if ("group_value".equalsIgnoreCase(bundle.getString("group_key"))) {
                openGroupScreen();
            } else if (!TextUtils.isEmpty(bundle.getString("url"))) {
                RestClient restClient = new RestClient(this);
                restClient.callback(this).getDeplinkingData(bundle.getString("url"));
            }else if("explore_value".equalsIgnoreCase(bundle.getString("explore_key"))) {
                openDashBoardScreen();
            }
        }

                RestClient restClient = new RestClient(this);
                restClient.callback(this).getPriceData(BaseArguments.PRICE_RATE);
    }


    public void setUpUserData() {

        Session session = AppPreferences.getSession();
        if (session != null) {

            if (!TextUtils.isEmpty(session.getImage())) {
                setHeaderProfilePic(session.getImage(), R.drawable.ic_avatar);
            }

            if (!TextUtils.isEmpty(session.getUsername())) {
                setUserName(session.getUsername());
            }
            if (!TextUtils.isEmpty(session.getEmail())) {
                setNavLocationText(session.getEmail());
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpUserData();
        navigationViewHandler.setUpUserData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(runnable);

        }
    }

    private void calculateTime() {
        long time;
        time = TimeManager.getLastTime(this);
        long diffTime;

        diffTime = time - System.currentTimeMillis();
        if (diffTime < 0) {
            diffTime = 0;
        }
        waitForSomeTime(runnable, diffTime);
    }

    private void waitForSomeTime(Runnable runnable, long minuts) {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, minuts);
    }

    private void showAds() {
        getAds();
    }


    private void getAds() {
        RestClient restClient = new RestClient(this);
        restClient.callback(this).ads();
    }

    private void getLocation() {
        // ((MapGhanaApplication) getApplication()).setLocationServiceListner(this);
        mLocation = ((MapGhanaApplication) getApplication()).mLocation;
        if (ConnectionDetector.isNetAvail(this)) {

            if (!isMyServiceRunning()) {

                startService(new Intent(this, Locationservice.class));
                // displayLog(TAG, "onRunning: ");
            }
            if (mLocation != null) {
                //userLocationChange(mLocation);
            }
        }
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName().equals(service1.service.getPackageName())
                    && Locationservice.class.getName().equals(service1.service.getClassName()))) {
                //  displayLog(TAG, "isMyServiceRunning =MY servicunnin ");

                return true;
            }
        }
        return false;
    }

    private void showDashboardFragment() {

        changeFragment(new HomeFragment(), true, true, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }


    @Override
    public int getFragmentContainerResourceId() {
        return R.id.container;
    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {
        navigationViewHandler.setNavToggleButtonVisibilty(visibilty);
    }

    @Override
    public void setNavTitle(String title) {
        navigationViewHandler.setNavTitle(title);
    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {
        navigationViewHandler.setBussinessTypeLayoutVisibuility(visibilty);
    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {
        navigationViewHandler.setSearchButtonVisibuility(visibilty);
    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {
        navigationViewHandler.setNavigationToolbarVisibilty(visibilty);
    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {
        navigationViewHandler.setBackButtonVisibilty(visibilty);
    }

    @Override
    public void setHeaderProfilePic(String uri, int res) {
        navigationViewHandler.setHeaderProfilePic(uri, res);
    }

    @Override
    public void setUserName(String Name) {
        navigationViewHandler.setUserName(Name);
    }

    @Override
    public void setNavLocationText(String Name) {
        navigationViewHandler.setNavLocationText(Name);
    }

    @Override
    public void lockDrawer(boolean visibilty) {
        navigationViewHandler.lockDrawer(visibilty);
    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {
        navigationViewHandler.setNavTitleTextVisibilty(visibilty);
    }

    @Override
    public void setNavLocationTextVisibilty(boolean visibilty) {
        navigationViewHandler.setNavLocationTextVisibilty(visibilty);
    }

    @Override
    public void setNavAddressText(String title) {
        navigationViewHandler.setNavAddressText(title);
    }


    @Override
    public String getPostType() {
        return navigationViewHandler.getPostType();
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        if (apiId == ApiIds.ID_ADS) {
            TimeManager.saveFirst(this);
            if (response.isSuccessful()) {
                Gson gson = new Gson();
                try {
                    Ads ads = gson.fromJson(response.body().string(), Ads.class);
                    if (ads.getCode() != 0) {
                        String path = ads.getData().getVideo();
                        if (path != null && path.length() > 0) {
                            if (RestClient.API_COUNT == 0) {
                                Intent intent = new Intent(this, AdsActivity.class);
                                intent.putExtra("video_path", path);
                                intent.putExtra("ads_path", ads.getData().getAds_url());
                                startActivity(intent);
                            }

                        } else {
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
            } else {
            }
        } else if (apiId == ApiIds.ID_DEEPLINKING) {
            try {
                String s = response.body().string();
                Gson gson = new Gson();
                DeeplinkingData deeplinkingData = gson.fromJson(s, DeeplinkingData.class);
                if (deeplinkingData != null) {
                    if (deeplinkingData.getType().equalsIgnoreCase("event")) {
                        openEventDetail(deeplinkingData.getEvent_id());
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
                displayErrorDialog("Error", e.getMessage());
            }
        }else if (apiId == ApiIds.ID_PRICE_TRANSACTION) {
            try {
                String s = response.body().string();
                Gson gson = new Gson();
                TransactionPrice transactionPrice = gson.fromJson(s, TransactionPrice.class);
                if (transactionPrice != null) {
                    if (transactionPrice.getStatus() != 0) {
                        AppPreferences.setTransactionPrice(transactionPrice);
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
                displayErrorDialog("Error", e.getMessage());
            }
        }

    }

    @Override
    public void onFailResponse(int apiId, String error) {
        TimeManager.saveFirst(this);

    }

//    @Override
//    public void userLocationChange(Location location) {
//        mLocation = location;
//        if (mLocation != null) {
//            mCurrentLatitude = location.getLatitude();
//            mCurrentLongitude = location.getLongitude();
//            if (mCurrentLatitude != 0 && mCurrentLongitude != 0 && flag == 0) {
//                flag = 1;
//                reverseGeoCoding(new LatLng(mCurrentLatitude, mCurrentLongitude));
//            } else {
//            }
//
//            BaseFragment baseFragment = getLatestFragment();
//            if (baseFragment != null && baseFragment instanceof DashboardFragment) {
//                ((DashboardFragment) baseFragment).userLocationChange(location);
//            } else if (baseFragment instanceof PostDetailFragment) {
//                ((PostDetailFragment) baseFragment).userLocationChange(location);
//            } else if (baseFragment instanceof ViewDetailsFragment) {
//                ((ViewDetailsFragment) baseFragment).userLocationChange(location);
//            } else if (baseFragment instanceof SingleItemOnMapFragment) {
//                ((SingleItemOnMapFragment) baseFragment).userLocationChange(location);
//            } else if (baseFragment instanceof NavigationFragment) {
//                ((NavigationFragment) baseFragment).userLocationChange(location);
//            }
//           /* else if (baseFragment instanceof AddLocationFragment){
//                ((AddLocationFragment)baseFragment).userLocationChange(location);
//            }*/
//        }
//    }

//    @Override
//    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {
//        this.locationRequest = locationRequest;
//        mGoogleApiClient = googleApiClient;
//        setGoogleApiClient(googleApiClient);
//        setLocationRequest(locationRequest);
//    }

    public LocationRequest getLocationRequest() {
        return locationRequest;
    }

    public void setLocationRequest(LocationRequest locationRequest) {
        this.locationRequest = locationRequest;
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.mGoogleApiClient = googleApiClient;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onDestroy() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        stopService(new Intent(this, Locationservice.class));
        boolean rememberUser = SessionManager.isRememberUser(this);
        if (!rememberUser) {
            SessionManager.clearUserData(this);
        }
        super.onDestroy();
    }

//    @Override
//    public void onMapLoadedd(MapboxMap mapboxMap) {
//        this.mapboxMap = mapboxMap;
//        if (handler != null) {
//            calculateTime();
//        }
//
//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            String url = extras.getString("url");
//            if (!TextUtils.isEmpty(url)) {
//                RestClient restClient = new RestClient(this);
//                restClient.callback(this).getDeplinkingData(url);
//            }
//
//        }
//    }

    @Override
    public void setExploreVisibility(boolean visibility) {
        navigationViewHandler.setExploreVisibility(visibility);
    }

    @Override
    public void setCalenderVisibility(boolean visibility) {
        navigationViewHandler.setCalenderVisibility(visibility);
    }

    @Override
    public void setSearchVisibiltyInternal(boolean visibility) {
        navigationViewHandler.setSearchVisibiltyInternal(visibility);
    }

    @Override
    public void setimgMultiViewVisibility(boolean visibility) {
        navigationViewHandler.setimgMultiViewVisibility(visibility);
    }

    @Override
    public void setAtoZZtoAVisibiltyInternal(boolean visibility) {
        navigationViewHandler.setAtoZZtoAVisibiltyInternal(visibility);
    }


    public void showLoginMessageDialog() {
        MessageDialog messageDialog = new MessageDialog(this);
        messageDialog.setLoginListener(() -> {
            messageDialog.dismiss();
            goToLoginScreen();
        });
        messageDialog.show();
    }

    public void goToLoginScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivityForResult(intent, LOGIN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == 999) {
            setUpUserData();
            navigationViewHandler.setUpUserData();
        }
    }

    private void openPostDeatilFragment(int post_id) {
        PostDetailFragment detailsFragment = new PostDetailFragment();
        detailsFragment.setPost_id(post_id);
        changeFragment(detailsFragment, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);

    }

    private void openEventDetail(String id) {
        EventDetailFragment eventDetail = EventDetailFragment.newInstance("Event", id);

        changeFragment(eventDetail, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);

    }

    private void openEventListing() {
        EventListingFragment eventListingFragment = EventListingFragment.newInstance();

        changeFragment(eventListingFragment, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);

    }

    private void openGroupScreen() {
        GroupListing groupListing = GroupListing.newInstance();

        changeFragment(groupListing, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);

    }

    private void openDashBoardScreen() {
        changeFragment(new DashboardFragment(), true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);
    }
}
