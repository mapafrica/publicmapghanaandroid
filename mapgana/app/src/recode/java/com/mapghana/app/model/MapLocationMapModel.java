package com.mapghana.app.model;

import com.mapghana.app.interfaces.NavigateListener;

import java.io.Serializable;

public class MapLocationMapModel implements Serializable {

    private String lat;
    private String log;
    private String type;
    private String address;
    private boolean iseditLocation;


    public MapLocationMapModel(String lat, String log, String type,String address, boolean iseditLocation) {
        this.lat = lat;
        this.log = log;
        this.type = type;
        this.address = address;
        this.iseditLocation = iseditLocation;

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isIseditLocation() {
        return iseditLocation;
    }

    public void setIseditLocation(boolean iseditLocation) {
        this.iseditLocation = iseditLocation;
    }
}
