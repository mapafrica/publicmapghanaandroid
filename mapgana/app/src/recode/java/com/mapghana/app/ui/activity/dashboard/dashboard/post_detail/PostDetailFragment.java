package com.mapghana.app.ui.activity.dashboard.dashboard.post_detail;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.utils.ColorUtils;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.dialog.PopQRCodeFullScreen;
import com.mapghana.app.dialog.ShowStatusDialog;
import com.mapghana.app.interfaces.QRcodeSizeListener;
import com.mapghana.app.model.GalleryPojo;
import com.mapghana.app.model.GetDetails;
import com.mapghana.app.model.MapLocationMapModel;
import com.mapghana.app.model.OpeningHour;
import com.mapghana.app.model.Session;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.FeaturesAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.ReviewsAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.SlidingImageAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.navigation.NavigationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.writereview.WriteReviewFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.SearchFragment;
import com.mapghana.app.ui.activity.fullscreen.FullScreenActivity;
import com.mapghana.app.ui.navigationView.MapViewFragment;
import com.mapghana.app.ui.navigationView.MapviewNavigation;
import com.mapghana.app.ui.sidemenu.customOrder.AddCustomOrderFragment;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.utils.Utils;
import com.mapghana.util.ConnectionDetector;
import com.mapghana.util.file_path_handler.FilePathHelper;
import com.rd.PageIndicatorView;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class  PostDetailFragment extends AppBaseFragment
        implements OnMapReadyCallback, PermissionsListener, LocationServiceListner,
        SlidingImageAdapter.VideoClickListener, QRcodeSizeListener {
    public LocationComponent locationComponent;
    private RecyclerView rvFeatures, rvReviews;
    private AppCompatTextView tvName, tvEmail, tvReviews,
            tvWebsite, tvRating, tv_note, tv_type, tvTotalRating, tvCategoryName, tvSubCategoryName, tvPhone, tv_tag, tvInstagram, tvFacebook, tvYoutube, tvSoundCloud, tvGaming, tv_share_loc, tvWriteReview, mapNavigate, tv_navigation;
    private AppCompatTextView tvMF, tvSat, tvSun, tv_twitter;
    private ImageView imgProfile, imgStatus, iv_profile_pic;
    private RatingBar rating_bar;
    private LinearLayout llOpeningHour, llContact, ll_website, ll_feature, llBlog, llGaming, ll_type;
    private LinearLayout ll_email;
    private RelativeLayout rlMF, rlSat, rlSunday;
    private ViewPager viewPager;
    private PageIndicatorView pageIndicatorView;
    private String category = "";
    private String name;
    private String address;
    private int TYPE;
    private int resource;
    private int zoomLevel;
    private double latitude, longitude;
    private LatLng latLng;
    private MapView mapView;
    private MapboxMap map;
    private boolean isMapReady;
    private PermissionsManager permissionsManager;
    private List<GalleryPojo> postGalleryList;
    private List<GetDetails.DataBean.PostReviewBean> userImageList = new ArrayList<>();
    private AppBaseMapBox.CustomSupportMapFragment mapFragment;
    private String profileImagePath;
    private ReviewsAdapter reviewsAdapter;
    private NavigationFragment navigationFragment;
    private int post_id;
    private SlidingImageAdapter slidingImageAdapter;
    private List<String> video_mime_List;
    private ImageView QRCode;
    private PopQRCodeFullScreen dialog;
    private SymbolManager symbolManager;
    private CircleManager circleManager;
    private ImageView QrShare;
    private GetDetails data;
    private ImageView favourite, tip, flag;
    private LinearLayout llEliteUser;
    private AppCompatTextView txtMapghanaId;
    private AppCompatTextView mWhatsApp;
    private Session session;
    private TextView status;
    private ImageView mImgStatus;
    private Button order;

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_category_details;
    }

    @Override
    public void initializeComponent() {
        init();
        initViewById(getView());
        initListener();
        mapView = getView().findViewById(R.id.map_view);
        mapView.getMapAsync(this);
        session = AppPreferences.getSession();

        postGalleryList = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        video_mime_List.addAll(Constants.getVideoMimeList());
        navigationFragment = new NavigationFragment();
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        rvFeatures.setLayoutManager(manager);
        rvReviews.setLayoutManager(new LinearLayoutManager(getContext()));
        iv_profile_pic.setVisibility(View.GONE);

        FilePathHelper filePathHelper = new FilePathHelper();
        setData();
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
    }

    private void initViewById(View view) {
        llEliteUser = view.findViewById(R.id.eliteUser);
        txtMapghanaId = view.findViewById(R.id.txt_mapghana_id);
        favourite = view.findViewById(R.id.favourite);
        tip = view.findViewById(R.id.tip);
        flag = view.findViewById(R.id.flag);
        QrShare = view.findViewById(R.id.share_qr);
        rvFeatures = view.findViewById(R.id.rvFeatures);
        rvReviews = view.findViewById(R.id.rvReviews);
        tvName = view.findViewById(R.id.tvName);
        tvEmail = view.findViewById(R.id.tvEmail);
        QRCode = view.findViewById(R.id.qr_code);
        tvRating = view.findViewById(R.id.tvRating);
        tvReviews = view.findViewById(R.id.tvReviews);
        tv_note = view.findViewById(R.id.tv_note);
        tv_type = view.findViewById(R.id.tv_type);
        order = view.findViewById(R.id.order);
        tvTotalRating = view.findViewById(R.id.tvTotalRating);
        tvCategoryName = view.findViewById(R.id.tvCategoryName);
        tvSubCategoryName = view.findViewById(R.id.tvSubCategoryName);
        tv_tag = view.findViewById(R.id.tv_tag);
        tvMF = view.findViewById(R.id.tvMF);
        tvSat = view.findViewById(R.id.tvSat);
        tvSun = view.findViewById(R.id.tvSun);
        tv_twitter = view.findViewById(R.id.tvTwitter);
        imgProfile = view.findViewById(R.id.imgProfile);
        imgStatus = view.findViewById(R.id.imgStatus);
        iv_profile_pic = view.findViewById(R.id.iv_profile_pic);
        llContact = view.findViewById(R.id.ll_contact);
        llBlog = view.findViewById(R.id.ll_blog);
        mapNavigate = view.findViewById(R.id.map_navigate);
        llGaming = view.findViewById(R.id.ll_gaming);
        ll_website = view.findViewById(R.id.ll_website);
        ll_feature = view.findViewById(R.id.ll_feature);
        rating_bar = view.findViewById(R.id.rating_bar);
        llOpeningHour = view.findViewById(R.id.llOpeningHour);
        ll_type = view.findViewById(R.id.ll_type);
        rlMF = view.findViewById(R.id.rlMF);
        rlSat = view.findViewById(R.id.rlSat);
        rlSunday = view.findViewById(R.id.rlSunday);
        viewPager = view.findViewById(R.id.view_pager);
        pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
        tv_share_loc = view.findViewById(R.id.tv_share_loc);
        tv_navigation = view.findViewById(R.id.tv_navigation);
        tvWriteReview = view.findViewById(R.id.tvWriteReview);
        ll_email = view.findViewById(R.id.ll_email);
        tvInstagram = view.findViewById(R.id.tvInstagram);
        tvFacebook = view.findViewById(R.id.tvFacebook);
        tvYoutube = view.findViewById(R.id.tvYoutube);
        tvSoundCloud = view.findViewById(R.id.tvCloud);
        mWhatsApp = view.findViewById(R.id.tvWhatsApp);
        status = view.findViewById(R.id.txt_status);
        mImgStatus = view.findViewById(R.id.status_image);

        displayErrorDialog(getString(R.string.app_name), getString(R.string.string_convey_message_post_detail));
    }

    private void initListener() {
        tvWriteReview.setOnClickListener(this);
        llContact.setOnClickListener(this);
        ll_email.setOnClickListener(this);
        tv_share_loc.setOnClickListener(this);
        tv_navigation.setOnClickListener(this);
        imgStatus.setOnClickListener(this);
        ll_website.setOnClickListener(this);
        iv_profile_pic.setOnClickListener(this);
        ll_type.setOnClickListener(this);
        order.setOnClickListener(this);
        tv_twitter.setOnClickListener(v -> {
            Utils.openTwitter(tv_twitter.getText().toString().trim(), getActivity());
        });

        mapNavigate.setOnClickListener(v -> {
            navigateMapView(data.getData().getLat(), data.getData().getLog());
        });

        mWhatsApp.setOnClickListener(v -> {
            Utils.openWhatsApp(mWhatsApp.getText().toString(), getActivity());
        });

        tvInstagram.setOnClickListener(v -> {
            Utils.openChatLink(tvInstagram.getText().toString().trim(), getActivity());
        });

        tvFacebook.setOnClickListener(v -> {
            Utils.openChatLink(tvFacebook.getText().toString().trim(), getActivity());
        });
    }

    @Override
    public void viewCreateFromBackStack() {
        super.viewCreateFromBackStack();
        init();
    }

    public void navigateMapView(double lat, double log) {
        MapViewFragment mapViewFragment = new MapViewFragment();
        mapViewFragment.setLocation(new LatLng(lat, log));
        ((AppBaseActivity) getActivity()).pushFragment(mapViewFragment, true);
    }

    public void makeSpanableString(String link, Context context, TextView view) {
        SpannableString ss = new SpannableString(link);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan, 0, link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        view.setText(ss);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setHighlightColor(Color.TRANSPARENT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvWriteReview:

                try {
                    if (!TextUtils.isEmpty(session.getUsername())) {

                        WriteReviewFragment fragment = new WriteReviewFragment();
                        Object tag = rating_bar.getTag();

                        if (tag != null) {
                            String status = (String) tag;
                            fragment.setStatus(status);
                        }
                        fragment.setPost_id(post_id);
                        getDashboardActivity().changeFragment(fragment, true, false, 0,
                                R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                                true);
                    } else {
                        getDashboardActivity().showLoginMessageDialog();
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv_share_loc:
                shareLocation();
                break;
            case R.id.imgStatus:
                showStatusSheet();
                break;
            case R.id.iv_profile_pic:
                showFullProfilePic();
                break;
            case R.id.tv_navigation:
                navigateNagivationMapView(data);
                break;
            case R.id.ll_type:
                goToSearch();
                break;
            case R.id.order:
                try {
                    if (!TextUtils.isEmpty(session.getUsername())) {

                        AddCustomOrderFragment fragment = new AddCustomOrderFragment();

                        fragment.setPost_id(post_id);
                        getDashboardActivity().changeFragment(fragment, true, false, 0,
                                R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                                true);
                    } else {
                        getDashboardActivity().showLoginMessageDialog();
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void navigateNagivationMapView(GetDetails item) {
        Intent intent = new Intent(getActivity(), MapviewNavigation.class);
        MapLocationMapModel model = new MapLocationMapModel(String.valueOf(item.getData().getLat()), String.valueOf(item.getData().getLog()), null, "", false);
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", model);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void goToSearch() {
        String s = tv_type.getText().toString().trim();
        if (s != null && s.length() > 0) {
            SearchFragment fragment = new SearchFragment();
            fragment.setType(s);
            try {
                getDashboardActivity().changeFragment(fragment, true, true,
                        1, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                        true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void showFullProfilePic() {


        List<GalleryPojo> list = new ArrayList<>();
        GalleryPojo galleryPojo = new GalleryPojo();
        galleryPojo.setId(1);
        galleryPojo.setImage(profileImagePath);
        list.add(galleryPojo);
        Bundle bundle = new Bundle();
        bundle.putInt("position", 0);
        bundle.putString("gallery", new Gson().toJson(list));
        //bundle.putParcelableArrayList("gallery", (ArrayList<? extends Parcelable>) list);
        changeActivity(bundle);
    }

    private void shareLocation() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "MapGhana");
            String sAux = "\nLet me recommend you this application\n\n";
            // sAux = sAux + "https://play.google.com/store/apps/details?id=com.mapghana \n\n";
            sAux = sAux + BaseArguments.domain + "/deeplink?id=" + post_id;
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            displayErrorDialog("", e.getMessage());
        }
    }

    private void showStatusSheet() {
        ShowStatusDialog dialog = new ShowStatusDialog(getContext());
        dialog.setCancelable(true);
        dialog.show();
    }


    private void getDetails() {

        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).getDetailsOfOnePost(String.valueOf(post_id));
        } else {
            displayToast(Constants.No_Internet);
        }
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_Details_1) {
                try {
                    String s = response.body().string();
                    updateUI(s);
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void initializePostGalley(List<GetDetails.DataBean.PostGalleryBean> postGallery) {
        slidingImageAdapter = new SlidingImageAdapter(getContext(), postGalleryList, this, this);
        GalleryPojo galleryPojo;
        for (int i = 0; i < postGallery.size(); i++) {
            galleryPojo = new GalleryPojo();
            galleryPojo.setId(postGallery.get(i).getId());
            galleryPojo.setImage(postGallery.get(i).getImage());
            postGalleryList.add(galleryPojo);
        }
        viewPager.setAdapter(slidingImageAdapter);
        pageIndicatorView.setViewPager(viewPager);
        pageIndicatorView.setCount(postGallery.size());
        pageIndicatorView.setSelection(0);
        viewPager.setCurrentItem(0);
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    private void setData() {
        getDetails();
    }

    @Override
    public void onAdapterClickListener(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);

        bundle.putString("gallery", new Gson().toJson(postGalleryList));
        changeActivity(bundle);
    }

    @Override
    public void onVideoClickListener(int position, int current_position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putInt("current_position", current_position);
        bundle.putString("gallery", new Gson().toJson(postGalleryList));
        changeActivity(bundle);
    }

    @Override
    public void onAdapterClickListener(int position, String action) {
        if (action.equalsIgnoreCase("fromSingleImage")) {

            String image = userImageList.get(position).getUser().getImage();
            if (image != null && !image.equals("")) {
                List<GalleryPojo> list = new ArrayList<>();
                GalleryPojo galleryPojo = new GalleryPojo();
                galleryPojo.setId(userImageList.get(position).getId());
                galleryPojo.setImage(image);
                list.add(galleryPojo);

                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                // bundle.putParcelableArrayList("gallery", (ArrayList<? extends Parcelable>) list);
                bundle.putString("gallery", new Gson().toJson(list));

                changeActivity(bundle);
            } else {
                displayToast("There is no profile image");
            }
        }
    }

    private void changeActivity(Bundle bundle) {
        Intent intent = new Intent(getContext(), FullScreenActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    @Override
    public void userLocationChange(Location location) {
        //moveMarker(location);
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }


    private void addEmailSection(ArrayList<String> email) {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;

        for (String emailText : email) {
            final View addView = layoutInflater.inflate(R.layout.item_email, null);
            TextView etEmail = addView.findViewById(R.id.txt_mail);
            makeSpanableString(emailText, getActivity(), etEmail);
            etEmail.setOnClickListener(v -> {
                Utils.onSendMail(etEmail.getText().toString().trim(), getActivity());
            });

            ll_email.addView(addView);
        }

    }

    private void addPhoneSection(ArrayList<String> phone) {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;

        for (String phoneText : phone) {
            final View addView = layoutInflater.inflate(R.layout.item_phone, null);
            TextView txtPhone = addView.findViewById(R.id.txt_phone);
            makeSpanableString(phoneText, getActivity(), txtPhone);
            txtPhone.setOnClickListener(v -> {
                Utils.startDialing(txtPhone.getText().toString(), getActivity());
            });

            llContact.addView(addView);
        }

    }

    private void addWebsiteSection(ArrayList<String> website) {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;

        for (String webText : website) {
            final View addView = layoutInflater.inflate(R.layout.item_webtext, null);
            TextView txtWeb = addView.findViewById(R.id.txt_webText);
            makeSpanableString(webText, getActivity(), txtWeb);
            txtWeb.setOnClickListener(v -> {
                Utils.openWebsite(txtWeb.getText().toString().trim(), getActivity(), getView());
            });

            ll_website.addView(addView);
        }

    }

    private void addBlogSection(ArrayList<String> blog) {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;

        for (String blogText : blog) {
            final View addView = layoutInflater.inflate(R.layout.item_blog, null);
            TextView txtBlog = addView.findViewById(R.id.txt_blog);
            txtBlog.setText(blogText);
            if ( blogText.contains("blogspot")) {
                txtBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_blogger, 0);
            } else if ( blogText.contains("tumblr")) {
                txtBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tumblr, 0);
            } else if ( blogText.contains("wordpress")) {
                txtBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_wordpress, 0);
            } else if ( blogText.contains("medium")) {
                txtBlog.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_medium, 0);
            }
            txtBlog.setOnClickListener(v -> {

            });

            llBlog.addView(addView);
        }

    }

    private void addGamingSection(ArrayList<String> gaming) {

        LayoutInflater layoutInflater =
                (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;

        for (String gamingtext : gaming) {
            final View addView = layoutInflater.inflate(R.layout.item_gaming, null);
            TextView txtGaming = addView.findViewById(R.id.txt_gaming);
            txtGaming.setText(gamingtext);

            if (gamingtext.contains("steam")) {
                txtGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_steam, 0);
            } else if ( gamingtext.contains("psn")) {
                txtGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_playstation, 0);
            } else if ( gamingtext.contains("nin")) {
                txtGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_nintendo, 0);
            } else if (gamingtext.contains("xbl")) {
                txtGaming.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_xbox, 0);
            }

            txtGaming.setOnClickListener(v -> {

            });

            llBlog.addView(addView);
        }

    }

    private void createPolygon(LatLng latLng) {
        circleManager.create(new CircleOptions()
                .withLatLng(latLng)
                .withCircleColor(ColorUtils.colorToRgbaString(getActivity().getResources().getColor(R.color.colorYellowtransparent)))
                .withCircleRadius(100f)
                .withDraggable(false)

        );
    }


    private void setMapMarker() {
        if (isMapReady) {
            if ("general".equalsIgnoreCase(data.getData().getLocation_profile())) {
                createPolygon(new LatLng(data.getData().getLat(), data.getData().getLog()));
            } else {
                String markerOption = data.getData().getName() + "\nAddress: " + data.getData().getAddress() + "\nStatus: " + data.getData().getStatus();
                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.mapbox_marker_icon_default);
                map.getStyle().addImage("marker-" + data.getData().getId(), bm);
                symbolManager.create(new SymbolOptions()
                        .withLatLng(new LatLng(data.getData().getLat(), data.getData().getLog()))
                        .withIconImage("marker-" + data.getData().getId())
                        //set the below attributes according to your requirements
                        .withIconSize(0.5f)
                        .withIconOffset(new Float[]{0f, -1.5f})
                        .withZIndex(10)
                        .withTextField(markerOption)
                        .withTextHaloColor("rgba(255, 255, 255, 100)")
                        .withTextHaloWidth(5.0f)
                        .withTextAnchor("top")
                        .withTextOffset(new Float[]{0f, 1.5f})
                );
                map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(data.getData().getLat(), data.getData().getLog())));
            }
        }
    }

    private void updateUI(String s) {
        if (!isVisible()) return;
        Gson gson = new Gson();
        data = gson.fromJson(s, GetDetails.class);

        if (data.getStatus() != 0) {

            setMapMarker();

            if ("general".equalsIgnoreCase(data.getData().getLocation_profile())) {
                mapNavigate.setVisibility(View.GONE);
                tv_navigation.setVisibility(View.GONE);
                map.getUiSettings().setZoomGesturesEnabled(false);

            } else {
                mapNavigate.setVisibility(View.VISIBLE);
                tv_navigation.setVisibility(View.GONE);
            }

            if ("Y".equalsIgnoreCase(data.getData().getFavourite())) {
                favourite.setVisibility(View.VISIBLE);
            } else {
                favourite.setVisibility(View.INVISIBLE);
            }


            if ("Y".equalsIgnoreCase(data.getData().getTip())) {
                tip.setVisibility(View.VISIBLE);
            } else {
                tip.setVisibility(View.INVISIBLE);
            }

            if ("Y".equalsIgnoreCase(data.getData().getFlag())) {
                flag.setVisibility(View.VISIBLE);
            } else {
                flag.setVisibility(View.INVISIBLE);
            }

            if ("Y".equalsIgnoreCase(data.getData().getElite())) {
                llEliteUser.setVisibility(View.VISIBLE);
                txtMapghanaId.setVisibility(View.VISIBLE);
                txtMapghanaId.setText(data.getData().getElite());
            } else {
                llEliteUser.setVisibility(View.GONE);
                txtMapghanaId.setVisibility(View.GONE);
            }

            if ("Y".equalsIgnoreCase(data.getData().getPhone_visibility())) {
                llContact.setVisibility(View.GONE);
            } else {
                llContact.setVisibility(View.VISIBLE);
            }

            if (data.getData().getPost_for().equalsIgnoreCase("individuals")) {
                profileImagePath = data.getData().getImage();
                if (profileImagePath != null && profileImagePath.trim().length() > 0) {
                    iv_profile_pic.setVisibility(View.VISIBLE);

                    try {

                        Glide.with(getContext()).load(profileImagePath).override(250, 250)
                                .placeholder(R.drawable.profile).dontAnimate().into(iv_profile_pic);
                    } catch (IllegalArgumentException e) {

                    }
                } else {
                    iv_profile_pic.setVisibility(View.GONE);
                }
            } else {
                iv_profile_pic.setVisibility(View.GONE);
            }
            name = data.getData().getName();
            address = data.getData().getAddress();
            getNavHandler().setNavTitle(name);
            tvName.setText(name);
            ArrayList<String> mail = data.getData().getEmail();
            if (mail.size() > 0) {
                ll_email.setVisibility(View.VISIBLE);
                addEmailSection(mail);
            } else {
                ll_email.setVisibility(View.GONE);
            }

            ArrayList<String> web = data.getData().getWebsite();
            if (web.size() > 0) {
                ll_website.setVisibility(View.VISIBLE);
                addWebsiteSection(web);
            } else {
                ll_website.setVisibility(View.GONE);
            }

            ArrayList<String> blog = data.getData().getBlog();
            if (blog.size() > 0) {
                llBlog.setVisibility(View.VISIBLE);
                addBlogSection(blog);
            } else {
                llBlog.setVisibility(View.GONE);
            }
            ArrayList<String> gaming = data.getData().getGaming();
            if (gaming.size() > 0) {

                llGaming.setVisibility(View.VISIBLE);
                addGamingSection(gaming);
            } else {

                llGaming.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(data.getData().getWhatsApp())) {
                mWhatsApp.setVisibility(View.VISIBLE);
                mWhatsApp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_whatspp, 0, 0, 0);
                mWhatsApp.setText(data.getData().getWhatsApp());
            } else {
                mWhatsApp.setVisibility(View.GONE);
            }
            String twitter = data.getData().getTwitter();
            if (twitter != null && twitter.trim().length() > 0) {
                tv_twitter.setVisibility(View.VISIBLE);
                makeSpanableString(twitter, getActivity(), tv_twitter);
            } else {
                tv_twitter.setVisibility(View.GONE);
            }

            String instagram = data.getData().getInstagram();
            if (instagram != null && instagram.trim().length() > 0) {
                tvInstagram.setVisibility(View.VISIBLE);
                makeSpanableString(instagram, getActivity(), tvInstagram);

            } else {
                tvInstagram.setVisibility(View.GONE);
            }

            String facebook = data.getData().getFacebook();
            if (facebook != null && facebook.trim().length() > 0) {
                tvFacebook.setVisibility(View.VISIBLE);
                makeSpanableString(facebook, getActivity(), tvFacebook);

            } else {
                tvFacebook.setVisibility(View.GONE);
            }

            String youtube = data.getData().getYoutube();
            if (youtube != null && facebook.trim().length() > 0) {
                tvYoutube.setVisibility(View.VISIBLE);
                tvYoutube.setText(facebook);

            } else {
                tvYoutube.setVisibility(View.GONE);
            }

            String soundCloud = data.getData().getYoutube();
            if (soundCloud != null && soundCloud.trim().length() > 0) {
                tvSoundCloud.setVisibility(View.VISIBLE);
                tvSoundCloud.setText(soundCloud);

            } else {
                tvSoundCloud.setVisibility(View.GONE);
            }


            final Bitmap[] QrCodImage = {null};
            Glide.clear(QRCode);
            Glide.with(MapGhanaApplication.sharedInstance())
                    .load(BaseArguments.POST_QR_CODE_URL + "" + data.getData().getId())
                    .asBitmap()
                    .dontAnimate()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            QrCodImage[0] = resource;
                            return false;
                        }
                    })
                    .into(QRCode);

            QRCode.setOnClickListener(view -> {
                dialog = new PopQRCodeFullScreen();
                dialog.setQrImage(BaseArguments.POST_QR_CODE_URL + "" + data.getData().getId(), String.valueOf(data.getData().getId()));
                dialog.setListener(this);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "FullScreenDialog");
            });


            QrShare.setOnClickListener(view -> {
                Utils.saveQrImage(QrCodImage[0], data);
                Utils.sendQRPost(getActivity(), data);
            });
            LayerDrawable stars = (LayerDrawable) rating_bar.getProgressDrawable();

            rating_bar.setTag(data.getData().getStatus());
            stars.getDrawable(1).setColorFilter(data.getData().getRatingColor(getContext()),
                    PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(2).setColorFilter(data.getData().getRatingColor(getContext()),
                    PorterDuff.Mode.SRC_ATOP);
            double rate = data.getData().getAvg_rating();

            rating_bar.setRating((float) rate);
            if (data.getData().hasRating()) {
                tvRating.setTextColor(data.getData().getRatingColor(getContext()));
                tvRating.setText(new DecimalFormat("##.#").format(rate) + " out of 5 star");
                tvRating.setVisibility(View.VISIBLE);
            } else {
                tvRating.setVisibility(View.GONE);
            }


            tv_note.setText(data.getData().getNotes().trim());
            String type = data.getData().getType();
            if (type != null && type.length() > 0) {
                ll_type.setVisibility(View.VISIBLE);
                tv_type.setText(type);
            }


            tv_tag.setText(data.getData().getTags());
            ArrayList<String> phoneList = data.getData().getPhone();
            if (phoneList.size() > 0) {
                llContact.setVisibility(View.VISIBLE);
                addPhoneSection(phoneList);
            } else {
                llContact.setVisibility(View.GONE);
            }
            String available_category = data.getData().getCategory().getName();
            tvCategoryName.setText(available_category);
            GetDetails.DataBean.SubcategoryBean subCategoryBean = data.getData().getSubcategory();
            if (subCategoryBean != null) {
                if (subCategoryBean.getName().trim().length() > 0) {
                    tvSubCategoryName.setVisibility(View.VISIBLE);
                    tvSubCategoryName.setText("(" + subCategoryBean.getName() + ")");
                    available_category = subCategoryBean.getName();
                }
            }
            if (data.getData().getTotal_review() == 0) {
                tvTotalRating.setVisibility(View.GONE);
            } else {
                tvTotalRating.setText("(" + data.getData().getTotal_review() + ") - ");
            }

            List<GetDetails.DataBean.PostGalleryBean> postGallery = data.getData().getPostGallery();
            if (postGallery != null && postGallery.size() > 0 && postGallery.get(0).getImage() != null &&
                    !postGallery.get(0).getImage().equals("")) {
                initializePostGalley(postGallery);
            } else {
                if (profileImagePath != null && !profileImagePath.equals("")) {
                    GetDetails.DataBean.PostGalleryBean bean = new GetDetails.DataBean.PostGalleryBean();
                    bean.setImage(profileImagePath);
                    List<GetDetails.DataBean.PostGalleryBean> list = new ArrayList<>();
                    list.add(bean);
                    initializePostGalley(list);
                } else {
                    imgProfile.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.GONE);
                    //imgProfile.setImageResource(R.mipmap.d);
                }
            }
            // set opening hour
            String openHour = data.getData().getOpening_hours();
            if (openHour != null) {
                llOpeningHour.setVisibility(View.VISIBLE);
                OpeningHour openingHour = gson.fromJson(openHour, OpeningHour.class);
                if (openingHour.getMondayfriday() != null) {
                    rlMF.setVisibility(View.VISIBLE);
                    tvMF.setText(openingHour.getMondayfriday().getStart() + " - " +
                            openingHour.getMondayfriday().getEnd());
                }
                if (openingHour.getSaturday() != null) {
                    rlSat.setVisibility(View.VISIBLE);
                    tvSat.setText(openingHour.getSaturday().getStart() + " - " +
                            openingHour.getSaturday().getEnd());
                }
                if (openingHour.getSunday() != null) {
                    rlSunday.setVisibility(View.VISIBLE);
                    tvSun.setText(openingHour.getSunday().getStart() + " - " +
                            openingHour.getSunday().getEnd());
                }
            } else {
                llOpeningHour.setVisibility(View.GONE);
            }

            List<String> features = data.getData().getFeatures();
            if (features != null && features.size() > 0) {
                ll_feature.setVisibility(View.VISIBLE);
                FeaturesAdapter adapter = new FeaturesAdapter(features);
                rvFeatures.setAdapter(adapter);
            } else {
                ll_feature.setVisibility(View.GONE);
            }

            List<GetDetails.DataBean.PostReviewBean> reviewsList = new ArrayList<>();
            reviewsList.addAll(data.getData().getPostReview());
            if (reviewsList != null) {
                tvReviews.setVisibility(View.VISIBLE);
                userImageList.addAll(reviewsList);
                reviewsAdapter = new ReviewsAdapter(this, reviewsList);
                reviewsAdapter.setStatus(data.getData().getStatus());
                rvReviews.setAdapter(reviewsAdapter);
            } else {
                tvReviews.setVisibility(View.GONE);
            }

            latitude = data.getData().getLat();
            longitude = data.getData().getLog();

            try {
                latLng = new LatLng(latitude, longitude);
            } catch (IllegalArgumentException e) {

            }


            String status = data.getData().getStatus().trim();
            String postFor = data.getData().getPost_for();
            if (postFor != null && postFor.equalsIgnoreCase(Constants.individuals)) {
                String gender = "";
                gender = data.getData().getGender().trim();
                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_gril;
                        imgStatus.setImageResource(R.mipmap.right_green);
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resource = R.mipmap.individual_red_girl;
                        imgStatus.setImageResource(R.mipmap.right_red);
                        TYPE = 2;
                    } else {
                        resource = R.mipmap.indi_girl_gray;
                        TYPE = 3;
                        imgStatus.setImageResource(R.mipmap.right_gray);

                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.individual_green_man;
                        imgStatus.setImageResource(R.mipmap.right_green);
                        TYPE = 1;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        imgStatus.setImageResource(R.mipmap.right_red);
                        TYPE = 2;
                        resource = R.mipmap.individual_red_man;
                    } else {
                        resource = R.mipmap.individual_gray_man;
                        TYPE = 3;
                        imgStatus.setImageResource(R.mipmap.right_gray);
                    }
                }

                category = "Occupation: " + available_category;
            } else if (postFor != null && postFor.equalsIgnoreCase(Constants.organizations)) {
                if (status != null && status.equalsIgnoreCase(Constants.verified)) {
                    resource = R.mipmap.location_gree_org;
                    imgStatus.setImageResource(R.mipmap.right_green);
                    TYPE = 1;

                } else if (status != null && status.equalsIgnoreCase(Constants.unverified)) {
                    resource = R.mipmap.location_red_org;
                    imgStatus.setImageResource(R.mipmap.right_red);
                    TYPE = 2;

                } else {
                    //(type==3)
                    resource = R.mipmap.location_gry_org;
                    imgStatus.setImageResource(R.mipmap.right_gray);
                    TYPE = 3;

                }
                //  category = "Category: " + available_category;

                category = "Category: ";
                GetDetails.DataBean.CategoryBean categoryObj = data.getData().getCategory();
                if (categoryObj != null) {
                    category = category + categoryObj.getName();
                }
                GetDetails.DataBean.SubcategoryBean subcategoryObj = data.getData().getSubcategory();
                if (subcategoryObj != null) {
                    category = category + " (" + subcategoryObj.getName() + ")";
                }
            }
            if (latLng != null) {

            } else {
                // displayToast("Invalid latitude and longitude");
            }
        } else {
            displayErrorDialog("Error", data.getError());
        }
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.map = mapboxMap;
        isMapReady = true;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            enableLocationComponent(style);
            symbolManager = new SymbolManager(mapView, map, style);
            circleManager = new CircleManager(mapView, map, style);
            symbolManager.setIconAllowOverlap(false);  //your choice t/f
            symbolManager.setTextAllowOverlap(false);  //your choice t/f
            Location location = Locationservice.sharedInstance().getLastLocation();
            if (location != null) {
                Point originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
            }
            if (data != null) {
                setMapMarker();
            }
        });
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = map.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(map.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onClose() {
        dialog.dismiss();
    }
}
