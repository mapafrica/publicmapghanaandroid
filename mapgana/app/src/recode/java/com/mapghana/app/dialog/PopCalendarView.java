package com.mapghana.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.adapter.HwAdapter;
import com.mapghana.app.interfaces.CalenderClick;
import com.mapghana.app.model.HomeCollection;
import com.mapghana.app.utils.Utils;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class PopCalendarView extends Dialog implements
        View.OnClickListener {
    public GregorianCalendar cal_month, cal_month_copy;
    private HwAdapter hwAdapter;
    private TextView tv_month;
    private List<HomeCollection> date_collection_arr;
    private List<HomeCollection> mHomeCollection;
    private GridView gridview;
    private Activity context;
    private ArrayList<String> mList;
    private CalenderClick calenderClick;

    public PopCalendarView(Activity context, ArrayList<String> list, CalenderClick calenderClick) {
        super(context);
       this.context = context;
       this.mList = list;
       this.calenderClick = calenderClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_calendar_view_event);

        date_collection_arr = new ArrayList<>();
        mHomeCollection = new ArrayList<>();


        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        hwAdapter = new HwAdapter(context, cal_month, mList);

        tv_month = findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));


        ImageButton previous = findViewById(R.id.ib_prev);
        previous.setOnClickListener(v -> {

            setPreviousMonth();
            refreshCalendar();
        });
        ImageButton next = findViewById(R.id.Ib_next);
        next.setOnClickListener(v -> {
            setNextMonth();
            refreshCalendar();
        });
        gridview = findViewById(R.id.gv_calendar);
        gridview.setAdapter(hwAdapter);
        gridview.setOnItemClickListener((parent, v, position, id) -> {
            String selectedGridDate = HwAdapter.day_string.get(position);
            if(mList.contains(selectedGridDate)) {
                calenderClick.onClick(selectedGridDate);
            }

        });
    }

    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1), cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);

        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH, cal_month.get(GregorianCalendar.MONTH) - 1);

        }

    }

    public void refreshCalendar() {
        hwAdapter.refreshDays();
        hwAdapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_close:
                break;
            default:
                break;
        }
        dismiss();
    }

}
