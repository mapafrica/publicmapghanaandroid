package com.mapghana.app.ui.sidemenu.Event;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.adapter.EventSectionListAdapterByDate;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.dialog.PopUpEventDetail;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventListingByDate;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.Utils;
import com.mapghana.customviews.CustomDatePickerDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Response;


public class EventListByDate extends AppBaseFragment implements EventListCallback, CustomDatePickerDialog.OnDateListener {

    private RecyclerView recyclerView;
    private String date = null;
    private ArrayList<EventDetail> data;
    private TextView emptyView;
    private EventSectionListAdapterByDate itemListDataAdapter;
    private PopUpEventDetail customDialog;
    private TextView txtDate;
    private Calendar eventCalendar;
    private static final String DATE = "date";

    public static EventListByDate newInstance(String date) {
        EventListByDate eventListByDate = new EventListByDate();
        Bundle args = new Bundle();
        args.putString(DATE, date);
        eventListByDate.setArguments(args);

        return eventListByDate;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.layout_event_list_by_date;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Events));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }


    @Override
    public void initializeComponent() {
        init();
        initViewById(getView());

        data = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        itemListDataAdapter = new EventSectionListAdapterByDate(this, data);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        recyclerView.setAdapter(itemListDataAdapter);

        if (getArguments() != null) {
            date = getArguments().getString(DATE);
        }
        RestClient restClient = new RestClient(getContext());
        String url = BaseArguments.EVENT_LISTING_BY_DATE + "" + Utils.getddmmyyyyHygn(date);
        restClient.callback(this).getEventListingByDate(url);
    }

    private void initViewById(View view) {
        recyclerView = view.findViewById(R.id.recycleView);
        emptyView = view.findViewById(R.id.empty_view);
        txtDate = view.findViewById(R.id.txt_date);
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_EVENT_LISTING_BY_DATE) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    EventListingByDate eventListingData = gson.fromJson(s, EventListingByDate.class);
                    if (eventListingData.getStatus() == 1) {

                        if (eventListingData.getData().getData().size() != 0) {

                            for (EventDetail detail : eventListingData.getData().getData()) {
                                detail.setImage(detail.getImage());
                                data.add(detail);
                            }

                            itemListDataAdapter.setData(data);
                            emptyView.setVisibility(View.GONE);
                        } else {
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);

    }

    private void OnDateEventDailog() {
        CustomDatePickerDialog dialog = new CustomDatePickerDialog();
        dialog.create(getContext(), false).callback(this).show();
    }


    @Override
    public void onClickEvent(EventDetail model) {
        showPopUp(model, EventListByDate.this);

    }

    public void navigateToEventDetailScreen(AppBaseActivity baseActivity, EventDetail item) {
        baseActivity.pushFragment(EventDetailFragment.newInstance(item.getName(), Integer.toString(item.getId())), true);
    }

    @Override
    public void onPopUpClickEvent(EventDetail model) {
        navigateToEventDetailScreen((AppBaseActivity) getActivity(), model);
    }

    @Override
    public void onCloseIcon() {
        customDialog.dismiss();
    }

    private void showPopUp(EventDetail model, EventListCallback listener) {
        customDialog = new PopUpEventDetail(getActivity(), listener, model, true);
        customDialog.setCancelable(false);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        customDialog.show();
    }

    @Override
    public void onSuccess(Date date) {
        if (date != null) {
            displayProgressBar(false);
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            String eventDate = format.format(date);
            txtDate.setText(Utils.getddMMYYYY(date));
            RestClient restClient = new RestClient(getContext());
            String url = BaseArguments.EVENT_LISTING_BY_DATE + "" + eventDate;
            restClient.callback(this).getEventListingByDate(url);
        }
    }
}
