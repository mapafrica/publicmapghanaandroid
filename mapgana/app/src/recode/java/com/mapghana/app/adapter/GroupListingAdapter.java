package com.mapghana.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.model.GroupData;
import com.mapghana.app.ui.sidemenu.Group.GroupListing;

import java.util.ArrayList;

public class GroupListingAdapter extends RecyclerView.Adapter<GroupListingAdapter.ItemRowHolder> {

    private ArrayList<GroupData> mList;
    private Context mContext;
    private GroupListing mListener;

    public GroupListingAdapter(Context context, ArrayList<GroupData> dataList, GroupListing listener) {
        this.mList = dataList;
        this.mContext = context;
        this.mListener = listener;
    }


    public void setDataList(ArrayList<GroupData> dataList) {
        this.mList = dataList;
        notifyDataSetChanged();
    }
    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_group_list_list_format, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {
        itemRowHolder.setPosition(i);
        final String sectionName = mList.get(i).getGroup_name();

        itemRowHolder.itemTitle.setText(sectionName);

        if("Y".equalsIgnoreCase(mList.get(i).getElite()) ){
            itemRowHolder.icBadge.setVisibility(View.VISIBLE);
        }else{
            itemRowHolder.icBadge.setVisibility(View.INVISIBLE);
        }

        if("Y".equalsIgnoreCase(mList.get(i).getVerified()) ){
            itemRowHolder.icVerified.setVisibility(View.VISIBLE);
        }else{
            itemRowHolder.icVerified.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {
        int position;
        protected TextView itemTitle;

        protected ImageView icBadge;
        protected ImageView icVerified;
        protected ProgressBar bar;

        public void setPosition(int position) {
            this.position = position;
        }
        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle =  view.findViewById(R.id.group_name);

            this.icBadge =  view.findViewById(R.id.iv_badge);
            this.icVerified =  view.findViewById(R.id.iv_verified);
            view.setOnClickListener(v -> {
                mListener.onClickEvent(mList.get(position),"List");
            });
        }

    }

}