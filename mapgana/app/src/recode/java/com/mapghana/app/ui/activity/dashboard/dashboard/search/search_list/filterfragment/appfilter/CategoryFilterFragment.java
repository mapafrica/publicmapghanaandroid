package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.Category;
import com.mapghana.app.model.FilterModel;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter.adapter.CategoryFilterAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.app.utils.Constants;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFilterFragment extends AppBaseFragment {

    private List<Category.DataBean> categoryList;
    private RecyclerView rvTypes;
    private static OnCategorySelectedListener onCategorySelectedListeners;

    private CategoryFilterAdapter adapter;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_apply_filter;
    }

    @Override
    public void initializeComponent() {
        init();
        rvTypes=(RecyclerView)getView().findViewById(R.id.rvTypes);
        rvTypes.setLayoutManager(new LinearLayoutManager(getContext()));
        categoryList=new ArrayList<>();
        adapter=new CategoryFilterAdapter(this,categoryList);
        rvTypes.setAdapter(adapter);

        getCategory();
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(false);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(false);
        getNavHandler().setExploreVisibility(false);
    }

    public static CategoryFilterFragment newInstance( OnCategorySelectedListener onCategorySelectedListener) {
        onCategorySelectedListeners=onCategorySelectedListener;
        Bundle args = new Bundle();

        CategoryFilterFragment fragment = new CategoryFilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void getCategory() {
        if (ConnectionDetector.isNetAvail(getContext())){
            displayProgressBar(false);
            RestClient restClient;
            restClient=new RestClient(getContext());
            restClient.callback(this).category();
        }
        else {
            displayToast(Constants.No_Internet);
        }
    }

    private void setSelectedItem(){
        if (categoryList!=null && categoryList.size()>0){
            String s= DefaulFilterSpf.getFilter(getContext());
            Gson gson=new Gson();
            FilterModel filterModel= gson.fromJson(s, FilterModel.class);

            if (filterModel !=null && filterModel.getCategoty_id()!=null && !filterModel.getCategoty_id().equals("")){
                int category_id=Integer.parseInt(filterModel.getCategoty_id());
                for (int i=0; i<categoryList.size();i++){
                    if (categoryList.get(i).getId()==category_id){
                        updateUI(i);
                    }
                }
            }
        }
    }

    private void updateUI(int pos){
        for (int i=0; i<categoryList.size(); i++){
            if (i==pos){
                categoryList.get(i).setSelected(true);
            }
            else {
                categoryList.get(i).setSelected(false);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId== ApiIds.ID_CATEGORY) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Category category = gson.fromJson(s, Category.class);
                    if (category.getStatus()!=0){
                        categoryList.addAll(category.getData());
                        if (categoryList.size()>0){
                            adapter.notifyDataSetChanged();
                            setSelectedItem();
                        }
                    }
                    else {
                        displayErrorDialog("Error", category.getError());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        }
        else {
            displayErrorDialog("Error", response.message());
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }

    @Override
    public void onAdapterClickListener(int position) {
        if (categoryList!=null && categoryList.size()>0){
            onCategorySelectedListeners.onCategorySelectedListener(String.valueOf(categoryList.get(position).getId()));
        }
    }

    public interface OnCategorySelectedListener{
        void onCategorySelectedListener(String categoty_id);
    }
}
