package com.mapghana.app.model;

public class CompressionModel {
    private long id;
    private String sourcePath;
    private String destinationPath;
    private int compressionStatus;

    private boolean isImage;
    private boolean isVideoCompressed;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public int getCompressionStatus() {
        return compressionStatus;
    }

    public void setCompressionStatus(int compressionStatus) {
        this.compressionStatus = compressionStatus;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }

    public boolean isVideoCompressed() {
        return isVideoCompressed;
    }

    public void setVideoCompressed(boolean videoCompressed) {
        isVideoCompressed = videoCompressed;
    }
}
