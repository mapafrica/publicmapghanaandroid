package com.mapghana.app.ui.sidemenu.Event;

public interface DialogCallback {

    void onSave(String type);
    void onCancel(String type);

}
