package com.mapghana.app.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseDialog;

public class MessageDialog extends AppBaseDialog {
    private Context context;
    private TextView tv_msg, tv_cancel, tv_login;
    private ImageView iv_close;
    private  LoginListener loginListener;

    public void setLoginListener(LoginListener loginListener) {
        this.loginListener = loginListener;
    }

    public MessageDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResourceView() {
        return R.layout.dialog_message;
    }

    @Override
    protected void initializeComponent() {
        tv_msg=this.findViewById(R.id.tv_msg);
        tv_cancel=this.findViewById(R.id.tv_cancel);
        tv_login=this.findViewById(R.id.tv_action);
        iv_close=this.findViewById(R.id.iv_close);

        tv_cancel.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        iv_close.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_cancel:
            case R.id.iv_close:
                dismiss();
                break;
            case R.id.tv_action:
                if (loginListener!=null){
                    loginListener.onLoginListener();
                }
                break;
        }
    }

    public interface LoginListener{
        void onLoginListener();
    }
}
