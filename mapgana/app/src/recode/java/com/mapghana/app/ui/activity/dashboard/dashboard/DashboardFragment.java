package com.mapghana.app.ui.activity.dashboard.dashboard;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.ColorUtils;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.helpers.navigation.NavigationViewHandler;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.model.IndividualPosts;
import com.mapghana.app.model.ObjectDetail;
import com.mapghana.app.model.OrganizationPosts;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.LocationServiceListner;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.ui.activity.dashboard.dashboard.add_location.AddLocationFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.PostDetailFragment;
import com.mapghana.app.ui.sidemenu.Event.EventDetailFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog.PostListingDialog;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.Utils;
import com.mapghana.customviews.TypefaceTextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends AppBaseFragment implements
        NavigationViewHandler.OnSpinnerItemChangeListeners, OnMapReadyCallback, PermissionsListener, LocationServiceListner {

    private HashMap<Marker, OrganizationPosts.DataBean> markerHashMap_Org;
    private HashMap<Marker, IndividualPosts.DataBean> markerHashMap_Indi;
    private HashMap<Marker, GlobeSearch.DataBean> markerHashMap_All;
    private ArrayList<OrganizationPosts.DataBean> orgList;
    private ArrayList<IndividualPosts.DataBean> indiList;
    private ArrayList<EventDetail> eventList;
    private ArrayList<ObjectDetail> objectList;
    private ArrayList<GlobeSearch.DataBean> allList;
    private MapView mapView;
    private MapboxMap map;
    private PermissionsManager permissionsManager;
    public LocationComponent locationComponent;
    private Point originPoint;
    private int TYPE;
    private float zoomLevel;
    private Marker marker_current_loc;
    private double log, lat;
    private boolean isAlreadyRequesting;
    PostListingDialog dialog;
    private LinearLayout ll_view;
    private ImageButton imgMyLoc;
    private Location previousCameraPosition;
    private SymbolManager symbolManager;
    private CircleManager circleManager;
    private String type;
    private Handler reHitHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            reHitHandler.removeCallbacks(runnable);
            try {
                getDashboardActivity().navigationViewHandler.getPostLists();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public void initializeComponent() {
        init();
        isAlreadyRequesting = false;
        AppCompatTextView tvPostListing;
        tvPostListing = getView().findViewById(R.id.tvPostListing);
        imgMyLoc = getView().findViewById(R.id.imgMyLoc);
        ll_view = getView().findViewById(R.id.ll_view);
        dialog = new PostListingDialog(getActivity(), this);
        orgList = new ArrayList<>();
        indiList = new ArrayList<>();
        allList = new ArrayList<>();
        eventList = new ArrayList<>();
        objectList = new ArrayList<>();
        tvPostListing.setOnClickListener(this);
        imgMyLoc.setOnClickListener(this);
        markerHashMap_Org = new HashMap<>();
        markerHashMap_Indi = new HashMap<>();
        markerHashMap_All = new HashMap<>();
        zoomLevel = 13;
        try {
            getDashboardActivity().navigationViewHandler.newInstance(this);
            reHitHandler.postDelayed(runnable, 100);
            getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    setMapPadding();
                }
            });
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        mapView = getView().findViewById(R.id.map_view);
        Location location = Locationservice.sharedInstance().getLastLocation();
        if (location != null) {
            originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
        }
        mapView.getMapAsync(this);
    }

    private void setMapPadding() {
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().setMapPadding(0,
                    Math.round(getResources().getDimension(R.dimen.dp50)),
                    0, ll_view.getHeight() - Math.round(imgMyLoc.getHeight() * 0.80f));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(true);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitleTextVisibilty(false);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setExploreVisibility(false);

        try {
            String currentLocationAddress = getApplication().getCurrentLocationAddress();
            if (currentLocationAddress != null && !currentLocationAddress.isEmpty()) {
                getNavHandler().setNavLocationTextVisibilty(true);
                getNavHandler().setNavAddressText(currentLocationAddress);
            }


        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }


    private void openEventDetail(String id, AppBaseActivity baseActivity) {
        EventDetailFragment eventDetail = EventDetailFragment.newInstance("Event", id);

        baseActivity.changeFragment(eventDetail, true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0, R.anim.alpha_gone_anim, true);

    }


    private void changeFragment(int post_id) {
        if (post_id == 0) {
            return;
        }
        if(Constants.ORGANISATION.equalsIgnoreCase(type) ||  Constants.INDIVIDUALS.equalsIgnoreCase(type) ) {
            PostDetailFragment detailsFragment = new PostDetailFragment();
            detailsFragment.setPost_id(post_id);
            try {
                getDashboardActivity().changeFragment(detailsFragment, true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }else if(Constants.events.equalsIgnoreCase(type)) {
            openEventDetail(Integer.toString(post_id),(AppBaseActivity) getActivity());

        }

    }

    @Override
    public void viewCreateFromBackStack() {
        super.viewCreateFromBackStack();

        init();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvPostListing:
                if (dialog != null) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    dialog.show();
                }
                break;
            case R.id.imgMyLoc:
                setMarkersOnMapAgain();
                break;
        }
    }

    @Override
    public void onAdapterClickListener(String action) {
        AddLocationFragment fragment = new AddLocationFragment();
        if (action.equals(getResources().getString(R.string.Individuals))) {
            fragment.setType(getResources().getString(R.string.Individuals));
        } else if (action.equals(getResources().getString(R.string.Organizations))) {
            fragment.setType(getResources().getString(R.string.Organizations));
        }
        try {
            getDashboardActivity().changeFragment(fragment, true, false, 0, R.anim.alpha_visible_anim,
                    0, 0, R.anim.alpha_gone_anim, true);
            // getDashboardActivity().getMapHandler().addBottomFragment(fragment);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void OnSpinnerItemOrganizationListeners(List<OrganizationPosts.DataBean> orList, String type) {
        if (!isVisible()) return;
        this.type = type;
        clearIndiHashMap();
        clearOrgHashMap();
        clearAllHashMap();
        clearAllList();
        clearIndiList();
        clearOrgList();
        TYPE = 0;

        symbolManager.deleteAll();
        orgList.addAll(orList);
        setMarkerOfOrganization(orList);
    }

    @Override
    public void OnSpinnerItemIndividualListeners(List<IndividualPosts.DataBean> inList, String type) {
        if (!isVisible()) return;
        this.type = type;
        clearIndiHashMap();
        clearOrgHashMap();
        clearAllHashMap();
        clearAllList();
        clearIndiList();
        clearOrgList();
        TYPE = 1;
        symbolManager.deleteAll();
        indiList.addAll(inList);
        setMarkerOfIndividual(inList);


    }

    @Override
    public void OnSpinnerItemEventListeners(List<EventDetail> eventList, String type) {
        symbolManager.deleteAll();
        this.type = type;
        eventList.addAll(eventList);
        setMarkerOfEvent(eventList);
    }

    @Override
    public void OnSpinnerItemObjectListeners(List<ObjectDetail> inList, String type) {
        symbolManager.deleteAll();
        this.type = type;
        objectList.addAll(inList);
        setMarkerObject(inList);
    }

    @Override
    public void OnSpinnerItemAllListeners(List<GlobeSearch.DataBean> allList) {
        if (!isVisible()) return;

        clearIndiHashMap();
        clearOrgHashMap();
        clearAllHashMap();
        clearAllList();
        clearIndiList();
        clearOrgList();
        TYPE = 2;
        symbolManager.deleteAll();
        this.allList.addAll(allList);
        setMarkerOfAll(this.allList);

    }


    private void setMarkersOnMapAgain() {
        map.easeCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originPoint.latitude(), originPoint.longitude()), 12));
    }

    private void setCurrentLocation() {

        if (lat != 0 && log != 0) {
            try {
                marker_current_loc = getDashboardActivity().
                        getMapHandler().addMarker(getContext(), R.mipmap.my_location, new LatLng(lat, log),
                        Constants.You, "");
                if (TYPE == 0) {
                    OrganizationPosts.DataBean bean = new OrganizationPosts.DataBean();
                    bean.setName(Constants.You);
                    markerHashMap_Org.put(marker_current_loc, bean);
                } else if (TYPE == 1) {
                    IndividualPosts.DataBean bean = new IndividualPosts.DataBean();
                    bean.setName(Constants.You);
                    markerHashMap_Indi.put(marker_current_loc, bean);
                } else if (TYPE == 2) {
                    GlobeSearch.DataBean bean = new GlobeSearch.DataBean();
                    bean.setName(Constants.You);
                    markerHashMap_All.put(marker_current_loc, bean);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }


        }
    }

    private void setMarkerOfOrganization(List<OrganizationPosts.DataBean> data) {
        clearOrgHashMap();
        symbolManager.deleteAll();
        setCurrentLocation();
        try {
            if (data != null && data.size() > 0) {
                int resource = 0;
                for (int i = 0; i < data.size(); i++) {
                    OrganizationPosts.DataBean dataBean = data.get(i);

                    LatLng latLng = new LatLng(dataBean.getLat(),
                            dataBean.getLog());
                    if (dataBean.getStatus().trim().equalsIgnoreCase(Constants.verified)) {
                        resource = R.mipmap.location_gree_org;
                    } else if (dataBean.getStatus().trim().equalsIgnoreCase(Constants.unverified)) {
                        resource = R.mipmap.location_red_org;
                    } else {
                        resource = R.mipmap.location_gry_org;
                    }
                    String categoryName = "Category: ";
                    OrganizationPosts.DataBean.CategoryBean category = dataBean.getCategory();
                    if (category != null) {
                        categoryName = categoryName + category.getName();
                    }
                    OrganizationPosts.DataBean.SubcategoryBean subcategory = dataBean.getSubcategory();
                    if (subcategory != null) {
                        categoryName = categoryName + " (" + subcategory.getName() + ")";
                    }
                    if (latLng != null) {

                        if ("general".equalsIgnoreCase(data.get(i).getLocation_profile())) {
                            createPolygon(latLng);
                            String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_icon_transparent);
                            map.getStyle().addImage("search_marker-" + data.get(i).getId(), bm);
                            symbolManager.create(new SymbolOptions()
                                    .withLatLng(latLng)
                                    .withIconImage("search_marker-" + data.get(i).getId())
                                    //set the below attributes according to your requirements
                                    .withIconSize(0.5f)
                                    .withIconOffset(new Float[]{0f, -1.5f})
                                    .withZIndex(10)
                                    .withTextField(markerOption)
                                    .withTextHaloColor("rgba(255, 255, 255, 100)")
                                    .withTextHaloWidth(5.0f)
                                    .withTextAnchor("top")
                                    .withTextOffset(new Float[]{0f, 1.5f})
                            );
                        } else {
                            String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                            Bitmap bm = BitmapFactory.decodeResource(getResources(), resource);
                            map.getStyle().addImage("search_marker-" + data.get(i).getId(), bm);
                            symbolManager.create(new SymbolOptions()
                                    .withLatLng(latLng)
                                    .withIconImage("search_marker-" + data.get(i).getId())
                                    //set the below attributes according to your requirements
                                    .withIconSize(0.5f)
                                    .withIconOffset(new Float[]{0f, -1.5f})
                                    .withZIndex(10)
                                    .withTextField(markerOption)
                                    .withTextHaloColor("rgba(255, 255, 255, 100)")
                                    .withTextHaloWidth(5.0f)
                                    .withTextAnchor("top")
                                    .withTextOffset(new Float[]{0f, 1.5f})
                            );
                        }
                    } else {
                        displayToast("Invalid latitude and longitude");
                    }
                }
            } else {
            }
        } catch (Exception e) {
        }
        isAlreadyRequesting = false;

    }


    private void createPolygon(LatLng latLng) {
        circleManager.create(new CircleOptions()
                .withLatLng(latLng)
                .withCircleColor(ColorUtils.colorToRgbaString(getActivity().getResources().getColor(R.color.colorYellowtransparent)))
                .withCircleRadius(100f)
                .withDraggable(false)

        );
    }

    private void setMarkerOfIndividual(List<IndividualPosts.DataBean> data) {
        clearIndiHashMap();
        symbolManager.deleteAll();
        Marker marker;
        setCurrentLocation();

        if (data != null && data.size() > 0) {
            int resourse = 0;
            String status = "";
            String gender = "";
            String category = "";
            for (int i = 0; i < data.size(); i++) {
                LatLng latLng = new LatLng(data.get(i).getLat(),
                        data.get(i).getLog());
                status = data.get(i).getStatus().trim();
                gender = data.get(i).getGender().trim();

                if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.individual_green_gril;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.individual_red_girl;
                    } else {
                        resourse = R.mipmap.indi_girl_gray;
                    }
                } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                    if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.individual_green_man;
                    } else if (status != null && !status.equals("") &&
                            status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.individual_red_man;
                    } else {
                        resourse = R.mipmap.individual_gray_man;
                    }
                }
                if (latLng != null) {

                    if ("general".equalsIgnoreCase(data.get(i).getLocation_profile())) {
                        createPolygon(latLng);
                        String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_icon_transparent);
                        map.getStyle().addImage("search_marker-" + data.get(i).getId(), bm);
                        symbolManager.create(new SymbolOptions()
                                .withLatLng(latLng)
                                .withIconImage("search_marker-" + data.get(i).getId())
                                //set the below attributes according to your requirements
                                .withIconSize(0.5f)
                                .withIconOffset(new Float[]{0f, -1.5f})
                                .withZIndex(10)
                                .withTextField(markerOption)
                                .withTextHaloColor("rgba(255, 255, 255, 100)")
                                .withTextHaloWidth(5.0f)
                                .withTextAnchor("top")
                                .withTextOffset(new Float[]{0f, 1.5f})
                        );
                    } else {
                        String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                        Bitmap bm = BitmapFactory.decodeResource(getResources(), resourse);
                        map.getStyle().addImage("search_marker-" + data.get(i).getId(), bm);
                        symbolManager.create(new SymbolOptions()
                                .withLatLng(latLng)
                                .withIconImage("search_marker-" + data.get(i).getId())
                                //set the below attributes according to your requirements
                                .withIconSize(0.5f)
                                .withIconOffset(new Float[]{0f, -1.5f})
                                .withZIndex(10)
                                .withTextField(markerOption)
                                .withTextHaloColor("rgba(255, 255, 255, 100)")
                                .withTextHaloWidth(5.0f)
                                .withTextAnchor("top")
                                .withTextOffset(new Float[]{0f, 1.5f})
                        );
                    }
                } else {
                    displayToast("Invalid latitude and longitude");
                }
            }
        } else {
        }
        isAlreadyRequesting = false;

    }

    private void setMarkerObject(List<ObjectDetail> data) {

            try{

                if(data != null && data.size() > 0) {
                    int drawableResourceId = 0;
                    for(int i = 0; i < data.size(); i++) {
                        LatLng latLng = null;
                        if(!TextUtils.isEmpty(data.get(i).getLat()) && !TextUtils.isEmpty(data.get(i).getLog())) {
                            try {
                                latLng = new LatLng(Double.valueOf(data.get(i).getLat()),
                                        Double.valueOf(data.get(i).getLog()));
                            } catch (IllegalArgumentException e) {
                                continue;
                            }

                            if (data.get(i).getType().equalsIgnoreCase("deaddrop")) {
                                drawableResourceId = R.drawable.deaddrop;

                            } else if (data.get(i).getType().equalsIgnoreCase("zi network")) {
                                drawableResourceId = R.drawable.zi_network;

                            } else if (data.get(i).getType().equalsIgnoreCase("relic")) {
                                drawableResourceId = R.drawable.relic;

                            } else {
                                drawableResourceId = R.mipmap.location_gry_org;

                            }

                            if ("general".equalsIgnoreCase(data.get(i).getLocation_profile())) {
                                createPolygon(latLng);
                                String markerOption = data.get(i).getName();
                                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_icon_transparent);
                                map.getStyle().addImage("object-" + data.get(i).getId(), bm);
                                symbolManager.create(new SymbolOptions()
                                        .withLatLng(latLng)
                                        .withIconImage("object-" + data.get(i).getId())
                                        //set the below attributes according to your requirements
                                        .withIconSize(0.5f)
                                        .withIconOffset(new Float[]{0f, -1.5f})
                                        .withZIndex(10)
                                        .withTextField(markerOption)
                                        .withTextHaloColor("rgba(255, 255, 255, 100)")
                                        .withTextHaloWidth(5.0f)
                                        .withTextAnchor("top")
                                        .withTextOffset(new Float[]{0f, 1.5f})
                                );
                            } else {
                                String markerOption = data.get(i).getName();
                                Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableResourceId);
                                map.getStyle().addImage("object-" + data.get(i).getId(), bm);
                                symbolManager.create(new SymbolOptions()
                                        .withLatLng(latLng)
                                        .withIconImage("object-" + data.get(i).getId())
                                        //set the below attributes according to your requirements
                                        .withIconSize(0.5f)
                                        .withIconOffset(new Float[]{0f, -1.5f})
                                        .withZIndex(10)
                                        .withTextField(markerOption)
                                        .withTextHaloColor("rgba(255, 255, 255, 100)")
                                        .withTextHaloWidth(5.0f)
                                        .withTextAnchor("top")
                                        .withTextOffset(new Float[]{0f, 1.5f})
                                );
                            }
                        }

                    }

                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    private void setMarkerOfEvent(List<EventDetail> data) {
        symbolManager.deleteAll();

        if (data != null && data.size() > 0) {
            int resourse = 0;
            for (int i = 0; i < data.size(); i++) {

                if (data.get(i).getEvent_date_time().size() > 0) {
                    String lat = data.get(i).getEvent_date_time().get(0).getLat();
                    String log = data.get(i).getEvent_date_time().get(0).getLog();
                    if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(log)) {
                        LatLng latLng = new LatLng(Double.valueOf(lat), Double.valueOf(log));

                        resourse = R.drawable.event_marker;

                        if (latLng != null) {

                            String markerOption = data.get(i).getName();
                            Bitmap bm = BitmapFactory.decodeResource(getActivity().getResources(), resourse);
                            map.getStyle().addImage("event_marker-" + data.get(i).getId(), bm);
                            symbolManager.create(new SymbolOptions()
                                    .withLatLng(latLng)
                                    .withIconImage("event_marker-" + data.get(i).getId())
                                    //set the below attributes according to your requirements
                                    .withIconSize(0.5f)
                                    .withIconOffset(new Float[]{0f, -1.5f})
                                    .withZIndex(10)
                                    .withTextField(markerOption)
                                    .withTextHaloColor("rgba(255, 255, 255, 100)")
                                    .withTextHaloWidth(5.0f)
                                    .withTextAnchor("top")
                                    .withTextOffset(new Float[]{0f, 1.5f})
                            );

                        } else {
                            displayToast("Invalid latitude and longitude");
                        }
                    }
                }
            }
        } else {
        }
        isAlreadyRequesting = false;

    }


    private void setMarkerOfAll(ArrayList<GlobeSearch.DataBean> data) {
        clearAllHashMap();
        symbolManager.deleteAll();
        Marker marker;
        String category = "";

        setCurrentLocation();
        if (data != null && data.size() > 0) {
            int resourse = 0;
            String status = "";
            String gender = "";
            String postFor = "";
            for (int i = 0; i < data.size(); i++) {
                GlobeSearch.DataBean dataBean = data.get(i);
                LatLng latLng = new LatLng(dataBean.getLat(),
                        dataBean.getLog());

                status = dataBean.getStatus().trim();
                gender = dataBean.getGender().trim();
                postFor = dataBean.getPost_for();
                if (postFor != null && postFor.equalsIgnoreCase(Constants.individuals)) {
                    if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.female)) {
                        if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.verified)) {
                            resourse = R.mipmap.individual_green_gril;
                        } else if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.unverified)) {
                            resourse = R.mipmap.individual_red_girl;
                        } else {
                            resourse = R.mipmap.indi_girl_gray;
                        }
                    } else if (gender != null && !gender.equals("") && gender.equalsIgnoreCase(Constants.male)) {
                        if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.verified)) {
                            resourse = R.mipmap.individual_green_man;
                        } else if (status != null && !status.equals("") &&
                                status.equalsIgnoreCase(Constants.unverified)) {
                            resourse = R.mipmap.individual_red_man;
                        } else {
                            resourse = R.mipmap.individual_gray_man;
                        }
                    }
                    category = "Occupation: " + dataBean.getCategory().getName();

                } else if (postFor != null &&
                        postFor.equalsIgnoreCase(Constants.organizations)) {
                    if (status != null &&
                            status.equalsIgnoreCase(Constants.verified)) {
                        resourse = R.mipmap.location_gree_org;
                    } else if (status != null && status.equalsIgnoreCase(Constants.unverified)) {
                        resourse = R.mipmap.location_red_org;
                    } else {
                        resourse = R.mipmap.location_gry_org;
                    }
                    category = "Category: " + dataBean.getCategory().getName();
                    GlobeSearch.DataBean.SubcategoryBean subcategory = dataBean.getSubcategory();
                    if (subcategory != null) {
                        category = category + " (" + subcategory.getName() + ")";
                    }

                }
                if (latLng != null) {

                    if ("general".equalsIgnoreCase(data.get(i).getLocation_profile())) {
                        createPolygon(latLng);
                        String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_icon_transparent);
                        map.getStyle().addImage("search_marker-" + data.get(i).getId(), bm);
                        symbolManager.create(new SymbolOptions()
                                .withLatLng(latLng)
                                .withIconImage("search_marker-" + data.get(i).getId())
                                //set the below attributes according to your requirements
                                .withIconSize(0.5f)
                                .withIconOffset(new Float[]{0f, -1.5f})
                                .withZIndex(10)
                                .withTextField(markerOption)
                                .withTextHaloColor("rgba(255, 255, 255, 100)")
                                .withTextHaloWidth(5.0f)
                                .withTextAnchor("top")
                                .withTextOffset(new Float[]{0f, 1.5f})
                        );
                    } else {
                        String markerOption = data.get(i).getName() + "\nCategory: " + data.get(i).getCategory().getName() + "\nAddress: " + data.get(i).getAddress() + "\nStatus: " + data.get(i).getStatus();
                        Bitmap bm = BitmapFactory.decodeResource(getResources(), resourse);
                        map.getStyle().addImage("search_marker-" + data.get(i).getId(), bm);
                        symbolManager.create(new SymbolOptions()
                                .withLatLng(latLng)
                                .withIconImage("search_marker-" + data.get(i).getId())
                                //set the below attributes according to your requirements
                                .withIconSize(0.5f)
                                .withIconOffset(new Float[]{0f, -1.5f})
                                .withZIndex(10)
                                .withTextField(markerOption)
                                .withTextHaloColor("rgba(255, 255, 255, 100)")
                                .withTextHaloWidth(5.0f)
                                .withTextAnchor("top")
                                .withTextOffset(new Float[]{0f, 1.5f})
                        );
                    }
                } else {
                    displayToast("Invalid latitude and longitude");
                }
            }
        } else {
        }
        isAlreadyRequesting = false;

    }

    public void clearOrgHashMap() {
        if (markerHashMap_Org != null && markerHashMap_Org.size() > 0) {
            markerHashMap_Org.clear();
        }

    }

    public void clearOrgList() {
        if (orgList != null && orgList.size() > 0) {
            orgList.clear();
        }
    }

    public void clearIndiHashMap() {
        if (markerHashMap_Indi != null && markerHashMap_Indi.size() > 0) {
            markerHashMap_Indi.clear();
        }
    }

    public void clearIndiList() {
        if (indiList != null && indiList.size() > 0) {
            indiList.clear();
        }
    }

    public void clearAllHashMap() {
        if (markerHashMap_All != null && markerHashMap_All.size() > 0) {
            markerHashMap_All.clear();
        }
    }

    public void clearAllList() {
        if (allList != null && allList.size() > 0) {
            allList.clear();
        }
    }

    private void clearData() {
        if (!isVisible()) return;

        clearIndiHashMap();
        clearOrgHashMap();
        clearAllHashMap();
        clearAllList();
        clearIndiList();
        clearOrgList();
        try {
            getDashboardActivity().getMapHandler().getMapBoxFragment().clearMarker();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.map = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            enableLocationComponent(style);
            symbolManager = new SymbolManager(mapView, map, style);
            circleManager = new CircleManager(mapView, map, style);
            symbolManager.setIconAllowOverlap(false);  //your choice t/f
            symbolManager.setTextAllowOverlap(false);  //your choice t/f
            addDestinationIconSymbolLayer(style);

            Location location = Locationservice.sharedInstance().getLastLocation();
            if (location != null) {
                Point originPoint = Point.fromLngLat(location.getLongitude(), location.getLatitude());
            }

            symbolManager.addClickListener(symbol -> {
                String[] str = symbol.getIconImage().split("-");
                changeFragment(Integer.valueOf(str[1]));
            });
        });


    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = map.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(map.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void userLocationChange(Location location) {
        Toast.makeText(getActivity(), "" + location.getLatitude(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest) {

    }

}