package com.mapghana.app.ui.sidemenu.customOrder;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.CustomOrderModel;
import com.mapghana.app.model.Session;
import com.mapghana.app.model.UserCustomOrderListModel;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.utils.AppPreferences;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class AdminCustomListingFragment extends AppBaseFragment implements CustomOrderAdapter.CustomOrderInterface {

    private RecyclerView mRecyclerView;
    private CustomOrderAdapter customOrderAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView mNoDataFound;

    public static AdminCustomListingFragment newInstance(String id) {
        AdminCustomListingFragment fragment = new AdminCustomListingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle("Search Custom Order");
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_admin_custom_order_listing;
    }

    @Override
    public void initializeComponent() {
        initToolBar();
        findViewById(getView());
    }

    @Override
    public void reInitializeComponent() {
    }

    private void findViewById(View view) {
        mRecyclerView = view.findViewById(R.id.custom_order);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mNoDataFound = view.findViewById(R.id.img_no_data_found);

        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setHasFixedSize(true);

        Session session = AppPreferences.getSession();
        callGetApi(session);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            callGetApi(session);
        });

        initRecyclerView();
    }

    private void initRecyclerView() {

        customOrderAdapter = new CustomOrderAdapter(this, (AppBaseActivity) getActivity(),new ArrayList<CustomOrderModel>(), false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(customOrderAdapter);
    }

    private void callGetApi(Session session) {

        RestClient restClient = new RestClient(getContext());
        restClient.callback(this).getUserCustomOrderList(String.valueOf(session.getId()));
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_USER_CUSTOM_ORDER_LIST) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    swipeRefreshLayout.setRefreshing(false);
                    UserCustomOrderListModel objectListingModel = gson.fromJson(s, UserCustomOrderListModel.class);
                    if (objectListingModel.getStatus() == 1) {
                        //customOrderAdapter.setDataList(objectListingModel.getData());
                        if(objectListingModel.getData().size() > 0) {
                            mNoDataFound.setVisibility(View.VISIBLE);
                        }
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        swipeRefreshLayout.setRefreshing(false);
        displayErrorDialog("Error", error);

    }

    private void openMakePayemntScreen(CustomOrderModel model, String screenType, String paymentType, int amount) {
        Intent intent = new Intent(getActivity(), MapghanaPayment.class);
        intent.putExtra("id", model.getId());
        intent.putExtra("screen_type", screenType);
        intent.putExtra("payment_type", paymentType);
        intent.putExtra("amount", amount);
        startActivity(intent);
    }

    @Override
    public void onPayClick(CustomOrderModel model) {
        openMakePayemntScreen(model, "custom_order", "custom_order", model.getPrice());
    }

    @Override
    public void ItemClick(CustomOrderModel model) {

    }
}

