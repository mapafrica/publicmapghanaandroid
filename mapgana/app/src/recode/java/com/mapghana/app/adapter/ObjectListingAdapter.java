package com.mapghana.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.model.ObjectListingContent;
import com.mapghana.app.ui.sidemenu.Object.ObjectListing;

import java.util.ArrayList;

public class ObjectListingAdapter extends RecyclerView.Adapter<ObjectListingAdapter.ItemRowHolder> {

    private ArrayList<ObjectListingContent> mList;
    private Context mContext;
    private ObjectListing mListener;

    public ObjectListingAdapter(Context context, ArrayList<ObjectListingContent> dataList, ObjectListing listener) {
        this.mList = dataList;
        this.mContext = context;
        this.mListener = listener;
    }


    public void setDataList(ArrayList<ObjectListingContent> dataList) {
        this.mList = dataList;
        notifyDataSetChanged();
    }
    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_object_listing_category, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {
        itemRowHolder.setPosition(i);
        final String sectionName = mList.get(i).getName();
        itemRowHolder.itemTitle.setText(sectionName);

    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {
        int position;
        protected TextView itemTitle;

        public void setPosition(int position) {
            this.position = position;
        }
        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle =  view.findViewById(R.id.txt_name);
            view.setOnClickListener(v -> {
                mListener.onClickEvent(mList.get(position));
            });
        }

    }

}