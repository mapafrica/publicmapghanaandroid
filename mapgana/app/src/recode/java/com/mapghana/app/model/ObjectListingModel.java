package com.mapghana.app.model;

import java.io.Serializable;
import java.util.List;

public class ObjectListingModel implements Serializable {

    private int status;
    private String message;
    private List<ObjectListingContent> data;


    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<ObjectListingContent> getData() {
        return data;
    }


}

