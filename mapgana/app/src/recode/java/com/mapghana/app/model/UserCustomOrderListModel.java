package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class UserCustomOrderListModel implements Serializable {
    private int status;
    private String message;
    private ArrayList<CustomOrderModel> data;


    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<CustomOrderModel> getData() {
        return data;
    }
}
