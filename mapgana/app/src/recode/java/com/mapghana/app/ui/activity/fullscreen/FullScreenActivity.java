package com.mapghana.app.ui.activity.fullscreen;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.webkit.MimeTypeMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.model.GalleryPojo;
import com.mapghana.app.utils.Constants;
import com.mapghana.base.TabLayoutAdapter;
import com.rd.PageIndicatorView;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FullScreenActivity extends AppBaseActivity {

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_full_screen;
    }

    @Override
    public void initializeComponent() {
        PageIndicatorView pageIndicatorView;
        ViewPager viewPager;

        pageIndicatorView = (PageIndicatorView) findViewById(R.id.pageIndicatorView);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.getString("gallery") != null) {
            String gallery = bundle.getString("gallery");
            List<GalleryPojo> galleryList = new Gson().fromJson(gallery, new TypeToken<List<GalleryPojo>>() {
            }.getType());

            int position = bundle.getInt("position");
            int play_position = bundle.getInt("current_position");
            /*FullScreenPagerAdapter fullScreenPagerAdapter = new FullScreenPagerAdapter(galleryList, this,
                    position, current_position);*/

            TabLayoutAdapter tabLayoutAdapter = new TabLayoutAdapter(getFm());
            List<String> imageMimeList = Constants.getImageMimeList();
            List<String> videoMimeList = Constants.getVideoMimeList();

            for (int i = 0; i < galleryList.size(); i++) {

                GalleryPojo galleryPojo = galleryList.get(i);
                String extension = MimeTypeMap.getFileExtensionFromUrl(galleryPojo.getImage());
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                if (imageMimeList.contains(mimeType)) {
                    galleryPojo.setVideo(false);

                } else if (videoMimeList.contains(mimeType)) {
                    galleryPojo.setVideo(true);
                }
                FullScreenImageFragment fullScreenImageFragment = new FullScreenImageFragment();
                fullScreenImageFragment.setGalleryPojo(galleryPojo);
                if (i == position)
                    fullScreenImageFragment.setPlay_position(play_position);
                tabLayoutAdapter.addFragment(fullScreenImageFragment, "");

            }
            viewPager.setAdapter(tabLayoutAdapter);
            pageIndicatorView.setViewPager(viewPager);
            pageIndicatorView.setCount(galleryList.size());
            pageIndicatorView.setSelection(position);
            viewPager.setCurrentItem(position);
        }
    }

    @Override
    public void setCalenderVisibility(boolean visibility) {

    }

    @Override
    public void setSearchVisibiltyInternal(boolean visibility) {

    }

    @Override
    public void setimgMultiViewVisibility(boolean visibility) {

    }

    @Override
    public void setAtoZZtoAVisibiltyInternal(boolean visibility) {

    }
}
