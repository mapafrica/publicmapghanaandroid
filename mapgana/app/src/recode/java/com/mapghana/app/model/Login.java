package com.mapghana.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ubuntu on 6/1/18.
 */

public class Login {


    /**
     * status : 1
     * message : successfully login
     * data : {"id":7,"name":"k1","username":"k1","email":"k1@gmail.com","dob":"08-01-2018","gender":"female","phone_number":"2222222222","image":null,"lat":null,"long":null,"device_id":"121212","device_type":"A","location":"jaipur","role":"user","status":"0","created_at":"2018-01-12 06:32:30","updated_at":"2018-01-12 06:32:30"}
     * error :
     */

    private int status;
    private String message;
    private Session data;
    private String error;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Session getData() {
        return data;
    }

    public void setData(Session data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
