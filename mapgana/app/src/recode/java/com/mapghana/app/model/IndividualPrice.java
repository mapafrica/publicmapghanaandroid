package com.mapghana.app.model;

public class IndividualPrice {

    private int advance_feature;
    private int optimization;
    private int phone_number_visibility;
    private int password_protect;

    public int getAdvance_feature() {
        return advance_feature;
    }

    public void setAdvance_feature(int advance_feature) {
        this.advance_feature = advance_feature;
    }

    public int getOptimization() {
        return optimization;
    }

    public void setOptimization(int optimization) {
        this.optimization = optimization;
    }

    public int getPhone_number_visibility() {
        return phone_number_visibility;
    }

    public void setPhone_number_visibility(int phone_number_visibility) {
        this.phone_number_visibility = phone_number_visibility;
    }

    public int getPassword_protect() {
        return password_protect;
    }

    public void setPassword_protect(int password_protect) {
        this.password_protect = password_protect;
    }
}
