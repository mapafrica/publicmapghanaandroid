package com.mapghana.app.ui.sidemenu.organization.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.model.Category;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.iterface.SetOnCategoryClickListener;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class SubTypeAdapter extends BaseRecycleAdapter {

    SetOnCategoryClickListener setOnCategoryClickListener;
    private List<Category.DataBean.SubCategoryBean> subCategoryList;


    public SubTypeAdapter(SetOnCategoryClickListener setOnCategoryClickListener,
                          List<Category.DataBean.SubCategoryBean> subCategoryList) {
        this.setOnCategoryClickListener = setOnCategoryClickListener;
        this.subCategoryList = subCategoryList;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_subtype;
    }

    @Override
    protected int getItemSize() {
        if (subCategoryList!=null && subCategoryList.size()>0){
            return subCategoryList.size();
        }
        return 0;
    }

    @Override
    protected BaseRecycleAdapter.ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }
    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private AppCompatTextView tvSubTitle;
        private LinearLayout llSubType;
        private int pos;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvSubTitle= itemView.findViewById(R.id.tvSubTitle);
            llSubType= itemView.findViewById(R.id.llSubType);
        }

        @Override
        public void setData(int position) {
            pos=position;
         tvSubTitle.setText(subCategoryList.get(position).getName());
         llSubType.setTag(position);
         llSubType.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        switch (v.getId()){
            case R.id.llSubType:
                setOnCategoryClickListener.setOnCategoryClickListener((Integer) v.getTag(), String.valueOf(subCategoryList.get((Integer) v.getTag()).getId()));
                break;
        }
        }
    }


}
