package com.mapghana.app.model;

import java.io.Serializable;

public class CustomOrderModel implements Serializable {

    private String id;
    private int user_id;
    private String title;
    private String description;
    private String budget;
    private String imageUrl;
    private String search_status;
    private int price;

    public CustomOrderModel(String title, String description, String budget, String imageUrl) {
        this.title = title;
        this.description = description;
        this.budget = budget;
        this.imageUrl = imageUrl;
    }

    public int getPrice() {
        return price;
    }

    public String getSearch_status() {
        return search_status;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getBudget() {
        return budget;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
