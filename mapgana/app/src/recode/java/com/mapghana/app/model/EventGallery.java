package com.mapghana.app.model;

import java.io.Serializable;

public class EventGallery implements Serializable {

    private int id;
    private int event_id;
    private String file_pathname;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getFile_pathname() {
        return file_pathname;
    }

    public void setFile_pathname(String file_pathname) {
        this.file_pathname = file_pathname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
