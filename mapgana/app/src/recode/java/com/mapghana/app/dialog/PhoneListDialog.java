package com.mapghana.app.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseDialog;
import com.mapghana.app.ui.activity.dashboard.dashboard.post_detail.adapter.DialPhoneAdapter;

/**
 * Created by ubuntu on 19/4/18.
 */

public class PhoneListDialog extends AppBaseDialog {

    private RecyclerView rv_view;
    private ImageView iv_close;
    private String[] phone_array;
    private DialPhoneAdapter.DialPhoneListener dialPhoneListener;

    public DialPhoneAdapter.DialPhoneListener getDialPhoneListener() {
        return dialPhoneListener;
    }

    public void setDialPhoneListener(DialPhoneAdapter.DialPhoneListener dialPhoneListener) {
        this.dialPhoneListener = dialPhoneListener;
    }



    public PhoneListDialog(@NonNull Context context, String[] phone_array) {
        super(context);
        this.phone_array=phone_array;
    }

    @Override
    protected int getLayoutResourceView() {
        return R.layout.dialog_calls;
    }

    @Override
    protected void initializeComponent() {
        rv_view=this.findViewById(R.id.rv_view);
        iv_close=this.findViewById(R.id.iv_close);
        intializeRecyclerView();

        iv_close.setOnClickListener(this);
    }

    private void intializeRecyclerView(){
        rv_view.setLayoutManager(new LinearLayoutManager(getContext()));
        DialPhoneAdapter adapter=new DialPhoneAdapter(phone_array, dialPhoneListener);
        rv_view.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_close:
                dismiss();
                break;
        }
    }
}
