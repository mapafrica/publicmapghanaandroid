package com.mapghana.app.model;

import java.io.Serializable;
import java.util.List;

public class PopularIndividual implements Serializable {

    private int code;
    private String error;
    private int status;
    private String message;
    private List<GlobeSearch.DataBean> data;

    public int getCode() {
        return code;
    }

    public String getError() {
        return error;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<GlobeSearch.DataBean> getData() {
        return data;
    }
}
