package com.mapghana.app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class EventDetail implements Serializable {

    private int id;
    private int user_id;
    private String name;
    private String image;
    private String type;
    private String event_coverage_on;
    private String event_coverage;
    private String host_name;
    private int phone;
    private String email;
    private String paid_event;
    private int ticket_price;
    private String seat;
    private int no_of_seats;
    private String mapgh_ticket_hosting;
    private String post_payment_channel;
    private String branch_name;
    private String bank_name;
    private double account_number;
    private String gatekeeping_services;
    private String details;
    private String edate;
    private String qr_code;
    private String qr_status;
    private String vodafone;
    private String vodafone_holder_name;
    private String vodafone_holder_phone_num;
    private String vodafone_voucher;
    private String mtnmobile;
    private String mtnmobile_holder_name;
    private String mtnmobile_holder_phone_num;
    private String mtnmobile_voucher;
    private String airteltigo;
    private String airteltigo_holder_name;
    private String airteltigo_holder_phone_num;
    private String airteltigo_voucher;
    private boolean isMapNavigation;
    private boolean isTicketBooked;
    public ArrayList<EventGallery> highlights_gallery;

    public boolean isMapNavigation() {
        return isMapNavigation;
    }

    public void setMapNavigation(boolean mapNavigation) {
        isMapNavigation = mapNavigation;
    }

    public boolean isTicketBooked() {
        return isTicketBooked;
    }

    public void setTicketBooked(boolean ticketBooked) {
        isTicketBooked = ticketBooked;
    }

    public ArrayList<EventGallery> getHighlights_gallery() {
        return highlights_gallery;
    }

    private ArrayList<EventInfoSection> event_date_time;
    public ArrayList<EventGallery> event_gallery;

    public String getVodafone() {
        return vodafone;
    }

    public String getVodafone_holder_name() {
        return vodafone_holder_name;
    }

    public String getVodafone_holder_phone_num() {
        return vodafone_holder_phone_num;
    }

    public String getVodafone_voucher() {
        return vodafone_voucher;
    }

    public String getMtnmobile() {
        return mtnmobile;
    }

    public String getMtnmobile_holder_name() {
        return mtnmobile_holder_name;
    }

    public String getMtnmobile_holder_phone_num() {
        return mtnmobile_holder_phone_num;
    }

    public String getMtnmobile_voucher() {
        return mtnmobile_voucher;
    }

    public String getAirteltigo() {
        return airteltigo;
    }

    public String getAirteltigo_holder_name() {
        return airteltigo_holder_name;
    }

    public String getAirteltigo_holder_phone_num() {
        return airteltigo_holder_phone_num;
    }

    public String getAirteltigo_voucher() {
        return airteltigo_voucher;
    }

    public String getQr_code() {
        return qr_code;
    }

    public String getQr_status() {
        return qr_status;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEdate() {
        return edate;
    }

    public ArrayList<EventInfoSection> getEvent_date_time() {
        return event_date_time;
    }

    public ArrayList<EventGallery> getEvent_gallery() {
        return event_gallery;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getType() {
        return type;
    }

    public String getEvent_coverage_on() {
        return event_coverage_on;
    }

    public String getEvent_coverage() {
        return event_coverage;
    }

    public String getHost_name() {
        return host_name;
    }

    public int getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPaid_event() {
        return paid_event;
    }

    public int getTicket_price() {
        return ticket_price;
    }

    public String getSeat() {
        return seat;
    }

    public int getNo_of_seats() {
        return no_of_seats;
    }

    public String getMapgh_ticket_hosting() {
        return mapgh_ticket_hosting;
    }

    public String getPost_payment_channel() {
        return post_payment_channel;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public double getAccount_number() {
        return account_number;
    }

    public String getGatekeeping_services() {
        return gatekeeping_services;
    }

    public String getDetails() {
        return details;
    }
}
