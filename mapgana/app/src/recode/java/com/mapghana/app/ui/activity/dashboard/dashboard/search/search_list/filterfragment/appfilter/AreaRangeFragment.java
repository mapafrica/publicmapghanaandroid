package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.appfilter;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.FilterModel;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.custom_rangebar.RangeBar;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AreaRangeFragment extends AppBaseFragment implements RangeBar.OnRangeBarChangeListener {


    private RangeBar rangeBar;
    private TypefaceTextView tvLeftDis, tvRightDis;
    private static OnRangeChangeListener onRangeChangeListeners;
    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_area_range;
    }

    @Override
    public void initializeComponent() {
        rangeBar=(RangeBar)getView().findViewById(R.id.rangeBar);
        tvLeftDis=(TypefaceTextView)getView().findViewById(R.id.tvLeftDis);
        tvRightDis=(TypefaceTextView)getView().findViewById(R.id.tvRightDis);
        rangeBar.setOnRangeBarChangeListener(this);


        /*
        // to format a text
        rangeBar.setFormatter(new IRangeBarFormatter() {
            @Override
            public String format(String value) {
                Typeface typeface=Typeface.createFromAsset(getContext().getAssets(), "opensans_regular.ttf");

                return null;
            }
        });*/

        setSelectedRange();
    }

    private void setSelectedRange() {
        String s=DefaulFilterSpf.getFilter(getContext());
        Gson gson=new Gson();
        FilterModel filterModel= gson.fromJson(s, FilterModel.class);

        if (filterModel !=null && filterModel.getDistance()!=null && !filterModel.getDistance().equals("")){
            rangeBar.setRangePinsByIndices(0, Integer.parseInt(filterModel.getDistance()));
        }
    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
        tvLeftDis.setText(leftPinValue+" KM");
        tvRightDis.setText(rightPinValue+" KM");
        onRangeChangeListeners.onRangeChangeListener(rightPinValue);
    }

    public interface OnRangeChangeListener{
        void onRangeChangeListener(String rightPinValue);
    }

    public static AreaRangeFragment newInstance(OnRangeChangeListener onRangeChangeListener) {
        onRangeChangeListeners=onRangeChangeListener;
        Bundle args = new Bundle();

        AreaRangeFragment fragment = new AreaRangeFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
