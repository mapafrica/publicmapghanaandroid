package com.mapghana.app.model;

import java.io.Serializable;
import java.util.List;

public class ObjectMapModel implements Serializable {

    private int status;
    private String message;
    private List<ObjectDetail> data;


    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<ObjectDetail> getData() {
        return data;
    }


}

