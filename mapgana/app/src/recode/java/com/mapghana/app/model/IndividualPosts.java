package com.mapghana.app.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 29/1/18.
 */

public class IndividualPosts {

    private int code;
    private String error;
    private int status;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

        private int id;
        private String name;
        private int user_id;
        private int city_id;
        private String post_for;
        private int category_id;
        private int sub_category_id;
        private ArrayList<String> email;
        private String address;
        private ArrayList<String> website;
        private String twitter;
        private String dob;
        private String gender;
        private String image;
        private String type;
        private String status;
        private String location_profile;
        private String phone_visibility;
        private String availability;
        private String optimization;
        private String mapghanaid;
        private String twitter1;
        private String instagram;
        private String facebook;
        private String youtube;
        private String soundclud;
        private ArrayList<String> blog;
        private ArrayList<String> gaming;
        private String profile_visibility;
        private String password_protected;
        private String password;
        private String is_payment_done;
        private Object opening_hours;
        private String notes;
        private String tags;
        private double lat;
        private double log;
        private Object deleted_at;
        private String created_at;
        private String updated_at;
        private int total_review;
        private double avg_rating;
        private CategoryBean category;
        private ArrayList<String> phone;
        private ArrayList<String> features;
        private ArrayList<PostGalleryBean> postGallery;

        public int getId() {
            return id;
        }

        public String getLocation_profile() {
            return location_profile;
        }

        public String getPhone_visibility() {
            return phone_visibility;
        }

        public String getAvailability() {
            return availability;
        }

        public String getOptimization() {
            return optimization;
        }


        public String getMapghanaid() {
            return mapghanaid;
        }

        public String getTwitter1() {
            return twitter1;
        }

        public String getInstagram() {
            return instagram;
        }

        public String getFacebook() {
            return facebook;
        }

        public String getYoutube() {
            return youtube;
        }

        public String getSoundclud() {
            return soundclud;
        }

        public ArrayList<String> getBlog() {
            return blog;
        }

        public void setBlog(ArrayList<String> blog) {
            this.blog = blog;
        }

        public ArrayList<String> getGaming() {
            return gaming;
        }

        public void setGaming(ArrayList<String> gaming) {
            this.gaming = gaming;
        }

        public String getProfile_visibility() {
            return profile_visibility;
        }

        public String getPassword_protected() {
            return password_protected;
        }

        public String getPassword() {
            return password;
        }

        public String getIs_payment_done() {
            return is_payment_done;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }

        public String getPost_for() {
            return post_for;
        }

        public void setPost_for(String post_for) {
            this.post_for = post_for;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(int sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }


        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getImage() {
            return image;
        }

        public List<String> getEmail() {
            return email;
        }


        public List<String> getWebsite() {
            return website;
        }

        public void setEmail(ArrayList<String> email) {
            this.email = email;
        }

        public void setWebsite(ArrayList<String> website) {
            this.website = website;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getOpening_hours() {
            return opening_hours;
        }

        public void setOpening_hours(Object opening_hours) {
            this.opening_hours = opening_hours;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLog() {
            return log;
        }

        public void setLog(double log) {
            this.log = log;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getTotal_review() {
            return total_review;
        }

        public void setTotal_review(int total_review) {
            this.total_review = total_review;
        }

        public double getAvg_rating() {
            return avg_rating;
        }

        public void setAvg_rating(double avg_rating) {
            this.avg_rating = avg_rating;
        }

        public CategoryBean getCategory() {
            return category;
        }

        public void setCategory(CategoryBean category) {
            this.category = category;
        }

        public List<String> getPhone() {
            return phone;
        }


        public List<PostGalleryBean> getPostGallery() {
            return postGallery;
        }


        public static class CategoryBean {
            /**
             * id : 1
             * name : Internet Access
             * image : null
             * type : individuals
             * status : 1
             * popular : 0
             * deleted_at : null
             * created_at : null
             * updated_at : 2018-01-17 10:07:52
             */

            private int id;
            private String name;
            private Object image;
            private String type;
            private String status;
            private String popular;
            private Object deleted_at;
            private Object created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getImage() {
                return image;
            }

            public void setImage(Object image) {
                this.image = image;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getPopular() {
                return popular;
            }

            public void setPopular(String popular) {
                this.popular = popular;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public Object getCreated_at() {
                return created_at;
            }

            public void setCreated_at(Object created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class PostGalleryBean {
            /**
             * id : 7
             * post_id : 12
             * image : http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1515664069247.png
             * deleted_at : null
             * created_at : 2018-01-11 09:47:49
             * updated_at : 2018-01-11 09:47:49
             */

            private int id;
            private int post_id;
            private String image;
            private Object deleted_at;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getPost_id() {
                return post_id;
            }

            public void setPost_id(int post_id) {
                this.post_id = post_id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
