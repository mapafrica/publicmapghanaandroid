package com.mapghana.app.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.GetDetails;
import com.mapghana.app.ui.sidemenu.Event.DialogCallback;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    private static int EARTH_RADIUS = 6371;

    public static String formateddate(String date) {

        String convertedDate = convertedDate(date);
        DateTime dateTime = DateTimeFormat.forPattern("dd/MM/yyyy").parseDateTime(convertedDate);
        DateTime today = new DateTime();
        DateTime yesterday = today.minusDays(1);
        DateTime twodaysago = today.minusDays(2);
        DateTime tomorrow = today.minusDays(-1);

        if (dateTime.toLocalDate().equals(today.toLocalDate())) {
            return "Today ";
        } else if (dateTime.toLocalDate().equals(yesterday.toLocalDate())) {
            return "Yesterday ";
        } else if (dateTime.toLocalDate().equals(twodaysago.toLocalDate())) {
            return convertedDate;
        } else if (dateTime.toLocalDate().equals(tomorrow.toLocalDate())) {
            return "Tomorrow ";
        } else {
            return convertedDate;
        }
    }

    public static String getddMMYYYY(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String eventDate = format.format(date);

        return eventDate;
    }

    public static String convertedDate(String date) {

        String[] str = date.split("-");
        return str[2] + "/" + str[1] + "/" + str[0];
    }

    public static void showToast(Context context, View view, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static String getDay(String date) {

        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format2 = new SimpleDateFormat("EEEE");
        String finalDay = format2.format(dt1);
        return finalDay;
    }

    public static String getDayTypeFormatter(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("E, dd MMMM yyyy");
        Date date = null;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(dateString);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);

        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        return output;
    }


    public static String getTimeStandardFormat(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("HH:mm a");
        Date date = null;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(dateString);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);

        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        return output;
    }

    public static String getddmmyyyyHygn(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(dateString);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);

        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        return output;
    }


    public static String getYYYYMMDDHHSS(String dateTime) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(dateTime);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);

        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        return output;
    }

    public static void saveImage(Bitmap finalBitmap, EventDetail item) {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/saved_images");
        Log.i("Directory", "==" + myDir);
        myDir.mkdirs();

        String fname = item.getName() + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveQrImage(Bitmap finalBitmap, GetDetails item) {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/saved_images");
        Log.i("Directory", "==" + myDir);
        myDir.mkdirs();

        String fname = item.getData().getName() + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void send(Context context, EventDetail item) {
        try {
            File myFile = new File("/storage/emulated/0/saved_images/" + item.getName() + ".jpg");
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String ext = myFile.getName().substring(myFile.getName().lastIndexOf(".") + 1);
            String type = mime.getMimeTypeFromExtension(ext);
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            sharingIntent.setType(type);
            sharingIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(myFile));
            context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public static void sendQRPost(Context context, GetDetails item) {
        try {
            File myFile = new File("/storage/emulated/0/saved_images/" + item.getData() + ".jpg");
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String ext = myFile.getName().substring(myFile.getName().lastIndexOf(".") + 1);
            String type = mime.getMimeTypeFromExtension(ext);
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            sharingIntent.setType(type);
            sharingIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(myFile));
            context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /*  Open chat link*/
    public static void openChatLink(String url, Context context) {

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, " You don't have any app to open link", Toast.LENGTH_LONG).show();
        }
    }

    /*open twitter*/
    public static void openTwitter(String value, Context context) {

        if (value.startsWith("@")) {
            value = value.substring(1);
        }
        try {
            context.getPackageManager().getPermissionInfo("com.twitter.android", 0);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setClassName("com.twitter.android", "com.twitter.android.ProfileActivity");
            intent.putExtra("screen_name", value);
            context.startActivity(intent);
        } catch (Exception e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + value)));
        }
    }

    /*open website*/
    public static void openWebsite(String url, Context context, View view) {
        try {
            if (url.length() != 0) {
                String domain = "http://";
                if (!url.startsWith(domain) && !url.startsWith("https://")) {
                    url = domain + url;
                }
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(url));
                context.startActivity(Intent.createChooser(webIntent, "Choose Browser:"));
            }
        } catch (ActivityNotFoundException e) {
            showToast(context, view, "No apps found to open this link in your phone");
        }
    }

    public static void onSendMail(String address, Context context) {
        if (!TextUtils.isEmpty(address)) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
            emailIntent.setType("message/rfc822");
            context.startActivity(Intent.createChooser(emailIntent, "Choose an email client:"));
        }

    }

    public static void openWhatsApp(String phone, Context context) {
        if (!TextUtils.isEmpty(phone)) {
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);

            try {
                String url = "https://api.whatsapp.com/send?phone=" + phone;
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                if (i.resolveActivity(packageManager) != null) {
                    context.startActivity(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /*dial number*/
    public static void startDialing(String phone, Context context) {
        if (phone == null || phone.length() == 0) return;
        String phoneNumber = "";
        if (phone.startsWith("0")) {
            phoneNumber = phone;
        } else {
            phoneNumber = "0" + phone;
        }
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        context.startActivity(callIntent);
    }

    public static void openDialog(AppBaseActivity activity, String title, String message, String type, DialogCallback listener) {


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(title).setMessage(message)
                .setNegativeButton("CANCEL", (arg0, arg1) -> {
                    listener.onCancel(type);
                })
                .setPositiveButton("CONTINUE", (arg0, arg1) -> {
                    listener.onSave(type);
                });
        Dialog dialog = builder.create();
        dialog.show();
    }

}
