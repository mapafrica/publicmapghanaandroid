package com.mapghana.app.interfaces;

import com.mapghana.app.model.GlobeSearch;

public interface SearchSingleItemClick {
    void onClick(GlobeSearch.DataBean item);
}
