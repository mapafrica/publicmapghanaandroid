package com.mapghana.app.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mapghana.R;
import com.mapghana.app.model.GroupGridSectionDataModel;
import com.mapghana.app.model.SectionDataModel;
import com.mapghana.app.ui.sidemenu.Event.EventListingFragment;
import com.mapghana.app.ui.sidemenu.Group.GroupListing;
import com.mapghana.app.utils.Utils;

import java.util.ArrayList;

public class GroupGridListingAdapter extends RecyclerView.Adapter<GroupGridListingAdapter.ItemRowHolder> {

    private ArrayList<GroupGridSectionDataModel> mList;
    private Context mContext;
    private GroupListing mListener;

    public GroupGridListingAdapter(Context context, ArrayList<GroupGridSectionDataModel> dataList, GroupListing listener) {
        this.mList = dataList;
        this.mContext = context;
        this.mListener = listener;
    }

    public void setDataList(ArrayList<GroupGridSectionDataModel> dataList) {
        this.mList = dataList;
        notifyDataSetChanged();
    }
    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_event_listing_horizontal_view, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String sectionName = mList.get(i).getHeaderTitle();
        ArrayList singleSectionItems = mList.get(i).getAllItemsInSection();

        itemRowHolder.itemTitle.setText(sectionName);

        GroupGridSectionListAdapter itemListDataAdapter = new GroupGridSectionListAdapter(mListener, singleSectionItems);
        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);

    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;

        protected RecyclerView recycler_view_list;

        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle =  view.findViewById(R.id.event_header_title);
            this.recycler_view_list = view.findViewById(R.id.rv_item_event_horizontal);

        }

    }

}