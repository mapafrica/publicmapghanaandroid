package com.mapghana.app.ui.activity.mainactivity.fragments;


import android.support.v4.app.Fragment;
import android.view.View;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewPasswordFragment extends AppBaseFragment {



    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_new_password;
    }

    @Override
    public void initializeComponent() {
        TypefaceTextView tvSubmit;

        getToolBar().setToolbarVisibilityTB(true);
        getToolBar().setTitleTextTB(getResources().getString(R.string.New_Password));
        tvSubmit=(TypefaceTextView)getView().findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSubmit:
                clearBackStack(0);
                break;
        }
    }
}
