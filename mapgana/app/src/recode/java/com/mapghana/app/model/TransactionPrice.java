package com.mapghana.app.model;

import java.io.Serializable;

public class TransactionPrice implements Serializable {

    private int status;
    private String message;
    private EventPrice event;
    private GroupPrice group;
    private IndividualPrice individual;
    private OrganisationPrice organisation;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EventPrice getEvent() {
        return event;
    }

    public void setEvent(EventPrice event) {
        this.event = event;
    }

    public GroupPrice getGroup() {
        return group;
    }

    public void setGroup(GroupPrice group) {
        this.group = group;
    }

    public IndividualPrice getIndividual() {
        return individual;
    }

    public void setIndividual(IndividualPrice individual) {
        this.individual = individual;
    }

    public OrganisationPrice getOrganisation() {
        return organisation;
    }

    public void setOrganisation(OrganisationPrice organisation) {
        this.organisation = organisation;
    }
}
