package com.mapghana.app.model;

import java.io.Serializable;
import java.util.List;

public class GroupListData implements Serializable {

    private int current_page;
    private List<GroupData> data;
    private int from;
    private int last_page;
    private String letter;


    public int getCurrent_page() {
        return current_page;
    }

    public List<GroupData> getData() {
        return data;
    }

    public int getFrom() {
        return from;
    }

    public int getLast_page() {
        return last_page;
    }

    public String getLetter() {
        return letter;
    }
}
