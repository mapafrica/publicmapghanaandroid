package com.mapghana.app.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.mapghana.R;
import com.mapghana.base.BaseDialog;

/**
 * Created by ubuntu on 24/2/18.
 */

public class ShowStatusDialog extends BaseDialog {

    public ShowStatusDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResourceView() {
        return R.layout.dialog_show_status;
    }

    @Override
    protected void initializeComponent() {
      /*  TypefaceTextView tv_status_descrption=(TypefaceTextView)this.findViewById(R.id.tv_status_descrption);
        TypefaceTextView tv_ok=(TypefaceTextView)this.findViewById(R.id.tv_ok);
        tv_status_descrption.setText(message);
        tv_ok.setOnClickListener(this);*/

        ImageView iv_close=(ImageView)this.findViewById(R.id.iv_close);
        iv_close.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
