package com.mapghana.app.ui.activity.dashboard.dashboard.search;


import android.animation.Animator;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.interfaces.SearchSingleItemClick;
import com.mapghana.app.model.City;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.model.PopularIndividual;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.spf.SessionManager;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.SearchListFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.adapter.MyAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.app.ui.navigationView.MapViewDetailFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.adapter.CityAdapter;
import com.mapghana.app.utils.Constants;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends AppBaseFragment implements SearchSingleItemClick, AdapterView.OnItemSelectedListener {

    private final int TIME_INTERVAL = 500;
    CityAdapter cityAdapter;
    private Animator animator;
    private RecyclerView recycler_view;
    private AutoCompleteTextView autoTv;
    private TextView tv_keyword;
    private Handler handler;
    private Runnable runnable;
    private List<GlobeSearch.DataBean> globalSearchList = new ArrayList();
    private String results = "";
    private int TYPE;
    private MyAdapter itemListOrgAdapter;
    private ProgressBar progressBar;
    private Call<ResponseBody> previousCall;
    private int currentPage = 0;
    private int totalPages = 20000;
    private boolean loadingNextData = false;
    private CardView cv_no_record_found;
    private String type;
    private String city_id = "";
    private Spinner spLoc;
    private ArrayList<City.DataBean> cityList;
    private String category_id = "", keyword = "";
    private boolean fromHome = false;
    private boolean isPopularClick;
    private ImageView imgSearch;

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (previousCall != null) {
                previousCall.cancel();
            }

            TYPE = 0;
            updateViewVisibitity(cv_no_record_found, View.GONE);
            setLoadingNextData(false);
            if (autoTv.getText().toString().trim().length() > 0) {
                waitForSometime();
            } else {
                currentPage = 0;
                clearList();
                notifyAdapter();
            }
        }
    };

    public void setType(String type) {
        this.type = type;

    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public void setFromHome(boolean fromHome) {
        this.fromHome = fromHome;
    }

    public void setPopularDataClick(boolean isPopularClick) {
        this.isPopularClick = isPopularClick;
    }



    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_search;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        autoTv.postDelayed(() -> {

            autoTv.addTextChangedListener(textWatcher);
            if (type != null) {
                if (type != null && type.length() > 0) {
                    autoTv.setText(type);
                }
            }
        }, 200);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        childFm = getChildFragmentManager();
        if (getView() == null) {
            setupFragmentViewByResource(inflater, container);
            initializeComponent();

        }
        return getView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        autoTv.removeTextChangedListener(textWatcher);
    }

    @Override
    public void initializeComponent() {
        getNavHandler().setNavigationToolbarVisibilty(false);


        ImageButton imgBack;
        imgSearch = getView().findViewById(R.id.imgSearch);
        imgBack = getView().findViewById(R.id.imgBack);
        recycler_view = getView().findViewById(R.id.recycler_view);
        autoTv = getView().findViewById(R.id.autoTv);
        progressBar = getView().findViewById(R.id.progressBar);
        cv_no_record_found = getView().findViewById(R.id.cv_no_record_found);
        spLoc = getView().findViewById(R.id.spLoc);
        tv_keyword = getView().findViewById(R.id.tv_keyword);
        cityList = new ArrayList<>();

        cv_no_record_found.setVisibility(View.GONE);
        progressBar.setVisibility(View.INVISIBLE);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);

        setUpRecyclerView();
        handler = new Handler();
        runnable = () -> {
            currentPage = 0;
            setupLayout();
        };
        autoTv.setThreshold(1);


        spLoc.setOnItemSelectedListener(this);
        imgSearch.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        setUpData();

        // getCity();

    }

    private void setUpRecyclerView() {
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        itemListOrgAdapter = new MyAdapter(getActivity(), this, globalSearchList);
        recycler_view.setAdapter(itemListOrgAdapter);

        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int visibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                if (!loadingNextData && totalItemCount <= visibleItem + 3) {
                    TYPE = 0;
                    setupLayout();
                }
            }
        });
    }


    private void setUpData() {
        tv_keyword.setVisibility(View.GONE);
        autoTv.setVisibility(View.VISIBLE);


        if (fromHome) {
            tv_keyword.setVisibility(View.VISIBLE);
            autoTv.setVisibility(View.GONE);

            try {
                Gson gson = new Gson();
                GlobeSearch data = gson.fromJson(results, GlobeSearch.class);
                if (data.getStatus() != 0) {
                    List<GlobeSearch.DataBean> data1 = data.getSearchData().getData();
                    if (data1 == null) {
                        totalPages = currentPage;
                        return;
                    }
                    if (data1.size() > 0) {
                        // notifyAdapter();
                        GlobeSearch.DataBean.CategoryBean category = data1.get(0).getCategory();
                        if (category != null) {
                            // category_id = String.valueOf(category.getId());
                            if (keyword != null && !keyword.isEmpty()) {
                                tv_keyword.setText(keyword);
                            } else {
                                tv_keyword.setText(category.getName());
                            }
                        }
                        if (currentPage == 1) {
                            addData(data1);
                        } else {
                            updateData(data1);
                        }
                        if (TYPE == 1) {
                            onSearch();
                        }
                    } else {
                        if (currentPage == 1) {
                            clearList();
                            itemListOrgAdapter.notifyDataSetChanged();
                        }

                        totalPages = currentPage;
                        updateNoDataView();
                        return;
                        // displayToast("No result found...");
                    }

                } else {
                    currentPage--;
                    displayToast(data.getError());
                }

            } catch (Exception e) {
            }
        } else if(isPopularClick){
             imgSearch.setVisibility(View.GONE);
            autoTv.setVisibility(View.GONE);
            tv_keyword.setVisibility(View.VISIBLE);
            try {
                Gson gson = new Gson();
                PopularIndividual data = gson.fromJson(results, PopularIndividual.class);
                if (data.getStatus() != 0) {
                    List<GlobeSearch.DataBean> data1 = data.getData();
                    if (data1.size() > 0) {
                        addData(data1);

                    }

                }

            } catch (Exception e) {
            }

        }
    }

    private void getCity() {
        try {
            ArrayList<City.DataBean> myCityList = getApplication().getCityList();
            if (myCityList != null && myCityList.size() > 0) {
                cityList.addAll(myCityList);
                cityAdapter = new CityAdapter(getContext(), cityList);
                cityAdapter.setDataBean(0);
                cityAdapter.setTextColor(R.color.colorWhite);
                cityAdapter.setViewBackgroundColor(R.color.colorDarkGray);
                spLoc.setAdapter(cityAdapter);
                setSelectedCity();
            } else {
                if (ConnectionDetector.isNetAvail(getActivity())) {
                    RestClient restClient = new RestClient(getContext());
                    displayProgressBar(false);
                    restClient.callback(this).city();
                } else {
                    displayToast(Constants.No_Internet);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    private void doRequest() {
        if (getContext() == null) {
            return;
        }

        if (tv_keyword.getText().toString().toLowerCase().length() > 0 ||
                autoTv.getText() != null && autoTv.getText().toString().trim().length() != 0) {
            if (ConnectionDetector.isNetAvail(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                setLoadingNextData(true);
                RestClient restClient = new RestClient(getContext());
                String keyword = autoTv.getText().toString().trim();
                if (tv_keyword.getText().toString().trim().length() > 0) {
                    keyword = tv_keyword.getText().toString().trim();
                }
                previousCall = restClient.callback(this).searchFilter(keyword, category_id, city_id, "", "", "",
                        String.valueOf(currentPage));
            } else {
                progressBar.setVisibility(View.GONE);
                currentPage--;
                setLoadingNextData(false);
                displayToast(Constants.No_Internet);
            }
        } else {
            progressBar.setVisibility(View.GONE);
            currentPage--;

        }
    }

    public void setupLayout() {
        if (currentPage == 0) {
            currentPage = 1;
            doRequest();
            return;
        }
        if (totalPages > currentPage) {
            currentPage = currentPage + 1;
            doRequest();
        }
    }

    private void setLoadingNextData(boolean isLoading) {
        loadingNextData = isLoading;
    }

    private void waitForSometime() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, TIME_INTERVAL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                hideKeyboard();
                getActivity().onBackPressed();
                break;
            case R.id.imgSearch:
                hideKeyboard();
                if (tv_keyword.getText().toString().trim().length() > 0
                        || autoTv.getText().toString().trim().length() > 0) {
                    onSearch();
                }
                break;
        }
    }

    private void onSearch() {
        TYPE = 1;
        if (results.trim().length() > 0) {
            // clear old default filters
            DefaulFilterSpf.clearFilter(getContext());
            SearchListFragment itemFragment = new SearchListFragment();
            String text = autoTv.getText().toString().trim();
            if (tv_keyword.getText().toString().trim().length() > 0) {
                text = tv_keyword.getText().toString().trim();
            }
            itemFragment.setSearch_keyword(text);
            itemFragment.setSearch_response(results);
            progressBar.setVisibility(View.INVISIBLE);
            try {
                getDashboardActivity().changeFragment(itemFragment, true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                return;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        } else {
            handler.removeCallbacks(runnable);
            handler.post(runnable);
        }
    }

    @Override
    public void viewCreateFromBackStack() {
        super.viewCreateFromBackStack();
        getNavHandler().setNavigationToolbarVisibilty(false);


    }

    private void popPasswordCheck(GlobeSearch.DataBean item) {
        final AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity()).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dialog_password_check, null);

        final EditText editText = dialogView.findViewById(R.id.edt_comment);
        Button button1 = dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = dialogView.findViewById(R.id.buttonCancel);

        button2.setOnClickListener(view -> dialogBuilder.dismiss());
        button1.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(editText.getText().toString().trim())) {
                if (editText.getText().toString().trim().equals(item.getPassword())) {
                    navigateToMapViewScreen(String.valueOf(item.getId()));
                    dialogBuilder.dismiss();
                }
            } else {
                displayToast("Please enter password to unlock post");
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();
    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        progressBar.setVisibility(View.INVISIBLE);
        setLoadingNextData(false);

        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_SEarch_Filter) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GlobeSearch data = gson.fromJson(s, GlobeSearch.class);
                    if (data.getStatus() != 0) {
                        List<GlobeSearch.DataBean> data1 = data.getSearchData().getData();
                        if (data1 == null) {
                            totalPages = currentPage;
                            return;
                        }
                        if (data1.size() > 0) {
                            results = s;
                            // notifyAdapter();
                            if (currentPage == 1) {
                                addData(data1);
                            } else {
                                updateData(data1);
                            }
                            if (TYPE == 1) {
                                onSearch();
                            }
                        } else {
                            if (currentPage == 1) {
                                clearList();
                                itemListOrgAdapter.notifyDataSetChanged();
                            }

                            totalPages = currentPage;
                            updateNoDataView();
                            return;
                            // displayToast("No result found...");
                        }

                    } else {
                        currentPage--;
                        displayToast(data.getError());
                    }

                } catch (IOException e) {
                    currentPage--;
                    e.printStackTrace();
                    displayToast(e.getMessage());
                }
            } else if (apiId == ApiIds.ID_city) {
                try {
                    cityList.clear();
                    String s = response.body().string();
                    Gson gson = new Gson();
                    City city = gson.fromJson(s, City.class);
                    if (city.getStatus() != 0) {

                      /*  City.DataBean city1 = new City.DataBean();
                        city1.setName("Select City");
                        cityList.add(0, city1);*/
                        cityList.addAll(city.getData());
                        try {
                            getApplication().setCityList(cityList);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        cityAdapter = new CityAdapter(getContext(), cityList);
                        cityAdapter.setTextColor(R.color.colorWhite);
                        cityAdapter.setViewBackgroundColor(R.color.colorDarkGray);
                        cityAdapter.setDataBean(0);
                        spLoc.setAdapter(cityAdapter);
                        setSelectedCity();
                    } else {
                        displayErrorDialog("Error", city.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            currentPage--;
            displayToast(response.message());
        }
    }


    @Override
    public void onFailResponse(int apiId, String error) {
        currentPage--;
        progressBar.setVisibility(View.INVISIBLE);
        // displayToast( error);
    }

    private void setSelectedCity() {
        if (cityList != null && cityList.size() > 0) {
            String id = SessionManager.getSelectedCityId(getContext());
            if (id != null && !id.isEmpty()) {
                int my_sel_id = Integer.parseInt(id);
                for (int i = 0; i < cityList.size(); i++) {
                    if (cityList.get(i).getId() == my_sel_id) {
                        spLoc.setSelection(i);
                        cityAdapter.setDataBean(i);
                        break;
                    }
                }
            }
        }
    }

    private void addData(List<GlobeSearch.DataBean> modelList) {
        globalSearchList.clear();
        if (modelList != null) {
            globalSearchList.addAll(modelList);
        }
        if (itemListOrgAdapter != null) {
            itemListOrgAdapter.notifyDataSetChanged();
            updateNoDataView();
        }
    }

    private void updateData(List<GlobeSearch.DataBean> modelList) {
        int previousSize = globalSearchList.size();
        if (modelList != null) {
            globalSearchList.addAll(modelList);
        }
        int currentSize = globalSearchList.size();
        if (itemListOrgAdapter != null && previousSize < currentSize) {
            itemListOrgAdapter.notifyItemRangeInserted(previousSize, currentSize);
            updateNoDataView();
        }
    }

    private void updateNoDataView() {
        if (globalSearchList.size() > 0) {
            updateViewVisibitity(cv_no_record_found, View.GONE);
        } else {
            updateViewVisibitity(cv_no_record_found, View.VISIBLE);
        }
    }

    private void clearList() {
        globalSearchList.clear();
        results = "";
    }

    private void notifyAdapter() {

        itemListOrgAdapter.notifyDataSetChanged();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
     /*   if (cityAdapter == null || cityList == null || cityList.size() == 0) return;
        cityAdapter.setDataBean(position);
        if (!cityList.get(position).getName().equals(Constants.Select_City)) {
            city_id = String.valueOf(cityList.get(position).getId());
        } else {
            city_id = "";
        }
        SessionManager.saveSelectedCityId(SearchFragment.this.getActivity(), city_id);

        handler.post(runnable);*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(GlobeSearch.DataBean item) {
        if (globalSearchList != null && globalSearchList.size() > 0) {
            hideKeyboard();
            if ("Y".equalsIgnoreCase(item.getPassword_protected())) {
                popPasswordCheck(item);
            } else {
                navigateToMapViewScreen(String.valueOf(item.getId()));
            }

        }
    }

    private void navigateToMapViewScreen(String id) {
        try {
            getDashboardActivity().changeFragment(MapViewDetailFragment.newInstance(Constants.SEARCH, id, Constants.SEARCH), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim,
                    true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
