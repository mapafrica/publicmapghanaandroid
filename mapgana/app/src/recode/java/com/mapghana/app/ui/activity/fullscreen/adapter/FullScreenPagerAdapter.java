package com.mapghana.app.ui.activity.fullscreen.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.mapghana.R;
import com.mapghana.app.model.GalleryPojo;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.TouchImageView;
import com.mapghana.util.file_path_handler.FilePathHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by komal yogi
 */
public class FullScreenPagerAdapter extends PagerAdapter {


    private List<GalleryPojo> image_list;
    private LayoutInflater layoutInflater;
    private Context context;
    private List<String> image_mime_List;
    private List<String> video_mime_List;
    private int position;
    private int play_position;
    private boolean isFirstTime = false;
    private MediaController mediaController;

    public FullScreenPagerAdapter(List<GalleryPojo> image_list,
                                  Context context, int position, int play_position) {
        this.image_list = image_list;
        this.context = context;
        this.position = position;
        this.play_position = play_position;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        image_mime_List = new ArrayList<>();
        video_mime_List = new ArrayList<>();
        image_mime_List.addAll(Constants.getImageMimeList());
        video_mime_List.addAll(Constants.getVideoMimeList());
    }

    @Override
    public int getCount() {
        return image_list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View viewLayout = layoutInflater.inflate(R.layout.item_image_slider, container, false);
        TouchImageView imageDisplay = (TouchImageView) viewLayout.findViewById(R.id.touchImageView);
        final VideoView video_view = (VideoView) viewLayout.findViewById(R.id.video_view);
        final ImageButton ib_start = (ImageButton) viewLayout.findViewById(R.id.ib_start);
        final RelativeLayout rl_main_view = (RelativeLayout) viewLayout.findViewById(R.id.rl_main_view);
        final RelativeLayout rl_view = (RelativeLayout) viewLayout.findViewById(R.id.rl_view);
        final ProgressBar progressBar = (ProgressBar) viewLayout.findViewById(R.id.progressBar);

        FilePathHelper filePathHelper = new FilePathHelper();
        String mimeType = filePathHelper.getMimeType(image_list.get(position).getImage());
        String mediaPath = image_list.get(position).getImage();

        if (image_mime_List.contains(mimeType)) {

            imageDisplay.setVisibility(View.VISIBLE);
            rl_view.setVisibility(View.INVISIBLE);
//            ((AppBaseActivity)context).loadImage(context, imageDisplay, null, mediaPath,
//                    R.drawable.pleasewait, R.drawable.pleasewait, R.drawable.pleasewait);

            Glide.with(context).load(mediaPath)
                    .override(500, 500).
                    placeholder(R.drawable.pleasewait).
                    dontAnimate().into(imageDisplay);
            if (mediaController != null) {
                mediaController.setVisibility(View.INVISIBLE);
            }

            ((ViewPager) container).addView(viewLayout);
            return viewLayout;
        } else if (video_mime_List.contains(mimeType)) {
            imageDisplay.setVisibility(View.INVISIBLE);
            rl_view.setVisibility(View.VISIBLE);
            video_view.setVideoPath(mediaPath);

            if (mediaController == null) {
                mediaController = new MediaController(context);
                video_view.setMediaController(mediaController);
                mediaController.setAnchorView(video_view);
            }

            rl_main_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (video_mime_List.contains(mimeType)) {
                        progressBar.setVisibility(View.INVISIBLE);
                        showMediaController(ib_start, progressBar, video_view);

                    }
                }
            });


            ib_start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.INVISIBLE);
                    showMediaController(ib_start, progressBar, video_view);
                }
            });

            if (!isFirstTime && this.position == position) {
                if (play_position != 0) {
                    isFirstTime = true;
                    video_view.seekTo(play_position);
                }
            }

            ((ViewPager) container).addView(viewLayout);
            return viewLayout;
        }
        ((ViewPager) container).addView(viewLayout);
        return viewLayout;

    }

    private void showMediaController(ImageButton ib_start, ProgressBar progressBar, VideoView video_view) {
        ib_start.setVisibility(View.INVISIBLE);
        mediaController.setVisibility(View.VISIBLE);
        mediaController.show();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}