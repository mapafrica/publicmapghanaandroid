package com.mapghana.app.model;

/**
 * Created by ubuntu on 25/1/18.
 */

public class SendReview {


    /**
     * status : 1
     * code : 200
     * data : {"post_id":"12","user_id":"3","title":"sdd","message":"xvdsvgdfdbdfbdfcbh","rating":"2.5","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1517042796758.jpg","updated_at":"2018-01-27 08:46:36","created_at":"2018-01-27 08:46:36","id":15}
     * message : successfully
     * error :
     */

    private int status;
    private int code;
    private DataBean data;
    private String message;
    private String error;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static class DataBean {
        /**
         * post_id : 12
         * user_id : 3
         * title : sdd
         * message : xvdsvgdfdbdfbdfcbh
         * rating : 2.5
         * image : http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1517042796758.jpg
         * updated_at : 2018-01-27 08:46:36
         * created_at : 2018-01-27 08:46:36
         * id : 15
         */

        private String post_id;
        private String user_id;
        private String title;
        private String message;
        private String rating;
        private String image;
        private String updated_at;
        private String created_at;
        private int id;

        public String getPost_id() {
            return post_id;
        }

        public void setPost_id(String post_id) {
            this.post_id = post_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
