package com.mapghana.app.ui.sidemenu.Event;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapghana.R;
import com.mapghana.app.adapter.EventMediaListAdapter;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.app_base.AppBaseMapBox;
import com.mapghana.app.dialog.PopQRCodeFullScreen;
import com.mapghana.app.dialog.PopUpMediaGallery;
import com.mapghana.app.interfaces.MediaClickHandler;
import com.mapghana.app.interfaces.QRcodeSizeListener;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.EventGallery;
import com.mapghana.app.model.EventInfoSection;
import com.mapghana.app.model.MapLocationMapModel;
import com.mapghana.app.model.PaymentResponse;
import com.mapghana.app.model.Rsvp;
import com.mapghana.app.model.Session;
import com.mapghana.app.paymentSection.MapghanaPayment;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.rest.BaseArguments;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.service.Locationservice;
import com.mapghana.app.ui.navigationView.MapViewFragment;
import com.mapghana.app.ui.navigationView.MapviewNavigation;
import com.mapghana.app.utils.AppPreferences;
import com.mapghana.app.utils.Constants;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.utils.Utils;
import com.mapghana.util.TouchableWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.view.View.GONE;

public class EventDetailFragment extends AppBaseFragment implements OnMapReadyCallback, PermissionsListener, EventListCallback, QRcodeSizeListener {

    private String mEventType = "multiEvent";
    private String mTitle;
    private int zoomLevel;
    private EventDetail eventDetail;
    private RelativeLayout mapContainer;
    private ImageView posterImage;
    private List<LatLng> boundList;
    private Marker marker;
    private TouchableWrapper my_view;
    private PermissionsManager permissionsManager;
    public LocationComponent locationComponent;
    private TextView mapNavigate, mapNavigation, gateKeepingIndication;
    private PopQRCodeFullScreen dialog;
    private TextView ticket_price;
    private TextView bookTicket;
    private TextView rsvp;
    private ImageView QRCode;
    private ImageView QrShare;
    private ImageView mJoinMemberOne;
    private ImageView mJoinMemberTwo;
    private ImageView mJoinMemberThree;
    private TextView mNumberOfEventMember;
    private TextView mAboutEvent;
    private TextView mScrollEventPreviousMedia;
    private RecyclerView mRvPreviousMedia;
    private AppBaseMapBox.CustomSupportMapFragment mapFragment;
    private TextView mContactEventInfo;
    private RelativeLayout rlMedia;
    private ImageButton imgMyLoc;
    private FrameLayout flPeopleJoined;
    private MapView mapView;
    private MapboxMap mapboxMap;
    private PopUpMediaGallery customDialog;
    private LinearLayout eventDetailSection;
    private ScrollView scrollView;
    private LottieAnimationView bar, Qr_scanner;
    private String id;
    private SymbolManager symbolManager;
    private boolean isMapReady;
    private RestClient restClient;
    private String screenType, paymentType;
    private Style style;

    public static EventDetailFragment newInstance(String title, String id) {
        EventDetailFragment fragment = new EventDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constants.TITLE, title);
        args.putString(Constants.id, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_api_token));
        if (getArguments() != null) {
            mTitle = getArguments().getString(Constants.TITLE);
            id = getArguments().getString(Constants.id);
        }

    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_add_event_detail_page;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initViewById(View rootView) {
        restClient = new RestClient(getActivity());
        scrollView = rootView.findViewById(R.id.scroll_view);
        bar = rootView.findViewById(R.id.progress_bar);
        Qr_scanner = rootView.findViewById(R.id.Qr_scanner);
        mapView = rootView.findViewById(R.id.mapEventView);
        posterImage = rootView.findViewById(R.id.event_poster_image);
        gateKeepingIndication = rootView.findViewById(R.id.event_gatekeeping_indicator);
        mapNavigate = rootView.findViewById(R.id.map_navigate);
        mapNavigation = rootView.findViewById(R.id.map_navigation);
        ticket_price = rootView.findViewById(R.id.ticket_price);
        bookTicket = rootView.findViewById(R.id.book_ticket);
        rsvp = rootView.findViewById(R.id.rsvp);
        eventDetailSection = rootView.findViewById(R.id.layout_event_info_section);
        QRCode = rootView.findViewById(R.id.qr_code);
        QrShare = rootView.findViewById(R.id.share_qr);
        mJoinMemberOne = rootView.findViewById(R.id.join_people_one);
        mJoinMemberTwo = rootView.findViewById(R.id.join_people_two);
        mJoinMemberThree = rootView.findViewById(R.id.join_people_three);
        mNumberOfEventMember = rootView.findViewById(R.id.event_member_join);
        mAboutEvent = rootView.findViewById(R.id.about_event);
        mScrollEventPreviousMedia = rootView.findViewById(R.id.scroll_event_previous_media);
        mRvPreviousMedia = rootView.findViewById(R.id.rv_previous_media);
        mContactEventInfo = rootView.findViewById(R.id.contact_event_info);
        rlMedia = rootView.findViewById(R.id.rl_header_media);
        flPeopleJoined = rootView.findViewById(R.id.fl_people_joined);
        boundList = new ArrayList<>();

        rsvp.setOnClickListener(v -> {
            displayProgressBar(false);
            Session session = AppPreferences.getSession();
            if (!TextUtils.isEmpty(session.getUsername())) {
                restClient.callback(this).postRsvp(session.getId(), eventDetail.getId());
            } else {
                try {
                    getDashboardActivity().showLoginMessageDialog();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }


        });
        displayProgressBar(false);
        restClient.callback(this).getEventDetailById(id);

        // Register the local broadcast
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                paymentReceiver,
                new IntentFilter(Constants.LOCAL_BROADCAST_PAYMENT)
        );
    }

    // Initialize a new BroadcastReceiver instance
    private BroadcastReceiver paymentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            screenType = intent.getStringExtra("screen_type");
            paymentType = intent.getStringExtra("payment_type");

            if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_SUCCESS)) {

                if ("map_navigation".equalsIgnoreCase(paymentType) && "event_detail".equalsIgnoreCase(screenType)) {
                    eventDetail.setMapNavigation(true);
                } else if ("book_ticket".equalsIgnoreCase(paymentType) && "event_detail".equalsIgnoreCase(screenType)) {
                    bookTicket.setVisibility(GONE);
                    eventDetail.setTicketBooked(true);
                }

                Gson gson = new Gson();
                PaymentResponse paymentResponse = gson.fromJson(intent.getStringExtra(Constants.data), PaymentResponse.class);
                callPaymentToServer(paymentResponse);

            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_ERROR)) {

                if ("map_navigation".equalsIgnoreCase(paymentType) && "event_detail".equalsIgnoreCase(screenType)) {
                    eventDetail.setMapNavigation(false);
                } else if ("book_ticket".equalsIgnoreCase(paymentType) && "event_detail".equalsIgnoreCase(screenType)) {
                    bookTicket.setVisibility(View.VISIBLE);
                    eventDetail.setTicketBooked(false);
                }


            } else if (intent.getStringExtra(Constants.type).equalsIgnoreCase(Constants.LOCAL_BROADCAST_PAYMENT_CANCELLED)) {
                if ("map_navigation".equalsIgnoreCase(paymentType) && "event_detail".equalsIgnoreCase(screenType)) {
                    eventDetail.setMapNavigation(false);
                } else if ("book_ticket".equalsIgnoreCase(paymentType) && "event_detail".equalsIgnoreCase(screenType)) {
                    bookTicket.setVisibility(View.VISIBLE);
                    eventDetail.setTicketBooked(false);
                }
            }
        }
    };

    private void callPaymentToServer(PaymentResponse paymentResponse) {
        Session session = AppPreferences.getSession();
        restClient.callback(this).transaction(String.valueOf(eventDetail.getId()), String.valueOf(paymentResponse.getData().getAmount()), paymentResponse.getData().getPaymentId(), session.getEmail(), session.getName(), session.getName(), screenType, paymentType);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }


    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        isMapReady = true;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            this.style = style;
            symbolManager = new SymbolManager(mapView, mapboxMap, style);
            symbolManager.setIconAllowOverlap(false);  //your choice t/f
            symbolManager.setTextAllowOverlap(false);  //yo
            enableLocationComponent(style);

            if (eventDetail != null) {
                setMapMarker();
            }

        });
    }

    private void setMapMarker() {
        if (isMapReady && mapboxMap != null && style != null && symbolManager != null) {

            if (eventDetail.getEvent_date_time().size() > 0) {
                String markerOption = eventDetail.getName() + "\nAddress: " + eventDetail.getEvent_date_time().get(0).getAddress();
                Bitmap bm = BitmapFactory.decodeResource(Objects.requireNonNull(getActivity()).getResources(), R.drawable.mapbox_marker_icon_default);
                if (style != null) {
                    style.addImage("marker-" + eventDetail.getId(), bm);
                }

                symbolManager.create(new SymbolOptions()
                        .withLatLng(new LatLng(Double.valueOf(eventDetail.getEvent_date_time().get(0).getLat()), Double.valueOf(eventDetail.getEvent_date_time().get(0).getLog())))
                        .withIconImage("marker-" + eventDetail.getId())
                        //set the below attributes according to your requirements
                        .withIconSize(0.5f)
                        .withIconOffset(new Float[]{0f, -1.5f})
                        .withZIndex(10)
                        .withTextField(markerOption)
                        .withTextHaloColor("rgba(255, 255, 255, 100)")
                        .withTextHaloWidth(5.0f)
                        .withTextAnchor("top")
                        .withTextOffset(new Float[]{0f, 1.5f})
                );
                mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.valueOf(eventDetail.getEvent_date_time().get(0).getLat()), Double.valueOf(eventDetail.getEvent_date_time().get(0).getLog()))));
            } else {
                Location location = Locationservice.sharedInstance().getLastLocation();
                if (location != null) {
                    Bitmap bm = BitmapFactory.decodeResource(Objects.requireNonNull(getActivity()).getResources(), R.drawable.event_marker);
                    mapboxMap.getStyle().addImage("marker-" + eventDetail.getId(), bm);
                    symbolManager.create(new SymbolOptions()
                            .withLatLng(new LatLng(location.getLatitude(), location.getLongitude()))
                            .withIconImage("marker-" + eventDetail.getId())
                            //set the below attributes according to your requirements
                            .withIconSize(0.5f)
                            .withIconOffset(new Float[]{0f, -1.5f})
                            .withZIndex(10)
                            .withTextHaloColor("rgba(255, 255, 255, 100)")
                            .withTextHaloWidth(5.0f)
                            .withTextAnchor("top")
                            .withTextOffset(new Float[]{0f, 1.5f})
                    );
                    mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                }

            }


        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initializeComponent() {
        initToolBar();
        initViewById(getView());

    }

    public void makeSpanableString(String link, Context context, TextView view) {
        SpannableString ss = new SpannableString(link);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                switch (textView.getId()) {
                    case R.id.contact_event_info:
                        Utils.startDialing(String.valueOf(eventDetail.getPhone()), getActivity());
                        break;
                }

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan, 14, link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        view.setText(ss);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setHighlightColor(Color.TRANSPARENT);
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(mTitle);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setExploreVisibility(false);
        getNavHandler().setCalenderVisibility(false);
        getNavHandler().setSearchVisibiltyInternal(false);
        getNavHandler().setimgMultiViewVisibility(false);
        getNavHandler().setAtoZZtoAVisibiltyInternal(false);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void inflateEventInfoView(ArrayList<EventInfoSection> list, Context context) {

        mapView.setVisibility(GONE);
        posterImage.setVisibility(GONE);
        if (eventDetail.getEvent_date_time().size() > 1) {

            posterImage.setVisibility(View.VISIBLE);
            Glide.clear(posterImage);
            Glide.with(MapGhanaApplication.sharedInstance())
                    .load(eventDetail.getImage())
                    .asBitmap()
                    .dontAnimate()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            bar.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            posterImage.setImageBitmap(resource);
                            bar.setVisibility(GONE);
                            return false;
                        }
                    })
                    .into(posterImage);

            mapNavigation.setVisibility(View.INVISIBLE);
            mapNavigate.setVisibility(View.INVISIBLE);

        } else {
            setMapMarker();
            posterImage.setVisibility(GONE);
            mapView.setVisibility(View.VISIBLE);
        }

        Session session = AppPreferences.getSession();
        restClient.callback(this).getRSVPEvent(BaseArguments.GET_RSVP_EVENT + "?user_id=" + session.getId() + "&event_id=" + eventDetail.getId());
        LayoutInflater inflater = LayoutInflater.from(context);

        for (EventInfoSection section : list) {

            View inflatedLayout = inflater.inflate(R.layout.layout_event_info_section, null, false);
            TextView eventDayDate = inflatedLayout.findViewById(R.id.event_date_day);
            TextView eventVenue = inflatedLayout.findViewById(R.id.event_venue);
            TextView eventStartEndTime = inflatedLayout.findViewById(R.id.event_start_end_time);
            ImageView mapIcon = inflatedLayout.findViewById(R.id.icon_map);
            TextView mapNavigate = inflatedLayout.findViewById(R.id.map_navigate);
            TextView mapNavigation = inflatedLayout.findViewById(R.id.map_navigation);
            String lat = section.getLat();
            String log = section.getLog();

            if (list.size() > 1) {
                mapNavigate.setVisibility(View.VISIBLE);
                mapNavigation.setVisibility(View.VISIBLE);
            } else {
                mapNavigate.setVisibility(View.GONE);
                mapNavigation.setVisibility(View.GONE);
            }
            mapNavigate.setOnClickListener(v -> {
                navigateMapView(Double.valueOf(lat), Double.valueOf(log));
            });

            mapNavigation.setOnClickListener(v -> {
                directionLocationRoute(new LatLng(Double.valueOf(lat), Double.valueOf(log)));
            });

            eventDayDate.setText(Utils.getDayTypeFormatter(section.getStart_date_time()));
            eventVenue.setText(section.getAddress());
            eventStartEndTime.setText(Utils.getTimeStandardFormat(section.getStart_date_time()) + " - " + Utils.getTimeStandardFormat(section.getEnd_date_time()));
            eventDetailSection.addView(inflatedLayout);
        }

        final Bitmap[] QrCodImage = {null};
        Glide.clear(QRCode);
        Glide.with(MapGhanaApplication.sharedInstance())
                .load(BaseArguments.QR_CODE_URL + "" + eventDetail.getId())
                .asBitmap()
                .dontAnimate()
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        Qr_scanner.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        QrCodImage[0] = resource;
                        Qr_scanner.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .into(QRCode);

        QRCode.setOnClickListener(view -> {
            dialog = new PopQRCodeFullScreen();
            dialog.setQrImage(BaseArguments.QR_CODE_URL + "" + eventDetail.getId(), String.valueOf(eventDetail.getId()));
            dialog.setListener(this);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            dialog.show(ft, "FullScreenDialog");
        });


        if (eventDetail.getGatekeeping_services().equalsIgnoreCase("Y")) {
            gateKeepingIndication.setVisibility(View.VISIBLE);
        } else {
            gateKeepingIndication.setVisibility(View.INVISIBLE);

        }
        if (eventDetail.getTicket_price() > 0) {
            bookTicket.setVisibility(View.VISIBLE);
        } else {
            bookTicket.setVisibility(View.GONE);
        }


        bookTicket.setOnClickListener(view -> {
            openMakePayemntScreen("event_detail", "book_ticket", 1000);
        });

        mapNavigate.setOnClickListener(view -> {
            navigateMapView(Double.valueOf(eventDetail.getEvent_date_time().get(0).getLat()), Double.valueOf(eventDetail.getEvent_date_time().get(0).getLog()));
        });

        mapNavigation.setOnClickListener(view -> {

            if (eventDetail.isMapNavigation()) {
                navigateNagivationMapView(eventDetail);
            } else {
                openMakePayemntScreen("event_detail", "map_navigation", 100);
            }

        });
        if (eventDetail.getTicket_price() > 0) {
            ticket_price.setVisibility(View.VISIBLE);
            ticket_price.setText("GHC " + eventDetail.getTicket_price());
        } else {
            ticket_price.setVisibility(View.INVISIBLE);
        }


        mAboutEvent.setText(eventDetail.getDetails());

        if (eventDetail.getEvent_gallery().size() == 0) {
            rlMedia.setVisibility(GONE);

        } else {
            rlMedia.setVisibility(View.VISIBLE);

        }

        if (eventDetail.getNo_of_seats() == 0) {
            mNumberOfEventMember.setVisibility(GONE);
            flPeopleJoined.setVisibility(GONE);
        } else {
            mNumberOfEventMember.setText(eventDetail.getNo_of_seats() + " people joined");
            flPeopleJoined.setVisibility(View.VISIBLE);
        }

        MediaClickHandler mediaClickHandler = gallery -> {
            popUpgallery(context, eventDetail.getEvent_gallery());
        };

        initRecycleLayoutManager(context, eventDetail, mediaClickHandler);

        QrShare.setOnClickListener(view -> {
            Utils.saveImage(QrCodImage[0], eventDetail);
            Utils.send(context, eventDetail);
        });


        if (eventDetail.getPhone() == 0) {
            mContactEventInfo.setVisibility(GONE);
        } else {
            makeSpanableString(String.format(getString(R.string.event_contact_info), String.valueOf(eventDetail.getPhone())), getActivity(), mContactEventInfo);
        }

    }

    private void openMakePayemntScreen(String screenType, String paymentType, int amount) {
        Intent intent = new Intent(getActivity(), MapghanaPayment.class);
        intent.putExtra("id", eventDetail.getId());
        intent.putExtra("screen_type", screenType);
        intent.putExtra("payment_type", paymentType);
        intent.putExtra("amount", amount);
        startActivity(intent);
    }

    private void startDialing(String phone) {
        if (phone == null || phone.length() == 0) return;

        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phone));
        startActivity(callIntent);
    }

    private void initRecycleLayoutManager(Context context, EventDetail item, MediaClickHandler listener) {

        mRvPreviousMedia.setHasFixedSize(true);
        EventMediaListAdapter adapter = new EventMediaListAdapter(getContext(), item.getEvent_gallery(), listener);

        mRvPreviousMedia.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mRvPreviousMedia.setAdapter(adapter);

    }

    private void popUpgallery(Context context, List<EventGallery> item) {
        customDialog = new PopUpMediaGallery(context, this, item, true, "eventDetail");
        customDialog.setCancelable(false);
        customDialog.show();
    }

    public void navigateNagivationMapView(EventDetail item) {
        Intent intent = new Intent(getActivity(), MapviewNavigation.class);
        MapLocationMapModel model = new MapLocationMapModel(item.getEvent_date_time().get(0).getLat(), item.getEvent_date_time().get(0).getLog(), null, "evetDetailFragment", false);
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", model);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void navigateMapView(double lat, double log) {
        MapViewFragment mapViewFragment = new MapViewFragment();
        mapViewFragment.setLocation(new LatLng(lat, log));
        ((AppBaseActivity) getActivity()).pushFragment(mapViewFragment, true);
    }

    public void directionLocationRoute(LatLng latLng) {
        MapViewFragment mapViewFragment = new MapViewFragment();
        mapViewFragment.setLocation(new LatLng(latLng.getLatitude(), latLng.getLongitude()));
        ((AppBaseActivity) getActivity()).pushFragment(mapViewFragment, true);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        super.onSuccessResponse(apiId, response);
        if (response.isSuccessful()) {
            dismissProgressBar();
            if (apiId == ApiIds.ID_RSVP_EVENT) {
                rsvp.setVisibility(GONE);
            } else if (apiId == ApiIds.ID_EVENT_DETAIL_BY_ID) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    eventDetail = gson.fromJson(s, EventDetail.class);
                    inflateEventInfoView(eventDetail.getEvent_date_time(), getActivity());
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            } else if (apiId == ApiIds.ID_GET_RSVP_EVENT) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    Rsvp rsvpItem = gson.fromJson(s, Rsvp.class);
                    if (rsvpItem.getStatus() == 1) {
                        rsvp.setVisibility(GONE);
                    } else {
                        rsvp.setVisibility(View.VISIBLE);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }

            } else if (apiId == ApiIds.ID_TRANSACTION) {
                Utils.showToast(getActivity(), getView(), "transaction successfully");
            }
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        super.onFailResponse(apiId, error);
        dismissProgressBar();
        if (apiId == ApiIds.ID_EVENT_DETAIL_BY_ID) {
            dismissProgressBar();
            Utils.showToast(getActivity(), getView(), "failure");
        } else if (apiId == ApiIds.ID_RSVP_EVENT) {
            dismissProgressBar();
            rsvp.setVisibility(View.VISIBLE);
            Utils.showToast(getActivity(), getView(), "failure");
        }

    }

    @Override
    public void onClickEvent(EventDetail model) {

    }

    @Override
    public void onPopUpClickEvent(EventDetail model) {

    }

    @Override
    public void onCloseIcon() {

    }

    @Override
    public void onClose() {
        dialog.dismiss();
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();

        }
    }
}
