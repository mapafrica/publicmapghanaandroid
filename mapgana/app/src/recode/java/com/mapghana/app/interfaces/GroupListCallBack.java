package com.mapghana.app.interfaces;

import com.mapghana.app.model.EventDetail;
import com.mapghana.app.model.GroupData;

public interface GroupListCallBack {
    void onClickEvent(GroupData model,String type);

    void onPopUpClickEvent(GroupData model);

    void onCloseIcon();
}

