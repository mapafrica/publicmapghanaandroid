package com.mapghana.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mapghana.R;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.model.EventDetail;
import com.mapghana.app.ui.sidemenu.Event.EventListByDate;

import java.util.ArrayList;

public class EventSectionListAdapterByDate extends RecyclerView.Adapter<EventSectionListAdapterByDate.SingleItemRowHolder> {

    private ArrayList<EventDetail> itemsList;
    private EventListByDate mListener;

    public EventSectionListAdapterByDate(EventListByDate listener, ArrayList<EventDetail> itemsList) {
        this.itemsList = itemsList;
        this.mListener = listener;
    }

    public void setData(ArrayList<EventDetail> list) {
        this.itemsList = list;
        notifyDataSetChanged();
    }
    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event_date_wise_list, null);
        return new SingleItemRowHolder(v);

    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {
        holder.setPosition(i);
        EventDetail singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getName());

        Glide.clear(holder.itemImage);
        Glide.with(MapGhanaApplication.sharedInstance())
                .load(singleItem.getImage())
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean
                            isFirstResource) {
                        holder.progress_bar.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable>
                            target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progress_bar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.itemImage);
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected ImageView itemImage;
        protected ImageView seatsIcon;
        private LottieAnimationView progress_bar;
        private int position;

        public void setPosition(int position) {
            this.position = position;
        }

        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = view.findViewById(R.id.event_title);
            this.itemImage = view.findViewById(R.id.event_image);
            this.progress_bar = view.findViewById(R.id.progress_bar);
            this.seatsIcon = view.findViewById(R.id.event_ticket_icon);

            view.setOnClickListener(v -> {
               mListener.onClickEvent(itemsList.get(position));
            });

        }
    }

}