package com.mapghana.app.app_base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.mapghana.R;
import com.mapghana.base.BaseFragment;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.handler.NavigationViewHandlerInterface;
import com.mapghana.handler.ToolbarHandlerInterface;
import com.mapghana.rest.ApiHitListener;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ubuntu on 28/12/17.
 */

public abstract class AppBaseFragment extends BaseFragment
        implements AdapterClickListener, ApiHitListener {

    public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private Dialog alertDialogProgressBar;
    private LottieAnimationView lottieAnimationView;
    private AlertDialog mErrorDialog;

    protected ToolbarHandlerInterface getToolBar() {
        return (AppBaseActivity) getActivity();
    }

    protected NavigationViewHandlerInterface getNavHandler() {
        return (AppBaseActivity) getActivity();
    }

    @Override
    public void onAdapterClickListener(int position) {

    }

    @Override
    public void onAdapterClickListener(String action) {

    }

    @Override
    public void onAdapterClickListener(int position, String action) {

    }

    @Override
    public void reInitializeComponent() {

    }

    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {

    /*    int code = response.code();
        if (code == 404) {
            SessionManager.logout(getActivity());
            getActivity().finish();
        }
*/
    }

    @Override
    public void onFailResponse(int apiId, String error) {
    }


    public void displayProgressBar(boolean isCancellable) {
        displayProgressBar(isCancellable, "");
    }

    public void displayProgressBar(boolean isCancellable, String msg) {
        dismissProgressBar();
        if (getActivity()==null)return ;
        alertDialogProgressBar = new Dialog(getActivity(),
                R.style.YourCustomStyle);
        alertDialogProgressBar.setCancelable(isCancellable);
        alertDialogProgressBar
                .requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.progress_dialog);
        TextView tvTitle = alertDialogProgressBar.findViewById(R.id.tvTitle);
        lottieAnimationView = alertDialogProgressBar.findViewById(R.id.mk_loader);

        if (msg!=null && !msg.isEmpty()) {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(msg);
        }else {
            tvTitle.setVisibility(View.GONE);
        }
        alertDialogProgressBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        if (getActivity() != null && !getActivity().isFinishing())
            alertDialogProgressBar.show();
            lottieAnimationView.playAnimation();

    }

    public void dismissProgressBar() {
        if (getActivity()!=null &&alertDialogProgressBar != null) {
            alertDialogProgressBar.dismiss();
            lottieAnimationView.cancelAnimation();
        }
    }

    public void displayErrorDialog(String title, String content) {
        mErrorDialog = new AlertDialog.Builder(getContext())
                // .setTitle(title)
                .setMessage(content)
                // .setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.ic_launcher))
                .setCancelable(false)
                .setNegativeButton(R.string.dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        mErrorDialog.show();
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);

    }

    public boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }




    public void updateViewVisibitity(View myView, int visibility) {
        if (myView != null && myView.getVisibility() != visibility) {
            myView.setVisibility(visibility);
        }
    }


}
