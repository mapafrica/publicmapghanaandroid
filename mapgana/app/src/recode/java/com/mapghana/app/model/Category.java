package com.mapghana.app.model;

import java.util.List;

/**
 * Created by ubuntu on 12/1/18.
 */

public class Category {

    private int code;
    private String error;
    private int status;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2
         * name : Accomodation
         * image : null
         * sub_category : [{"id":3,"cat_id":2,"name":"Hotel","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516014361.jpg","status":"1","deleted_at":null,"created_at":"2018-01-15 10:36:53","updated_at":"2018-01-15 11:06:01"},{"id":4,"cat_id":2,"name":"Lodge","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516014361.jpg","status":"1","deleted_at":null,"created_at":"2018-01-15 10:36:53","updated_at":"2018-01-15 11:06:01"},{"id":5,"cat_id":2,"name":"Bed & Breakfast","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516014361.jpg","status":"1","deleted_at":null,"created_at":"2018-01-15 10:36:53","updated_at":"2018-01-15 11:06:01"},{"id":6,"cat_id":2,"name":"Hostel","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516014361.jpg","status":"1","deleted_at":null,"created_at":"2018-01-15 10:36:53","updated_at":"2018-01-15 11:06:01"},{"id":7,"cat_id":2,"name":"Apartments","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516014361.jpg","status":"1","deleted_at":null,"created_at":"2018-01-15 10:36:53","updated_at":"2018-01-15 11:06:01"},{"id":8,"cat_id":2,"name":"Other","image":"http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516014361.jpg","status":"1","deleted_at":null,"created_at":"2018-01-15 10:36:53","updated_at":"2018-01-15 11:06:01"}]
         */

        private boolean selected =false;
        private int id;
        private String name;
        private Object image;
        private List<SubCategoryBean> sub_category;

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public List<SubCategoryBean> getSub_category() {
            return sub_category;
        }

        public void setSub_category(List<SubCategoryBean> sub_category) {
            this.sub_category = sub_category;
        }

        public static class SubCategoryBean {
            /**
             * id : 3
             * cat_id : 2
             * name : Hotel
             * image : http://205.147.102.6/p/sites/mapgh_laravel/public/storage/upload/1516014361.jpg
             * status : 1
             * deleted_at : null
             * created_at : 2018-01-15 10:36:53
             * updated_at : 2018-01-15 11:06:01
             */

            private int id;
            private int cat_id;
            private String name;
            private String image;
            private String status;
            private Object deleted_at;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCat_id() {
                return cat_id;
            }

            public void setCat_id(int cat_id) {
                this.cat_id = cat_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
