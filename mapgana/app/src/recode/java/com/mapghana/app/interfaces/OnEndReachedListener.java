package com.mapghana.app.interfaces;

public interface OnEndReachedListener {
    void onEndReached(int position);

}
