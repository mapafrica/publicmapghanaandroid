package com.mapghana.app.interfaces;

import com.mapghana.app.model.EventInfoSection;

public interface EventMapListener {
    void onMapIconClick(EventInfoSection item);
}
