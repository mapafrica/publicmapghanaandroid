package com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list;


import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.model.FilterModel;
import com.mapghana.app.model.GlobeSearch;
import com.mapghana.app.rest.ApiIds;
import com.mapghana.app.retrofit.RestClient;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.detail_on_map.SingleItemOnMapFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.adapter.ItemListOrgAdapter;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.filterfragment.FilterFragment;
import com.mapghana.app.ui.activity.dashboard.dashboard.search.search_list.spf.DefaulFilterSpf;
import com.mapghana.app.utils.Constants;
import com.mapghana.util.ConnectionDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchListFragment extends AppBaseFragment {

    private AppCompatTextView tvTitle;
    private RecyclerView rvItems;
    private ImageButton ivFilter, ivBackButton;
    private List<GlobeSearch.DataBean> listData;
    private AppCompatEditText etSearch;
    private ItemListOrgAdapter itemListOrgAdapter;
    private String search_keyword, search_response;

    public void setSearch_keyword(String search_keyword) {
        this.search_keyword = search_keyword;
    }

    public void setSearch_response(String search_response) {
        this.search_response = search_response;
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_category_item;
    }

    @Override
    public void initializeComponent() {
        init();
        tvTitle = getView().findViewById(R.id.tvTitle);
        rvItems = getView().findViewById(R.id.rvItems);
        ivFilter = getView().findViewById(R.id.ivFilter);
        ivBackButton = getView().findViewById(R.id.ivBackButton);
        etSearch = getView().findViewById(R.id.etSearch);
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        ivBackButton.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        tvTitle.setText(getString(R.string.Search_Results));
        listData = new ArrayList<>();
        itemListOrgAdapter = new ItemListOrgAdapter(this, listData, listData);
        rvItems.setAdapter(itemListOrgAdapter);
        // displayLog(TAG, "getArguments: " + getArguments());
        if (search_response != null && search_response.length() > 0 && DefaulFilterSpf.getFilter(getContext()) == null) {
            //  String search_response=getArguments().getString(Constants.search_response);
            Gson gson = new Gson();
            GlobeSearch globeSearch = gson.fromJson(search_response, GlobeSearch.class);
            listData.addAll(globeSearch.getSearchData().getData());
            itemListOrgAdapter.notifyDataSetChanged();
        } else if (search_keyword != null && search_keyword.length() > 0 && DefaulFilterSpf.getFilter(getContext()) != null) {
            // search_keyword=getArguments().getString(Constants.keyword);
            String s = DefaulFilterSpf.getFilter(getContext());
            Gson gson = new Gson();
            FilterModel filterModel = gson.fromJson(s, FilterModel.class);
            String city_id = "";
            String categoty_id = "";
            String lat = "";
            String log = "";
            String distance = "";
            if (filterModel.getCategoty_id() != null && !filterModel.getCategoty_id().equals("")) {
                categoty_id = filterModel.getCategoty_id();
            }
            if (filterModel.getCity_id() != null && !filterModel.getCity_id().equals("")) {
                city_id = filterModel.getCity_id();
            }
            if (filterModel.getLat() != 0) {
                lat = String.valueOf(filterModel.getLat());
            }
            if (filterModel.getLog() != 0) {
                log = String.valueOf(filterModel.getLog());
            }
            if (filterModel.getDistance() != null && !filterModel.getDistance().equals("")) {
                distance = filterModel.getDistance();
            }

            doRequest(search_keyword, categoty_id, city_id, lat, log, distance);
        }
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (listData != null && listData.size() > 0) {
                    itemListOrgAdapter.getFilter().filter(s.toString());
                }
            }
        });

    }

    @Override
    public void viewCreateFromBackStack() {
        super.viewCreateFromBackStack();
        init();

    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().lockDrawer(true);
        getNavHandler().setExploreVisibility(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackButton:
                getActivity().onBackPressed();
                DefaulFilterSpf.clearFilter(getContext());
                break;
            case R.id.ivFilter:
                try {
                    getDashboardActivity().changeFragment(new FilterFragment(), true, false, 0,
                            R.anim.alpha_visible_anim, 0,
                            0, R.anim.alpha_gone_anim, true);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onAdapterClickListener(int id) {
        if (listData != null && listData.size() > 0) {

            SingleItemOnMapFragment detailsFragment = new SingleItemOnMapFragment();
            detailsFragment.setPost_id(id);

            try {
               /* getDashboardActivity().getMapHandler().updateMainContainerVisivility(View.GONE);
                getDashboardActivity().getMapHandler().showSingleItemOnMapFragment(detailsFragment);*/
                getDashboardActivity().changeFragment(detailsFragment, true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void doRequest(String keyword, String category_id, String city_id,
                           String lat, String log, String distance) {
        if (ConnectionDetector.isNetAvail(getContext())) {
            displayProgressBar(false);
            RestClient restClient = new RestClient(getContext());
            restClient.callback(this).searchFilter(keyword, category_id, city_id, lat, log, distance, "");
        } else {
            displayToast(Constants.No_Internet);
        }
    }


    @Override
    public void onSuccessResponse(int apiId, Response<ResponseBody> response) {
        dismissProgressBar();
        if (response.isSuccessful()) {
            if (apiId == ApiIds.ID_POSTS_SEarch_Filter) {
                try {
                    String s = response.body().string();
                    Gson gson = new Gson();
                    GlobeSearch data = gson.fromJson(s, GlobeSearch.class);
                    if (data.getStatus() != 0) {
                        clearList();
                        listData.addAll(data.getSearchData().getData());
                        if (listData.size() > 0) {
                            itemListOrgAdapter.notifyDataSetChanged();
                        } else {
                            displayToast("No result found...");
                        }
                    } else {
                        displayErrorDialog("Error", data.getError());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    displayErrorDialog("Error", e.getMessage());
                }
            }
        } else {
            displayErrorDialog("Error", response.message());
        }
    }

    private void clearList() {
        if (listData != null && listData.size() > 0) {
            listData.clear();
        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        dismissProgressBar();
        displayErrorDialog("Error", error);
    }
}
