package com.mapghana.app.ui.activity.mainactivity;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.helpers.toolbar.ToolbarHandler;
import com.mapghana.app.ui.activity.mainactivity.fragments.LoginFragment;

public class MainActivity extends AppBaseActivity {

    private ToolbarHandler toolbarHandler;

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    public ToolbarHandler getToolbarHandler() {
        return toolbarHandler;
    }

    @Override
    public void initializeComponent() {
        toolbarHandler = new ToolbarHandler(this);
        toolbarHandler.findViews();
        toolbarHandler.setToolbarVisibility(false);
        changeFragment(new LoginFragment(), false, false, 0, R.anim.alpha_visible_anim, 0, 0,
                R.anim.alpha_gone_anim, true);
    }

    @Override
    public int getFragmentContainerResourceId() {
        return R.id.container;
    }



    @Override
    public void setTitleButtonVisibiltyTB(boolean visibility) {
        toolbarHandler.setTitleButtonVisibilty(visibility);
    }

    @Override
    public void setTitleTextTB(String title) {
        toolbarHandler.setTitleText(title);
    }

    @Override
    public void setbackButtonVisibiltyTB(boolean visibility) {
        toolbarHandler.setbackButtonVisibilty(visibility);
    }

    @Override
    public void setToolbarVisibilityTB(boolean visibility) {
        toolbarHandler.setToolbarVisibility(visibility);
    }

    @Override
    public void setCalenderVisibility(boolean visibility) {

    }

    @Override
    public void setSearchVisibiltyInternal(boolean visibility) {

    }

    @Override
    public void setimgMultiViewVisibility(boolean visibility) {

    }

    @Override
    public void setAtoZZtoAVisibiltyInternal(boolean visibility) {

    }
}
