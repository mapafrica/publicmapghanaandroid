package com.mapghana.app.ui.activity.mainactivity.fragments.signup;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.mapghana.R;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.Valiations;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends AppBaseFragment implements CustomDatePickerDialog.OnDateListener {

    private static final String TAG = "SignUpFragment";
    private LinearLayout llExistingUser;
    private Spinner spGender;
    private TypefaceTextView tvSignUp, tvDob;
    private TypefaceEditText etUname, etPassword, etEmail, etPhone, etLoc;
    private String DOB;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {

        return R.layout.fragment_sign_up;
    }

    @Override
    public void initializeComponent() {
        getToolBar().setToolbarVisibilityTB(false);
        llExistingUser=(LinearLayout)getView().findViewById(R.id.llExistingUser);
        spGender=(Spinner)getView().findViewById(R.id.spGender);
        tvSignUp=(TypefaceTextView)getView().findViewById(R.id.tvSignUp);
        tvDob=(TypefaceTextView)getView().findViewById(R.id.tvDob);
        etUname=(TypefaceEditText)getView().findViewById(R.id.etUname);
        etPassword=(TypefaceEditText)getView().findViewById(R.id.etPassword);
        etEmail=(TypefaceEditText)getView().findViewById(R.id.etEmail);
        etPhone=(TypefaceEditText)getView().findViewById(R.id.etPhone);
        etLoc=(TypefaceEditText)getView().findViewById(R.id.etLoc);
        prepareData();
        tvSignUp.setOnClickListener(this);
        tvDob.setOnClickListener(this);
        llExistingUser.setOnClickListener(this);

    }

    private void prepareData(){
        SimpleSpinnerAdapter adapter=new SimpleSpinnerAdapter(getContext(),getContext().getResources().getStringArray(R.array.gender_array));
        spGender.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llExistingUser:
                getActivity().onBackPressed();
                break;
            case R.id.tvSignUp:
                onSignup();
                break;
            case R.id.tvDob:
                onDobDialog();
                break;
        }
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog=new CustomDatePickerDialog();
        if (dialog !=null){
            dialog.create(getContext()).callback(this).show();
        }
    }

    private void onSignup() {
        String name, password, mail, phone, dob, loc;
        name=etUname.getText().toString().trim();
        password=etPassword.getText().toString().trim();
        mail=etEmail.getText().toString().trim();
        phone=etPhone.getText().toString().trim();
        loc=etLoc.getText().toString().trim();
        if (Valiations.hasText(etUname) && Valiations.hasText(etPassword) && Valiations.isEmailAddress(etEmail, true)
                && Valiations.isPhoneNumber(etPhone, true) && Valiations.hasText(etLoc)){
            if (tvDob.getText().toString().trim().equals(getResources().getString(R.string.Date_of_birth))){
                displayToast("Select Date Of Birth");
                return;
            }

            displayToast("all ok");
        }
    }

    @Override
    public void onSuccess(Date date) {
        if (date!=null) {
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            DOB = format.format(date);
            tvDob.setText(DOB);
        }
    }
}
