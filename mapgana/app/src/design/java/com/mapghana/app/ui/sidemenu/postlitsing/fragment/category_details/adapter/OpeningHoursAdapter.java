package com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

/**
 * Created by ubuntu on 30/12/17.
 */

public class OpeningHoursAdapter extends BaseRecycleAdapter {


    String[] day;
    String[] time;

    public OpeningHoursAdapter(String[] day, String[] time) {
        this.day = day;
        this.time = time;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_openhours;
    }

    @Override
    protected int getItemSize() {
        return day.length;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new OpeningHoursAdapter.MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder{
        private TypefaceTextView tvNameLeft, tvNameRight;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvNameLeft=(TypefaceTextView)itemView.findViewById(R.id.tvNameLeft);
            tvNameRight=(TypefaceTextView)itemView.findViewById(R.id.tvNameRight);
        }

        @Override
        public void setData(int position) {
            tvNameLeft.setText(day[position]);
            tvNameRight.setText(time[position]);
        }

    }
}
