package com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter;


import android.graphics.Typeface;
import android.support.v4.app.Fragment;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.custom_rangebar.IRangeBarFormatter;
import com.mapghana.custom_rangebar.RangeBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AreaRangeFragment extends AppBaseFragment implements RangeBar.OnRangeBarChangeListener {


    private RangeBar rangeBar;
    private TypefaceTextView tvLeftDis, tvRightDis;
    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_area_range;
    }

    @Override
    public void initializeComponent() {
        rangeBar=(RangeBar)getView().findViewById(R.id.rangeBar);
        tvLeftDis=(TypefaceTextView)getView().findViewById(R.id.tvLeftDis);
        tvRightDis=(TypefaceTextView)getView().findViewById(R.id.tvRightDis);
        rangeBar.setOnRangeBarChangeListener(this);


        /*
        // to format a text
        rangeBar.setFormatter(new IRangeBarFormatter() {
            @Override
            public String format(String value) {
                Typeface typeface=Typeface.createFromAsset(getContext().getAssets(), "opensans_regular.ttf");

                return null;
            }
        });*/
    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
        tvLeftDis.setText(leftPinValue+" KM");
        tvRightDis.setText(rightPinValue+" KM");
    }
}
