package com.mapghana.app.ui.sidemenu.postlitsing;


import android.support.v4.app.Fragment;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostListingOrganizationFragment extends AppBaseFragment {

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_post_listing_organization;
    }

    @Override
    public void initializeComponent() {
        init();
    }

    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }
}
