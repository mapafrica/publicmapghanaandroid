package com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.adapter.FeaturesAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.adapter.OpeningHoursAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.adapter.ReviewsAdapter;
import com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.writereview.WriteReviewFragment;
import com.mapghana.customviews.TypefaceTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryDetailsFragment extends AppBaseFragment {

    private List list;
    private RecyclerView rvFeatures, evOpeningHours, rvReviews;
    private TypefaceTextView tvWriteReview;
    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_category_details;
    }

    @Override
    public void initializeComponent() {
        init();
        rvFeatures=(RecyclerView)getView().findViewById(R.id.rvFeatures);
        evOpeningHours=(RecyclerView)getView().findViewById(R.id.evOpeningHours);
        rvReviews=(RecyclerView)getView().findViewById(R.id.rvReviews);
        tvWriteReview=(TypefaceTextView)getView().findViewById(R.id.tvWriteReview);
        GridLayoutManager manager=new GridLayoutManager(getContext(), 2);
        rvFeatures.setLayoutManager(manager);
        evOpeningHours.setLayoutManager(new LinearLayoutManager(getContext()));
        rvReviews.setLayoutManager(new LinearLayoutManager(getContext()));
        list=new ArrayList();
        prepareData();
        tvWriteReview.setOnClickListener(this);
    }

    private void prepareData() {
        FeaturesAdapter adapter=new FeaturesAdapter
                (getContext().getResources().getStringArray(R.array.features_array));
        rvFeatures.setAdapter(adapter);

        OpeningHoursAdapter timeAdapter=new OpeningHoursAdapter
                (getContext().getResources().getStringArray(R.array.day_array),
                        getContext().getResources().getStringArray(R.array.open_hour_array));
        evOpeningHours.setAdapter(timeAdapter);

        rvReviews.setAdapter(new ReviewsAdapter(list));
    }

    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Hotel));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvWriteReview:
                ((AppBaseActivity)getActivity()).changeFragment(new WriteReviewFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim,
                        true);
                break;

        }
    }
}
