package com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter.adapter.CategoryFilterAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFilterFragment extends AppBaseFragment {

    private RecyclerView rvTypes;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_apply_filter;
    }

    @Override
    public void initializeComponent() {
      //  init();
        rvTypes=(RecyclerView)getView().findViewById(R.id.rvTypes);
        rvTypes.setLayoutManager(new LinearLayoutManager(getContext()));
        rvTypes.setAdapter(new CategoryFilterAdapter(this,getContext().getResources().getStringArray(R.array.organization_type_array)));
    }

    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(false);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(false);
    }
}
