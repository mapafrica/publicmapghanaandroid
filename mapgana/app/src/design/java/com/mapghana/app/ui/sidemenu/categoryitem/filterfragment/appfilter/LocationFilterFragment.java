package com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter.adapter.LocationFilterAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationFilterFragment extends AppBaseFragment {

    private Spinner spLoc;

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_location_filter;
    }

    @Override
    public void initializeComponent() {
       // init();
        spLoc=(Spinner)getView().findViewById(R.id.spLoc);
        spLoc.setAdapter(new LocationFilterAdapter(getContext(), getContext().getResources().getStringArray(R.array.countries_array)));
    }
    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(false);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(false);
    }
}
