package com.mapghana.app;

import android.app.Application;
import android.location.Location;

import com.mapghana.mapservice.LocationServiceListner;

/**
 * Created by ubuntu on 28/12/17.
 */

public class App extends Application {

    LocationServiceListner locationServiceListner;
    public Location mLocation;

    public LocationServiceListner getLocationServiceListner() {
        return locationServiceListner;
    }

    public void setLocationServiceListner(LocationServiceListner locationServiceListner) {
        this.locationServiceListner = locationServiceListner;
    }

}
