package com.mapghana.app.app_base;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.mapghana.base.BaseActivity;
import com.mapghana.handler.NavigationViewHandlerInterface;
import com.mapghana.handler.ToolbarHandlerInterface;

/**
 * Created by ubuntu on 28/12/17.
 */

public abstract class AppBaseActivity extends BaseActivity
        implements NavigationViewHandlerInterface, ToolbarHandlerInterface, View.OnClickListener{

    private static final String TAG = "AppBaseActivity";
    @Override
    public void setTitleButtonVisibiltyTB(boolean visibility) {

    }

    @Override
    public void setTitleTextTB(String title) {

    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {

    }

    @Override
    public void setbackButtonVisibiltyTB(boolean visibility) {

    }

    @Override
    public void setNavTitle(String title) {

    }

    @Override
    public void setToolbarVisibilityTB(boolean visibility) {
    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {

    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {

    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {

    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {

    }

    @Override
    public void setHeaderProfilePic(String uri, Drawable drawable) {

    }

    @Override
    public void setUserName(String Name) {

    }

    @Override
    public void setNavLocationText(String Name) {

    }

    @Override
    public void lockDrawer(boolean visibilty) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {

    }

    @Override
    public void setPostType(String postType) {

    }

    @Override
    public String getPostType() {
        return null;
    }
}
