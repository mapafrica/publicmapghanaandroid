package com.mapghana.app.ui.sidemenu.categoryitem;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.adapter.ItemListAdapter;
import com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.FilterFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.viewdetails.ViewDetailsFragment;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryItemFragment extends AppBaseFragment {

    private TypefaceTextView tvTitle;
    private RecyclerView rvItems;
    private ImageButton ivFilter, ivBackButton;
    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_category_item;
    }

    @Override
    public void initializeComponent() {
        init();
        tvTitle=(TypefaceTextView)getView().findViewById(R.id.tvTitle);
        rvItems=(RecyclerView) getView().findViewById(R.id.rvItems);
        ivFilter=(ImageButton)getView().findViewById(R.id.ivFilter);
        ivBackButton=(ImageButton)getView().findViewById(R.id.ivBackButton);
        tvTitle.setText("Hotel");
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        ivBackButton.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        rvItems.setAdapter(new ItemListAdapter(this));
    }
    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().lockDrawer(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackButton:
                getActivity().onBackPressed();
                break;
            case R.id.ivFilter:
                ((AppBaseActivity)getActivity()).changeFragment(new FilterFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0,  R.anim.alpha_gone_anim, true);
                break;
        }
    }

    @Override
    public void onAdapterClickListener(int position) {
        ((AppBaseActivity)getActivity()).changeFragment(new ViewDetailsFragment(), true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0,  R.anim.alpha_gone_anim, true);
    }



}
