package com.mapghana.app.helpers.navigation;

import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.SpinnerAdapter;
import com.mapghana.app.ui.activity.dashboard.fragment.dashboard.search.SearchFragment;
import com.mapghana.app.ui.sidemenu.contactus.ContactUsFragment;
import com.mapghana.app.ui.sidemenu.individual.IndividualFragment;
import com.mapghana.app.ui.sidemenu.organization.OrganizationFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingIndividualsFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingOrganizationFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog.PostListingDialog;
import com.mapghana.app.ui.sidemenu.profile.ProfileFragment;
import com.mapghana.customviews.CircleImageView;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.handler.NavigationViewHandlerInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 6/12/17.
 */

public class NavigationViewHandler
        implements NavigationViewHandlerInterface, View.OnClickListener,
        AdapterClickListener, AdapterView.OnItemSelectedListener {

    private AppBaseActivity baseActivity;
    private DrawerLayout drawerLayout;
    private LinearLayout llLocation;
    private RelativeLayout llNavToolBar;
    private ImageButton ivToggleNavButton, ivBack, imgNavSearch;
    private TypefaceTextView tvTitle;
    private NavigationView navigationView;

    private CircleImageView ivUserPic;
    private TypefaceTextView tvUserName, tvNavLocation;
    private String postType;
    private Spinner spType;
    private RelativeLayout rl_profile;

    private static final String TAG = "NavigationViewHandler";


    public NavigationViewHandler(AppBaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public void findViewIds() {
        RecyclerView recyclerView;


        drawerLayout = (DrawerLayout) baseActivity.findViewById(R.id.drawerLayout);
        llNavToolBar = (RelativeLayout) baseActivity.findViewById(R.id.llNavToolBar);
        rl_profile = (RelativeLayout) baseActivity.findViewById(R.id.rl_profile);
        llLocation = (LinearLayout) baseActivity.findViewById(R.id.llLocation);
        ivToggleNavButton = (ImageButton) baseActivity.findViewById(R.id.ivToggleNavButton);
        ivBack = (ImageButton) baseActivity.findViewById(R.id.ivBack);
        imgNavSearch = (ImageButton) baseActivity.findViewById(R.id.imgNavSearch);
        tvTitle = (TypefaceTextView) baseActivity.findViewById(R.id.tvTitle);
        tvUserName = (TypefaceTextView) baseActivity.findViewById(R.id.tvUserName);
        tvNavLocation = (TypefaceTextView) baseActivity.findViewById(R.id.tvNavLocation);
        spType = (Spinner) baseActivity.findViewById(R.id.spType);
        recyclerView = (RecyclerView) baseActivity.findViewById(R.id.recyclerView);
        ivUserPic = (CircleImageView) baseActivity.findViewById(R.id.ivUserPic);
        navigationView = (NavigationView) baseActivity.findViewById(R.id.navigation_view);
        LinearLayoutManager manager = new LinearLayoutManager(baseActivity.getBaseContext());
        recyclerView.setLayoutManager(manager);

        ivBack.setOnClickListener(this);
        imgNavSearch.setOnClickListener(this);
        ivToggleNavButton.setOnClickListener(this);
        rl_profile.setOnClickListener(this);
        recyclerView.setAdapter(new NavigationMenuAdapter(this, getMenuList()));
        spType.setAdapter(new SpinnerAdapter(baseActivity.getBaseContext(), getTypes(), R.layout.item_post_type));
        spType.setOnItemSelectedListener(this);
    }

    private String[] getTypes() {
        return baseActivity.getResources().getStringArray(R.array.type_array);
    }

    private List<NavMenuModel> getMenuList() {
        List<NavMenuModel> menuList = new ArrayList<>();

        NavMenuModel navMenuModel1 = new NavMenuModel();
        navMenuModel1.setTitle(baseActivity.getResources().getString(R.string.Organizations));
        navMenuModel1.setIconPath(R.mipmap.organizations);
        menuList.add(navMenuModel1);

        NavMenuModel navMenuModel2 = new NavMenuModel();
        navMenuModel2.setTitle(baseActivity.getResources().getString(R.string.Individuals));
        navMenuModel2.setIconPath(R.mipmap.individuals);
        menuList.add(navMenuModel2);

        NavMenuModel navMenuModel3 = new NavMenuModel();
        navMenuModel3.setTitle(baseActivity.getResources().getString(R.string.Post_Listing));
        navMenuModel3.setIconPath(R.mipmap.post_listing);
        menuList.add(navMenuModel3);


        NavMenuModel navMenuModel = new NavMenuModel();
        navMenuModel.setTitle(baseActivity.getResources().getString(R.string.Contact_Us));
        navMenuModel.setIconPath(R.mipmap.call);
        menuList.add(navMenuModel);

        NavMenuModel navMenuModel5 = new NavMenuModel();
        navMenuModel5.setTitle(baseActivity.getResources().getString(R.string.About_Us));
        navMenuModel5.setIconPath(R.mipmap.call);
        menuList.add(navMenuModel5);

        NavMenuModel navMenuModel4 = new NavMenuModel();
        navMenuModel4.setTitle(baseActivity.getResources().getString(R.string.Logout));
        navMenuModel4.setIconPath(R.mipmap.log);
        menuList.add(navMenuModel4);

        return menuList;
    }


    @Override
    public void setNavTitle(String title) {
        if (tvTitle != null && !title.equals("")) {
            tvTitle.setText(title);
        }
    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {
        if (visibilty) {
            llNavToolBar.setVisibility(View.VISIBLE);
        } else {
            llNavToolBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {

        if (visibilty) {
            ivToggleNavButton.setVisibility(View.VISIBLE);
        } else {
            ivToggleNavButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {

        if (visibilty) {
            ivBack.setVisibility(View.VISIBLE);
        } else {
            ivBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void setHeaderProfilePic(String uri, Drawable drawable) {

        if (uri != null && !uri.equals("")) {
            Glide.with(baseActivity).load(uri).override(250, 250)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivUserPic);
        } else {
            ivUserPic.setImageDrawable(drawable);
        }
    }

    @Override
    public void setUserName(String name) {
        if (name != null && !name.equals("")) {
            tvUserName.setText(name);
        }
    }

    @Override
    public void onClick(View view) {

        if (ivToggleNavButton == view) {
            setDrawableStatus();
        }
        switch (view.getId()) {
            case R.id.rl_profile:
                setDrawableStatus();
                baseActivity.changeFragment(new ProfileFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.imgNavSearch:
                baseActivity.changeFragment(new SearchFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.ivBack:
                baseActivity.onBackPressed();
                break;
        }
    }

    public void setDrawableStatus() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawers();
        } else {
            drawerLayout.openDrawer(navigationView);

        }
    }

    @Override
    public void onAdapterClickListener(int position) {
        switch (position) {
            case 0:
                setDrawableStatus();
                baseActivity.changeFragment(new OrganizationFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, true);
                break;

            case 1:
                setDrawableStatus();
                baseActivity.changeFragment(new IndividualFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, true);

                break;

            case 2:
                setDrawableStatus();
                goToPostListing();
                break;
            case 3:
                setDrawableStatus();
                baseActivity.changeFragment(new ContactUsFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0, R.anim.translate_left_to_right_medium, true);

                break;
            /*case 3:
                setDrawableStatus();
                baseActivity.changeFragment(new ContactUsFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0,
                        0,  R.anim.translate_left_to_right_medium, true);

                break;
*/

        }
    }

    private void goToPostListing() {
        PostListingDialog dialog = new PostListingDialog(baseActivity,
                this, getPostType());
        dialog.show();
    }

    @Override
    public void onAdapterClickListener(String action) {

        if (action.equals(baseActivity.getResources().getString(R.string.Individuals))) {
            setPostType(baseActivity.getResources().getString(R.string.Individuals));
            baseActivity.changeFragment(new PostListingIndividualsFragment(), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        } else if (action.equals(baseActivity.getResources().getString(R.string.Organizations))) {
            setPostType(baseActivity.getResources().getString(R.string.Organizations));
            baseActivity.changeFragment(new PostListingOrganizationFragment(), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        }
    }

    @Override
    public void lockDrawer(boolean visibilty) {
        if (visibilty) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }


    private void showToast(String msg) {
        Toast.makeText(baseActivity, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {

        if (visibilty) {
            llLocation.setVisibility(View.VISIBLE);
        } else {
            llLocation.setVisibility(View.GONE);

        }
    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {
        if (visibilty) {
            imgNavSearch.setVisibility(View.VISIBLE);
        } else {
            imgNavSearch.setVisibility(View.GONE);

        }
    }

    @Override
    public void setNavLocationText(String Name) {
        tvNavLocation.setText(Name);
    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {
        if (visibilty) {
            tvTitle.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);

        }
    }


    @Override
    public void onAdapterClickListener(int position, String action) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            setPostType(baseActivity.getResources().getString(R.string.Organizations));
        } else if (position == 1) {
            setPostType(baseActivity.getResources().getString(R.string.Individuals));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void setPostType(String postType) {
        this.postType = postType;

        if (postType.equals(baseActivity.getResources().getString(R.string.Organizations))) {
            updateSpinnerText(0);
        } else if (postType.equals(baseActivity.getResources().getString(R.string.Individuals))) {
            updateSpinnerText(1);
        }

    }

    @Override
    public String getPostType() {
        return postType;
    }

    private void updateSpinnerText(int position) {
        spType.setSelection(position);
    }
}
