package com.mapghana.app.ui.sidemenu.organization.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class TypeAdapter extends BaseRecycleAdapter {
    AdapterClickListener adapterClickListener;

    String string[];

    public TypeAdapter(AdapterClickListener adapterClickListener, String[] string) {
        this.adapterClickListener = adapterClickListener;
        this.string = string;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_types;
    }

    @Override
    protected int getItemSize() {
        if (string!=null && string.length>0){
            return string.length;
        }
        return 0;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }
    class MyViewHolder extends ViewHolder{
        private TypefaceTextView tvTitle;

        private RecyclerView rvSubType;
        private int pos;
        private int visibility;
        private LinearLayout llButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle=(TypefaceTextView)itemView.findViewById(R.id.tvTitle);
            rvSubType=(RecyclerView) itemView.findViewById(R.id.rvSubType);
            llButton=(LinearLayout) itemView.findViewById(R.id.llButton);
            rvSubType.setLayoutManager(new LinearLayoutManager(getContext()));
        }

        @Override
        public void setData(int position) {
            pos=position;
            tvTitle.setText(string[position]);
            llButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case  R.id.llButton:
                        updateRv();
                    break;
            }
        }

        private void updateRv(){
            if (visibility==0) {
                visibility = 1;
                rvSubType.setVisibility(View.VISIBLE);
                rvSubType.setAdapter(new SubTypeAdapter(adapterClickListener, getContext().getResources().getStringArray(R.array.accom_array)));
            }
            else {
                visibility=0;
                rvSubType.setVisibility(View.GONE);
            }
        }

    }
}
