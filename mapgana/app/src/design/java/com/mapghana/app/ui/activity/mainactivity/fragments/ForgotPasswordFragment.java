package com.mapghana.app.ui.activity.mainactivity.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.base.BaseFragment;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends AppBaseFragment {


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void initializeComponent() {
         TypefaceTextView tvSubmit;

        getToolBar().setToolbarVisibilityTB(true);
        getToolBar().setTitleTextTB(getResources().getString(R.string.Forgot_password));

        tvSubmit=(TypefaceTextView)getView().findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSubmit:
                ((AppBaseActivity)getActivity()).changeFragment(new NewPasswordFragment(), true,
                        false, 0, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                        true);
                break;
        }
    }
}
