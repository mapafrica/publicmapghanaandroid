package com.mapghana.app.app_base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mapghana.R;
import com.mapghana.customviews.TypefaceTextView;

/**
 * Created by ubuntu on 28/12/17.
 */

public class SpinnerAdapter extends BaseAdapter implements View.OnClickListener {

    private Context context;
    private String [] titles;
    private int resource;

    public SpinnerAdapter(Context context, String[] titles, int resource) {
        this.context = context;
        this.titles = titles;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public Object getItem(int position) {
        return titles[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    int pos=0;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        pos=position;
        RelativeLayout llType;
        TypefaceTextView tvType;
        if (convertView==null){
            convertView=LayoutInflater.from(context).inflate(resource, parent, false);
        }

        llType=(RelativeLayout)convertView.findViewById(R.id.llType);
        tvType=(TypefaceTextView)convertView.findViewById(R.id.tvType);
        tvType.setText(titles[position]);
      //  llType.setOnClickListener(this);
        return convertView;
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(context, ""+pos, Toast.LENGTH_SHORT).show();
    }
}
