package com.mapghana.app.ui.sidemenu.organization;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.activity.dashboard.fragment.dashboard.search.SearchFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.CategoryItemFragment;
import com.mapghana.app.ui.sidemenu.organization.adapter.TypeAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrganizationFragment extends AppBaseFragment {

    private RecyclerView rvTypes;
    private ImageView imgSearch;

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_organization;
    }

    @Override
    public void initializeComponent() {
        init();
        rvTypes=(RecyclerView)getView().findViewById(R.id.rvTypes);
        imgSearch=(ImageView) getView().findViewById(R.id.imgSearch);
        rvTypes.setLayoutManager(new LinearLayoutManager(getContext()));
        rvTypes.setAdapter(new TypeAdapter(this,getContext().getResources().getStringArray(R.array.organization_type_array)));
        imgSearch.setOnClickListener(this);
    }

    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Organizations));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    @Override
    public void onAdapterClickListener(int position) {
        ((AppBaseActivity)getActivity()).changeFragment(new CategoryItemFragment(), true, false, 0,
                R.anim.alpha_visible_anim, 0,
                0,  R.anim.alpha_gone_anim,
                true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgSearch:
              /*  ((AppBaseActivity)getActivity()).changeFragment(new SearchFragment(), true, false, 0,
                        R.anim.translate_right_to_left_anim, 0, 0, 0,
                        true);*/
              break;
        }
    }
}
