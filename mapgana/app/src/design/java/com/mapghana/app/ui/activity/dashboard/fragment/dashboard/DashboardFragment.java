package com.mapghana.app.ui.activity.dashboard.fragment.dashboard;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingIndividualsFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.PostListingOrganizationFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog.PostListingDialog;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends AppBaseFragment {

    private static final String TAG = "DashboardFragment";

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public void initializeComponent() {
        init();
        TypefaceTextView tvPostListing;
        tvPostListing=(TypefaceTextView)getView().findViewById(R.id.tvPostListing);
        tvPostListing.setOnClickListener(this);
    }

    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(true);
        getNavHandler().setSearchButtonVisibuility(true);
        getNavHandler().setBackButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(false);
        getNavHandler().lockDrawer(false);
        getNavHandler().setNavToggleButtonVisibilty(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvPostListing:
                PostListingDialog dialog=new PostListingDialog(getContext(),
                        this, getNavHandler().getPostType());
                dialog.show();
                break;
        }
    }

    @Override
    public void onAdapterClickListener(String action) {

        if (action.equals(getResources().getString(R.string.Individuals))){
            getNavHandler().setPostType(getResources().getString(R.string.Individuals));
            ((AppBaseActivity) getActivity()).changeFragment(new PostListingIndividualsFragment(), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        }
        else if (action.equals(getResources().getString(R.string.Organizations))){
            getNavHandler().setPostType(getResources().getString(R.string.Organizations));
            ((AppBaseActivity) getActivity()).changeFragment(new PostListingOrganizationFragment(), true, false, 0,
                    R.anim.alpha_visible_anim, 0,
                    0, R.anim.alpha_gone_anim, true);
        }
    }
}
