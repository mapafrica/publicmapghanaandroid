package com.mapghana.app.ui.sidemenu.categoryitem.adapter;

import android.view.View;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

/**
 * Created by ubuntu on 3/1/18.
 */

public class ItemListAdapter extends BaseRecycleAdapter {

    AdapterClickListener adapterClickListener;

    public ItemListAdapter(AdapterClickListener adapterClickListener) {
        this.adapterClickListener = adapterClickListener;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_list_item;
    }

    @Override
    protected int getItemSize() {
        return 10;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }
    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private TypefaceTextView tvName;
        private LinearLayout llView;
        private int pos;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvName=(TypefaceTextView)itemView.findViewById(R.id.tvName);
            llView=(LinearLayout)itemView.findViewById(R.id.llView);
        }

        @Override
        public void setData(int position) {
            pos=position;
            llView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.llView:
                    adapterClickListener.onAdapterClickListener(pos);
                    break;
            }
        }
    }
}
