package com.mapghana.app.ui.activity.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.ui.activity.mainactivity.MainActivity;

public class SplashActivity extends AppBaseActivity {

    private Animation animation_trans, animation_alpha;

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_splash;
    }


    @Override
    public void initializeComponent() {
        /*RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.rlLayout);
        ImageView logo=(ImageView) findViewById(R.id.imgView);
        animation_alpha= AnimationUtils.loadAnimation(getBaseContext(), R.anim.alpha_visible_long);
        animation_trans= AnimationUtils.loadAnimation(getBaseContext(), R.anim.translate_bottom_to_up_anim);
        logo.startAnimation(animation_alpha);
        relativeLayout.startAnimation(animation_trans);*/
        final Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                StartActivityWithFinish(MainActivity.class);
            }
        });thread.start();

    }
}
