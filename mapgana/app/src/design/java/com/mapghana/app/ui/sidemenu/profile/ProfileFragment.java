package com.mapghana.app.ui.sidemenu.profile;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.mapghana.R;
import com.mapghana.app.adapter.SimpleSpinnerAdapter;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.utils.Constants;
import com.mapghana.customviews.CustomDatePickerDialog;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.util.Valiations;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends AppBaseFragment implements CustomDatePickerDialog.OnDateListener {

    private static final String TAG = "SignUpFragment";
    private Spinner spGender;
    private TypefaceTextView tvUpdate, tvDob, tvTitle;
    private TypefaceEditText etUname, etEmail, etPhone, etLoc;
    private String DOB;
    private ImageButton ivBackButton;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void initializeComponent() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        spGender=(Spinner)getView().findViewById(R.id.spGender);
        tvUpdate=(TypefaceTextView)getView().findViewById(R.id.tvUpdate);
        tvDob=(TypefaceTextView)getView().findViewById(R.id.tvDob);
        tvTitle=(TypefaceTextView)getView().findViewById(R.id.tvTitle);
        etUname=(TypefaceEditText)getView().findViewById(R.id.etUname);
        etEmail=(TypefaceEditText)getView().findViewById(R.id.etEmail);
        etPhone=(TypefaceEditText)getView().findViewById(R.id.etPhone);
        etLoc=(TypefaceEditText)getView().findViewById(R.id.etLoc);
        ivBackButton=(ImageButton) getView().findViewById(R.id.ivBackButton);

        prepareData();
        tvUpdate.setOnClickListener(this);
        tvDob.setOnClickListener(this);
        ivBackButton.setOnClickListener(this);
    }
    private void prepareData(){
        tvTitle.setText(getContext().getResources().getString(R.string.Profile));
        SimpleSpinnerAdapter adapter=new SimpleSpinnerAdapter(getContext(),getContext().getResources().getStringArray(R.array.gender_array));
        spGender.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvUpdate:
                onSignup();
                break;
            case R.id.tvDob:
                onDobDialog();
                break;
            case R.id.ivBackButton:
                getActivity().onBackPressed();
                break;
        }
    }

    private void onSignup() {
        if (Valiations.hasText(etUname) && Valiations.isEmailAddress(etEmail, true)
                && Valiations.isPhoneNumber(etPhone, true) && Valiations.hasText(etLoc)){
            if (tvDob.getText().toString().trim().equals(getResources().getString(R.string.Date_of_birth))){
                displayToast("Select Date Of Birth");
                return;
            }
            if (getItem()==null){
                displayToast("Select Gender");

                return;
            }
            displayToast("All Ok");
           /* if (ConnectionDetector.isNetAvail(getContext())){
                displayProgressBar(false);
                restClient.callback(this).updateProfile(ID,name, mail, tvDob.getText().toString().trim(), getItem(), phone, loc, getContext());
            }
            else {
                displayToast(Constants.No_Internet);
            }*/
        }
    }

    private void onDobDialog() {
        CustomDatePickerDialog dialog=new CustomDatePickerDialog();
        if (dialog !=null){
            dialog.create(getContext()).callback(this).show();
        }
    }


    @Override
    public void onSuccess(Date date) {
        if (date!=null) {
            SimpleDateFormat format = new SimpleDateFormat(Constants.date_format);
            DOB = format.format(date);
            tvDob.setText(DOB);
        }
    }

    private String getItem(){
        int position= spGender.getSelectedItemPosition();
        String[] array=getContext().getResources().getStringArray(R.array.gender_array);
        for (int i=0; i<array.length; i++ ){
            if (position==i){
                return array[i];
            }
        }
        return null;
    }
}
