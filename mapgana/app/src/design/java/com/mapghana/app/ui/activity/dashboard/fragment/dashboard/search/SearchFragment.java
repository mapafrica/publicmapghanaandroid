package com.mapghana.app.ui.activity.dashboard.fragment.dashboard.search;


import android.animation.Animator;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.activity.dashboard.fragment.dashboard.search.adapter.SearchAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends AppBaseFragment {

    private Animator animator;
    private static final String TAG = "SearchFragment";
    LinearLayout myView;
    private RecyclerView recycler_view;
    private List list;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_search;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myView=getView().findViewById(R.id.myView);
        recycler_view=(RecyclerView)getView().findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        list=new ArrayList();
        recycler_view.setAdapter(new SearchAdapter(list));
        init();

    }

    @Override
    public void initializeComponent() {
        getNavHandler().setNavigationToolbarVisibilty(false);
        ImageView imgSearch;
        ImageButton imgBack;
        imgSearch=(ImageView) getView().findViewById(R.id.imgSearch);
        imgBack=(ImageButton) getView().findViewById(R.id.imgBack);
        imgSearch.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }

    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        myView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                    myView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                // get the final radius for the clipping circle
                float finalRadius = (float) Math.hypot(0, myView.getHeight());// hypot will calculat how much do you want to run animation
                playReveal(myView.getWidth(),0, 0, finalRadius);
            }
        });
        }


    }

    private void playReveal(int cx,int cy,float startRadius,float finalRadius){
        // Android native animator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            animator =
                    ViewAnimationUtils.createCircularReveal(myView, cx, cy, startRadius, finalRadius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(500);
            animator.start();
        }

    }

}
