package com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.writereview;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class WriteReviewFragment extends AppBaseFragment {

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_write_review;
    }

    @Override
    public void initializeComponent() {
        init();
    }
    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Write_a_Review));
    }

}
