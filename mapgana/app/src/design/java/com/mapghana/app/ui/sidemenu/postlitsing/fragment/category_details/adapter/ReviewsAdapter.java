package com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class ReviewsAdapter extends BaseRecycleAdapter {
    private List list;

    public ReviewsAdapter(List list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_reviews;
    }

    @Override
    protected int getItemSize() {
        if (list!=null && list.size()>0) {
            return list.size();
        }
        else {
            return 2;
        }
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder{
        private TypefaceTextView tvName;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName=(TypefaceTextView)itemView.findViewById(R.id.tvName);
        }

        @Override
        public void setData(int position) {
        }

    }
    
}
