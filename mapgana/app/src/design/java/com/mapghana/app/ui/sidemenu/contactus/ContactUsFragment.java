package com.mapghana.app.ui.sidemenu.contactus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends AppBaseFragment {


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_contact_us;
    }

    @Override
    public void initializeComponent() {
        init();

    }
    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Contact_Us));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

}
