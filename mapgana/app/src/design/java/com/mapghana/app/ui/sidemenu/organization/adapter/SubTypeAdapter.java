package com.mapghana.app.ui.sidemenu.organization.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

/**
 * Created by ubuntu on 3/1/18.
 */

public class SubTypeAdapter extends BaseRecycleAdapter {
    String titles[];
    AdapterClickListener adapterClickListener;

    public SubTypeAdapter( AdapterClickListener adapterClickListener, String[] titles) {
        this.adapterClickListener = adapterClickListener;
        this.titles = titles;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_subtype;
    }

    @Override
    protected int getItemSize() {
        if (titles!=null && titles.length>0){
            return titles.length;
        }
        return 0;
    }

    @Override
    protected BaseRecycleAdapter.ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }
    class MyViewHolder extends BaseRecycleAdapter.ViewHolder {
        private TypefaceTextView tvSubTitle;
        private LinearLayout llSubType;
        private int pos;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvSubTitle=(TypefaceTextView)itemView.findViewById(R.id.tvSubTitle);
            llSubType=(LinearLayout)itemView.findViewById(R.id.llSubType);
        }

        @Override
        public void setData(int position) {
            pos=position;
         tvSubTitle.setText(titles[position]);
         llSubType.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        switch (v.getId()){
            case R.id.llSubType:
                adapterClickListener.onAdapterClickListener(pos);
                break;
        }
        }
    }
}
