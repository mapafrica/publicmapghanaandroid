package com.mapghana.app.app_base;

import com.mapghana.base.BaseFragment;
import com.mapghana.handler.AdapterClickListener;
import com.mapghana.handler.NavigationViewHandlerInterface;
import com.mapghana.handler.ToolbarHandlerInterface;

/**
 * Created by ubuntu on 28/12/17.
 */

public abstract class AppBaseFragment extends BaseFragment implements AdapterClickListener {

    protected  ToolbarHandlerInterface getToolBar(){
      return   (AppBaseActivity)getActivity();
    }

    protected NavigationViewHandlerInterface getNavHandler(){
        return (AppBaseActivity)getActivity();
    }

    @Override
    public void onAdapterClickListener(int position) {

    }

    @Override
    public void onAdapterClickListener(String action) {

    }

    @Override
    public void onAdapterClickListener(int position, String action) {

    }
}
