package com.mapghana.app.ui.activity.dashboard;

import android.graphics.drawable.Drawable;
import android.util.Log;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.helpers.navigation.NavigationViewHandler;
import com.mapghana.app.ui.activity.dashboard.fragment.dashboard.DashboardFragment;

public class DashboardActivity  extends AppBaseActivity {

    private static final String TAG = "DashboardActivity";
    private NavigationViewHandler navigationViewHandler;

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void initializeComponent() {

        navigationViewHandler=new NavigationViewHandler(this);
        navigationViewHandler.findViewIds();
        changeFragment(new DashboardFragment(), false, true, 0,
                R.anim.alpha_visible_anim, 0,
                0,  R.anim.alpha_gone_anim, true);


        setUserName(getResources().getString(R.string.Lamberrta_Lauren));
        setNavLocationText(getResources().getString(R.string.Chicago));
    }

    @Override
    public int getFragmentContainerResourceId() {
        return R.id.frame_container;
    }

    @Override
    public void setNavToggleButtonVisibilty(boolean visibilty) {
        navigationViewHandler.setNavToggleButtonVisibilty(visibilty);
    }

    @Override
    public void setNavTitle(String title) {
        navigationViewHandler.setNavTitle(title);

    }

    @Override
    public void setBussinessTypeLayoutVisibuility(boolean visibilty) {
        navigationViewHandler.setBussinessTypeLayoutVisibuility(visibilty);

    }

    @Override
    public void setSearchButtonVisibuility(boolean visibilty) {
        navigationViewHandler.setSearchButtonVisibuility(visibilty);

    }

    @Override
    public void setNavigationToolbarVisibilty(boolean visibilty) {
        navigationViewHandler.setNavigationToolbarVisibilty(visibilty);

    }

    @Override
    public void setBackButtonVisibilty(boolean visibilty) {
        navigationViewHandler.setBackButtonVisibilty(visibilty);

    }

    @Override
    public void setHeaderProfilePic(String uri, Drawable drawable) {

        navigationViewHandler.setHeaderProfilePic(uri, drawable);

    }

    @Override
    public void setUserName(String Name) {
        navigationViewHandler.setUserName(Name);

    }

    @Override
    public void setNavLocationText(String Name) {
        navigationViewHandler.setNavLocationText(Name);

    }

    @Override
    public void lockDrawer(boolean visibilty) {
        navigationViewHandler.lockDrawer(visibilty);
    }

    @Override
    public void setNavTitleTextVisibilty(boolean visibilty) {
        navigationViewHandler.setNavTitleTextVisibilty(visibilty);
    }

    @Override
    public void setPostType(String postType) {
        navigationViewHandler.setPostType(postType);
    }

    @Override
    public String getPostType() {
        return navigationViewHandler.getPostType();
    }
}
