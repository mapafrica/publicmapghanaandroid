package com.mapghana.app.ui.sidemenu.categoryitem.filterfragment;


import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter.AreaRangeFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter.CategoryFilterFragment;
import com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter.LocationFilterFragment;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends AppBaseFragment {

    private static final String TAG = "FilterFragment";
    private TypefaceTextView tvCategory, tvLoc, tvAreaRange;
    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_filter;
    }

    @Override
    public void initializeComponent() {
        init();
        tvCategory=(TypefaceTextView)getView().findViewById(R.id.tvCategory);
        tvLoc=(TypefaceTextView)getView().findViewById(R.id.tvLoc);
        tvAreaRange=(TypefaceTextView)getView().findViewById(R.id.tvAreaRange);

        changeChildFragment(new CategoryFilterFragment(), false, false,
                0 ,  R.anim.alpha_visible_anim, 0,
                0,  R.anim.alpha_gone_anim);
        tvCategory.setOnClickListener(this);
        tvLoc.setOnClickListener(this);
        tvAreaRange.setOnClickListener(this);
    }
    private void init() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Filter));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvCategory:
                updateUI(tvCategory);
                changeChildFragment(new CategoryFilterFragment(), false, false,
                        0 ,  R.anim.alpha_visible_anim, 0,
                        0,  R.anim.alpha_gone_anim);
                break;
            case R.id.tvLoc:
                updateUI(tvLoc);
                changeChildFragment(new LocationFilterFragment(), false, false,
                        0 ,  R.anim.alpha_visible_anim, 0,
                        0,  R.anim.alpha_gone_anim);

                break;
            case R.id.tvAreaRange:
                updateUI(tvAreaRange);
                changeChildFragment(new AreaRangeFragment(), false, false,
                        0 ,  R.anim.alpha_visible_anim, 0,
                        0,  R.anim.alpha_gone_anim);

                break;
        }
    }

    @Override
    public int getFragmentContainerResourceId(Fragment fragment) {
        return R.id.child_container;
    }

    private void updateUI(TypefaceTextView view){
        TypefaceTextView[] tv_array={tvCategory,tvLoc, tvAreaRange};
         for (int i=0; i<tv_array.length; i++){
             if (view==tv_array[i]){
                 tv_array[i].setTextColor(getResources().getColor(R.color.colorRed));
             }
             else {
                 tv_array[i].setTextColor(getResources().getColor(R.color.colorDarkGray));

             }
         }
    }
}
