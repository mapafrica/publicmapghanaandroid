package com.mapghana.app.ui.sidemenu.categoryitem.viewdetails;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.CategoryDetailsFragment;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewDetailsFragment extends AppBaseFragment {


    private TypefaceTextView tvViewDetails;
    private ImageButton imgBack;
    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_view_details;
    }

    @Override
    public void initializeComponent() {
        init();
        tvViewDetails=(TypefaceTextView)getView().findViewById(R.id.tvViewDetails);
        imgBack=(ImageButton) getView().findViewById(R.id.imgBack);
        tvViewDetails.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvViewDetails:
                ((AppBaseActivity)getActivity()).changeFragment(new CategoryDetailsFragment(), true, false, 0,
                        R.anim.alpha_visible_anim, 0,
                        0,  R.anim.alpha_gone_anim, true);
            break;
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }
    private void init(){
        getNavHandler().setNavigationToolbarVisibilty(false);
        getNavHandler().lockDrawer(true);
    }

}
