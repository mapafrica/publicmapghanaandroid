package com.mapghana.app.ui.sidemenu.postlitsing;


import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseFragment;


public class PostListingIndividualsFragment extends AppBaseFragment {
    private EditText etName, etAddress, etWhatsApp, etDateOfBirth, etAboutMe, etKeywordNickName, etMapghanaId, etTwitter, etInstagram, etFacebook, etYoutube, etSoundCloud, etBlog, etGaming;
    private SwitchCompat switchCompatPhoneNumberVisibility, switchCompatAvailability, switchCompatProfileVisibility, switchCompatPasswordProtect;
    private Spinner spinnerGender, spinnerRegion, spinnerStatus, spinnerOccupation;
    private ImageView imageViewAddEmail, imageViewAddedPhone, imageViewAddWebsite, imageViewFeature, imageView


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_post_listing_individual;
    }

    @Override
    public void initializeComponent() {

        initToolBar();
    }

    private void initToolBar() {
        getNavHandler().setNavigationToolbarVisibilty(true);
        getNavHandler().setBussinessTypeLayoutVisibuility(false);
        getNavHandler().setSearchButtonVisibuility(false);
        getNavHandler().setBackButtonVisibilty(true);
        getNavHandler().setNavTitle(getResources().getString(R.string.Post_Listing));
        getNavHandler().lockDrawer(true);
        getNavHandler().setNavToggleButtonVisibilty(false);
        getNavHandler().setNavTitleTextVisibilty(true);
    }

    private void initViewById(View view) {

        etName = view.findViewById(R.id.etName);
        etAddress = view.findViewById(R.id.etAddress);
        etWhatsApp = view.findViewById(R.id.etWhatsApp);
        etDateOfBirth = view.findViewById(R.id.etDob);
        etAboutMe = view.findViewById(R.id.et_bio_about_me);
        etKeywordNickName = view.findViewById(R.id.et_nickname_keyword_description);
        etMapghanaId = view.findViewById(R.id.etmapghana_id);
        etTwitter = view.findViewById(R.id.et_twitter);
        etInstagram = view.findViewById(R.id.et_instagram);
        etFacebook = view.findViewById(R.id.et_facebook);
        etYoutube = view.findViewById(R.id.et_youtube_vimeo);
        etSoundCloud = view.findViewById(R.id.et_sound_cloud);
        etBlog = view.findViewById(R.id.et_blog);
        etGaming = view.findViewById(R.id.et_gaming);

        switchCompatPhoneNumberVisibility = view.findViewById(R.id.phone_number_visibilty);
        switchCompatAvailability = view.findViewById(R.id.switch_availability);
        switchCompatProfileVisibility = view.findViewById(R.id.paid_event_switch);
        switchCompatPasswordProtect = view.findViewById(R.id.mapgh_ticket_hosting);

    }
}
