package com.mapghana.app.app_base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import com.mapghana.base.BaseDialog;
import com.mapghana.handler.AdapterClickListener;

/**
 * Created by ubuntu on 30/12/17.
 */

public abstract class AppBaseDialog extends BaseDialog
        implements RadioGroup.OnCheckedChangeListener, AdapterClickListener {

    public AppBaseDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }

    @Override
    public void onAdapterClickListener(int position) {

    }

    @Override
    public void onAdapterClickListener(String action) {

    }

    @Override
    public void onAdapterClickListener(int position, String action) {

    }
}
