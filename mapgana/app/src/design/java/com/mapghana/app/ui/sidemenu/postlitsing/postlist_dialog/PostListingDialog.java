package com.mapghana.app.ui.sidemenu.postlitsing.postlist_dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseDialog;
import com.mapghana.customviews.TypefaceRadioButton;
import com.mapghana.handler.AdapterClickListener;

/**
 * Created by ubuntu on 30/12/17.
 */

public class PostListingDialog extends AppBaseDialog {

    private static final String TAG = "PostListingDialog";
    private AdapterClickListener adapterClickListener;
    private String postType;
    private TypefaceRadioButton rdOrg, rdIndi;

    public PostListingDialog(@NonNull Context context, AdapterClickListener adapterClickListener, String postType) {
        super(context);
        this.adapterClickListener = adapterClickListener;
        this.postType = postType;
    }

    @Override
    protected int getLayoutResourceView() {
        return R.layout.dialog_bussiness_type;
    }

    @Override
    protected void initializeComponent() {
        rdOrg=(TypefaceRadioButton)this.findViewById(R.id.rdOrg);
        rdIndi=(TypefaceRadioButton)this.findViewById(R.id.rdIndi);
        init();
        rdOrg.setOnClickListener(this);
        rdIndi.setOnClickListener(this);
    }

    private void init() {
        if (postType != null && postType.equals(getContext().getResources().getString(R.string.Organizations))) {
            updateUI(rdOrg);
        } else if (postType != null && postType.equals(getContext().getResources().getString(R.string.Individuals))) {
            updateUI(rdIndi);
        }
    }

    @Override
    public void onClick(View v) {
        String title="";
        switch (v.getId()){
            case R.id.rdOrg:
                updateUI(rdOrg);
                title=rdOrg.getText().toString().trim();
                break;
            case R.id.rdIndi:
                updateUI(rdIndi);
                title=rdIndi.getText().toString().trim();
                break;
        }
        adapterClickListener.onAdapterClickListener(title);
        dismiss();
    }

    private void updateUI(RadioButton selectedRadio){
        RadioButton[] radio_array={rdIndi, rdOrg};
        for (int i=0; i<radio_array.length; i++){
            if (selectedRadio==radio_array[i]){
                radio_array[i].setChecked(true);
            }
            else {
                radio_array[i].setChecked(false);
            }
        }
    }
}
