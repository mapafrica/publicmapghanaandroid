package com.mapghana.app.ui.sidemenu.postlitsing.fragment.category_details.adapter;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

/**
 * Created by ubuntu on 30/12/17.
 */

public class FeaturesAdapter extends BaseRecycleAdapter {

    String[] features;

    public FeaturesAdapter(String[] features) {
        this.features = features;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_features;
    }

    @Override
    protected int getItemSize() {
        return features.length;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder{
        private TypefaceTextView  tvNameLeft;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvNameLeft=(TypefaceTextView)itemView.findViewById(R.id.tvNameLeft);
        }

        @Override
        public void setData(int position) {
            tvNameLeft.setText(features[position]);
        }

    }
}
