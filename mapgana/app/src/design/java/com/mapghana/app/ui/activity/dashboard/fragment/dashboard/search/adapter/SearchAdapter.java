package com.mapghana.app.ui.activity.dashboard.fragment.dashboard.search.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.mapghana.R;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;

import java.util.List;

/**
 * Created by ubuntu on 3/1/18.
 */

public class SearchAdapter extends BaseRecycleAdapter {


    List list;
    public SearchAdapter(List list) {
        this.list=list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected int getLayoutResourceView(int viewType) {

        return R.layout.item_text_view;
    }

    @Override
    protected int getItemSize() {
        if (list!=null && list.size()>0) {
            return list.size();
        }
        else {
            return 5;
        }
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }

    class MyViewHolder extends ViewHolder{
        private TypefaceTextView  tvTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle=(TypefaceTextView)itemView.findViewById(R.id.tvTitle);
        }

        @Override
        public void setData(int position) {
            ;
        }

    }
}
