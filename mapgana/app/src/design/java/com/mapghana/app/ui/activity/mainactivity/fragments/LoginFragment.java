package com.mapghana.app.ui.activity.mainactivity.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mapghana.R;
import com.mapghana.app.app_base.AppBaseActivity;
import com.mapghana.app.app_base.AppBaseFragment;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;
import com.mapghana.app.ui.activity.mainactivity.fragments.signup.SignUpFragment;
import com.mapghana.customviews.TypefaceEditText;
import com.mapghana.customviews.TypefaceTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends AppBaseFragment {

    private static final String TAG = "LoginFragment";
    private TypefaceEditText etEmail, etPassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_login;
    }

    @Override
    public void initializeComponent() {
        getToolBar().setToolbarVisibilityTB(false);
        LinearLayout llCreateAcc;
        TypefaceTextView tvForgotPass, tvLogin;
        llCreateAcc=(LinearLayout)getView().findViewById(R.id.llCreateAcc);
        tvForgotPass=(TypefaceTextView)getView().findViewById(R.id.tvForgotPass);
        tvLogin=(TypefaceTextView)getView().findViewById(R.id.tvLogin);
        etEmail=(TypefaceEditText)getView().findViewById(R.id.etEmail);
        etPassword=(TypefaceEditText)getView().findViewById(R.id.etPassword);
        llCreateAcc.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llCreateAcc:
                ((AppBaseActivity)getActivity()).changeFragment(new SignUpFragment(), true,
                        false, 0, R.anim.alpha_visible_anim, 0, 0, R.anim.alpha_gone_anim,
                        true);

                break;
            case R.id.tvForgotPass:
                ((AppBaseActivity)getActivity()).changeFragment(new ForgotPasswordFragment(), true,
                        false, 0,  R.anim.alpha_visible_anim,0, 0, R.anim.alpha_gone_anim, true);
                break;
            case R.id.tvLogin:
                onLogin();
                break;
        }
    }

    private void onLogin() {
    /*   String mail= etEmail.getText().toString().trim();
       String password= etPassword.getText().toString().trim();*/
    //  if (Valiations.isEmailAddress(etEmail, true) && Valiations.hasText(etPassword)) {
          ((AppBaseActivity)getActivity()).StartActivity(DashboardActivity.class);
          clearBackStack(0);
   //   }
    }



}
