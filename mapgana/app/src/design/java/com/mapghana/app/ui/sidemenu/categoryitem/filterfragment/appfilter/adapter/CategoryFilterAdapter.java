package com.mapghana.app.ui.sidemenu.categoryitem.filterfragment.appfilter.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.mapghana.R;
import com.mapghana.app.ui.sidemenu.organization.adapter.SubTypeAdapter;
import com.mapghana.app.ui.sidemenu.organization.adapter.TypeAdapter;
import com.mapghana.base.BaseRecycleAdapter;
import com.mapghana.customviews.TypefaceTextView;
import com.mapghana.handler.AdapterClickListener;

/**
 * Created by ubuntu on 3/1/18.
 */

public class CategoryFilterAdapter extends BaseRecycleAdapter {
    AdapterClickListener adapterClickListener;

    String string[];

    public CategoryFilterAdapter(AdapterClickListener adapterClickListener, String[] string) {
        this.adapterClickListener = adapterClickListener;
        this.string = string;
    }

    @Override
    protected int getLayoutResourceView(int viewType) {
        return R.layout.item_category_filter;
    }

    @Override
    protected int getItemSize() {
        if (string!=null && string.length>0){
            return string.length;
        }
        return 0;
    }

    @Override
    protected ViewHolder setViewHolder(int viewType, View view) {
        return new MyViewHolder(view);
    }
    class MyViewHolder extends ViewHolder{
        private TypefaceTextView tvTitle;
        private ImageButton imgDown;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle=(TypefaceTextView)itemView.findViewById(R.id.tvTitle);
            imgDown=(ImageButton)itemView.findViewById(R.id.imgDown);

        }

        @Override
        public void setData(int position) {
            tvTitle.setText(string[position]);
            imgDown.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){

            }
        }


    }
}
