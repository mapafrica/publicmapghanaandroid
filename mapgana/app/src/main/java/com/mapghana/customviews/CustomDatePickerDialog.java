package com.mapghana.customviews;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

public class CustomDatePickerDialog implements DatePickerDialog.OnDateSetListener {

    private Context mContext;
    private OnDateListener mOnDateListener;
    private DatePickerDialog datePickerDialog;
    private static final String TAG = "CustomDatePickerDialog";
    private boolean maxDate;

    public CustomDatePickerDialog create(Context context, boolean maxDate) {
        mContext = context;
        this.maxDate =maxDate;
        return this;
    }

    public CustomDatePickerDialog callback(OnDateListener onDateListener) {
        mOnDateListener = onDateListener;
        return this;
    }

    public void show() {
        // Use the current date as the default date in the picker
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        show(null);

        // Create a new instance of DatePickerDialog
    }

    public void show( Calendar c) {
        // Use the current date as the default date in the picker
        if(c==null)
            c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(mContext, android.R.style.Theme_Holo_Dialog_MinWidth, this, year, month, day);
        datePickerDialog.setCancelable(true);
        datePickerDialog.getDatePicker().setSpinnersShown(true);
        datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(android.R.color.transparent)));
        if (maxDate){
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        Date date = calendar.getTime();
        Log.e(TAG, "year: "+year+", monthOfYear: "+monthOfYear+", dayOfMonth: "+dayOfMonth );
        mOnDateListener.onSuccess(date);
    }

    public interface OnDateListener {
        void onSuccess(Date date);
    }




}
