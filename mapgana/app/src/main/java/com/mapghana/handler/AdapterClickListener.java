package com.mapghana.handler;

/**
 * Created by ubuntu on 6/12/17.
 */

public interface AdapterClickListener {
     void onAdapterClickListener(int position);
     void onAdapterClickListener(String action);
     void onAdapterClickListener(int position, String action);
}
