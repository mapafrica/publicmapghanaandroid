package com.mapghana.handler;

/**
 * Created by ubuntu on 6/12/17.
 */

public interface NavigationViewHandlerInterface {

    // on dashboard
    void setNavToggleButtonVisibilty(boolean visibilty);
    void setNavTitle(String title);
    void setBussinessTypeLayoutVisibuility(boolean visibilty);
    void setSearchButtonVisibuility(boolean visibilty);
    void setNavigationToolbarVisibilty(boolean visibilty);
    void setBackButtonVisibilty(boolean visibilty);
    void setNavTitleTextVisibilty(boolean visibilty);
    void setNavLocationTextVisibilty(boolean visibilty);
    void setNavAddressText(String title);
    void setExploreVisibility(boolean visibility);
    void setCalenderVisibility(boolean visibility);
    void setSearchVisibiltyInternal(boolean visibility);
    void setimgMultiViewVisibility(boolean visibility);
    void setAtoZZtoAVisibiltyInternal(boolean visibility);

    void setHeaderProfilePic(String uri, int res);
    void setUserName(String Name);
    void setNavLocationText(String Name);

    String getPostType();
    void lockDrawer(boolean visibilty);





}
