package com.mapghana.handler;

public interface MediaAddAdpterListener {

    void onAddClick(String type);
    void onDeleteClick(int position, String type);
}
