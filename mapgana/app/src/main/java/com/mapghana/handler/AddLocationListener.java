package com.mapghana.handler;

public interface AddLocationListener {
    void getLocation(String lat, String log, String address);
}
