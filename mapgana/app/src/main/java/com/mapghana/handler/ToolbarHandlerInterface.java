package com.mapghana.handler;

/**
 * Created by ubuntu on 6/12/17.
 */

public interface ToolbarHandlerInterface {

    void setTitleButtonVisibiltyTB(boolean visibility);

    void setTitleTextTB(String title);

    void setbackButtonVisibiltyTB(boolean visibility);

    void setToolbarVisibilityTB(boolean visibility);

}
