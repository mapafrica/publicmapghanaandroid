package com.mapghana.videocompression;

import android.os.AsyncTask;

/**
 * @author Sunil kumar Yadav
 * @Since 9/7/18
 */
public class VideoCompress {


    private static final String TAG = VideoCompress.class.getSimpleName();

    public static VideoCompressTask compressVideoHigh(String srcPath, String destPath, long mTask_id,CompressListener listener) {
        VideoCompressTask task = new VideoCompressTask(listener, 1, mTask_id);//MediaController.COMPRESS_QUALITY_HIGH
        task.execute(srcPath, destPath);
        return task;
    }

   /* public static VideoCompressTask compressVideoMedium(String srcPath, String destPath, CompressListener listener) {
        VideoCompressTask task = new VideoCompressTask(listener, MediaController.COMPRESS_QUALITY_MEDIUM);
        task.execute(srcPath, destPath);
        return task;
    }

    public static VideoCompressTask compressVideoLow(String srcPath, String destPath, CompressListener listener) {
        VideoCompressTask task =  new VideoCompressTask(listener, MediaController.COMPRESS_QUALITY_LOW);
        task.execute(srcPath, destPath);
        return task;
    }
*/
    private static class VideoCompressTask extends AsyncTask<String, Float, Boolean> {
        String source;
        String destination;
        long mTask_id;
        private CompressListener mListener;
        private int mQuality;

        public VideoCompressTask(CompressListener listener, int quality,long task_id) {
            mListener = listener;
            mQuality = quality;
            mTask_id = task_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mListener != null) {
                mListener.onStart(mTask_id);
            }
        }

        @Override
        protected Boolean doInBackground(String... paths) {
            source=paths[0];
            destination=paths[1];
            return MediaController.getInstance().convertVideo(paths[0], paths[1], mQuality, new MediaController.CompressProgressListener() {
                @Override
                public void onProgress(float percent) {
                    publishProgress(percent);
                }
            });
        }

        @Override
        protected void onProgressUpdate(Float... percent) {
            super.onProgressUpdate(percent);
            if (mListener != null) {
                mListener.onProgress(mTask_id,percent[0]);
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (mListener != null) {
                if (result) {
                    mListener.onSuccess(mTask_id, source, destination);
                } else {
                    mListener.onFail(mTask_id,source, destination);
                }
            }
        }
    }

    public interface CompressListener {
        void onStart(long mTask_id);
        void onSuccess(long mTask_id, String source, String destination);
        void onFail(long mTask_id, String source, String destination);
        void onProgress(long mTask_id, float percent);
    }
}
