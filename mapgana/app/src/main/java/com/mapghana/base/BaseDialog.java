package com.mapghana.base;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.mapghana.R;

/**
 * Created by ubuntu on 11/12/17.
 */

public abstract class BaseDialog extends Dialog implements View.OnClickListener


{
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Window window=getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(getLayoutResourceView());
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        View view=window.getDecorView();
        view.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
      /*  findView();
        init();*/
      initializeComponent();
    }

    public BaseDialog(@NonNull Context context) {
        super(context);
        this.context=context;
    }


    protected abstract int getLayoutResourceView();

 /*   protected abstract void findView();
    protected abstract void init();*/

    protected abstract void initializeComponent();
    @Override
    public void onClick(View v) {

    }

    public void setAnimation(){
        WindowManager.LayoutParams layoutParams=getWindow().getAttributes();
        layoutParams.windowAnimations= R.style.BadeDialogStyle;
        layoutParams.gravity= Gravity.CENTER;
        layoutParams.width= ViewGroup.LayoutParams.MATCH_PARENT;
    }

    public void displayLog(String TAG, String msg){
       // Log.e(TAG, msg );
    }
}
