package com.mapghana.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.mapghana.BuildConfig;
import com.mapghana.app.utils.MapGhanaApplication;
import com.mapghana.app.ui.activity.dashboard.DashboardActivity;


/**
 * Created by bitu on 15/8/17.
 */

public abstract class BaseFragment extends Fragment
        implements View.OnClickListener {

    protected FragmentManager childFm;
    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        childFm = getChildFragmentManager();
        //  if (view == null) {
        setupFragmentViewByResource(inflater, container);
        initializeComponent();
      /*  }
        else {
            reInitializeComponent();
        }*/
        return view;
    }

    @Override
    public void onClick(View v) {

    }

    public synchronized void hideKeyboard() {
        if (!isValidActivity()) return;
        View view = getActivity().getCurrentFocus();
        if (view == null) {
            view = new View(getActivity());
        }
        hideKeyboard(view);
    }

    public synchronized void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        if (!isValidActivity()) return;
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        notifyToActivity(2);
    }

    /* public FullHeightLinearLayoutManager getFullHeightLinearLayoutManager () {
         return new FullHeightLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
     }
 */
    public LinearLayoutManager getVerticalLinearLayoutManager() {
        return new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
    }

    public LinearLayoutManager getGridLayoutManager(int column) {
        return new GridLayoutManager(getActivity(), column);
    }


    public void setupFragmentViewByResource(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = inflater.inflate(getLayoutResourceId(), container, false);
    }

    @Nullable
    @Override
    public View getView() {
        return view;
    }

    public void notifyToActivity(int tag) {
        if (getActivity() == null) return;
        switch (tag) {
            case 1:
                ((BaseActivity) getActivity()).onCreateViewFragment(this);
                break;
            case 2:
                ((BaseActivity) getActivity()).onDestroyViewFragment(this);
                break;
        }
    }

    public abstract int getLayoutResourceId();

    public int getFragmentContainerResourceId(Fragment fragment) {
        return -1;
    }

    public abstract void initializeComponent();

    public abstract void reInitializeComponent();

    public void viewCreateFromBackStack() {

    }

    public FragmentManager getChildFm() {
        return childFm;
    }

    public FragmentTransaction getNewChildFragmentTransaction() {
        return getChildFm().beginTransaction();
    }

    public void clearBackStack(int pos) {
        if (getFragmentManager().getBackStackEntryCount() > pos) {
            try {
                getFragmentManager().popBackStackImmediate(
                        getFragmentManager().getBackStackEntryAt(pos).getId(),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } catch (IllegalStateException e) {
            }
        }
    }

    public void clearChildFragmentBackStack() {
        clearChildFragmentBackStack(0);
    }

    public void clearChildFragmentBackStack(int pos) {
        if (childFm.getBackStackEntryCount() > pos) {
            try {
                childFm.popBackStackImmediate(childFm.getBackStackEntryAt(pos).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } catch (IllegalStateException e) {
            }
        }
    }

    public boolean handleOnBackPress() {
        if (getChildFm().getBackStackEntryCount() > 0) {
            getChildFm().popBackStackImmediate();
            hideKeyboard();
            return true;
        }
        return false;
    }


    public void changeChildFragment(Fragment f, boolean addBackStack,
                                    boolean clearAll, int pos, int enterAnim, int exitAnim,
                                    int enterAnimBackStack, int exitAnimBackStack) {

        if (clearAll) {
            clearChildFragmentBackStack(pos);
        }
        if (getFragmentContainerResourceId(f) == -1) return;
        if (f != null) {
            try {
                FragmentTransaction ft = getNewChildFragmentTransaction();
                ft.setCustomAnimations(enterAnim, exitAnim, enterAnimBackStack, exitAnimBackStack);
                ft.replace(getFragmentContainerResourceId(f), f, f.getClass().getSimpleName());
                if (addBackStack) {
                    ft.addToBackStack(f.getClass().getSimpleName());
                }
                ft.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*   @Override
       public void userLoggedIn (UserModel userModel) {

       }

       @Override
       public void loggedInUserUpdate (UserModel userModel) {

       }

       @Override
       public void loggedInUserClear () {

       }

       @Override
       public void onWebRequestCall (WebRequest webRequest) {

       }

       @Override
       public void onWebRequestResponse (WebRequest webRequest) {

       }
   */
    public boolean isValidActivity() {
        return getActivity() != null;
    }

    public boolean isValidString(String data) {
        return data != null && !data.trim().isEmpty();
    }

    public void printLog(String msg) {
        if (BuildConfig.DEBUG && msg != null) {
            displayLog(getClass().getSimpleName(), msg);
        }
    }

    private Toast toast;

    public void displayToast(String msg) {
        try {
            toast.getView().isShown();     // true if visible
            toast.setText(msg);
        } catch (Exception e) {         // invisible if exception
            toast = Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT);
        }
        toast.show();  //finally display it

    }

    public void displayLog(String TAG, String method, String msg) {
       /* if (BuildConfig.DEBUG)
            displayLog(TAG, method + ": " + msg);*/

    }

    public void displayLog(String TAG, String msg) {
        /*if (BuildConfig.DEBUG)
            Log.e(TAG, msg);*/
    }

    public DashboardActivity getDashboardActivity() throws IllegalAccessException {
        if (getActivity() == null)
            throw new IllegalAccessException("Activity is not found");
        return (DashboardActivity) getActivity();
    }

    public MapGhanaApplication getApplication() throws IllegalAccessException {
        if (getActivity() == null)
            throw new IllegalAccessException("Activity is not found");
        return (MapGhanaApplication) getActivity().getApplication();
    }

}
