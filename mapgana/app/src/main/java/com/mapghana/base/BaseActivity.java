package com.mapghana.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.mapghana.R;
import com.mapghana.app.ui.navigationView.MapViewFragment;


/**
 * Created by bitu on 15/8/17.
 */

public abstract class BaseActivity extends AppCompatActivity {
    private FragmentManager fm;
    private Bundle savedInstanceState;

    @Override
    protected void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState=savedInstanceState;
        fm = getSupportFragmentManager();
        beforeSetContentView();
        setContentView(getLayoutResourceId());
        initializeComponent();
    }


    public FragmentManager getFm () {
        return fm;
    }

    public FragmentTransaction getNewFragmentTransaction () {
        return getFm().beginTransaction();
    }

    public void clearFragmentBackStack () {
        clearFragmentBackStack(1);
    }

    public Fragment getFragmentByTag (String tag) {
        return getFm().findFragmentByTag(tag);
    }

    public void clearFragmentBackStack (int pos) {
        if (fm !=null && fm.getBackStackEntryCount() > pos) {
            try {
                fm.popBackStackImmediate(fm.getBackStackEntryAt(pos).getId(),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } catch (IllegalStateException e) {
            }
        }
    }

    public int getFragmentCount(){
      return   getSupportFragmentManager().getBackStackEntryCount();
    }

    @Override
    public void onBackPressed () {
        Fragment baseFragment = getFm().findFragmentById(R.id.container);
        if (baseFragment != null && baseFragment instanceof BaseFragment) {
            if (((BaseFragment) baseFragment).handleOnBackPress()) {
                return;
            }
        }
        super.onBackPressed();
    }

    public BaseFragment getLatestFragment () {

        Fragment fragment = fm.findFragmentById(getFragmentContainerResourceId());
        if (fragment != null && fragment instanceof BaseFragment) {
            return ((BaseFragment) fragment);
        }

        return null;
    }

    public void replaceFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(getFragmentContainerResourceId(), MapViewFragment.newInstance());
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public void pushFragment(Fragment fragment, boolean animated) {
        FragmentManager fm = this.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (animated) {
            ft.setCustomAnimations(
                    R.anim.slide_in_from_right, 0,
                    0, R.anim.slide_out_to_right);
        }
        ft.replace(getFragmentContainerResourceId(), fragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public void addFragment(Fragment fragment, boolean animated) {
        FragmentManager fm = this.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (animated) {
            ft.setCustomAnimations(
                    R.anim.slide_in_from_right, 0,
                    0, R.anim.slide_out_to_right);
        }
        ft.add(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public void changeFragment (Fragment f, boolean addBackStack,
                                boolean clearAll, int pos, int enterAnim, int exitAnim,
                                int enterAnimBackStack, int exitAnimBackStack, boolean isReplace) {
        if (getFragmentContainerResourceId() == -1) return;
        if (clearAll) {
            clearFragmentBackStack(pos);
        }
        if (f != null) {
            try {
                FragmentTransaction ft = getNewFragmentTransaction();
                ft.setCustomAnimations(enterAnim, exitAnim, enterAnimBackStack, exitAnimBackStack);
                if (isReplace) {
                    ft.replace(getFragmentContainerResourceId(), f, f.getClass().getSimpleName());
                } else {
                    ft.add(getFragmentContainerResourceId(), f, f.getClass().getSimpleName());
                }
                if (addBackStack) {
                    ft.addToBackStack(f.getClass().getSimpleName());
                }
                ft.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void beforeSetContentView () {

    }

    public abstract int getLayoutResourceId ();

    public int getFragmentContainerResourceId () {
        return -1;
    }

    public abstract void initializeComponent ();

    public void onCreateViewFragment (BaseFragment baseFragment) {
    }


    public void onDestroyViewFragment (BaseFragment baseFragment) {
    }

   /* @Override
    public void userLoggedIn (UserModel userModel) {
        BaseFragment baseFragment = getLatestFragment();
        if (baseFragment != null && baseFragment.isVisible()) {
            baseFragment.userLoggedIn(userModel);
        }
    }

    @Override
    public void loggedInUserUpdate (UserModel userModel) {
        BaseFragment baseFragment = getLatestFragment();
        if (baseFragment != null && baseFragment.isVisible()) {
            baseFragment.loggedInUserUpdate(userModel);
        }
    }

    @Override
    public void loggedInUserClear () {
        BaseFragment baseFragment = getLatestFragment();
        if (baseFragment != null && baseFragment.isVisible()) {
            baseFragment.loggedInUserClear();
        }
    }*/

   /* @Override
    public void onWebRequestCall (WebRequest webRequest) {

    }

    @Override
    public void onWebRequestResponse (WebRequest webRequest) {
        BaseFragment baseFragment = getLatestFragment();
        if (baseFragment != null && baseFragment.isVisible()) {
            baseFragment.onWebRequestResponse(webRequest);
        }
    }*/

    public boolean isValidString (String data) {
        return data != null && !data.trim().isEmpty();
    }

    public void printLog (String msg) {
        if (msg == null) return;
        displayLog(getClass().getSimpleName(), msg);
    }

    public void showToast (String message) {
        if (message == null || message.trim().isEmpty()) return;
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void StartActivity(Class aClass){
        Intent intent=new Intent(getBaseContext(), aClass);
        startActivity(intent);
    }
    public void StartActivityWithFinish(Class aClass, Bundle bundle){
        Intent intent=new Intent(getBaseContext(), aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (bundle!=null){
            intent.putExtras(bundle);
        }
        startActivity(intent);
        finish();
    }

    public Bundle getSavedInstanceState(){
        return savedInstanceState;
    }

    public void displayLog(String TAG, String msg){
      //  Log.e(TAG, msg );
    }
}
