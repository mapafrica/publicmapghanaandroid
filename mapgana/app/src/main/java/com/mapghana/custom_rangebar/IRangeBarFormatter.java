package com.mapghana.custom_rangebar;

/**
 * Created by franciscomorais on 24/06/15.
 */
public interface IRangeBarFormatter {

    String format(String value);

}