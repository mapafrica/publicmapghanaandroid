package com.mapghana.util;

import android.content.Context;
import android.support.v7.view.ContextThemeWrapper;
import android.widget.MediaController;

import com.mapghana.R;

public class CustomMediaController extends MediaController {

    public CustomMediaController(Context context) {
        super(new ContextThemeWrapper(context, R.style.CustomMediaControllerTheme));
    }


}
