package com.mapghana.model;

import android.content.res.Resources;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;

/**
 * Created by bitu on 17/8/17.
 */

public class DeviceScreenModel {
    private static DeviceScreenModel deviceScreenModel;
    private Rect deviceRect;
    private float density;
    private float scaledDensity;
    private float xdpi;
    private int densityDpi;
    private DisplayMetrics displayMetrics;

    public DeviceScreenModel(DisplayMetrics displayMetrics) {
        this.displayMetrics = displayMetrics;
        this.deviceRect = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.density = displayMetrics.density;
        this.xdpi = displayMetrics.xdpi;
        this.scaledDensity = displayMetrics.scaledDensity;
        this.densityDpi = displayMetrics.densityDpi;
    }

    public DisplayMetrics getDisplayMetrics() {
        return displayMetrics;
    }

    public static DeviceScreenModel getInstance() {
        if (deviceScreenModel == null) {
            deviceScreenModel = new DeviceScreenModel(Resources.getSystem().getDisplayMetrics());
        }
        return deviceScreenModel;
    }

    public Rect getDeviceRect() {
        return deviceRect;
    }

    public float getDensity() {
        return density;
    }


    public float getScaledDensity() {
        return scaledDensity;
    }


    public int getDensityDpi() {
        return densityDpi;
    }


    public int convertDpToPixel(float dp) {
        // return Math.round(dp * ((float) densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return Math.round(dp * density);
    }


    public int getNavigationViewWidth() {
        return (int) (deviceRect.width() * 0.80f);
    }


    public int getBannerHeight() {

        return (int) (deviceRect.height() * 0.35f);
    }

    public int getProfileBannerHeight() {
        return (int) (deviceRect.height() * 0.30f);

    }

    public int getWidth() {
        return (int) (deviceRect.width());
    }

    public int getWidth(int width, float value) {
        int v = (int) value;
        return (width * v);
    }

    public int getWidth(float percent) {
        return Math.round(deviceRect.width() * percent);
    }

    public int getHeight(float percent) {
        return Math.round(deviceRect.height() * percent);
    }


    public LinearLayout.LayoutParams getLinearLayoutParams(LinearLayout linearLayout,
                                                           int width,
                                                           int height,
                                                           int left,
                                                           int top,
                                                           int right,
                                                           int bottom) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        linearLayout.setPadding(left, top, right, bottom);
        return layoutParams;
    }


}
