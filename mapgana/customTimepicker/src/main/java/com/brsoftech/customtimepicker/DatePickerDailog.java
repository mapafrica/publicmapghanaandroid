package com.brsoftech.customtimepicker;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;

import com.wheel.AbstractWheelTextAdapter;
import com.wheel.ArrayWheelAdapter;
import com.wheel.NumericWheelAdapter;
import com.wheel.OnWheelChangedListener;
import com.wheel.WheelView;

import java.util.Calendar;

public class DatePickerDailog extends Dialog {

    private Context Mcontex;

    private int NoOfYear = 40;

    private static final int showingitemsCount = 5;

    public DatePickerDailog(Context context, Calendar calendar,
                            final DatePickerListner dtp) {

        super(context);
        AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE = (int) context
                .getResources().getDimension(R.dimen.text_size);
        int btnTB_p = context.getResources().getDimensionPixelSize(
                R.dimen.button_padding_tb);
        int btnLR_p = context.getResources().getDimensionPixelSize(
                R.dimen.button_padding_lf);
        Mcontex = context;

        LinearLayout lytmain = new LinearLayout(Mcontex);
        lytmain.setOrientation(LinearLayout.VERTICAL);
        lytmain.setBackgroundColor(Color.parseColor("#FFFFFF"));
        LinearLayout lytdate = new LinearLayout(Mcontex);
        LinearLayout lytbutton = new LinearLayout(Mcontex);
        lytbutton.setBackgroundColor(Color.parseColor("#90DEDEDE"));

        TextView btnset = new TextView(Mcontex);
        TextView btncancel = new TextView(Mcontex);
        btnset.setBackgroundResource(R.drawable.roundright_bg);
        btnset.setTextColor(Color.parseColor("#000000"));
        btncancel.setBackgroundResource(R.drawable.roundleft_bg);
        btncancel.setTextColor(Color.parseColor("#000000"));

        btnset.setTextSize(AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE);
        btncancel.setTextSize(AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE);
        btnset.setText("Set");
        btncancel.setText("Cancel");

        final WheelView month = new WheelView(Mcontex);
        month.setExtratext("M");
        month.setCyclic(true);
        month.setVisibleItems(showingitemsCount);
        final WheelView year = new WheelView(Mcontex);
        year.setExtratext("Y");
        year.setCyclic(true);
        year.setVisibleItems(showingitemsCount);
        final WheelView day = new WheelView(Mcontex);
        day.setExtratext("D");
        day.setCyclic(true);
        day.setVisibleItems(showingitemsCount);

        lytdate.addView(day, new LinearLayout.LayoutParams(0,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.20f));
        lytdate.addView(month, new LinearLayout.LayoutParams(0,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.60f));
        lytdate.addView(year, new LinearLayout.LayoutParams(0,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.20f));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutParams pa = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1);

        lytbutton.addView(btncancel, pa);
        lytbutton.addView(btnset, pa);
        btnset.setPadding(0, 15, 0, 15);
        btncancel.setPadding(0, 15, 0, 15);
        btnset.setGravity(Gravity.CENTER);
        btncancel.setGravity(Gravity.CENTER);
        lytbutton.setPadding(btnLR_p, btnTB_p, btnLR_p, btnTB_p);
        LinearLayout.LayoutParams pa1 = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.FILL_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

        lytdate.setLayoutParams(pa1);

        final TextView Header_tv = new TextView(Mcontex);
        Header_tv.setLayoutParams(pa1);
        Header_tv.setTextSize(AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE);
        Header_tv.setText("a");
        Header_tv.setGravity(Gravity.CENTER);
        Header_tv.setTextColor(Color.parseColor("#4884E7"));
        Header_tv.setBackgroundColor(Color.parseColor("#90DEDEDE"));
        Header_tv.setPadding(0, 15, 0, 15);

        lytmain.addView(Header_tv);
        lytmain.addView(lytdate);
        lytmain.addView(lytbutton);

        setContentView(lytmain);

        getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);

        // month
        int curMonth = calendar.get(Calendar.MONTH);
        String months[] = new String[]{"January", "February", "March",
                "April", "May", "June", "July", "August", "September",
                "October", "November", "December"};
        OnWheelChangedListener listener = new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                Calendar c = updateDays(year, month, day);
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH) + 1;
                int year = c.get(Calendar.YEAR);
                String date = (day <= 9 ? "0" + day : day + "") + "-"
                        + (month <= 9 ? "0" + month : month + "") + "-" + year;
                Header_tv.setText(date);
            }
        };
        month.addChangingListener(listener);
        year.addChangingListener(listener);
        day.addChangingListener(listener);

        month.setViewAdapter(new DateArrayAdapter(context, months, curMonth));
        month.setCurrentItem(curMonth);

        Calendar cal = Calendar.getInstance();
        // year
        int curYear = calendar.get(Calendar.YEAR);
        int Year = cal.get(Calendar.YEAR);

        year.setViewAdapter(new DateNumericAdapter(context, Year - NoOfYear,
                Year + NoOfYear, NoOfYear));
        year.setCurrentItem(curYear - (Year - NoOfYear));

        // day
        updateDays(year, month, day);
        day.setCurrentItem(calendar.get(Calendar.DAY_OF_MONTH) - 1);

        btnset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar c = updateDays(year, month, day);
                dtp.OnDoneButton(DatePickerDailog.this, c);
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dtp.OnCancelButton(DatePickerDailog.this);

            }
        });

    }

    Calendar updateDays(WheelView year, WheelView month, WheelView day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,
                calendar.get(Calendar.YEAR)
                        + (year.getCurrentItem() - NoOfYear));
        calendar.set(Calendar.MONTH, month.getCurrentItem());

        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        day.setViewAdapter(new DateNumericAdapter(Mcontex, 1, maxDays, calendar
                .get(Calendar.DAY_OF_MONTH) - 1));
        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
        day.setCurrentItem(curDay - 1, true);
        calendar.set(Calendar.DAY_OF_MONTH, curDay);
        return calendar;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(R.color.semiblack);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.windowAnimations = R.style.dialog_style;
        wlmp.gravity = Gravity.BOTTOM;

        setTitle(null);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setOnCancelListener(null);
    }

    private class DateNumericAdapter extends NumericWheelAdapter {
        int currentItem;
        int currentValue;

        public DateNumericAdapter(Context context, int minValue, int maxValue,
                                  int current) {
            super(context, minValue, maxValue);
            this.currentValue = current;
            // setTextSize(20);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(null, Typeface.BOLD);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    private class DateArrayAdapter extends ArrayWheelAdapter<String> {
        int currentItem;
        int currentValue;

        public DateArrayAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            // setTextSize(20);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(null, Typeface.BOLD);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    public interface DatePickerListner {
        public void OnDoneButton(Dialog dialog, Calendar c);

        public void OnCancelButton(Dialog dialog);
    }
}
