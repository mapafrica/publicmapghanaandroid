package com.brsoftech.customtimepicker;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;

import com.wheel.AbstractWheelTextAdapter;
import com.wheel.ArrayWheelAdapter;
import com.wheel.OnWheelChangedListener;
import com.wheel.WheelView;

import java.util.Calendar;

public class TimePickerDialog extends Dialog {

    private Context Mcontex;
    private static final int showingitemsCount = 5;

    public TimePickerDialog(Context context, Calendar calendar,
                            final TimePickerListner dtp) {

        super(context);
        AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE = context.getResources()
                .getDimensionPixelSize(R.dimen.text_size);

        int btnTB_p = context.getResources().getDimensionPixelSize(
                R.dimen.button_padding_tb);
        int btnLR_p = context.getResources().getDimensionPixelSize(
                R.dimen.button_padding_lf);
        Mcontex = context;
        LinearLayout lytmain = new LinearLayout(Mcontex);
        lytmain.setOrientation(LinearLayout.VERTICAL);
        lytmain.setBackgroundColor(Color.parseColor("#FFFFFF"));
        LinearLayout lytdate = new LinearLayout(Mcontex);
        LinearLayout lytbutton = new LinearLayout(Mcontex);
        lytbutton.setBackgroundColor(Color.parseColor("#90DEDEDE"));

        TextView btnset = new TextView(Mcontex);
        TextView btncancel = new TextView(Mcontex);
        btnset.setBackgroundResource(R.drawable.roundright_bg);
        btnset.setTextColor(Color.parseColor("#000000"));
        btncancel.setBackgroundResource(R.drawable.roundleft_bg);
        btncancel.setTextColor(Color.parseColor("#000000"));
        btnset.setTextSize(AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE);
        btncancel.setTextSize(AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE);
        btnset.setText("Set");
        btncancel.setText("Cancel");

        final WheelView hour = new WheelView(Mcontex);
        hour.setCyclic(true);
        hour.setExtratext("H");
        hour.setVisibleItems(showingitemsCount);
        hour.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                // TODO Auto-generated method stub

            }
        });
        final WheelView minute = new WheelView(Mcontex);
        minute.setCyclic(true);
        minute.setExtratext("M");
        minute.setVisibleItems(showingitemsCount);
        final WheelView am_pm = new WheelView(Mcontex);
        am_pm.setVisibleItems(showingitemsCount);
       //  am_pm.setCyclic(true);

        lytdate.addView(hour, new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.30f));
        lytdate.addView(minute, new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.40f));
        lytdate.addView(am_pm, new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.30f));
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutParams pa = new LayoutParams(
                android.view.ViewGroup.LayoutParams.FILL_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1);

        lytbutton.addView(btncancel, pa);
        lytbutton.addView(btnset, pa);
        btnset.setPadding(0, 15, 0, 15);
        btncancel.setPadding(0, 15, 0, 15);
        btnset.setGravity(Gravity.CENTER);
        btncancel.setGravity(Gravity.CENTER);
        lytbutton.setPadding(btnLR_p, btnTB_p, btnLR_p, btnTB_p);
        LinearLayout.LayoutParams pa1 = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.FILL_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

        lytdate.setLayoutParams(pa1);

        final TextView Header_tv = new TextView(Mcontex);
        Header_tv.setLayoutParams(pa1);
        Header_tv.setTextSize(AbstractWheelTextAdapter.DEFAULT_TEXT_SIZE);
        Header_tv.setText("a");
        Header_tv.setGravity(Gravity.CENTER);
        Header_tv.setTextColor(Color.parseColor("#4884E7"));
        Header_tv.setBackgroundColor(Color.parseColor("#90DEDEDE"));
        Header_tv.setPadding(0, 15, 0, 15);
        lytmain.addView(Header_tv);
        lytmain.addView(lytdate);
        lytmain.addView(lytbutton);

        setContentView(lytmain);

        getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        OnWheelChangedListener listener = new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                Calendar c = updateTimes(hour, minute, am_pm);
                int hour = c.get(Calendar.HOUR);
                int minute = c.get(Calendar.MINUTE);
                int amPm = c.get(Calendar.AM_PM);
                String time = (hour <= 9 ? (hour == 0 ? "12" : "0" + hour)
                        : hour + "")
                        + ":"
                        + (minute <= 9 ? "0" + minute : minute + "")
                        + " "
                        + (amPm == 0 ? "AM" : "PM");
                Header_tv.setText(time);
            }
        };

        // month
        // int curMonth = calendar.get(Calendar.MONTH);
        hour.addChangingListener(listener);
        minute.addChangingListener(listener);
        am_pm.addChangingListener(listener);
        int curHour = calendar.get(Calendar.HOUR);
        String hours[] = new String[]{"1", "2", "3", "4", "5", "6", "7", "8",
                "9", "10", "11", "12"};
        hour.setViewAdapter(new TimeArrayAdapter(context, hours, curHour - 1));
        hour.setCurrentItem(curHour - 1);

        int curMin = calendar.get(Calendar.MINUTE);
        String minutes[] = new String[]{"00", "01", "02", "03", "04", "05",
                "06", "07", "08", "09", "10", "11", "12", "13", "14", "15",
                "16", "17", "18", "19", "20", "21", "22", "23", "24", "25",
                "26", "27", "28", "29", "30", "31", "32", "33", "34", "35",
                "36", "37", "38", "39", "40", "41", "42", "43", "44", "45",
                "46", "47", "48", "49", "50", "51", "52", "53", "54", "55",
                "56", "57", "58", "59"};
        minute.setViewAdapter(new TimeArrayAdapter(context, minutes, curMin));
        minute.setCurrentItem(curMin);

        int curAmPm = calendar.get(Calendar.AM_PM);
        String ampms[] = new String[]{"AM", "PM"};
        am_pm.setViewAdapter(new TimeArrayAdapter(context, ampms, curAmPm));
        am_pm.setCurrentItem(curAmPm);

        btnset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar c = updateTimes(hour, minute, am_pm);
                dtp.OnDoneButton(TimePickerDialog.this, c);
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dtp.OnCancelButton(TimePickerDialog.this);

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(R.color.semiblack);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.windowAnimations = R.style.dialog_style;
        wlmp.gravity = Gravity.BOTTOM;

        setTitle(null);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setOnCancelListener(null);
    }

    Calendar updateTimes(WheelView hous, WheelView minute, WheelView am_pm) {
        Calendar calendar = Calendar.getInstance();
        int h = hous.getCurrentItem() + 1;
        if (h == 12) {
            h = 0;
        }
        calendar.set(Calendar.HOUR, h);
        calendar.set(Calendar.MINUTE, minute.getCurrentItem());
        calendar.set(Calendar.AM_PM, am_pm.getCurrentItem());
        return calendar;

    }

    private class TimeArrayAdapter extends ArrayWheelAdapter<String> {
        int currentItem;
        int currentValue;

        public TimeArrayAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;

            // setTextSize(20);

        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(null, Typeface.BOLD);

        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;

            View v = super.getItem(index, cachedView, parent);
            // ((TextView) v).setTextColor(0xFF0000F0);

            return v;
        }
    }

    public interface TimePickerListner {
        public void OnDoneButton(Dialog dialog, Calendar c);

        public void OnCancelButton(Dialog dialog);
    }

}
