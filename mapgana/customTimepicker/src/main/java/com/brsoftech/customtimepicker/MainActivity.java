package com.brsoftech.customtimepicker;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	}

	public void showDatepicker(View v) {
		DatePickerDailog dp1 = new DatePickerDailog(MainActivity.this,
				Calendar.getInstance(),
				new DatePickerDailog.DatePickerListner() {

					@Override
					public void OnDoneButton(Dialog dialog, Calendar c) {
						// TODO Auto-generated method stub

						String Date = c.get(Calendar.YEAR) + "-"
								+ c.get(Calendar.MONTH) + "-"
								+ c.get(Calendar.DAY_OF_MONTH);
						Toast.makeText(MainActivity.this, Date,
								Toast.LENGTH_LONG).show();
						dialog.dismiss();
					}

					@Override
					public void OnCancelButton(Dialog dialog) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

		dp1.show();
	}

	public void showTimepicker(View v) {
		TimePickerDialog dp = new TimePickerDialog(MainActivity.this,
				Calendar.getInstance(),
				new TimePickerDialog.TimePickerListner() {

					@Override
					public void OnDoneButton(Dialog dialog, Calendar c) {
						// TODO Auto-generated method stub
						String Time = c.get(Calendar.HOUR) + ":"
								+ c.get(Calendar.MINUTE) + " "
								+ c.get(Calendar.AM_PM);
						Toast.makeText(MainActivity.this, Time,
								Toast.LENGTH_LONG).show();
						dialog.dismiss();
					}

					@Override
					public void OnCancelButton(Dialog dialog) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

		dp.show();
	}

}
