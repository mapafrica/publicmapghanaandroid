package com.wheel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomitemLayout extends LinearLayout {
	WheelView wheelView;
	String extratext = "";

	public static final int MAXRotation = 65;

	public int rotaionAngle = 0;

	@SuppressLint("NewApi")
	public CustomitemLayout(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}

	public CustomitemLayout(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public CustomitemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public CustomitemLayout(Context context, WheelView wheelView) {
		super(context);
		this.wheelView = wheelView;
		extratext = wheelView.getExtraText();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		changesChilsview();
		super.onDraw(canvas);
	}

	private void changesChilsview() {
		// int totalcount = wheelView.getViewAdapter().getItemsCount();
		// if (totalcount > getChildCount()) {
		int j = getChildCount() / 2;
		int childCount = getChildCount();

		int totalcount = wheelView.getViewAdapter().getItemsCount();
		int curretnI = wheelView.getCurrentItem();
		CharSequence charsle = wheelView.getViewAdapter().getItemText(curretnI);
		if (j > 0 && charsle != null) {
			String selected = charsle.toString();
			rotaionAngle = MAXRotation / j;
			for (int i = 0; i < this.getChildCount(); i++) {
				View child = getChildAt(i);
				if (child instanceof TextView) {
					TextView tv = ((TextView) child);
					String text = tv.getText().toString().split(" ")[0];
					if (text.equals(selected) || text.equals("0" + selected)) {
						tv.setTextColor(0xff4884E7);
						tv.setTypeface(Typeface.DEFAULT, Typeface.BOLD);

						// text += " " + extratext;
						if (!extratext.isEmpty()) {
							Spanned s = Html.fromHtml(text + " "
									+ "<sup><small><small>" + extratext
									+ "</small></small></sup>");
							tv.setText(s);
						} else {
							tv.setText(text);
						}
						if (totalcount > childCount) {
							tv.setRotationX(getNormalizerotation(i, j));
						} else {
							tv.setRotationX(0);
						}
					} else {
						tv.setTextColor(0x60000000);
						tv.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);

						tv.setText(text);
						if (totalcount > childCount) {
							tv.setRotationX(getNormalizerotation(i, j));
						} else {
							tv.setRotationX(0);
						}
					}

				}
			}
		}
		// }
	}

	public int getNormalizerotation(int i, int center) {
		if (i < center) {
			return (center - i) * rotaionAngle;

		} else if (i > center) {
			return -(i - center) * rotaionAngle;
		} else {
			return 0;
		}

	}

	@Override
	protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
		// TODO Auto-generated method stub
		// if (child instanceof TextView) {
		// ((TextView) child).setTextColor(0xffff0000);
		// }
		return super.drawChild(canvas, child, drawingTime);
	}

}
